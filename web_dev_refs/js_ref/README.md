# JavaScript Reference

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

JavaScript is a cross-platform, object-oriented scripting language used to make webpages interactive. There are also more advanced server side versions of JavaScript (e.g., Node.js), which allow you to add more functionality to a website than downloading files (e.g., realtime collaboration between multiple computers). Inside a host environment like a web browser, JavaScript can be used to programmatically control the environment objects.

JavaScript contains a standard library of objects and a core set of language elements. Core JavaScript can be extended for a variety of purposes by supplementing it with additional objects. For example:

- *Client-side JavaScript* extends the core language by supplying objects to control a browser and its Document Object Model (DOM).
- *Server-side JavaScript* extends the core language by supplying objects relevant to running JavaScript on a server.

This means that, in the browser, JavaScript can change the way the webpage (DOM) looks. Likewise, Node.js JavaScript on the server can respond to custom requests sent by code executed in the browser.

## JavaScript and Java

JavaScript and Java are similar in some ways, but fundamentally different in other ways. The JavaScript language resembles Java, but does not have Java's static typing and strong type checking. JavaScript follows most Java expression syntax, naming conventions, and basic control-flow constructs, which was why it was renamed from LiveScript to JavaScript.

In contrast to Java's compile-time system of classes built by declarations, JavaScript supports a runtime system based on a small number of data types representing numeric, Boolean, and string values.

JavaScript has a prototype-based object model instead of the more common class-based object model. The prototype-based model provides dynamic inheritance, i.e., what is inherited can vary for individual objects.

JavaScript also supports functions without any special declarative requirements. Functions can be properties of objects, executing as loosely typed methods.

Compared to Java, JavaScript is a free-form language. You do not have to declare all variables, classes, and methods. You do not have to be concerned with whether methods are public, private, or protected, and you do not have to implement interfaces. Variables, parameters, and function return types are not explicitly typed.

Java is a class-based programming language designed for fast execution and type safety (e.g., you cannot cast a Java integer into an object reference or access private memory by corrupting the Java bytecode). Java's class-based model means that programs exclusively consist of classes and their methods. Java's class inheritance and strong typing generally require tightly coupled object hierarchies.

In contrast, JavaScript descends in spirit from a line of smaller, dynamically typed languages, like HyperTalk and dBase.

## JavaScript and the ECMAScript Specification

JavaScript is standardized at [Ecma International](https://ecma-international.org/), the European association for standardizing information and communication systems (ECMA was formerly an acronym for the European Computer Manufacturers Association) to deliver a standardized, international programming language based on JavaScript. This standardized version of JavaScript, called ECMAScript, behaves the same way in all applications that support the standard. The ECMAScript standard is documented in the [ECMA-262 specification](https://ecma-international.org/publications-and-standards/standards/ecma-262/).

The ECMAScript specification does not describe the Document Object Model (DOM), which is standardized by the [World Wide Web Consortium (W3C)](https://www.w3.org/) and/or [WHATWG (Web Hypertext Application Technology Working Group)](https://whatwg.org/). The DOM defines the way in which HTML document objects are exposed to a script.

The ECMAScript specification is a set of requirements for implementing ECMAScript. It is useful if you want to implement standards-compliant language features in your ECMAScript implementation or engine.

The ECMAScript document is not intended to help script programmers. It may use  terminology and syntax that is unfamiliar to a JavaScript programmer. The JavaScript documentation describes aspects of the language that are appropriate for a JavaScript programmer.

Although the description of the language may differ in ECMAScript, the language itself remains the same. JavaScript supports all functionality outlined in the ECMAScript specification.

## Entering and Running JavaScript

A useful tool for exploring JavaScript is the JavaScript Console (sometimes called the Web Console, or just the console). This is a tool that enables you to enter JavaScript and run it in the current page.

The [console](https://firefox-source-docs.mozilla.org/devtools-user/web_console/) works the same way as the `eval()` function. The last expression entered is returned. It can be imagined that every time something is entered into the console, it is actually surrounded by `console.log` around `eval`:

```js
console.log(eval("2 + 2"));
```

## Grammar and Types

### Basics

JavaScript borrows most of its syntax from Java, C, and C++, but it has also been influenced by Awk, Perl, and Python.

JavaScript is case-sensitive and uses the Unicode character set. Instructions are called [statements](https://developer.mozilla.org/en-US/docs/Glossary/Statement) and are separated by semicolons (`;`). It is considered best practice to always write a semicolon after a statement, even when it is not strictly needed (e.g., when a statement is written on its own line).

The source text of JavaScript script gets scanned from left to right, and is converted into a sequence of input elements which are tokens, control characters, line terminators, comments, or [whitespace](https://developer.mozilla.org/en-US/docs/Glossary/Whitespace).

### Comments

The syntax of comments is the same as in C++ and in many other languages:

```js
// A one line comment

/*
This is a longer,
multi-line comment
*/
```

You cannot nest multi-line (block) comments.

Comments behave like whitespace and are discarded during script execution.

### Declarations

JavaScript has three kinds of variable declarations:

- [`var`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/var) - Declares a variable, optionally initializing it to a value.
- [`let`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let) - Declares a block-scoped, local variable, optionally initializing it to a value.
- [`const`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const) - Declares a block-scoped, read-only named constant.

#### Variables

Variables are used as symbolic names for values. The names of variables, called [identifiers](https://developer.mozilla.org/en-US/docs/Glossary/Identifier), conform to certain rules.

Usually, a JavaScript identifier starts with a letter, underscore (`_`), or dollar sign (`$`). Subsequent characters can also be digits (`0` – `9`). Because JavaScript is case sensitive, letters include the characters `A` through `Z` (uppercase), as well as `a` through `z` (lowercase).

#### Declaring Variables

You can declare a variable in two ways:

- With the keyword `var` (e.g., `var x = 42`). This syntax can be used to declare both local and global variables, depending on the execution context.
- With the keyword `let` or `const` (e.g., `let y = 13`). This syntax can be used to declare a block-scope local variable.

You can declare variables to unpack values using the [destructuring assignment](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment) syntax:

```js
let a, b, rest;

[a, b] = [10, 20];

console.log(a);
// 10
console.log(b);
// 20

[a, b, ...rest] = [10, 20, 30, 40, 50];

console.log(rest);
// Array [30, 40, 50]
```

Variables should always be declared before they are used. JavaScript used to allow assigning to undeclared variables, which creates an [undeclared global variable](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/var#description). This is an error in [strict mode](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode#assigning_to_undeclared_variables) and should be avoided altogether.

#### Declaration and Initialization

In a statement like `let x = 5`, the let x part is the *declaration*, and the `= 42` part is the *initializer*. The declaration allows the variable to be accessed later in code without throwing a [`ReferenceError`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ReferenceError), while the initializer assigns a value to the variable. In `var` and `let` declarations, the initializer is optional. If a variable is declared without an initializer, it is assigned the value [`undefined`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined).

`const` declarations always need an initializer because they forbid any kind of assignment after declaration.

#### Variable Scope

A variable may belong to one of the following [scopes](https://developer.mozilla.org/en-US/docs/Glossary/Scope):

- Global scope - The default scope for all code running in script mode.
- Module scope - The scope for code running in module mode.
- Function scope - The scope created with a [function](https://developer.mozilla.org/en-US/docs/Glossary/Function).

Variables declared with `let` or `const` can belong to an additional scope:

- Block scope - The scope created with a pair of curly braces (a [block](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/block)).


When you declare a variable outside of any function, it is called a *global* variable because it is available to any other code in the current document. When you declare a variable within a function, it is called a *local* variable because it is available only within that function.

`let` and `const` declarations can also be scoped to the [block statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#block_statement) that they are declared in.

```js
if (Math.random() > 0.5) {
  const y = 5;
}
console.log(y);
// ReferenceError: y is not defined
```

However, variables created with `var` are not block-scoped, but only local to the function (or global scope) that the block resides within.

```js
if (true) {
  var x = 5;
}
console.log(x);
// 5
```

#### Variable Hoisting

`var`-declared variables are [hoisted](https://developer.mozilla.org/en-US/docs/Glossary/Hoisting), meaning that you can refer to the variable anywhere in its scope, even if its declaration is not reached yet. You can see `var` declarations as being *lifted* to the top of its function or global scope.

However, if you access a variable before it is declared, the value is always `undefined` because only its declaration and default initialization (with `undefined`) is hoisted, but not its value assignment.

```js
console.log(x === undefined);
// true
var x = 4;


(function () {
  console.log(x);
  // undefined
  var x = "local value";
})();
```

The above examples will be interpreted the same as:

```js
var x;
console.log(x === undefined);
// true
x = 4;


(function () {
  var x;
  console.log(x);
  // undefined
  x = "local value";
})();
```

Due to hoisting, all `var` statements in a function should be placed as near to the top of the function as possible.

Referencing a variable created with `let` or `const` in a block before the variable declaration always results in a [`ReferenceError`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ReferenceError) because the variable is in a [temporal dead zone](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let#temporal_dead_zone_tdz) from the start of the block until the declaration is processed.

```js
console.log(x);
// ReferenceError
const x = 4;

console.log(y);
// ReferenceError
let y = 4;
```

Function declarations are entirely hoisted, i.e., you can safely call the function anywhere in its scope.

#### Global Variables

Global variables are properties of the *global object*.

In web pages, the global object is [`window`](https://developer.mozilla.org/en-US/docs/Web/API/Window), so you can read and set global variables using the `window.variable` syntax. In all environments, the [`globalThis`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/globalThis) variable (which itself is a global variable) may be used to read and set global variables. This is to provide a consistent interface among various JavaScript runtimes.

Consequently, you can access global variables declared in one window or frame from another window or frame by specifying the `window` or `frame` name. For example, if a variable called `mochi` is declared in a document, you can refer to this variable from an `iframe` as `parent.mochi`.

#### Constants

You can create a read-only, named constant with the [`const`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const) keyword.

```js
const PI = 3.14;
```

A constant cannot change value through assignment or be re-declared while the script is running. It must be initialized to a value. The scope rules for constants are the same as those for `let` block-scope variables.

You cannot declare a constant with the same name as a function or variable in the same scope:

```js
// This will cause an error
function f() {}


const f = 5;
```

```js
// This will cause an error too
function f() {
  const g = 5;
  var g;
}
```

However, `const` only prevents *re-assignments*, but does not prevent *mutations*. The properties of objects assigned to constants are not protected, so the following statement is executed without problems:

```js
const EX_OBJECT = {key: "value"};

console.log(EX_OBJECT);
// Object { key: "value" }

EX_OBJECT.key = "otherValue";

console.log(EX_OBJECT);
// Object { key: "otherValue" }
```

Also, the contents of an array are not protected, so the following statement is executed without problems:

```js
const EX_ARRAY = ["HTML", "CSS"];

EX_ARRAY.push("Javascript");

console.log(EX_ARRAY);
// Array(3) [ "HTML", "CSS", "Javascript" ]
```

### Data Structures and Types

#### Data Types

The latest ECMAScript standard defines eight data types:

- Seven data types are [primitives](https://developer.mozilla.org/en-US/docs/Glossary/Primitive):
    1. [Boolean](https://developer.mozilla.org/en-US/docs/Glossary/Boolean) - `true` and `false`.
    2. [null](https://developer.mozilla.org/en-US/docs/Glossary/Null) - A special keyword denoting a null value. Since JavaScript is case-sensitive, `null` is not the same as `Null`, `NULL`, or any other variant.
    3. [undefined](https://developer.mozilla.org/en-US/docs/Glossary/Undefined) - A top-level property whose value is not defined.
    4. [Number](https://developer.mozilla.org/en-US/docs/Glossary/Number) - An integer or floating point number.
    5. [BigInt](https://developer.mozilla.org/en-US/docs/Glossary/BigInt) - An integer with arbitrary precision. For example: `9007199254740992n`.
    6. [String](https://developer.mozilla.org/en-US/docs/Glossary/String) - A sequence of characters that represent a text value.
    7. [Symbol](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol) - A data type whose instances are unique and immutable.
- [Object](https://developer.mozilla.org/en-US/docs/Glossary/Object)

[Functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions) are the other fundamental elements of the language. While functions are technically a kind of object, you can think of objects as labels for value containers, and functions as procedures that your script can perform.

#### Data Type Conversion

JavaScript is a *dynamically typed* language, which means you do not have to specify the data type of a variable when you declare it. It also means that data types are automatically converted, as-needed, during script execution.

#### Numbers and the `+` Operator

In expressions involving numeric and string values with the `+` (unary plus) operator, JavaScript converts numeric values to strings:

```js
x = "The answer is " + 24;  // "The answer is 24"
y = 24 + " is the answer";  // "24 is the answer"
z = "24" + 4;  // "244"
```

With all other operators, JavaScript does *not* convert numeric values to strings:

```js
"24" - 4;  // 20
"24" * 4;  // 96
```

#### Converting Strings to Numbers

In the case that a value representing a number is in memory as a string, there are methods for conversion:

- [`.parseInt()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt)
    - A best practice for `.parseInt()` is to always include the `radix` parameter. The `radix` parameter is used to specify which numerical system is to be used.
- [`.parseFloat()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseFloat)

```js
parseInt("101", 10);  // 101
```

An alternative method of retrieving a number from a string is with the `+` operator:

```js
"1.1" + "1.1";  // '1.11.1'
(+"1.1") + (+"1.1");  // 2.2
// Note: The parentheses are added for clarity and are not required.
```

### Literals

*Literals* represent values in JavaScript. These are fixed values (not variables) that you *literally* provide in your script.

#### Array Literals

An array literal is a list of zero or more expressions, each of which represents an array element, enclosed in square brackets (`[]`). When you create an array using an array literal, it is initialized with the specified values as its elements, and its `length` is set to the number of arguments specified.

```js
const coffees = ["french roast", "colombian", "kona"];
```

##### Extra Commas In Array Literals

If you put two commas in a row in an array literal, the array leaves an empty slot for the unspecified element:

```js
const teas = ["sencha", , "matcha"];
console.log(teas);
// Array(3) [ "sencha", <1 empty slot>, "matcha" ]
```

Note that the second item is *empty*, which is not exactly the same as the actual `undefined` value. When using array-traversing methods like `Array.prototype.map()`, empty slots are skipped. However, index-accessing `teas[1]` still returns `undefined`.

If you include a trailing comma at the end of the list of elements, the comma is ignored.

In the following example, the length of the array is four, and `teas[1]` and `teas[3]` are missing. Only the last comma is ignored:

```js
const teas = ["sencha",  , "matcha",  ,];
console.log(teas.length);
// 4
```

[Trailing commas](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Trailing_commas) help keep git diffs clean when you have a multi-line array because appending an item to the end only adds one line, but does not modify the previous line:

```diff
const teas = [
  "sencha",
  "matcha",
+ "hōjicha",
];
```

When writing your own code, you should explicitly declare missing elements as `undefined`, or at least insert a comment to highlight their absence. Doing this increases your code's clarity and maintainability.

```js
const coffees = ["french roast", undefined, "colombian", undefined,];
const teas = ["sencha", /* empty */, "matcha", /* empty */,];
```

#### Boolean Literals

The Boolean type has two literal values, `true` and `false`.

Do not confuse the primitive Boolean values `true` and `false` with the true and false values of the [`Boolean`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean#boolean_primitives_and_boolean_objects) object. The `Boolean` object is a wrapper around the primitive Boolean data type.

#### Numeric Literals

JavaScript numeric literals include integer literals in different bases, as well as floating-point literals in base-10.

The language specification requires numeric literals to be unsigned. Nevertheless, code fragments like `-123.4` are fine, being interpreted as a unary `-` operator applied to the numeric literal `123.4`.

##### Integer Literals

Integer and `BigInt` literals can be written in decimal (base 10), hexadecimal (base 16), octal (base 8), and binary (base 2).

- A *decimal* integer literal is a sequence of digits without a leading `0`.
- A leading `0` on an integer literal, or a leading `0o` (or `0O`) indicates it is in *octal*. Octal integer literals can include only the digits `0`-`7`.
- A leading `0x` (or `0X`) indicates a *hexadecimal* integer literal. Hexadecimal integers can include digits (`0`-`9`) and the letters `a`-`f` and `A`-`F`.

    The case of a character does not change its value. Therefore, `0xa` === `0xA` === `10` and `0xf === `0xF` === `15`.

- A leading `0b` (or `0B`) indicates a *binary* integer literal. Binary integer literals can only include the digits `0` and `1`.
- A trailing `n` suffix on an integer literal indicates a `BigInt` literal. The `BigInt` literal can use any of the above bases. Note that leading-zero octal syntax like `0123n` is not allowed, but `0o123n` is fine.

For more information, see [Numeric literals in the Lexical grammar reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#numeric_literals).

##### Floating-Point Literals

A floating-point literal can have the following parts:

- An unsigned decimal integer
- A decimal point (`.`)
- A fraction (another decimal number)
- An exponent

The exponent part is an `e` or `E` followed by an integer, which can be signed (preceded by `+` or `-`). A floating-point literal must have at least one digit, and either a decimal point or `e` (or `E`).

Succinctly, the syntax is:

```text
[digits].[digits][(E|e)[(+|-)]digits]
```

For example:

```js
3.1415926
.123456789
3.1E+12
.1e-23
```

#### Object Literals

An object literal is a list of zero or more pairs of property names and associated values of an object, enclosed in curly braces (`{}`):

```js
const sales = "Toyota";


function carTypes(name) {
  return name === "Kia" ? name : `Sorry, we don't sell ${name}.`;
}


const car = {
  currCar: "Hyundai",
  getCar: carTypes("Kia"),
  special: sales
};

console.log(car.currCar);
// Hyundai
console.log(car.getCar);
// Kia
console.log(car.special);
// Toyota
```

Do not use an object literal at the beginning of a statement. This will lead to an error (or not behave as you expect) because the `{` will be interpreted as the beginning of a block.

Additionally, you can use a numeric or string literal for the name of a property or nest an object inside another:

```js
const car = {
  cars: {
    a: "Saab",
    b: "Jeep"
  },
  7: "Mazda"
};

console.log(car.cars.b);
// Jeep
console.log(car[7]);
// Mazda
```

Object property names can be any string, including an empty string. If the property name would not be a valid JavaScript identifier or number, it must be enclosed in quotes.

Property names that are not valid identifiers cannot be accessed as a dot (`.`) property:

```js
const unusualPropertyNames = {
  "": "An empty string",
  "!": "Bang!"
}

console.log(unusualPropertyNames."");
// Uncaught SyntaxError: missing name after . operator
console.log(unusualPropertyNames.!);
// Uncaught SyntaxError: missing name after . operator
```

Instead, they must be accessed with the bracket notation (`[]`):

```js
const unusualPropertyNames = {
  "": "An empty string",
  "!": "Bang!"
}

console.log(unusualPropertyNames[""]);
// An empty string
console.log(unusualPropertyNames["!"]);
// Bang!
```

##### Enhanced Object Literals

Object literals support a range of shorthand syntaxes that include setting the prototype at construction, shorthand for `foo: foo` assignments, defining methods, making `.super()` calls, and computing property names with expressions.

Together, these also bring object literals and class declarations closer together, and allow object-based design to benefit from some of the same conveniences.

```js
const obj = {
  // __proto__
  __proto__: theProtoObj,
  // Shorthand for 'handler: handler'
  handler,
  // Methods
  toString() {
    // Super calls
    return "d " + super.toString();
  },
  // Computed (dynamic) property names
  ["prop_" + (function () {return 42})()]: 42,
};
```

#### Regex Literals

A regex literal is a pattern enclosed between forward slashes (`//`):

```js
const re = /ab+c/;
```

#### String Literals

A string literal is zero or more characters enclosed in double (`"`) or single (`'`) quotation marks.

```js
'foo'
"bar"
'1234'
'one line\nanother line'
"Haru's cat"
```

You should use string literals unless you specifically need to use a [`String`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) object.

You can call any of the `String` object's methods on a string literal value. JavaScript automatically converts the string literal to a temporary `String` object, calls the method, then discards the temporary `String` object.

You can also use the `.length` property with a string literal:

```js
console.log("Haru's cat".length); // 10
```

Template literals are also available. Template literals are enclosed by the tick mark (`` ` ``) character instead of double or single quotes.


Template literals provide [syntactic sugar](https://en.wikipedia.org/wiki/Syntactic_sugar) for constructing strings:

```js
// Basic literal string creation
`In JavaScript, '\n' is a line-feed.`

// Multiline strings
`In JavaScript, template strings can run
 over multiple lines, but double and single
 quoted strings cannot.`

// String interpolation
const name = 'Luigi', time = 'today';
`Hello ${name}, how are you ${time}?`
```

[Tagged templates](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals#tagged_templates) are a compact syntax for specifying a template literal, along with a call to a *tag* function for parsing it. A tagged template is just a more succinct and semantic way to invoke a function that processes a string and a set of relevant values.

The name of the template tag function precedes the template literal. For example, the template tag function below is named `print`. The `print` function will interpolate the arguments and serialize any objects or arrays that may come up, avoiding the nettlesome `[object Object]`.

```js
const formatArg = function (arg) {
  if (Array.isArray(arg)) {
    // Print a bulleted list
    return arg.map(
      function (part) {
        return `- ${part}`
    }).join("\n");
  }

  if (arg.toString === Object.prototype.toString) {
    /* This object will be serialized to "[object Object]".
       Let us print something nicer. */
    return JSON.stringify(arg);
  }

  return arg;
};

const print = function (segments, ...args) {
  /* For any well-formed template literal, there will always be N args and
     (N+1) string segments. */
  let message = segments[0];

  segments.slice(1).forEach(
    function (segment, index) {
      message += formatArg(args[index]) + segment;
    }
  );

  console.log(message);
};

const todos = [
  "Learn JavaScript",
  "Learn Web APIs",
  "Set up website",
  "Be edified!",
];

const progress = {javascript: 20, html: 50, css: 10};

print`\
To do:

${todos}

Current progress is: ${progress}
`;

/*
To do:

- Learn JavaScript
- Learn Web APIs
- Set up website
- Be edified!

Current progress is: {"javascript":20,"html":50,"css":10}
*/
```

Since tagged template literals are just syntactic sugar for function calls, you can re-write the above as an equivalent function call:

```js
print(
  ["To do:\n\n", "\n\nCurrent progress is: ", "\n"],
  todos,
  progress
);
```

##### Using Special Characters In Strings

In addition to ordinary characters, you can also include special characters in strings:

```js
"one line \n another line";
/*
"one line 
 another line"
*/
```

The following table lists the special characters that you can use in JavaScript strings:

<details>
  <summary>Show/Hide Special Characters In Strings</summary>
  <table>
    <caption>Special Characters In Strings</caption>
    <thead>
      <tr>
        <th>Character</th>
        <th>Meaning</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><code>\0</code></td>
        <td>Null Byte</td>
      </tr>
      <tr>
        <td><code>\b</code></td>
        <td>Backspace</td>
      </tr>
      <tr>
        <td><code>\f</code></td>
        <td>Form Feed</td>
      </tr>
      <tr>
        <td><code>\n</code></td>
        <td>New Line</td>
      </tr>
      <tr>
        <td><code>\r</code></td>
        <td>Carriage Return</td>
      </tr>
      <tr>
        <td><code>\t</code></td>
        <td>Tab</td>
      </tr>
      <tr>
        <td><code>\v</code></td>
        <td>Vertical tab</td>
      </tr>
      <tr>
        <td><code>\'</code></td>
        <td>Apostrophe or single quote</td>
      </tr>
      <tr>
        <td><code>\"</code></td>
        <td>Double quote</td>
      </tr>
      <tr>
        <td><code>\\</code></td>
        <td>Backslash character</td>
      </tr>
      <tr>
        <td><code>\XXX</code></td>
        <td>The character with the Latin-1 encoding specified by up to three octal digits <code>XXX</code> between <code>0</code> and <code>377</code>. For example, <code>\251</code> is the octal sequence for the copyright symbol.</td>
      </tr>
      <tr>
        <td><code>\xXX</code></td>
        <td>The character with the Latin-1 encoding specified by the two hexadecimal digits <code>XX</code> between <code>00</code> and <code>FF</code>. For example, <code>\xA9</code> is the hexadecimal sequence for the copyright symbol.</td>
      </tr>
      <tr>
        <td><code>\uXXXX</code></td>
        <td>The Unicode character specified by the four hexadecimal digits <code>XXXX</code>. For example, <code>\u00A9</code> is the Unicode sequence for the copyright symbol. See <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#string_literals">Unicode escape sequences</a>.</td>
      </tr>
      <tr>
        <td><code>\u{XXXXX}</code></td>
        <td>Unicode code point escapes. For example, <code>\u{2F804}</code> is the same as the Unicode escapes <code>\uD87E\uDC04</code>.</td>
      </tr>
    </tbody>
  </table>
</details>

##### Escaping Characters

You can insert a quotation mark inside a string by preceding it with a backslash. This is known as *escaping* the quotation mark:

```js
const quote = "He read \"Delay, Deny, Defend\" by Jay M. Feinman.";
console.log(quote);
// He read "Delay, Deny, Defend" by Jay M. Feinman.
```

To include a literal backslash inside a string, you must escape the backslash character:

```js
const home = "C:\\temp";
```

You can also escape line breaks by preceding them with backslash. The backslash and line break are both removed from the value of the string:

```js
const str =
"Radicalized, \
by Cory Doctorow, \
was published \
on March 19, 2019.";
console.log(str);
// Radicalized, by Cory Doctorow, was published on March 19, 2019.
```

## Control Flow and Error Handling

Control flow statements can be used to incorporate a great deal of interactivity in your application. The semicolon (`;`) character is used to separate statements in JavaScript code. Any JavaScript expression is also a statement.

### Block Statement

The most basic statement is a *block statement*, which is used to group statements. The block is delimited by a pair of curly braces:

```js
{
  statement1;
  statement2;
  // ...
  statementN;
}
```

Block statements are commonly used with control flow statements (`if`, `for`, `while`):

```js
while (x < 10) {
  x++;
}
```

`var`-declared variables are not block-scoped, but are scoped to the containing function or script, and the effects of setting them persist beyond the block itself. 

```js
var x = 1;
{
  var x = 2;
}
console.log(x);
// 2
```

### Conditional Statements

A conditional statement is a set of commands that executes if a specified condition is true. JavaScript supports two conditional statements:

- `if...else`
- `switch`

#### `if...else` Statement

Use the `if` statement to execute a statement if a logical condition is `true`. Use the optional `else` clause to execute a statement if the condition is `false`. 

```js
if (condition) {
  statement1;
} else {
  statement2;
}
```

The condition can be [any expression that evaluates to `true` or `false`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean#description).

You can also compound the statements using else if to have multiple conditions tested in sequence:

```js
if (condition1) {
  statement1;
} else if (condition2) {
  statement2;
} else if (conditionN) {
  statementN;
} else {
  statementLast;
}
```

##### Best Practice

In general, it is good practice to always use block statements, especially when nesting `if` statements:

```js
if (condition) {
  // Statements for when condition is true
  // ...
} else {
  // Statements for when condition is false
  // ...
}
```

In general it is good practice to not have an `if...else` with an assignment like `x = y` as a condition:

```js
// Do not do this
if (x = y) {
  // Statements here
}
```

##### Falsy Values

The following values evaluate to `false` (also known as [Falsy](https://developer.mozilla.org/en-US/docs/Glossary/Falsy) values):

- `false`
- `undefined`
- `null`
- `0`
- `NaN`
- An empty string (`""`)

All other values (including all objects) evaluate to `true` when passed to a conditional statement.

Do not confuse the primitive boolean values true and false with the true and false values of the Boolean object:

```js
const b = new Boolean(false);

if (b) {
  // This condition evaluates to true.
}
if (b == true) {
  // This condition evaluates to false.
}
```

#### `switch` Statement

A `switch` statement allows a program to evaluate an expression and attempt to match the expression's value to a `case` label. If a match is found, the program executes the associated statement.

```js
switch (expression) {
  case label1:
    statements1;
    break;
  case label2:
    statements2;
    break;
  default:
    statementsDefault;
}
```

JavaScript evaluates the above `switch` statement as follows:

- The program first looks for a `case` clause with a label matching the value of expression and then transfers control to that clause, executing the associated statements.
- If no matching label is found, the program looks for the optional `default` clause:
    - If a `default` clause is found, the program transfers control to that clause, executing the associated statements.
    - If no `default` clause is found, the program resumes execution at the statement following the end of `switch`.

By convention, the `default` clause is written as the last clause, but it does not need to be so.

##### `break` Statements

The optional `break` statement associated with each `case` clause ensures that the program breaks out of `switch` once the matched statement is executed, and then continues execution at the statement following `switch`. If `break` is omitted, the program continues execution inside the switch statement and will execute statements under the next `case`, and so on.

```js
const fruitType = "Plantains";

switch (fruitType) {
  case "Oranges":
    console.log("Oranges are $0.59 a pound.");
    break;
  case "Apples":
    console.log("Apples are $0.32 a pound.");
    break;
  case "Plantains":
    console.log("Plantains are $0.48 a pound.");
    break;
  case "Mangoes":
    console.log("Mangoes are $0.56 a pound.");
    break;
  default:
    console.log(`Sorry, we are out of ${fruitType}.`);
}
console.log("Is there anything else you would like?");
```

### Exception Handling Statements

You can throw exceptions using the [`throw` statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#throw_statement) and handle them using the [`try...catch` statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#try...catch_statement).

#### Exception Types

Basically, any object can be thrown in JavaScript. However, not all thrown objects are created equal. It is common to throw numbers or strings as errors, but it is frequently more effective to use one of the exception types specifically created for this purpose:

- [ECMASript Exceptions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error#error_types)
- [`DOMException`](https://developer.mozilla.org/en-US/docs/Web/API/DOMException)

#### `throw` Statement

Use the `throw` statement to throw an exception. A `throw` statement specifies the value to be thrown:

```js
throw expression;
```

You may throw any expression, not just expressions of a specific type. The following code throws several exceptions of varying types:

```js
throw "Error8";  // String type
throw 28;  // Number type
throw true;  // Boolean type
throw {
  toString() {
    return "I am an object.";
  }
};
```

#### `try...catch` Statement

The `try...catch` statement marks a block of statements to try, and specifies one or more responses should an exception be thrown. If an exception is thrown, the `try...catch` statement catches it.

The `try...catch` statement consists of a `try` block, which contains one or more statements, and a `catch` block, containing statements that specify what to do if an exception is thrown in the `try` block.

If any statement within the `try` block (or in a function called from within the `try` block) throws an exception, control immediately shifts to the `catch` block. If no exception is thrown in the `try` block, the `catch` block is skipped. The `finally` block executes after the `try` and `catch` blocks execute, but before the statements following the `try...catch` statement.

```js
function getMonthName(mo) {
  // Adjust month number for array index, so that 0 = Jan, 11 = Dec
  mo--;

  const months = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
  ];

  if (months[mo]) {
    return months[mo];
  } else {
    throw new Error("InvalidMonthNo");
  }
}


try {
  monthName = getMonthName(exMonth);
} catch (exc) {
  monthName = "unknown";
   // Pass exception object to error handler (i.e., your own function)
  logErrors(exc);
}
```

##### The `catch` Block

You can use a `catch` block to handle all exceptions that may be generated in a `try` block:

```js
catch (exception) {
  statements
}
```

The `catch` block specifies an identifier (`exception` in the preceding syntax) that holds the value specified by the `throw` statement. You can use this identifier to get information about the exception that was thrown.

JavaScript creates this identifier when the `catch` block is entered. The identifier lasts **only for the duration of the `catch` block**. Once the `catch` block finishes executing, the identifier no longer exists.

When logging errors to the console inside a `catch` block, using `console.error()` rather than `console.log()` is advised for debugging. It formats the message as an error and adds it to the list of error messages generated by the page.

##### The `finally` Block

The `finally` block contains statements to be executed *after* the `try` and `catch` blocks execute. Additionally, the `finally` block executes *before* the code that follows the `try...catch...finally` statement.

The `finally` block will execute whether or not an exception is thrown. However, if an exception is thrown, the statements in the finally block execute even if no `catch` block handles the exception that was thrown.

You can use the `finally` block to make your script fail gracefully when an exception occurs. For example, you may need to release a resource that your script has tied up.

```js
try {
  openExFile();
  writeExFile(exData);  // This may throw an error
} catch (exc) {
  handleError(exc);  // If an error occurred, handle it
} finally {
  closeExFile();  // Always close the resource
}
```

If the `finally` block returns a value, this value becomes the return value of the entire `try...catch...finally` production, regardless of any `return` statements in the `try` and `catch` blocks:

```js
function f() {
  try {
    console.log(0);
    throw "bogus";
  } catch (exc) {
    console.log(1);
    // This return statement is suspended until finally block has completed
    return true;
    console.log(2);  // Not reachable
  } finally {
    console.log(3);
    return false;  // Overwrites the previous return
    console.log(4);  // Not reachable
  }
  // `return false` is executed now
  console.log(5);  // Not reachable
}


console.log(f());
/*
0
1
3
false
*/
```

Overwriting of return values by the `finally` block also applies to exceptions thrown or re-thrown inside of the `catch` block:

```js
function f() {
  try {
    throw "bogus";
  } catch (exc) {
    console.log('caught inner "bogus"');
    // This throw statement is suspended until finally block has completed
    throw exc;
  } finally {
    return false;  // Overwrites the previous throw
  }
  // `return false` is executed now
}


try {
  console.log(f());
} catch (exc) {
  /* This is never reached. While f() executes, the finally block returns
     false, which overwrites the throw inside the above catch. */
  console.log('caught outer "bogus"');
}
/*
caught inner "bogus"
false
*/
```

##### Nesting `try...catch` Statements

You can nest one or more `try...catch` statements.

If an inner `try` block does not have a corresponding `catch` block:

1. It *must* contain a `finally` block.
2. The enclosing `try...catch` statement's `catch` block is checked for a match.

For more information, refer to [nested try-blocks](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch#nested_try_blocks).

#### Utilizing Error Objects

Depending on the type of error, you may be able to use the `name` and `message` properties to get a more refined message.

The `name` property provides the general class of `Error` (e.g., `DOMException` or `Error`), while `message` generally provides a more succinct message than one would get by converting the error object to a string.

If you are throwing your own exceptions, in order to take advantage of these properties (e.g., if your `catch` block does not discriminate between your own exceptions and system exceptions), you can use the `Error` constructor:

```js
function doSomethingErrorProne() {
  if (exCodeMakesAMistake()) {
    throw new Error("The message");
  } else {
    doSomethingToGetAJavaScriptError();
  }
}


try {
  doSomethingErrorProne();
} catch (exc) {
  // Now, we actually use console.error()
  console.error(exc.name);
  // Error
  console.error(exc.message);
  // "The message", or a JavaScript error message
}
```

## Loops and Iteration

Loops offer a quick and easy way to do something repeatedly. There are many different kinds of loops, but they all essentially do the same thing, they repeat an action some number of times.

The various loop mechanisms offer different ways to determine the start and end points of the loop. There are various situations that are more easily served by one type of loop over the others. 

### `for` Statement

A [`for`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for) loop repeats until a specified condition evaluates to false. The JavaScript `for` loop is similar to the Java and C `for` loop.

A `for` statement looks as follows:

```js
for (initialization; condition; afterthought) {
  statements
}
```

When a `for` loop executes, the following occurs:

1. The initializing expression `initialization`, if any, is executed. Usually, this expression initializes one or more loop counters, but the syntax allows an expression of any degree of complexity. This expression can also declare variables.
2. The `condition` expression is evaluated. If the value of `condition` is true, the loop statements execute. Otherwise, the `for` loop terminates. If the `condition` expression is entirely omitted, the condition is assumed to be `true`.
3. The `statements` execute.
4. If present, the update expression `afterthought` is executed.
5. Control returns to Step 2.

```
<form name="selectForm">
  <label for="musicTypes">Choose some music types, then click the button below:</label>
  <select id="musicTypes" name="musicTypes" multiple>
    <option selected>R&B</option>
    <option>Jazz</option>
    <option>Blues</option>
    <option>New Age</option>
    <option>Classical</option>
    <option>Opera</option>
  </select>
  <button id="btn" type="button">How many are selected?</button>
</form>
```

```js
function countSelected(selectObject) {
  let numberSelected = 0;

  for (let i = 0; i < selectObject.options.length; i++) {
    if (selectObject.options[i].selected) {
      numberSelected++;
    }
  }

  return numberSelected;
}


const btn = document.getElementById("btn");

btn.addEventListener(
  "click",
  function () {
    const musicTypes = document.selectForm.musicTypes;
    console.log(`You have selected ${countSelected(musicTypes)} option(s).`);
  }
);
```

### `do...while` Statement

The [`do...while`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/do...while) statement repeats until a specified condition evaluates to `false`.

A `do...while` statement looks as follows:

```js
do {
  statements
}
while (condition);
```

`statements` are always executed once before the condition is checked.

If `condition` is `true`, the `statements` execute again. At the end of every execution, the condition is checked. When the condition is `false`, execution stops, and control passes to the statement following `do...while`.

```js
let i = 0;
do {
  i += 1;
  console.log(i);
} while (i < 5);
/*
1
2
3
4
5
*/
```

### `while` Statement

A `while` statement executes its statements as long as a specified condition evaluates to `true`:

```js
while (condition) {
  statements
}
```

If the condition becomes `false`, `statements` within the loop stops executing and control passes to the statement following the loop.

The condition test occurs *before* `statements` in the loop are executed. If the condition returns `true`, `statements` are executed and the condition is tested again. If the condition returns `false`, execution stops, and control is passed to the statement following `while`.

```js
let [n, x] = [0, 0];

while (n < 3) {
  n++;
  x += n;
}

console.log(n);
// 3
console.log(x);
// 6
```

Avoid infinite loops. Make sure the condition in a loop eventually becomes `false`. Otherwise, the loop will never terminate:

```js
// Infinite loops are bad
while (true) {
  console.log("Hello, world!");
}
```

### `label` Statement

A [`label`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/label) provides a statement with an identifier that lets you refer to it elsewhere in your program. For example, you can use a label to identify a loop, and then use the `break` or `continue` statements to indicate whether a program should interrupt the loop or continue its execution.

```js
label:
  statement
```

The value of `label` may be any JavaScript identifier that is not a reserved word. The `statement` that you identify with a label may be any statement.

### `break` Statement

Use the `break` statement to terminate a loop, `switch`, or in conjunction with a `label` statement.

- When you use `break` without a label, it terminates the innermost enclosing `while`, `do-while`, `for`, or `switch` immediately and transfers control to the following statement.

        const [a, theValue] = ["xyz", "y"];

        for (let i = 0; i < a.length; i++) {
          if (a[i] === theValue) {
            console.log(`i: ${i}, theValue: ${theValue}`);
            break;
          }
        }
        // i: 1, theValue: y

- When you use `break` with a label, it terminates the specified labeled statement.

        let [x, z] = [0, 0];

        labelCancelLoops: while (true) {
          console.log("Outer loops:", x);
          x += 1;
          z = 1;

          while (true) {
            console.log("Inner loops:", z);
            z += 1;
            
            if (z === 10 && x === 10) {
              break labelCancelLoops;
            } else if (z === 10) {
              break;
            }
          }
        }

### `continue` Statement

The `continue` statement can be used to restart a `while`, `do-while`, `for`, or `label` statement.

- When you use `continue` without a label, it terminates the current iteration of the innermost enclosing `while`, `do-while`, or `for` statement and continues execution of the loop with the next iteration.

    In contrast to the `break` statement, `continue` does not terminate the execution of the loop entirely. In a `while` loop, it jumps back to the condition. In a `for` loop, it jumps to the `increment-expression`.

        let [i, n] = [0, 0];

        while (i < 5) {
          i++;

          if (i === 3) {
            continue;
          }

          n += i;

          console.log(n);
        }

- When you use `continue` with a `label`, it applies to the looping statement identified with that `label`.

        let [i, j] = [0, 10];

        checkIandJ: while (i < 4) {
          console.log(i);
          i += 1;

          checkJ: while (j > 4) {
            console.log(j);
            j -= 1;

            if (j % 2 === 0) {
              continue checkJ;
            }

            console.log(j, "is odd.");
          }

          console.log("i =", i);
          console.log("j =", j);
        }

### `for...in` Statement

The `for...in` statement iterates a specified variable over all the enumerable properties of an object. For each distinct property, JavaScript executes the specified statements.

```js
for (variable in object) {
  statements
}
```

#### Arrays

Although it may be tempting to use the `for...in` statement as a way to iterate over `Array` elements, keep in mind, it will return **the name of your user-defined properties in addition to the numeric indexes**.

Therefore, it is better to use a traditional `for` loop with a numeric index when iterating over arrays.

### `for...of` Statement

The [`for...of`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of) statement creates a loop Iterating over [iterable objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols) (including [`Array`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array), [`Map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map), [`Set`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set), [`arguments`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments) object and so on), invoking a custom iteration hook with statements to be executed for the value of each distinct property.

```js
for (variable of iterable) {
  statements
}
```

The following example shows the difference between a `for...of` loop and a `for...in` loop. While `for...in` iterates over property names, `for...of` iterates over property values:

```js
const arr = [3, 5, 7];
arr.foo = "hello";

for (const i in arr) {
  console.log(i);
}
/*
0
1
2
foo
*/

for (const i of arr) {
  console.log(i);
}
/*
3
5
7
*/
```

The ``for...of` and `for...in` statements can also be used with [destructuring](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment). For example, you can simultaneously loop over the keys and values of an object using [`Object.entries()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/entries):

```js
const obj = {foo: 1, bar: 2};

for (const [key, val] of Object.entries(obj)) {
  console.log(key, val);
}
/*
foo 1
bar 2
*/
```

## Functions

Functions are one of the fundamental building blocks in JavaScript. A function in JavaScript is similar to a procedure, i.e., a set of statements that performs a task or calculates a value.

However, for a procedure to qualify as a function, it should take some input and return an output where there is some obvious relationship between the input and the output. To use a function, you must define it somewhere in the scope from which you wish to call it.

### Defining Functions

#### Function Definitions

A *function definition* (also called a function declaration or function statement) consists of the [`function`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function) keyword, followed by:

- The name of the function.
- A list of parameters to the function, enclosed in parentheses and separated by commas.
- The JavaScript statements that define the function, enclosed in curly braces, `{ /* … */ }`.

```js
function square(number) {
  return number * number;
}
```

Essentially, parameters are passed to functions by *value*. So, if the code within the body of a function assigns a completely new value to a parameter that was passed to the function, **the change is not reflected globally or in the code that called that function**.

When you pass an object as a parameter, if the function changes the object's properties, that change is visible outside the function:

```js
function exFunc(exObject) {
  exObject.make = "Toyota";
}


const exCar = {
  make: "Kia",
  model: "Soul",
  year: 2016,
};

console.log(exCar.make);
// "Kia"
exFunc(exCar);
console.log(exCar.make);
// "Toyota"
```

When you pass an array as a parameter, if the function changes any of the array's values, that change is visible outside the function:

```js
function exFunc(exArr) {
  exArr[0] = 30;
}


const arr = [45];

console.log(arr[0]);
// 45
exFunc(arr);
console.log(arr[0]);
// 30
```

#### Function Expressions

Functions can also be created by a [function expression](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/function). Such a function can be *anonymous*, i.e., it does not have to have a name. For example, the function `square` could have been defined as:

```js
const square = function (number) {
  return number * number;
};

console.log(square(4));
// 16
```

However, a name *can* be provided with a function expression. Providing a name allows the function to refer to itself, and also makes it easier to identify the function in a debugger's stack traces:

```js
const factorial = function fac(n) {
  return n < 2 ? 1 : n * fac(n - 1);
};

console.log(factorial(3));
// 6
```

Function expressions are convenient when passing a function as an argument to another function. The following example creates a `map()` function that should receive a function as the first argument and an array as the second argument:

```js
function map(f, a) {
  const result = new Array(a.length);

  for (let i = 0; i < a.length; i++) {
    result[i] = f(a[i]);
  }

  return result;
}
```

In the following code, the function receives a function defined by a function expression and executes it for every element of the array received as a second argument:

```js
function map(f, a) {
  const result = new Array(a.length);

  for (let i = 0; i < a.length; i++) {
    result[i] = f(a[i]);
  }

  return result;
}


const cube = function (x) {
  return x * x * x;
};

const numbers = [0, 1, 2, 5, 10];
console.log(map(cube, numbers));
// Array(5) [ 0, 1, 8, 125, 1000 ]
```

A function can be defined based on a condition:

```js
let exFunc;

if (num === 0) {
  exFunc = function (exObject) {
    exObject.make = "Toyota";
  };
}
```

You can also use the [`Function`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function) constructor to create functions from a string at runtime, much like [`eval()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/eval).

### Calling Functions

*Defining* a function does not *execute* it. Defining it names the function and specifies what to do when the function is called. *Calling* the function actually performs the specified actions with the indicated parameters.

Functions must be in *scope* when they are called, but the function declaration can be [hoisted](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions#function_hoisting) (appear below the call in the code). The scope of a function declaration is the function in which it is declared (or the entire program, if it is declared at the top level).

A function can call itself. For example, here is a function that computes factorials recursively:

```js
function factorial(n) {
  if (n === 0 || n === 1) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}
```

There are other ways to call functions. Often, there are cases where a function needs to be dynamically called, or the number of arguments to a function vary, or in which the context of the function call needs to be set to a specific object determined at runtime.

*Functions are themselves objects*. In turn, these objects have methods.

#### Function Hoisting

Consider the following:

```js
console.log(square(5));
// 25


function square(n) {
  return n * n;
}
```

This code runs without any error, despite the `square()` function being called before it is declared. This is because the JavaScript interpreter hoists the entire function declaration to the top of the current scope. So, the code above is equivalent to:

```js
// All function declarations are effectively at the top of the scope
function square(n) {
  return n * n;
}


console.log(square(5));
// 25
```

Function hoisting only works with function *declarations*, not with function *expressions*.

### Function Scope

Variables defined inside a function cannot be accessed from anywhere outside the function, because the variable is defined only in the scope of the function. However, a function can access all variables and functions defined inside the scope in which it is defined.

In other words, a function defined in the global scope can access all variables defined in the global scope. A function defined inside another function can also access all variables defined in its parent function, and any other variables to which the parent function has access.

```js
// The following variables are defined in the global scope
const num1 = 20;
const num2 = 3;
const name = "Lacey";


// This function is defined in the global scope
function multiply() {
  return num1 * num2;
}


// A nested function example
function getScore() {
  const num1 = 2;
  const num2 = 3;

  function add() {
    return `${name} scored ${num1 + num2}`;
  }

  return add();
}


console.log(multiply());
// 60
console.log(getScore());
// "Lacey scored 5"
```

### Scope and the Function Stack

#### Recursion

A function can refer to and call itself. There are three ways for a function to refer to itself:

- The function's name
- [`arguments.callee`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments/callee)
- An in-scope variable that refers to the function

Consider the following function definition:

```js
const foo = function bar() {
  // tatements go here
};
```

Within the function body, the following are all equivalent:

- `bar()`
- `arguments.callee()`
- `foo()`

A function that calls itself is called a *recursive function*. In some ways, recursion is analogous to a loop. Both execute the same code multiple times, and both require a condition to avoid an infinite loop or infinite recursion, respectively.

Consider the following loop:

```js
let x = 0;

// `x < 10` is the loop condition
while (x < 10) {
  x++;
}
```

It can be converted into a recursive function declaration, followed by a call to that function:

```js
function loop(x) {
  // `x >= 10` is the exit condition (equivalent to `!(x < 10)`)
  if (x >= 10) {
    return;
  }
  loop(x + 1);  // The recursive call
}


loop(0);
```

Some algorithms cannot be simple iterative loops. For example, getting all the nodes of a tree structure (such as the [DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model)) is easier via recursion:

```js
function walkTree(node) {
  if (node === null) {
    return;
  }

  // Do something with node

  for (let i = 0; i < node.childNodes.length; i++) {
    walkTree(node.childNodes[i]);
  }
}
```

Compared to the function `loop`, each recursive call itself makes many recursive calls here.

It is possible to convert any recursive algorithm to a non-recursive one, but the logic is often much more complex, and doing so requires the use of a stack.

Recursion itself uses a stack, i.e., the function stack. The stack-like behavior can be seen in the following example:

```js
function foo(i) {
  if (i < 0) {
    return;
  }
  console.log(`begin: ${i}`);
  foo(i - 1);
  console.log(`end: ${i}`);
}


foo(3);
/*
begin: 3
begin: 2
begin: 1
begin: 0
end: 0
end: 1
end: 2
end: 3
*/
```

#### Nested Functions and Closures

You may nest a function within another function. The nested (inner) function is private to its containing (outer) function.

It also forms a *closure*. A closure is an expression (most commonly, a function) that can have free variables together with an environment that binds those variables, which *closes* the expression.

Since a nested function is a closure, this means that a nested function can *inherit* the arguments and variables of its containing function. In other words, the inner function contains the scope of the outer function.

To summarize:

- The inner function can be accessed only from statements in the outer function.
- The inner function forms a closure. The inner function can use the arguments and variables of the outer function, while the outer function cannot use the arguments and variables of the inner function.

```js
function addSquares(a, b) {

  function square(x) {
    return x * x;
  }

  return square(a) + square(b);
}


console.log(addSquares(2, 3));
// 13
console.log(addSquares(3, 4));
// 25
console.log(addSquares(4, 5));
// 41
```

Since the inner function forms a closure, you can call the outer function and specify arguments for both the outer and inner function:

```js
function outside(x) {

  function inside(y) {
    return x + y;
  }

  return inside;
}


const fnInside = outside(3);
console.log(fnInside(5));
// 8
console.log(outside(3)(5));
// 8
```

#### Preservation of Variables

Notice how `x` is preserved when `inside` is returned. A closure must preserve the arguments and variables in all scopes it references. Since each call provides potentially different arguments, a new closure is created for each call to `outside`. The memory can be freed only when the returned `inside` is no longer accessible.

This is not different from storing references in other objects, but is often less obvious because one does not directly set the references and cannot inspect them.

#### Multiply-Nested Functions

Functions can be multiply-nested. For example:

- A function (`a`) contains a function (`b`), which itself contains a function (`c`).
- Both functions `b` and `c` form closures here. So, `b` can access `a`, and `c` can access `b`.
- In addition, since `c` can access `b` which can access `a`, `c` can also access `a`.

Thus, the closures can contain multiple scopes; they recursively contain the scope of the functions containing it. This is called *scope chaining*.

```js
function a(x) {

  function b(y) {

    function c(z) {
      console.log(x + y + z);
    }

    c(3);
  }

  b(2);
}


a(1);
// Logs 6 (which is 1 + 2 + 3)
```

Above, `c` accesses `b`'s `y` and `a`'s `x`. This can be done because:

- `b` forms a closure including `a` (i.e., `b` can access `a`'s arguments and variables).
- `c` forms a closure including `b`.
- `c`'s closure includes `b` and `b`'s closure includes `a`, then `c`'s closure also includes `a`. This means `c` can access *both* `b` *and* `a`'s arguments and variables. That is, `c` chains the scopes of `b` and `a`, in that order.

The reverse, however, is not true. `a` cannot access `c` because `a` cannot access any argument or variable of `b`, which `c` is a variable of. Thus, `c` remains private to only `b`.

#### Name Conflicts

When two arguments or variables in the scopes of a closure have the same name, there is a *name conflict*. More nested scopes take precedence. So, the innermost scope takes the highest precedence, while the outermost scope takes the lowest.

This is the *scope chain*. The first on the chain is the innermost scope, and the last is the outermost scope.

```js
function outside() {
  const x = 5;

  function inside(x) {
    return x * 2;
  }

  return inside;
}

console.log(outside()(10));
// 20 (instead of 10)
```

### Closures

Closures are one of the most powerful features of JavaScript. JavaScript allows for the nesting of functions and grants the inner function full access to all the variables and functions defined inside the outer function (and all other variables and functions that the outer function has access to).

However, the outer function does *not* have access to the variables and functions defined inside the inner function. This provides a sort of encapsulation for the variables of the inner function.

Also, since the inner function has access to the scope of the outer function, the variables and functions defined in the outer function will live longer than the duration of the outer function execution, if the inner function manages to survive beyond the life of the outer function. A closure is created when the inner function is somehow made available to any scope outside the outer function.

```js
// The outer function defines a variable called handle
const person = function (handle) {
  const getHandle = function () {
    /* The inner function has access to the handle variable
       of the outer function */
    return handle;
  };
  // Return the inner function, thereby exposing it to outer scopes
  return getHandle;
};
const exPerson = person("LisasDad1990");

console.log(exPerson());
// LisasDad1990
```

It can be much more complex than the code above. An object containing methods for manipulating the inner variables of the outer function can be returned. In this case, a variable of the outer function would be accessible to the inner functions, and there would be no other way to access the inner variables except through the inner functions.

The inner variables of the inner functions would act as safe stores for the outer arguments and variables. They would hold *persistent* and *encapsulated* data for the inner functions to work with. The functions would not even have to be assigned to a variable or have a name.

```js
const getCode = (function () {
  // A code we do not want outsiders to be able to modify
  const apiCode = "0]Eal(eh&2"; 

  return function () {
    return apiCode;
  };
})();

console.log(getCode());
// "0]Eal(eh&2"
```

There are a number of pitfalls to watch out for when using closures. For example, if an enclosed function defines a variable with the same name as a variable in the outer scope, then there is no way to refer to the variable in the outer scope again. The inner scope variable *overrides* the outer one, until the program exits the inner scope. It can be thought of as a name conflict.

```js
const createPerson = function (handle) {
  // The outer function defines a variable called handle
  return {
    setHandle(handle) {
      /* The enclosed function also defines a variable called handle.
         How do we access the handle defined by the outer function? */
      // 
      handle = handle;
    }
  };
};
```

### Using the `arguments` Object

The arguments of a function are maintained in an array-like object. Within a function, you can address the arguments passed to it as follows:

```js
arguments[i];
```

`i` is the ordinal number of the argument, starting at `0`. So, the first argument passed to a function would be `arguments[0]`. The total number of arguments is indicated by `arguments.length`.

Using the `arguments` object, you can call a function with more arguments than it is formally declared to accept. Often, this is useful if you do not know in advance how many arguments will be passed to the function.

You can use `arguments.length` to determine the number of arguments actually passed to the function, and then access each argument using the `arguments` object.

```js
function exConcat(separator) {
  let result = "";

  for (let i = 1; i < arguments.length; i++) {
    result += arguments[i] + separator;
  }

  return result;
}


console.log(exConcat(", ", "Logan Lents", "Patricia Lents"));
// Logan Lents, Patricia Lents,
```

Note that the `arguments` variable is *array-like*, but not an array. It is array-like in that it has a numbered index and a `length` property. However, it does not possess all of the array-manipulation methods.

### Function Parameters

There are two special kinds of parameter syntax:

- Default parameters
- Rest parameters

#### Default Parameters

In JavaScript, parameters of functions default to `undefined`. However, in some situations it might be useful to set a different default value. This is what default parameters do.

```js
function multiply(a, b) {
  b = typeof b !== "undefined" ? b : 1;
  return a * b;
}


console.log(multiply(5));
// 5
```

For more details, refer to [default parameters](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters).

#### Rest Parameters

The [rest parameter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters) syntax allows us to represent an indefinite number of arguments as an array.

```js
function multiply(multiplier, ...theArgs) {
  return theArgs.map(function (x) {return multiplier * x});
}


const arr = multiply(2, 1, 2, 3);
console.log(arr);
// Array(3) [ 2, 4, 6 ]
```

### Arrow Functions

An [arrow function expression](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions) has a shorter syntax compared to function expressions and does not have its own [`this`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this), [`arguments`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments), [`super`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/super), or [`new.target`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/new.target). Arrow functions are always anonymous.

Two factors influenced the introduction of arrow functions:

- Shorter functions
- Non-binding of `this`

#### Shorter Functions

In some functional patterns, shorter functions may be welcome:

```js
const a = ["Hydrogen", "Helium", "Lithium", "Beryllium"];

const a2 = a.map(
  function (s) {
    return s.length;
  }
);
console.log(a2);
// Array(4) [ 8, 6, 7, 9 ]

const a3 = a.map((s) => s.length);
console.log(a3);
// Array(4) [ 8, 6, 7, 9 ]
```

#### No Separate `this`

Until arrow functions, every new function defined its own `this` value (i.e., a new object in the case of a constructor, `undefined` in strict mode function calls, the base object if the function is called as an *object method*). This proved to be less than ideal with an object-oriented style of programming.

In ECMAScript 3/5, this issue was fixed by assigning the value in this to a variable that could be closed over.

Alternatively, a [bound function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind) could be created so that the proper `this` value would be passed to the `growUp()` function.

An arrow function does not have its own `this`. The `this` value of the enclosing execution context is used. Thus, in the following code, the `this` within the function that is passed to `setInterval()` has the same value as `this` in the enclosing function:

```js
function Person() {
  this.age = 0;

  setInterval(
    function () {
      this.age++;  // this properly refers to the person instance object
    },
    1000
  );
}


const p = new Person();
```

## Expressions and Operators

An *expression* is a valid unit of code that resolves to a value. There are two types of expressions:

1. Those that have side effects (e.g., assigning values)
2. Those that purely *evaluate*

The expression `x = 7` is an example of the first type. This expression uses the `=` operator to assign the value seven to the variable `x`. The expression itself evaluates to `7`.

The expression `3 + 4` is an example of the second type. This expression uses the `+` operator to add `3` and `4` together and produces a value, `7`. However, if it's not eventually part of a bigger construct, its result will be immediately discarded.

All complex expressions are joined by *operators*. These operators join operands either formed by higher-precedence operators or one of the [basic expressions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_operators#basic_expressions).

The precedence of operators determines the order they are applied when evaluating an expression. You can override operator precedence by using parentheses (which creates a [grouped expression](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_operators#grouping_operator)). The [Operator Precedence Reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_precedence#table) page has detailed information on operator precedence, and its various caveats.

JavaScript has both *binary* and *unary* operators, and one special ternary operator, the conditional operator.

A binary operator requires two operands, one before the operator and one after the operator:

```text
operand1 operator operand2
```

This form is called an *infix* binary operator, because the operator is placed between two operands. All binary operators in JavaScript are infix.

A unary operator requires a single operand, either before or after the operator:

```text
operator operand
operand operator
```

The `operator operand` form is called a *prefix* unary operator, and the `operand operator` form is called a *postfix* unary operator. `++` and `--` are the only postfix operators in JavaScript. All other operators (e.g., `!`, `typeof`) are prefix.

### Assignment Operators

An assignment operator assigns a value to its left operand based on the value of its right operand. There are also compound assignment operators that are shorthand for the operations listed in the following table:

<details>
  <summary>Show/Hide Assignment Operators</summary><br />
  <table>
  <caption>Assignment Operators</caption>
    <thead>
      <tr>
        <th>Name</th>
        <th>Shorthand operator</th>
        <th>Meaning</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Assignment">Assignment</  a></td>
        <td><code>x = f()</code></td>
        <td><code>x = f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Addition_assignment">Addition assignment</a></td>
        <td><code>x += f()</code></td>
        <td><code>x = x + f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Subtraction_assignment">Subtraction assignment</a></td>
        <td><code>x -= f()</code></td>
        <td><code>x = x - f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Multiplication_assignment">Multiplication assignment</a></td>
        <td><code>x *= f()</code></td>
        <td><code>x = x * f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Division_assignment">Division assignment</a></td>
        <td><code>x /= f()</code></td>
        <td><code>x = x / f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Remainder_assignment">Remainder assignment</a></td>
        <td><code>x %= f()</code></td>
        <td><code>x = x % f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Exponentiation_assignment">Exponentiation assignment</a></td>
        <td><code>x **= f()</code></td>
        <td><code>x = x ** f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Left_shift_assignment">Left shift assignment</a></td>
        <td><code>x &lt;&lt;= f()</code></td>
        <td><code>x = x &lt;&lt; f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Right_shift_assignment">Right shift assignment</a></td>
        <td><code>x &gt;&gt;= f()</code></td>
        <td><code>x = x &gt;&gt; f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Unsigned_right_shift_assignment">Unsigned right shift assignment</a></td>
        <td><code>x &gt;&gt;&gt;= f()</code></td>
        <td><code>x = x &gt;&gt;&gt; f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_AND_assignment">Bitwise AND assignment</a></td>
        <td><code>x &amp;= f()</code></td>
        <td><code>x = x &amp; f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_XOR_assignment">Bitwise XOR assignment</a></td>
        <td><code>x ^= f()</code></td>
        <td><code>x = x ^ f()</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_OR_assignment">Bitwise OR assignment</a></td>
        <td><code>x |= f()</code></td>
        <td><code>x = x | f()</code></td>
        </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_AND_assignment">Logical AND assignment</a></td>
        <td><code>x &amp;&amp;= f()</code></td>
        <td><code>x &amp;&amp; (x = f())</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_OR_assignment">Logical OR assignment</a></td>
        <td><code>x ||= f()</code></td>
        <td><code>x || (x = f())</code></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_assignment">Nullish coalescing assignment</a></td>
        <td><code>x ??= f()</code></td>
        <td><code>x ?? (x = f())</code></td>
      </tr>
    </tbody>
  </table>
</details>

#### Assigning To Properties

If an expression evaluates to an [object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_objects), then the left-hand side of an assignment expression may make assignments to properties of that expression:

```js
const obj = {};

obj.x = 3;
console.log(obj.x);
// 3
console.log(obj);
// Object { x: 3 }

const key = "y";

obj[key] = 5;
console.log(obj[key]);
// 5
console.log(obj);
// Object { x: 3, y: 5 }
```

If an expression does not evaluate to an object, then assignments to properties of that expression do not assign:

```js
const val = 0;

val.x = 3;
console.log(val.x);
// undefined
console.log(val);
// 0
```

In [strict mode](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode#converting_mistakes_into_errors), the code above throws an error because one cannot assign properties to primitives.

It is an error to assign values to unmodifiable properties or to properties of an expression without properties (`null` or `undefined`).

#### Destructuring

For more complex assignments, the [destructuring assignment](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment) syntax is a JavaScript expression that makes it possible to extract data from arrays or objects using a syntax that mirrors the construction of array and object literals.

Without destructuring, it takes multiple statements to extract values from arrays and objects:

```js
const foo = ["one", "two", "three"];

const one = foo[0];
const two = foo[1];
const three = foo[2];
```

With destructuring, you can extract multiple values into distinct variables using a single statement:

```js
const [one, two, three] = foo;
```

#### Evaluation and Nesting

In general, assignments are used within a variable declaration or as standalone statements.

```js
/* Declares a variable x and initializes it to the result of f().
   The result of the x = f() assignment expression is discarded. */
let x = f();

x = g();  // Reassigns the variable x to the result of g().
```

However, like other expressions, assignment expressions like `x = f()` evaluate into a result value. Although this result value is usually not used, it can then be used by another expression.

Chaining assignments or nesting assignments in other expressions can result in surprising behavior. For this reason, some JavaScript style guides [discourage chaining or nesting assignments](https://github.com/airbnb/javascript/blob/master/README.md#variables--no-chain-assignment).

```js
let x;
const y = (x = f());  // Or equivalently, const y = x = f();

console.log(y);  // Logs the return value of the assignment x = f().
console.log(x = f());  // Logs the return value directly.

/* An assignment expression can be nested in any place
   where expressions are generally allowed,
   such as array literals' elements or as function calls' arguments. */
console.log([0, x = f(), 0]);
console.log(f(0, x = f(), 0));
```

`x = f()` evaluates into whatever `f()`'s result is, `x += f()` evaluates into the resulting sum `x + f()`, and `x **= f()` evaluates into the resulting power `x ** f()`.

In the case of logical assignments, `x &&= f()`, `x ||= f()`, and `x ??= f()`, the return value is that of the logical operation without the assignment, so `x && f()`, `x || f()`, and `x ?? f()`, respectively.

When chaining these expressions without parentheses or other grouping operators like array literals, the assignment expressions are *grouped right to left* (they are [right-associative](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_precedence#precedence_and_associativity)), but they are *evaluated left to right*.

Note that for all assignment operators other than `=` itself, the resulting values are always based on the operands' values before the operation.

```js
function f() {
  console.log("F!");
  return 2;
}


function g() {
  console.log("G!");
  return 3;
}


let x, y;

y = x = f();
y = [f(), x = g()];
x[f()] = g();
/*
F!
F!
G!
F!
G!
3
*/
```

#### Avoid Assignment Chains

Since, chaining/nesting assignments in other expressions can result in surprising behavior, it is discouraged. In particular, putting a variable chain in a `const`, `let`, or `var` statement often does not work. Only the outermost/leftmost variable would get declared. Other variables within the assignment chain are *not* declared by the `const`/`let`/`var` statement:

```js
const z = y = x = f();
```

This statement seemingly declares the variables `x`, `y`, and `z`. However, it only actually declares the variable `z`. `y` and `x` are either invalid references to nonexistent variables (in strict mode) or, worse, would implicitly create global variables for `x` and `y` in [sloppy mode](https://developer.mozilla.org/en-US/docs/Glossary/Sloppy_mode).

### Comparison Operators

A comparison operator compares its operands and returns a logical value based on whether the comparison is true. The operands can be numerical, string, logical, or object values.

Strings are compared based on standard lexicographical ordering, using Unicode values. In most cases, if the two operands are not of the same type, JavaScript attempts to convert them to an appropriate type for the comparison. This behavior generally results in comparing the operands numerically.

The sole exceptions to type conversion within comparisons involve the `===` and `!==` operators, which perform strict equality and inequality comparisons. These operators do not attempt to convert the operands to compatible types before checking equality.

The following table describes the comparison operators in terms of this sample code:

```js
const var1 = 3;
const var2 = 4;
```

<details>
  <summary>Show/Hide Comparison Operators</summary><br />
  <table>
    <caption>Comparison operators</caption>
    <thead>
      <tr>
        <th>Operator</th>
        <th>Description</th>
        <th>Examples returning true</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Equality">Equal</a> (<code>==</code>)</td>
        <td>Returns <code>true</code> if the operands are equal.</td>
        <td>
          <code>3 == var1</code>
          <p><code>"3" == var1</code></p>
          <code>3 == '3'</code>
        </td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Inequality">Not   equal</a> (<code>!=</code>)</td>
        <td>Returns <code>true</code> if the operands are not equal.</td>
        <td><code>var1 != 4<br>var2 != "3"</code></td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Strict_equality">Strict equal</a> (<code>===</code>)</td>
        <td>Returns <code>true</code> if the operands are equal and of the same type. See also <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is"><code>Object.is</code></a> and <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness">sameness in JS</a>.
        </td>
        <td><code>3 === var1</code></td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Strict_inequality">Strict not equal</a> (<code>!==</code>)</td>
        <td>Returns <code>true</code> if the operands are of the same type but not equal,   or are of different type.</td>
        <td><code>var1 !== "3"<br>3 !== '3'</code></td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Greater_than">Greater   than</a> (<code>&gt;</code>)</td>
        <td>Returns <code>true</code> if the left operand is greater than the right operand.</td>
        <td><code>var2 &gt; var1<br>"12" &gt; 2</code></td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Greater_than_or_equal">Greater than or equal</a> (<code>&gt;=</code>)</td>
        <td>Returns <code>true</code> if the left operand is greater than or equal to the right operand.</td>
        <td><code>var2 &gt;= var1<br>var1 &gt;= 3</code></td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Less_than">Less than</a> (<code>&lt;</code>)
        </td>
        <td>Returns <code>true</code> if the left operand is less than the right operand.</td>
        <td><code>var1 &lt; var2<br>"2" &lt; 12</code></td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Less_than_or_equal">Less than or equal</a> (<code>&lt;=</code>)
        </td>
        <td>Returns <code>true</code> if the left operand is less than or equal to the right operand.</td>
        <td><code>var1 &lt;= var2<br>var2 &lt;= 5</code></td>
      </tr>
    </tbody>
  </table>
</details>

### Arithmetic Operators

An arithmetic operator takes numerical values (either literals or variables) as their operands and returns a single numerical value. These operators work as they do in most other programming languages when used with floating point numbers (in particular, note that division by zero produces [`Infinity`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Infinity)).

In addition to the standard arithmetic operations (`+`, `-`, `*`, `/`), JavaScript provides the arithmetic operators listed in the following table:

<details>
  <summary>Show/Hide Arithmetic Operators</summary><br />
  <table>
    <caption>Arithmetic operators</caption>
    <thead>
      <tr>
        <th>Operator</th>
        <th>Description</th>
        <th>Example</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Remainder">Remainder</a> (<code>%</code>)</td>
        <td>Binary operator. Returns the integer remainder of dividing the two operands.</td>
        <td>12 % 5 returns 2.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Increment">Increment</a> (<code>++</code>)</td>
        <td>Unary operator. Adds one to its operand. If used as a prefix operator (<code>++x</code>), returns the value of its operand after adding one. If used as a postfix operator (<code>x++</code>), returns the value of its operand before adding one.</td>
        <td>If <code>x</code> is 3, then <code>++x</code> sets <code>x</code> to 4 and returns 4, whereas <code>x++</code> returns 3 and, only then, sets <code>x</code> to 4.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Decrement">Decrement</a> (<code>--</code>)</td>
        <td>Unary operator. Subtracts one from its operand. The return value is analogous to that for the increment operator.</td>
        <td>If <code>x</code> is 3, then <code>--x</code> sets <code>x</code> to 2 and returns 2, whereas <code>x--</code> returns 3 and, only then, sets <code>x</code> to 2.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Unary_negation">Unary negation</a> (<code>-</code>)</td>
        <td>Unary operator. Returns the negation of its operand.</td>
        <td>If <code>x</code> is 3, then <code>-x</code> returns -3.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Unary_plus">Unary   plus</a> (<code>+</code>)</td>
        <td>Unary operator. Attempts to <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number#number_coercion">convert the operand to a number</a>, if it is not already.</td>
        <td>
          <p><code>+"3"</code> returns <code>3</code>.</p>
          <p><code>+true</code> returns <code>1</code>.</p>
        </td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Exponentiation">Exponentiation operator</a> (<code>**</code>)</td>
        <td>Calculates the <code>base</code> to the <code>exponent</code> power, that is, <code>base^exponent</code></td>
        <td><code>2 ** 3</code> returns <code>8</code>.<br><code>10 ** -1</code>returns <code>0.1</code>.
        </td>
      </tr>
    </tbody>
  </table>
</details>

### Bitwise Operators

A bitwise operator treats their operands as a set of 32 bits (zeros and ones), rather than as decimal, hexadecimal, or octal numbers. For example, the decimal number nine has a binary representation of 1001. Bitwise operators perform their operations on such binary representations, but they return standard JavaScript numerical values.

<details>
  <summary>Show/Hide Bitwise Operators</summary><br />
  <table>
    <thead>
      <tr>
        <th>Operator</th>
        <th>Usage</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_AND">Bitwise AND</  a></td>
        <td><code>a &amp; b</code></td>
        <td>Returns a one in each bit position for which the corresponding bits of both   operands are ones.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_OR">Bitwise OR</  a></td>
        <td><code>a | b</code></td>
        <td>Returns a zero in each bit position for which the corresponding bits of both   operands are zeros.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_XOR">Bitwise XOR</  a></td>
        <td><code>a ^ b</code></td>
        <td>Returns a zero in each bit position for which the corresponding bits are the same. [Returns a one in each bit position for which the corresponding bits are   different.]</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_NOT">Bitwise NOT</  a></td>
        <td><code>~ a</code></td>
        <td>Inverts the bits of its operand.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Left_shift">Left shift</  a></td>
        <td><code>a &lt;&lt; b</code></td>
        <td>Shifts <code>a</code> in binary representation <code>b</code> bits to the left,   shifting in zeros from the right.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Right_shift">Sign-propagating right shift</a></td>
        <td><code>a &gt;&gt; b</code></td>
        <td>Shifts <code>a</code> in binary representation <code>b</code> bits to the right,   discarding bits shifted off.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Unsigned_right_shift">Zero-fill right shift</a></td>
        <td><code>a &gt;&gt;&gt; b</code></td>
        <td>Shifts <code>a</code> in binary representation <code>b</code> bits to the right,   discarding bits shifted off, and shifting in zeros from the left.</td>
      </tr>
    </tbody>
  </table>
</details>

#### Bitwise Logical Operators

Conceptually, the bitwise logical operators work as follows:

- The operands are converted to thirty-two-bit integers and expressed by a series of bits (zeros and ones). Numbers with more than 32 bits get their most significant bits discarded.

    For example, the following integer with more than 32 bits will be converted to a 32-bit integer:

        Before: 1110 0110 1111 1010 0000 0000 0000 0110 0000 0000 0001
        After:                 1010 0000 0000 0000 0110 0000 0000 0001

- Each bit in the first operand is paired with the corresponding bit in the second operand: first bit to first bit, second bit to second bit, and so on.
- The operator is applied to each pair of bits and the result is constructed bitwise.

For example, the binary representation of nine is 1001, and the binary representation of fifteen is 1111. So, when the bitwise operators are applied to these values, the results are as follows:

<table>
  <thead>
    <tr>
      <th>Expression</th>
      <th>Result</th>
      <th>Binary Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>15 &amp; 9</code></td>
      <td><code>9</code></td>
      <td><code>1111 &amp; 1001 = 1001</code></td>
    </tr>
    <tr>
      <td><code>15 | 9</code></td>
      <td><code>15</code></td>
      <td><code>1111 | 1001 = 1111</code></td>
    </tr>
    <tr>
      <td><code>15 ^ 9</code></td>
      <td><code>6</code></td>
      <td><code>1111 ^ 1001 = 0110</code></td>
    </tr>
    <tr>
      <td><code>~15</code></td>
      <td><code>-16</code></td>
      <td><code>~ 0000 0000 … 0000 1111 = 1111 1111 ... 1111 0000</code></td>
    </tr>
    <tr>
      <td><code>~9</code></td>
      <td><code>-10</code></td>
      <td><code>~ 0000 0000 … 0000 1001 = 1111 1111 ... 1111 0110</code></td>
    </tr>
  </tbody>
</table>

Note that all 32 bits are inverted using the Bitwise NOT operator, and that values with the most significant (left-most) bit set to 1 represent negative numbers (two's-complement representation). `~x` evaluates to the same value that `-x - 1` evaluates to.

#### Bitwise Shift Operators

The bitwise shift operators take two operands:

1. A quantity to be shifted
2. The number of bit positions by which the first operand is to be shifted

The direction of the shift operation is controlled by the operator used.

Shift operators convert their operands to 32-bit integers and return a result of either type [`Number`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number) or [`BigInt`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt). Specifically, if the type of the left operand is `BigInt`, they return `BigInt`. Otherwise, they return `Number`.

<details>
  <summary>Show/Hide Bitwise Shift Operators</summary><br />
  <table>
    <caption>Bitwise Shift Operators
    </caption>
    <thead>
      <tr>
        <th>Operator</th>
        <th>Description</th>
        <th>Example</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Left_shift">Left shift</a><br>(<code>&lt;&lt;</code>)</td>
        <td>This operator shifts the first operand the specified number of bits to the left. Excess bits shifted off to the left are discarded. Zero bits are shifted in from the right.
        </td>
        <td><code>9&lt;&lt;2</code> yields 36, because 1001 shifted 2 bits to the left becomes 100100, which is 36.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Right_shift">Sign-propagating right shift</a> (<code>&gt;&gt;</code>)</td>
        <td>This operator shifts the first operand the specified number of bits to the right. Excess bits shifted off to the right are discarded. Copies of the leftmost bit are shifted in from the left.</td>
        <td><code>9&gt;&gt;2</code> yields 2, because 1001 shifted 2 bits to the right becomes 10, which is 2. Likewise, <code>-9&gt;&gt;2</code> yields -3, because the sign is preserved.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Unsigned_right_shift">Zero-fill right shift</a> (<code>&gt;&gt;&gt;</code>)</td>
        <td>This operator shifts the first operand the specified number of bits to the right. Excess bits shifted off to the right are discarded. Zero bits are shifted in from the left.</td>
        <td><code>19&gt;&gt;&gt;2</code> yields 4, because 10011 shifted 2 bits to the right becomes 100, which is 4. For non-negative numbers, zero-fill right shift and sign-propagating right shift yield the same result.</td>
      </tr>
    </tbody>
  </table>
</details>

### Logical Operators

Typically, logical operators are used with Boolean (logical) values. When they are, they return a Boolean value. However, the `&&`, `||`, and `??` operators actually return the value of one of the specified operands.

So, if these operators are used with non-Boolean values, they may return a non-Boolean value. Therefore, they are more adequately called *value selection operators*.

<details>
  <summary>Show/Hide Logical Operators</summary><br />
  <table>
    <caption>Logical operators</caption>
    <thead>
      <tr>
        <th>Operator</th>
        <th>Usage</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr><td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_AND">Logical AND</a> (<code>&amp;&amp;</code>)</td>
        <td><code>expr1 &amp;&amp; expr2</code></td>
        <td>Returns <code>expr1</code> if it can be converted to <code>false</code>. Otherwise, returns <code>expr2</code>. Thus, when used with Boolean values, <code>&amp;&amp;</code> returns <code>true</code> if both operands are true; otherwise, returns <code>false</code>.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_OR">Logical OR</a> (<code>||</code>)</td>
        <td><code>expr1 || expr2</code></td>
        <td>Returns <code>expr1</code> if it can be converted to <code>true</code>. Otherwise, returns <code>expr2</code>. Thus, when used with Boolean values, <code>||</code> returns <code>true</code> if either operand is true; if both are false, returns <code>false</code>.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing">Nullish coalescing operator</a> (<code>??</code>)</td>
        <td><code>expr1 ?? expr2</code></td>
        <td>Returns <code>expr1</code> if it is neither <code>null</code> nor <code>undefined</code>; otherwise, returns <code>expr2</code>.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_NOT">Logical NOT</a> (<code>!</code>)</td>
        <td><code>!expr</code></td>
        <td>Returns <code>false</code> if its single operand can be converted to <code>true</code>; otherwise, returns <code>true</code>.</td>
      </tr>
    </tbody>
  </table>
</details>

Examples of expressions that can be converted to `false` are those that evaluate to `null`, `0`, `0n`, `NaN`, the empty string (`""`), or `undefined`.

```js
// Logical AND Examples
true && true;  // true
true && false;  // false
false && true;  // false
false && 3 === 4;  // false
"Cat" && "Dog";  // "Dog"
false && "Cat";  // false
"Cat" && false;  // false
```

```js
// Logical OR Examples
true || true;  // true
false || true;  // true
true || false;  // true
false || 3 === 4;  // false
"Cat" || "Dog";  // "Cat"
false || "Cat";  // "Cat"
"Cat" || false;  // "Cat"
```

```js
// Nullish Examples
null ?? 1;  // 1
undefined ?? 2;  // 2
false ?? 3;  // false
0 ?? 4;  // 0
```

Note how `??` works like `||`, but it only returns the second expression when the first one is *nullish*, i.e. `null` or `undefined`. `??` is a better alternative than `||` for setting defaults for values that might be `null` or `undefined`, in particular when values like `''` or `0` are valid values and the default should not apply.

```js
// Logical NOT Examples
!true;  // false
!false;  // true
!"Cat";  // false
```

#### Short-Circuit Evaluation

As logical expressions are evaluated left to right, they are tested for possible *short-circuit* evaluation using the following rules:

- `falsy && anything` is short-circuit evaluated to the falsy value.
- `truthy || anything` is short-circuit evaluated to the truthy value.
- `nonNullish ?? anything` is short-circuit evaluated to the non-nullish value.

The rules of logic guarantee that these evaluations are always correct. Note that the *anything* part of the above expressions is not evaluated, so any side effects of doing so do not take effect.

### BigInt Operators

Most operators that can be used between numbers can be used between [`BigInt`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt) values, as well.

```js
// BigInt addition
1n + 2n;  // 3n
// Division with BigInts round towards zero
1n / 2n;  // 0n
// Bitwise operations with BigInts do not truncate either side
40000000000000000n >> 2n;  // 10000000000000000n
```

One exception is [unsigned right shift (`>>>`)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Unsigned_right_shift), which is not defined for `BigInt` values. This is because a `BigInt` does not have a fixed width, so technically it does not have a *highest bit*.

```js
8n >>> 2n;
// Uncaught TypeError: can't convert BigInt to number
```

BigInts and numbers are not mutually replaceable, i.e., you cannot mix them in calculations.

```js
1n + 2
// Uncaught TypeError: can't convert BigInt to number
```

This is because `BigInt` is neither a subset nor a superset of numbers. `BigInt`s have higher precision than numbers when representing large integers, but cannot represent decimals, so implicit conversion on either side might lose precision. Use explicit conversion to signal whether you wish the operation to be a number operation or a `BigInt` one.

```js
Number(1n) + 2;  // 3
1n + BigInt(2);  // 3n
```

You can compare BigInts with numbers:

```js
1n > 2;  // false
3 > 2n;  // true
```

### String Operators

In addition to the comparison operators, which can be used on string values, the concatenation operator (`+`) concatenates two string values together, returning another string that is the union of the two operand strings.

```js
console.log("Death" + "Eater");
// DeathEater
```

The shorthand assignment operator `+=` can also be used to concatenate strings:

```js
let exString = "Somewhere along the way, ";
exString += "there have to be consequences.";
console.log(exString);
// Somewhere along the way, there have to be consequences.
```

### Conditional (Ternary) Operator

The [conditional operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_operator) is the only JavaScript operator that takes three operands. The operator can have one of two values based on a condition. 

```js
condition ? val1 : val2
```

If condition is `true`, the operator has the value of `val1`. Otherwise, it has the value of `val2`. You can use the conditional operator anywhere you would use a standard operator.

### Comma Operator

The [comma operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Comma_operator) (`,`) evaluates both of its operands and returns the value of the last operand. This operator is primarily used inside a `for` loop, to allow multiple variables to be updated each time through the loop.

```js
const x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
const a = [x, x, x, x, x];

for (let i = 0, j = 9; i <= j; i++, j--) {
  //                              ^ Legitimate use of comma operator
  console.log(`a[${i}][${j}]= ${a[i][j]}`);
}
```

It is regarded as bad style to use it elsewhere, when it is not necessary. Often, two separate statements can and should be used instead.

### Unary Operators

A unary operation is an operation with only one operand.

#### `delete`

The [`delete`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/delete) operator deletes an object's property.

```js
delete object.property;
delete object[propertyKey];
delete objectName[index];
```

Above, `object` is the name of an object, `property` is an existing property, and `propertyKey` is a string or symbol referring to an existing property.

If the `delete` operator succeeds, it removes the property from the object. Trying to access it afterwards will yield `undefined`. The `delete` operator returns `true` if the operation is possible. It returns `false` if the operation is not possible.

```js
delete Math.PI;  // Returns false (cannot delete non-configurable properties)

const exObj = {h: 4};
delete exObj.h;  // Returns true (can delete user-defined properties)
```

##### Deleting Array Elements

Since arrays are just objects, it is technically possible to delete elements from them. However, this is regarded as a bad practice. Try to avoid it.

When you delete an array property, the array length is not affected and other elements are not re-indexed. To achieve that behavior, it is much better to just overwrite the element with the value `undefined`.

To actually manipulate the array, use the various array methods such as [`splice`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice).

#### `typeof`

The [`typeof` operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof) returns a string indicating the type of the unevaluated operand. `operand` is the string, variable, keyword, or object for which the type is to be returned. The parentheses are optional.

```js
const exFun = new Function("5 + 2");
const shape = "round";
const size = 1;
const foo = ["apple", "mango", "orange"];
const today = new Date();

typeof exFun;  // "function"
typeof shape;  // "string"
typeof size;  // "number"
typeof foo;  // "object"
typeof today;  // "object"
typeof doesntExist; // "undefined"
```

For the keywords `true` and `null`, the `typeof` operator returns:

```js
typeof true;  // "boolean"
typeof null;  // "object"
```

For a number or string, the `typeof` operator returns:

```js
typeof 64;  // "number"
typeof "Hello world";  // "string"
```

For property values, the `typeof` operator returns the type of value the property contains:

```js
typeof document.lastModified;  // "string"
typeof window.length;  // "number"
typeof Math.LN2;  // "number"
```

For methods and functions, the `typeof` operator returns:

```js
typeof blur;  // "function"
typeof eval;  // "function"
typeof parseInt;  // "function"
typeof Math.abs;  // "function"
```

For predefined objects, the `typeof` operator returns:

```js
typeof Date;  // "function"
typeof Function;  // "function"
typeof Math;  // "object"
typeof Option;  // "function"
typeof String;  // "function"
```

#### `void`

The [`void` operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/void) specifies an expression to be evaluated without returning a value. `expression` is a JavaScript expression to evaluate. The parentheses surrounding the expression are optional, but it is good style to use them to avoid precedence issues.

```js
void (expression)
```

### Relational Operators

A relational operator compares its operands and returns a Boolean value based on whether the comparison is true.

#### `in`

The [`in` operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/in) returns `true` if the specified property is in the specified object.

```js
propNameOrNumber in objectName
```

Above, `propNameOrNumber` is a string, numeric, or symbol expression representing a property name or array index, and `objectName` is the name of an object.

```js
// Arrays
const trees = ["redwood", "bay", "cedar", "oak", "maple"];
0 in trees;  // true
3 in trees;  // true
6 in trees;  // false
"bay" in trees;  // false
// You must specify the index number, not the value at that index
"length" in trees;  // true (length is an Array property)

// Built-in objects
"PI" in Math;  // true
const exString = new String("coral");
"length" in exString;  // true

// Custom objects
const exCar = { make: "Toyota", model: "Prius", year: 2024 };
"make" in exCar;  // true
"model" in exCar;  // true
```

#### `instanceof`

The [`instanceof` operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/instanceof) returns `true` if the specified object is of the specified object type.

```js
object instanceof objectType
```

Above, `object` is the object to test against `objectType`, and `objectType` is a constructor representing a type, such as `Date` or `Array`.

Use `instanceof` when you need to confirm the type of an object at runtime. For example, the following code uses `instanceof` to determine whether `exDay` is a `Date` object. Since `exDay` is a `Date` object, the statements in the `if` statement execute.

```js
const exDay = new Date(2024, 12, 4);
if (exDay instanceof Date) {
  // Statements to execute
}
```

### Basic Expressions

All operators eventually operate on one or more basic expressions. These basic expressions include identifiers and literals, but there are a few other kinds, as well.

#### `this`

Use the [`this` keyword](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this) to refer to the current object. In general, `this` refers to the calling object in a method. Use `this` either with the dot or the bracket notation:

```js
this["propertyName"];
this.propertyName;
```

Suppose a function called `validate` validates an object's `value` property, given the object and the high and low values:

```js
function validate(obj, lowVal, highVal) {
  if (obj.value < lowVal || obj.value > highVal) {
    console.log("Invalid Value.");
  }
}
```

You could call `validate` in each form element's `onChange` event handler, using `this` to pass it to the form element, as in the following example:

```html
<p>Enter a number between 18 and 99:</p>
<input type="text" name="age" size="3" onChange="validate(this, 18, 99);" />
```

#### Grouping Operator

The grouping operator `( )` controls the precedence of evaluation in expressions.

```js
const [a, b, c] = [1, 2, 3];

// Default precedence
a + b * c;  // 7
// Evaluated by default like this
a + (b * c);  // 7

// Now overriding precedence
// Addition before multiplication
(a + b) * c;  // 9

// Which is equivalent to
a * c + b * c;  // 9
```

#### Property Accessor

The [property accessor](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Property_accessors) syntax gets property values on objects, using either dot notation or bracket notation.

```js
object.property;
object["property"];
```

#### Optional Chaining

The [optional chaining](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining) syntax (`?.`) performs the chained operation on an object if it is defined and non-`null`, and otherwise short-circuits the operation and returns `undefined`. This allows you to operate on a value that may be `null` or `undefined` without causing a `TypeError`.

```js
maybeObject?.property;
maybeObject?.[property];
maybeFunction?.();
```

#### `new`

The `new` operator creates an instance of a user-defined object type or of one of the built-in object types.

```js
const objectName = new ObjectType(param1, param2, /* ..., */ paramN);
```


#### `super`

The `super` keyword is used to call functions on an object's parent. It is useful with `classes` to call the parent constructor.

```js
super(args);  // Calls the parent constructor
super.functionOnParent(args);
```

## Numbers and Dates

### Numbers

In JavaScript, numbers are implemented in [double-precision 64-bit binary format IEEE 754](https://en.wikipedia.org/wiki/Double-precision_floating-point_format). In addition to being able to represent floating-point numbers, the number type has three symbolic values:

1. [`Infinity`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Infinity)
2. `-Infinity`
3. [`NaN`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NaN) (not-a-number)

You can use four types of number literals:

1. decimal
2. binary
3. octal
4. hexadecimal

#### Decimal Numbers

Decimal literals can start with a zero (`0`) followed by another decimal digit, but if all digits after the leading `0` are smaller than 8, the number is interpreted as an octal number.

```js
1234567890;
124;
```

This is considered a legacy syntax, and number literals prefixed with 0, whether interpreted as octal or decimal, cause a syntax error in strict mode. Use the `0o` prefix instead.

```js
0888;  // 888 parsed as decimal
0777;  // Parsed as octal, 511 in decimal
```

#### Binary Numbers

Binary number syntax uses a leading zero followed by a lowercase or uppercase Latin letter B (`0b` or `0B`). If the digits after the `0b` are not 0 or 1, the following [`SyntaxError`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SyntaxError) is thrown: `Missing binary digits after 0b`.

```js
0b10000000000000000000000000000000;  // 2147483648
0b01111111100000000000000000000000;  // 2139095040
0B00000000011111111111111111111111;  // 8388607
```

#### Octal Numbers

The standard syntax for octal numbers is to prefix them with `0o`.

```js
0O755;  // 493
0o644;  // 420
```

#### Hexadecimal Numbers

Hexadecimal number syntax uses a leading zero followed by a lowercase or uppercase Latin letter X (`0x` or `0X`). If the digits after `0x` are outside the range (0123456789ABCDEF), the following `SyntaxError` is thrown: `Identifier starts immediately after numeric literal`.

```js
0xFFFFFFFFFFFFFFFFF;  // 295147905179352830000
0x123456789ABCDEF;  // 81985529216486900
0XA;  // 10
```

#### Exponentiation

```js
0e-5;  // 0
0e+5;  // 0
5e1;  // 50
175e-2;  // 1.75
1e3;  // 1000
1e-3;  // 0.001
1E3;  // 1000
```

### Number Object

The built-in [`Number`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number) object has properties for numerical constants, such as maximum value, not-a-number, and infinity. You cannot change the values of these properties and you use them as follows:

```js
const biggestNum = Number.MAX_VALUE;
const smallestNum = Number.MIN_VALUE;
const infiniteNum = Number.POSITIVE_INFINITY;
const negInfiniteNum = Number.NEGATIVE_INFINITY;
const notANum = Number.NaN;
```

You always refer to a property of the predefined `Number` object as shown above, and not as a property of a `Number` object you create yourself.

<details>
  <summary>Show/Hide <code>Number</code> Object Properties</summary><br />
  <table>
    <caption><code>Number</code> Object Properties</caption>
    <thead>
      <tr>
        <th>Property</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MAX_VALUE"><code>Number.MAX_VALUE</code></a></td>
        <td>The largest positive representable number (<code>1.7976931348623157e+308</code>)</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MIN_VALUE"><code>Number.MIN_VALUE</code></a></td>
        <td>The smallest positive representable number (<code>5e-324</code>)</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/NaN"><code>Number.NaN</code></a></td>
       <td>Special "not a number" value</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/NEGATIVE_INFINITY"><code>Number.NEGATIVE_INFINITY</code></a></td>
        <td>Special negative infinite value; returned on overflow</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/POSITIVE_INFINITY"><code>Number.POSITIVE_INFINITY</code></a></td>
        <td>Special positive infinite value; returned on overflow</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/EPSILON"><code>Number.EPSILON</code></a></td>
        <td>Difference between <code>1</code> and the smallest value greater than <code>1</code> that can be represented as a <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number"><code>Number</code></a> (<code>2.220446049250313e-16</code>)</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MIN_SAFE_INTEGER"><code>Number.MIN_SAFE_INTEGER</code></a></td>
        <td>Minimum safe integer in JavaScript (−2^53 + 1, or <code>−9007199254740991</code>)</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MAX_SAFE_INTEGER"><code>Number.MAX_SAFE_INTEGER</code></a></td>
        <td>Maximum safe integer in JavaScript (+2^53 − 1, or <code>+9007199254740991</code>)</td>
      </tr>
    </tbody>
  </table>
</details>

<details>
  <summary>Show/Hide <code>Number</code> Object Methods</summary><br />
  <table>
    <caption><code>Number</code> Object Methods</caption>
    <thead>
      <tr>
        <th>Method</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/parseFloat"><code>Number.parseFloat()</code></a></td>
        <td>Parses a string argument and returns a floating point number. Same as the global <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseFloat"><code>parseFloat()</code></a> function.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/parseInt"><code>Number.parseInt()</code></a></td>
        <td>Parses a string argument and returns an integer of the specified radix or base. Same as the global <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt"><code>parseInt()</code></a> function.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isFinite"><code>Number.isFinite()</code></a></td>
        <td>Determines whether the passed value is a finite number.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger"><code>Number.isInteger()</code></a></td>
        <td>Determines whether the passed value is an integer.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isNaN"><code>Number.isNaN()</code></a></td>
        <td>Determines whether the passed value is <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NaN"><code>NaN</code></a>. More robust version of the original global <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/isNaN"><code>isNaN()</code></a>.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isSafeInteger"><code>Number.isSafeInteger()</code></a></td>
        <td>Determines whether the provided value is a number that is a <em>safe integer</em>.</td>
      </tr>
    </tbody>
  </table>
</details>

The `Number` prototype provides methods for retrieving information from `Number` objects in various formats.

<details>
  <summary>Show/Hide <code>Number.prototype</code> Methods</summary><br />
  <table>
    <caption><code>Number.prototype</code> Methods</caption>
    <thead>
      <tr>
        <th>Method</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toExponential"><code>.toExponential()</code></a></td>
        <td>Returns a string representing the number in exponential notation.</td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toFixed"><code>.toFixed()</code></a></td>
        <td>Returns a string representing the number in fixed-point notation.</td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toPrecision"><code>.toPrecision()</code></a></td>
      <td>Returns a string representing the number to a specified precision in   fixed-point   notation.</td>
      </tr>
    </tbody>
  </table>
</details>

### `Math` Object

The built-in [`Math`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math) object has properties and methods for mathematical constants and functions.

Standard mathematical functions are methods of `Math`. These include trigonometric, logarithmic, exponential, and other functions. Note that all trigonometric methods of `Math` take arguments in radians.

<details>
  <summary>Show/Hide <code>Math</code> Methods</summary><br />
  <table class="standard-table">
    <caption><code>Math</code> Methods</caption>
    <thead>
      <tr>
        <th>Method</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/abs"><code>.abs()</code></a></td>
        <td>Absolute value</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/sin"><code>.sin()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/cos"><code>.cos()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/tan"><code>.tan()</code></a>
        </td>
        <td>Standard trigonometric functions, with the argument in radians.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/asin"><code>.asin()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/acos"><code>.acos()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/atan"><code>.atan()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/atan2"><code>.atan2()</code></a>
        </td>
        <td>Inverse trigonometric functions. Return values in radians.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/sinh"><code>.sinh()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/cosh"><code>.cosh()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/tanh"><code>.tanh()</code></a>
        </td>
        <td>Hyperbolic functions. Argument in hyperbolic angle.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/asinh"><code>.asinh()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/acosh"><code>.acosh()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/atanh"><code>.atanh()</code></a>
        </td>
        <td>Inverse hyperbolic functions. Return values in hyperbolic angle.</td>
      </tr>
      <tr>
        <td><p><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/pow"><code>.pow()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/exp"><code>.exp()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/expm1"><code>.expm1()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/log"><code>.log()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/log10"><code>.log10()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/log1p"><code>.log1p()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/log2"><code>.log2()</code></a></p></td>
        <td>Exponential and logarithmic functions.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/floor"><code>.floor()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/ceil"><code>.ceil()</code></a></td>
        <td>Returns the largest/smallest integer less/greater than or equal to an argument.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/min"><code>.min()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/max"><code>.max()</code></a></td>
        <td>Returns the minimum or maximum (respectively) value of a comma separated list of numbers as arguments.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random"><code>.random()</code></a></td>
        <td>Returns a random number between 0 and 1.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round"><code>.round()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/fround"><code>.fround()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/trunc"><code>.trunc()</code></a></td>
        <td>Rounding and truncation functions.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/sqrt"><code>.sqrt()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/cbrt"><code>.cbrt()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/hypot"><code>.hypot()</code></a></td>
        <td>Square root, cube root, Square root of the sum of square arguments.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/sign"><code>.sign()</code></a></td>
        <td>The sign of a number, indicating whether the number is positive, negative, or zero.
        </td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/clz32"><code>.clz32()</code></a>,<br><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/imul"><code>.imul()</code></a></td>
        <td>Number of leading zero bits in the 32-bit binary representation.<br>The result of the C-like 32-bit multiplication of the two arguments.</td>
      </tr>
    </tbody>
  </table>
</details>

### `BigInt`s

One shortcoming of number values is they only have 64 bits. In practice, due to using IEEE 754 encoding, they cannot represent any integer larger than `[Number.MAX_SAFE_INTEGER`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MAX_SAFE_INTEGER) (which is 2^53 - 1) accurately.

To solve the need of encoding binary data and to interoperate with other languages that offer wide integers like `i64` (64-bit integers) and `i128` (128-bit integers), JavaScript also offers another data type to represent arbitrarily large integers, [`BigInt`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt).

A `BigInt` can be defined as an integer literal suffixed by `n`:

```js
const b1 = 123n;
// Can be arbitrarily large
const b2 = -1234567890987654321n;
```

`BigInt`s can also be constructed from number values or string values using the `BigInt()` constructor:

```js
const b1 = BigInt(123);
/* Using a string prevents loss of precision, since long number
   literals do not represent what they seem like. */
const b2 = BigInt("-1234567890987654321");
```

Conceptually, a `BigInt` is just an arbitrarily long sequence of bits that encodes an integer. You can safely do any arithmetic operations without losing precision or over-/underflowing.

```js
const integer = 12 ** 34;
// 4.9222352429520264e+36 ; Only has limited precision
const bigint = 12n ** 34n;  // 4922235242952026704037113243122008064n
```

Compared to numbers, `BigInt` values yield higher precision when representing large *integers*. However, they cannot represent floating-point numbers. For example, division would round to zero:

```js
const bigintDiv = 5n / 2n;  // 2n, because there is no 2.5 in BigInt
```

`Math` functions cannot be used on `BigInt` values.

Choosing between `BigInt` and `Number` depends on your use-case and your input's range. The precision of numbers should be able to accommodate most day-to-day tasks already, and `BigInt`s are most suitable for handling binary data.

### `Date` Object

JavaScript does not have a date data type. However, you can use the [`Date`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date) object and its methods to work with dates and times in your applications. The `Date` object has a large number of methods for setting, getting, and manipulating dates. It does not have any properties.

JavaScript handles dates similarly to Java. The two languages have many of the same date methods, and both languages store dates as the number of milliseconds since midnight at the beginning of January 1, 1970, UTC, with a UNIX timestamp being the number of seconds since the same instant. The instant at the midnight at the beginning of January 1, 1970, UTC is called the [epoch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#the_epoch_timestamps_and_invalid_date).

The `Date` object range is -100,000,000 days to 100,000,000 days relative to the epoch.

```js
const dateObjectName = new Date([parameters]);
```

Above, `dateObjectName` is the name of the `Date` object being created. It can be a new object or a property of an existing object.

Calling `Date` without the `new` keyword returns a string representing the current date and time.

The `parameters` in the preceding syntax can be any of the following:

- Nothing - Creates today's date and time. For example, `today = new Date();`.
- A string representing a date, in many different forms. The exact forms supported differ among engines, but the following form is always supported: `YYYY-MM-DDTHH:mm:ss.sssZ`.

    For example, `xmas98 = new Date("1998-12-25")`. If you omit hours, minutes, or seconds, the value will be set to zero.

- A set of integer values for year, month, and day. For example, `xmas98 = new Date(1998, 11, 25)`.
- A set of integer values for year, month, day, hour, minute, and seconds. For example, `xmas98 = new Date(1998, 11, 25, 9, 30, 0);`.

#### Methods of the `Date` Object

The `Date` object methods for handling dates and times fall into four broad categories:

1. *set* methods, for setting date and time values in `Date` objects.
2. *get* methods, for getting date and time values from `Date` objects.
3. *to* methods, for returning string values from `Date` objects.
4. parse and UTC methods, for parsing `Date` strings.

With the *get* and *set* methods you can get and set seconds, minutes, hours, day of the month, day of the week, months, and years separately. There is a `.getDay()` method that returns the day of the week, but no corresponding `.setDay()` method, because the day of the week is set automatically.

These methods use integers to represent these values as follows:

- Seconds and minutes: 0 to 59
- Hours: 0 to 23
- Day: 0 (Sunday) to 6 (Saturday)
- Date: 1 to 31 (day of the month)
- Months: 0 (January) to 11 (December)
- Year: years since 1900

```js
const xmas98 = new Date("1998-12-25");
console.log(xmas98.getMonth());
// 11
console.log(xmas98.getFullYear())
// 1998
```

The `.getTime()` and `.setTime()` methods are useful for comparing dates. The `.getTime()` method returns the number of milliseconds since the epoch for a `Date` object.

```js
const today = new Date();
const endYear = new Date(1998, 11, 31, 23, 59, 59, 999);  // Set day and month
endYear.setFullYear(today.getFullYear());  // Set year to this year

const msPerDay = 24 * 60 * 60 * 1000;  // Number of milliseconds per day
let daysLeft = (endYear.getTime() - today.getTime()) / msPerDay;
daysLeft = Math.round(daysLeft);  // Returns days left in the year
```

The `.parse()` method is useful for assigning values from date strings to existing `Date` objects:

```js
const pubDate = new Date();
pubDate.setTime(Date.parse("May 06, 1998"));
```

## Text Formatting

### Strings

JavaScript's [`String`](https://developer.mozilla.org/en-US/docs/Glossary/String) type is used to represent textual data. It is a set of *elements* of 16-bit unsigned integer values (UTF-16 code units). Each element in the `String` occupies a position in the `String`.

The first element is at index `0`, the next at index `1`, and so on. The length of a `String` is the number of elements in it. You can create strings using string literals or string objects.

#### String Literals

You can declare strings in source code using either single or double quotes:

```js
'the cause'
"was just"
```

More advanced strings can be created using escape sequences.

##### Hexadecimal Escape Sequences

The number after `\x` is interpreted as a [hexadecimal](https://en.wikipedia.org/wiki/Hexadecimal) number.

```js
"\xA9"  // "©"
```

##### Unicode Escape Sequences

The Unicode escape sequences require at least four hexadecimal digits following `\u`.

```js
"\u00A9"  // "©"
```

##### Unicode Code Point Escapes

With Unicode code point escapes, any character can be escaped using hexadecimal numbers, so that it is possible to use Unicode code points up to `0x10FFFF`. With the four-digit Unicode escapes, it is often necessary to write the surrogate halves separately to achieve the same result.

See also [`String.fromCodePoint()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/fromCodePoint) or [`String.prototype.codePointAt()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/codePointAt).

```js
"\u{2F804}"

// The same with simple Unicode escapes
"\uD87E\uDC04"
```

#### String Objects

The `String` object is a wrapper around the string primitive data type.

```js
// Creates a String object
const foo = new String(
  "...a world in which the wicked went about their days "
  + "frightened of retribution was a more just one than "
  + "a world where the wicked held their heads high."
);
console.log(foo);
// String { "...a world in which the wicked went about their days frightened of retribution was a more just one than a world where the wicked held their heads high." }
typeof foo;  // "object"
```

You can call any of the methods of the `String` object on a string literal value. JavaScript automatically converts the string literal to a temporary `String` object, calls the method, then discards the temporary `String` object. Also, you can use the `.length` property with a string literal.

You should use string literals unless you specifically need to use a `String` object because `String` objects can have counterintuitive behavior. For example:

```js
const firstString = "2 + 2";  // Creates a string literal value
const secondString = new String("2 + 2");  // Creates a String object

eval(firstString);  // Returns the number 4
eval(secondString);  // Returns a String object containing "2 + 2"
```

A String object has one property, `.length`, that indicates the number of UTF-16 code units in the string.

```js
const hello = (
  "While the Democrats would give more help to the poor than the Republicans, "
  + "they were not capable (indeed, not really desirous) of "
  + "seriously tampering with an economic system in which "
  + "corporate profit comes before human need."
);
const helloLength = hello.length;

hello[0] = "Howard Zinn";  // This has no effect because strings are immutable
hello[0];  // This returns "W"
```

Characters whose Unicode scalar values are greater than U+FFFF (e.g., some rare Chinese/Japanese/Korean/Vietnamese characters and some emoji) are stored in UTF-16 with two surrogate code units each. Accessing the individual code units in such a string using square brackets may have undesirable consequences, like the formation of strings with unmatched surrogate code units, in violation of the Unicode standard.

See also [`String.fromCodePoint()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/fromCodePoint) or [`String.prototype.codePointAt()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/codePointAt) for more information.

A `String` object has a variety of methods,

<details>
  <summary>Show/Hide <code>String</code> Methods</summary><br />
  <table>
    <caption><code>String</code> Methods</caption>
    <thead>
      <tr>
        <th>Method</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/charAt"><code>.charAt()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/charCodeAt"><code>.charCodeAt()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/codePointAt"><code>.codePointAt()</code></a></td>
        <td>Return the character or character code at the specified position in string.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/indexOf"><code>.indexOf()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/lastIndexOf"><code>.lastIndexOf()</code></a></td>
        <td>Return the position of specified substring in the string or last position of specified substring, respectively.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith"><code>.startsWith()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith"><code>.endsWith()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes"><code>.includes()</code></a></td>
        <td>Returns whether or not the string starts, ends or contains a specified string.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/concat"><code>.concat()</code></a></td>
        <td>Combines the text of two strings and returns a new string.</td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split"><code>.split()</code></a></td>
        <td>Splits a <code>String</code> object into an array of strings by separating the string into substrings.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/slice"><code>.slice()</code></a></td>
        <td>Extracts a section of a string and returns a new string.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/substring"><code>.substring()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/substr"><code>.substr()</code></a></td>
        <td>Return the specified subset of the string, either by specifying the start and end indexes or the start index and a length.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/match"><code>.match()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/matchAll"><code>.matchAll()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace"><code>.replace()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replaceAll"><code>.replaceAll()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/search"><code>.search()</code></a></td>
        <td>Work with regular expressions.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/toLowerCase"><code>.toLowerCase()</code></a>, <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/toUpperCase"><code>.toUpperCase()</code></a></td>
        <td><p>Return the string in all lowercase or all uppercase, respectively.</p></td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/normalize"><code>.normalize()</code></a></td>
        <td>Returns the Unicode Normalization Form of the calling string value.</td>
      </tr>
      <tr>
      <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/repeat"><code>.repeat()</code></a></td>
        <td>Returns a string consisting of the elements of the object repeated the given times.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/trim"><code>.trim()</code></a></td>
        <td>Trims whitespace from the beginning and end of the string.</td>
      </tr>
    </tbody>
  </table>
</details>

#### Multi-Line Template Literals

[Template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals) are string literals allowing embedded expressions. You can use multi-line strings and string interpolation features with them.

Template literals are enclosed by backtick ([grave accent](https://en.wikipedia.org/wiki/Grave_accent)) characters (\`) instead of double or single quotes. Template literals can contain placeholders. These are indicated by the dollar sign and curly braces (`${expression}`).

##### Multi-Lines

Any new line characters inserted in the source are part of the template literal. Using normal strings, you would have to use the following syntax in order to get multi-line strings:

```js
console.log(
  "Ladies. Gentlemen.\n\
You've eaten well. You've eaten Gotham's wealth. Its spirit.",
);
/*
Ladies. Gentlemen.
You've eaten well. You've eaten Gotham's wealth. Its spirit.
*/
```

To get the same effect with multi-line strings, you can now write:

```js
console.log(
  `But your feast is nearly over.
From this moment on, none of you are safe.`
)
/*
But your feast is nearly over.
From this moment on, none of you are safe.
*/
```

##### Embedded Expressions

In order to embed expressions within normal strings, you would use the following syntax:

```js
const five = 5;
const ten = 10;
console.log(
  "Fifteen is " + (five + ten) + " and not " + (2 * five + ten) + ".",
);
// "Fifteen is 15 and not 20."
```

Now, with template literals, you are able to make use of the syntactic sugar making substitutions like this more readable:

```js
const five = 5;
const ten = 10;
console.log(`Fifteen is ${five + ten} and not ${2 * five + ten}.`);
// "Fifteen is 15 and not 20."
```
### Internationalization

The [`Intl`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl) object is the namespace for the ECMAScript Internationalization API, which provides language sensitive string comparison, number formatting, and date and time formatting. The constructors for [`Intl.Collator`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/Collator), [`Intl.NumberFormat`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat), and [`Intl.DateTimeFormat`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat) objects are properties of the `Intl` object.

#### Date and Time Formatting

The [`Intl.DateTimeFormat`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat) object is useful for formatting date and time.

```js
// July 17, 2014 00:00:00 UTC:
const july172014 = new Date("2014-07-17");

const options = {
  year: "2-digit",
  month: "2-digit",
  day: "2-digit",
  hour: "2-digit",
  minute: "2-digit",
  timeZoneName: "short",
};
const americanDateTime = new Intl.DateTimeFormat("en-US", options).format;

// Local timezone vary depending on your settings
// In CEST, logs: 07/17/14, 02:00 AM GMT+2
// In PDT, logs: 07/16/14, 05:00 PM GMT-7
console.log(americanDateTime(july172014));
```

#### Number Formatting

The [`Intl.NumberFormat`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat) object is useful for formatting numbers, e.g., currencies.

```js
const gasPrice = new Intl.NumberFormat(
  "en-US",
  {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 3,
  }
);
console.log(gasPrice.format(5.259));
// $5.259

const hanDecimalRMBInChina = new Intl.NumberFormat(
  "zh-CN-u-nu-hanidec",
  {
    style: "currency",
    currency: "CNY",
  }
);
console.log(hanDecimalRMBInChina.format(1314.25));
// ¥一,三一四.二五
```

#### Collation

The [`Intl.Collator`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/Collator) object is useful for comparing and sorting strings.

For example, there are actually two different sort orders in German, *phonebook* and *dictionary*. Phonebook sort emphasizes sound, and it is as if *ä* and *ö* were expanded to *ae* and *oe*, prior to sorting.

```js
const names = ["Hochberg", "Hönigswald", "Holzman"];
const germanPhonebook = new Intl.Collator("de-DE-u-co-phonebk");

// As if sorting ["Hochberg", "Hoenigswald", "Holzman"]
console.log(names.sort(germanPhonebook.compare).join(", "));
// "Hochberg, Hönigswald, Holzman"
```

## Regular Expressions

Regular expressions are patterns used to match character combinations in strings. In JavaScript, regular expressions are also objects. These patterns are used with the [`.exec()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/exec) and [`.test()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test) methods of [`RegExp`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp), and with the [`.match()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/match), [`.matchAll()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/matchAll), [`.replace()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace), [`.replaceAll()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replaceAll), [`.search()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/search), and [`.split()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split) methods of [`String`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String).


### Creating a Regular Expression

You construct a regular expression in one of two ways:

1. Using a regular expression literal, which consists of a pattern enclosed between slashes:

        const re = /ab+c/;

    Regular expression literals provide compilation of the regular expression when the script is loaded. If the regular expression remains constant, using this can improve performance.

2. Calling the constructor function of the `RegExp` object:

        const re = new RegExp("ab+c");

    Using the constructor function provides runtime compilation of the regular expression. Use the constructor function when you know the regular expression pattern will be changing, or you do not know the pattern and are getting it from another source, such as user input.

### Writing a Regular Expression Pattern

A regular expression pattern is composed of simple characters or a combination of simple and special characters.

#### Using Simple Patterns

Simple patterns are constructed of characters for which you want to find a direct (exact) match.

#### Using Special Characters

When the search for a match requires something more than a direct match (e.g., finding one or more b's, or finding white space), you can include special characters in the pattern.

The following pages provide lists of the different special characters that fit into each category, along with descriptions and examples:

- [Assertions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions/Assertions)


    Assertions include boundaries, which indicate the beginnings and endings of lines and words, and other patterns indicating in some way that a match is possible (including look-ahead, look-behind, and conditional expressions).

- [Character Classes](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions/Character_classes)


    Character classes distinguish different types of characters. For example, distinguishing between letters and digits.

- [Groups and Backreferences](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions/Groups_and_backreferences)

    Groups group multiple patterns as a whole, and capturing groups provide extra submatch information when using a regular expression pattern to match against a string. Backreferences refer to a previously captured group in the same regular expression.

- [Quantifiers](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions/Quantifiers)

    Quantifiers indicate numbers of characters or expressions to match.

<details>
  <summary>Show/Hide Regular Expression Special Characters</summary><br />
  <table>
    <caption>Regular Expression Special Characters</caption>
    <thead>
      <tr>
        <th>Characters / constructs</th>
        <th>Corresponding article</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><code>[xyz]</code>, <code>[^xyz]</code>, <code>.</code>, <code>\d</code>, <code>\D</code>, <code>\w</code>, <code>\W</code>, <code>\s</code>, <code>\S</code>, <code>\t</code>, <code>\r</code>, <code>\n</code>, <code>\v</code>, <code>\f</code>, <code>[\b]</code>, <code>\0</code>, <code>\c<em>X</em></code>, <code>\x<em>hh</em></code>, <code>\u<em>hhhh</em></code>, <code>\u<em>{hhhh}</em></code>, <code><em>x</em>|<em>y</em></code>
        </td>
        <td><p><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions/Character_classes">Character classes</a></p></td>
      </tr>
      <tr>
        <td><code>^</code>, <code>$</code>, <code>\b</code>, <code>\B</code>, <code>x(?=y)</code>, <code>x(?!y)</code>, <code>(?&lt;=y)x</code>, <code>(?&lt;!y)x</code></td>
        <td><p><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions/Assertions">Assertions</a></p></td>
      </tr>
      <tr>
        <td><code>(<em>x</em>)</code>, <code>(?&lt;Name&gt;x)</code>, <code>(?:<em>x</em>)</code>, <code>\<em>n</em></code>, <code>\k&lt;Name&gt;</code></td>
        <td><p><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions/Groups_and_backreferences">Groups and backreferences</a></p></td>
      </tr>
      <tr>
        <td><code><em>x</em>*</code>, <code><em>x</em>+</code>, <code><em>x</em>?</code>, <code><em>x</em>{<em>n</em>}</code>, <code><em>x</em>{<em>n</em>,}</code>, <code><em>x</em>{<em>n</em>,<em>m</em>}</code></td>
        <td><p><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions/Quantifiers">Quantifiers</a></p></td>
      </tr>
    </tbody>
  </table>
</details>

For more information on regular expression special characters, see the [Regular Expression Syntax Cheat Sheet](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions/Cheatsheet).

#### Escaping

If you need to use any of the special characters literally, you must escape it by putting a backslash (`\`) in front of it. Similarly, if you're writing a regular expression literal and need to match a slash (`/`), you need to escape that (otherwise, it terminates the pattern).

To match a literal backslash, you need to escape the backslash.

If using the `RegExp` constructor with a string literal, remember that the backslash is an escape in string literals. So, to use it in the regular expression, you need to escape it at the string literal level.

The [`RegExp.escape()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/escape) function returns a new string where all special characters in regex syntax are escaped.

#### Using Parentheses

Parentheses around any part of the regular expression pattern causes that part of the matched substring to be remembered. Once remembered, the substring can be recalled for other use.

See [Groups and backreferences](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions/Groups_and_backreferences#using_groups) for more details.

### Using Regular Expressions in JavaScript

Regular expressions are used with the `RegExp` methods `.test()` and `.exec()` and with the `String` methods `.match()`, `.matchAll()`, `.replace()`, `.replaceAll()`, `.search()`, and `.split()`.

<details>
  <summary>Show/Hide Regular Expression Methods</summary><br />
  <table>
    <caption>Regular Expression Methods</caption>
    <thead>
      <tr>
        <th>Method</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/exec"><code>.exec()</code></a></td>
        <td>Executes a search for a match in a string. It returns an array of information or <code>null</code> on a mismatch.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test"><code>.test()</code></a></td>
        <td>Tests for a match in a string. It returns <code>true</code> or <code>false</code>.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/match"><code>.match()</code></a></td>
        <td>Returns an array containing all of the matches, including capturing groups, or <code>null</code> if no match is found.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/matchAll"><code>.matchAll()</code></a></td>
        <td>Returns an iterator containing all of the matches, including capturing groups.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/search"><code>.search()</code></a></td>
        <td>Tests for a match in a string. It returns the index of the match, or <code>-1</code> if the search fails.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace"><code>.replace()</code></a></td>
        <td>Executes a search for a match in a string, and replaces the matched substring with a replacement substring.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replaceAll"><code>.replaceAll()</code></a></td>
        <td>Executes a search for all matches in a string, and replaces the matched substrings with a replacement substring.</td>
      </tr>
      <tr>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split"><code>.split()</code></a></td>
        <td>Uses a regular expression or a fixed string to break a string into an array of substrings.</td>
      </tr>
    </tbody>
  </table>
</details>

When you want to know whether a pattern is found in a string, use the `.test()` or `.search()` methods. For more information (but slower execution) use the `.exec()` or `.match()` methods.

If you use `.exec()` or `.match()`, and if the match succeeds, these methods return an array and update properties of the associated regular expression object, and also of the predefined regular expression object, `RegExp`. If the match fails, the `.exec()` method returns `null` (which coerces to `false`).

```js
const exRe = /d(b+)d/g;
const exArray = exRe.exec("cdbbdbsbz");
```

If you do not need to access the properties of the regular expression, an alternative way of creating `exArray` is with this script:

```js
const exArray = /d(b+)d/g.exec("cdbbdbsbz");
/* Similar to 'cdbbdbsbz'.match(/d(b+)d/g). However,
   'cdbbdbsbz'.match(/d(b+)d/g) outputs [ "dbbd" ],
   while /d(b+)d/g.exec('cdbbdbsbz') outputs
   [ 'dbbd', 'bb', index: 1, input: 'cdbbdbsbz' ] */
```

For further information about the different behaviors, see [Using the global search flag with `.exec()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions#using_the_global_search_flag_with_exec).

If you want to construct the regular expression from a string, yet another alternative is this script:

```js
const exRe = new RegExp("d(b+)d", "g");
const exArray = exRe.exec("cdbbdbsbz");
```

With these scripts, the match succeeds and returns the array, and updates the properties shown in the following table.

<table>
  <caption>Results of regular expression execution.</caption>
  <thead>
    <tr>
      <th>Object</th>
      <th>Property or index</th>
      <th>Description</th>
      <th>In this example</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td rowspan="4"><code>exArray</code></td>
      <td></td>
      <td>The matched string and all remembered substrings.</td>
      <td><code>['dbbd', 'bb', index: 1, input: 'cdbbdbsbz']</code></td>
    </tr>
    <tr>
      <td><code>index</code></td>
      <td>The 0-based index of the match in the input string.</td>
      <td><code>1</code></td>
    </tr>
    <tr>
      <td><code>input</code></td>
      <td>The original string.</td>
      <td><code>'cdbbdbsbz'</code></td>
    </tr>
    <tr>
      <td><code>[0]</code></td>
      <td>The last matched characters.</td>
      <td><code>'dbbd'</code></td>
    </tr>
    <tr>
      <td rowspan="2"><code>exRe</code></td>
      <td><code>lastIndex</code></td>
      <td>The index at which to start the next match. (This property is set only if the regular expression uses the g option, described in <a href="#advanced_searching_with_flags">Advanced Searching With Flags</a>.)
      </td>
      <td><code>5</code></td>
    </tr>
    <tr>
      <td><code>source</code></td>
      <td>The text of the pattern. Updated at the time that the regular expression is created, not executed.</td>
      <td><code>'d(b+)d'</code></td>
    </tr>
  </tbody>
</table>

As shown in the second form of this example, you can use a regular expression created with an object initializer without assigning it to a variable. However, if you do, every occurrence is a new regular expression. For this reason, if you use this form without assigning it to a variable, you cannot subsequently access the properties of that regular expression.

For example, assume you have this script:

```js
const exRe = /d(b+)d/g;
const exArray = exRe.exec("cdbbdbsbz");
console.log(`The value of lastIndex is ${exRe.lastIndex}`);
// The value of lastIndex is 5
```

However, if you have this script:

```js
const exArray = /d(b+)d/g.exec("cdbbdbsbz");
console.log(`The value of lastIndex is ${/d(b+)d/g.lastIndex}`);
// The value of lastIndex is 0
```

The occurrences of `/d(b+)d/g` in the two statements are different regular expression objects and hence have different values for their `lastIndex` property. If you need to access the properties of a regular expression created with an object initializer, you should first assign it to a variable.

#### Advanced Searching With Flags

Regular expressions have optional flags that allow for functionality like global searching and case-insensitive searching. These flags can be used separately or together in any order, and are included as part of the regular expression.

<details>
  <summary>Show/Hide Regular Expression Flags</summary><br />
  <table>
    <caption>Regular Expression Flags</caption>
    <thead>
      <tr>
        <th>Flag</th>
        <th>Description</th>
        <th>Corresponding property</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><code>d</code></td>
        <td>Generate indices for substring matches.</td>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/hasIndices"><code>hasIndices</code></a></td>
      </tr>
      <tr>
        <td><code>g</code></td>
        <td>Global search.</td>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/global"><code>global</code></a></td>
      </tr>
      <tr>
        <td><code>i</code></td>
        <td>Case-insensitive search.</td>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/ignoreCase"><code>ignoreCase</code></a></td>
      </tr>
      <tr>
        <td><code>m</code></td>
        <td>Allows <code>^</code> and <code>$</code> to match next to newline characters.</td>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/multiline"><code>multiline</code></a></td>
      </tr>
      <tr>
        <td><code>s</code></td>
        <td>Allows <code>.</code> to match newline characters.</td>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/dotAll"><code>dotAll</code></a></td>
      </tr>
      <tr>
        <td><code>u</code></td>
        <td><em>Unicode</em>. Treat a pattern as a sequence of Unicode code points.</td>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/unicode"><code>unicode</code></a></td>
      </tr>
      <tr>
        <td><code>v</code></td>
        <td>An upgrade to the <code>u</code> mode with more Unicode features.</td>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/unicodeSets"><code>unicodeSets</code></a></td>
      </tr>
      <tr>
        <td><code>y</code></td>
        <td>Perform a <em>sticky</em> search that matches starting at the current position in the target string.</td>
        <td><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/sticky"><code>sticky</code></a></td>
      </tr>
    </tbody>
  </table>
</details>

To include a flag with the regular expression, use this syntax:

```
const re = /pattern/flags;
```

Or:

```js
const re = new RegExp("pattern", "flags");
```

Note that the flags are an integral part of a regular expression. They cannot be added or removed later.

The `m` flag is used to specify that a multiline input string should be treated as multiple lines. If the `m` flag is used, `^` and `$` match at the start or end of any line within the input string instead of the start or end of the entire string.

The `i`, `m`, and `s` flags can be enabled or disabled for specific parts of a regex using the [modifier](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Regular_expressions/Modifier) syntax.

##### Using the Global Search Flag With `.exec()`

The [`RegExp.prototype.exec()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/exec) method with the `g` flag iteratively returns each match and its position:

```js
const str = "fee fi fo fum";
const re = /\w+\s/g;

console.log(re.exec(str));
// Array [ "fee " ]
console.log(re.exec(str));
// Array [ "fi " ]
console.log(re.exec(str));
// Array [ "fo " ]
console.log(re.exec(str));
// null
```

In contrast, the [`String.prototype.match()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/match) method returns all matches at once, but without their position:

```js
const str = "fee fi fo fum";
const re = /\w+\s/g;

console.log(str.match(re));
// Array(3) [ "fee ", "fi ", "fo " ]
```

##### Using Unicode Regular Expressions

The `u` flag is used to create *unicode* regular expressions, i.e., regular expressions that support matching against unicode text. An important feature that is enabled in unicode mode is [Unicode property escapes](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Regular_expressions/Unicode_character_class_escape).

For example, the following regular expression might be used to match against an arbitrary unicode *word*:

```js
/\p{L}*/u;
```

Unicode regular expressions have different execution behavior, as well. [`RegExp.prototype.unicode`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/unicode) contains more information about this.


### Tools

- [RegExr](https://regexr.com/) - An online tool to learn, build, and test regular expressions.
- [Regular Expressions 101](https://regex101.com/) - An online regex builder/debugger.
- [RegExLearn](https://regexlearn.com/) - Online interactive tutorials, cheat sheet, and playground.
- [CyrilEx Regex Tester](https://extendsclass.com/regex-tester.html) - An online visual regex tester.

## Indexed Collections

An *array* is an ordered list of values that you refer to with a name and an index.

JavaScript does not have an explicit array data type. However, you can use the predefined `Array` object and its methods to work with arrays in your applications. The `Array` object has methods for manipulating arrays in various ways, such as joining, reversing, and sorting them. It has a property for determining the array length and other properties for use with regular expressions.

### Creating an Array

The following statements create equivalent arrays:

```js
const arr1 = new Array(element0, element1, /* ..., */ elementN);
const arr2 = [element0, element1, /* ..., */ elementN];
```

The bracket syntax is called an *array literal* or *array initializer*. It is shorter than other forms of array creation, and so is generally preferred. See [Array literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_types#array_literals) for details.

To create an array with non-zero length, but without any items, either of the following can be used:

```js
const arr1 = new Array(arrayLength);

// This has exactly the same effect
const arr2 = [];
arr2.length = arrayLength;
```

In the above code, `arrayLength` must be a `Number`. Otherwise, an array with a single element (the provided value) will be created. Calling `arr.length` will return `arrayLength`, but the array does not contain any elements. A `for...in` loop will not find any property on the array.

In addition to a newly defined variable as shown above, arrays can also be assigned as a property of a new or an existing object:

```js
const obj = {};
obj.prop = [element0, element1, /* ..., */ elementN];

// Or
const obj = {prop: [element0, element1, /* ..., */ elementN]};
```

If you wish to initialize an array with a single element and the element happens to be a `Number`, you must use the bracket syntax. When a single `Number` value is passed to the `Array()` constructor or function, it is interpreted as an `arrayLength`, not as a single element.

This creates an array with only one element, the number 24:

```js
const arr = [24];
```

This creates an array with no elements and `arr.length` set to 24:

```js
const arr = Array(24);
```

This is equivalent to:

```js
const arr = [];
arr.length = 24;
```

If your code needs to create arrays with single elements of an arbitrary data type, it is safer to use array literals. Alternatively, create an empty array before adding the single element to it.

You can also use the [`Array.of`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/of) static method to create arrays with single element:

```js
const wisenArray = Array.of(9.3);  // wisenArray contains only one element 9.3
```

### Referring to Array Elements

Since elements are also properties, you can access them using [property accessors](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Property_accessors).


```js
const arr = ["one", "two", "three"];
arr[2];  // "three"
arr.length;  // 3
```

### Populating an Array

You can populate an array by assigning values to its elements.

```js
const cherishedMemories = [];
cherishedMemories[0] = "That one's not meant for the celluloid brothel.";
cherishedMemories[1] = "It's a song I write only for myself.";
cherishedMemories[2] = "The world can't lay claim to any of it.";
```
If you supply a non-integer value to the array operator in the code above, a property will be created in the object representing the array, instead of an array element.

```js
const arr = [];
arr[3.4] = "oranges";
console.log(arr.length);
// 0
console.log(Object.hasOwn(arr, 3.4));
// true
```

You can also populate an array when you create it:

```js
const exArray = new Array("Hello", myVar, 3.14159);
// Or
const exArray = ["mango", "apple", "orange"];
```

#### Understanding Length

At the implementation level, JavaScript's arrays actually store their elements as standard object properties, using the array index as the property name.

The `.length` property is special. Its value is always a positive integer greater than the index of the last element, if one exists. You can also assign to the `.length` property.

Writing a value that is shorter than the number of stored items truncates the array. Writing 0 empties it entirely:

```js
const nums = ["one", "two", "three"];
console.log(nums.length);
// 3

nums.length = 2;
console.log(nums);
// Array [ "one", "two" ]

nums.length = 0;
console.log(nums);
// Array []

nums.length = 3;
console.log(nums);
// Array(3) [ <3 empty slots> ]
```

#### Iterating Over Arrays

A common operation is to iterate over the values of an array, processing each one in some way:

```js
const colors = ["red", "green", "blue"];

for (const color of colors) {
  console.log(color);
}
/*
red
green
blue
*/
```

#### Array Methods

The [`.concat()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat) method joins two or more arrays and returns a new array:

```js
let exArray = ["1", "2", "3"];
exArray = exArray.concat("a", "b", "c");
// exArray is now ["1", "2", "3", "a", "b", "c"]
```

The [`.join()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join) method joins all elements of an array into a string:

```js
const exArray = ["earth", "water", "air", "fire"];
const list = exArray.join(" - ");
// list is "earth - water - air - fire"
```

The [`.push()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push) method adds one or more elements to the end of an array and returns the resulting length of the array:

```js
const exArray = ["1", "2"];
exArray.push("3");  // 3
// exArray is now ["1", "2", "3"]
```

The [`.pop()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/pop) method removes the last element from an array and returns that element:

```js
const exArray = ["1", "2", "3"];
const last = exArray.pop();  // exArray is now ["1", "2"], last is "3"
```

The [`.shift()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/shift) method removes the first element from an array and returns that element:

```js
const exArray = ["1", "2", "3"];
const first = exArray.shift();  // exArray is now ["2", "3"], first is "1"
```

The [`.unshift()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/unshift) method adds one or more elements to the front of an array and returns the new length of the array:

```js
const exArray = ["1", "2", "3"];
exArray.unshift("4", "5");  // 5
// exArray is ["4", "5", "1", "2", "3"]
```

The [`.slice()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice) method extracts a section of an array and returns a new array:

```js
let exArray = ["a", "b", "c", "d", "e"];
exArray = exArray.slice(1, 4);
// exArray is Array(3) [ "b", "c", "d" ]
```

The [`.at()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/at) method returns the element at the specified index in the array, or `undefined` if the index is out of range. It is notably used for negative indices that access elements from the end of the array.

```js
const exArray = ["a", "b", "c", "d", "e"];
exArray.at(-2);  // "d"
```

The [`.splice()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice) method removes elements from an array and (optionally) replaces them. It returns the items which were removed from the array.

```js
const exArray = ["1", "2", "3", "4", "5"];
exArray.splice(1, 3, "a", "b", "c", "d");  // Array(3) [ "2", "3", "4" ]
// exArray is now ["1", "a", "b", "c", "d", "5"]
/* This code started at index one, removed 3 elements there,
   and then inserted all consecutive elements in its place. */
```

The [`.reverse()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse) method transposes the elements of an array, in place. The first array element becomes the last and the last becomes the first. It returns a reference to the array.

```js
const exArray = ["1", "2", "3"];
exArray.reverse();  // Array(3) [ "3", "2", "1" ]
```

The [`.flat().`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flat) method returns a new array with all sub-array elements concatenated into it recursively up to the specified depth:

```js
let exArray = [1, 2, [3, 4]];
exArray = exArray.flat();  // exArray is Array(4) [ 1, 2, 3, 4 ]
```

The [`.sort()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort) method sorts the elements of an array in place, and returns a reference to the array:

```js
const exArray = ["earth", "water", "air", "fire"];
exArray.sort();
// exArray is Array(4) [ "air", "earth", "fire", "water" ]
```

`.sort()` can also take a callback function to determine how array elements are compared. The callback function is called with two arguments, which are two values from the array.

The function compares these two values and returns a positive number, negative number, or zero, indicating the order of the two values. For instance, the following will sort the array by the last letter of a string:

```js
const exArray = ["earth", "water", "air", "fire"];


function sortFn(a, b) {
  if (a[a.length - 1] < b[b.length - 1]) {
    return -1;
  } else if (a[a.length - 1] > b[b.length - 1]) {
    return 1;
  }

  return 0;
}


exArray.sort(sortFn);  // Array(4) [ "fire", "earth", "water", "air" ]
```

- If `a` is less than `b` by the sorting system, return `-1` (or any negative number).
- If `a` is greater than `b` by the sorting system, return `1` (or any positive number).
- If `a` and `b` are considered equivalent, return `0`.

The [`.indexOf()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf) method searches the array for `searchElement` and returns the index of the first match:

```js
const a = ["a", "b", "a", "b", "a"];
console.log(a.indexOf("b"));
// 1

// Now try again, starting from after the last match
console.log(a.indexOf("b", 2));
// 3
console.log(a.indexOf("z"));
// -1, because z was not found
```

The [`.lastIndexOf()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/lastIndexOf) method works like `.indexOf()`, but starts at the end and searches backwards:

```js
const a = ["a", "b", "c", "d", "a", "b"];
console.log(a.lastIndexOf("b"));
// 5

// Now try again, starting from before the last match
console.log(a.lastIndexOf("b", 4));
// 1
console.log(a.lastIndexOf("z"));
// -1
```

The [`.forEach()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach) method executes callback on every array item and returns `undefined`:

```js
const a = ["a", "b", "c"];

a.forEach(
  function (element) {
    console.log(element);
  }
);
/*
a
b
c
*/
```

The `.forEach()` method (and others below) that take a callback are known as *iterative methods* because they iterate over the entire array in some fashion. Each one takes an optional second argument called `thisArg`.

If provided, `thisArg` becomes the value of the `this` keyword inside the body of the callback function. If not provided, as with other cases where a function is invoked outside of an explicit object context, `this` will refer to the global object (e.g., [`window`](https://developer.mozilla.org/en-US/docs/Web/API/Window), [`globalThis`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/globalThis)) when the function is [not strict](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode), or `undefined` when the function is strict.

Note that the `.sort()` method mentioned above is not an iterative method because its callback function is only used for comparison and may not be called in any particular order, based on element order. `.sort()` does not accept the `thisArg` parameter either.

The [`.map()` ](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)method returns a new array of the return value from executing callback on every array item:

```js
const a1 = ["a", "b", "c"];
const a2 = a1.map(function (item) {return item.toUpperCase()});

console.log(a2);
// Array(3) [ "A", "B", "C" ]
```

The [`.flatMap()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flatMap) method runs `.map()` followed by a `.flat()` of depth 1:

```js
const a1 = ["a", "b", "c"];
const a2 = a1.flatMap(
  function (item) {
    return [item.toUpperCase(), item.toLowerCase()]
  }
);

console.log(a2);
// Array(6) [ "A", "a", "B", "b", "C", "c" ]
```

The [`.filter()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter) method returns a new array containing the items for which `callback` returned `true`:

```js
const a1 = ["a", 10, "b", 20, "c", 30];
const a2 = a1.filter(function (item) {return typeof item === "number"});

console.log(a2);
// Array(3) [ 10, 20, 30 ]
```

The [`.find()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find) method returns the first item for which callback returned `true`:

```js
const a1 = ["a", 10, "b", 20, "c", 30];
const i = a1.find(function (item) {return typeof item === "number"});

console.log(i);
// 10
```

The [`.findLast()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findLast) method returns the last item for which callback returned `true`:

```js
const a1 = ["a", 10, "b", 20, "c", 30];
const i = a1.findLast(function (item) {return typeof item === "number"});

console.log(i);
// 30
```

The [`.findIndex()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex) method returns the index of the first item for which callback returned `true`:

```js
const a1 = ["a", 10, "b", 20, "c", 30];
const i = a1.findIndex(function (item) {return typeof item === "number"});

console.log(i);
// 1
```

The [`.findLastIndex()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findLastIndex) method returns the index of the last item for which callback returned `true`:

```js
const a1 = ["a", 10, "b", 20, "c", 30];
const i = a1.findLastIndex(function (item) {return typeof item === "number"});

console.log(i);
// 5
```

The [`.every()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every) method returns `true` if callback returns `true` for every item in the array:

```js
const a1 = [1, 2, 3];
const a2 = [1, "2", 3];


function isNumber(value) {
  return typeof value === "number";
}


console.log(a1.every(isNumber));
// true
console.log(a2.every(isNumber));
// false
```

The [`.some()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some) method returns `true` if callback returns `true` for at least one item in the array:

```js
const a1 = [1, 2, 3];
const a2 = [1, "2", 3];
const a3 = ["1", "2", "3"];


function isNumber(value) {
  return typeof value === "number";
}


console.log(a1.some(isNumber));
// true
console.log(a2.some(isNumber));
// true
console.log(a3.some(isNumber));
// false
```

The [`.reduce()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce) method applies `callback(accumulator, currentValue, currentIndex, array)` for each value in the array for the purpose of reducing the list of items down to a single value. The `.reduce()` method returns the final value returned by `callback` function.

If `initialValue` is specified, then `callback` is called with `initialValue` as the first parameter value and the value of the first item in the array as the second parameter value.

If `initialValue` is not specified, then callback's first two parameter values will be the first and second elements of the array. On *every* subsequent call, the first parameter's value will be whatever `callback` returned on the previous call, and the second parameter's value will be the next value in the array.

If `callback` needs access to the index of the item being processed, or access to the entire array, they are available as optional parameters.

```js
const a = [10, 20, 30];
const total = a.reduce(
  function (accumulator, currentValue) {
    return accumulator + currentValue
  },
  0
);

console.log(total);
// 60
```

The `.reduceRight()` method works like `.reduce()`, but starts with the last element.

`.reduce()` and `.reduceRight()` are the least obvious of the iterative array methods. They should be used for algorithms that combine two values recursively in order to reduce a sequence down to a single value.

### Array Transformation

You can transform back and forth between arrays and other data structures.

#### Grouping the Elements of An Array

The [`Object.groupBy()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/groupBy) method can be used to group the elements of an array, using a test function that returns a string indicating the group of the current element.

To use `Object.groupBy()`, you supply a callback function that is called with the current element, and optionally the current index and array, and returns a string indicating the group of the element.

```js
const inventory = [
  {name: "cabbage", type: "vegetables"},
  {name: "plantains", type: "fruit"},
  {name: "salmon", type: "meat"},
  {name: "blueberries", type: "fruit"}
];

const result = Object.groupBy(
  inventory,
  function ({type}) {return type}
);

console.log(result);
/*
{
  fruit: [
    { name: 'plantains', type: 'fruit' },
    { name: 'blueberries', type: 'fruit' }
  ],
  meat: [{ name: 'salmon', type: 'meat' }],
  vegetables: [{ name: 'cabbage', type: 'vegetables' }]
}
*/
```

Above, [object destructuring syntax for function arguments](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment#unpacking_properties_from_objects_passed_as_a_function_parameter) is used to unpack the `type` element from the passed object. The result is an object that has properties named after the unique strings returned by the callback. Each property is assigned an array containing the elements in the group.

Note that the returned object references the *same* elements as the original array (not [deep copies](https://developer.mozilla.org/en-US/docs/Glossary/Deep_copy)). Changing the internal structure of these elements will be reflected in both the original array and the returned object.

If you cannot use a string as the key, for example, if the information to group is associated with an object that might change, then you can instead use [`Map.groupBy()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/groupBy). This is very similar to `Object.groupBy()` except that it groups the elements of the array into a [`Map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map) that can use an arbitrary value ([object](https://developer.mozilla.org/en-US/docs/Glossary/Object) or [primitive](https://developer.mozilla.org/en-US/docs/Glossary/Primitive)) as a key.

### Sparse Arrays

Arrays can contain *empty slots*, which are not the same as slots filled with the value `undefined`. Empty slots can be created in one of the following ways:

```js
// Array constructor:
const a = Array(5);  // [ <5 empty items> ]

// Consecutive commas in array literal:
const b = [1, 2, , , 5];  // [ 1, 2, <2 empty items>, 5 ]

// Directly setting a slot with index greater than array.length:
const c = [1, 2];
c[4] = 5;  // [ 1, 2, <2 empty items>, 5 ]

// Elongating an array by directly setting .length:
const d = [1, 2];
d.length = 5;  // [ 1, 2, <3 empty items> ]

// Deleting an element:
const e = [1, 2, 3, 4, 5];
delete e[2];  // [ 1, 2, <1 empty item>, 4, 5 ]
```

In some operations, empty slots behave as if they are filled with undefined:

```js
const arr = [1, 2, , , 5];  // Create a sparse array

// Indexed access
console.log(arr[2]);
// undefined

// For...of
for (const i of arr) {
  console.log(i);
}
/*
1
2
undefined
undefined
5
undefined
*/

// Spreading
const another = [...arr];
// another is Array(5) [ 1, 2, undefined, undefined, 5 ]
```

In other operations (most notably array iteration methods), empty slots are skipped:

```js
const mapped = arr.map(function (i) {return i + 1});
// mapped is Array(5) [ 2, 3, <2 empty slots>, 6 ]

arr.forEach(function (i) {return console.log(i)});
/*
1
2
5
*/

const filtered = arr.filter(function () {return true});
// filtered is Array(3) [ 1, 2, 5 ]

const hasFalsy = arr.some(function (k) {return !k});
// hasFalsy is false

// Property enumeration
const keys = Object.keys(arr);
// keys is [ "0", "1", "4" ]
for (const key in arr) {
  console.log(key);
}
/*
0
1
4
*/

// Spreading into an object uses property enumeration, not the array's iterator
const objectSpread = { ...arr };
// objectSpread is Object { 0: 1, 1: 2, 4: 5 }
```

For a complete list of how array methods behave with sparse arrays, see [the `Array` reference page](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#array_methods_and_empty_slots).

### Multi-Dimensional Arrays

Arrays can be nested, meaning that an array can contain another array as an element. Using this characteristic of JavaScript arrays, multi-dimensional arrays can be created.

The following code creates a two-dimensional array:

```js
const a = new Array(4);

for (let i = 0; i < 4; i++) {
  a[i] = new Array(4);

  for (let j = 0; j < 4; j++) {
    a[i][j] = `[${i}, ${j}]`;
  }
}
```

This example creates an array with the following rows:

```text
Row 0: [0, 0] [0, 1] [0, 2] [0, 3]
Row 1: [1, 0] [1, 1] [1, 2] [1, 3]
Row 2: [2, 0] [2, 1] [2, 2] [2, 3]
Row 3: [3, 0] [3, 1] [3, 2] [3, 3]
```

### Using Arrays to Store Other Properties

Arrays can be used like objects to store related information:

```js
const arr = [1, 2, 3];

arr.property = "value";

console.log(arr.property);
// value
```

For example, when an array is the result of a match between a regular expression and a string, the array returns properties and elements that provide information about the match. An array is the return value of [`RegExp.prototype.exec()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/exec), [`String.prototype.match()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/match), and [`String.prototype.split()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split).

### Working With Array-Like Objects

Some JavaScript objects (e.g., the [`NodeList`](https://developer.mozilla.org/en-US/docs/Web/API/NodeList) returned by [`document.getElementsByTagName()`](https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementsByTagName) or the [`arguments`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments) object made available within the body of a function), look and behave like arrays on the surface, but do not share all of their methods. The `arguments` object provides a [`.length`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/length) attribute, but does not implement array methods like [`.forEach()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach).

Array methods cannot be called directly on array-like objects:

```js
function printArguments() {
  arguments.forEach(
    function (item) {
      console.log(item);
    }
  );
  // TypeError: arguments.forEach is not a function
}
```

You can indirectly call array methods on array-like objects:

```js
function printArguments() {
  Array.prototype.forEach.call(
    arguments,
    function (item) {
      console.log(item);
    }
  );
}
```

Array prototype methods can be used on strings, as well, since they provide sequential access to their characters in a similar way to arrays:

```js
Array.prototype.forEach.call(
  "a string",
  function (chr) {
    console.log(chr);
  }
);
```

## Keyed Collections

Keyed collections are collections of data that are indexed by a key. `Map` and `Set` objects contain elements that are iterable in the order of insertion.

### Maps

#### `Map` Object

A [`Map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map) object is a key/value map that can iterate its elements in insertion order.

```js
const sayings = new Map(
  [
    ["dog", "woof"],
    ["cat", "meow"],
    ["elephant", "toot"]
  ]
);

sayings.size;  // 3

sayings.get("dog");  // "woof"
sayings.get("fox");  // undefined
sayings.has("bird");  // false
sayings.delete("dog");  // true
sayings.has("dog");  // false

for (const [key, value] of sayings) {
  console.log(`${key} goes ${value}`);
}
/*
cat goes meow
elephant goes toot
*/

sayings.clear();
sayings.size;  // 0
```

#### `Object` and `Map` Compared

Traditionally, [objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) have been used to map strings to values. Objects allow you to set keys to values, retrieve those values, delete keys, and detect whether something is stored at a key. `Map` objects, however, have a few more advantages that make them better maps.

- The keys of an `Object` are [strings](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) or [symbols](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol), whereas they can be of any value for a `Map`.
- You can easily get the `size` of a `Map`, while you have to manually keep track of size for an `Object`.
- The iteration of maps is in insertion order of the elements.
- An Object has a prototype, so there are default keys in the map (this can be bypassed using `map = Object.create(null)`).

These three tips can help you to decide whether to use a `Map` or an `Object`:

1. Use maps over objects when keys are unknown until run time, and when all keys are the same type and all values are the same type.
2. Use maps if there is a need to store primitive values as keys because object treats each key as a string, whether it is a number value, boolean value, or any other primitive value.
3. Use objects when there is logic that operates on individual elements.

#### `WeakMap` Object

A [`WeakMap`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap) is a collection of key/value pairs whose keys must be objects or non-registered symbols, with values of any arbitrary [JavaScript type](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures), and which does not create strong references to its keys. That is, an object's presence as a key in a `WeakMap` does not prevent the object from being garbage collected.

Once an object used as a key has been collected, its corresponding values in any `WeakMap` become candidates for garbage collection, as well, as long as they are not strongly referred to elsewhere. The only primitive type that can be used as a `WeakMap` key is symbol (more specifically, non-registered symbols) because non-registered symbols are guaranteed to be unique and cannot be re-created.

Essentially, the `WeakMap` API is the same as the Map API. However, a `WeakMap` does not allow observing the liveness of its keys, which is why it does not allow enumeration. So, there is no method to obtain a list of the keys in a `WeakMap`. If there were, the list would depend on the state of garbage collection, thus introducing non-determinism.

For more information and example code, refer to the [`WeakMap` reference page](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap#why_weakmap).

One use case of WeakMap objects is to store private data for an object, or to hide implementation details. The private data and methods belong inside the object and are stored in the `privates` object, which is a `WeakMap`. Everything exposed on the instance and prototype is public. Everything else is inaccessible from the outside world because `privates` is not exported from the module.

```js
const privates = new WeakMap();


function Public() {
  const me = {
    // Private data goes here
  };
  privates.set(this, me);
}


Public.prototype.method = function () {
  const me = privates.get(this);
  // Do stuff with private data in `me`
  // ...
};

module.exports = Public;
```

### Sets

#### `Set` Object

[`Set`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set) objects are collections of unique values. You can iterate its elements in insertion order. A value in a `Set` may only occur once. It is unique in the `Set`'s collection.

```js
const exSet = new Set([1, "example text", "foo"]);

exSet.has(1);  // true
exSet.delete("foo");  // true
exSet.size;  // 2

for (const item of exSet) {
  console.log(item);
}
/*
1
example text
*/
```

#### Converting Between `Array` and `Set`

You can create an [`Array`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array) from a `Set` using [`Array.from()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from) or the [spread syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax). Also, the `Set` constructor accepts an `Array` to convert in the other direction.

```js
exSet = new Set([1, 2, 3, 4]);

Array.from(exSet);
[...exSet];
```

#### `Array` and `Set` Compared

Traditionally, a set of elements has been stored in arrays in JavaScript in a lot of situations. However, the `Set` object has some advantages:

- Deleting `Array` elements by value (`arr.splice(arr.indexOf(val), 1)`) is very slow.
- `Set` objects let you delete elements by their value. With an array, you would have to `splice` based on an element's index.
- The value [`NaN`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NaN) cannot be found with `indexOf` in an array.
- `Set` objects store unique values. You do not have to manually keep track of duplicates.

#### `WeakSet` Object

[`WeakSet`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakSet) objects are collections of garbage-collectable values, including objects and [non-registered symbols](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol#shared_symbols_in_the_global_symbol_registry). A value in the `WeakSet` may only occur once. It is unique in the `WeakSet`'s collection.

The main differences to the `Set` object are:

- `WeakSet`s are **collections of *objects or symbols only***, and not of arbitrary values of any type.
- The `WeakSet` is *weak*. References to objects in the collection are held weakly. If there is no other reference to an object stored in the `WeakSet`, they can be garbage collected. That also means that there is no list of current objects stored in the collection.
- `WeakSet`s are not enumerable.

The use cases of `WeakSet` objects are limited. They will not leak memory. For example, it can be safe to use DOM elements as a key and mark them for tracking purposes.

### Key and Value Equality of `Map` and `Set`

Both the key equality of `Map` objects and the value equality of `Set` objects are based on the [SameValueZero algorithm](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness#same-value-zero_equality):

- Equality works like the identity comparison operator `===`.
- `-0` and `+0` are considered equal.
- `NaN` is considered equal to itself (contrary to `===`).

## Working With Objects

JavaScript is designed on an object-based paradigm. An object is a collection of [properties](https://developer.mozilla.org/en-US/docs/Glossary/Property/JavaScript), and a property is an association between a name (or *key*) and a value. A property's value can be a function, in which case the property is known as a [method](https://developer.mozilla.org/en-US/docs/Glossary/Method).

Objects in JavaScript, just as in many other programming languages, can be compared to objects in real life. In JavaScript, an object is a standalone entity, with properties and type.

In addition to objects that are predefined in the browser, you can define your own objects.

### Creating New Objects

You can create an object using an [object initializer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer). Alternatively, you can first create a constructor function and then instantiate an object by invoking that function with the `new` operator.

#### Using Object Initializers

Object initializers are also called *object literals*. The term *object initializer* is consistent with the terminology used by C++.

The syntax for an object using an object initializer is:

```js
const obj = {
  property1: value1,  // Property name may be an identifier
  2: value2,  // Or a number
  "property n": value3,  // Or a string
};
```

Each property name before colons is an identifier (either a name, number, or string literal), and each `valueN` is an expression whose value is assigned to the property name. The property name can also be an expression. Computed keys need to be wrapped in square brackets.

If you do not need to refer to an object elsewhere, you do not need to assign it to a variable. Note that you may need to wrap the object literal in parentheses if the object appears where a statement is expected, so as not to have the literal be confused with a block statement.

Object initializers are expressions, and each object initializer results in a new object being created whenever the statement in which it appears is executed. Identical object initializers create distinct objects that do not compare to each other as equal.

Objects created with initializers are called *plain objects* because they are instances of [`Object`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object), but not any other object type. Some object types have special initializer syntaxes, e.g., [array initializers](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_types#array_literals) and [regex literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions#creating_a_regular_expression).

#### Using a Constructor Function

Alternatively, you can create an object with these two steps:

1. Define the object type by writing a constructor function. There is a strong convention to use a capital initial letter.
2. Create an instance of the object with [`new`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/new).

To define an object type, create a function for the object type that specifies its name, properties, and methods:

```js
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}


const exCar = new Car("Toyota", "Corolla", 2024);
```

`this` is used to assign values to the object's properties based on the values passed to the function. The order of the arguments in the constructor function's call should match the parameters in the constructor function's definition.

An object can have a property that is itself another object.

You can always add a property to a previously defined object:

```js
exCar.color = "white";
```

To add the new property to *all* new objects of the same type, you have to add the property to the constructor function definition.

You can also use the [`class`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes) syntax to define a constructor function.

#### Using the `Object.create()` Method

Objects can also be created using the [`Object.create()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create) method. This method can be useful because it allows you to choose the [prototype](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain) object for the object you want to create, without having to define a constructor function.

```js
// Animal properties and method encapsulation
const Animal = {
  type: "Invertebrates",
  displayType: function () {
    console.log(this.type);
  }
};

// Create new animal type called animal1
const animal1 = Object.create(Animal);
animal1.displayType();
// Invertebrates

// Create new animal type called fish
const fish = Object.create(Animal);
fish.type = "Fishes";
fish.displayType();
// Fishes
```

### Objects and Properties

A JavaScript object has properties associated with it. Object properties are basically the same as variables, except that they are associated with objects, not [scopes](https://developer.mozilla.org/en-US/docs/Glossary/Scope). The properties of an object define the characteristics of the object.

Like JavaScript variables, property names are case sensitive. Property names can only be strings or symbols. All keys are [converted to strings](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String#string_coercion), unless they are symbols. [Array indices](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#array_indices) are, in fact, properties with string keys that contain integers.

#### Accessing Properties

You can access a property of an object by its property name. [Property accessors](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Property_accessors) come in two syntaxes:

1. dot notation
2. bracket notation

```js
// Dot notation
exCar.make = "Ford";
exCar.model = "Mustang";
exCar.year = 1969;

// Bracket notation
exCar["make"] = "Ford";
exCar["model"] = "Mustang";
exCar["year"] = 1969;
```

An object property name can be any JavaScript string or symbol, including an empty string. However, you cannot use dot notation to access a property whose name is not a valid JavaScript identifier. The bracket notation is also useful when property names are to be dynamically determined, i.e. not determinable until runtime.

```js
const exObj = {};
const str = "exString";
const rand = Math.random();
const anotherObj = {};

// Create additional properties on exObj
exObj.type = "Dot syntax for a key named type";
exObj["date created"] = "This key has a space";
exObj[str] = "This key is in variable str";
exObj[rand] = "A random number is the key here";
exObj[anotherObj] = "This key is object anotherObj";
exObj[""] = "This key is an empty string";

console.log(exObj);
/*
Object { type: "Dot syntax for a key named type", "date created": "This key has a space", exString: "This key is in variable str", "0.03672070381534087": "A random number is the key here", "[object Object]": "This key is object anotherObj", "": "This key is an empty string" }
*/
console.log(exObj.exString);
// This key is in variable str
```

Beware of using square brackets to access properties whose names are given by external input. This may make your code susceptible to [object injection attacks](https://github.com/eslint-community/eslint-plugin-security/blob/main/docs/the-dangers-of-square-bracket-notation.md).

Nonexistent properties of an object have value `undefined` (and not `null`).

#### Enumerating Properties

There are three native ways to list/traverse object properties:

1. [`for...in`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in) loops. This method traverses all of the enumerable string properties of an object, as well as its prototype chain.
2. [`Object.keys()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys). This method returns an array with only the enumerable own string property names (keys) in the object `exObj`, but not those in the prototype chain.
3. [`Object.getOwnPropertyNames()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames). This method returns an array containing all the own string property names in the object `exObj`, regardless of if they are enumerable or not.

You can use the bracket notation with `for...in` to iterate over all the enumerable properties of an object:

```js
function showProps(obj, objName) {
  let result = "";

  for (const i in obj) {
    if (Object.hasOwn(obj, i)) {
      result += `${objName}.${i} = ${obj[i]}\n`;
    }
  }

  console.log(result);
}
```

The term *own property* refers to the properties of the object, but excluding those of the prototype chain.

There is no native way to list inherited non-enumerable properties. However, this can be achieved with the following function:

```js
function listAllProperties(exObj) {
  let objectToInspect = exObj;
  let result = [];

  while (objectToInspect !== null) {
    result = result.concat(Object.getOwnPropertyNames(objectToInspect));
    objectToInspect = Object.getPrototypeOf(objectToInspect);
  }

  return result;
}
```

For more information, see [Enumerability and ownership of properties](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Enumerability_and_ownership_of_properties).

#### Deleting Properties

You can remove a non-inherited property using the delete operator:

```js
// Creates a new object, exObj, with two properties, a and b.
const exObj = new Object();
exObj.a = 5;
exObj.b = 12;

// Removes the a property, leaving exObj with only the b property.
delete exObj.a;
console.log("a" in exObj);
// false
```

### Inheritance

All objects in JavaScript inherit from at least one other object. The object being inherited from is known as the prototype, and the inherited properties can be found in the `prototype` object of the constructor.

See [Inheritance and the prototype chain](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain) for more information.

#### Defining Properties For All Objects of One Type

You can add a property to all objects created through a certain constructor using the [`prototype`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/prototype) property. This defines a property that is shared by all objects of the specified type, rather than by just one instance of the object.

```js
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}


car1 = new Car("Toyota", "Prius", "2024")
Car.prototype.color = "black";
console.log(car1.color);
// "black"
```

### Defining Methods

A *method* is a function associated with an object, or, put differently, a method is a property of an object that is a function. Methods are defined the way normal functions are defined, except that they have to be assigned as the property of an object.

```js
objectName.methodName = functionName;

const exObj = {
  exMethod: function (params) {
    // Do something
  },
  // This works too
  exOtherMethod(params) {
    // Do something else
  },
};
```

You can then call the method in the context of the object as follows:

```js
objectName.methodName(params);
```

Methods are typically defined on the prototype object of the constructor, so that all objects of the same type share the same method.

```js
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}


car1 = new Car("Toyota", "Corolla", "2023");

Car.prototype.displayCar = function () {
  const result = `A beautiful ${this.year} ${this.make} ${this.model}`;
  console.log(result);
};

car1.displayCar();
// A beautiful 2023 Toyota Corolla
```

#### Using `this` For Object References

JavaScript has a special keyword, [`this`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this), that you can use within a method to refer to the current object.

```js
const Manager = {
  name: "Delenn",
  age: 37,
  job: "Software Engineer",
};
const Intern = {
  name: "Stephen",
  age: 21,
  job: "Software Engineer Intern",
};


function sayHi() {
  console.log(`Hello, my name is ${this.name}.`);
}


// Add sayHi() method to both objects
Manager.sayHi = sayHi;
Intern.sayHi = sayHi;

Manager.sayHi();
// Hello, my name is Delenn.
Intern.sayHi();
// Hello, my name is Stephen.
```

`this` is a *hidden parameter* of a function call that is passed in by specifying the object before the function that was called. For example, in `Manager.sayHi()`, `this` is the `Manager` object, because `Manager` comes before the method `sayHi()`.

If you access the same function from another object, `this` will change, as well. If you use other methods to call the function, like [`Function.prototype.call()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call) or [`Reflect.apply()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/apply), you can explicitly pass the value of `this` as an argument.

### Defining Getters and Setters

A [getter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get) is a function associated with a property that gets the value of a specific property. A [setter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/set) is a function associated with a property that sets the value of a specific property. Together, they can indirectly represent the value of a property.

Getters and setters can be either:

- Defined within [object initializers](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_objects#using_object_initializers)
- Added later to any existing object

Within object initializers, getters and setters are defined like regular [methods](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Method_definitions), but prefixed with the keywords `get` or `set`. The getter method must not expect a parameter, while the setter method expects exactly one parameter (i.e., the new value to set).

```js
const exObj = {
  a: 7,
  get b() {
    return this.a + 1;
  },
  set c(x) {
    this.a = x / 2;
  },
};

console.log(exObj.a);
// 7
console.log(exObj.b);
// 8, returned from the .b() method
exObj.c = 50;  // Calls the .c() method
console.log(exObj.a);
// 25
```

Getters and setters can also be added to an object at any time after creation using the [`Object.defineProperties()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperties) method. This method's first parameter is the object on which you want to define the getter or setter. The second parameter is an object whose property names are the getter or setter names, and whose property values are objects for defining the getter or setter functions.

```js
const exObj = {a: 0};

Object.defineProperties(
  exObj,
  {
    b: {get() {return this.a + 1;}},
    c: {set(x) {this.a = x / 2;}},
  }
);

exObj.c = 10;  // Runs the setter, which assigns 10 / 2 (5) to the `a` property
console.log(exObj.b);
// Runs the getter, which yields a + 1, that is, 6
```

Which of the two forms to choose depends on your programming style and the task at hand. If you can change the definition of the original object, you will probably define getters and setters through the original initializer. This form is more compact and natural.

However, if you need to add getters and setters later, maybe because you did not write the particular object, then the second form is your only option. The second form better represents the dynamic nature of JavaScript, but it can make the code hard to read and understand.

### Comparing Objects

In JavaScript, objects are a reference type. Two distinct objects are never equal, even if they have the same properties. Only comparing the same object reference with itself yields true.

```js
// Two variables, two distinct objects with the same properties
const fruit = {name: "pineapple"};
const anotherFruit = {name: "pineapple"};

fruit == anotherFruit;  // return false
fruit === anotherFruit;  // return false
```

```js
// Two variables, a single object
const fruit = {name: "pineapple"};
const anotherFruit = fruit;  // Assign fruit object reference to anotherFruit

// Here fruit and anotherFruit are pointing to same object
fruit == anotherFruit;  // return true
fruit === anotherFruit;  // return true

fruit.name = "fig";
console.log(anotherFruit);
// Object { name: "fig" }
```

## Using Classes

JavaScript is a prototype-based language, i.e., an object's behaviors are specified by its own properties and its prototype's properties. However, with the addition of classes, the creation of hierarchies of objects and the inheritance of properties and their values are much more in-line with other object-oriented languages, like Java.

In many other languages, classes (or constructors) are clearly distinguished from objects or instances. In JavaScript, classes are mainly an abstraction over the existing prototypical inheritance mechanism. All patterns are convertible to prototype-based inheritance.

Classes themselves are normal JavaScript values, as well, and have their own prototype chains. In fact, most plain JavaScript functions can be used as constructors, i.e., you use the `new` operator with a constructor function to create a new object.

### Overview of Classes

`Date` is a built-in class of JavaScript:

```js
const bigDay = new Date(2019, 6, 19);

console.log(bigDay.toLocaleDateString());
// 7/19/2019

if (bigDay.getTime() < Date.now()) {
  console.log("Once upon a time...");
}
// Once upon a time...
```

Classes create objects through the `new` operator. Each object has some properties (data or method) added by the class. The class stores some properties (data or method) itself, which are usually used to interact with instances.

There are three key features of classes:

1. Constructor
2. Instance methods and instance fields
3. STatic methods and static fields

### Declaring a Class

Usually, classes are created with *class declarations*:

```js
class ExClass {
  // Class body...
}

```

Within a class body, there are a range of features available.

```
class ExClass {
  // Constructor
  constructor() {
    // Constructor body
  }

  // Instance field
  exField = "foo";

  // Instance method
  exMethod() {
    // exMethod body
  }

  // Static field
  static exStaticField = "bar";

  // Static method
  static exStaticMethod() {
    // exStaticMethod body
  }

  // Static block
  static {
    // Static initialization code
  }

  /* Fields, methods, static fields, and static methods
     all have private forms */
  #exPrivateField = "bar";
}
```

The example above would roughly translate to the following with function constructors:

```js
function ExClass() {
  this.exField = "foo";
  // Constructor body
}


ExClass.exStaticField = "bar";

ExClass.exStaticMethod = function () {
  // exStaticMethod body
};

ExClass.prototype.exMethod = function () {
  // exMethod body
};

(function () {
  // Static initialization code
})();
```

Note that private fields and methods are new features in classes with no trivial equivalent in function constructors.

#### Constructing a Class

After a class has been declared, you can create instances of it using the `new` operator:

```js
const exInstance = new ExClass();
console.log(exInstance.exField);
// foo
exInstance.exMethod();
```

Typical function constructors can both be constructed with `new` and called without `new`. However, attempting to *call* a class without `new` will result in an error:

```js
const exInstance = ExClass();
// Uncaught TypeError: class constructors must be invoked with 'new'
```

#### Class Declaration Hoisting

Unlike function declarations, class declarations are not [hoisted](https://developer.mozilla.org/en-US/docs/Glossary/Hoisting) (or, in some interpretations, hoisted but with the temporal dead zone restriction), which means that you cannot use a class before it is declared.

```js
new ExClass();
/*
Uncaught ReferenceError: can't access lexical declaration 'ExClass' before
initialization
*/

class ExClass {}
```

This behavior is similar to variables declared with `let` and `const`.

#### Class Expressions

Similar to functions, class declarations also have their expression counterparts:

```js
const ExClass = class {
  // Class body...
};
```

Class expressions can have names, as well. The expression's name is only visible to the class's body:

```js
const ExClass = class ExClassLongerName {
  // Class body. Here ExClass and ExClassLongerName point to the same class.
};
new ExClassLongerName();
// Uncaught ReferenceError: ExClassLongerName is not defined
```

### Constructor

The job of a class is to act as a *factory* for objects (i.e., instances). With classes, the instance creation is done by the [constructor](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/constructor).

```js
class Color {
  constructor(r, g, b) {
    // Assign the RGB values as a property of `this`.
    this.values = [r, g, b];
  }
}


const red = new Color(255, 0, 0);
console.log(red);
/*
Object { values: (3) […] }
  values: Array(3) [ 255, 0, 0 ]
*/
```

The above example is mostly equivalent to:

```js
function createColor(r, g, b) {
  return {
    values: [r, g, b],
  };
}
```

The class constructor's syntax is the same as a normal function, which means that you can use other syntaxes, like rest parameters:

```js
class Color {
  constructor(...values) {
    this.values = values;
  }
}

const red = new Color(255, 0, 0);
// Creates an instance with the same shape as above.
```

Each time you call `new`, a different instance is created. Within a class constructor, the value of `this` points to the newly created instance. You can assign properties to it or read existing properties.

The `this` value will be automatically returned as the result of `new`. You are advised to *not* return any value from the constructor. If you return a non-primitive value, it will become the value of the `new` expression, and the [value of `this` is dropped](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/new#description).


### Instance Methods

The power of classes is that they can be used as *templates* that automatically assign methods to instances.

For our `Color` class, we can add a method called `.getRed()`, which returns the red value of the color:

```js
class Color {
  constructor(r, g, b) {
    this.values = [r, g, b];
  }

  getRed() {
    return this.values[0];
  }
}


const red = new Color(255, 0, 0);
console.log(red.getRed());
// 255
```

Without methods, you may be tempted to define the function within the constructor. This works, but this creates a new function every time a `Color` instance is created. Instead, if you use a method, it will be shared between all instances.

A function can be shared between all instances, but still have its behavior differ when different instances call it, because the value of `this` is different. The method is defined on the prototype of all instances, or `Color.prototype`, which is explained in more detail in [Inheritance and the prototype chain](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain).

Similarly, we can create a new method called `.setRed()`, which sets the red value of the color:

```js
class Color {
  constructor(r, g, b) {
    this.values = [r, g, b];
  }

  getRed() {
    return this.values[0];
  }

  setRed(value) {
    this.values[0] = value;
  }
}


const red = new Color(255, 0, 0);
red.setRed(0);
console.log(red.getRed());
// 0
```

### Private Fields

There is a philosophy in object-oriented programming called [encapsulation](https://en.wikipedia.org/wiki/Encapsulation_(computer_programming)). This means that you should not access the underlying implementation of an object, but instead use well-abstracted methods to interact with it.

For example, if we suddenly decided to represent colors as [HSL](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value/hsl) instead:


```js
class Color {
  constructor(r, g, b) {
    this.values = rgbToHSL([r, g, b]);
  }

  getRed() {
    return this.values[0];
  }

  setRed(value) {
    this.values[0] = value;
  }
}


const red = new Color(255, 0, 0);
console.log(red.values[0]);
// 0
// It's not 255 anymore because the H value for pure red is 0
```

The user assumption that `.values` means the RGB value is no longer valid and it may cause their logic to break. So, if you are an implementor of a class, you want to hide the internal data structure of your instance from your user, both to keep the API clean and to prevent the user's code from breaking when you refactor your code. In classes, this is done through [private fields](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/Private_properties).

A private field is an identifier prefixed with `#` (the hash symbol). The hash is an integral part of the field's name, which means a private property can never have name clash with a public property.

In order to refer to a private field anywhere in the class, you must *declare* it in the class body (you cannot create a private property on-the-fly). Apart from this, a private field is pretty much equivalent to a normal property.

```
class Color {
  // Declare: every Color instance has a private field called #values.
  #values;

  constructor(r, g, b) {
    this.#values = [r, g, b];
  }

  getRed() {
    return this.#values[0];
  }

  setRed(value) {
    this.#values[0] = value;
  }
}


const red = new Color(255, 0, 0);
console.log(red.getRed());
// 255
```

Accessing private fields outside the class is an early syntax error. JavaScript can guard against this because `#privateField` is a special syntax, so it can do some static analysis and find all usage of private fields before even evaluating the code.

```
console.log(red.#values);
/*
Uncaught SyntaxError: reference to undeclared private field or method #values
*/
```

Note that code run in the Chromium/Chrome console can access private properties outside the class. This is a DevTools-only relaxation of the JavaScript syntax restriction.

Private fields in JavaScript are hard private, i.e., if the class does not implement methods that expose these private fields, there is absolutely no mechanism to retrieve them from outside the class. This means that you are safe to do any refactors to your class's private fields, as long as the behavior of the exposed methods stay the same.

We can add some more logic in the `.getRed()` and `.setRed()` methods, instead of making them simple pass-through methods:

```
class Color {
  #values;

  constructor(r, g, b) {
    this.#values = [r, g, b];
  }

  getRed() {
    return this.#values[0];
  }

  setRed(value) {
    if (value < 0 || value > 255) {
      throw new RangeError("Invalid R value");
    }
    this.#values[0] = value;
  }
}


const red = new Color(255, 0, 0);
red.setRed(1000);
// Uncaught RangeError: Invalid R value
```

If we leave the `.values` property exposed, our users can easily circumvent that check by assigning to `.values[0]` directly and create invalid colors. However, with a well-encapsulated API, we can make our code more robust and prevent logic errors downstream.

A class method can read the private fields of other instances, as long as they belong to the same class:

```
class Color {
  #values;

  constructor(r, g, b) {
    this.#values = [r, g, b];
  }

  redDifference(anotherColor) {
    return this.#values[0] - anotherColor.#values[0];
  }
}


const red = new Color(255, 0, 0);
const crimson = new Color(220, 20, 60);
red.redDifference(crimson);
// 35
```

Accessing a nonexistent private property throws an error instead of returning `undefined`, like normal properties do. If you do not know if a private field exists on an object and you wish to access it without using `try`/`catch` to handle the error, you can use the [`in`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/in) operator:

```
class Color {
  #values;

  constructor(r, g, b) {
    this.#values = [r, g, b];
  }

  redDifference(anotherColor) {
    if (!(#values in anotherColor)) {
      throw new TypeError("Color instance expected");
    }
    return this.#values[0] - anotherColor.#values[0];
  }
}
```

Keep in mindm the `#` is a special identifier syntax and you cannot use the field name as if it is a string. `"#values"` in `anotherColor` would look for a property name literally called `"#values"`, instead of a private field.

There are some limitations in using private properties. The same name cannot be declared twice in a single class and they cannot be deleted. Both lead to early syntax errors.

```
class BadIdeas {
  #firstName;
  #firstName;  // Syntax error occurs here
  #lastName;

  constructor() {
    delete this.#lastName;  // Also a syntax error
  }
}
```

Methods, [getters, and setters](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_classes#accessor_fields) can be private, as well. They're useful when you have something complex that the class needs to do internally but no other part of the code should be allowed to call.

For example, imagine creating HTML custom elements that should do something complicated when activated. Furthermore, the somewhat complicated things that happen when the element is activated should be restricted to this class, because no other part of the JavaScript will (or should) access it.

```
class Counter extends HTMLElement {
  #xValue = 0;

  constructor() {
    super();
    this.onclick = this.#clicked.bind(this);
  }

  get #x() {
    return this.#xValue;
  }

  set #x(value) {
    this.#xValue = value;
    window.requestAnimationFrame(this.#render.bind(this));
  }

  #clicked() {
    this.#x++;
  }

  #render() {
    this.textContent = this.#x.toString();
  }

  connectedCallback() {
    this.#render();
  }
}


customElements.define("num-counter", Counter);
```

In the example above, pretty much every field and method is private to the class. Thus, it presents an interface to the rest of the code that is essentially just like a built-in HTML element. No other part of the program has the power to affect any of the internals of `Counter`.

### Accessor Fields

Using methods to simply access a property is still somewhat inefficient in JavaScript. *Accessor fields* allow us to manipulate something as if it is an actual property.

```js
class Color {
  constructor(r, g, b) {
    this.values = [r, g, b];
  }

  get red() {
    return this.values[0];
  }

  set red(value) {
    this.values[0] = value;
  }
}


const red = new Color(255, 0, 0);
red.red = 0;
console.log(red.red);
// 0
```

It looks as if the object has a property called `red`, but no such property exists on the instance. There are only two methods, but they are prefixed with `get` and `set`, which allows them to be manipulated as if they were properties.

If a field only has a getter but no setter, it will be effectively read-only:

```js
class Color {
  constructor(r, g, b) {
    this.values = [r, g, b];
  }

  get red() {
    return this.values[0];
  }
}


const red = new Color(255, 0, 0);
red.red = 0;
console.log(red.red);
// 255
```

In strict mode, the `red.red = 0` line will throw a type error. In non-strict mode, the assignment is silently ignored.

### Public Fields

Private fields also have their public counterparts, which allow every instance to have a property. Usually, fields are designed to be independent of the constructor's parameters.

```js
class ExClass {
  luckyNumber = Math.random();
}


console.log(new ExClass().luckyNumber);
// 0.6596834101703819
console.log(new ExClass().luckyNumber);
// 0.649686113111184
```

Public fields are almost equivalent to assigning a property to `this`. For example, the above example can also be converted to:

```js
class ExClass {
  constructor() {
    this.luckyNumber = Math.random();
  }
}
```

### Static Properties

Prefixing utility methods with what they deal with is called *namespacing* and is considered a good practice. For example, in addition to the older, unprefixed `.parseInt()` method, JavaScript also later added the prefixed `Number.parseInt()` method to indicate that it is for dealing with numbers.

[*Static properties*](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/static) are a group of class features that are defined on the class itself, rather than on individual instances of the class. These features include:

- Static methods
- Static fields
- Static getters and setters

Everything also has private counterparts. For example, for our `Color` class, we can create a static method that checks whether a given triplet is a valid RGB value:

```js
class Color {
  static isValid(r, g, b) {
    return (r >= 0 && r <= 255) &&
           (g >= 0 && g <= 255) &&
           (b >= 0 && b <= 255);
  }
}


Color.isValid(255, 0, 0);
// true
Color.isValid(1000, 0, 0);
// false
```

Static properties are very similar to their instance counterparts, except that:

- They are prefixed with `static`
- They are not accessible from instances

```js
console.log(new Color(0, 0, 0).isValid);
// undefined
```

There is also a special construct called a [*static initialization block*](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/Static_initialization_blocks), which is a block of code that runs when the class is first loaded.

```js
class ExClass {
  static {
    ExClass.myStaticProperty = "foo";
  }
}


console.log(ExClass.myStaticProperty);
// foo
```

Static initialization blocks are almost equivalent to immediately executing some code after a class has been declared. The only difference is that they have access to static private properties.

### Extends and Inheritance

A key feature that classes bring about is *inheritance*, which means that one object can *borrow* a large part of another object's behaviors, while overriding or enhancing certain parts with its own logic.

For example, suppose our `Color` class now needs to support transparency. We may be tempted to add a new field that indicates its transparency. However, this means every instance (even the vast majority which are not transparent), will have to have the extra alpha value, which is inelegant. Plus, if the features keep growing, our `Color` class will become bloated and intractable.

Instead, in object-oriented programming, we would create a *derived class*. The derived class has access to all public properties of the parent class. In JavaScript, derived classes are declared with an [`extends`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/extends) clause, which indicates the class it extends from.

```
class Color {
  #values;

  constructor(r, g, b) {
    this.#values = [r, g, b];
  }

  get red() {
    return this.#values[0];
  }

  set red(value) {
    this.#values[0] = value;
  }

  static isValid(r, g, b) {
    return (r >= 0 && r <= 255) &&
           (g >= 0 && g <= 255) &&
           (b >= 0 && b <= 255);
  }
}


class ColorWithAlpha extends Color {
  #alpha;

  constructor(r, g, b, a) {
    super(r, g, b);
    this.#alpha = a;
  }

  get alpha() {
    return this.#alpha;
  }

  set alpha(value) {
    if (value < 0 || value > 1) {
      throw new RangeError("Alpha value must be between 0 and 1");
    }
    this.#alpha = value;
  }
}
```

In the constructor, we are calling `super(r, g, b)`. It is a language requirement to call `super()` before accessing `this`. The `super()` call calls the parent class's constructor to initialize `this`.

A derived class inherits all methods from its parent. For example, although `ColorWithAlpha` does not declare a `get red()` accessor itself, you can still access `red` because this behavior is specified by the parent class:

```js
const color = new ColorWithAlpha(255, 0, 0, 0.5);
console.log(color.red);
// 255
```

Derived classes can also override methods from the parent class. For example, all classes implicitly inherit the `Object` class, which defines some basic methods. Derived class can override a method from a parent class to add or modify functionality:

```
class Color {
  #values;

  constructor(r, g, b) {
    this.#values = [r, g, b];
  }

  get red() {
    return this.#values[0];
  }

  set red(value) {
    this.#values[0] = value;
  }

  static isValid(r, g, b) {
    return (r >= 0 && r <= 255) &&
           (g >= 0 && g <= 255) &&
           (b >= 0 && b <= 255);
  }

  toString() {
    return this.#values.join(", ");
  }
}


console.log(new Color(255, 0, 0).toString());
// 255, 0, 0
```

Within derived classes, you can access the parent class's methods by using `super()`. This allows you to build enhancement methods and avoid code duplication.

```
class ColorWithAlpha extends Color {
  #alpha;

  constructor(r, g, b, a) {
    super(r, g, b);
    this.#alpha = a;
  }

  get alpha() {
    return this.#alpha;
  }

  set alpha(value) {
    if (value < 0 || value > 1) {
      throw new RangeError("Alpha value must be between 0 and 1");
    }
    this.#alpha = value;
  }

  toString() {
    // Call the parent class's toString() and build on the return value
    return `${super.toString()}, ${this.#alpha}`;
  }
}

console.log(new ColorWithAlpha(255, 0, 0, 0.5).toString());
// 255, 0, 0, 0.5
```

When you use `extends`, the static methods inherit from each other, as well, so you can also override or enhance them.

```
class ColorWithAlpha extends Color {
  #alpha;

  constructor(r, g, b, a) {
    super(r, g, b);
    this.#alpha = a;
  }

  get alpha() {
    return this.#alpha;
  }

  set alpha(value) {
    if (value < 0 || value > 1) {
      throw new RangeError("Alpha value must be between 0 and 1");
    }
    this.#alpha = value;
  }

  static isValid(r, g, b, a) {
    // Call the parent class's isValid() and build on the return value
    return super.isValid(r, g, b) && a >= 0 && a <= 1;
  }

  toString() {
    // Call the parent class's toString() and build on the return value
    return `${super.toString()}, ${this.#alpha}`;
  }
}


console.log(ColorWithAlpha.isValid(255, 0, 0, -1));
// false
```

Derived classes do not have access to the parent class's private fields. This is another key aspect to JavaScript private fields being *hard private*. Private fields are scoped to the class body itself and do not grant access to *any* outside code.

```
class ColorWithAlpha extends Color {
  #alpha;

  constructor(r, g, b, a) {
    super(r, g, b);
    this.#alpha = a;
  }

  get alpha() {
    return this.#alpha;
  }

  set alpha(value) {
    if (value < 0 || value > 1) {
      throw new RangeError("Alpha value must be between 0 and 1");
    }
    this.#alpha = value;
  }

  static isValid(r, g, b, a) {
    // Call the parent class's isValid() and build on the return value
    return super.isValid(r, g, b) && a >= 0 && a <= 1;
  }

  toString() {
    // Call the parent class's toString() and build on the return value
    return `${super.toString()}, ${this.#alpha}`;
  }

  log() {
    console.log(this.#values);
    /* Uncaught SyntaxError: reference to undeclared private field
       or method #values */
  }
}
```

A class can only extend from one class. This prevents problems in multiple inheritance like the [diamond problem](https://en.wikipedia.org/wiki/Multiple_inheritance#The_diamond_problem). However, due to the dynamic nature of JavaScript, it is still possible to achieve the effect of multiple inheritance through class composition and [mixins](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/extends#mix-ins).

Instances of derived classes are also instances of the base class:

```js
const color = new ColorWithAlpha(255, 0, 0, 0.5);
console.log(color instanceof Color);
// true
console.log(color instanceof ColorWithAlpha);
// true
```

### Why Classes?

Classes introduce a paradigm, or a way to organize your code. Classes are the foundations of object-oriented programming (OOP), which is built on concepts like [inheritance](https://en.wikipedia.org/wiki/Inheritance_(object-oriented_programming)) and [polymorphism](https://en.wikipedia.org/wiki/Polymorphism_(computer_science)).

Some developers are philosophically against certain OOP practices and avoid classes as a result. For example mutability and internal state are important aspects of object-oriented programming, but often make code hard to reason with, i.e., any seemingly innocent operation may have unexpected side effects and change the behavior in other parts of the program.

In order to reuse code, we usually resort to extending classes, which can create large hierarchies of inheritance patterns. However, it is often hard to cleanly describe inheritance when one class can only extend one other class. Often, we want the behavior of multiple classes.

In Java, this is done through interfaces. In JavaScript, it can be done through mixins. Regardless, it is still not convenient.

Classes remain a powerful way to organize code on a higher level. In general, you should consider using classes when you want to create objects that store their own internal data and expose a lot of behavior.

## Using Promises

A [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) is an object representing the eventual completion or failure of an asynchronous operation. Essentially, a promise is a returned object to which you attach callbacks, instead of passing callbacks into a function.

For example, imagine a function, `createAudioFileAsync()`, which asynchronously generates a sound file given a configuration record and two callback functions: one called if the audio file is successfully created, and the other called if an error occurs.

```js
function successCallback(result) {
  console.log(`Audio file ready at URL: ${result}`);
}


function failureCallback(error) {
  console.error(`Error generating audio file: ${error}`);
}


createAudioFileAsync(audioSettings, successCallback, failureCallback);
```

If `createAudioFileAsync()` were rewritten to return a promise, you would attach your callbacks to it instead:

```js
createAudioFileAsync(audioSettings).then(successCallback, failureCallback);
```

This convention has several advantages.

### Chaining

A common need is to execute two or more asynchronous operations back to back, where each subsequent operation starts when the previous operation succeeds, with the result from the previous step.

With promises, we accomplish this by creating a promise chain. The API design of promises makes this great because callbacks are attached to the returned promise object, instead of being passed into a function (i.e., this helps avoid [callback hell](http://callbackhell.com/)).

The `.then()` method returns a **new promise**, different from the original:

```js
const promise = doSomething();
const promise2 = promise.then(successCallback, failureCallback);
```

This second promise (`promise2`) represents the completion not just of `doSomething()`, but also of the `successCallback` or `failureCallback` you passed in, which can be other asynchronous functions returning a promise. When that is the case, any callbacks added to `promise2` get queued behind the promise returned by either `successCallback` or `failureCallback`.

The following template can be used to create any function returning a promise:

```js
function doSomething() {
  return new Promise(
    function (resolve) {
      setTimeout(
        function () {
          // Other things to do before completion of the promise
          console.log("Did something");
          // The fulfillment value of the promise
          resolve("https://example.com/");
        },
        200
      );
    }
  );
}
```

With this pattern, you can create longer chains of processing, where each promise represents the completion of one asynchronous step in the chain. In addition, the arguments to `.then()` are optional, and `.catch(failureCallback)` is short for `.then(null, failureCallback)`, so if your error handling code is the same for all steps, you can attach it to the end of the chain:

```js
doSomething()
  .then(function (result) {return doSomethingElse(result);})
  .then(function (newResult) {return doThirdThing(newResult);})
  .then(
    function (finalResult) {
      console.log(`Got the final result: ${finalResult}`);
    }
  )
  .catch(failureCallback);
```

`doSomethingElse` and `doThirdThing` can return any value. If they return promises, that promise is first waited until it settles, and the next callback receives the fulfillment value, not the promise itself.

It is important to always return promises from `.then()` callbacks, even if the promise always resolves to `undefined`. If the previous handler started a promise but did not return it, there is no way to track its settlement anymore, and the promise is said to be *floating*.

```js
doSomething()
  .then(
    function (url) {
      // Missing `return` keyword in front of fetch(url).
      fetch(url);
    }
  )
  .then(
    function (result) {
      /* result is undefined because nothing is returned from the previous
         handler. There is no way to know the return value of the fetch()
         call anymore, or whether it succeeded at all. */
    }
  );
```

By returning the result of the `fetch()` call (which is a promise), we can both track its completion and receive its value when it completes:

```js
doSomething()
  .then(
    function (url) {
      // `return` keyword added
      return fetch(url);
    }
  )
  .then(
    function (result) {
      // result is a Response object
    }
  );
```

Floating promises could be worse if you have race conditions, e.g., if the promise from the last handler is not returned, the next `.then()` handler will be called early, and any value it reads may be incomplete. Therefore, as a rule of thumb, whenever your operation encounters a promise, return it and defer its handling to the next `.then()` handler:

```js
doSomething()
  .then(function (url) {return fetch(url)})
  .then(function (res) {return res.json()})
  .then(
    function (data) {
      listOfIngredients.push(data);
    }
  )
  .then(
    function () {
      console.log(listOfIngredients);
    }
  );
```

Using [`async`/`await`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function) can help you write code that is more intuitive and resembles synchronous code. Below is the same example using `async`/`await`:

```js
async function logIngredients() {
  const url = await doSomething();
  const res = await fetch(url);
  const data = await res.json();
  listOfIngredients.push(data);
  console.log(listOfIngredients);
}
```

Note how the code looks exactly like synchronous code, except for the `await` keywords in front of promises.

`async`/`await` builds on promises. For example, `doSomething()` is the same function as before, so there is minimal refactoring needed to change from promises to `async`/`await`.

`async`/`await` has the same concurrency semantics as normal promise chains. `await` within one async function does not stop the entire program, only the parts that depend on its value, so other async jobs can still run while the `await` is pending.

### Error Handling

```js
doSomething()
  .then(function (result) {return doSomethingElse(result);})
  .then(function (newResult) {return doThirdThing(newResult);})
  .then(
    function (finalResult) {
      console.log(`Got the final result: ${finalResult}`);
    }
  )
  .catch(failureCallback);
```

In the code above, if there is an exception, the browser will look down the chain for `.catch()` handlers or `onRejected`. This is very much modeled after how synchronous code works:

```js
try {
  const result = syncDoSomething();
  const newResult = syncDoSomethingElse(result);
  const finalResult = syncDoThirdThing(newResult);
  console.log(`Got the final result: ${finalResult}`);
} catch (error) {
  failureCallback(error);
}
```

This symmetry with asynchronous code culminates in the `async`/`await` syntax:

```js
async function foo() {
  try {
    const result = await doSomething();
    const newResult = await doSomethingElse(result);
    const finalResult = await doThirdThing(newResult);
    console.log(`Got the final result: ${finalResult}`);
  } catch (error) {
    failureCallback(error);
  }
}
```

Promises solve a fundamental flaw of callback hell (also known as the callback pyramid of doom) by catching all errors, even thrown exceptions and programming errors. This is essential for functional composition of asynchronous operations. All errors are now handled by the [`.catch()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/catch) method at the end of the chain, and you should almost never need to use `try`/`catch` without using `async`/`await`.

#### Nesting

Simple promise chains are best kept flat without nesting, as nesting can be a result of careless composition.

Nesting is a control structure to limit the scope of `catch` statements. Specifically, a nested `catch` only catches failures in its scope and below, not errors higher up in the chain outside the nested scope. When used correctly, this gives greater precision in error recovery:

```js
doSomethingCritical()
  .then(
    function (result) {
      return doSomethingOptional(result)
        .then(
          function (optionalResult) {
            return doSomethingExtraNice(optionalResult)
          }
        )
        .catch(function (e) {})
    },
  )  // Ignore if optional stuff fails; proceed.
  .then(function () {return moreCriticalStuff()})
  .catch(function (e) {console.error(`Critical failure: ${e.message}`)});
```

The inner error-silencing `catch` handler only catches failures from `doSomethingOptional()` and `doSomethingExtraNice()`, after which the code resumes with `moreCriticalStuff()`. Importantly, if `doSomethingCritical()` fails, its error is caught by the final (outer) `catch` only, and does not get swallowed by the inner `catch` handler.

In `async`/`await`, this code looks like:

```js
async function main() {
  try {
    const result = await doSomethingCritical();
    try {
      const optionalResult = await doSomethingOptional(result);
      await doSomethingExtraNice(optionalResult);
    } catch (e) {
      // Ignore failures in optional steps and proceed.
    }
    await moreCriticalStuff();
  } catch (e) {
    console.error(`Critical failure: ${e.message}`);
  }
}
```

If you do not have sophisticated error handling, you likely do not need nested `.then()` handlers. Instead, use a flat chain and put the error handling logic at the end.

#### Chaining After a Catch

It is possible to chain after a failure, i.e. a `catch`, which is useful to accomplish new actions even after an action failed in the chain.

```js
doSomething()
  .then(
    function () {
      throw new Error("Something failed");
      console.log("Do this");
    }
  )
  .catch(
    function () {
      console.error("Do that");
    }
  )
  .then(
    function () {
      console.log("Do this, no matter what happened before");
    }
  );
```

This will output the following text:

```text
Initial
Do that
Do this, no matter what happened before
```

The text *Do this* is not displayed because the *Something failed* error caused a rejection.

In `async`/`await`, this code looks like:

```js
async function main() {
  try {
    await doSomething();
    throw new Error("Something failed");
    console.log("Do this");
  } catch (e) {
    console.error("Do that");
  }
  console.log("Do this, no matter what happened before");
}
```

#### Promise Rejection Events

If a promise rejection event is not handled by any handler, it bubbles to the top of the call stack, and the host needs to surface it. On the web, whenever a promise is rejected, one of two events is sent to the global scope (generally, this is either the [`window`](https://developer.mozilla.org/en-US/docs/Web/API/Window) or, if being used in a web worker, it is the [`Worker`](https://developer.mozilla.org/en-US/docs/Web/API/Worker) or other worker-based interface).

The two events are:

1. [`unhandledrejection`](https://developer.mozilla.org/en-US/docs/Web/API/Window/unhandledrejection_event) - Sent when a promise is rejected, but there is no rejection handler available.
2. [`rejectionhandled`](https://developer.mozilla.org/en-US/docs/Web/API/Window/rejectionhandled_event) - Sent when a handler is attached to a rejected promise that has already caused an `unhandledrejection` event.

In both cases, the event (of type [`PromiseRejectionEvent`](https://developer.mozilla.org/en-US/docs/Web/API/PromiseRejectionEvent)) has as members a [`.promise`](https://developer.mozilla.org/en-US/docs/Web/API/PromiseRejectionEvent/promise) property indicating the promise that was rejected, and a reason property that provides the [`.reason`](https://developer.mozilla.org/en-US/docs/Web/API/PromiseRejectionEvent/reason) given for the promise to be rejected.

These make it possible to offer fallback error handling for promises, as well as to help debug issues with your promise management. These handlers are global per context, so all errors will go to the same event handlers, regardless of source.

In [Node.js](https://developer.mozilla.org/en-US/docs/Glossary/Node.js), handling promise rejection is slightly different. You capture unhandled rejections by adding a handler for the Node.js `unhandledRejection` event (notice the difference in capitalization of the name):

```js
process.on(
  "unhandledRejection",
  function (reason, promise) {
    // Add code here to examine the `promise` and `reason` values
  }
);
```

For Node.js, to prevent the error from being logged to the console (the default action that would otherwise occur), adding that `process.on()` listener is all that is necessary. There is no need for an equivalent of the browser runtime's [`.preventDefault()`](https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault) method.

However, if you add that process.on listener but do not also have code within it to handle rejected promises, they will be dropped on the floor and silently ignored. So ideally, you should add code within that listener to examine each rejected promise and make sure it was not caused by an actual code bug.

### Composition

There are four [composition tools](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise#promise_concurrency) for running asynchronous operations concurrently:

1. [`Promise.all()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all)
2. [`Promise.allSettled()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/allSettled)
3. [`Promise.any()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/any)
4. [`Promise.race()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/race)


We can start operations at the same time and wait for them all to finish like this:

```js
Promise.all([func1(), func2(), func3()]).then(
  function ([result1, result2, result3]) {
    // Use result1, result2, and result3
  }
);
```

If one of the promises in the array rejects, `Promise.all()` immediately rejects the returned promise and aborts the other operations. This may cause unexpected state or behavior. [`Promise.allSettled()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/allSettled) is another composition tool that ensures all operations are complete before resolving.

These methods all run promises concurrently. A sequence of promises are started simultaneously and do not wait for each other. Sequential composition is possible using some clever JavaScript:

```js
[func1, func2, func3]
  .reduce(function (p, f) {return p.then(f)}, Promise.resolve())
  .then(
    function (result3) {
      // Use result3
    }
  );
```

Above, we [reduce](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce) an array of asynchronous functions down to a promise chain. The code above is equivalent to:

```js
Promise.resolve()
  .then(func1)
  .then(func2)
  .then(func3)
  .then(
    function (result3) {
      // Use result3
    }
  );
```

This can be made into a reusable compose function, which is common in functional programming:

```js
const applyAsync = function (acc, val) {return acc.then(val)};
const composeAsync =
  function(...funcs) {
    function (x) {
      return funcs.reduce(applyAsync, Promise.resolve(x))
    }
  };
```

The `composeAsync()` function accepts any number of functions as arguments and returns a new function that accepts an initial value to be passed through the composition pipeline:

```js
const transformData = composeAsync(func1, func2, func3);
const result3 = transformData(data);
```

Sequential composition can also be done more succinctly with `async`/`await`:

```js
let result;
for (const f of [func1, func2, func3]) {
  result = await f(result);
}
// Use last result (i.e., result3)
```

Before you sequentially compose promises, consider if it is really necessary. It is always better to concurrently run promises so that they do not unnecessarily block each other, unless one promise's execution depends on another's result.

### Cancellation

`Promise` itself has no first-class protocol for cancellation, but you may be able to directly cancel the underlying asynchronous operation, typically using [`AbortController`](https://developer.mozilla.org/en-US/docs/Web/API/AbortController).


### Creating a Promise Around An Old Callback API

A [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) can be created from scratch using its constructor. This should be needed only to wrap old APIs.

In an ideal world, all asynchronous functions would already return promises. Unfortunately, some APIs still expect success and/or failure callbacks to be passed in the old way. The most obvious example is the [`setTimeout()`](https://developer.mozilla.org/en-US/docs/Web/API/Window/setTimeout) function:

```js
setTimeout(
  function () {saySomething("10 seconds passed")},
  10 * 1000
);
```

Mixing old-style callbacks and promises is problematic. If `saySomething()` fails or contains a programming error, nothing catches it. This is intrinsic to the design of `setTimeout()`.

Fortunately, we can wrap `setTimeout()` in a promise. The best practice is to wrap the callback-accepting functions at the lowest possible level, and then never call them directly again:

```js
const wait =
  function (ms) {
    return new Promise(
      function (resolve) {
        return setTimeout(resolve, ms)
      }
    )
  };

wait(10 * 1000)
  .then(
    function () {
      return saySomething("10 seconds")
    }
  )
  .catch(failureCallback);
```

The promise constructor takes an executor function that lets us resolve or reject a promise manually. Since `setTimeout()` does not really fail, we left out reject in this case.

### Timing

#### Guarantees

In the callback-based API, when and how the callback gets called depends on the API implementor. For example, the callback may be called synchronously or asynchronously:

```js
function doSomething(callback) {
  if (Math.random() > 0.5) {
    callback();
  } else {
    setTimeout(() => callback(), 1000);
  }
}
```

The above design is strongly discouraged because it leads to the so-called [*state of Zalgo*](https://blog.izs.me/2013/08/designing-apis-for-asynchrony/). In the context of designing asynchronous APIs, this means a callback is called synchronously in some cases but asynchronously in other cases, creating ambiguity for the caller. This API design makes side effects hard to analyze:

```js
let value = 1;
doSomething(
  function () {
    return value = 2;
  }
);
console.log(value);
// 1 or 2?
```

On the other hand, promises are a form of [inversion of control](https://en.wikipedia.org/wiki/Inversion_of_control). The API implementor does not control when the callback gets called.

Instead, the job of maintaining the callback queue and deciding when to call the callbacks is delegated to the promise implementation, and both the API user and API developer automatically gets strong semantic guarantees, including:

- Callbacks added with [`.then()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then) will never be invoked before the [completion of the current run](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Event_loop#run-to-completion) of the JavaScript event loop.
- These callbacks will be invoked even if they were added *after* the success or failure of the asynchronous operation that the promise represents.
- Multiple callbacks may be added by calling `.then()` several times. They will be invoked one after another, in the order in which they were inserted.

To avoid surprises, functions passed to `.then()` will never be called synchronously, even with an already-resolved promise:

```js
Promise.resolve().then(function () {console.log(2)});
console.log(1);
// Logs: 1, 2
```

Instead of running immediately, the passed-in function is put on a microtask queue, which means it runs later (only after the function which created it exits, and when the JavaScript execution stack is empty), just before control is returned to the event loop; i.e. pretty soon:

```js
const wait = 
  function (ms) {
    return new Promise(
      function (resolve) {
        setTimeout(resolve, ms)
      }
    )
  };

wait(0).then(function () {console.log(4)});
Promise.resolve()
  .then(function () {console.log(2)})
  .then(function () {console.log(3)});
console.log(1);
// 1, 2, 3, 4
```

#### Task Queues Versus Microtasks

Promise callbacks are handled as a [microtask](https://developer.mozilla.org/en-US/docs/Web/API/HTML_DOM_API/Microtask_guide) whereas `setTimeout()` callbacks are handled as task queues.

```js
const promise = new Promise(
  function (resolve, reject) {
    console.log("Promise callback");
    resolve();
  }
).then(
  function (result) {
    console.log("Promise callback (.then)");
  }
);

setTimeout(
  function () {
    console.log("event-loop cycle: Promise (fulfilled)", promise);
  },
  0
);

console.log("Promise (pending)", promise);
```

The code above will output:

```text
Promise callback
Promise (pending) Promise {<pending>}
Promise callback (.then)
event-loop cycle: Promise (fulfilled) Promise {<fulfilled>}
```

For more details, refer to [Tasks vs. microtasks](https://developer.mozilla.org/en-US/docs/Web/API/HTML_DOM_API/Microtask_guide/In_depth#tasks_vs._microtasks).


#### When Promises and Tasks Collide

If you run into situations in which you have promises and tasks (e.g., events or callbacks) that are firing in unpredictable orders, it is possible you may benefit from using a microtask to check status or balance out your promises when promises are conditionally created.

If you think microtasks may help solve this problem, see the [microtask guide](https://developer.mozilla.org/en-US/docs/Web/API/HTML_DOM_API/Microtask_guide) to learn more about how to use [`queueMicrotask()`](https://developer.mozilla.org/en-US/docs/Web/API/Window/queueMicrotask) to enqueue a function as a microtask.

## JavaScript Typed Arrays

JavaScript typed arrays are array-like objects that provide a mechanism for reading and writing raw binary data in memory buffers. Typed arrays are not intended to replace arrays for any kind of functionality. Instead, they provide developers with a familiar interface for manipulating binary data.

Each entry in a JavaScript typed array is a raw binary value in one of a number of supported formats, from 8-bit integers to 64-bit floating-point numbers. Typed array objects share many of the same methods as arrays with similar semantics. However, typed arrays are *not* to be confused with normal arrays. Moreover, not all methods available for normal arrays are supported by typed arrays (e.g., `.push()` and `.pop()`).

To achieve maximum flexibility and efficiency, JavaScript typed arrays split the implementation into *buffers* and *views*. A buffer is an object representing a chunk of data. It has no format to speak of, and offers no mechanism for accessing its contents. In order to access the memory contained in a buffer, you need to use a [view](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Typed_arrays#views). A view provides a *context*, i.e., a data type, starting offset, and number of elements.

### Buffers

There are two types of buffers:

- [`ArrayBuffer`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer)
- [`SharedArrayBuffer`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SharedArrayBuffer)

Both are low-level representations of a memory span. They have *array* in their names, but they do not have much to do with arrays. You cannot read or write to them directly.

Instead, buffers are generic objects that just contain raw data. In order to access the memory represented by a buffer, you need to use a view.

Buffers support the following actions:

- **Allocate** - As soon as a new buffer is created, a new memory span is allocated and initialized to `0`.
- **Copy** - Using the [`.slice()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer/slice) method, you can efficiently copy a portion of the memory without creating views to manually copy each byte.
- **Transfer** - Using the [`.transfer()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer/transfer) and [`.transferToFixedLength()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer/transferToFixedLength) methods, you can transfer ownership of the memory span to a new buffer object. This is useful when transferring data between different execution contexts without copying.

    After the transfer, the original buffer is no longer usable. A `SharedArrayBuffer` cannot be transferred (as the buffer is already shared by all execution contexts).

- **Resize** - Using the [`.resize()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer/resize) method, you can resize the memory span (either claim more memory space, as long as it does not pass the pre-set [`maxByteLength`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer/maxByteLength) limit, or release some memory space). `SharedArrayBuffer` can be [grown](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SharedArrayBuffer/grow), but not shrunk.

The difference between `ArrayBuffer` and `SharedArrayBuffer` is that the former is always owned by a single execution context at a time. If you pass an `ArrayBuffer` to a different execution context, it is *transferred* and the original `ArrayBuffer` becomes unusable. This ensures that only one execution context can access the memory at a time.

A `SharedArrayBuffer` is not transferred when passed to a different execution context, so it can be accessed by multiple execution contexts at the same time. This may introduce race conditions when multiple threads access the same memory span, so operations such as [`Atomics`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Atomics) methods become useful.

### Views

There are two main kinds of views:

- Typed Array Views
- `DataView`

Typed arrays provide [utility methods](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray#instance_methods) that allow you to conveniently transform binary data. `DataView` is more low-level and allows granular control of how data is accessed. The ways to read and write data using the two views are very different.

Both kinds of views cause [`ArrayBuffer.isView()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer/isView) to return `true`. They both have the following properties:

- `buffer` - The underlying buffer that the view references.
- `byteOffset` - The offset, in bytes, of the view from the start of its buffer.
- `byteLength` - The length, in bytes, of the view.

Both constructors accept the above three as separate arguments, although typed array constructors accept `length` as the number of elements rather than the number of bytes.

#### Typed Array Views

Typed array views have self-descriptive names and provide views for all the usual numeric types (e.g., `Int8`, `Uint32`, `Float64`). There is one special typed array view, [`Uint8ClampedArray`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint8ClampedArray), which clamps the values between `0` and `255`. This is useful for [Canvas data processing](https://developer.mozilla.org/en-US/docs/Web/API/ImageData).

<table>
  <thead>
    <tr>
      <th>Type</th>
      <th>Value Range</th>
      <th>Size in bytes</th>
      <th>Web IDL type</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Int8Array"><code>Int8Array</code></a></td>
      <td>-128 to 127</td>
      <td>1</td>
      <td><code>byte</code></td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Uint8Array"><code>Uint8Array</code></a></td>
      <td>0 to 255</td>
      <td>1</td>
      <td><code>octet</code></td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Uint8ClampedArray"><code>Uint8ClampedArray</code></a></td>
      <td>0 to 255</td>
      <td>1</td>
      <td><code>octet</code></td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Int16Array"><code>Int16Array</code></a></td>
      <td>-32768 to 32767</td>
      <td>2</td>
      <td><code>short</code></td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Uint16Array"><code>Uint16Array</code></a></td>
      <td>0 to 65535</td>
      <td>2</td>
      <td><code>unsigned short</code></td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Int32Array"><code>Int32Array</code></a></td>
      <td>-2147483648 to 2147483647</td>
      <td>4</td>
      <td><code>long</code></td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Uint32Array"><code>Uint32Array</code></a></td>
      <td>0 to 4294967295</td>
      <td>4</td>
      <td><code>unsigned long</code></td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Float16Array"><code>Float16Array</code></a></td>
      <td><code>-65504</code> to <code>65504</code></td>
      <td>2</td>
      <td>N/A</td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Float32Array"><code>Float32Array</code></a></td>
      <td><code>-3.4e38</code> to <code>3.4e38</code></td>
      <td>4</td>
      <td><code>unrestricted float</code></td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Float64Array"><code>Float64Array</code></a></td>
      <td><code>-1.8e308</code> to <code>1.8e308</code></td>
      <td>8</td>
      <td><code>unrestricted double</code></td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  BigInt64Array"><code>BigInt64Array</code></a></td>
      <td>-2<sup>63</sup> to 2<sup>63</sup> - 1</td>
      <td>8</td>
      <td><code>bigint</code></td>
    </tr>
    <tr>
      <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  BigUint64Array"><code>BigUint64Array</code></a></td>
      <td>0 to 2<sup>64</sup> - 1</td>
      <td>8</td>
      <td><code>bigint</code></td>
    </tr>
  </tbody>
</table>

All typed array views have the same methods and properties, as defined by the [`TypedArray`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray) class. They only differ in the underlying data type and the size in bytes. Refer to [Value encoding and normalization](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray#value_encoding_and_normalization) for more information.

Typed arrays are, in principle, fixed-length, so array methods that may change the length of an array are not available. This includes `.pop()`, `.push()`, `.shift()`, `.splice()`, and `.unshift()`.

In addition, `.flat` is unavailable because there are no nested typed arrays, and related methods (including `.concat()` and `.flatMap()`) do not have great use cases, so they are unavailable. Since `.splice()` is unavailable, so too is `.toSpliced()`. All other array methods are shared between `Array` and `TypedArray`.

`TypedArray` has the extra `.set()` and `.subarray()` methods that optimize working with multiple typed arrays that view the same buffer. The `.set()` method allows setting multiple typed array indices at once, using data from another array or typed array. If the two typed arrays share the same underlying buffer, the operation may be more efficient as it is a fast memory move. The `.subarray()` method creates a new typed array view that references the same buffer as the original typed array, but with a narrower span.

There is no way to directly change the length of a typed array without changing the underlying buffer. However, when the typed array views a resizable buffer and does not have a fixed `byteLength`, it is *length-tracking*, and will automatically resize to fit the underlying buffer as the resizable buffer is resized. Refer to [Behavior when viewing a resizable buffer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray#behavior_when_viewing_a_resizable_buffer) for details.

Similar to regular arrays, you can access typed array elements using [bracket notation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Property_accessors#bracket_notation). The corresponding bytes in the underlying buffer are retrieved and interpreted as a number. Any property access using a number (or the string representation of a number, since numbers are always converted to strings when accessing properties) will be proxied by the typed array. They never interact with the object itself.

This means that:

- Out-of-bounds index access always returns `undefined`, without actually accessing the property on the object.
- Any attempt to write to such an out-of-bounds property has no effect. It does not throw an error, but does not change the buffer or typed array either.
- Typed array indices appear to be configurable and writable, but any attempt to change their attributes will fail.

```js
const uint8 = new Uint8Array([1, 2, 3]);
console.log(uint8[0]);
// 1

// For illustrative purposes only. Not for production code.
uint8[-1] = 0;
uint8[2.5] = 0;
uint8[NaN] = 0;
console.log(Object.keys(uint8));
// ["0", "1", "2"]
console.log(uint8[NaN]);
// undefined

// Non-numeric access still works
uint8[true] = 0;
console.log(uint8[true]);
// 0

Object.freeze(uint8);
// Uncaught TypeError: can't redefine non-configurable property 0
```

#### `DataView`

The `DataView` is a low-level interface that provides a getter/setter API to read and write arbitrary data to the buffer. This is useful when dealing with different types of data. Typed array views are in the native byte-order (see [Endianness](https://developer.mozilla.org/en-US/docs/Glossary/Endianness)) of your platform.

With a `DataView`, the byte-order can be controlled. By default, it is big-endian. The bytes are ordered from most significant to least significant. This can be reversed, with the bytes ordered from least significant to most significant (little-endian), using getter/setter methods.

`DataView` does not require alignment. Multi-byte read and write can be started at any specified offset. The setter methods work the same way.

The following example uses a `DataView` to get the binary representation of any number:

```js
function toBinary(
  x,
  {type = "Float64", littleEndian = false, separator = " ", radix = 16} = {},
) {
  const bytesNeeded = globalThis[`${type}Array`].BYTES_PER_ELEMENT;
  const dv = new DataView(new ArrayBuffer(bytesNeeded));
  dv[`set${type}`](0, x, littleEndian);
  const bytes = Array.from(
    {length: bytesNeeded},
    function (_, i) {
      return dv.getUint8(i)
               .toString(radix)
               .padStart(8 / Math.log2(radix), "0")
    }
  );
  return bytes.join(separator);
}

console.log(toBinary(1.1));
// 3f f1 99 99 99 99 99 9a
console.log(toBinary(1.1, { littleEndian: true }));
// 9a 99 99 99 99 99 f1 3f
console.log(toBinary(20, { type: "Int8", radix: 2 }));
// 00010100
```

### Web APIs Using Typed Arrays

These are some examples of APIs that make use of typed arrays. There are others, and more are being added all the time.

- [`FileReader.prototype.readAsArrayBuffer()`](https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsArrayBuffer) - The `FileReader.prototype.readAsArrayBuffer()` method starts reading the contents of the specified [`Blob`](https://developer.mozilla.org/en-US/docs/Web/API/Blob) or [`File`](https://developer.mozilla.org/en-US/docs/Web/API/File).
- [`fetch()`](https://developer.mozilla.org/en-US/docs/Web/API/Window/fetch) - The [`body`](https://developer.mozilla.org/en-US/docs/Web/API/RequestInit#body) option to `fetch()` can be a typed array or [`ArrayBuffer`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer), enabling you to send these objects as the payload of a [`POST`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST) request.
- [`ImageData.data`](https://developer.mozilla.org/en-US/docs/Web/API/ImageData) - Is a [`Uint8ClampedArray`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint8ClampedArray) representing a one-dimensional array containing the data in the RGBA order, with integer values between `0` and `255` inclusive.

### Examples

#### Using Views With Buffers

Create a buffer with a fixed length of 16-bytes:

```js
const buffer = new ArrayBuffer(16);
```

At this point, we have a chunk of memory whose bytes are all pre-initialized to `0`. Confirm that the buffer is the right size:

```js
if (buffer.byteLength === 16) {
  console.log("Yes, it is 16 bytes.");
} else {
  console.log("It is the wrong size!");
}
```

Create a view that treats the data in the buffer as an array of 32-bit signed integers:

```js
const int32View = new Int32Array(buffer);
```

Access the fields in the array like a normal array:

```js
for (let i = 0; i < int32View.length; i++) {
  int32View[i] = i * 2;
}
```

This fills out the 4 entries in the array (4 entries at 4 bytes each makes 16 total bytes) with the values `0`, `2`, `4`, and `6`.

#### Multiple Views On the Same Data

We continue building on the last example by creating multiple views on the same data:

```js
const int16View = new Int16Array(buffer);

for (let i = 0; i < int16View.length; i++) {
  console.log(`Entry ${i}: ${int16View[i]}`);
}
```

Here, we create a 16-bit integer view that shares the same buffer as the existing 32-bit view and we output all the values in the buffer as 16-bit integers. Now, we get the output `0`, `0`, `2`, `0`, `4`, `0`, `6`, `0` (assuming little-endian encoding):

```text
Int16Array  |   0  |  0   |   2  |  0   |   4  |  0   |   6  |  0   |
Int32Array  |      0      |      2      |      4      |      6      |
ArrayBuffer | 00 00 00 00 | 02 00 00 00 | 04 00 00 00 | 06 00 00 00 |
```

We can go further:

```js
int16View[0] = 32;
console.log(`Entry 0 in the 32-bit array is now ${int32View[0]}`);
```

The output from this is `Entry 0 in the 32-bit array is now 32`.

In other words, the two arrays are indeed viewed on the same data buffer, treating it as different formats.

```text
Int16Array  |  32  |  0   |   2  |  0   |   4  |  0   |   6  |  0   |
Int32Array  |     32      |      2      |      4      |      6      |
ArrayBuffer | 20 00 00 00 | 02 00 00 00 | 04 00 00 00 | 06 00 00 00 |
```

You can do this with any view type, although if you set an integer and then read it as a floating-point number, you will probably get a strange result because the bits are interpreted differently.

#### Reading Text From a Buffer

Buffers do not always represent numbers. For example, reading a file can give you a text data buffer. You can read this data out of the buffer using a typed array.

The following reads UTF-8 text using the [`TextDecoder`](https://developer.mozilla.org/en-US/docs/Web/API/TextDecoder) web API:

```js
const buffer = new ArrayBuffer(8);
const uint8 = new Uint8Array(buffer);
// Data manually written here, but pretend it was already in the buffer
uint8.set([228, 189, 160, 229, 165, 189]);
const text = new TextDecoder().decode(uint8);
console.log(text);
// "你好"
```

The following reads UTF-16 text using the [`String.fromCharCode()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/fromCharCode) method:

```js
const buffer = new ArrayBuffer(8);
const uint16 = new Uint16Array(buffer);
// Data manually written here, but pretend it was already in the buffer
uint16.set([0x4f60, 0x597d]);
const text = String.fromCharCode(...uint16);
console.log(text);
// "你好"
```

#### Working With Complex Data Structures

By combining a single buffer with multiple views of different types, starting at different offsets into the buffer, you can interact with data objects containing multiple data types. This lets you, for example, interact with complex data structures from [WebGL](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API) or data files.

Consider this C structure:

```js
struct someStruct {
  unsigned long id;
  char username[16];
  float amountDue;
};
```

You can access a buffer containing data in this format like this:

```js
const buffer = new ArrayBuffer(24);

// ... read the data into the buffer ...

const idView = new Uint32Array(buffer, 0, 1);
const usernameView = new Uint8Array(buffer, 4, 16);
const amountDueView = new Float32Array(buffer, 20, 1);
```

Then, you can access the amount due with `amountDueView[0]`.

Keep in mind, the [data structure alignment](https://en.wikipedia.org/wiki/Data_structure_alignment) in a C structure is platform-dependent. Take precautions and considerations for these padding differences.

#### Conversion to Normal Arrays

After processing a typed array, it is sometimes useful to convert it back to a normal array in order to benefit from the [`Array`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array) prototype. This can be done using [`Array.from()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from):

```js
const typedArray = new Uint8Array([1, 2, 3, 4]);
const normalArray = Array.from(typedArray);
```

This can also be done with the [spread syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax):

```js
const typedArray = new Uint8Array([1, 2, 3, 4]);
const normalArray = [...typedArray];
```

## Iterators and Generators

Iterators and generators bring the concept of iteration directly into the core language and provide a mechanism for customizing the behavior of `for...of` loops.

### Iterators

In JavaScript, an iterator is an object that defines a sequence and, potentially, a return value upon its termination. Specifically, an iterator is any object that implements the [Iterator protocol](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols#the_iterator_protocol) by having a `.next()` method that returns an object with two properties:

- `.value` - The next value in the iteration sequence.
- `.done` - This is `true` if the last value in the sequence has already been consumed. If `value` is present alongside `done`, it is the iterator's return value.

Once created, an iterator object can be iterated explicitly by repeatedly calling `.next()`. Iterating over an iterator *consumes* the iterator because it is generally only possible to do once. After a terminating value has been yielded, additional calls to `.next()` should continue to return `{done: true}`.

The most common iterator in JavaScript is the Array iterator, which returns each value in the associated array in sequence.

Keep in mind, arrays must be allocated in their entirety, but iterators are consumed only as necessary. Therefore, iterators can express sequences of unlimited size, such as the range of integers between `0` and [`Infinity`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Infinity).

The following example creates a range iterator that defines a sequence of integers from `start` (inclusive) to `end` (exclusive) spaced `step` apart. Its final return value is the size of the sequence it created, tracked by the variable `iterationCount`.

```js
function makeRangeIterator(start = 0, end = Infinity, step = 1) {
  let nextIndex = start;
  let iterationCount = 0;

  const rangeIterator = {
    next() {
      let result;
      if (nextIndex < end) {
        result = {value: nextIndex, done: false};
        nextIndex += step;
        iterationCount++;
        return result;
      }
      return {value: iterationCount, done: true};
    },
  };
  return rangeIterator;
}


const iter = makeRangeIterator(1, 10, 2);

let result = iter.next();
while (!result.done) {
  console.log(result.value);
  result = iter.next();
}
/*
1
3
5
7
9
*/

console.log("Iterated over sequence of size:", result.value);
// Iterated over sequence of size: 5
```

It is not possible to reflectively know whether a particular object is an iterator. If you need to do this, use [Iterables](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_generators#iterables).

### Generator Functions

Custom iterators are a useful tool, but their creation requires careful programming, due to the need to explicitly maintain their internal state. Generator functions provide a powerful alternative. They allow you to define an iterative algorithm by writing a single function whose execution is not continuous. Generator functions are written using the [`function*`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function*) syntax.

When called, generator functions do not initially execute their code. Instead, they return a special type of iterator, called a *Generator*. When a value is consumed by calling the generator's `.next` method, the Generator function executes until it encounters the `yield` keyword.

The function can be called as many times as desired, and returns a new Generator each time. Each Generator may only be iterated once.

```js
function* makeRangeIterator(start = 0, end = Infinity, step = 1) {
  let iterationCount = 0;
  for (let i = start; i < end; i += step) {
    iterationCount++;
    yield i;
  }
  return iterationCount;
}


const iter = makeRangeIterator(1, 10, 2);

let result = iter.next();
while (!result.done) {
  console.log(result.value);
  result = iter.next();
}
/*
1
3
5
7
9
*/

console.log("Iterated over sequence of size:", result.value);
// Iterated over sequence of size: 5
```

### Iterables

An object is *iterable* if it defines its iteration behavior, such as what values are looped over in a `for...of` construct. Some built-in types, such as `Array` or `Map`, have a default iteration behavior, while other types (such as `Object`) do not.

In order to be iterable, an object must implement the `[Symbol.iterator]()` method. This means that the object (or one of the objects up its prototype chain) must have a property with a [`Symbol.iterator`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol/iterator) key.

It may be possible to iterate over an iterable more than once, or only once. It is up to the programmer to know which is the case.

Iterables that can iterate only once (e.g., Generators) customarily return this from their `[Symbol.iterator]()` method, whereas iterables that can be iterated many times must return a new iterator on each invocation of `[Symbol.iterator]()`.

```js
function* makeIterator() {
  yield 1;
  yield 2;
}


const iter = makeIterator();
for (const itItem of iter) {
  console.log(itItem);
}
/*
1
2
*/
console.log(iter[Symbol.iterator]() === iter);
// true

iter[Symbol.iterator] = function* () {
  yield 1;
  yield 2;
};
// function* Symbol.iterator()

/*
This example show us generator(iterator) is an iterable object,
which has the [Symbol.iterator]() method return the `iter` (itself),
and consequently, the iterator object can iterate only once.

If we change the [Symbol.iterator]() method of `iter` to a function/generator,
which returns a new iterator/generator object, `iter` can iterate many times.
*/
```

#### User-Defined Iterables


You can make your own iterables:

```js
const myIterable = {
  *[Symbol.iterator]() {
    yield 1;
    yield 2;
    yield 3;
  },
};
```

User-defined iterables can be used in `for...of` loops or the spread syntax, as usual.

```js
const myIterable = {
  *[Symbol.iterator]() {
    yield 1;
    yield 2;
    yield 3;
  },
};

for (const value of myIterable) {
  console.log(value);
}
/*
1
2
3
*/

[...myIterable];
// Array(3) [ 1, 2, 3 ]
```

#### Built-In Iterables

`String`, `Array`, `TypedArray`, `Map`, and `Set` are all built-in iterables because their prototype objects all have a `Symbol.iterator` method.

#### Syntaxes Expecting Iterables

Some statements and expressions expect iterables. For example, the `for...of` loops, spread syntax, [`yield*`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/yield*), and destructuring syntax.

```js
for (const value of ["a", "b", "c"]) {
  console.log(value);
}
/*
a
b
c
*/

[..."abc"];
// Array(3) [ "a", "b", "c" ]


function* gen() {
  yield* ["a", "b", "c"];
}


gen().next();
// Object { value: "a", done: false }

[a, b, c] = new Set(["a", "b", "c"]);
// Set(3) [ "a", "b", "c" ]
a;
// "a"
```

### Advanced Generators

Generators compute their `yield`ed values *on demand*, which allows them to efficiently represent sequences that are expensive to compute (or even infinite sequences, as previously demonstrated).

The `.next()` method also accepts a value, which can be used to modify the internal state of the generator. A value passed to `.next()` will be received by `yield`.

Note that a value passed to the *first* invocation of `.next()` is always ignored.

Here is the fibonacci generator using `.next(x)` to restart the sequence:

```js
function* fibonacci() {
  let current = 0;
  let next = 1;
  while (true) {
    const reset = yield current;
    [current, next] = [next, next + current];
    if (reset) {
      current = 0;
      next = 1;
    }
  }
}


const sequence = fibonacci();
console.log(sequence.next().value);
// 0
console.log(sequence.next().value);
// 1
console.log(sequence.next().value);
// 1
console.log(sequence.next().value);
// 2
console.log(sequence.next().value);
// 3
console.log(sequence.next().value);
// 5
console.log(sequence.next().value);
// 8
console.log(sequence.next(true).value);
// 0
console.log(sequence.next().value);
// 1
console.log(sequence.next().value);
// 1
console.log(sequence.next().value);
// 2
```

You can force a generator to throw an exception by calling its `.throw()` method and passing the exception value it should throw. This exception will be thrown from the current suspended context of the generator, as if the `yield` that is currently suspended were instead a `throw value` statement.

If the exception is not caught from within the generator, it will propagate up through the call to `.throw()`, and subsequent calls to `.next()` will result in the `done` property being `true`.

Generators have a `.return()` method that returns the given value and finishes the generator itself.

## Internationalization

The [`Intl`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl) object is the namespace for the ECMAScript Internationalization API, which provides a wide range of locale- and culture-sensitive data and operations.

### Date and Time Formatting

The [`Intl.DateTimeFormat`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat) object is useful for formatting date and time. The following formats a date for English as used in the United States.

```js
// July 17, 2014 00:00:00 UTC
const july172014 = new Date("2014-07-17");

const options = {
  year: "2-digit",
  month: "2-digit",
  day: "2-digit",
  hour: "2-digit",
  minute: "2-digit",
  timeZoneName: "short",
};
const americanDateTime = new Intl.DateTimeFormat("en-US", options).format;

// Local timezone vary depending on your settings
console.log(americanDateTime(july172014));
// In EDT, logs: 07/16/14, 08:00 PM EDT
```

### Number Formatting

The [`Intl.NumberFormat`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat) object is useful for formatting numbers, e.g., currencies.

```js
const gasPrice = new Intl.NumberFormat(
  "en-US",
  {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 3,
  }
);

console.log(gasPrice.format(5.259));
// $5.259

const hanDecimalRMBInChina = new Intl.NumberFormat(
  "zh-CN-u-nu-hanidec",
  {
    style: "currency",
    currency: "CNY",
  }
);

console.log(hanDecimalRMBInChina.format(1314.25));
// ¥一,三一四.二五
```

### Collation

The [`Intl.Collator`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/Collator) object is useful for comparing and sorting strings.

For example, there are two different sort orders in German, *phonebook* and *dictionary*. Phonebook sort emphasizes sound. For example, it is as if *ä* and *ö* were expanded to *ae* and *oe* prior to sorting.

```js
const names = ["Hochberg", "Hönigswald", "Holzman"];
const germanPhonebook = new Intl.Collator("de-DE-u-co-phonebk");

// As if sorting ["Hochberg", "Hoenigswald", "Holzman"]
console.log(names.sort(germanPhonebook.compare).join(", "));
// Hochberg, Hönigswald, Holzman
```

Some German words conjugate with extra umlauts, so in dictionaries it is sensible to order ignoring umlauts (except when ordering words differing only by umlauts, e.g., *schon* before *schön*).

```js
const names = ["Hochberg", "Hönigswald", "Holzman"];
const germanDictionary = new Intl.Collator("de-DE-u-co-dict");

// As if sorting ["Hochberg", "Honigswald", "Holzman"]
console.log(names.sort(germanDictionary.compare).join(", "));
// Hochberg, Holzman, Hönigswald
```

For more information about the `Intl` API, see [Introducing the JavaScript Internationalization API](https://hacks.mozilla.org/2014/12/introducing-the-javascript-internationalization-api/).

## Meta Programming

The [`Proxy`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy) and [`Reflect`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy) objects allow you to intercept and define custom behavior for fundamental language operations (e.g., property lookup, assignment, enumeration, and function invocation). With the help of these two objects, you are able to program at the meta level of JavaScript.

### Proxies

[`Proxy`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy) objects allow you to intercept certain operations (e.g., getting a property on an object) and to implement custom behaviors.

```js
const handler = {
  get(target, name) {
    return name in target ? target[name] : 42;
  },
};

const p = new Proxy({}, handler);
p.a = 1;
console.log(p.a, p.b);
// 1 42
```

The `Proxy` object defines a `target` (above, an empty object) and a `handler` object, in which a `get` *trap* is implemented. Above, an object that is proxied will not return `undefined` when getting undefined properties, but instead returns the number `42`.

Additional examples are available on the [`Proxy`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy) reference page.

#### Terminology

The following terms are used when talking about the functionality of proxies.

- [**handler**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy) - Placeholder object which contains traps.
- **traps** - The methods that provide property access. (This is analogous to the concept of *traps* in operating systems.)
- **target** - Object that the proxy virtualizes. Often, it is used as storage backend for the proxy. Invariants (semantics that remain unchanged) regarding object non-extensibility or non-configurable properties are verified against the target.
- **invariants** - Semantics that remain unchanged when implementing custom operations are called *invariants*. If you violate the invariants of a handler, a [`TypeError`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypeError) will be thrown.

### Handlers and Traps

The following table summarizes the available traps available to Proxy objects.

<details>
  <summary>Show/Hide Handlers and Traps</summary><br />
  <table>
    <caption>Handlers and Traps</caption>
    <thead>
      <tr>
        <th>Handler / trap</th>
        <th>Interceptions</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  getPrototypeOf"><code>handler.getPrototypeOf()</code></a></td>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/getPrototypeOf"><code>Object.getPrototypeOf()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/getPrototypeOf"><code>Reflect.getPrototypeOf()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/proto"><code>__proto__</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/isPrototypeOf"><code>Object.prototype.isPrototypeOf()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Operators/  instanceof"><code>instanceof</code></a></td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  setPrototypeOf"><code>handler.setPrototypeOf()</code></a></td>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/setPrototypeOf"><code>Object.setPrototypeOf()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/  setPrototypeOf"><code>Reflect.setPrototypeOf()</code></a></td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  isExtensible"><code>handler.isExtensible()</code></a></td>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/isExtensible"><code>Object.isExtensible()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/isExtensible"><code>Reflect.  isExtensible()</code></a></td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  preventExtensions"><code>handler.preventExtensions()</code></a></td>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/preventExtensions"><code>Object.preventExtensions()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/  preventExtensions"><code>Reflect.preventExtensions()</code></a></td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  getOwnPropertyDescriptor"><code>handler.getOwnPropertyDescriptor()</code></a></td>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyDescriptor"><code>Object.getOwnPropertyDescriptor()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/  getOwnPropertyDescriptor"><code>Reflect.getOwnPropertyDescriptor()</code></a></td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  defineProperty"><code>handler.defineProperty()</code></a></td>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty"><code>Object.defineProperty()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/  defineProperty"><code>Reflect.defineProperty()</code></a></td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  has"><code>handler.has()</code></a></td>
        <td>
          <dl>
            <dt><a href="#property_query">Property query</a></dt>
            <dd><code>foo in proxy</code></dd>
          <dt><a href="#inherited_property_query">Inherited property query</a></dt>
            <dd><code>foo in Object.create(<var>proxy</var>)</code><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/has"><code>Reflect.has()</code></a></dd>
          </dl>
        </td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  get"><code>handler.get()</code></a></td>
        <td>
          <dl>
            <dt><a href="#property_access">Property access</a></dt>
            <dd><code><var>proxy</var>[foo]</code><br><code><var>proxy</var>.bar</code></dd>
          <dt><a href="#inherited_property_access">Inherited property access</a></dt>
            <dd><code>Object.create(<var>proxy</var>)[foo]</code><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/get"><code>Reflect.get()</code></a></dd>
          </dl>
        </td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  set"><code>handler.set()</code></a></td>
        <td>
          <dl>
          <dt><a href="#property_assignment">Property   assignment</a></dt>
            <dd><code><var>proxy</var>[foo] = bar</code><br><code><var>proxy</var>.foo =   bar</code></dd>
          <dt><a   href="#inherited_property_assignment">Inherited property assignment</a></dt>
            <dd><code>Object.create(<var>proxy</var>)[foo] = bar</code><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/set"><code>Reflect.set  ()</code></a></dd>
          </dl>
        </td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  deleteProperty"><code>handler.deleteProperty()</code></a></td>
        <td>
          <dl>
          <dt><a href="#property_deletion">Property deletion</  a></dt>
            <dd><code>delete <var>proxy</var>[foo]</code><br><code>delete <var>proxy</var>.foo</code><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/  Reflect/deleteProperty"><code>Reflect.deleteProperty()</code></a></dd>
          </dl>
        </td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  ownKeys"><code>handler.ownKeys()</code></a></td>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames"><code>Object.getOwnPropertyNames()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertySymbols"><code>Object.getOwnPropertySymbols()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys"><code>Object.keys()</code></a><br><a href="/en-US/docs/Web/JavaScript/  Reference/Global_Objects/Reflect/ownKeys"><code>Reflect.ownKeys()</code></a></td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  apply"><code>handler.apply()</code></a></td>
        <td><code>proxy(..args)</code><br><a href="/en-US/docs/Web/JavaScript/Reference/  Global_Objects/Function/apply"><code>Function.prototype.apply()</code></a> and
        <a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call"><code>Function.prototype.call()</code></a><br><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/apply"><code>Reflect.apply()</  code></a></td>
      </tr>
      <tr>
        <td><a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/  construct"><code>handler.construct()</code></a></td>
        <td><code>new proxy(...args)</code><br><a href="/en-US/docs/Web/JavaScript/  Reference/Global_Objects/Reflect/construct"><code>Reflect.construct()</code></a></td>
      </tr>
    </tbody>
  </table>
</details>

### Revocable `Proxy`

The [`Proxy.revocable()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/revocable) method is used to create a revocable `Proxy` object. This means that the proxy can be revoked via the function `revoke()` and switches the proxy off.

Afterwards, any operation on the proxy leads to a `TypeError`.

```js
const revocable = Proxy.revocable(
  {},
  {
    get(target, name) {
      return `[[${name}]]`;
    },
  },
);
const proxy = revocable.proxy;
console.log(proxy.foo);
// [[foo]]

revocable.revoke();

console.log(proxy.foo);
// Uncaught TypeError: illegal operation attempted on a revoked proxy

proxy.foo = 1;
// Uncaught TypeError: illegal operation attempted on a revoked proxy

delete proxy.foo;
// Uncaught TypeError: illegal operation attempted on a revoked proxy

console.log(typeof proxy);  // typeof does not trigger any trap
// object
```

### Reflection

[`Reflect`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect) is a built-in object that provides methods for interceptable JavaScript operations. The methods are the same as those of the proxy handler's.

`Reflect` is not a function object. `Reflect` helps with forwarding default operations from the handler to the `target`.

For example, with `Reflect.has()`, you get the `in` operator as a function:

```js
Reflect.has(Object, "assign");
// true
```

#### A Better `apply()` Function

Before `Reflect`, you typically use the [`Function.prototype.apply()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/apply) method to call a function with a given `this` value and `arguments` provided as an array (or an [array-like object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Indexed_collections#working_with_array-like_objects)).

```js
Function.prototype.apply.call(Math.floor, undefined, [1.75]);
```

With `Reflect.apply`, this becomes less verbose and easier to understand:

```js
Reflect.apply(Math.floor, undefined, [1.75]);
// 1

Reflect.apply(String.fromCharCode, undefined, [104, 101, 108, 108, 111]);
// "hello"

Reflect.apply(RegExp.prototype.exec, /ab/, ["confabulation"]).index;
// 4

Reflect.apply("".charAt, "ponies", [3]);
// "i"
```

#### Checking If Property Definition Have Been Successful

With [`Object.defineProperty`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty), which returns an object if successful, or throws a `TypeError` otherwise, you would use a [`try...catch`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch) block to catch any error that occurred while defining a property. Since [`Reflect.defineProperty()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect/defineProperty) returns a Boolean success status, you can just use an [`if...else`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else) block here:

```js
if (Reflect.defineProperty(target, property, attributes)) {
  // Success
} else {
  // Failure
}
```

## Internal JavaScript

```html
<script>
  document.addEventListener(
    "DOMContentLoaded",
    function () {
      const buttons = document.querySelectorAll("button");
  
  
      function createParagraph() {
        const para = document.createElement("p");
        para.textContent = "Button was selected.";
        document.body.appendChild(para);
      }
  
  
      for (const button of buttons) {
        button.addEventListener("click", createParagraph);
      }
    }
  );
</script>
```

The browser's `DOMContentLoaded` event signifies that the HTML body is completely loaded and parsed. The JavaScript inside this block will not run until after that event is fired.

## External JavaScript

`<script src="script.js" defer></script>`

```js
// script.js
const buttons = document.querySelectorAll("button");


function createParagraph() {
  const para = document.createElement("p");
  para.textContent = "Button was selected.";
  document.body.appendChild(para);
}


for (const button of buttons) {
  button.addEventListener("click", createParagraph);
}
```

The `defer` attribute tells the browser to continue downloading the HTML content once the `<script>` tab element has been reached. In this case, both the script and HTML simultaneously load and the code will work. `defer` only works for external scripts.

### `async` and `defer`

Scripts loaded using the `async` attribute download the script without blocking the page while the script is being fetched. However, once the download is complete, the script will execute, which blocks the page from rendering.

You get no guarantee that scripts will run in any specific order. It is best to use `async` when the scripts in the page run independently from each other and depend on no other script on the page. `async` should be used when you have a collection of background scripts to load in and you just want to get them in place as soon as possible.

Scripts loaded with `defer` load in the order they appear on the page. They will not run until the page content has all loaded, which is useful if your scripts depend on the DOM being in place.

To summarize:

- `async` and `defer` both instruct the browser to download the script(s) in a separate thread, while the rest of the page is downloading, so the page loading is not blocked during the fetch process.
- Scripts with an `async` attribute will execute as soon as the download is complete. This blocks the page and does not guarantee any specific execution order.
- Scripts with a `defer` attribute will load in the order they are in and will only execute once everything has finished loading.
- If your scripts should be immediately run and they do not have any dependencies, use `async`.
- If your scripts need to wait for parsing and depend on other scripts and/or the DOM being in place, load them using `defer` and put their corresponding `<script>` elements in the order you want the browser to execute them.

## Documentation

- [ECMAScript Language Specification](https://www.ecma-international.org/publications-and-standards/standards/ecma-262/)
- [MDN Interactive Playground](https://developer.mozilla.org/play)
- [Node.js](https://nodejs.org/)
    - [Node.js Documentation](https://nodejs.org/docs/latest/api/)
        - [How to use the Node.js REPL](https://nodejs.org/en/learn/command-line/how-to-use-the-nodejs-repl)
- [npm](https://www.npmjs.com/)
- [Web Technology MDN](https://developer.mozilla.org/docs/Web)
    - [Events](https://developer.mozilla.org/docs/Web/Events)
    - [HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP)
        - [HTTP Request Methods](https://developer.mozilla.org/docs/Web/HTTP/Methods)
    - [JavaScript](https://developer.mozilla.org/docs/Web/JavaScript)
        - [JavaScript Guide](https://developer.mozilla.org/docs/Web/JavaScript/Guide)
            - [Assignment Operators](https://developer.mozilla.org/docs/Web/JavaScript/Guide/Expressions_and_operators#assignment_operators)
            - [Comparison Operators](https://developer.mozilla.org/docs/Web/JavaScript/Guide/Expressions_and_operators#comparison_operators)
            - [Conditional Statements](https://developer.mozilla.org/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#conditional_statements)
            - [Expressions and Operators](https://developer.mozilla.org/docs/Web/JavaScript/Guide/Expressions_and_operators)
            - [Loops and Iteration](https://developer.mozilla.org/docs/Web/JavaScript/Guide/Loops_and_iteration)
            - [Numbers and Dates](https://developer.mozilla.org/docs/Web/JavaScript/Guide/Numbers_and_dates)
            - [Regular Expressions](https://developer.mozilla.org/docs/Web/JavaScript/Guide/Regular_Expressions)
            - [Using Promises](https://developer.mozilla.org/docs/Web/JavaScript/Guide/Using_promises)
        - [JavaScript Reference](https://developer.mozilla.org/docs/Web/JavaScript/Reference)
            - [Arrow Functions](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Functions/Arrow_functions)
            - [Classes](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Classes)
            - [Errors](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Errors)
            - [Escape Sequences](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Lexical_grammar#escape_sequences)
            - [Functions](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Functions)
            - [Global Objects](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects)
            - [JSON](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/JSON)
            - [Keywords](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Lexical_grammar#keywords)
            - [Operator Precedence](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Operator_precedence)
            - [Promise Reference](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Promise)
            - [Template Literals](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Template_literals)
        - [Useful String Methods](https://developer.mozilla.org/docs/Learn/JavaScript/First_steps/Useful_string_methods)
    - [Web APIs](https://developer.mozilla.org/docs/Web/API)
        - [console](https://developer.mozilla.org/docs/Web/API/console)
        - [Event API](https://developer.mozilla.org/docs/Web/API/Event)
        - [Fetch API Overview](https://developer.mozilla.org/docs/Web/API/Fetch_API)
        - [Using Fetch](https://developer.mozilla.org/docs/Web/API/Fetch_API/Using_Fetch)
        - [Using Service Workers](https://developer.mozilla.org/docs/Web/API/Service_Worker_API/Using_Service_Workers)
        - [Using Web Workers](https://developer.mozilla.org/docs/Web/API/Web_Workers_API/Using_web_workers)