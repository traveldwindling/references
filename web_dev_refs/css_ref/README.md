# CSS Reference

A collection of CSS reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

[Cascading Style Sheets (CSS)](https://en.wikipedia.org/wiki/Cascading_Style_Sheets) is a style sheet language that is used to describe the presentation of structured documents written in a markup language. Usually, it is used to create the layout and visual style of webpages written in the [Hypertext Markup Language (HTML)](https://en.wikipedia.org/wiki/HTML) and [Extensible HTML (XHTML)](https://en.wikipedia.org/wiki/XHTML), but it can be applied to documents written in the [Extensible Markup Language (XML)](https://en.wikipedia.org/wiki/XML), [Scalable Vector Graphics (SVG)](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics), and the [XML User Interface Language (XUL)](https://en.wikipedia.org/wiki/XUL), as well.

## Quick Guide

General syntax is:

```css
selector {
  property: value;
}
```

`/* A CSS comment */`

### Absolute Values

Absolute values include:

- `cm` - centimeters
- `in` - inches
- `mm` - millimeters
- `pc` - picas
- `pt` - points
- `px` - pixels

### Relative Values

Relative values include:

- `ch` - character
- `em` - ems
- `ex` - exes
- `rem` - root ems
- `vh` - viewport height
- `vmax` - viewport maximum
- `vmin` - viewport minimum
- `vw` - viewport width

`rem` to `px` translation is determined by the font size of the `html` element. For most browsers, the default font size is `16px`. When using `rem` units to create a uniformly scalable design, your media queries should also be in `rem` units.

In the case of typographical properties like `font-size`, `em` to `px` translation is determined by the font size of the direct or nearest parent. In the case of other properties like `width`, `em` to `px` translation is determined by the font size of the element itself.

Percentage values are calculated relative to their parent element. When using relative values, it is best to use percentages with the `body` element.

### When to Use Relative Versus Absolute Values

- Use `rem` units for font sizes.
- In general, start with using `rem` units for margins, padding, and borders. This will ensure that these values are always relative to the font size, so they will scale up/down, accordingly.
- For small boxes that need to scale to accommodate content, use `rem` units.
- For purely typographical features that should be relative to the local font size (e.g., letter spacing), use `em` units.
- For visual effects that only work well at a certain size (e.g., shadows), use `px` units.
- Use `rem` units for media queries, unless you need to target a specific device, in which case `px` units may be more appropriate.

### Spacing, Indentation, Width, and Height

To set the _spacing_ between letters and words, use the `letter-spacing` and `word-spacing` properties, respectively.

To _indent_ text, use the `text-indent` property. To create a hanging indent, use a negative value.

_Line height_ is set with the `line-height` property. Usually, line height is set to 1 to 1.2 times the size of the font. You can use `line-height` to force text to be vertically centered in an element, which is often used in menus and works best with single lines of text.

The spacing between elements is controlled by _margins_. Unlike other properties, the `margin` property is not, by default, inherited.

Margins help control the spacing between elements. Horizontal margins are cumulative, while vertical margins are not. Vertical margins also differ in several ways.

- Floated elements are removed from the normal document flow. Top margins beneath floated elements will move up and wrap around those objects.
- When two vertical margins touch, they collapse. If one value is larger than the other, it will take precedence. This behavior is meant to prevent double spaces between paragraphs.

    When you have elements that touch each other, or even nested elements, if there is nothing to keep the vertical margins from touching (e.g., borders, padding), then all of those vertical margins will collapse, both inner and outer margins.

The `auto` value is used in conjunction with the `margin` property. It tells the browser to give it whatever is left over of the width of an element. It can be used to horizontally center an element. Setting the width of the element will prevent it from stretching out to the edges of its container.

It is sufficient to think of the width and height properties as _content width and content height_, while the _overall width_ of an element is determined by its borders, padding, width, and height.

Interior space (i.e., space inside of an element) is controlled through three properties:

1. `width`
2. `height`
3. `padding`

Block-level content occupies its own space in the document flow and will stretch to fill the width of its parent element.

Borders, padding, and the content width of an element are cumulative and determine an element’s total width. If you change one of these properties and do not define the others, they will either shrink or expand, based upon the available space you are giving them.

If your element no longer fits within its provided space, browsers are instructed to _ignore the element's margins_, given that there is enough room inside the parent element (i.e., margins will be sacrificed to help an element stay within its parent element).

If you increase the width of an element to a dimension where you no longer have margin to sacrifice, you will end up with _overflow_. Default overflow behavior is to make that content visible, but you can hide it, if you choose to.

You can add `scroll` to make an [overflow](https://developer.mozilla.org/en-US/docs/Web/CSS/overflow) area accessible.

Padding is the space between an element’s content and the inside edge of its border. Padding:

- Is primarily used to keep content away from the edge of its parent container.
- Is subtracted from the available width, if you do not have a predefined width.
- Will be added to a predefined width (i.e., it is cumulative).
- Will have content width and content height added to it (i.e., they are also cumulative).

Padding does not have an `auto` value, but it does have percentages. Padding flexes based on the amount of available space (i.e., percentages give you fluid and flexible padding regions).

You have the choice to use either padding or margins to help define an element’s size. You will need to account for all germane properties when attempting to fit elements to a specific size.

### Media Queries

Media queries are useful when you want to modify your site or app depending on a device's general type (e.g., `print` versus `screen`), or specific characteristics and parameters (e.g., screen resolution or browser viewport width).

A media query is composed of an optional _media type_ and any number of _media feature_ expressions. Multiple queries can be combined in various ways by using _logical operators_. Media queries are case-insensitive.

```css
@media screen and (max-width: 37.5rem) {
  ...
}
```

## Documentation

- [CSS: Cascading Style Sheets](https://developer.mozilla.org/en-US/docs/Web/CSS)
    - [Block-Level Content](https://developer.mozilla.org/en-US/docs/Glossary/Block-level_content)
    - [CSS Values and Units](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units)
    - [Inline-Level Content](https://developer.mozilla.org/en-US/docs/Glossary/Inline-level_content)
    - [Organizing Your CSS](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Organizing)
- [Firefox DevTools User Docs](https://firefox-source-docs.mozilla.org/devtools-user/index.html)
- [HTML Color Picker](https://www.w3schools.com/colors/colors_picker.asp)
- [`normalize.css`](https://github.com/necolas/normalize.css)