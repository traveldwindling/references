# Web Development References

A collection of Web development reference materials.

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="css_ref/">CSS Reference</a></td>
      <td>A collection of CSS reference materials.</td>
    </tr>
    <tr>
      <td><a href="js_ref/">JavaScript Reference</a></td>
      <td>A collection of JavaScript reference materials.</td>
    </tr>
  </tbody>
</table>