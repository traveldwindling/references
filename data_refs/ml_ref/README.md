# Machine Learning Reference

A collection of machine learning reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## Machine Learning Packages

```console
python3 -m pip install \
        imbalanced-learn \
        lightgbm \
        nltk \
        scikit-learn \
        spacy &&
    python3 -m spacy download en_core_web_sm
```

## Machine Learning General Steps

1. Process and prepare data for analysis.
2. Shortlist algorithms to utilize based on requirements.
3. Tune algorithm parameters to optimize the results.

    <table>
    <caption>Supervised Learning Tuning Parameters</caption>
    <thead>
        <tr>
        <th>Algorithm</th>
        <th>Parameters</th>
        </tr>
    </thead>
    <tbody>
        <tr>
        <td>Regression Analysis</td>
        <td>Regularization parameter (lasso and ridge regression)</td>
        </tr>
        <tr>
        <td>k-Nearest Neighbors</td>
        <td>Number of nearest neighbors</td>
        </tr>
        <tr>
        <td>Support Vector Machine</td>
        <td>
            <ul>
            <li>Soft margin constant</li>
            <li>Kernel parameters</li>
            <li>Insensitivity parameter</li>
            </ul>
        </td>
        </tr>
        <tr>
        <td>Decision Tree</td>
        <td>
            <ul>
            <li>Minimum size of terminal nodes</li>
            <li>Maximum number of terminal nodes</li>
            <li>Maximum tree depth</li>
            </ul>
        </td>
        </tr>
        <tr>
        <td>Random Forests</td>
        <td>
            <ul>
            <li>All parameters of a decision tree</li>
            <li>Number of trees</li>
            <li>Number of variables to select at each split</li>
            </ul>
        </td>
        </tr>
        <tr>
        <td>Neural Networks</td>
        <td>
            <ul>
            <li>Number of hidden layers</li>
            <li>Number of neurons in each layer</li>
            <li>Number of training iterations</li>
            <li>Learning rate</li>
            <li>Initial weights</li>
            </ul>
        </td>
        </tr>
    </tbody>
    </table>

    Making a model more complicated can reduce prediction errors, but can lead to [overfitting](https://en.wikipedia.org/wiki/Overfitting) and reduced generalizability of the model.

    Model complexity can be kept in check using [regularization](https://en.wikipedia.org/wiki/Regularization_(mathematics)), which penalizes model complexity increases by artificially inflating prediction error. This allows the algorithm to account for both complexity and accuracy in optimizing its original parameters.

4. Build models and compare them to select the best one.

## Variable Selection

Variable selection can be a trial-and-error process.

Plots can be used to examine correlations between variables. Select promising ones for further analysis.

## Feature Preparation

Feature preparation can involve:

- Recoding extant variables
- Creating new variables from extant variables
- Combining multiple extant variables using [dimension reduction](https://en.wikipedia.org/wiki/Dimensionality_reduction)

Various techniques can be used to convert categorical and ordinal features into numerical features.

### Categorical

#### One-Hot Encoding (OHE)

For low cardinality variables and regression-based algorithms, _One-Hot Encoding (OHE)_ can be used:

```python
import pandas as pd  # Enable data structures and data analysis tools

X_train = pd.get_dummies(X_train, drop_first=True)
X_test = pd.get_dummies(X_test, drop_first=True)
```

<https://pandas.pydata.org/docs/reference/api/pandas.get_dummies.html>

#### Label Encoding

For tree-based algorithms, _Label Encoding_ can be used:

```python
import pandas as pd  # Enable data structures and data analysis tools
from sklearn.preprocessing import OrdinalEncoder  # Enable label encoding

encoder = OrdinalEncoder()  # Create label encoding class instance

encoder.fit(X_train)  # Fit the encoder

# Recreate DataFrames with encoded data
X_train = pd.DataFrame(encoder.transform(X_train), columns=X_train.columns)
X_test = pd.DataFrame(encoder.transform(X_test), columns=X_test.columns)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.OrdinalEncoder.html>

### Ordinal

#### Ordinal Encoding

For ordinal variables, _Ordinal Encoding_ can be used:

```python
# Map variable values to a numeric label
ord_val_map = {
    'val_1': 0,
    'val_2': 1,
    'val_3': 2
}

# Replace ordinal labels with numeric labels
X_train['ex_col'] = X_train['ex_col'].map(ord_val_map)
X_test['ex_col'] = X_test['ex_col'].map(ord_val_map)
```

### Numerical

#### Scaling, Standardization, and Normalization

Many machine learning algorithms perform better or converge on an answer faster when features are on a relatively similar scale and/or standardized. Scaling and standardization can help features be more efficiently processed by these algorithms and can improve model training times.

##### Scaling

Generally, _scaling_ means to change the data range and is used when features do not have the same range of values. Scaling does not change the shape of a distribution (this is similar to how a scale model of a building has the same proportions as the original, just smaller).

Scaling is important when:

- The data has varying scales. Features with a large range will have a large influence, will dominate the statistics, and will suppress other features with small ranges.
- The machine learning model is based on calculating _distances_.
- The machine learning model does not make any assumptions about the data distribution's shape.

Scaling is appropriate to use when you do not know the distribution of your data or when you know that the distribution is not a [normal distribution](https://en.wikipedia.org/wiki/Normal_distribution).

The following tasks/algorithms are sensitive to feature scales:

- Image Processing
- Neural Networks
- K-Nearest-Neighbors
- K-Means
- Support Vector Machine
- Clustering Analyses
- Principal Component Analysis

Scaling is also used with gradient descent-based algorithms to improve the speed of the descent.

Algorithms that are _not distance-based_ are insensitive to feature scales. Naive Bayes and Tree-Based models are also insensitive to feature scales.

##### Standardization

Generally, _standardization_ means to change normally distributed data into a distribution of z-scores, where the resulting distribution has a mean of `0` and a standard deviation of `1`, where `μ` is the mean and `σ` is the standard deviation (i.e., a [standard normal distribution](https://en.wikipedia.org/wiki/Normal_distribution#Standard_normal_distribution)). Scaling is often implied.

If standardization is used with data that is _not_ normally distributed, it may distort the relative space among the features.

Standardization is important when:

- The data has varying scales.
- The machine learning model _does_ make assumptions about your data having a normal distribution. Machine learning models that make this assumption include:
    - Linear Regression
    - Logistic Regression
    - Linear Discriminant Analysis

##### Normalization

_Normalization_ can refer to either scaling or standardization, as well as other concepts and techniques. It may be best to avoid using this term, as it can lead to confusion.

##### Outliers

Both scaling and standardization may be sensitive to outliers, i.e., if you leave outliers in your data set when you perform scaling/standardization, they may distort your model's estimations. So, when you have outliers, winsorize the data set before scaling/standardization, or use a scaling method that accounts for outliers.

#### Feature Scaling

If raw numerical features have different scales, _Feature Scaling_ can be used to account for this.

If your distribution is not normally distributed and you are unconcerned about outliers' influence, you can use `MinMaxScaler()`:


```python
import pandas as pd  # Enable data structures and data analysis tools
# Enable transforming of features by scaling each feature to a given range
from sklearn.preprocessing import MinMaxScaler

# Suppress SettingWithCopyWarning warnings
pd.options.mode.chained_assignment = None

# Define list of numeric column names
num_cols = ['ex_col_1', 'ex_col_2', 'ex_col_3']

scaler = MinMaxScaler()  # Create scalar class instance

# Tune scalar class instance on training data
scaler.fit(X_train[numeric])

# Transform training and test numerical columns
X_train[num_cols] = scaler.transform(X_train[num_cols])
X_test[num_cols] = scaler.transform(X_test[num_cols])
```

In Min-Max scaling, data is transformed such that features are within a specific range. The default range is `[0, 1]` (`[-1, 1]` if there are negative values), where `x'` is the scaled value.

`x′ = ( x - min(x) ) / ( max(x) - min(x) )`

For each value in a feature, `MinMaxScaler()` subtracts the minimum value in the feature and then divides by the range. The range is the difference between the original maximum and original minimum.

`MinMaxScaler()` preserves the shape of the original distribution. It does not meaningfully change the information embedded in the original data. `MinMaxScaler()` does not reduce the importance of outliers.

The features all remain on the same relative scale. The relative spaces between each feature’s values are maintained.

When feature scaling, `MinMaxScaler()` is a good place to start, unless your data is normally distributed or you want outliers to have a reduced influence.

<https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html>

To reduce the effects of outliers, [winsorize](https://en.wikipedia.org/wiki/Winsorizing) the data set before using `MinMaxScaler()`, or consider using `RobustScaler()`:

```python
# Enable scaling features using statistics that are robust to outliers
from sklearn.preprocessing import RobustScaler
```

Robust scaling transforms the feature vector by subtracting the median, and then dividing by the interquartile range (`75% value — 25% value`).

`x′ = ( x - median ) / ( 75_per - 25_per )`

Like with `MinMaxScaler()`, features with large values will now have a similar scale as the other features. Note that RobustScaler does not scale the data into a predetermined interval like `MinMaxScaler()`, i.e., it does not meet the strict definition of scaling.

The range for each feature after `RobustScaler()` is applied is larger than it is for `MinMaxScaler()`, i.e., the relative spaces between each feature’s values are distorted and not the same now.

Use RobustScaler if you want to reduce the effects of outliers, relative to `MinMaxScaler()`.

<https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.RobustScaler.html>

If you know that your data is normally distributed, you can use `StandardScaler()`:

```python
# Enable standardizing features by removing the mean and scaling
# to unit variance
from sklearn.preprocessing import StandardScaler
```

`StandardScaler()` standardizes a feature by subtracting the mean and then scaling to unit variance. Unit variance means dividing all of the values by the standard deviation. `StandardScaler()` does not meet the strict definition of scaling.

`z = (x - μ) / σ`

`StandardScaler()` _assumes data is normally distributed_ within each feature and scales them such that the distribution is centered around a mean of `0` with a standard deviation of `1`. In other words, `StandardScaler()` is meant to convert normal distributions into standard normal distributions, which are normal distributions of standardized values (i.e., z-scores).

Since `StandardScaler()` creates a standard normal distribution with a mean of `0` and standard deviation of `1`, `68%` of the values will lie be between `-1` and `1`, i.e., the [Empirical Rule](https://en.wikipedia.org/wiki/68%E2%80%9395%E2%80%9399.7_rule) applies. The variance is also equal to `1` because `variance = standard deviation squared` and `1^2 = 1`.

The range for each feature after `StandardScaler()` is applied is larger than it is for `MinMaxScaler()`.

Keep in mind, `StandardScaler()` may distort the relative distances between the feature values if the original distribution is not normally distributed. If data is not normally distributed, this is not the best scaler to use.

<https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html>

## Missing Data

Missing data can be:

- Estimated (_mode_ for qualitative data, _median_ for quantitative data)
- Computed using Supervised Learning algorithms (this is time-consuming, but more accurate, since the algorithms estimate missing values based on similar transactions)
- Removed (generally avoided, as it reduces the amount of data for analysis and could skew the data)

## Algorithms

### Unsupervised Learning

[Unsupervised learning](https://en.wikipedia.org/wiki/Unsupervised_learning) is when algorithms are used to find patterns in the data that _we are unaware_ of (i.e., there is no target). Results are validated via indirect means (e.g., checking if generated clusters correspond to familiar categories).

<table>
  <thead>
    <tr>
      <th></th>
      <th></th>
      <th>k-Means Clustering</th>
      <th>Principal Component Analysis</th>
      <th>Association Rules</th>
      <th>Social Network Analysis (Louvain Method)</th>
      <th>Social Network Analysis (PageRank)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Input</th>
      <td>Binary Values</td>
      <td></td>
      <td></td>
      <td>&#10003;</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td>Continuous Values</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td>Nodes and Edges</td>
      <td></td>
      <td></td>
      <td></td>
      <td>&#10003;</td>
      <td>&#10003;</td>
    </tr>
    <tr>
      <th>Output</th>
      <td>Categories</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td></td>
      <td>&#10003;</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td>Associations</td>
      <td></td>
      <td></td>
      <td>&#10003;</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td>Ranks</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>&#10003;</td>
    </tr>
  </tbody>
</table>

#### [k-Means Clustering](https://en.wikipedia.org/wiki/K-means_clustering)

**Summary**

- Technique to group similar data points together. The number of clusters, `k`, must be specified in advance.
- To cluster data points, each point is first assigned to the nearest cluster, and then the position of cluster centers (centroids) are updated. These two steps are repeated until there are no further changes in cluster membership.
- Works best for spherical, non-overlapping clusters.

**Limitations**

- Each data point can only be assigned to one cluster.
- Clusters are assumed to be spherical.

    The iterative process of finding data points closet to a cluster center is akin to narrowing the cluster's radius, so that the resulting cluster resembles a compact sphere. This could be a problem if the shape of the actual cluster is an ellipse, where elongated clusters might be truncated with their members subsumed into nearby clusters.

- Clusters are assumed to be discrete. Clusters are not permitted to overlap or be nested within each other.

    Instead of coercing each data point into a single cluster, a more robust clustering technique that computes the probabilities of how likely each data point might belong to other clusters could be used, thus helping identify non-spherical or overlapping clusters.

    A good strategy could be to start with k-means clustering for a basic understanding of a data structure, before delving into more advanced methods to mitigate its limitations.

#### [Principal Component Analysis](https://en.wikipedia.org/wiki/Principal_Component_Analysis)

**Summary**

- A _dimension reduction_ technique that allows the expression of data with a smaller set of variables, called _principal components_.
- Each principal component is a weighted sum of original variables. Top principal components are used to improve analysis and visualization.
- Works best when the most informative dimensions have the largest data spread and are orthogonal to each other (i.e., 90 degrees to one another).

**Limitations**

- Assumes that the largest spread of data points are the most useful.
- Interpretations of generated components have to be inferred. However, having prior domain knowledge may help.
- Assumes orthogonal principal components.

    To resolve this, we can use an alternative technique referred to as [Independent Component Analysis (ICA)](https://en.wikipedia.org/wiki/Independent_component_analysis), which allows components to be non-orthogonal, but forbids them from overlapping in the information that they contain. This results in each independent component revealing unique information on the data set.

    When in doubt, consider running an ICA to verify and complement results from a PCA.

#### [Association Rules](https://en.wikipedia.org/wiki/Association_rule_learning)

**Summary**

- Reveals how frequently items appear on their own in relation to each other.
- Three common ways to measure association:

    1. _Support_ of `{X}` indicates how frequently item `X` appears.
    2. _Confidence_ of `{X -> Y}` indicates how frequently item `Y` appears when item `X` is present.
    3. _Lift_ of `{X -> Y}` indicates how frequently items `X` and `Y` appear together, while accounting for how frequently each would appear on its own.

The [Apriori algorithm](https://en.wikipedia.org/wiki/Apriori_algorithm) accelerates the search for frequent item sets by putting away a large proportion of infrequent ones.

**Limitations**

- Computationally expensive, even after using the Apriori algorithm, if inventories are large, or when the support threshold is low.

    An alternative solution is to reduce the number of comparisons through the use of advanced data structures to more efficiently sort candidate item sets.

- Associations could happen by chance among a large number of items. To ensure that the discovered associations are generalizable, they should be _validated_.

#### [Social Network Analysis](https://en.wikipedia.org/wiki/Social_network_analysis)

**Summary**

- Technique that allows us to map and analyze relationships between entities.
- [Lovain method](https://en.wikipedia.org/wiki/Louvain_method) identifies clusters in a network in a way that maximizes interactions within clusters and minimizes those between clusters. Works best when clusters are equally sized and discrete.
- [PageRank algorithm](https://en.wikipedia.org/wiki/PageRank) ranks nodes in a network based on their number of links, as well as the strength and source of those links. Helps us to identify dominant nodes in a network, but is biased against newer nodes, which have less time to build up substantial links.

**Limitations**

- Absence of finding relationships that are not represented by information captured in the data set.
- Under-appreciation of other factors that influence data set factors, but are not captured in the data set.

To verify that the chosen data sources are viable and analysis techniques sufficiently robust, results should be checked against other sources of information.

### Supervised Learning

[Supervised learning](https://en.wikipedia.org/wiki/Supervised_learning) is when algorithms use patterns in the data to make predictions based on pre-existing patterns (i.e., features/predictor variables).

Features are analogous to _questions_ and the targets are analogous to the _answers_. As the teacher supervising the model, you provide both the questions and answers to the model, but provide no explanation as to how the features lead to the answers.

<table>
  <thead>
    <tr>
      <th></th>
      <th></th>
      <th>Regression Analysis</th>
      <th>k-Nearest Neighbors</th>
      <th>Support Vector Machine</th>
      <th>Decision Tree</th>
      <th>Random Forests</th>
      <th>Neural Networks</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Prediction</th>
      <td>Binary Outcomes</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
    </tr>
    <tr>
      <td></td>
      <td>Categorical Outcomes</td>
      <td></td>
      <td>&#10003;</td>
      <td></td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
    </tr>
    <tr>
      <td></td>
      <td>Class Probabilities</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td></td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
    </tr>
    <tr>
      <td></td>
      <td>Continuous Outcomes</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td></td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
    </tr>
    <tr>
      <td></td>
      <td>Non-Linear Relationships</td>
      <td></td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
    </tr>
    <tr>
      <th>Analysis</th>
      <td>Large Number of Variables</td>
      <td></td>
      <td></td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
    </tr>
    <tr>
      <td></td>
      <td>Simple to Use</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td></td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td>Fast Computation</td>
      <td>&#10003;</td>
      <td></td>
      <td></td>
      <td>&#10003;</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>Results</th>
      <td>Highly Accurate</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>&#10003;</td>
      <td>&#10003;</td>
    </tr>
    <tr>
      <td></td>
      <td>Interpretable</td>
      <td>&#10003;</td>
      <td>&#10003;</td>
      <td></td>
      <td>&#10003;</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

#### [Linear Regression](https://en.wikipedia.org/wiki/Linear_regression)

**Summary**

- Finds the best-fit trend line that passes through or sits close to as many data points as possible.
- Trend line is derived from a weighted combination of predictors. The weights are called _regression coefficients_, which indicate the strength of a predictor in the presence of other predictors.
- Works best when there is little correlation between predictors, no outliers, and when the expected trend is a straight line.

**Limitations**

- Accounts for all data points equally. Having a few data points with extreme values could significantly skew the trend line (i.e., outlier sensitivity).

    To detect this, scatterplots/boxplots could be used to identify outliers before further analysis.

- Highly-correlated predictors distort the interpretation of the trend line weights, resulting in [multicollinearity](https://en.wikipedia.org/wiki/Multicollinearity).

    To overcome multicollinearity, either exclude correlated predictors prior to analysis, or use more advanced techniques like [lasso](https://en.wikipedia.org/wiki/Lasso_(statistics)) or [ridge](https://en.wikipedia.org/wiki/Ridge_regression) regression.

- To address curved trends, predictor values may need to be transformed or an alternative algorithm should be used (e.g., Support Vector Machines).
- Correlation is not causation.

#### [k-Nearest Neighbors (KNN)](https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm)

**Summary**

- Technique that classifies a data point (e.g., by _majority voting_) by referencing the classifications of other data points it is closet to.
- `k` is the number of data points to reference and is determined via [cross-validation](https://en.wikipedia.org/wiki/Cross-validation_(statistics)).
- Works best when predictors are few and classes are about the same size. Inaccurate classifications could still be flagged as potential anomalies.

**Limitations**

- If there are multiple classes to be predicted and the classes drastically differ in size, data points belonging to the smallest class might be overshadowed by those from larger classes and suffer from higher risk of misclassification.

    To improve accuracy, _weighted voting_ could be used instead of majority voting, which would ensure that the classes of closer neighbors are weighted more heavily than those further away.

- If there are too many predictors to consider, it would be computationally intensive to identify and process nearest neighbors across multiple dimensions. Also, some predictors may be redundant and not improve prediction accuracy.

    To address this, [dimension reduction](https://en.wikipedia.org/wiki/Dimensionality_reduction) can be used to extract only the most powerful predictors for analysis.

#### [Support Vector Machine (SVM)](https://en.wikipedia.org/wiki/Support_vector_machine)

**Summary**

- Classifies data points into two groups by drawing a boundary down the middle between peripheral data points (i.e., _support vectors_) of both groups.
- Resilient against outliers as it uses a _buffer zone_ that allows a few data points to be on the incorrect side of the boundary. Also, employs the [kernel trick](https://en.wikipedia.org/wiki/Kernel_method#Mathematics:_the_kernel_trick) to efficiently derive curved boundaries.
- Works best when data points from a large sample have to be classified into two distinct groups.

**Limitations**

- Relies on support vectors to determine decision boundaries. A small sample would mean fewer support vectors for accurate positioning of the boundaries.
- Only able to classify two groups at a time. If there are more than two groups, SVM would need to be iterated to distinguish each group from the rest through a technique known as [multi-class SVM](https://en.wikipedia.org/wiki/Support_vector_machine#Multiclass_SVM).
- When a large overlap exists between data points from both groups, a data point near the boundary might be more prone to misclassification.

    Also, does not provide additional information on the probability of misclassification for each data point. However, a data point's distance from the decision boundary could be used to gauge the likelihood of its classification accuracy.

#### [Decision Tree](https://en.wikipedia.org/wiki/Decision_tree_learning)

**Summary**

- Makes predictions by asking a sequence of binary questions.
- Data sample is repeatedly split to obtain homogeneous groups in a process called [recursive partitioning](https://en.wikipedia.org/wiki/Recursive_partitioning), until a stopping criterion is reached.

**Limitations**

- As decision trees grow by splitting data points into homogeneous groups, a slight change in the data could trigger changes to the split and result in a different tree. Also, as decision trees aim for the best way to split data points each time, they are vulnerable to _overfitting_.
- Using the best binary question to split the data at the start might not lead to the most accurate predictions. Sometimes, less effective initial splits may lead to better subsequent predictions.

    Two methods to address this issue and diversify trees are:

    1. Choose different combinations of binary questions at random to grow multiple trees and then aggregate the predictions from those trees (i.e., create a _random forest_).
    2. Strategically select binary questions, such that prediction accuracy for each subsequent tree improves incrementally. Then, take the weighted average of the predictions from all trees (i.e., _gradient boosting_).

#### [Random Forests](https://en.wikipedia.org/wiki/Random_forest)

**Summary**

- Often, yields better prediction accuracy than decision trees because it leverages two techniques: [bootstrap aggregating](https://en.wikipedia.org/wiki/Bootstrap_aggregating) and [ensembling](https://en.wikipedia.org/wiki/Ensemble_learning).
    - Bootstrap aggregating involves generating a series of uncorrelated decision trees by randomly restricting variables during the splitting process. Ensembling involves combining the predictions of these trees.
- Results of a random forest are not interpretable, but predictors can be ranked according to their contributions to prediction accuracy.

**Limitations**

- Random forests are considered [black boxes](https://en.wikipedia.org/wiki/Black_box) because they comprise randomly generated decision trees that are not led by clear prediction rules. The lack of clarity on how its predictions are made could bring about ethical concerns when applied to certain areas (e.g., medical diagnosis, policing).

#### [Neural Networks](https://en.wikipedia.org/wiki/Artificial_neural_network)

**Summary**

- Comprise layers of neurons. During training, neurons in the first layer are activated by input data, and these activations are propagated to neurons in subsequent layers, eventually reaching the final output layer to make predictions.
- Whether a neuron is activated or not depends on the strength and source of the activation it receives, as set out in its [activation rule](https://en.wikipedia.org/wiki/Activation_function). Activation rules are refined using feedback on prediction accuracy, through a process called [backpropagation](https://en.wikipedia.org/wiki/Backpropagation).
- Perform best when large data sets and advanced computing hardware are available. Results are nonetheless largely uninterpretable.

**Limitations**

- The complexity of a neural network enables it to recognize inputs with intricate features, but it can only do so when there are large amounts of data available to train on. If the training set is too small, _overfitting_ will occur.

    The risk of overfitting can be minimized by:

    1. **Subsampling** - Inputs to the network are _smoothened_ by taking averages over a sample of the signal.
    2. **Distortions** - More data is created by introducing distortions. By treating each distorted image as a new input, the size of the training data is expanded. Distortions used should reflect those present in the original data set.
    3. **Dropout** - Half of the neurons during a training cycle are randomly excluded, precluding small neuron clusters from developing an over-reliance on each other. This forces different combinations of neurons to work together, so as to uncover more features from the training examples.

- Training a neural network that comprises thousands of neurons could take a long time. Upgrading to more powerful computing hardware/services would help, but could be costly.

    The algorithm could be modified to trade significantly faster processing speed for slightly lower prediction accuracy:

    1. [Stochastic Gradient Descent](https://en.wikipedia.org/wiki/Stochastic_gradient_descent) - Instead of using _all_ training examples to update a single parameter in an iteration, as in classic gradient descent, just _one_ training example in each iteration is used to update the parameter. The resulting parameter values might not be optimal, but they usually yield decent accuracy.
    2. [Mini-Batch Gradient Descent](https://en.wikipedia.org/wiki/Stochastic_gradient_descent#Iterative_method) - References a subset of training examples for each iteration. Is a middle-ground approach between classic gradient descent and stochastic gradient descent.
    3. [Fully-Connected Layers](https://en.wikipedia.org/wiki/Convolutional_neural_network#Fully_connected_layers) To avoid examining all possible neural pathway combinations, neurons in the initial layers (which process smaller, low-level features) could be left partially connected. Only in the final layers, where larger, high-level features are processed, are neurons in adjacent layers fully connected.

- Neural networks consist of multiple layers, each with hundreds of neurons governed by different activation rules. This makes it difficult to pinpoint a specific combination of input signals that would result in a correct prediction.

    A neural network's _black box_ characteristic could make it difficult to justify its use, especially in ethical decisions.

### Reinforcement Learning

[Reinforcement learning](https://en.wikipedia.org/wiki/Reinforcement_learning) is when algorithms use patterns in the data to make predictions and continuously improve themselves using _feedback from the results_.

#### [Multi-Armed Bandits](https://en.wikipedia.org/wiki/Multi-armed_bandit)

- Deals with the question of how to best allocate resources, whether to exploit known returns, or to search for better alternatives.
- One solution is to first explore available options before allocating all remaining resources to the best-performing option. This strategy is called [A/B testing](https://en.wikipedia.org/wiki/A/B_testing).
- Another solution is to steadily increase resources allocated to the best-performing option over time. This is known as the [epsilon-decreasing strategy](https://en.wikipedia.org/wiki/Multi-armed_bandit#Semi-uniform_strategies).
- While the epsilon-decreasing strategy gives higher returns than A/B testing in most cases, it is not easy to determine the optimal rate to update the allocation of resources.

**Limitations**

- To use an epsilon-decreasing strategy, it is crucial to control the value of epsilon. If epsilon decreases too slowly, the better option to exploit may be missed. If done too quickly, the wrong option may be exploited.

    To compute epsilon, a method called [Thompson sampling](https://en.wikipedia.org/wiki/Thompson_sampling) could be used.

## Evaluation Metrics

### [Classification](https://en.wikipedia.org/wiki/Statistical_classification)

#### Accuracy

The proportion of predictions that proved to be correct. Easy to understand, but leaves out information about _where_ prediction errors occur.

`accuracy = number of correct answers / total number of questions`

#### [Confusion Matrix](https://en.wikipedia.org/wiki/Confusion_matrix)

Compares and contrasts _True Positives (TP)_ and _True Negatives (TN)_ with _False Positives (FP)_ ([Type I errors](https://en.wikipedia.org/wiki/Type_I_and_type_II_errors#Type_I_error)) and _False Negatives (FN)_ ([Type II errors](https://en.wikipedia.org/wiki/Type_I_and_type_II_errors#Type_II_error)). Provides insight into where prediction model succeeded and where it failed.

<table>
  <caption>Confusion Matrix</caption>
  <tbody>
    <tr>
      <th>0</th>
      <td>True Negative</td>
      <td>False Positive</td>
    </tr>
    <tr>
      <th>1</th>
      <td>False Negative</td>
      <td>True Positive</td>
    </tr>
    <tr>
      <td></td>
      <th>0</th>
      <th>1</th>
    </tr>
  </tbody>
</table>

#### [Recall (Sensitivity, TPR)](https://en.wikipedia.org/wiki/Sensitivity_and_specificity#Sensitivity)

The proportion of True Positive (TP) target values (TP + FN) that a classification model accurately predicted. Describes how well a model understands the properties of the target class and how well it recognized the class (i.e., how well a model is _correctly predicting positives_).

The closer this value is to `1`, the better the model is at identifying/predicting TPs.

`Recall = TP / (TP + FN)`

#### [Precision (Specificity, TNR, Positive Predictive Value)](https://en.wikipedia.org/wiki/Sensitivity_and_specificity#Specificity)

The proportion of predicted positive target values (TP + FP) that are actually TPs. Detects whether a model is overdoing it by assigning too many positive labels (i.e., how well a classification model is _correctly identifying negatives_).

The closer this value is to `1`, the better the model is at discriminating between positive and negative targets.

`Precision = TP / (TP + FP)`

#### [F1](https://en.wikipedia.org/wiki/F-score)

The [harmonic mean](https://en.wikipedia.org/wiki/Harmonic_mean) of recall and precision. Helps to simultaneously control for both Recall and Precision.

The higher the quality of a model, the closer these values will be towards `1`.

`F1 = (2 x Precision x Recall) / (Precision + Recall)`

### [Regression](https://en.wikipedia.org/wiki/Regression_analysis)

#### [Root Mean Square Error (RMSE)](https://en.wikipedia.org/wiki/Mean_squared_error)

Square root of the **Mean Square Error (MSE)**, i.e., the average squared difference between the predicted and actual values across all data points. The MSE/RMSE give more weight to values further away from the mean. This makes them sensitive to outliers, which are severely penalized.

RMSE is expressed in _absolute_ values, i.e., in the same units as the target variable. Therefore, it needs to be compared to the RMSE of a random model (e.g., a constant model that always predicts the target mean).

`RMSE = sqrt(Sum of the squares of the observations' errors / Number of observations)`

#### Root Mean Squared Logarithmic Error (RMSLE)

Like the RMSE, but also accounts for the _direction_ of the error versus just the magnitude of the error. Used when you want to avoid under-estimates more than over-estimates.

#### [R^2 (Coefficient of Determination)](https://en.wikipedia.org/wiki/Coefficient_of_determination)

Serves as an indicator of a model's quality. If its value is `1`, the model has perfect prediction. It its value is `0`, the model is no better than the constant model. It its value is negative, the model quality is very low.

Expressed in _relative_ values. Therefore, it avoids having to be compare a regression model's MSE score to a constant regression model's MSE score.

`R^2 = 1 - (Model MSE / Mean MSE)`

[Mean Absolute Error (MAE)](https://en.wikipedia.org/wiki/Mean_absolute_error)

Average error of a regression model's predictions for a target from the target's mean. Taking the absolute error cancels out the difference between underfitting and overfitting (e.g., a few large errors will be balanced out by numerous small errors).

Therefore, the MAE is not sensitive to outliers like the MAE/RMSE are, i.e., all errors are equally penalized.

`MAE = (1 / N) N∑i=1 |y_i - ȳ_i|`

## Model Creation

### Classification

#### Logistic Regression

```python
# Enable creation of logistic regression classification models
from sklearn.linear_model import LogisticRegression

model = LogisticRegression(random_state=12345, solver='lbfgs')
```

<https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html>

#### Decision Tree

```python
# Enable creation of decision tree classification models
from sklearn.tree import DecisionTreeClassifier

model = DecisionTreeClassifier(random_state=12345, max_depth=3)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html>

#### Random Forest

```python
# Enable creation of random forest classification models
from sklearn.ensemble import RandomForestClassifier

model = RandomForestClassifier(random_state=12345, n_estimators=100)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html>

#### LightGBM

```python
# Enable creation of gradient boosting machine classification models
from lightgbm import LGBMClassifier

model = LGBMClassifier()
```

<https://lightgbm.readthedocs.io/en/latest/pythonapi/lightgbm.LGBMClassifier.html>

### Regression

#### Linear

```python
# Enable creation of linear regression models
from sklearn.linear_model import LinearRegression

model = LinearRegression()
```

<https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html>

#### Decision Tree

```python
# Enable creation of decision tree regression models
from sklearn.tree import DecisionTreeRegressor

model = DecisionTreeRegressor(random_state=12345, max_depth=3)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeRegressor.html>

#### Random Forest

```python
# Enable creation of random forest regression models
from sklearn.ensemble import RandomForestRegressor

model = RandomForestRegressor(random_state=12345, n_estimators=100)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html>

#### LightGBM

```python
# Enable creation of gradient boosting machine regression models
from lightgbm import LGBMRegressor

model = LGBMRegressor()
```

<https://lightgbm.readthedocs.io/en/latest/pythonapi/lightgbm.LGBMRegressor.html>

## Model Usage

### Training

Train a model with its `.fit()` method:

`model.fit(X_train, y_train)  # Train model`

### Predicting

Predict targets from validation or test data set features using a trained model's `.predict()` method:

`y_pred_val = model.predict(X_val)  # Predict target`

`y_pred_test = model.predict(X_test)  # Predict target`

## Model Evaluation

### Data Sets

- _Training_ - Used to train (i.e., fit) your model.
- _Validation_ - Used for unbiased model evaluation during hyperparameter tuning (e.g., for each set of hyperparameters, you fit the model with the training set and assess its performance with the validation set).

    For less complex cases where hyperparameter tuning is not applicable, a validation set may be unnecessary.

- _Test_ - Used for unbiased model evaluation of the **final** model.

### Data Splits

`train_test_split()` can be used for two-way training/test data set splits:

```python
# Enable splitting of arrays or matrices into random train and test subsets
from sklearn.model_selection import train_test_split

# Define features and target
X = df.drop('ex_tgt', axis=1)
y = df['ex_tgt']

# Two-way data split
X_train, X_test, y_train, y_test = \
    train_test_split(X, y, test_size=0.25, random_state=12345)
```

`np.split()` and `.sample()` can be used for three-way training, validation, and test data set splits:

```python
import numpy as np  # Enable scientific computing with Python

# Split DataFrame into three DataFrames (60%, 20%, 20%)
train, validate, test = np.split(df.sample(frac=1, random_state=12345),
                                 [int(.6 * df.shape[0]),
                                 int(.8 * df.shape[0])])

# Reset indices
train = train.reset_index(drop=True)
validate = validate.reset_index(drop=True)
test = test.reset_index(drop=True)

# Define training set features and target
X_train = train.drop(['ex_tgt'], axis=1)
y_train = train['ex_tgt']

# Define validation set features and target
X_val = validate.drop(['ex_tgt'], axis=1)
y_val = validate['ex_tgt']

# Define testing set features and target
X_test = test.drop(['ex_tgt'], axis=1)
y_test = test['ex_tgt']
```

<https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html>

<https://numpy.org/doc/stable/reference/generated/numpy.split.html>

#### Cross-Validation

You can use `cross_val_score()` to obtain an array of scores of the estimator for each run of the cross validation:

```python
# Enabling evaluating a score by cross-validation
from sklearn.model_selection import cross_val_score

scores = cross_val_score(model, X_train, y_train, scoring='accuracy', cv=5)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.cross_val_score.html>

<https://scikit-learn.org/stable/modules/model_evaluation.html#the-scoring-parameter-defining-model-evaluation-rules>

### Classification

#### Balanced Classes

##### Accuracy

Generate trained model target predictions:

`y_pred = model.predict(X_test)  # Predict target`

Use `accuracy_score()`:

```python
# Enable calculation of model accuracy score
from sklearn.metrics import accuracy_score

accuracy_score(y_test, y_pred)  # Calculate model accuracy
```

<https://scikit-learn.org/stable/modules/generated/sklearn.metrics.accuracy_score.html>

Or, use a classifier's `.score()` method:

`model.score(X_test, y_test)  # Calculate model accuracy`

#### Imbalanced Classes

Generate trained model target predictions:

`y_pred = model.predict(X_test)  # Predict target`

##### Confusion Matrix

```python
# Enable confusion matrix calculation
from sklearn.metrics import confusion_matrix

confusion_matrix(y_test, y_pred)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html>

##### Recall / Sensitivity / True Positive Rate

```python
# Enable recall score calculation
from sklearn.metrics import recall_score

recall_score(y_test, y_pred)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.metrics.recall_score.html>

##### Precision / Specificity / True Negative Rate

```python
# Enable precision score calculation
from sklearn.metrics import precision_score

precision_score(y_test, y_pred)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.metrics.precision_score.html>

##### F1

```python
from sklearn.metrics import f1_score  # Enable calculation of F1 score

f1_score(y_test, y_pred)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html>

##### Area Under Curve-Receiver Operating Characteristic (AUC-ROC)

```python
# Enable calculation of AUC-ROC score
from sklearn.metrics import roc_auc_score

# Calculate observation class target probabilities
probs_test = model.predict_proba(X_test)
# Extract positive class probabilities
probs_test_one = probs_test[:, 1]

roc_auc_score(y_test, probs_test_one)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.metrics.roc_auc_score.html>

### Regression

Generate trained model target predictions:

`y_pred = model.predict(X_test)  # Predict target`

#### Mean Square Error (MSE)

```python
# Enable calculation of MSE
from sklearn.metrics import mean_squared_error

mean_squared_error(y_test, y_pred)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.metrics.mean_squared_error.html>

#### Root Mean Squared Error (RMSE)

```python
# Enable calculation of MSE
from sklearn.metrics import mean_squared_error

mean_squared_error(y_test, y_pred, squared=False)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.metrics.mean_squared_error.html>

#### R^2 (Coefficient of Determination)

```python
from sklearn.metrics import r2_score  # Enable calculation of R^2

r2_score(y_test, y_pred)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.metrics.r2_score.html>

#### Mean Absolute Error (MAE)

```python
# Enable calculation of MAE
from sklearn.metrics import mean_absolute_error

mean_absolute_error(y_test, y_pred)
```

<https://scikit-learn.org/stable/modules/generated/sklearn.metrics.mean_absolute_error.html>

### Sanity Checks

#### Classification

##### Accuracy

```python
import pandas as pd  # Enable data structures and data analysis tools
from sklearn.metrics import accuracy_score  # Enable accuracy score calculation

# Define predicted constant
y_pred = pd.Series(0, index=y_test.index)

accuracy_score(y_test, y_pred)
```

#### Regression

##### Mean Square Error (MSE)

```python
import pandas as pd  # Enable data structures and data analysis tools
# Enable calculation of Mean Squared Error
from sklearn.metrics import mean_squared_error

# Define predicted constant
y_pred = pd.Series(y_train.mean(), index=y_test.index)

mean_squared_error(y_test, y_pred)
```

##### Mean Absolute Error (MAE)

```python
import pandas as pd  # Enable data structures and data analysis tools
# Enable calculation of Mean Absolute Error
from sklearn.metrics import mean_absolute_error

# Define predicted constant
y_pred = pd.Series(y_train.median(), index=y_test.index)

mean_absolute_error(y_test, y_pred)
```

## Model Import/Export

You can import and export models using [Joblib](https://joblib.readthedocs.io/en/latest/):

`python3 -m pip install joblib`

### Import

```python
# Enable reconstructing a Python object from a file persisted with joblib.dump
from joblib import load

model = joblib.load('ex_model.joblib')
```

<https://joblib.readthedocs.io/en/latest/generated/joblib.load.html>

### Export

```python
# Enable persisting an arbitrary Python object into one file
from joblib import dump

joblib.dump(model, 'ex_model.joblib')
```

<https://joblib.readthedocs.io/en/latest/generated/joblib.dump.html>

## Time Series

A time series is a [stochastic process](https://en.wikipedia.org/wiki/Stochastic_process), i.e., it has random variation and its distribution changes over time. It has a mean and variance, and these values also change over time.

A stochastic process is _stationary_ if its distribution does not change over time. An example of a stationary stochastic process is periodic fluctuations of values. If a distribution does change, the stochastic process is _nonstationary_.

A stationary time series is a time series where the mean and standard deviation do not change. When comparing multiple time series, the one where the mean and standard deviation change more slowly is said to be more stationary.

Nonstationary time series are harder to forecast because their properties change too quickly.

### Differences

_Times series differences_ is a sequence of differences between neighboring elements of a time series (i.e., the previous value is subtracted from the current one). This makes a time series more stationary, and hence easier to forecast.

To find time series differences, use the `DataFrame.shift()` method:

```python
# Create time series differences DataFrame
ts_diffs = df - df.shift(fill_value=0)
```

All values are shifted one step forward along the time axis. The last value of the series is lost because there is no place to shift it to. The resulting `NaN` value is filled via the `fill_value` argument (`fill_value=0`).

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.shift.html>

### Forecasting

_Forecasting_ seeks to develop a model that predicts the future values of a time series based on previous data. The length of time into the future for which the forecast is prepared is called a _forecast horizon_.

Since a time series model is intended to predict future values, the training set data **must** precede the test set data. Otherwise, model testing will be flawed.

### Feature Creation

Feature creation is used to create features for forecasting. The different types of features are:

- **Calendar Features** - Usually, trends and seasonality are linked to a specific date. The `datetime64[ns]` type in pandas contains the germane calendar information that can be extracted into separate columns.
- **Lag Features** - The previous values in the time series tell you whether the function `x(t)` (where `t = time`) will increase or decrease. The lag values are obtained using the `DataFrame.shift()` method.
- **Rolling Mean** - The rolling mean feature sets the general trend of the time series.

    The rolling mean at `t` takes into account the current value of the `x(t)` series, which _we do not want_ because the current value belongs to the target and not the features. **Make sure that you use lag features to exclude the current value when engineering the features** (i.e., use `DataFrame.shift()` to shift the target back one time interval prior to calculating the rolling mean).

```python
 # Create calendar features
df['month'] = df.index.month
df['day'] = df.index.day
df['day_of_week'] = df.index.dayofweek
df['hour'] = df.index.hour

# Create lag features
for i in range(1, ex_lag_vars_num + 1):
    df[f'lag_{i}'] = df['ex_tgt'].shift(i)

# Create rolling mean
df['rolling_mean'] = \
    df['ex_tgt'].shift().rolling(ex_rolling_mean_size).mean()

df = df.dropna()  # Drop rows missing values
```

### Data Split

```python
# Enable splitting of arrays or matrices into random train and test subsets
from sklearn.model_selection import train_test_split

# Define features and target
X = df.drop('ex_tgt', axis=1)
y = df['ex_tgt']

# Two-way data split
X_train, X_test, y_train, y_test = \
    train_test_split(X, y, shuffle=False, test_size=0.2)
```

Confirm:

```python
# Ensure that test data set starts AFTER training data set
output = (
    f' {"Data Set":^19} | {"Minimum Index":^19} | {"Maximum Index":^19} \n'
    f'{"":~^65}\n'
    f' {"Train":^19} | {y_train.index.min()} | '
    f'{y_train.index.max()} \n'
    f'{"":-^65}\n'
    f' {"Test":^19} | {y_test.index.min()} | {y_test.index.max()} '
)
print(output)
```

### Sanity Check

```python
# Define predicted value using the previous value in the time series
# Use latest value of train target as first value of test target
y_pred_prev = y_test.shift(fill_value=y_train.iloc[-1])
```

## Data Sets

- [MachineLearningMastery.com](https://github.com/jbrownlee/Datasets)
- [Real world datasets — scikit-learn](https://scikit-learn.org/stable/datasets/real_world.html)
- [Toy datasets — scikit-learn](https://scikit-learn.org/stable/datasets/toy_dataset.html)
- [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets.php)

## Documentation

- [Chatbot Arena](https://lmarena.ai/?leaderboard)
- [Dark Visitors](https://darkvisitors.com/)
- [GPT4All](https://gpt4all.io/index.html)
- [HuggingChat AI Playground](https://huggingface.co/chat/)
- [imbalanced-learn Documentation](https://imbalanced-learn.org/stable/)
- [LightGBM](https://github.com/microsoft/LightGBM)
- [`LLM`](https://github.com/simonw/llm)
- [LM Studio](https://lmstudio.ai/)
- [Natural Language Toolkit (NLTK)](https://www.nltk.org/)
- [Open LLM Leaderboard](https://huggingface.co/spaces/open-llm-leaderboard/open_llm_leaderboard#/)
- [Perplexity Labs AI Playground](https://labs.perplexity.ai/)
- [scikit-learn User Guide](https://scikit-learn.org/stable/user_guide.html)
    - [`scoring` Parameter](https://scikit-learn.org/stable/modules/model_evaluation.html#the-scoring-parameter-defining-model-evaluation-rules)
- [spaCy](https://spacy.io/)
- [Vercel AI Playground](https://sdk.vercel.ai/playground)