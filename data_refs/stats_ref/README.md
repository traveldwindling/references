# Statistics Reference

A collection of statistics reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## Special Symbols

<table>
  <caption>Special Symbols</caption>
  <thead>
    <tr>
      <th>Symbol</th>
      <th>Name</th>
      <th>TeX</th>
      <th>Usage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>α</td>
      <td>Greek small letter alpha</td>
      <td><code>\alpha</code></td>
      <td>Probability of a Type I Error (False Positive Rate)</td>
    </tr>
    <tr>
      <td>β</td>
      <td>Greek small letter beta</td>
      <td><code>\beta</code></td>
      <td>Probability of a Type II Error (False Negative Rate)</td>
    </tr>
    <tr>
      <td>χ</td>
      <td>Greek small letter chi</td>
      <td><code>\chi</code></td>
      <td>Continuous probability distribution</td>
    </tr>
    <tr>
      <td>⋅</td>
      <td>Dot operator</td>
      <td><code>\cdot</code></td>
      <td>Multiplication or dot product operator</td>
    </tr>
    <tr>
      <td>=</td>
      <td>Equivalent</td>
      <td><code>\equiv</code></td>
      <td>Relationship expression or boolean logical operator</td>
    </tr>
    <tr>
      <td>ε</td>
      <td>Greek small letter epsilon</td>
      <td><code>\epsilon</code></td>
      <td>Represent error (residual) bounds</td>
    </tr>
    <tr>
      <td>η</td>
      <td>Greek small letter eta</td>
      <td><code>\eta</code></td>
      <td>Effect size calculations</td>
    </tr>
    <tr>
      <td>≥</td>
      <td>Greater than or equal</td>
      <td><code>\geq</code></td>
      <td>Relationship expression or boolean logical operator</td>
    </tr>
    <tr>
      <td>∞</td>
      <td>Infinity</td>
      <td><code>\infty</code></td>
      <td>Concept of infinity</td>
    </tr>
    <tr>
      <td>λ</td>
      <td>Greek small letter lambda</td>
      <td><code>\lambda</code></td>
      <td>Density of occurrences within a time interval as modelled by the Poisson distribution</td>
    </tr>
    <tr>
      <td>≤</td>
      <td>Less than or equal</td>
      <td><code>\leq</code></td>
      <td>Relationship expression or boolean logical operator</td>
    </tr>
    <tr>
      <td>μ</td>
      <td>Greek small letter mu</td>
      <td><code>\mu</code></td>
      <td>Population mean of a distribution</td>
    </tr>
    <tr>
      <td>∇</td>
      <td>Nabla</td>
      <td><code>\nabla</code></td>
      <td>The gradient operator</td>
    </tr>
    <tr>
      <td>≠</td>
      <td>Not equal</td>
      <td><code>\neq</code></td>
      <td>Relationship expression or boolean logical operator</td>
    </tr>
    <tr>
      <td>∂</td>
      <td>Partial</td>
      <td><code>\partial</code></td>
      <td>Partial differential</td>
    </tr>
    <tr>
      <td>π</td>
      <td>Greek small letter pi</td>
      <td><code>\pi</code></td>
      <td>True Positive Rate</td>
    </tr>
    <tr>
      <td>ρ</td>
      <td>Greek small letter rho</td>
      <td><code>\rho</code></td>
      <td>Population correlation coefficient</td>
    </tr>
    <tr>
      <td>Σ</td>
      <td>Greek capital letter sigma</td>
      <td><code>\Sigma</code></td>
      <td>Summation operator</td>
    </tr>
    <tr>
      <td>σ</td>
      <td>Greek small letter sigma</td>
      <td><code>\sigma</code></td>
      <td>Standard deviation of a population</td>
    </tr>
    <tr>
      <td>x̄</td>
      <td>X bar</td>
      <td><code>\bar{x}</code></td>
      <td>Sample mean of a distribution</td>
    </tr>
    <tr>
      <td>ȳ</td>
      <td>Y bar</td>
      <td><code>\bar{y}</code></td>
      <td>Sample mean of a distribution</td>
    </tr>
    <tr>
      <td>ŷ</td>
      <td>Y hat</td>
      <td><code>\hat{y}</code></td>
      <td>Estimated (predicted) value of y obtained using a regression equation</td>
    </tr>
  </tbody>
</table>

## Variable Types

[Quantitative data](https://en.wikipedia.org/wiki/Quantitative_research) is information recorded as numbers. It represents an objective measurement or a count. Temperature, weight, and a count of transactions are examples of quantitative variables. Quantitative variables are also referred to as _numerical_ variables.

[Qualitative data](https://en.wikipedia.org/wiki/Qualitative_property) is information that represents characteristics that are not measured with numbers. Instead, observations fall within a countable number of groups. This variable type can capture information that is not easily measured and can be subjective. Taste, eye color, and marital status are examples of qualitative variables.

Within these two broad divisions, there are various variable subtypes.

### Quantitative Data Subtypes

There are two types of quantitative variables:

1. Continuous
2. Discrete

#### Continuous Data

[Continuous variables](https://en.wikipedia.org/wiki/Continuous_or_discrete_variable#Continuous_variable) can take on any numeric value and can be meaningfully divided into smaller increments, including fractional and decimal values. There are an _infinite_ number of possible values between any two values.

Typically, you measure continuous variables on a scale. With continuous variables, you can assess properties like the mean, median, distribution, range, and standard deviation.

#### Discrete Data

[Discrete variables](https://en.wikipedia.org/wiki/Continuous_or_discrete_variable#Discrete_variable) are a count of the presence of a characteristic, result, item, or activity. These measures cannot be meaningfully divided into smaller increments. There are a _finite_ number of possible values that you can record for an observation.

With discrete variables, you can calculate and assess a rate of occurrence or a summary of the count, such as the mean, sum, and standard deviation.

### Qualitative Data Subtypes

When you record information that categorizes your observations, you are collecting qualitative data. There are three types of qualitative variables:

- Categorical
- Binary
- Ordinal

With these data types, proportions of each category are often of interest. Hence, bar charts and pie charts are conventional methods for graphing qualitative variables because they are useful for displaying the relative percentage of each group out of the entire sample.

Less information is gleaned from qualitative data versus quantitative data. In cases where you have a choice between recording a characteristic as a continuous or qualitative variable, it is usually best to record the continuous data.

#### Categorical Data

[Categorical variables](https://en.wikipedia.org/wiki/Categorical_variable) have values that you can put into a countable number of distinct groups based on a characteristic. For a categorical variable, you can assign categories, but the categories _have no natural order_.

Categorical variables are also referred to as _attribute_ or _nominal_ variables.

#### Binary Data

[Binary variables](https://en.wikipedia.org/wiki/Binary_variable) can only have two values. If you can place an observation into only two categories, you have a binary variable. They are helpful for calculating proportions or percentages.

Binary variables are also referred to as _dichotomous_ or _indicator_ variables.

#### Ordinal Data

[Ordinal variables](https://en.wikipedia.org/wiki/Ordinal_variable) have at least three categories, and the categories _have a natural order_. Often, ordinal variables are considered to have a combination of qualitative and quantitative properties.

These variables are frequently represented using numbers. In number form, you can calculate average scores as with quantitative variables. However, the numbers have limited usefulness because the differences between ranks might not be constant.

For example, first, second, and third place in a race are ordinal data. However, the difference in time between first and second place might not be the same as the difference in time between second and third place.

## Visualizing Data

Single sets of numbers can be visualized in strip-charts, box plots, and histograms.

Pairs of numbers can be visualized as scatter plots. Time-series can be visualized as line graphs.

Infographics highlight interesting features and can guide viewers through a story, but should be used with awareness of their _purpose_ and their _impact_.

Usually, the independent/predictor variable is plotted on the x-axis, and the dependent/criterion variable is plotted on the y-axis.

There are four common features of a good data visualization:

1. It contains reliable information.
2. The design has been chosen so that relevant patterns become conspicuous.
3. It is presented in an attractive manner, but appearance should not get in the way of _honesty, clarity, and depth_.
4. When appropriate, it is organized in a way that enables exploration.

There are _many_ types of visualizations. It can be helpful to group them by their function.

### Visualizations By Function

#### Comparisons

- [Bubble chart](https://en.wikipedia.org/wiki/Bubble_chart)
- Bullet chart
- [Choropleth map](https://en.wikipedia.org/wiki/Choropleth_map)
- [Concent map](https://en.wikipedia.org/wiki/Concept_map)
- Diverging [bar chart](https://en.wikipedia.org/wiki/Bar_chart)
- [Donut chart](https://en.wikipedia.org/wiki/Pie_chart#Doughnut_chart)
- [Pie chart](https://en.wikipedia.org/wiki/Pie_chart)
- [Treemap](https://en.wikipedia.org/wiki/Treemapping)

#### Data Over Time

- [Area chart](https://en.wikipedia.org/wiki/Area_chart)
- Bubble chart
- [Line chart](https://en.wikipedia.org/wiki/Line_chart)
- Stacked area chart
- Unstacked area chart

#### Distribution

- [Box plot](https://en.wikipedia.org/wiki/Box_plot)
- Density curve with histogram
- Density plot
- [Histogram](https://en.wikipedia.org/wiki/Histogram)
- [Population pyramid](https://en.wikipedia.org/wiki/Population_pyramid)
- Strip plot
- [Violin plot](https://en.wikipedia.org/wiki/Violin_plot)

#### Ranking

- Horizontal bar chart
- Lollipop chart
- Multi-set bar chart
- Stacked bar chart
- Vertical bar chart

#### Relationship

- Bubble chart
- [Heat map](https://en.wikipedia.org/wiki/Heat_map)
- Marginal histogram
- Pair plot
- [Scatter plot](https://en.wikipedia.org/wiki/Scatter_plot)

### Framing

Creating an appropriate graph type and expressing proportions requires careful design choices of color, font, and language to ensure engagement and readability. An audience's emotional response to something like a table may also be influenced by the choice of which columns to display.

This phenomenon is known as negative or positive _framing_ (e.g., 5% mortality sounds worse than 95% survival). Ideally, both positive and negative frames should be presented if you want to provide impartial information.

When creating a table, the order of both the columns and rows should be considered. For graphs, a crucial choice is where to start the horizontal axis. It is likely best to start with a _logical and meaningful baseline_.

## Statistical Models

A [statistical model](https://en.wikipedia.org/wiki/Statistical_model) is a formal representation of the relationships between variables that can be used for the desired explanation or prediction. Two commonly used models in statistics are [analysis of variance (ANOVA)](https://en.wikipedia.org/wiki/Analysis_of_variance) and [regression](https://en.wikipedia.org/wiki/Regression_analysis) models.

Both models are used with a continuous response variable. In ANOVA, this variable is referred to as the _dependent_ variable. In regression, this variable is referred to as the _criterion_ variable.

ANOVA is used when the _independent_ variables are categorical variables.

Regression is used when the _predictor_ variables are continuous. Regression can be used with categorical predictor variables if they are first converted into [dummy variables](https://en.wikipedia.org/wiki/Dummy_variable_(statistics)), which are continuous.

## Assumption of Variance

The most common statistical tests and procedures that make an assumption of equal variance include:

1. _t_-tests
2. ANOVA
3. Linear Regression

### _t_-tests

A two sample _t_-test is used to test whether or not the means of two independent populations are equal.

The test makes the assumption that the variances of these two groups are equal. There are two ways to test if this assumption is met:

1. Use the rule of thumb ratio.

    If the ratio of the larger variance to the smaller variance is less than 4, we can assume that the variances are approximately equal and use the two sample _t_-test.

2. Perform an _F_-test.

    The [_F_-test](https://en.wikipedia.org/wiki/F-test) tests the null hypothesis that the groups have equal variances versus the alternative hypothesis that the groups do not have equal variances. If the _p_-value of the test is less than some significance level (e.g., 0.05), then we have evidence to say that the groups do not have equal variances.

If this assumption is violated, we can perform [Welchs _t_-test](https://en.wikipedia.org/wiki/Welch%27s_t-test), which is a non-parametric version of the two sample t-test and does not make the assumption that the two groups have equal variances.

### ANOVA

An ANOVA is used to determine whether or not there is a significant difference between the means of three or more independent populations.

An ANOVA assumes that each of the groups has equal variance. There are two ways to test if this assumption is met:

1. Create box plots.

    The longer the box, the higher the variance.

2. Conduct [Bartlett's Test](https://en.wikipedia.org/wiki/Bartlett%27s_test).

    Bartlett's Test tests the null hypothesis that the groups have equal variances versus the alternative hypothesis that the groups do not have equal variances. If the _p_-value of the test is less than some significance level (e.g., 0.05), then we have evidence to say that the groups do not have equal variances.

In general, ANOVAs are considered to be fairly robust against violations of the equal variances assumption, as long as each sample has the _same size_.

If the sample sizes are not the same and this assumption is severely violated, you could instead use the [Kruskal-Wallis Test](https://en.wikipedia.org/wiki/Kruskal%E2%80%93Wallis_one-way_analysis_of_variance), which is a non-parametric version of the one-way ANOVA.

### Linear Regression

Linear regression is used to quantify the relationship between one or more predictor variables and a criterion variable.

Linear regression makes the assumption that the _residuals_ (i.e., the difference between an observed value and a predicted value) have constant variance at every level of the predictor variable(s). This is known as [homoscedasticity](https://en.wikipedia.org/wiki/Homoscedasticity) (i.e., a sequence of random variables that have the same finite variance).

When this is not the case, the residuals are said to suffer from [heteroscedasticity](https://en.wikipedia.org/wiki/Heteroscedasticity) and the results of the regression analysis become unreliable.

The most common way to determine if this assumption is met is to create a plot of residuals versus fitted (predicted) values. If the residuals in this plot seem to be randomly scattered around zero, then the assumption of homoscedasticity is likely met.

However, if there exists a _systematic pattern_ in the residuals (e.g., a distinctive cone shape in the plotted data), then heteroscedasticity is a problem.

If this assumption is violated, a common way to deal with it is to transform the criterion variable using one of the following three transformations:

1. _Log Transformation_: Transform the criterion variable from `y` to `log(y)`.
2. _Square Root Transformation_: Transform the criterion variable from `y` to `sqrt(y)`.
3. _Cube Root Transformation_: Transform the criterion variable from `y` to `y^1/3`.

Another way to fix heteroscedasticity is to use [weighted least squares regression](https://en.wikipedia.org/wiki/Weighted_least_squares). This type of regression assigns a weight to each data point based on the variance of its fitted value.

This gives small weights to data points that have higher variances, which shrinks their squared residuals. When the proper weights are used, this can eliminate the problem of heteroscedasticity.

## Hypothesis Tests Based on Data Types

In statistics, the type of variable greatly determines which kinds of hypothesis tests you can perform.

### Quantitative Data

#### Continuous Data

Two common hypothesis tests that can be used with continuous data are _t_-tests to assess means and variance tests to assess dispersion around the mean. Both tests come in one-sample and two-sample versions.

One-sample tests allow you to compare your sample estimate to a target value. The two-sample tests let you compare the sample estimates to each other.

- [Student's t-test](https://en.wikipedia.org/wiki/Student%27s_t-test)
- [Levene's test](https://en.wikipedia.org/wiki/Levene%27s_test)

There is also a group of tests that assess the median rather than the mean. These are known as [nonparametric tests](https://en.wikipedia.org/wiki/Nonparametric_statistics) and are less frequently used.

However, consider using a nonparametric test if your data are highly skewed and the median better represents the actual center of your data than the mean.

#### Regression Analysis With Continuous Criterion Variable

[Linear regression](https://en.wikipedia.org/wiki/Linear_regression) (also known as ordinary least squares (OLS) and linear least squares) helps you understand the mean change in a criterion variable given a one-unit change in each predictor variable. In addition, [polynomials](https://en.wikipedia.org/wiki/Polynomial) can be used to model curvature and include interaction effects.

Linear regression estimates parameters by minimizing the [sum of the squared errors (SSE)](https://en.wikipedia.org/wiki/Residual_sum_of_squares). If you have a continuous criterion variable, linear regression is probably the first type of regression that you should consider.

[Nonlinear regression](https://en.wikipedia.org/wiki/Nonlinear_regression) also requires a continuous criterion variable, but it provides greater flexibility to fit curves than linear regression.

Like linear regression, nonlinear regression estimates the parameters by minimizing the SSE. However, nonlinear models use an iterative algorithm, rather than the linear approach of solving them directly with matrix equations.

Therefore, you need to be concerned with:

- Determining which algorithm to use
- Specifying good starting values
- The possibility of either not converging on a solution, or converging on a local minimum rather than a global minimum SSE
- Specifying the correct functional form

It may be best to first fit a model using linear regression, and then determine whether the linear model provides an adequate fit by checking the residual plots. If you cannot obtain a good fit using linear regression, try a nonlinear model because it can fit a wider variety of curves.

- [Lasso regression](https://en.wikipedia.org/wiki/Lasso_(statistics))
- [Partial least squares regression](https://en.wikipedia.org/wiki/Partial_least_squares_regression)
- [Ridge regression](https://en.wikipedia.org/wiki/Ridge_regression)

#### Discrete Data

[Count data](https://en.wikipedia.org/wiki/Count_data) can only have non-negative integers (e.g., 0, 1, 2). Count data is distinct from ordinal data, which is ranked. Often, count data is modeled using the Poisson distribution.

The [Poisson distribution](https://en.wikipedia.org/wiki/Poisson_distribution) is a discrete probability distribution that expresses the probability of a given number of events occurring in a fixed interval of time or space, if these events occur with a known constant mean rate and independently of the time since the last event. Whereas the normal (Gaussian) distribution requires two parameters (i.e., the population mean and standard deviation), the Poisson distribution only depends on its mean.

Poisson data are a count of the presence of a characteristic, result, or activity over a constant amount of time, area, or other length of observation. With Poisson data, you can assess a rate of occurrence.

- Two-sample Poisson rate test

#### Regression Analysis With Discrete Criterion Variable

If your criterion variable is a count of items, events, results, or activities, you might need to use a different type of regression model. Count data with higher means tend to be normally distributed and you can often use linear regression.

However, count data with smaller means can be skewed, and linear regression might have a hard time fitting these data. For these cases, there are several types of models you can use.

- [Poisson regression](https://en.wikipedia.org/wiki/Poisson_regression)

Not all count data follow the Poisson distribution because this distribution has some stringent restrictions. There are alternative analyses you can perform when you have count data.

- Negative binomial regression
- [Zero-inflated models](https://en.wikipedia.org/wiki/Zero-inflated_model)

### Qualitative Data

#### Categorical Data

The [Chi-square test of independence](https://en.wikipedia.org/wiki/Chi-squared_test) determines whether there is a statistically significant relationship between categorical variables. It is a hypothesis test that answers whether the values of one categorical variable depend on the value of other categorical variables.

#### Binary Data

Binary data are useful for calculating proportions or percentages. Hypothesis tests that assess proportions require binary data and allow you to use sample data to make inferences about the proportions of populations.

- [Fisher's exact test](https://en.wikipedia.org/wiki/Fisher%27s_exact_test)
- One proportion z-test

#### Regression Analysis With Categorical Criterion Variable

[Logistic regression](https://en.wikipedia.org/wiki/Logistic_regression) transforms the criterion variable and then uses [maximum likelihood estimation (MLE)](https://en.wikipedia.org/wiki/Maximum_likelihood_estimation), rather than least squares, to estimate the parameters. It was developed for proportions and ensures that the derived probability curve cannot go above 100% or below 0%.

Logistic regression describes the relationship between a set of predictor variables and a categorical criterion variable. Choose the type of logistic model based on the type of categorical criterion variable that you have.

- [Binary regression](https://en.wikipedia.org/wiki/Binary_regression)
- [Ordinal regression](https://en.wikipedia.org/wiki/Ordered_logit)
- Nominal regression

## Correlation

### Pearson Correlation Coefficient

The [Pearson correlation coefficient](https://en.wikipedia.org/wiki/Pearson_correlation_coefficient) is appropriate when you have a pair of continuous variables and their relationship is linear. It runs between `-1` and `+1`, and expresses how close to a straight line the data points fall.

A correlation of `+1` occurs if all the points lie on a straight line going upwards, while a correlation of `-1` occurs if all the points lie on a straight line going downwards. A correlation near `0` can come from a random scatter of points, or any other pattern in which there is no systematic trend upwards or downwards.

The sign of the coefficient indicates whether it is a positive or negative relationship. A positive correlation means that as one variable increases, the other variable also tends to increase. A negative correlation signifies that as one variable increases, the other tends to decrease.

Values close to `-1` or `+1` represent stronger relationships than values closer to zero.

### Spearman's Rank Correlation Coefficient

[Spearman's rank correlation coefficient](https://en.wikipedia.org/wiki/Spearman%27s_rank_correlation_coefficient) is appropriate when you have a pair of continuous variables and their relationship is _not_ linear, or you have a pair of ordinal variables. It depends on the ranks of the data rather than their specific values. This means that it can be near `+1` or `-1` if the points are close to a line that steadily increases or decreases, _even if this line is not straight_.

Spearman's rank correlation coefficient requires that your continuous data follow a [monotonic](https://en.wikipedia.org/wiki/Monotonic_function) relationship or be ordinal data.

In a monotonic relationship, as one variable increases, the other variable tends to either increase or decrease, but not necessarily in a straight line. This aspect of Spearman's correlation allows you to fit curvilinear relationships. However, there must be a tendency to change in a specific direction.

Spearman's rank correlation coefficient is often referred to as Spearman's `ρ` (rho).

### Correlation Issues

Correlation coefficients are simply summaries of association and _cannot be used to conclude_ that there is definitely an underlying relationship between predictors and a criterion, or why a relationship might exist.

[Sampling bias](https://en.wikipedia.org/wiki/Sampling_bias) occurs when a sample is created in such a way that some members of a population are more likely to be included than others (e.g., a researcher's knowledge of which patients were getting which treatments in clinical trials), which can result in a biased sample. If this is not accounted for, the results of the study are skewed.

People have a tendency for [apophenia](https://en.wikipedia.org/wiki/Apophenia), i.e., the predilection to construct reasons for a connection between what are actually unrelated events.

The _statistical_ idea of causation is not strictly deterministic. That is, when we say that _X_ causes _Y_, we do not mean that every time _X_ occurs, _Y_ will occur too, or that _Y_ will only occur if _X_ occurs. We mean that if we intervene and force _X_ to occur, then _Y_ tends to happen more often.

We can never say that _X_ caused _Y_ _in a specific case_, only that _X_ increases the proportion of times that _Y_ happens. This has two vital consequences for what we have to do if we want to know what causes what:

1. In order to infer causation with real confidence, we ideally need to intervene and perform _experiments_.
2. Since we live in a [stochastic](https://en.wikipedia.org/wiki/Stochastic) world, we need to intervene more than once in order to amass evidence.

## Effect Sizes

Statistical significance is roughly the probability of finding your data if some hypothesis is true (i.e., the null hypothesis). If this probability is low, then this hypothesis was probably not true (i.e., we reject the null hypothesis). This may be a nice first step, but what we really need to know is _how much do the data differ from the hypothesis_?

An [effect size](https://en.wikipedia.org/wiki/Effect_size) summarizes this answer in a single, interpretable number. This is important because:

- Effect sizes allow us to compare effects, both within and across studies.
- We need an effect size measure to estimate power (`1 - β`, the probability of correctly identifying a true effect).
- Even before collecting data, effect sizes tell us which sample sizes we need to obtain a given level of power, often `0.80`.

It is common to organize effect size statistical methods into groups, based on the type of effect that is to be quantified. Two main groups of methods for calculating effect size are:

1. _Association_ - Statistical methods for quantifying an association between variables (e.g., correlation).
2. _Difference_ - Statistical methods for quantifying the difference between variables (e.g., difference between means).

The result of an effect size calculation must be interpreted and the interpretation depends on the statistical method used. A measure must be chosen based on the goals of the interpretation. Three types of calculated result include:

1. _Standardized Result_ - The effect size has a standard scale allowing it to be generally interpreted regardless of application (e.g., Cohen's _d_ calculation).
2. _Original Units Result_ - The effect size may use the original units of the variable, which can aid in the interpretation within the domain (e.g., difference between two sample means).
3. _Unit Free Result_ - The effect size may not have units such as a count or proportion (e.g., a correlation coefficient).

The effect size does not replace the results of a statistical hypothesis test. Instead, the effect size _complements_ the test. Ideally, the results of both the hypothesis test and the effect size calculation would be presented side-by-side.

- _Hypothesis Test_: Quantify the likelihood of observing the data given an assumption (null hypothesis).
- _Effect Size_: Quantify the size of the effect assuming that the effect is present.

### Chi-Square Tests

Chi-Square tests are used to determine if there is a statistically significant difference between the expected frequencies and observed frequencies between categorical variables.

Common effect size measures for chi-square tests are:

- Cohen's `ω` (omega)
- [Cramer's _V_](https://en.wikipedia.org/wiki/Cram%C3%A9r%27s_V) (chi-square independence test)
- [Contingency Coefficient](https://en.wikipedia.org/wiki/Contingency_table#Cram%C3%A9r's_V_and_the_contingency_coefficient_C) (chi-square independence test)

#### Cohen's `ω`

[Cohen's `ω`](https://en.wikipedia.org/wiki/Effect_size#Cohen's_omega_(%CF%89)) is the effect size measure of choice for:

- chi-square independence test
- chi-square goodness-of-fit test

Basic rules of thumb for Cohen's `ω` are:

- small effect: `ω` = 0.10
- medium effect: `ω` = 0.30
- large effect: `ω` = 0.50

Power and required sample sizes for chi-square tests cannot be directly computed from Cohen's `ω`. They depend on the [degrees of freedom (df)](https://en.wikipedia.org/wiki/Degrees_of_freedom_(statistics)) for the test.

### _t_-tests

Common effect size measures for _t_-tests are:

- Cohen's _d_ (all _t_-tests)
- Point-Biserial Correlation (independent samples _t_-test)

#### Cohen's _d_

[Cohen's _d_](https://en.wikipedia.org/wiki/Effect_size#Cohen's_d) is the effect size measure of choice for all three _t_-tests:

- one sample _t_-test
- independent samples _t_-test
- paired samples _t_-test

Basic rules of thumb are that:

- small effect: _d_ = 0.20
- medium effect: _d_ = 0.50
- large effect: _d_ = 0.80

### Pearson Correlations

For a Pearson correlation, the correlation itself (often denoted as _r_) is interpretable as an effect size measure.

Basic rules of thumb are that:

- small effect: _r_ = 0.10
- medium effect: _r_ = 0.30
- large effect: _r_ = 0.50

### ANOVA

Common effect size measures for ANOVA are:

- `η`_p^2 (partial eta squared)
- Cohen's _F_
- `ω`^2 (omega-squared)

#### Partial Eta Squared

Partial eta squared (`η`_p^2) is the effect size of choice for:

- ANOVA (between-subjects, one-way or factorial)
- repeated measures ANOVA (one-way or factorial)
- mixed ANOVA

Basic rules of thumb are that:

- small effect: `η`_p^2 = 0.01
- medium effect: `η`_p^2 = 0.06
- large effect: `η`_p^2 = 0.14

#### Cohen's _f_

Cohen's _f_ is an effect size measure for:

- ANOVA (between-subjects, one-way or factorial)
- repeated measures ANOVA (one-way or factorial)
- mixed ANOVA

Basic rules of thumb for Cohen's _f_ are that:

- small effect: _f_ = 0.10
- medium effect: _f_ = 0.25
- large effect: _f_ = 0.40

Power and required sample sizes for ANOVA can be computed from Cohen's _f_ and some other parameters.

#### Omega Squared

A less common, but perhaps better, alternative for partial eta-squared is [omega squared (`ω`^2)](https://en.wikipedia.org/wiki/Effect_size#Omega-squared_(%CF%892)).

Similarly to partial eta squared, `ω`^2 estimates which proportion of variance in the dependent variable is accounted for by an effect in the entire population. The latter, however, is a less biased estimator.

Basic rules of thumb are:

- small effect: `ω`^2 = 0.01
- medium effect: `ω`^2 = 0.06
- large effect: `ω`^2 = 0.14

### Linear Regression

Effect size measures for (simple and multiple) linear regression are:

- _f_^2 (entire model and individual predictor)
- _R_^2 (entire model)
- *r*_part^2 - squared semipartial (or _part_) correlation (individual predictor)

#### _f_-Squared

The effect size measure of choice for (simple and multiple) linear regression is _f_^2.

Basic rules of thumb are that:

- small effect: _f_^2 = 0.02
- medium effect: _f_^2 = 0.15
- large effect: _f_^2 = 0.35

_f_^2 is useful for computing the power and/or required sample size for a regression model or individual predictor. However, these also depend on the number of predictors involved.

## Descriptive Statistics

A _sample distribution_ is the data pattern from the participants in a sample. It is also referred to as a _data distribution_ or an [empirical distribution](https://en.wikipedia.org/wiki/Empirical_distribution_function).

A distribution is _skewed_ when it is not roughly symmetric around a central value, due to the occurrence of extreme values. [Skewness](https://en.wikipedia.org/wiki/Skewness) refers to where the tail of the data is being pulled by the more extreme values (the majority of the data will lie in the opposite direction of the skew), e.g., right-skewed, left-skewed.

To reduce the impact of extreme data points, data can be transformed. For example, it can be plotted on a [logarithmic scale](https://en.wikipedia.org/wiki/Logarithmic_scale), where the space between 100 and 1,000 is the same as the space between 1,000 and 10,000.

When a set of counts or continuous observations are reduced to a single summary statistic, this is called an [average](https://en.wikipedia.org/wiki/Average). There are three basic interpretations of the term average:

1. _Mean_ - The sum of the numbers divided by the number of cases.
2. _Median_ - The middle value when the numbers are put in order (i.e., the 50th [percentile](https://en.wikipedia.org/wiki/Percentile)).
3. _Mode_ - The most common value.

The above values are also known as _measures of the location of the data distribution_.

Mean averages can be very misleading when the raw data do not form a symmetric pattern around a central value, but are instead skewed towards one side.

There are three ways to summarize a data distribution's spread:

1. _Range_ - The minimum to maximum value of a data distribution.
2. _Inter-Quartile Range (IQR)_ - The distance between the 25th and 75th percentiles of the data (i.e., the central half of the numbers). The box in a box plot represents the IQR.
3. _Standard Deviation_ - The most technically complex measure that is only appropriate for symmetric data, since it is unduly influenced by [outliers](https://en.wikipedia.org/wiki/Outlier).

Often, data has errors, outliers, and other strange values. These do not necessarily need to be individually identified and excluded.

The median and IQR are considered _robust measures_, as they are not unduly affected by odd observations.

Using a distribution's location and spread can take you a long way to grasping an overall pattern. However, there is no substitute for simply _looking at data properly_.

It is valuable to split data according to a factor that explains some of the overall variability.

## Inferential Statistics

[Deduction](https://en.wikipedia.org/wiki/Deductive_reasoning) is the process of using the rules of logic to work from general premises to particular conclusions.

[Induction](https://en.wikipedia.org/wiki/Inductive_reasoning) is taking particular instances and trying to work out general conclusions.

Deduction is logically certain, whereas induction is generally uncertain.

Inferential statistics makes estimates about populations using samples.

### The Inductive Inference Process

1. _Data_ - The recorded raw data.

    When going from Stage 1 to Stage 2, there are problems of _measurement_ (i.e., is what we record in our data an accurate reflection of what we are interested in?). We want our data to be:

    - Reliable, in the sense of having low variability from occasion to occasion, and so being a precise or repeatable number.
    - Valid, in the sense of measuring what we really want to measure, and not having systematic bias.

2. _Sample_ - The true number of participants in the sample.

    When going from Stage 2 to Stage 3, there could be [internal validity](https://en.wikipedia.org/wiki/Internal_validity) issues, which will depend on the fundamental quality of the study (i.e., was a causal relationship confidently established between treatment and outcome, how well was the study performed, are the study's conclusions correct). A crucial way to avoid bias is to use [random sampling](https://en.wikipedia.org/wiki/Simple_random_sample).

    If you want to be able to generalize from a sample to the population, you need to make sure that the sample is representative. Simply having large quantities of data does not necessarily help guarantee a good sample and can give false reassurance.

3. _Study Population_ - The number of participants in the study population, i.e., the ones that could have potentially been included in the study.

    When going from Stage 3 to Stage 4, there may be problems where the results do not reflect what we wanted to investigate if we have not been able to engage the participants in whom we are particularly interested in. This is an [external validity](https://en.wikipedia.org/wiki/External_validity) issue.

4. _Target Population_ - The number of participants in the target population.

When we have all the data (i.e., the sample is essentially the same as the study population), it is straightforward to produce statistics that describe what has been measured, but when we want to use the data to draw broader conclusions about what is happening, the _quality of the data_ becomes paramount. We need to be alert to the kind of systematic biases that can jeopardize the reliability of claims.

When describing a sample, metrics are referred to as _statistics_. When describing a population, metrics are referred to as _parameters_.

A population can be thought of as a physical group of individuals, but also as providing the _probability distribution_ for a random observation drawn from that population.

### The Normal Distribution

A [normal distribution](https://en.wikipedia.org/wiki/Normal_distribution) (also referred to as a Gaussian distribution) is a type of continuous probability distribution for a real-valued random variable. The normal distribution can be expected to occur for phenomena that are driven by large numbers of small influences (e.g., a complex physical trait that is not influenced by only a handful of genes).

When dealing with a normal distribution, informative assumptions can be made:

- 95% of the data is contained within the interval given by the mean +-2 standard deviations
- 99.7% of the data is contained within the interval given by the mean +-3 standard deviations

The mean and standard deviation are useful summary descriptions for many distribution types, but other measures are also helpful for normal distributions. For example, the 25th and 75th percentiles are known as the [quartiles](https://en.wikipedia.org/wiki/Quartile) and the distance between them is the IQR, which is a measure of the spread of the distribution.

#### The Standard Normal Distribution

The _standard normal distribution_ is a normal distribution of standardized values called [z-scores](https://en.wikipedia.org/wiki/Standard_score). A z-score is measured in units of the standard deviation.

The z-score tells you how many standard deviations (`σ`) the value `x` is above (to the right of) or below (to the left of) the mean (`μ`). Values of `x` that are larger than the mean have positive z-scores and values of `x` that are smaller than the mean have negative z-scores. If `x` equals the mean, then `x` has a z-score of zero.

#### The Empirical Rule

If `X` is a random variable and has a normal distribution with mean `μ` and standard deviation `σ`, then the [Empirical Rule](https://en.wikipedia.org/wiki/68%E2%80%9395%E2%80%9399.7_rule) states the following:

- About 68% of the data is within one standard deviation of the mean.
- About 95% of the data is within two standard deviations of the mean.
- About 99.7% of the data is within three standard deviations of the mean.
- The z-scores for `+1σ` and `–1σ` are `+1` and `1`, respectively.
- The z-scores for `+2σ` and `–2σ` are `+2` and `2`, respectively.
- The z-scores for `+3σ` and `–3σ` are `+3` and `3` respectively.

The empirical rule is also known as the _68-95-99.7 rule_.

### Population Types

Three types of populations from which a sample can be obtained are:

1. A _literal_ population.

    This is an identifiable group (e.g., a person picked at random or a volunteer when polling).

2. A _virtual_ population.

    Frequently, measurements are taken with a device. We know that we could always take more measurements and get a slightly different answer.
    
    The closeness of the multiple readings depends on the _precision_ of the device and the _stability_ of the circumstances. Think of this as drawing observations from a virtual population of all the measurements that could be taken if we had enough time.

3. A _metaphorical_ population, when there is no larger population at all.

    Here, we act as if the data point is drawn from some population at random, when clearly it is not. We did not do any sampling, we have all of the data, and there is no more that we could collect. It may be best to think of what we have observed as having been drawn from an imaginary space of possibilities.

In reality, applications of statistical science rarely involve literal random sampling, and it is increasingly common to have all of the data that is potentially available. Regardless, it is helpful to retain the idea of an imaginary population from which a sample is drawn. This allows us to use the mathematical techniques that have been developed for sampling from real populations.

### Determining Causation

In the statistical sense, _causation_ means that when we intervene, the chances of different outcomes are systematically changed.

The purpose of an experiment is to carry out a test that determines causation and estimates the average effect of a factor (treatment), without introducing biases that could give the wrong impression of its effectiveness.

A proper experiment should adhere to the following principles:

- _Controls_.

    We need an _experimental group_ that receives the actual treatment, and a _control group_ that receives a [placebo](https://en.wikipedia.org/wiki/Placebo) treatment.

- _Allocation of treatment_.

    It is important to compare like with like, so the experimental and control groups need to be as similar as possible. The best way to ensure this is by randomly allocating participants to be treated or not, and then seeing what happens to them.
    
    This is known as a [randomized controlled trial (RCT)](https://en.wikipedia.org/wiki/Randomized_controlled_trial). It attempts to ensure that the two groups are similar in all factors that could otherwise influence the outcome, including _those factors that we do not know about_.

- _People should be counted in the groups to which they were allocated_.

    Participants in groups should be counted in the final analysis _even if they did not take their treatment_. This is known as the [intention-to-treat](https://en.wikipedia.org/wiki/Intention-to-treat_analysis) principle.
    
    It means that the final estimate of the treatment effect measures the effect being assigned rather than actually receiving it. It is fair to expect that the apparent effect in an intention-to-treat analysis to be less than the effect of actually taking the treatment.

- _If possible, participants should not know which group they are in_.

    Participants should be blinded to the treatment they were taking.

- _Groups should be treated equally_.

    Experimenters and their assistants should also be blinded to the groups that participants are assigned to, to ensure that all participants are treated equally.

- _Measure everyone_.

    Every effort must be made to follow up on all participants (e.g., participants that drop out of an experiment may do so due to a treatment's side effects).

- _Do not rely on a single study_.

    Robust conclusions require multiple studies.

- _Systematically review the evidence_.

    When looking at multiple experiments, include every study that has been done in order to create a systematic review. The results may be formally combined in a [meta-analysis](https://en.wikipedia.org/wiki/Meta-analysis).

Only by conducting large, meticulous, randomized experiments (especially when put together into a meta-analysis) can you confidently conclude that a result is not due to chance.

An innovation in randomized experimentation is [A/B testing](https://en.wikipedia.org/wiki/A/B_testing) (e.g., in web design). A series of A/B tests can rapidly lead to an optimized design and huge samples mean that even small effects can be reliably detected.

#### Determining Causation When Randomization Is Not Possible

When data does not arise from an experiment, it is observational. Correlation is sorted out from causation via good design and statistical principles applied to observational data, combined with a healthy dose of skepticism.

Alternative observational studies include:

- [Prospective Cohort Study](https://en.wikipedia.org/wiki/Prospective_cohort_study) - A longitudinal cohort study that follows over time a group of similar individuals (cohorts) that differ with respect to certain factors under study, to determine how these factors affect rates of a certain outcome.
- [Retrospective Cohort Study](https://en.wikipedia.org/wiki/Retrospective_cohort_study) - A cohort of individuals that share a common exposure factor is compared with another group of equivalent individuals not exposed to that factor, to determine the factor's influence on the incidence of a condition (e.g., disease, death).
- [Case-Control Study](https://en.wikipedia.org/wiki/Case%E2%80%93control_study) - Two existing groups differing in outcome are identified and compared on the basis of a supposed causal attribute.

When an apparent association between two outcomes might be explained by an observed common factor that influences both, this common cause is known as a [confounder](https://en.wikipedia.org/wiki/Confounding). The simplest technique for dealing with confounders is to look at the relationship within each level of the confounder. This is known as [adjustment or stratification](https://en.wikipedia.org/wiki/Confounding#Decreasing_the_potential_for_confounding).

[Simpson's paradox](https://en.wikipedia.org/wiki/Simpson%27s_paradox) occurs when the apparent direction of an association is reversed by adjusting for a confounding factor, requiring a complete change in the interpretation of the data. Splitting data according to factors that may help explain observed associations can help identify such paradoxes.

[Reverse causation](https://en.wikipedia.org/wiki/Correlation_does_not_imply_causation#B_causes_A_(reverse_causation_or_reverse_causality)) is when the chain of causation is actually the reverse of what was claimed.

Potential common causes that we do not measure are known as _lurking factors_, since they:

- Remain in the background
- Are not included in any adjustment
- Are waiting to disrupt naive conclusions from observational data

The following criteria should be considered before concluding that an observed link between an exposure and an outcome is causal:

**Direct Evidence**

- The size of the effect is so large that it _cannot be explained by plausible confounding_.
- There is _appropriate temporal and/or spatial proximity_, in that cause precedes effect and effect occurs after a plausible interval, and/or cause occurs at the same site as the effect.
- _Dose responsiveness and reversibility_ is demonstrated, i.e., the effect increases as the exposure increases, and the evidence is stronger if the effect reduces upon reduction of the dose.

**Mechanistic Evidence**

- There is a _plausible mechanism of action_ (which could be biological, chemical, or mechanical), with external evidence for a _causal chain_.

**Parallel Evidence**

- The effect fits with what is already known.
- The effect is found when the study is replicated.
- The effect is found in similar, but not identical, studies.

A challenging area of statistical reasoning about causation is the _balance of probabilities_, where a direct causal link is established if the relative risk associated with the exposure is greater than two. This line of reasoning lead to a new area of study called [forensic epidemiology](https://en.wikipedia.org/wiki/Forensic_epidemiology), which tries to use evidence derived from populations to draw conclusions about what might have caused individual events to occur.

### Modeling Relationships Using Regression

Any process of fitting lines or curves to data is referred to as _regression_. Specifically, regression models provide a mathematical representation between a set of predictor variables and a criterion variable.

In basic regression, the criterion variable is the quantity that we want to predict or explain and it usually forms the vertical y-axis of a graph. The predictor variable is the quantity that we use for doing the prediction or explanation and it usually forms the x-axis of a graph.

The coefficients in a regression model indicate how much we expect the criterion variable to change when the predictor variable is observed to change.

The gradient of the least-squares prediction line is known as the _regression coefficient_. The meaning of the gradient of the least-squares prediction line depends on our assumptions about the relationship between the variables being studied. For correlational data, the gradient indicates how much we expect the criterion variable to change, on average, if we observe a one unit difference for the predictor variable.

If the standard deviations of a predictor and criterion variable are identical, then the gradient of the regression line _is_ the Pearson correlation coefficient.

Statistical models have two main components:

1. A mathematical formula that expresses a deterministic, predictable component.
2. The _residual error_, i.e., the inevitable inability of a model to represent what we observe.

`observation = deterministic model + residual error`

What we see and measure around us can be considered as the sum of a systematic mathematical idealized form, plus some random contribution that cannot yet be explained (i.e., the _signal and the noise_).

[Regression-to-the-mean](https://en.wikipedia.org/wiki/Regression_toward_the_mean) occurs when more extreme responses revert to nearer the long-term average, since a contribution to their previous extremeness was pure chance.

_Multiple linear regression_ is done when you have more than one predictor variable. This results in the scatter of data points in three dimensions, but the idea of least-squares is still applicable. Often, multiple regression is used when researchers are interested in one particular predictor variable and other variables need to be adjusted for, to allow for imbalances.

Each type of criterion variable has its own form of multiple regression, with a correspondingly different interpretation of the estimated coefficients.

_Logistic regression_ is a form of regression that was developed for proportions. It ensures that the logistic regression curve cannot go above 100% or below 0%.

### Estimates and Intervals

Being able to realistically assess the error of an estimate is a critical element of statistical science.

In a well-conducted experiment, the sample statistics should be close to population parameters:

- `m` - Sample mean
- `μ` - Population mean (lowercase Greek letter mu)
- `s` - Sample standard deviation
- `σ` - Population standard deviation (lowercase Greek letter sigma)

With a large enough sample size, a sample statistic may be a good enough summary of a population parameter. If we knew how much the calculated population estimates would vary if we repeatedly drew samples from a population, it would help tell us how accurate the estimate was.

However, we could only determine the precise variability in our estimates if we knew the precise details of the population, which we do not know. There are two ways to address this:

1. Make mathematical assumptions about the shape of the population distribution and use probability theory to work out the variability we would expect in our estimate, and therefore how far away we might expect the mean of our sample to be from the mean of the population.
2. Based on the assumption that the population looks roughly like the sample, repeatedly draw new samples from our sample (i.e., bootstrapping).

The idea of determining how an estimate varies through a process of resampling with replacement is known as [bootstrapping](https://en.wikipedia.org/wiki/Bootstrapping_(statistics)) the data. Bootstrapping is the _magical_ idea of pulling oneself up by one's own bootstraps, which is impossible.

In statistics, this idea is reflected in our ability to learn about the variability in an estimate without having to make any assumptions about the shape of the population distribution. For example, if we repeatedly sample 1,000 times, we get 1,000 possible estimates of the mean.

Bootstrap estimates around the mean of the original sample are known as _sampling distributions_ of estimates, since they reflect the variability in estimates that arise from repeated sampling of data.

Distributions of estimates based on resampled data will almost be symmetric around the mean of the original data, i.e., almost all trace of the skewness of the original samples will disappear. This is an illustration of the Central Limit Theorem, which states that the distribution of sample means tends towards the form of a normal distribution with increasing sample size, _almost regardless of the shape of the original data distribution_.

Bootstrap distributions allow us to quantify our uncertainty about our prior estimates. For example, we can find the range of values that contain 95% of the means of the bootstrap resamples and call this a 95% uncertainty interval for the original estimates (i.e., margins of error). The symmetry of the bootstrap distributions means that the uncertainty intervals are roughly symmetric around the original estimate.

Another important feature is that the bootstrap distributions get narrower as the sample size increases.

Bootstrapping provides an intuitive way of assessing uncertainty in estimates without making strong assumptions and without using probability theory. However, is it compute-intensive and may not be feasible when working with large quantities of data.

### Probability

The flipping of two coins can be used to demonstrate the rules of probability:

- _The probability of an event is a number between `0` and `1`_

    `0` for impossible events (e.g., flip no heads and no tails), `1` for certain events (flip any of the four possible combinations).

- _Complement rule_

    The probability of an event happening is one minus the probability of it not happening (e.g., the probability of _at least one tail_ is one minus the probability of _two heads_, `1 - 1/4 = 3/4`).

- _The addition (OR) rule_

    Add probabilities of mutually exclusive events (i.e., they cannot happen at the same time) to get the total probability (e.g., the probability of _at least one head_ is `3/4` since it comprises _two heads_ OR _head + tail_ OR _tail + head_, each with a probability of `1/4`).

- _The multiplication (AND) rule_

    Multiply probabilities to get the overall probability of a sequence of independent events (i.e., one does not affect the other) occurring (e.g., the probability of a head AND a head is `1/2 x 1/2 = 1/4`).

All probabilities are _conditional_. There are always assumptions and other factors that could affect the probability. We need to be careful when establishing conditions.

In the case of flipping two coins, the events are _independent_, in that the probability of flipping a tail on the second flip does not depend on what the first flip was. _Dependent_ events are more relevant to real life.

For example, assume that out of one thousand people, 10 (1%) have a disease. Of these ten people, 9 (90%) have a positive result. Out of the 990 people without the disease, 99 (10%) are falsely given a positive test result.

In total, we have `9 + 99 = 108` positive test results. So, the probability that a randomly chosen person will get a positive test result is `108/1,000 = ~11%`. However, of these 108 people, only 9 actually have the disease. So, there is only a `9/108 = ~8%` probability that a person actually has the disease.

That is, it is trivial to confuse the probability of a positive test result with the actual probability of having the disease. Often, this is seen in court cases involving DNA evidence and is known as the _prosecutor's fallacy_. There, the probability of a false positive DNA match is confused with the probability of a person's innocence given the DNA evidence.

The faulty logic is easier to spot with a statement like _if you are the Hulk, then you are an Avenger_ to _if you are an Avenger, then you are the Hulk_.

Popular definitions of probability include:

- _Classical probability_ - What is usually taught in school, based on symmetries of coins and dice. It can be defined as _The ratio of the number of outcomes favoring the event divided by the total number of possible outcomes, assuming the outcomes are all equally likely._ This definition is somewhat circular, as we need to have a definition of _equally likely_.
- _Enumerative probability_ - An extension of classical probability where a probability is obtained by enumerating the opportunities. For example, suppose there are three blue shirts and four black shirts in a drawer and we take a shirt at random, what is the probability of drawing a blue shirt?
- _Long-run frequency probability_ - Based on the proportion of times an event occurs in an infinite sequence of identical experiments. This type of probability is only reasonable for infinitely repeatable events, but most realistic situations are not infinitely repeatable.
- _Propensity or chance_ - The idea that there is some objective tendency of the situation to produce an event. It seems to provide no basis for people to estimate this rather metaphysical true chance.
- _Subjective or personal probability_ - This is a specific person's judgment about a specific occasion, based on their current knowledge and is roughly interpreted in terms of the betting odds (for small stakes) that they would find reasonable. This forms the basis for the _Bayesian_ school of statistical inference.

Often, we act as if observations we make are random, when we know that, technically, they are not (e.g., [pseudo-random-number generators](https://en.wikipedia.org/wiki/Pseudorandom_number_generator)).

Imagine three situations:

1. A data point is generated by a randomizing device (e.g., throwing dice, using a pseudo-random-number generator). Probability theory naturally applies here.
2. An extant data point is _chosen_ by a randomizing device.
3. There is an absence of randomness, but we _act_ as if a data point is generated by a random process. The majority of the application of statistics arises from this situation.

Before operating a randomizing device, we assume that we have a set of possible results that might be observed, together with their respective probabilities. If we associate each of these possible outcomes with a quantity (e.g., `0` for tails and `1` for heads), then we say we have a _random variable_ with a probability distribution.

In situation 1, the randomizing device ensures that the observation is generated at random from this distribution and when it is observed, the randomness disappears and all of these potential futures collapse down on to the actual observation.

Similarly, in situation 2, if we draw an individual at random and measure an attribute (e.g., height), then we have essentially drawn an observation at random from a population distribution of heights.

However, most of the time, we simply consider all of the available measurements to us at the time, which may have been informally collected and with no random sampling (situation 3). Here, we need to act as if data were generated by a random mechanism from a metaphorical population, even though we know that it was not.

We do not need to believe that events are actually driven by pure randomness. We simply need to assume that _chance_ encapsulates all of the inevitable unpredictability in the world (i.e., natural variability).

Probability forms the mathematical foundation for both pure randomness (which occurs with subatomic particles, coins, and dice) and natural randomness (e.g., birth weights, surgery survival rates, every phenomenon that is not completely predictable).

## Summaries

### Data Distributions

- The mean and standard deviation are only useful when you have a _symmetrical data distribution_. Much of the time, your data distribution will be skewed, so you will be better off using the [median](https://en.wikipedia.org/wiki/Median) and [Interquartile Range (IQR)](https://en.wikipedia.org/wiki/Interquartile_range), as they are [robust measures](https://en.wikipedia.org/wiki/Robust_measures_of_scale) that are not as strongly affected by extreme values.
- A symmetrical distribution is _not_ equivalent to a normal distribution (i.e., there are symmetrical distributions that are _not_ normal distributions). A normal distribution is a kind of symmetrical distribution that has a _bell-shaped curve_.

    There are also bell-shaped curves that are not normal distributions (e.g., [Cauchy](https://en.wikipedia.org/wiki/Cauchy_distribution), [Student's _t_](https://en.wikipedia.org/wiki/Student%27s_t-distribution)).

- In a [normal distribution](https://en.wikipedia.org/wiki/Normal_distribution), 95% of the data is contained within the interval given by the mean +-2 standard deviations, and 99.7% of the data is contained within the interval given by the mean +-3 standard deviations.
- _Sampling distributions of estimates_ are _sampling distributions of means_ (i.e., a distribution of sample means taken from a population) and reflect the variability (spread) in estimates that arise from repeated sampling of data.

    The means in the sampling distributions of means are spread around the mean from the population they were taken from. The mean of the distribution of sample means is the original population's mean.

    The standard error of the distribution of sample means is the standard deviation of the population divided by the square root of the sample size. This is referred to as the _standard error of the mean_.

    The distribution of sample means tends towards a normal distribution with increasing sample sizes. This is referred to as the _Central Limit Theorem_. As the sample sizes increase, you also see decreased spread and narrower distributions, which results in narrower confidence intervals, and therefore more accurate estimates.

    Whatever the shape of a population distribution from which measurements are sampled from, for large sample sizes (i.e., `n >= 30` or the data comes from a normal distribution), the distribution of sample means will be a normal distribution.

### Probability

- Mutually exclusive events can be added to find the total probability of an event.
- When you operate a randomizing device and assign an assumed set of possible observed results with a quantity, you obtain a random variable with a probability distribution for those results.
- A population can be thought of as providing the [probability distribution](https://en.wikipedia.org/wiki/Probability_distribution) for a random observation drawn from that population.
-  The [binomial distribution](https://en.wikipedia.org/wiki/Binomial_distribution) with parameters `n` and `p` is the discrete probability distribution of the number of successes in a sequence of _n_ independent experiments, each asking a yes–no question, and each with its own Boolean-valued outcome: success (with probability `p`) or failure (with probability `q = 1 - p`).

    A single success/failure experiment is called a [Bernoulli trial](https://en.wikipedia.org/wiki/Bernoulli_trial) (or _Bernoulli experiment_), and a sequence of outcomes is called a [Bernoulli process](https://en.wikipedia.org/wiki/Bernoulli_process). For a single trial (i.e., `n = 1`), the binomial distribution is a [Bernoulli distribution](https://en.wikipedia.org/wiki/Bernoulli_distribution). If you keep performing Bernoulli trials, the proportion of each outcome (e.g., a heads or a tails from a coin flip) will get closer and closer to the true underlying chance of the outcome (i.e., 50/50).

    Binomial probability distributions tend towards a normal distribution with increasing sample sizes. As the sample sizes increase, you also see decreased spread and narrower distributions, which results in narrower confidence intervals, and therefore more accurate estimates. In probability theory, the decrease in variability and convergence of the mean of the results towards the expected value as the sample size increases is referred to as the _Law of Large Numbers_.

- A _confidence interval_ is the range of population parameters for which an observed statistic is a plausible consequence. It is used to assess the precision of a statistic in estimating a parameter. A need to address the precision of a statistic is necessary because randomness from a sample introduces randomness into the statistic (i.e., the estimate of the parameter).
    - A 95% confidence interval is given the label 95% because, with repeated sampling from a population, 95% of such calculated intervals should contain the true parameter given the observed statistic. This percentage is the confidence level.
    - Exact confidence intervals based on probability theory are only precisely correct if the underlying population distribution is a normal distribution. However, with a sufficiently large sample size, we can assume that our estimates have normal distributions and so the exact intervals we determine are acceptable.
    - Confidence intervals are based on two components:

        1. _Type A_ - The standard confidence interval statistical measures that improve with increased sample sizes.
        2. _Type B_ - Systematic errors that do not decrease with increased samples sizes (e.g., bias) and need to be handled using non-statistical means (e.g., expert judgment, external evidence).

- It is probably wise to double any quoted margin of error in a poll to account for systematic errors made in the polling.
- To determine if a value has meaningfully changed from year to year, you can determine the 95% confidence intervals for each year. If the intervals do not overlap, then you can be 95% confident that there was a real change in the underlying rate.

    Alternatively, you can calculate each year's _rate of change_ and then determine the confidence intervals for the annual rate of change. If this confidence interval does not include 0, you can conclude with 95% confidence that there was a real change in the underlying rate.

- _Bootstrapping_ data is a nice way of assessing the uncertainty in estimates of population parameters from sample statistics because we do not have to make strong assumptions (i.e., we just need to assume that the population looks roughly like our sample) and we do not need to use probability theory. However, it is computationally-intensive.

### Null Hypothesis Significance Testing (NHST)

- Standard inferential tools like confidence intervals and _p_-values are known as _classical_ or _[frequentist](https://en.wikipedia.org/wiki/Frequentist_inference)_ methods, since they are based on long-run sampling properties of statistics.
- The [Fisherian approach](https://en.wikipedia.org/wiki/Frequentist_inference#Fisherian_reduction_and_Neyman-Pearson_operational_criteria) is based on estimation using the _likelihood_ function, which expresses the relative support given to the different parameter values by the data, and hypothesis testing based on _p_-values.
- The Neyman—Pearson approach is focused on decision-making. That is, if you decide the true answer is in a 95% confidence interval, then you will be right 95% of the time, and you should control Type I and Type II errors when hypothesis testing.
- _Statistical significance_ means that you have obtained a small _p_-value. `p < 0.05` and `p < 0.01` are used as convenient critical thresholds for indicating significance.
- There is a close connection between hypothesis testing and confidence intervals.

    A 95% confidence interval that includes the value that usually represents the null hypothesis (i.e., `0`) is logically equivalent to concluding that a statistic is less than 2 standard errors from `0`, i.e., the change is not significantly different from `0` and there is not enough evidence to reject the null hypothesis.

    - A two-sided _p_-value will be less than `0.05` if the 95% confidence interval does _not_ include the null hypothesis (generally `0`).
    - The 95% confidence interval is the set of null hypotheses that are _not_ rejected at `p < 0.05`.

- There is a close connection between the _size of a test_ (`α`) and Fisher's _p_-value. If `α` is considered the threshold at which results are considered insignificant, then the results that lead you to reject the null hypothesis are the same as those for which the _p_-value are less than `α`, i.e., `α` is the threshold significance level.

    - An `α` of `0.05` means that you reject the null hypothesis for all _p_-values less than `0.05`.

- If a sample's size is held constant, there is a trade-off between the _size of a test_ and the _power of a test_. If power (`1 - β`, the probability of correctly identifying a true effect) is increased by decreasing `β` (the probability of accepting the null hypothesis when it is false), you will increase `α` (the probability of rejecting the null hypothesis when it is true).

    In other words, if sample size is held constant, decreasing the probability of Type II errors increases both power _and_ the probability of Type I errors.

- Before running an experiment, it is considered best practice to specify the null and alternative hypotheses, as well as to determine the size and power of the study. Often, `α` is set to `0.05` and `π` is set to `0.80`.
- [Effect sizes](https://en.wikipedia.org/wiki/Effect_size) are needed to calculate the _power of a test_ (`1 - β`). Then, the effect size and power of a test can help you determine your required sample size.

    Effect sizes (i.e., the strength of the relationship between two variables) should be reported along side hypothesis test results.

- Size and power are irrelevant _after_ a study has been conducted. At that point, results are analyzed using confidence intervals to show the plausible values for experimental effects and Fisherian _p_-values to summarize the strength of the evidence against the null hypothesis.
- If repeated significance tests are done, even if using the Bonferroni method, and the null hypothesis is true, you are _certain_ to eventually reject the null hypothesis at any significance level. This is referred to as the [Law of the Iterated Logarithm](https://en.wikipedia.org/wiki/Law_of_the_iterated_logarithm).

### Regression

- In a [simple linear regression](https://en.wikipedia.org/wiki/Simple_linear_regression) model, the regression line is the least-squares prediction line, in that the regression seeks to minimize the sum of squared errors (SSE) between the actual and predicted values.

    The gradient (slope) of a regression line is known as the _regression coefficient_. It represents the change you would expect to see in the criterion variable for a one unit change in the predictor variable.

    The _intercept_ is the value of the criterion variable when the predictor variable is zero.

    If the standard deviations of a predictor and criterion variable are identical, then the gradient of the regression line _is_ the [Pearson correlation coefficient](https://en.wikipedia.org/wiki/Pearson_correlation_coefficient).

- The _t_-value is known as a [_t_-statistic](https://en.wikipedia.org/wiki/T-statistic) and is the link that tells you whether the association between a predictor and criterion variable is statistically significant. The _t_-value is a special case of a Student's _t_-statistic. Its formula is `estimate / standard error` and is interpreted as how far away the estimate is from `0`, measured in the number of standard errors.

    For large samples, _t_-values greater than `2` or less than `-2` correspond to a `p < 0.05`. Thresholds will be larger for smaller sample sizes.

### Bayesian Statistics

- _Bayes theorem_ is the probability of an event, based on our prior knowledge of conditions that might be related to an event. Bayesian probabilities are necessarily subjective and change as we receive new information.
    - Bayes theorem creates reversed frequency trees that respect the temporal order in which we come to know things, rather than the actual timeline of underlying causation. This is why what is now known as [Bayesian probability](https://en.wikipedia.org/wiki/Bayesian_probability) used to be called [inverse probability](https://en.wikipedia.org/wiki/Inverse_probability).
- In the Bayes framework, the data does not speak for itself. _External knowledge_ and our _judgment_ play a central role.
- Bayesian analysis establishes a _prior distribution_, combines it with evidence from data known as the _likelihood_, and gives a final conclusion known as the _posterior distribution_, which expresses all that is believed about an unknown quantity.
    - The main controversy about Bayesian analysis is the source of the prior distribution. If physical knowledge is not available, suggestions for obtaining prior distributions include using subjective judgment, learning from historical data, and specifying objective priors that try to let the data speak for themselves without introducing subjective judgment.
    - There is no _true_ prior distribution. Any analysis should include a [sensitivity analysis](https://en.wikipedia.org/wiki/Sensitivity_analysis) to a number of alternative choices, encompassing a range of different possible opinions.
- The _final odds_ are the initial odds for the hypotheses (i.e., the False Negative Rate divided by the False Positive Rate) times the likelihood ratio. The final odds can be converted into a percentage.
- The initial odds are referred to as the _prior odds_ and the final odds are referred to as the _posterior odds_. The posterior odds can become the new prior odds when introducing new, independent items of evidence.

    When combining all the evidence, the process is equivalent to multiplying the independent likelihood ratios together to form a composite likelihood ratio.

- Bayesian analysis makes the most sense when you have expected frequencies and two hypotheses. It does not do so well when you want to draw inferences about unknown quantities that may take on a range of values (e.g., parameters in statistical models).
- One popular and effective way to bring precision to sparse data is to use Bayesian smoothing via _multi-level regression and post-stratification (MRP)_. For example, in polling, voters are broken down into small _cells_, each comprising highly homogeneous groups of people. Bayesian ideas are used to calculate regression coefficients, and the regression model is used to predict how voters in each cell will vote.

    MRP will fail if many respondents give systematically misleading answers.

- Use of Bayes factors is contested as assumed prior distributions for any unknown parameters in both the null and alternative hypotheses underlie Bayes factors.
    - Unlike in Null Hypothesis Significance Testing (NHST), Bayes factors treat the two hypotheses symmetrically and can actively support a null hypothesis. If you put prior probabilities on hypotheses, you can calculate posterior probabilities of alternative theories.
- The Bayesian approach is based on external evidence about unknown quantities (expressed as a prior distribution) combined with evidence from the underlying probability model for the data (known as the likelihood). This yields a final, posterior distribution that forms the basis for all conclusions.
    - Standard, non-Bayesian schools design experiments using a Neyman—Pearson approach of Type I and Type II errors, and analyze their results from a Fisherian perspective using _p_-values as measures of evidence.
    - Choose an approach according to the _practical context_.

### Artificial Intelligence and Machine Learning

- _Narrow AI_ models are not expected to demonstrate imagination or flexibility in everyday life situations, which would require a _general AI_.
- A general AI will need a causal model for how the world actually works in order to answer human-level questions concerning the effect of interventions (_What if we do X?_) and counterfactuals (_What if we had not done X?_).
- Features that lack value for classification/prediction tasks are identified by data visualization or regression models, and then discarded. The number of features may be reduced by forming [composite measures](https://en.wikipedia.org/wiki/Composite_measure) that encapsulate most of the information.
- Naive rules (e.g., _constant models_) serve as baselines from which to gauge improvements obtained from a model (_sanity checks_).
- A _classification tree_ is the simplest form of algorithm. It contains a series of yes/no questions. The answer to each question decides the next question to ask until a conclusion is reached.
- The number of different error types that an algorithm makes is summarized in a _confusion matrix_.
    - A classification model's _recall_ (sensitivity) is its True Positive Rate (TPR). That is, out of all true positives, how many did a model accurately predict.
    - A classification model's _precision_ (specificity) is its True Negative Rate (TNR). That is, out of all the positives that a model predicts, how many were actually true positives.
- For classification tasks, _accuracy_ is a crude metric, as it does not provide an account of the confidence with which a prediction has been made.
- Classification algorithms that provide a probability (or any number), rather than a simple classification, can be compared using _Receiver Operating Characteristic (ROC curves)_. These curves plot a classification model's FPR and TPR over different classification threshold values, and compare it to the comparable curve for a random classification model.
    - The _Area Under Curve (AUC)-ROC_ tells you how good your model is. The greater the area under the curve, the closer the AUC-ROC score will be towards `1`, and the better the model will be at simultaneously maximizing the TPR and minimizing the FPR.

        Areas above `0.8` represent fairly good discriminatory ability.

    - AUC-ROC scores do not tell you how good a model's probabilities are. A _calibration plot_ can help you see how reliable a model's stated probabilities are by collecting together the events given a particular probability of occurrence, and calculating the proportion of such events that actually occurred. The closer the points are to the graph's diagonal line, the more reliable the model's probabilities are.
- _Hyperparameter tuning_ can be done during _cross-validation_. At each stage of cross-validation, a model can be developed for each of a range of different hyperparameters. For each parameter value, the average predictive performance over all the cross-validation stages is calculated.

    The optimal value for the hyperparameter is the one that gives the best cross-validatory performance, and this value is used to construct a model from the complete training set, which is the final version of the model.

- _Regression models_ are used to construct a formula to predict an outcome.
    - _Boosting_ is an iterative training procedure designed to pay more attention to difficult cases. Cases in the training set that are incorrectly classified at one iteration are given greater weight in the next iteration, with the number of iterations chosen using tenfold cross-validation (i.e., at each stage of cross-validation, `10%` of the data serves as the validation set).
    - _Regression coefficients_ model the features of an observation and _interactions_ are more complex, combined features. These coefficients provide some interpretation of the importance of different features.
- Classification models attempt to construct simple rules that identify groups of cases with similar expected outcomes, while regression models focus on the weight to be given to specific features, regardless of what else is observed on a case.
- To an extent, an algorithm can be reverse-engineered by providing it with false information to see how its output changes. This may give you some idea as to what factors are driving the output.

## Glossary

`α`  
False Positive Rate.

`β`  
False Negative Rate.

`1 - α`  
True Negative Rate.

`1 - β`  
True Positive Rate.

**[Absolute Risk](https://en.wikipedia.org/wiki/Absolute_risk)**  
The change in the proportion of each group that would be expected to experience a criterion variable. Absolute risk should be provided to an audience for clarity.

**[Area Under Curve (AUC)-ROC](https://en.wikipedia.org/wiki/Receiver_operating_characteristic#Area_under_the_curve)**  
A model's probability area under a ROC curve. Tells you how good your model is. The greater the area under the curve, the closer the AUC-ROC score will be towards `1`, and the better the model will be at simultaneously maximizing the True Positive Rate and minimizing the False Positive Rate.

Areas above `0.8` represent fairly good discriminatory ability.

**[Artificial Intelligence (AI)](https://en.wikipedia.org/wiki/Artificial_intelligence)**  
When algorithms embodied in machines are used to either carry out tasks that would normally require human involvement or to provide expert-level advice to humans.

**[Bayes Factor](https://en.wikipedia.org/wiki/Bayes_factor)**  
For scientific hypothesis testing, the Bayesian equivalent to the likelihood ratio. The ratio of two competing models represented by their evidence that is used to quantify support for one model over the other.

Can only be obtained by averaging with respect to the prior distribution of the unknown parameters.

**[Bayes' Theorem](https://en.wikipedia.org/wiki/Bayes%27_theorem)**  
The idea that probabilities are subjective and change as we receive new information.

**[Bayesian Brain](https://en.wikipedia.org/wiki/Bayesian_approaches_to_brain_function)**  
Observation that the human brain appears to work via Bayesian methods, i.e., we have prior expectations about what we will see in any context, and then only take notice of unexpected features in our vision that are then used to update our current perceptions.

**[Bias/Variance Trade-Off](https://en.wikipedia.org/wiki/Bias%E2%80%93variance_tradeoff)**  
The observation that decreasing bias increases estimate variance, and vice-versa.

**[Big Data](https://en.wikipedia.org/wiki/Big_data)**  
Data with a large number of examples (_n_) or characteristics/features (_p_ for parameters).

**[Bonferroni Correction](https://en.wikipedia.org/wiki/Bonferroni_correction)**  
Lowering of a significance threshold to address the _multiple testing problem_. `α / n`, where `n` is the number of tests done.

**[Boosting](https://en.wikipedia.org/wiki/Boosting_(machine_learning))**  
An iterative training procedure designed to pay more attention to difficult cases (i.e., cases in a training set that are incorrectly identified). Difficult cases are given greater weight in the next iteration. The number of iterations chosen could use tenfold cross-validation.

**[Bootstrapping](https://en.wikipedia.org/wiki/Bootstrapping_(statistics))**  
A test or metric that involves using random sampling with replacement to assign a measure of accuracy to a statistic (i.e., a sample estimate). Allows estimation of the sampling distribution of almost any statistic using random sampling methods.

**[Calibration Plot](https://en.wikipedia.org/wiki/Probabilistic_classification#Probability_calibration)**  
Plot of predicted probabilities (`x`) versus observed percentages (`y`). Helps you see how reliable a model's stated probabilities are by collecting together the events given a particular probability of occurrence and calculating the proportion of such events that actually occurred.

The closer the point are to the graph's diagonal line, the more reliable the model's probabilities are. Vertical bars could be used to represent what we would expect the actual measured proportion to be 95% of the time, if our model's predictions were reliable. If these vertical bars include the graph's diagonal line, the algorithm is considered well-calibrated.

**[Central Limit Theorem](https://en.wikipedia.org/wiki/Central_limit_theorem)**  
The idea that if you collect samples of a large enough size and create a histogram of either the samples' means or sums, the resulting histogram will tend to have an approximate normal shape. Sampling is done with replacement.

In other words, as sample sizes increase, a sampling distribution of estimates will tend toward a normal distribution with decreased spread, narrower distributions, narrower confidence intervals, and more accurate estimates.

The required size of the samples depends on the original population from which samples are drawn (usually, the sample size should be at least 30 or the data should come from a normal distribution). If the original population distribution is not normal, more observations are required for the sample means/sums to be normal.

The central limit theorem illustrates the law of large numbers.

**[Classification Task (Supervised Learning)](https://en.wikipedia.org/wiki/Statistical_classification)**  
Determines what category an observation belongs to.

**[Clustering (Unsupervised Learning)](https://en.wikipedia.org/wiki/Cluster_analysis)**  
Identification of similar groups without being aware that groups exist a priori. Is a strategy for dealing with an excessive number of cases.

After homogeneous clusters are identified, they are given a label and algorithms are built for classifying future cases.

**Coefficients**  
In linear regression, estimates that represent the expected change in the criterion variable per one unit change in the predictor variable.

**[Confidence Interval](https://en.wikipedia.org/wiki/Confidence_interval)**  
The range of parameters for which a statistic is a plausible consequence.

A confidence interval for a normal distribution that is based on +-2 standard errors is a _95% confidence interval_. A confidence interval for a normal distribution that is based on +-3 standard errors is a _99% confidence interval_.

**[Confusion Matrix](https://en.wikipedia.org/wiki/Confusion_matrix)**  
Matrix of model errors. Summarizes the number of different error types that an algorithm makes.

**Expected Frequencies**  
Expressing risk as a _1 in X_ chance. This technique promotes understanding and an appropriate sense of importance. However, using multiple _1 in..._ statements is not recommended, as they may become difficult to compare.

**[External Validity](https://en.wikipedia.org/wiki/External_validity)**  
The validity of applying the conclusions of a study outside the context of that study. That is, the extent to which the results of a study can be generalized to and across other situations, people, stimuli, and times.

**[Feature Engineering](https://en.wikipedia.org/wiki/Feature_engineering)**  
Reducing the raw data for each observation to a manageable amount due to an excessively large number of parameters (_p_).

**Final Odds**  
The initial odds for the hypotheses (i.e., the False Negative Rate divided by the False Positive Rate) times the likelihood ratio.

**[Hypothesis](https://en.wikipedia.org/wiki/Hypothesis)**  
A proposed explanation for a phenomenon.

**Intercept**  
In linear regression, the average of the criterion variable measure.

**[Internal Validity](https://en.wikipedia.org/wiki/Internal_validity)**  
The extent to which a piece of evidence supports a claim about cause and effect, within the context of a particular study. Determined by how well a study can rule out alternative explanations for its findings (e.g., sources of systematic error or bias).

**K-Nearest-Neighbor**  
Non-parametric method that classifies according to the majority outcome among close cases in the training set.

Used for classification and prediction tasks.

**[Law of Large Numbers](https://en.wikipedia.org/wiki/Law_of_large_numbers)**  
The idea that as you take larger and larger samples from any population, the sample mean will very likely get closer and closer to the population mean.

**Law of the Iterated Logarithm**  
The certainty of rejecting the null hypothesis (when it is true) at any significance level after repeated significance tests. Is addressed using statistical methods, like the _[Sequential Probability Ratio Test (SPRT)](https://en.wikipedia.org/wiki/Sequential_probability_ratio_test)_.

**[Likelihood Ratio](https://en.wikipedia.org/wiki/Likelihood-ratio_test)**  
The probability of the evidence assuming hypothesis A divided by the probability of the evidence assuming hypothesis B.

Alternatively, the probability of evidence if A divided by the probability of evidence if NOT A.

**[Machine Learning](https://en.wikipedia.org/wiki/Machine_learning)**  
A subset of AI where algorithms model associations and are unaware of underlying causal processes.

**[Margin of Error](https://en.wikipedia.org/wiki/Margin_of_error)**  
A half-width of a confidence interval. The margin of error is the _standard error of the mean_ times the _z-score_ that corresponds to a given confidence level (e.g., `1.96` for a 95% confidence level).

**[Mean-Squared-Error (MSE)](https://en.wikipedia.org/wiki/Mean_squared_error)**  
The average of the squares of the errors. Is analogous to the least-squares criterion from regression analysis. Used to evaluate prediction tasks predicting a numerical quantity.

**[Multi-Level Regression With Post-Stratification (MRP)](https://en.wikipedia.org/wiki/Multilevel_regression_with_poststratification)**  
Bayesian smoothing technique to bring precision to sparse data. Will fail if many respondents give systematically misleading answers.

**[Multiple Testing Problem](https://en.wikipedia.org/wiki/Multiple_comparisons_problem)**  
When multiple significance tests are carried out and the most significant result is reported. Is an issue because the chance of getting a false positive result (i.e., a Type I error) increases as you do more trials.

**[Narrow AI](https://en.wikipedia.org/wiki/Weak_artificial_intelligence)**  
Systems that can carry out closely prescribed tasks. These systems start with a vast number of examples and learn through trial and error, like a naive child.

**[Neural Networks](https://en.wikipedia.org/wiki/Neural_network)**  
Computing systems vaguely inspired by the biological neural networks that constitute animal brains. Use layers of weighted nodes, like a series of piled up logistic regressions. Weights are calculated via an optimization procedure and multiple networks can be constricted and averaged.

Networks with many layers are known as [deep-learning](https://en.wikipedia.org/wiki/Deep_learning) models.

Used for classification and prediction tasks.

**[Nonparametric Statistics](https://en.wikipedia.org/wiki/Nonparametric_statistics)**  
Statistics not solely based on parameterized families of probability distributions. Based on either being distribution-free or having a specified distribution with unspecified parameters.

Often used when the assumptions of parametric tests are violated.

**[Null Hypothesis](https://en.wikipedia.org/wiki/Null_hypothesis)**  
A simplified, negative form of statistical model that is never claimed to be true, but is just assumed to be the case until sufficient evidence is provided to reject it.

**[Odds](https://en.wikipedia.org/wiki/Odds)**  
The probability of an event happening divided by the probability of it not happening.

**[Odds Ratio](https://en.wikipedia.org/wiki/Odds_ratio)**  
The ratio of the odds of A in the presence of B and the odds of A in the absence of B. Conversely, the ratio of the odds of B in the presence of A and the odds of B in the absence of A.

For events that are fairly rare, the odds ratios will be numerically close to the relative risks. For common events, the odds ratio can be very different from the relative risk.

**[Overfitting](https://en.wikipedia.org/wiki/Overfitting)**  
When an algorithm becomes too complex and starts fitting noise rather than signal. Bias decreases, but estimate variance increases. Results from an attempt to be unbiased and take into account all available information.

Overfitting is avoided by using _regularization_ (i.e., the effects of the variables are pulled in towards zero) and _cross-validation_.

**[_p_-value](https://en.wikipedia.org/wiki/P-value)**  
The probability of getting a result at least as extreme as the observed value if the null hypothesis (and other model assumptions, e.g., lack of systematic bias, independent observations) is true. Represents the tail area(s) of the null hypothesis distribution that are above/below the observed sample measurement.

If this value is high, it indicates that your observed value is close to the center of the null hypothesis distribution (i.e., `0`), which indicates that the true parameter difference for the population in question is zero.

**[Parametric Statistics](https://en.wikipedia.org/wiki/Parametric_statistics)**  
Statistics that assume that sample data comes from a population that can be modeled by a probability distribution that has a fixed set of parameters (e.g., normal distributions are parameterized by the mean and standard deviation).

**[Power of a Test](https://en.wikipedia.org/wiki/Power_of_a_test)**  
The chance of rejecting the null hypothesis when the alternative hypothesis is true, i.e., the chance of correctly detecting a real effect. Also known as `1 - β` or `π` (where `β` is the probability of a Type II error), and the _True Positive Rate_.

**[Precision (Specificity, True Negative Rate)](https://en.wikipedia.org/wiki/Precision_and_recall#Precision)**  
The proportion of predicted positives that are actually true positives.

**Prediction Task**  
Tell us what is going to happen.

**Predictive Analytics**  
The performance of classification or prediction tasks.

**[Prior Odds](https://en.wikipedia.org/wiki/Prior_probability)**  
The initial odds of an event.

**[Prosecutor's Fallacy](https://en.wikipedia.org/wiki/Prosecutor%27s_fallacy)**  
Confusing the probability of a positive event test with the probability of the event itself.

**[Random Forests](https://en.wikipedia.org/wiki/Random_forest)**  
Ensemble learning method where the final output is made from a majority vote of a large number of trees.

Used for classification and prediction tasks.

**[Random Match Probability](https://en.wikipedia.org/wiki/Random_match_possibility)**  
A likelihood ratio that is calculated as the probability of a DNA match assuming a specific person (this is generally taken to be `1`) divided by the probability of a DNA match assuming an unrelated person randomly selected from the population. Typical values can go into the millions or billions.

**[Random Variable](https://en.wikipedia.org/wiki/Random_variable)**  
A variable whose values depend on outcomes of a random phenomenon. The mean of a random variable is known as its _expectation_.

**[Recall (Sensitivity, True Positive Rate)](https://en.wikipedia.org/wiki/Precision_and_recall#Recall)**  
Proportion of true positives that a model accurately predicts.

**[Receiver Operating Characteristic (ROC curves)](https://en.wikipedia.org/wiki/Receiver_operating_characteristic)**  
A plot of a classification model's False Positive Rate (FPR) and True Positive Rate (TPR) over different classification threshold values, versus the comparable curve for a random classification model.

**[Regression](https://en.wikipedia.org/wiki/Regression_analysis)**  
Any process of fitting lines or curves to data. Used to construct a simple formula to predict an outcome.

**Regression Coefficient**  
In linear regression, the gradient (slope) of a regression line. Represents the change you would expect to see in the criterion variable for a one unit change in the predictor variable.

**[Relative Risk](https://en.wikipedia.org/wiki/Relative_risk)**  
The increase in experiencing a criterion variable between a group that experiences a predictor variable versus a group that does not. This tends to convey an exaggerated importance.

**Sanity Check**  
Test to confirm that a model performs better than a constant model, which serves as a naive rule baseline to gauge improvements obtained from a model.

**Shrinkage**  
The fact that a _Bayes' estimate_ is always pulled in, or shrunk, towards the center of the initial distribution.

**[Size of a Test](https://en.wikipedia.org/wiki/Size_(statistics))**  
The probability of a Type I error. Also known as `α` and the _False Positive Rate_.

**[Standard Error](https://en.wikipedia.org/wiki/Standard_error)**  
The standard deviation of a statistic (i.e., a value obtained from a sample) sampling distribution.

**Standard Error of the Mean**  
An example of a standard error. A special standard deviation that is also known as the _standard deviation of the sampling distribution of the mean_. This is a standard deviation of a distribution created from _sample means_.

**[Statistical Significance](https://en.wikipedia.org/wiki/Statistical_significance)**  
The attainment of a small _p_-value. `p < 0.05` and `p < 0.01` are used as convenient critical thresholds for indicating significance.

In general, a high value for a statistic indicates _incompatibility_ with the null hypothesis, while a high _p_-value indicates _compatibility_ with the null hypothesis.

**[Support Vector Machines (SVM)](https://en.wikipedia.org/wiki/Support_vector_machine)**  
Supervised learning model that finds linear feature combinations that best split the different outcomes.

Used for classification and prediction tasks.

**[_t_-value](https://en.wikipedia.org/wiki/T-statistic)**  
An _estimate_ divided by the _standard error_. Tells you whether the association between a predictor and criterion variable is statistically significant, and is a special case of a Student's _t_-statistic that is used in regression models.

**Test Set**  
Data Set used to evaluate a model.

**Training Set**  
Data set used to build a model.

**[Type I Error](https://en.wikipedia.org/wiki/Type_I_and_type_II_errors#Type_I_error)**  
Probability of rejecting the null hypothesis when it is true. Also known as `α`, the _False Positive Rate_, and the _Size of a Test_.

**[Type II Error](https://en.wikipedia.org/wiki/Type_I_and_type_II_errors#Type_II_error)**  
Probability of accepting the null hypothesis when it is false. Also known as `β` and the _False Negative Rate_.

**Validation Set**  
Data set used for hyperparameter tuning and algorithm comparisons.

**[z-score](https://en.wikipedia.org/wiki/Standard_score)**  
A data point expressed in terms of its standard deviations away from the mean.

## Documentation

- [Writing Mathematics in Plain Text Email](https://pages.uoregon.edu/ncp/Courses/MathInPlainTextEmail.html)
- [Wumbo](https://wumbo.net/)
    - [Unicode Table](https://wumbo.net/tables/unicode/)