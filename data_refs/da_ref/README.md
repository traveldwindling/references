# Data Analysis Reference

A collection of data analysis reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## Data Analysis Packages

```console
python3 -m pip install \
    beautifulsoup4 \
    bottleneck \
    'dask[complete]' \
    duckdb \
    flake8 \
    geopandas \
    ipympl \
    ipython \
    ipywidgets \
    itables \
    jupyterlab \
    lxml \
    matplotlib \
    numexpr \
    odfpy \
    openpyxl \
    pandas \
    pillow \
    pyarrow \
    pycodestyle_magic \
    scipy \
    seaborn \
    SQLAlchemy \
    statsmodels \
    XlsxWriter \
    ydata-profiling
```

## IPython

### Configuration

A configuration file can be created to automatically load code when you start an IPython session:

`ipython profile create`

By default, the file is created at `${HOME}/.ipython/profile_default/ipython_config.py`. The configuration file is a fully-featured Python script.

The file's main usage is to set values on the `c` configuration object, which exists in the configuration file, e.g., `c.InteractiveShell.automagic = False`.

Collections that have been defined elsewhere or have default values can be modified using the `.append()`, `.extend()`, and `.prepend()` methods.

[Example Configuration File](https://ipython.readthedocs.io/en/stable/config/intro.html#example-configuration-file)

### Startup Files

To have code run at the beginning of every IPython session, add `.py` or `.ipy` scripts to `${HOME}/.ipython/profile_default/startup/`. Files in this directory are executed as soon as the IPython shell is constructed (prior to any code/files specified via the `exec_lines` or `exec_files` configurables) and are run in lexicographical order, which allows you to control the execution order of the files
with a prefix, e.g., (`00.py`, `01.py`).

### Help

Enter `?` at the IPython interactive prompt for an introduction and overview of IPython's features.

### Dynamic Object Information

`ex_obj??`

### Autosuggestions

To accept an IPython command autosuggestion, press either **Ctrl+f**, **Ctrl+e**, or **End**.

### System Shell Access

Any input line starting with a `!` is passed verbatim (minus the `!`) to the underlying operating system shell.

Shell command output can also be saved to a Python variable:

`ex_var = !ex_shell_cmd`

For the IPython kernel, the Python variable created is a `IPython.utils.text.SList` object.

Python variables can be referenced in `!` commands by surrounding them with curly braces (`{}`). For example:

```pycon
In [1]: lawrence_lessig = 'A quiet kid among strangers. A deep blue pool, hiding a volcano.'
    ...: 

In [2]: !echo {lawrence_lessig}
A quiet kid among strangers. A deep blue pool, hiding a volcano.

In [3]: 
```

### Built-In Magic Commands

These are some of the [built-in IPython magic commands](https://ipython.readthedocs.io/en/stable/interactive/magics.html):

**Line Magics**

`%automagic`  
Make magic functions callable without having to type the initial `%`.

Without arguments, `%automagic` toggles this feature on/off. With arguments, `%automagic` sets the value (`%automagic on`, `%automagic off`).

`%cd`  
Change the current working directory. Tilde expansion is supported (i.e., `%cd ~`).

`!cd` does not work for this purpose because the shell where `!cmd` runs is immediately discarded after executing `cmd`.

`%config`  
Display available IPython configuration classes.

To view what is configurable for a given class, do `%config ex_class`. `%config` can be passed a trait/value pair for configuration, e.g., `%config ex_class.ex_trait = ex_value`.

`%debug`  
Activate the interactive debugger in post-mortem mode. If an exception has just occurred, this lets you interactively inspect its stack frames.

This will always only work on the last traceback that occurred.

`%dhist`  
Print your history of visited directories.

`%doctest_mode`  
Toggle doctest mode on and off.

This mode is intended to make IPython behave as much as possible like a normal Python shell (e.g., how its prompt, exceptions, and output look). This makes it easy to copy and paste parts of a session into doctests.

`%edit`  
Open up an empty editor with a temporary file and execute the contents of this file when you close it.

By default, calls the editor specified by your `${EDITOR}` environment variable. If this variable is not set, defaults to `vi`.

A number of argument types can be passed to this command:

- _filename_ - File is loaded into the editor. Its contents are executed with `execfile()` when you exit, loading any code in the file into your interactive namespace.
- _ranges_ - Ranges of input history. The syntax is the same as the `%history` magic command.
- _string variable_ - Its contents are loaded into the editor.
- _object name_ (not a string) - IPython will try to locate the file where it was defined and open the editor at the point where it is defined.
- _macro_ - Opens up your specified editor with a temporary file containing the macro’s data. Upon exit, the macro is reloaded with the contents of the file.

`%history`  
Print input history, with most recent last. Add the `-n` option to include line numbers.

`%history -g ex_pat`  
Treat `ex_pat` as a glob pattern to search for in the full history.

`ex_pat` may contain a `?` to match a single unknown character or `*` to match any number of unknown characters.

`%history -g` without a pattern shows the full saved history.

`%logoff`  
Temporarily stop logging after you have started logging.

`%logon`  
Restart logging after temporarily stopping logging with `%logoff`.

For starting logging for the first time, you must use `%logstart`.

`%logstart`  
Start logging in a session.

If no name is given, logging information is saved to a file named `ipython_log.py` in your current directory, in [_rotate_](https://ipython.readthedocs.io/en/stable/interactive/magics.html#magic-logstart) mode.

`%load ex_source`  
Load code into the current frontend. `ex_source` can be  a filename, URL, input history range, macro, or element in the user namespace.

`%lsmagic`  
List currently available magic functions.

`%macro ex_name ex_range`  
Define a macro for future re-execution. `ex_range` can be ranges of history, filenames, and `str` objects.

This magic command creates a global variable `ex_name`, which is a string made by joining the slices and lines specified in `ex_range` into a single string. The variable acts like an automatic function that re-executes those lines as if you had typed them.

`%magic`  
Print information about the magic function system.

`%pdb`  
Toggle the interactive Python debugger debugger on and off.

`%pdef ex_callable_obj`  
Print the call signature for any callable object.

`%pdoc ex_obj`  
Print the docstring for an object.

If provided a class object, prints both the class and constructor docstrings.

`%pinfo2 ex_obj`  
Provide extra detailed information about an object. Equivalent to `ex_obj??` and is similar to combining `%pdoc ex_obj` and `%psource ex_obj`.

`%pip`  
Run the pip package manager within the current kernel.

`%psearch ex_pattern`  
Search for object in namespaces by wildcard.

`%psource ex_obj`  
Print (or run through pager) the source code for an object.

`%quickref`  
Show a quick reference of all IPython specific syntax and magics.

`%rerun`  
Re-run the last line of input.

You can provide a range of lines or a specific number of last lines (`-l ex_num`) as arguments to `%rerun` to only re-run those lines.

`%reset -f`
Reset the namespace by removing all names defined by the user (without asking for confirmation).

By default, a hard reset is done. Add the `-s` option to do a soft reset, which leaves the history intact.

`%run ex_file`  
Run the named file inside IPython as a program.

`%run -d ex_file`  
Run the named file under the Python debugger. Initial breakpoints are automatically set for you.

A custom breakpoint can be added with the `-b ex_line_num` option (`ex_line_num` should not be a blank line). Breakpoints in different files (e.g., an imported module) are supported, as well (`-b ex_file:ex_line_num`).

`%save ex_file`  
Save a set of lines or a macro to a given filename.

Use the `-a` option to have session content appended to `ex_file`, instead of overwriting it.

To start a new IPython session using the file's contents, enter `ipython -i ex_file`.

`%store`  
Show list of all variables and their current values.

`%store ex_var...`  
Store the current value of a variable. If you change the value of a variable, you need to store it again if you want to persist the new value.

`%store -r`  
Refresh all variables, aliases, and directory history.

`%store -r ex_var...`  
Refresh specified variables and aliases from store.

`%sx ex_shell_cmd`  
Run shell command and capture output. Equivalent to `!! ex_shell_cmd`.

Output is machine readable output from standard out (e.g., without colors) and splits on newlines.

`%tb`  
Print the last traceback.

`%timeit`  
Time execution of a Python statement or expression.

`%who_ls`  
Return a sorted list of all interactive variables.

Variable types can be passed to `%who_ls` to only view interactive variables of those types.

`%whos`  
Print all interactive variables, with some minimal formatting and extra information about each variable.

Variable types can be passed to `%whos` to only view interactive variables of those types.

**Cell Magics**

`%%bash`  
Run cells with Bash in a subprocess. Equivalent to `%%script bash`.

`%%capture ex_var`  
Run the cell and capture the standard output, standard error, and IPython's rich `display()` calls in `ex_var`.

`%%html`  
Render the cell as a block of HTML.

`%%script ex_pgm`  
Run a cell via `ex_pgm`.

`%%timeit`  
Time execution of a Python statement or expression.

`%%writefile ex_file`  
Write the contents of the cell to a file.

`ex_file` will be created if it does not exist. If `ex_file` exists, it will be overwritten, unless the `-a` (`--append`) option is used.

### Code Blocks

You can force execution of the current code block using the sequence **Escape**, **Enter**.

You can force insertion of a new line at the cursor position with **Ctrl+o**.

### Suppress Output

Place a `;` at the end of a line to suppress the printing of output, e.g., when doing calculations that generate long output you are not interested in seeing.

Also, this keeps the object out of the output cache, so if you are working with large temporary objects, they are released from memory sooner.

### Recursive Reload

`IPython.lib.deepreload` allows you to recursively reload a module, i.e., changes made to any of the module's dependencies are reloaded without having to exit:

`from IPython.lib.deepreload import reload as dreload`

## JupyterLab

Start JupyterLab with the `jupyter lab` command.

### Configuration

JupyterLab manages several different locations for its data, which can be viewed by running `jupyter lab path`:

- **Application Directory** (`<sys-prefix>/share/jupyter/lab/`): Where JupyterLab stores the main build of JupyterLab with associated data, including extensions built into JupyterLab. `<sys-prefix>` is the site-specific directory prefix of the current Python environment.
- **User Settings Directory** (`${HOME}/.jupyter/lab/user-settings/`): Where JupyterLab stores user-level settings for JupyterLab extensions.
- **Workspaces Directory** (`${HOME}/.jupyter/lab/workspaces/`): Where JupyterLab stores workspaces.

Also, JupyterLab honors `labconfig` subdirectories of the JupyterLab `config` directories in the JupyterLab path hierarchy, and can load prebuilt extensions from the `labextensions` subdirectories of the JupyterLab `data` directories.

All JupyterLab configuration paths can be viewed with the `jupyter --path` command.

A [default config file](https://docs.jupyter.org/en/latest/use/config.html#id2) for JupyterLab can be created by running:

`jupyter lab --generate-config`

The config file will be located at:

`${HOME}/.jupyter/jupyter_lab_config.py`

### Interface Configuration

These settings can be added under **Settings > Settings Editor > JSON Settings Editor**.

#### Notebook

<details>
    <summary>Show/Hide Notebook Settings</summary>

```json
{
    "codeCellConfig": {
        "autoClosingBrackets": false,
        "cursorBlinkRate": 530,
        "fontFamily": null,
        "fontSize": null,
        "lineHeight": null,
        "lineNumbers": false,
        "lineWrap": "off",
        "matchBrackets": true,
        "readOnly": false,
        "insertSpaces": true,
        "tabSize": 4,
        "wordWrapColumn": 80,
        "rulers": [
            79
        ],
        "codeFolding": false,
        "lineWiseCopyCut": true,
        "showTrailingSpace": false
    },
    "defaultCell": "code",
    "kernelShutdown": false,
    "markdownCellConfig": {
        "autoClosingBrackets": false,
        "cursorBlinkRate": 530,
        "fontFamily": null,
        "fontSize": null,
        "lineHeight": null,
        "lineNumbers": false,
        "lineWrap": "on",
        "matchBrackets": false,
        "readOnly": false,
        "insertSpaces": true,
        "tabSize": 4,
        "wordWrapColumn": 80,
        "rulers": [],
        "codeFolding": false,
        "lineWiseCopyCut": true,
        "showTrailingSpace": false
    },
    "rawCellConfig": {
        "autoClosingBrackets": false,
        "cursorBlinkRate": 530,
        "fontFamily": null,
        "fontSize": null,
        "lineHeight": null,
        "lineNumbers": false,
        "lineWrap": "on",
        "matchBrackets": false,
        "readOnly": false,
        "insertSpaces": true,
        "tabSize": 4,
        "wordWrapColumn": 80,
        "rulers": [],
        "codeFolding": false,
        "lineWiseCopyCut": true,
        "showTrailingSpace": false
    },
    "scrollPastEnd": true,
    "recordTiming": false,
    "maxNumberOutputs": 50,
    "showEditorForReadOnlyMarkdown": true,
    "kernelStatus": {
        "showOnStatusBar": false,
        "showProgress": true
    },
    "renderingLayout": "default",
    "sideBySideLeftMarginOverride": "10px",
    "sideBySideRightMarginOverride": "10px"
}
```

</details>

#### Text Editor

<details>
    <summary>Show/Hide Text Editor Settings</summary>

```json
{
    "editorConfig": {
        "autoClosingBrackets": false,
        "codeFolding": false,
        "cursorBlinkRate": 530,
        "fontFamily": null,
        "fontSize": null,
        "insertSpaces": true,
        "lineHeight": null,
        "lineNumbers": true,
        "lineWrap": "on",
        "matchBrackets": true,
        "readOnly": false,
        "rulers": [
            79
        ],
        "showTrailingSpace": false,
        "tabSize": 4,
        "wordWrapColumn": 80
    },
    "toolbar": []
}
```

</details>

### URLs and Workspaces

JupyterLab’s file navigation URLs are `/tree` URLs:

`http(s)://<server:port>/<lab-location>/lab/tree/path/to/notebook.ipynb`

JupyterLab sessions always reside in a workspace. Workspaces contain the state of JupyterLab, e.g., the files that are currently open, and the layout of the application areas and tabs. When the page is refreshed, the workspace is restored.

The default workspace does not have a name and resides at the primary `/lab` URL:

`http(s)://<server:port>/<lab-location>/lab`

All other workspaces have a name that is part of the URL:

`http(s)://<server:port>/<lab-location>/lab/workspaces/ex_ws_name`

Workspaces save their state on the server and can be shared between multiple users (or browsers), as long as they have access to the same server.

A workspace should only be open in a single browser tab at a time. If JupyterLab detects that a workspace is being simultaneously opened multiple times, it will prompt for a new workspace name.

The contents of a workspace can be copied into another workspace with the `clone` url parameter.

The `reset` url parameter clears a workspace of its contents.

Workspaces can also be exported and imported via the command line.

`http(s)://<server:port>/<lab-location>/lab/workspaces/ex_dst?clone=ex_src`  
Copy the contents of `ex_src` into `ex_dst`.

`http(s)://<server:port>/<lab-location>/lab/workspaces/ex_dst?clone`  
Copy the contents of the default workspace into `ex_dst`.

`http(s)://<server:port>/<lab-location>/lab?clone=ex_src`  
Copy the contents of `ex_src` into the default workspace.

`http(s)://<server:port>/<lab-location>/lab/workspaces/ex_ws?reset`  
Reset the contents of workspace `ex_ws`.

`http(s)://<server:port>/<lab-location>/lab/workspaces/lab?reset`  
Reset the contents of the default workspace.

`jupyter lab workspaces export > ex_file.json`  
Export the default JupyterLab workspace.

`jupyter lab workspaces export ex_ws > ex_file.json`  
Export `ex_ws` workspace.

`jupyter lab workspaces import ex_file.json`  
Import `ex_file.json` workspace file.

### Documents and Kernels

In the Jupyter architecture, _kernels_ are separate processes started by the server that run code in different programming languages and environments.

Any text file can be connected to a code console and kernel. Then, code _inside of the text file_ can be interactively run by selecting it and pressing **Shift + Enter**. Input/Output is displayed in the associated code console.

If the file is a Markdown file and the cursor is placed in a code block (without a selection), **Shift + Enter** runs the entire code block.

<https://jupyterlab.readthedocs.io/en/stable/user/documents_kernels.html>

### File and Output Formats

JupyterLab supports the viewing/editing of data in a wide variety of formats. A single file type may be supported by multiple viewers/editors (e.g., editing a `.md` file in the file editor or rendering/displaying it as HTML).

The display API for a kernel selects different output formats for notebook or code console content:

```python
from IPython.display import HTML
display(HTML("<h1>Kon'nichiwa sekai!</h1>"))
```

IPython's `display()` function can also create raw, rich output from a dictionary of keys (MIME types) values (MIME data):

```python
display({'text/html': '<h1>Hello World</h1>', 'text/plain': 'Hello World'},
        raw=True)
```

IPython kernel attributes:

```pycon
['Audio',
 'Code',
 'DisplayHandle',
 'DisplayObject',
 'FileLink',
 'FileLinks',
 'GeoJSON',
 'HTML',
 'IFrame',
 'Image',
 'JSON',
 'Javascript',
 'Latex',
 'Markdown',
 'Math',
 'Pretty',
 'ProgressBar',
 'SVG',
 'ScribdDocument',
 'TextDisplayObject',
 'Video',
 'VimeoVideo',
 'YouTubeVideo',
 '__builtins__',
 '__cached__',
 '__doc__',
 '__file__',
 '__loader__',
 '__name__',
 '__package__',
 '__spec__',
 'clear_output',
 'display',
 'display_html',
 'display_javascript',
 'display_jpeg',
 'display_json',
 'display_latex',
 'display_markdown',
 'display_pdf',
 'display_png',
 'display_pretty',
 'display_svg',
 'publish_display_data',
 'set_matplotlib_close',
 'set_matplotlib_formats',
 'update_display']
```

<table>
  <caption>Common Supported Data Formats</caption>
  <thead>
    <tr>
      <th>Data Format</th>
      <th>File Extensions</th>
      <th>MIME Types</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Markdown</td>
      <td><code>.md</code></td>
      <td><code>text/markdown</code></td>
    </tr>
    <tr>
      <td>Images</td>
      <td><code>.bmp</code>, <code>.gif</code>, <code>.jpeg</code>, <code>.jpg</code>, <code>.png</code>, <code>.svg</code></td>
      <td><code>image/bmp</code>, <code>image/gif</code>, <code>image/jpeg</code>, <code>image/png</code>, <code>image/svg+xml</code></td>
    </tr>
    <tr>
      <td>Delimiter-separated Values</td>
      <td><code>.csv</code></td>
      <td>None</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td><code>.json</code></td>
      <td><code>application/json</code></td>
    </tr>
    <tr>
      <td>HTML</td>
      <td><code>.html</code></td>
        <td><code>text/html</code></td>
    </tr>
    <tr>
      <td>LaTeX</td>
      <td><code>.tex</code></td>
      <td><code>text/latex</code></td>
    </tr>
    <tr>
      <td>PDF</td>
      <td><code>.pdf</code></td>
      <td><code>application/pdf</code></td>
    </tr>
    <tr>
      <td>Vega</td>
      <td><code>.vg</code>, <code>.vg.json</code></td>
      <td><code>application/vnd.vega.v5+json</code></td>
    </tr>
    <tr>
      <td>Vega-Lite</td>
      <td><code>.vl</code>, <code>.vl.json</code></td>
      <td><code>application/vnd.vegalite.v3+json</code></td>
    </tr>
    <tr>
      <td>Virtual DOM</td>
      <td><code>.vdom</code>, <code>.json</code></td>
      <td><code>application/vdom.v1+json</code></td>
    </tr>
  </tbody>
</table>

<https://jupyterlab.readthedocs.io/en/stable/user/file_formats.html>

### Keyboard Shortcuts

Open the _Command Palette_ by pressing **Ctrl+Shift+C**.

Switch between different notebook tabs by pressing **Ctrl+Shift+{** and **Ctrl+Shift+}**.

Keyboard shortcuts can be customized under **Settings > Settings Editor > Keyboard Shortcuts**.

```json
{
    "shortcuts": [
        {
            "command": "runmenu:restart-and-run-all",
            "keys": [
                "Accel Alt A"
            ],
            "selector": "[data-jp-code-runner]",
            "title": "Restart Kernel and Run All",
            "category": "Run Menu"
        }, 
    ]
}
```

### Multicursor

Hold down the **Alt** key and select the lines you would like a cursor placed on. Press the **left arrow**. There will be multiple cursors, one on each line.

### Tab Completion

When typing an object name, press **Tab** to view potential object matches.

Press **Shift+Tab** after typing an object name for additional information on that object.

### Extensions

A JupyterLab extension contains JavaScript that is installed into JupyterLab and run in the browser. An extension contains one or more plugins that extend JupyterLab.

There are two types of JupyterLab extensions:

1. Source extension - Requires a rebuild of JupyterLab when installed.
2. Prebuilt extension - Does not require a rebuild of JupyterLab.

JupyterLab extensions can be installed in various ways:

- `pip` or `conda` packages can install both source and prebuilt extensions.
- The JupyterLab _Extension Manager_ and `jupyter labextension install` command can install source extensions from [npm](https://www.npmjs.com/search?q=keywords:jupyterlab-extension).

Helpful commands include:

`jupyter labextension list`  
List all installed extensions.

The JavaScript package name displayed may be different from a package's `pip` or `conda` package name.

`jupyter labextension disable my-extension`  
Disable an extension.

`jupyter labextension enable my-extension`  
Enable an extension.

When using the command line, the level of the configuration can be targeted with the `--level` option. The level options are:

- `user`
- `system`
- `sys-prefix` (the default)

JupyterLab supports many community-contributed unofficial [extensions](https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/index.html).

### Exporting Notebooks

JupyterLab allows you to export your Jupyter notebook files (`.ipynb`) into other file formats, like:

- Asciidoc (`.asciidoc`)
- HTML (`.html`)
- LaTeX (`.tex`)
- Markdown (`.md`)
- PDF (`.pdf`)
- ReStructured Text (`.rst`)
- Executable Script (`.py`)
- Reveal.js Slides (`.html`)

Export options are available via **File > Save and Export Notebook As...**.

Exporting can also be accomplished via the [command line](https://nbconvert.readthedocs.io/en/latest/usage.html). For example:

`jupyter nbconvert --to html ex_nb.ipynb`

Exporting to _Reveal.js Slides_ requires [additional steps](https://jupyterlab.readthedocs.io/en/stable/user/export.html#reveal-js-slides).

### Cell Highlighting

Add the following to a cell for cell highlighting:

```html
<div class="alert alert-success" role="alert">
</div>
```

```html
<div class="alert alert-warning" role="alert">
</div>
```

```html
<div class="alert alert-danger" role="alert">
</div>
```

```html
<div class="alert alert-info" role="alert">
</div>
```

## Exploratory Data Analysis

### `ydata-profiling`

```python
# Enable generating a profile report from a data set
# stored as a pandas DataFrame
from ydata_profiling import ProfileReport
# Enable a high-level interface to allow displaying Web-based
# documents to users
import webbrowser as wb

# Generate standard profiling report
profile = ProfileReport(df, title='ex_title')

# Generate profiling report with custom description
prof_rpt_desc = 'ex_description'
profile = ex_df.profile_report(title=prof_rpt_title,
                               dataset={'description': prof_rpt_desc})

profile.to_widgets()  # Display report as set of widgets

profile.to_file('ex_report.html')  # Generate HTML report file
wb.open('ex_report.html')  # Open report in default browser
```

### D-Tale

```python
import dtale  # Enable web client for visualizing pandas objects

d = dtale.show(df)  # Assign a reference to a running D-Tale process

d.open_browser()  # Open default browser to this process
```

## Time Series

### Creation

To create a pandas time series `DataFrame`, convert the `DataFrame` index to datetime (e.g., `datetime64[ns]`) values.

If a data set _already has a properly formatted column of strings_ to use as the `DatetimeIndex`, the column can be converted to datetime values and set as the `DataFrame` index via panda's `read_*()` import functions' `parse_dates` and `index_col` parameters:

```python
import pandas as pd  # Enable data structures and data analysis tools

df = pd.read_csv(
    '/data_sets/ex_file.csv',
    parse_dates=['ex_dt_col'],
    index_col=['ex_dt_col']
)
```

If a data set has date and time values that are not properly formatted, post-import processing can help.

The following steps (where applicable) may be beneficial:

- Convert all date/time columns to strings.

        df['ex_dt_col'] = df['ex_dt_col'].astype('str')

- Refine the values to the desired date/time format using [string methods](https://pandas.pydata.org/docs/user_guide/text.html#string-methods).
- Concatenate the columns into the _final_ desired date/time format.

        df = df.assign(ex_final_dt=df['ex_date_col'] + df['ex_time_col'])

- Convert the final string date/time column to datetime values with `pandas.to_datetime()`.

        df['ex_final_dt'] = pd.to_datetime(df['ex_final_dt'],
                                           format='ex_dt_format')

A datetime column can be set as a `DataFrame` index using `DataFrame.set_index()`:

`df = df.set_index('ex_dt_col')  # Set df index to date/time column`

Sort the `DatetimeIndex` and confirm that the values are in chronological order:

```python
df = df.sort_index()  # Sort date/time index

# Confirm time series index is chronologically sorted
df.index.is_monotonic_increasing
```

<https://pandas.pydata.org/docs/reference/api/pandas.to_datetime.html>

### Resampling

_Resampling_ changes the interval of the values of the time series.

Resample a time series interval with the `DataFrame.resample()` method, which takes a new interval as an argument (e.g., `'3H'`). The values from the existing interval are grouped.

<table>
  <caption><code>DateOffset</code> Strings</caption>
  <thead>
    <tr>
      <th>Date Offset</th>
      <th>Frequency String</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.DateOffset.html#pandas.tseries.offsets.DateOffset"><code>DateOffset</code></a></td>
      <td>None</td>
      <td>Generic offset class, defaults to absolute 24 hours</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.BDay.html#pandas.tseries.offsets.BDay"><code>BDay</code></a> or <a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.BusinessDay.html#pandas.tseries.offsets.BusinessDay"><code>BusinessDay</code></a></td>
      <td><code>'B'</code></td>
      <td>Business day (weekday)</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.CDay.html#pandas.tseries.offsets.CDay"><code>CDay</code></a> or <a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.CustomBusinessDay.html#pandas.tseries.offsets.CustomBusinessDay"><code>CustomBusinessDay</code></a></td>
      <td><code>'C'</code></td>
      <td>Custom business day</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.Week.html#pandas.tseries.offsets.Week"><code>Week</code></a></td>
      <td><code>'W'</code></td>
      <td>One week, optionally anchored on a day of the week</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.WeekOfMonth.html#pandas.tseries.offsets.WeekOfMonth"><code>WeekOfMonth</code></a></td>
      <td><code>'WOM'</code></td>
      <td>The x-th day of the y-th week of each month</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.LastWeekOfMonth.html#pandas.tseries.offsets.LastWeekOfMonth"><code>LastWeekOfMonth</code></a></td>
      <td><code>'LWOM'</code></td>
      <td>The x-th day of the last week of each month</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.MonthEnd.html#pandas.tseries.offsets.MonthEnd"><code>MonthEnd</code></a></td>
      <td><code>'M'</code></td>
      <td>Calendar month end</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.MonthBegin.html#pandas.tseries.offsets.MonthBegin"><code>MonthBegin</code></a></td>
      <td><code>'MS'</code></td>
      <td>Calendar month begin</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.BMonthEnd.html#pandas.tseries.offsets.BMonthEnd"><code>BMonthEnd</code></a> or <a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.BusinessMonthEnd.html#pandas.tseries.offsets.BusinessMonthEnd"><code>BusinessMonthEnd</code></a></td>
      <td><code>'BM'</code></td>
      <td>Business month end</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.BMonthBegin.html#pandas.tseries.offsets.BMonthBegin"><code>BMonthBegin</code></a> or <a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.BusinessMonthBegin.html#pandas.tseries.offsets.BusinessMonthBegin"><code>BusinessMonthBegin</code></a></td>
      <td><code>'BMS'</code></td>
      <td>Business month begin</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.CBMonthEnd.html#pandas.tseries.offsets.CBMonthEnd"><code>CBMonthEnd</code></a> or <a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.CustomBusinessMonthEnd.html#pandas.tseries.offsets.CustomBusinessMonthEnd"><code>CustomBusinessMonthEnd</code></a></td>
      <td><code>'CBM'</code></td>
      <td>Custom business month end</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.CBMonthBegin.html#pandas.tseries.offsets.CBMonthBegin"><code>CBMonthBegin</code></a> or <a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.CustomBusinessMonthBegin.html#pandas.tseries.offsets.CustomBusinessMonthBegin"><code>CustomBusinessMonthBegin</code></a></td>
      <td><code>'CBMS'</code></td>
      <td>Custom business month begin</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.SemiMonthEnd.html#pandas.tseries.offsets.SemiMonthEnd"><code>SemiMonthEnd</code></a></td>
      <td><code>'SM'</code></td>
      <td>15th (or other day of month) and calendar month end</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.SemiMonthBegin.html#pandas.tseries.offsets.SemiMonthBegin"><code>SemiMonthBegin</code></a></td>
      <td><code>'SMS'</code></td>
      <td>15th (or other day of month) and calendar month begin</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.QuarterEnd.html#pandas.tseries.offsets.QuarterEnd"><code>QuarterEnd</code></a></td>
      <td><code>'Q'</code></td>
      <td>Calendar quarter end</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.QuarterBegin.html#pandas.tseries.offsets.QuarterBegin"><code>QuarterBegin</code></a></td>
      <td><code>'QS'</code></td>
      <td>Calendar quarter begin</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.BQuarterEnd.html#pandas.tseries.offsets.BQuarterEnd"><code>BQuarterEnd</code></a></td>
      <td><code>'BQ'</code></td>
      <td>Business quarter end</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.FY5253Quarter.html#pandas.tseries.offsets.FY5253Quarter"><code>FY5253Quarter</code></a></td>
      <td><code>'REQ'</code></td>
      <td>Retail (aka 52-53 week) quarter</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.YearEnd.html#pandas.tseries.offsets.YearEnd"><code>YearEnd</code></a></td>
      <td><code>'A'</code></td>
      <td>Calendar year end</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.YearBegin.html#pandas.tseries.offsets.YearBegin"><code>YearBegin</code></a></td>
      <td><code>'AS'</code> or <code>'BYS'</code></td>
      <td>Calendar year begin</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.BYearEnd.html#pandas.tseries.offsets.BYearEnd"><code>BYearEnd</code></a></td>
      <td><code>'BA'</code></td>
      <td>Business year end</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.BYearBegin.html#pandas.tseries.offsets.BYearBegin"><code>BYearBegin</code></a></td>
      <td><code>'BAS'</code></td>
      <td>Business year begin</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.FY5253.html#pandas.tseries.offsets.FY5253"><code>FY5253</code></a></td>
      <td><code>'RE'</code></td>
      <td>Retail (aka 52-53 week) year</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.Easter.html#pandas.tseries.offsets.Easter"><code>Easter</code></a></td>
      <td>None</td>
      <td>Easter holiday</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.BusinessHour.html#pandas.tseries.offsets.BusinessHour"><code>BusinessHour</code></a></td>
      <td><code>'BH'</code></td>
      <td>Business hour</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.CustomBusinessHour.html#pandas.tseries.offsets.CustomBusinessHour"><code>CustomBusinessHour</code></a></td>
      <td><code>'CBH'</code></td>
      <td>Custom business hour</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.Day.html#pandas.tseries.offsets.Day"><code>Day</code></a></td>
      <td><code>'D'</code></td>
      <td>One absolute day</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.Hour.html#pandas.tseries.offsets.Hour"><code>Hour</code></a></td>
      <td><code>'H'</code></td>
      <td>One hour</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.Minute.html#pandas.tseries.offsets.Minute"><code>Minute</code></a></td>
      <td><code>'T'</code> or <code>'min'</code></td>
      <td>One minute</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.Second.html#pandas.tseries.offsets.Second"><code>Second</code></a></td>
      <td><code>'S'</code></td>
      <td>One second</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.Milli.html#pandas.tseries.offsets.Milli"><code>Milli</code></a></td>
      <td><code>'L'</code> or <code>'ms'</code></td>
      <td>One millisecond</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.Micro.html#pandas.tseries.offsets.Micro"><code>Micro</code></a></td>
      <td><code>'U'</code> or <code>'us'</code></td>
      <td>One microsecond</td>
    </tr>
    <tr>
      <td><a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.tseries.offsets.Nano.html#pandas.tseries.offsets.Nano"><code>Nano</code></a></td>
      <td><code>'N'</code></td>
      <td>One nanosecond</td>
    </tr>
  </tbody>
</table>

The aggregated value of the series is calculated via chaining an aggregate method (e.g., `.sum()`, `.median()`, `.max()`, `.min()`) to calculate the new aggregate values.

```python
df = df.resample('ex_interval').ex_agg_meth()  # Resample df

df.head(10)  # Confirm
```

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.resample.html>

### Rolling Mean

The _rolling mean_ (or _moving average_) is a method of smoothing the data (i.e., reducing fluctuations) in a time series. The method involves finding the values least susceptible to fluctuations (i.e., the arithmetic mean).

The interval for averaging (the _window size_) is experimentally selected. The larger the interval, the stronger the smoothing. Then, the window starts to _roll_ almost from the beginning to the end of the time series. The mean value is calculated at each point.

In the moving average, the windows overlap and cannot go beyond the series. So, the number of obtained means is slightly less than the number of the initial values of the series.

Create a rolling window with the `DataFrame.rolling()` method and calculate the mean with `.mean()`:

`df['rolling_mean'] = df.rolling(ex_size).mean()  # Create rolling mean`

In pandas, the result of averaging is displayed _at the end of the window_, not in the middle.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.rolling.html>

### Rolling Standard Deviation

Use the `.rolling()` and `.std()` methods:

```python
# Create rolling standard deviation
df['rolling_std'] = df.rolling(ex_size).std()
```

### Trends, Seasonality, and Residuals

The `statsmodels.tsa.seasonal.seasonal_decompose()` function from the `tsa` (time series analysis) module can be used to break a time series down into three components:

1. _Trend_ - A smooth change of the mean value of the time series without repeating patterns (e.g., an annual increase in video game sales).
2. _Seasonality_ - Cyclically repeating patterns in a time series (e.g., an increase in video game sales every holiday season).
3. _Residuals_ - The component that cannot be explained by the trend and seasonality components (i.e., noise).

Trends and seasonality depend on the _scale_ of the data (e.g., you cannot see the patterns repeating every holiday season if there is data for only one year).

```python
# Enable seasonal decomposition using moving averages
from statsmodels.tsa.seasonal import seasonal_decompose

decomposed = seasonal_decompose(df)
```

`seasonal_decompose()` takes a time series and returns an instance of the `DecomposeResult` class. The resulting object stores the three components as attributes:

- `.trend` - trend component
- `.seasonal` - seasonal component
- `.resid` - residuals

<https://www.statsmodels.org/stable/generated/statsmodels.tsa.seasonal.seasonal_decompose.html>

#### Plotting Time Series Components

```python
# Enable comprehensive library for creating static, animated, and
# interactive visualizations in Python
import matplotlib.pyplot as plt

# Create seasonal decomposition
decomposed = seasonal_decompose(df['criterion'])

# Create figure and Axes
fig, (ax_1, ax_2, ax_3) = plt.subplots(3, 1, figsize=(10, 10),
                                       layout='constrained')

# Plot subplots
decomposed.trend.plot(ax=ax_1)
plt.title('Trend')

decomposed.seasonal.plot(ax=ax_2)
plt.title('Seasonality')

decomposed.resid.plot(ax=ax_3)
plt.title('Residuals');
```

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.html>

#### Time Series Component Validation

To confirm that a time series was correctly broken down, add the components together and compare the sum with the original piece of data. The values should match:

```python
# Sum arbitrary slice of components
components_sum = (
    decomposed.trend.iloc[ex_start_idx:ex_end_idx]
    + decomposed.seasonal.iloc[ex_start_idx:ex_end_idx]
    + decomposed.resid.iloc[ex_start_idx:ex_end_idx]
)
components_sum

# Obtain corresponding slice from original data
orig = df.iloc[ex_start_idx:ex_end_idx, ex_crit_col_idx]
orig
```

## Polars

### Usage

`import polars as pl  # Enable fast DataFrame library`

```python
# Single thread usage

query = (
    'SELECT '
    '* '
    'FROM '
    'Artist '
    'LIMIT '
    '10'
)
db_url = 'sqlite:////home/comcomly/chinook.sqlite'

# Read the results of a SQL query into a DataFrame
df = pl.read_database(
    query,
    db_url,
    engine='connectorx'
)
```

```python
# Define data directory and file path
data_dir = '/home/comcomly/data'
f_path = f'{data_dir}/ex_file.parquet'
```

`df.write_parquet(f_path)  # Write to Apache Parquet file`

```python
# Read into a DataFrame from a parquet file
df = pl.read_parquet(f_path, use_pyarrow=True, low_memory=True)

# Lazily read from a parquet file
df = pl.scan_parquet(f_path, low_memory=True)
```

```python
# Define query
query = (
    df.filter(pl.col('ex_col_1') == 'ex_val_1')
      .filter(pl.col('ex_col_2') == 'ex_val_2')
)
# Display a string representation of the query plan
print(query.explain())
```

```python
# Define columns of interest
cols = [
    'ex_col_1',
    'ex_col_2',
    'ex_col_3'
]

# Collect query results into a DataFrame
df_q = query.collect().select(pl.col(cols))
# df_q = query.collect().select('*')

# Cast to a pandas DataFrame
df_pd = df_q.to_pandas()
```

### Contexts

Polars has its own Domain Specific Language (DSL) for transforming data. Its two core components are *contexts* and *expressions*.

A context refers to the context in which an expression needs to be evaluated. There are three main contexts:

1. Selection - `df.select([..])`, `df.with_columns([..])`

    In the `.select()` context, the selection applies expressions over columns. The expressions in this context must produce `Series` that are all the same length or have a length of `1`.

    A `Series` of a length `1` will be broadcast to match the height of the `DataFrame`. A `.select()` may produce new columns that are aggregations, combinations of expressions, or literals.

        out = df.select(
            pl.sum('Bytes'),
            pl.col('Name')
              .sort(),
            pl.col('Name')
              .first()
              .alias('first_track_name'),
            (pl.mean('Bytes') * 10).alias('10_x_bytes')
        )

    The `.with_columns()` method is another entrance to the selection context. The main differences is that `.with_columns()` retains the original columns and adds new ones, while `.select()` drops the original columns.

        df = df.with_columns(
            pl.sum('Bytes')
              .alias('bytes_sum'),
            pl.col('Milliseconds')
              .count()
              .alias('count')
        )

2. Filtering - `df.filter()`

    In the `.filter()` context, you filter the existing `DataFrame` based on arbitrary expressions, which evaluate to the `Boolean` data type.

    `out = df.filter(pl.col('Bytes') > 8000000)`

3. Groupby / Aggregation - `df.groupby(..).agg([..])`

    In the `.groupby()` context, expressions work on groups and may yield results of any length (a group may have many members).

        out = (
            df.groupby('AlbumId')
              .agg(
                  pl.sum('Bytes'),  # Sum Bytes by AlbumId
                  pl.col('Milliseconds')
                    .count()
                    .alias('count'),  # Count AlbumId members
                  # Sum Milliseconds where Name != null
                  pl.col('Milliseconds')
                    .filter(pl.col('Name')
                    .is_not_null())
                    .sum()
                    .suffix('_sum'),
                  pl.col('Name')
                    .reverse()
                    .alias('reversed_name')
              )
        )

    All expressions are applied to the group defined by the `.groupby()` context. Besides `.groupby()`, `.groupby_dynamic()` and `.groupby_rolling()` are also entrances to the groupby context.

## Apache Spark

[Apache Spark](https://spark.apache.org/docs/latest/) is an open-source distributed computing framework. Spark allows us to store and process data on several computers at once. Spark is written in the [Scala](https://www.scala-lang.org/) programming language. [PySpark](https://spark.apache.org/docs/latest/api/python/index.html) is the Python API for Apache Spark.

As the volume of data grows, one computer may not be enough to perform calculations. This is where _distributed systems_ become useful. These systems give access to files stored on different machines.

Files are divided into *segments*, and each segment can be saved multiple times, in a few different locations. This method guarantees that the data can be utilized in case of an emergency.

Distributed file systems consist of multiple _nodes_. A node is a computer with the ability to compute and process data.

There are two types of nodes:

1. A _NameNode_ manages the assignment of files to different machine clusters.
2. A _DataNode_ stores and processes our data. Each file will be replicated in multiple data nodes to avoid data loss.

There are two ways to improve a computer's ability to store and process more data:

1. Increasing the efficiency of every node or replacing the nodes with more powerful ones. This method is called _scaling up_ (vertical scaling).
2. Increasing the _number_ of nodes in a cluster is called _scaling out_ (horizontal scaling). This way, nodes will perform parallel data processing. This method helps to scale faster.

### Resilient Distributed Data Sets

The basic building of the Spark library is the _Resilient Distributed Dataset (RDD)_ data type.

An RDD is a distributed data structure located across multiple nodes in a cluster. RDDs can be used for transforming unstructured data and are part of more complex data types (e.g., DataFrames).

`SparkContext()` is responsible for cluster operations in Spark. We define a SparkContext object by initializing it. We can also pass different configurations to it. Some examples of configurations are the app name (`appName`) and the URL (`master`) of the NameNode.

```python
# Enable main entry point for Spark functionality
from pyspark import SparkContext

sc = SparkContext(appName='ex_app_name')
```

<https://spark.apache.org/docs/latest/api/python/reference/api/pyspark.SparkContext.html>

`.parallelize()` can be used to convert a list into an RDD. It produces an _RDD parallel collection_, i.e., the objects that will be used in parallel processing.

<https://spark.apache.org/docs/latest/api/python/reference/api/pyspark.SparkContext.parallelize.html>

To get the contents of the RDD, call the `.take()` method:

```python
# Enable main entry point for Spark functionality
from pyspark import SparkContext

sc = SparkContext()
entry = sc.parallelize(['2025-02-18', 12.4, 28.2])
entry.take(3)

# [Stage 0:> 0 + 1) / 1]
# ['2025-02-18', 12.4, 28.2]
```

`Stage 0:>` shows the process of completing the task.

<https://spark.apache.org/docs/latest/api/python/reference/api/pyspark.RDD.take.html>

### PySpark DataFrames

A PySpark DataFrame is a table that stores its rows in RDDs. It is similar to pandas DataFrames, but there are a number of differences:

- PySpark DataFrames are immutable. This means that any change (such as renaming a column or adding another column) will produce a new copy of the DataFrame.
- PySpark DataFrames are _lazily evaluated_. This means that the calculations are delayed until the user requests their results. We can take a look at the DataFrame after executing the methods `.collect()` or `.show()`.

&nbsp;

- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrame.collect.html>
- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrame.show.html>

To work with a distributed file system, you will need to use the _DataFrame API_. It is located in the [`Spark SQL`](https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/index.html) module in the PySpark library. It can be accessed through PySpark, but also has its own command line console where you can write SQL commands without importing from the library.

Keep in mind, we used the `SparkContext` object to access the PySpark library, where we created RDDs. However, to create and use DataFrames, we will use a `SparkSession` object.

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.html>

Create the SparkSession object using the `.getOrCreate()` method:

```python
# Enable entry point to programming Spark with the Dataset and DataFrame API
from pyspark.sql import SparkSession

APP_NAME = 'ex_app'

spark = SparkSession.builder.appName(APP_NAME).getOrCreate()
```

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.builder.getOrCreate.html>

If you call the `.getOrCreate()` method again, the method will return the same object and will not create a new one.

PySpark DataFrames consist of rows. Unlike in pandas, a row in PySpark is a data type that contains column names and column values in each row of the table. PySpark shows which data types the columns contain.

Convert pandas DataFrames into PySpark DataFrames by calling `.createDataFrame()`.

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.createDataFrame.html>

Use the `.take()` method to extract data.

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrame.take.html>

A DataFrame in PySpark gets converted into a list of rows.

Use the `.select()` method to select columns.

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrame.select.html>

If you do not want the status to be displayed, add the `showConsoleProgress` setting and set its value to `false` when creating the SparkSession:

```python
# Enable entry point to programming Spark with the Dataset and DataFrame API
from pyspark.sql import SparkSession

APP_NAME = 'ex_app'

spark = (
    SparkSession.builder.appName(APP_NAME)
                        .config('spark.ui.showConsoleProgress', 'false')
                        .getOrCreate()
)
```

We can directly load a DataFrame from a `.csv` file using SparkSession's `.read` attribute. Call the `.load()` method of this attribute. This method takes a file path and loading options and generates a PySpark DataFrame.

- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.read.html>
- <https://spark.apache.org/docs/latest/sql-data-sources-load-save-functions.html>

Note that `true` and `false` values are written in lowercase letters, as in Scala, the programming language in which Apache Spark is written.

### Handling Missing Data

There are a few options for handling missing data in PySpark.

Use the `.describe()` method to get information about columns in the DataFrame. To print the result, we need to use the `.show()` method.

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrame.describe.html>

Use the `.summary()` method to print more detailed information about a DataFrame.

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrame.summary.html>

### SQL Queries in PySpark DataFrames

Use the `SparkSession` object to execute a SQL query.

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.sql.html>

### GroupBy

Perform aggregating calculations using the built-in `.groupBy()` method or `GROUP BY` within a SQL query.

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrame.groupBy.html>

To aggregate a DataFrame, apply the method and specify the column you would like to aggregate on inside the method. Then, apply the method that you would like to aggregate with. There are multiple methods available, including:

- `.sum()`
- `.mean()` (or `.avg()`)
- `.count()`
- `.min()`
- `.max()`

We can also apply multiple functions to different columns using the `.agg()` method. We can also create user-defined aggregate functions (UDAF) and use them to aggregate our data.

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrame.agg.html>

Use the `.sort()` method to sort by a column or columns.

<https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrame.sort.html>

### Code Examples

#### Spark Session Retrieval/Creation

```python
# Enable entry point to programming Spark with the Dataset and DataFrame API
from pyspark.sql import SparkSession

spark = SparkSession.builder.getOrCreate()
```

- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.html>
- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.html#pyspark.sql.SparkSession.builder>
- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.builder.getOrCreate.html>

#### Spark DataFrame Creation

```python
# Creation from SQL
sdf = spark.sql('ex_sql_query')

# Creation from pandas DataFrame
sdf = spark.createDataFrame(ex_pandas_df)

# Creation from Parquet file
sdf = spark.read.parquet(f_path)

# Creation from Parquet directory
sdf = spark.read.parquet(dir_path)

# Creation from multiple Parquet files
sdf = spark.read.parquet(f_path_1, f_path_2, f_path_3)

# Creation from multiple Parquet files via wildcard search
sdf = spark.read.parquet(f_path_pat)
```

- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.sql.html>
- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.createDataFrame.html>
- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrameReader.parquet.html>

#### pandas On Spark DataFrame Creation

```python
import pyspark.pandas as ps

# Creation from Spark DataFrame
pos_df = ex_spark_df.pandas_api()

# Creation from dictionary
pos_df = ps.DataFrame(ex_dict)

# Creation from pandas DataFrame
pos_df = ps.from_pandas(ex_pandas_df)

# Creation from Parquet file
pos_df = ps.read_parquet(f_path)
```

- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrame.pandas_api.html>
- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.pandas/api/pyspark.pandas.DataFrame.html>
- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.pandas/api/pyspark.pandas.read_parquet.html>

#### Parquet File Creation

```python
# Creation from Spark DataFrame
sdf.write.parquet(f_path)

# Creation from pandas on Spark DataFrame
pos_df.to_parquet(f_path)
```

- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.DataFrameWriter.parquet.html>
- <https://spark.apache.org/docs/latest/api/python/reference/pyspark.pandas/api/pyspark.pandas.DataFrame.to_parquet.html>

## Data Sets

- [Academic Torrents](https://academictorrents.com/)
- [Chinook Database](https://github.com/lerocha/chinook-database)
- [Data.gov](https://catalog.data.gov/dataset)
- [FiveThirtyEight](https://data.fivethirtyeight.com/)
- [IMDb](https://www.imdb.com/interfaces/)
- [State of New York Open Data](https://data.ny.gov/)
- [Medicare Provider Data](https://data.cms.gov/provider-data/search)
- [NYC Open Data](https://opendata.cityofnewyork.us/)
- [Open Data Inception](https://opendatainception.io/)
- [pandas/pandas/tests/io/data](https://github.com/pandas-dev/pandas/tree/main/pandas/tests/io/data)
- [SQLite Databases](https://github.com/duckdb/duckdb-sqlite/tree/main/data/db)
- [The Claremont Run](https://www.claremontrun.com/Data.html)

## Documentation

- [Apache Spark](https://spark.apache.org/docs/latest/index.html)
    - [Quick Start](https://spark.apache.org/docs/latest/quick-start.html)
    - [PySpark](https://spark.apache.org/docs/latest/api/python/index.html)
        - [Getting Started](https://spark.apache.org/docs/latest/api/python/getting_started/index.html)
        - [pandas API On Spark](https://spark.apache.org/docs/latest/api/python/user_guide/pandas_on_spark/index.html)
    - [Spark SQL](https://spark.apache.org/docs/latest/sql-programming-guide.html)
- [Beautiful Soup Documentation](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
- [ConnectorX Documentation](https://github.com/sfu-db/connector-x)
- [D-Tale Documentation](https://github.com/man-group/dtale#readme)
- [Dask](https://www.dask.org/)
    - [Dask Documentation](https://docs.dask.org/en/stable/index.html)
        - [API Reference](https://docs.dask.org/en/stable/api.html)
            - [DataFrame](https://docs.dask.org/en/stable/dataframe-api.html)
            - [Delayed](https://docs.dask.org/en/stable/delayed-api.html)
            - [Futures](https://docs.dask.org/en/stable/futures.html)
        - [Best Practices](https://docs.dask.org/en/stable/best-practices.html)
        - [Command Line Interface](https://docs.dask.org/en/stable/cli.html)
        - [Configuration](https://docs.dask.org/en/stable/configuration.html)
        - [Dask Internals](https://docs.dask.org/en/stable/internals.html)
        - [DataFrame](https://docs.dask.org/en/stable/dataframe.html)
        - [Debugging and Performance](https://docs.dask.org/en/stable/debugging-performance.html)
        - [Deploy Dask Clusters](https://docs.dask.org/en/stable/deploying.html)
            - [Additional Information](https://docs.dask.org/en/stable/deploying-extra.html)
            - [Cloud](https://docs.dask.org/en/stable/deploying-cloud.html)
            - [Command Line](https://docs.dask.org/en/stable/deploying-cli.html)
            - [Python API](https://docs.dask.org/en/stable/deploying-python.html)
        - [Machine Learning](https://docs.dask.org/en/stable/ml.html)
        - [Scheduling](https://docs.dask.org/en/stable/scheduling.html)
        - [Task Graphs](https://docs.dask.org/en/stable/graphs.html)
        - [Understanding Performance](https://docs.dask.org/en/stable/understanding-performance.html)
    - [Dask Examples](https://examples.dask.org/)
- [DuckDB](https://duckdb.org/)
    - [DuckDB Documentation](https://duckdb.org/docs/)
        - [Client APIs](https://duckdb.org/docs/api/overview)
            - [CLI API](https://duckdb.org/docs/api/cli/overview.html)
            - [Python API](https://duckdb.org/docs/api/python/overview.html)
                - [Known Python Issues](https://duckdb.org/docs/api/python/known_issues)
                - [Relational API](https://duckdb.org/docs/api/python/relational_api.html)
        - [Configuration](https://duckdb.org/docs/configuration/overview)
        - [Connect](https://duckdb.org/docs/connect/overview)
        - [Data Import](https://duckdb.org/docs/data/overview)
        - [Extensions](https://duckdb.org/docs/extensions/overview)
        - [Guides](https://duckdb.org/docs/guides/overview)
            - [Analyzing Railway Traffic in the Netherlands Tutorial](https://duckdb.org/2024/05/31/analyzing-railway-traffic-in-the-netherlands.html)
            - [Awesome DuckDB](https://github.com/davidgasquez/awesome-duckdb)
        - [SQL](https://duckdb.org/docs/sql/introduction)
            - [DuckDB's SQL Dialect](https://duckdb.org/docs/sql/dialect/overview)
                - [Friendly SQL](https://duckdb.org/docs/sql/dialect/friendly_sql)
- [GeoPandas Documentation](https://geopandas.org/en/stable/docs.html)
- [IPython Documentation](https://ipython.readthedocs.io/en/stable/index.html)
    - [IPython Reference](https://ipython.readthedocs.io/en/stable/interactive/reference.html#ipython-reference)
    - [Built-In Magic Commands](https://ipython.readthedocs.io/en/stable/interactive/magics.html#built-in-magic-commands)
    - [Configuration and Customization](https://ipython.readthedocs.io/en/stable/config/index.html#configuration-and-customization)
- [Interactive Tables Documentation](https://mwouts.github.io/itables/quick_start.html)
    - [DataTables Manual](https://datatables.net/manual/)
- [Jupyter Project Documentation](https://docs.jupyter.org/en/latest/)
    - [Jupyter Kernels](https://github.com/jupyter/jupyter/wiki/Jupyter-kernels#jupyter-kernels)
    - [Jupyter Widgets](https://ipywidgets.readthedocs.io/en/latest/)
    - [Jupyter Wiki](https://github.com/jupyter/jupyter/wiki)
    - [JupyterLab Documentation](https://jupyterlab.readthedocs.io/en/stable/)
    - [JupyterLite Documentation](https://jupyterlite.readthedocs.io/en/latest/)
    - [nbconvert Documentation](https://nbconvert.readthedocs.io/en/latest/index.html)
- [List Of tz Database Time Zones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
- [NumPy Documentation](https://numpy.org/doc/stable/)
- [odfpy](https://github.com/eea/odfpy)
- [openpyxl](https://openpyxl.readthedocs.io/en/stable/)
- [Polars API Reference](https://pola-rs.github.io/polars/py-polars/html/reference/index.html)
- [Polars User Guide](https://pola-rs.github.io/polars-book/)
- [PyArrow Documentation](https://arrow.apache.org/docs/python/index.html)
- [pycodestyle_magic](https://github.com/mattijn/pycodestyle_magic)
- [SciPy Documentation](https://docs.scipy.org/doc/scipy/)
- [Seaborn](https://seaborn.pydata.org/)
- [Spark Documentation](https://spark.apache.org/docs/latest/)
    - [PySpark Documentation](https://spark.apache.org/docs/latest/api/python/index.html)
- [statsmodels](https://www.statsmodels.org/stable/index.html)
- [ydata-profiling Documentation](https://ydata-profiling.ydata.ai/docs/master/index.html)