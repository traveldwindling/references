# Dask Reference

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## Primer

High-level collections are used to generate task graphs, which can be executed by schedulers on a single computer or a cluster.

The available collections are:

- Dask Array
- Dask DataFrame
- Dask Bag
- Dask Delayed
- Futures

Dask collections are normally imported as follows:

```python
import dask.array as da  # Enable parallel Dask array
import dask.bag as db  # Enable parallel collection of Python objects
import dask.dataframe as dd  # Enable parallel pandas DataFrame
```

### Creating a Dask Object

A Dask object can be created by supplying existing data and, optionally, including information about how the chunks should be structured.

```python
import pandas as pd  # Enable data structures and data analysis tools

index = pd.date_range('2024-12-04', periods=2400, freq='1h')
df = pd.DataFrame({'a': np.arange(2400), 'b': list('abcaddbe' * 300)},
                  index=index)
ddf = dd.from_pandas(df, npartitions=10)
```

Now, we have a Dask DataFrame with 2 columns and 2,400 rows composed of 10 partitions, where each partition has 240 rows. Each partition represents a piece of data.

`.divisions` and `.partitions` are key attributes of a Dask DataFrame:

```python
# Check the index values covered by each partition
ddf.divisions

# Access a particular partition
ddf.partitions[1]
```

Note that the length  of `.divisions` is equal to [`npartitions` + 1](https://docs.dask.org/en/stable/generated/dask.dataframe.DataFrame.repartition.html). This is because `.divisions` represents the upper and lower bounds of each partition.

### Indexing

Indexing Dask collections is like slicing NumPy arrays or pandas DataFrames.

```python
ddf['b']

ddf['2024-12-04':'2024-12-09 5:00']
```

### Computation

Dask is lazily evaluated. The result from a computation is not computed *until you ask for it*. Instead, a Dask task graph for the computation is produced.

To get the result from a Dask object, call the `.compute()` method:

```python
ddf['2024-12-04':'2024-12-09 5:00'].compute()
```

### Methods

Dask collections match existing NumPy and pandas methods. Call the method to set up the task graph, and then call `.compute()` to get the result:

```python
ddf['a'].mean()

ddf['a'].mean().compute()

ddf['b'].unique()

ddf['b'].unique().compute()
```

Like in pandas, methods can be chained:

```python
result = ddf['2024-12-04':'2024-12-09 5:00']['a'].cumsum() - 100

result

result.compute()
```

### Visualize the Task Graph

We can inspect the task graph to determine what is going on.

```python
result.dask

result.visualize()
# Requires graphviz
# pip install graphviz
```

### Low-Level Interfaces

Often, when parallelizing existing code bases or building custom algorithms, you run into code that is parallelizable, but is not just a large DataFrame or array.

[Dask Delayed](https://docs.dask.org/en/stable/delayed.html) lets you wrap individual function calls into a lazily constructed task graph:

```python
# Enable a flexible parallel computing library for analytics
import dask


@dask.delayed
def inc(x):
   return x + 1


@dask.delayed
def add(x, y):
   return x + y


a = inc(1)  # No work has happened yet
b = inc(2)  # No work has happened yet
c = add(a, b)  # No work has happened yet

c = c.compute()  # This triggers all of the above computations
```

### Scheduling

After a task graph has been generated, it is the [scheduler's job to execute it](https://docs.dask.org/en/stable/scheduling.html).

By default, for the majority of Dask Application Programming Interfaces (APIs), when you call `.compute()` on a Dask object, Dask uses the thread pool on your computer (i.e., the *threaded scheduler*) to run computations in parallel. This is true for [Dask Array](https://docs.dask.org/en/stable/array.html), [Dask DataFrame](https://docs.dask.org/en/stable/dataframe.html), and [Dask Delayed](https://docs.dask.org/en/stable/delayed.html). The exception is [Dask Bag](https://docs.dask.org/en/stable/bag.html), which uses the *multiprocessing scheduler* by default.

If you want more control, use the *distributed scheduler* instead. The distributed scheduler works well on both single and multiple machines. It can be thought of as an advanced scheduler.

This is how you set up a cluster that only uses your own computer:

```python
# Enable entry point for dask.distributed
from dask.distributed import Client

client = Client()
```

Once you create a client, any computation will run on the cluster that it points to.

This is how you connect to a cluster that is already running:

```python
# Enable entry point for dask.distributed
from dask.distributed import Client

client = Client('ex_scheduler_url')
```

There are a [variety of ways](https://docs.dask.org/en/stable/deploying.html) to set up a remote cluster.

Clients are automatically closed when a Python session ends. A client can be manually closed by calling the [`.close()`](https://distributed.dask.org/en/stable/api.html#distributed.Client.close) method.

If a client was created using `Client()` (without arguments), this will close the local cluster, as well. Otherwise [`.shutdown()`](https://distributed.dask.org/en/latest/api.html#distributed.Client.shutdown) can be called, which shuts down the connected scheduler and workers.

### Diagnostics

When using a distributed cluster, Dask provides a [diagnostics dashboard](https://docs.dask.org/en/stable/dashboard.html) where you can see your tasks as they are processed. By default, the dashboard is served at <http://127.0.0.1:8787/status>.

The diagnostic dashboard requires [Bokeh](https://pypi.org/project/bokeh/).

## Deploying Dask Clusters

Dask works well at many scales, ranging from a single computers to clusters of many computers. There are many ways to deploy and run Dask. For example:

- [Python API](https://docs.dask.org/en/stable/deploying-python.html)
- [Cloud](https://docs.dask.org/en/stable/deploying-cloud.html)

### Python API

Dask can be run without any configuration. By default, it will use threads on your local machine:

```python
ddf = dd.read_csv(...)
ddf['x'].sum().compute()  # Uses threads on your local machine
```

Alternatively, you can set up a fully-featured multi-process Dask cluster on your local machine, which uses the *distributed scheduler*. This gives you access to multi-process computation and diagnostic dashboards:

```python
# Enable entry point for dask.distributed
from dask.distributed import Client

client = Client()

# Dask works as normal and leverages the infrastructure defined above
df['x'].sum().compute()
```

This sets up a scheduler in your local process, along with a number of workers and threads per worker related to the number of cores in your machine. Here, workers are started as processes and each worker process has its own threads.

If you want to run workers in your same process, you can pass the `processes=False` keyword argument. Here, workers are started as threads of the single Dask process that is running the scheduler and the workers.

```python
client = Client(processes=False)
```

This can be preferable if you want to avoid inter-worker communication and your computations release the [global interpreter lock (GIL)](https://docs.python.org/3/glossary.html#term-global-interpreter-lock). Primarily, this is common when using NumPy or Dask Array.

To *explicitly* create a local cluster and client, do:

```python
# Enable creating local Scheduler and Workers
from dask.distributed import LocalCluster

cluster = LocalCluster()
client = cluster.get_client()
```

Essentially, `Client()` is shorthand for creating a local cluster and then passing that to your client.

If you want to run workers in the same process, pass the `processes=False` keyword argument to `LocalCluster()`:

```python
# Enable creating local Scheduler and Workers
from dask.distributed import LocalCluster

cluster = LocalCluster(processes=False)
client = cluster.get_client()
```

To view cluster information, use the `.cluster` attribute:

```python
client.cluster
# LocalCluster(44181732, 'tcp://127.0.0.1:56440', workers=4, threads=12, memory=31.31 GiB)
```

The diagnostic dashboard can be accessed at <http://127.0.0.1:8787/status> (requires [Bokeh](https://pypi.org/project/bokeh/)).

The LocalCluster cluster manager defined above is easy to use and works well on a single machine. It follows the same interface as all other Dask cluster managers, so it is easy to swap out when you are ready to scale up.

```python
# You can swap out LocalCluster for other cluster types
from dask.distributed import LocalCluster
from dask_cloudprovider.aws import EC2Cluster

cluster = LocalCluster()
# cluster = EC2Cluster()

client = cluster.get_client()
```

There are [many `LocalCluster()`](https://distributed.dask.org/en/stable/api.html#distributed.LocalCluster) parameters available.

If you want to improve a deployment, there are [additional concepts](https://docs.dask.org/en/stable/deployment-considerations.html) to review and understand.

#### Cluster Manager Features

Instantiating a cluster manager class like `LocalCluster()` and then passing it to the `Client` is a common pattern. Cluster managers also provide useful utilities to help you understand what is going on.

For example, you can retrieve the Dashboard URL:

```python
cluster.dashboard_link
```

You can retrieve logs from cluster components:

```python
cluster.get_logs()
```

If you are using a cluster manager that supports scaling, you can modify the number of workers manually or automatically, based on workload:

```python
cluster.scale(10)  # Sets the number of workers to 10

# Allows the cluster to auto scale to 10 when tasks are computed
cluster.adapt(minimum=1, maximum=10)
```

### Cloud

There are a [variety of ways](https://cloudprovider.dask.org/en/latest/) to deploy Dask on the cloud. Cloud providers offer managed services, like virtual machines (VMs), Kubernetes, Yarn, or custom APIs with which Dask can easily connect.

Some common deployment options to consider are:

- Directly launching cloud resources, such as VMs or containers, via a cluster manager with [Dask Cloud Provider](https://cloudprovider.dask.org/en/latest/).
- A commercial Dask deployment option, like [Coiled](https://coiled.io/), to handle the creation and management of Dask clusters on AWS, GCP, and Azure.
- A managed Kubernetes service and Dask's [Kubernetes integration](https://docs.dask.org/en/stable/deploying-kubernetes.html).
- A managed Yarn service, like [Amazon EMR](https://aws.amazon.com/emr/), or [Google Cloud DataProc](https://cloud.google.com/dataproc/) and [Dask-Yarn](https://yarn.dask.org/).

#### Cloud Deployment Example (AWS)

In order to create clusters on Amazon Web Services (AWS), you need to set your access key, secret key, and region. The simplest way is to use the `aws` command line tool:

```console
$ python3 -m pip install awscli 'dask-cloudprovider[aws]' &&
    aws configure
```

In order for your Dask workers to be able to connect to other AWS resources (e.g., S3), they will need credentials. The best practice way of doing this is to pass an 
[Identity and Access Management (IAM) role](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles.html) to be used by workers. The `iam_instance_profile` parameter can be used for this purpose.

<https://cloudprovider.dask.org/en/latest/aws.html#elastic-compute-cloud-ec2>

Alternatively, you could read in your local credentials created with `aws configure` and pass them along as environment variables:

```python
# Enable a basic configuration language that provides a structure similar to
# what is found in Microsoft Windows INI files
import configparser
# Enable utilities for common tasks involving the with statement
import contextlib
# Enable a portable way of using operating system dependent functionality
import os

# Enable deploying a Dask cluster using EC2
from dask_cloudprovider.aws import EC2Cluster


def get_aws_credentials() -> dict:
    '''
    Get AWS credentials.

    Returns
    ----------------
    dict
        Dictionary of AWS credential information.
    '''
    parser = configparser.RawConfigParser()

    parser.read(os.path.expanduser('~/.aws/config'))
    config = parser.items('default')

    parser.read(os.path.expanduser('~/.aws/credentials'))
    credentials = parser.items('default')

    all_credentials = {
        key.upper(): value
        for key, value in [*config, *credentials]
    }

    with contextlib.suppress(KeyError):
        all_credentials['AWS_REGION'] = all_credentials.pop('REGION')

    return all_credentials


cluster = EC2Cluster(env_vars=get_aws_credentials())
```

- <https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html>
- <https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html>

If the cluster manager is terminated without being able to perform cleanup, the default behavior of `EC2Cluster` is for the scheduler and workers to time out. This will result in the host VMs shutting down. This cluster manager also creates instances with the terminate on shutdown setting so all resources should be automatically removed.

If you chose to override those settings and disable auto cleanup you, can destroy resources with the following CLI commands:

```console
$ export CLUSTER_ID="cluster id printed during creation" &&
    aws ec2 describe-instances \
        --filters "Name=tag:Dask Cluster,Values=${CLUSTER_ID}" \
        --query 'Reservations[*].Instances[*].[InstanceId]' \
        --output text |
    xargs aws ec2 terminate-instances --instance-ids
```

To enable Secure Shell (SSH) access for debugging purposes, create `EC2Cluster` like so:

```python
cluster = EC2Cluster(key_name='ex_key_name',
                     security_groups=['ex_security_group_id'])
```

You can list your existing key pair names with:

```console
$ aws ec2 describe-key-pairs \
    --query 'KeyPairs[*].KeyName' \
    --output text
```

You will need to ensure your security group allows access on port `22`.

Most cloud users will also want to access cloud-hosted data on their respective cloud provider. It is recommended to [install additional libraries](https://docs.dask.org/en/stable/how-to/connect-to-remote-data.html) for easy data access on your cloud provider.

```console
python3 -m pip install s3fs
```

## Configuration

Configuration is specified in one of the following ways:

- [YAML](https://yaml.org/) files in `~/.config/dask/` or `/etc/dask/`
- Environment variables, like `DASK_DISTRIBUTED__SCHEDULER__WORK_STEALING=True`
- Default settings within sub-libraries

### Access Configuration

Usually, Dask's configuration system is using the `dask.config.get()` method. You can use `.` for nested access. For example:

```python
# Enable a flexible parallel computing library for analytics
import dask
# Enable a lightweight library for distributed computing in Python
import dask.distributed

dask.config.get('distributed.client')
dask.config.get('distributed.scheduler.unknown-task-duration')
```

`dask.distributed` populates `config` with the distributed defaults.

The `dask.config.config` dictionary can be inspected to get a sense of the configuration that is being used by the current system.

Note that the `.get()` method treats underscores and hyphens identically. For example, `dask.config.get('temporary-directory')` is equivalent to `dask.config.get('temporary_directory')`.

Values like `128 MiB` and `10s` are parsed using the functions in [utilities](https://docs.dask.org/en/stable/api.html#api-utilities).

### Specify Configuration

#### YAML Files

You can specify configuration values in YAML files.

```yaml
array:
  chunk-size: 128 MiB

distributed:
  worker:
    memory:
      spill: 0.85  # Default: 0.7
      target: 0.75  # Default: 0.6
      terminate: 0.98  # Default: 0.95

  dashboard:
    # Locate the dashboard if working on a Jupyter Hub server
    link: /user/ex_user/proxy/8787/status
```

These files can live in any of the following locations:

1. The `~/.config/dask` directory
2. The `{sys.prefix}/etc/dask` directory, local to Python
3. The `{prefix}/etc/dask` directories with `{prefix}` in [`site.PREFIXES`](https://docs.python.org/3/library/site.html#site.PREFIXES)
4. The root directory (specified by the `DASK_ROOT_CONFIG` environment variable or `/etc/dask/`, by default)

Dask searches for *all* YAML files within each of these directories and merges them together, preferring configuration files closer to the user over system configuration files (preference follows the order in the list above). Also, users can specify a path with the `DASK_CONFIG` environment variable, which takes precedence at the top of the list above.

By using this approach, Dask sub-projects, like `dask-kubernetes` or `dask-ml`, are enabled to separately manage configuration files, while having the same global configuration.

#### Environment Variables

You can also specify configuration values with environment variables:

```console
$ export DASK_DISTRIBUTED__SCHEDULER__WORK_STEALING=True &&
    export DASK_DISTRIBUTED__SCHEDULER__ALLOWED_FAILURES=5 &&
    export DASK_DISTRIBUTED__DASHBOARD__LINK="/user/ex_user/proxy/8787/status"
```

The above commands result in configuration values like the following:

```json
{
  "distributed": {
    "scheduler": {
      "work-stealing": true,
      "allowed-failures": 5
    }
  }
}
```

Dask searches for all environment variables that start with `DASK_`, then transforms keys by converting to lower case and changing double-underscores to nested structures.

Dask tries to parse all values with [`ast.literal_eval`](https://docs.python.org/3/library/ast.html#ast.literal_eval), letting users pass numeric and boolean values, as well as lists, dictionaries, and so on with normal Python syntax.

Environment variables take precedence over configuration values found in YAML files.

#### Defaults

Individual sub-projects may add their own default values when they are imported. These are always added with lower priority than YAML files or environment variables.

```python
# Enable Dask configuration module
import dask.config
dask.config.config  # No configuration by default

# Enable a lightweight library for distributed computing in Python
import dask.distributed
dask.config.config  # New values have been added
```

## Best Practices

These recommendations focus on best practices that are shared among all of the Dask APIs.

There are API-specific best practices, as well:

- [Array Best Practices](https://docs.dask.org/en/stable/array-best-practices.html)
- [DataFrame Best Practices](https://docs.dask.org/en/stable/dataframe-best-practices.html)
- [Delayed Best Practices](https://docs.dask.org/en/stable/delayed-best-practices.html)

### Start Small

Parallelism brings extra complexity and overhead. Sometimes, it is necessary for larger problems, but often it is not. Before adding a parallel computing system like Dask to your workload, you may want to first try alternatives:

- **Use better algorithms or data structures** - NumPy, pandas, and scikit-learn may have faster functions for what you are trying to do. Examination the library documentation to try a find a better pre-built algorithm.
- **Better file formats** - Efficient binary formats that support random access can often help you manage larger-than-memory datasets efficiently and simply.
- **Compiled code** - Compiling your Python code with [Numba](https://numba.pydata.org/) or [Cython](https://cython.org/) might make parallelism unnecessary. Alternatively, you might use the multi-core parallelism already within those libraries.
- **Sampling** - Even if you have a lot of data, there might not be much advantage from using all of it. By intelligently sampling, you might be able to derive the same insight from a more manageable subset.
- **Profile** - If you are trying to speed up slow code, it is important to first understand *why* it is slow. Modest time investments in profiling your code can help you identify what is slowing it down. This information can help you make better decisions about if parallelism is likely to help, or if other approaches are likely to be more effective.

### Use the Dashboard

Dask's dashboard helps you to understand the state of your workers. This information can help to guide you to efficient solutions. In parallel and distributed computing, there are new costs to be aware of, and so your old intuition may no longer be true. Working with the dashboard can help you re-learn about what is fast and slow, and how to deal with it.

### Avoid Very Large Partitions

Chunks of data should be small enough so that many of them fit in a worker's available memory at once. Often, you control this when you select [partition size](https://docs.dask.org/en/stable/dataframe-design.html#dataframe-design-partitions) in Dask DataFrame or [chunk size](https://docs.dask.org/en/stable/array-chunks.html) in Dask Array.

Likely, Dask will manipulate as many chunks in parallel on one machine as you have cores on that machine. So, if you have 1 GB chunks and 10 cores, then Dask is likely to use *at least* 10 GB of memory. Additionally, it is common for Dask to have 2-3 times as many chunks available to work on, so that it always has something to work on.

### Avoid Very Large Graphs

Dask workloads are composed of *tasks*. A task is a Python function (e.g., `np.sum()` applied onto a Python object, like a pandas DataFrame or NumPy array). If you are working with Dask collections with many partitions, then every operation you do likely generates many tasks, at least as many as the number of partitions in your collection.

Every task comes with some overhead. Specifically, this is somewhere between 200 us and 1 ms. For a computation with thousands of tasks, this is fine. There will be about a second of overhead, and that may not trouble you.

However, when you have very large graphs with millions of tasks, this may become troublesome because overhead is now in the 10 minutes to hours range, and because the overhead of dealing with such a large graph can start to overwhelm the scheduler.

You can build smaller graphs by:

- **Increasing your chunk size** - If you have 1,000 GB of data and are using 10 MB chunks, then you have 100,000 partitions. Every operation on such a collection will generate at least 100,000 tasks. However, if you increase your chunk size to 1 GB or even a few GB, then you reduce the overhead by orders of magnitude. This requires that your workers have much more than 1 GB of memory, but that is typical for larger workloads.
- **Fusing operations together** - Dask will do a bit of this on its own, but you can help it. If you have a very complex operation with dozens of sub-operations, maybe you can pack them into a single Python function and use a function like [`da.map_blocks()`](https://docs.dask.org/en/stable/generated/dask.array.map_blocks.html) or [`dd.map_partitions()`](https://docs.dask.org/en/stable/generated/dask.dataframe.DataFrame.map_partitions.html).

    In general, the more administrative work you can move into your functions, the better. That way, the Dask scheduler does not need to think about all of the fine-grained operations.

- **Breaking up your computation** - For very large workloads, you may also want to try sending smaller chunks to Dask at a time. For example, if you are processing a petabyte of data, but find that Dask is only happy with 100 TB, maybe you can break up your computation into 10 pieces and submit them one after the other.

### Learn Techniques For Customization

The high-level Dask collections (array, DataFrame, bag) include common operations that follow standard Python APIs from NumPy and pandas. However, many workloads are complex and may require operations that are not included in these high-level APIs.

There are many options to support custom workloads:

- All collections have a `.map_partitions()` or `.map_blocks()` function that applies a user-provided function across every pandas DataFrame/NumPy array in the collection. Since Dask collections are made up of normal Python objects, it is often relatively easy to map custom functions across partitions of a data set without much modification.

        df.map_partitions(ex_custom_fn)

- Use more complex `.map_*()` functions. Sometimes, your custom behavior is not very parallel, but requires more advanced communication. For example, you may need to communicate a little bit of information from one partition to the next, or maybe you want to build a custom aggregation. Dask collections include methods for these, as well.
- For even more complex workloads, you can convert your collections into individual blocks and arrange those blocks as you like using Dask Delayed. Usually, there is a `.to_delayed()` method on every collection.

For more detailed information on customization, the following documentation may help:

- [`dask.array.blockwise()`](https://docs.dask.org/en/stable/generated/dask.array.blockwise.html#dask.array.blockwise)
- [`dask.array.map_blocks()`](https://docs.dask.org/en/stable/generated/dask.array.map_blocks.html#dask.array.map_blocks)
- [`dask.array.map_overlap()`](https://docs.dask.org/en/stable/generated/dask.array.map_overlap.html#dask.array.map_overlap)
- [`dask.array.reduction()`](https://docs.dask.org/en/stable/generated/dask.array.reduction.html#dask.array.reduction)
- [`dask.dataframe.groupby.Aggregation()`](https://docs.dask.org/en/stable/generated/dask.dataframe.groupby.Aggregation.html#dask.dataframe.groupby.Aggregation)
- [`dask.dataframe.map_overlap()`](https://docs.dask.org/en/stable/generated/dask.dataframe.map_overlap.html#dask.dataframe.map_overlap)
- [`dask.dataframe.map_partitions()`](https://docs.dask.org/en/stable/generated/dask.dataframe.map_partitions.html#dask.dataframe.map_partitions)

### Store Data Efficiently

As your ability to compute increases, you will likely find that data access and I/O take up a larger portion of your total time. Additionally, parallel computing will often add new constraints to how your store your data, particularly around providing random access to blocks of your data that are in-line with how you plan to compute on it.

For example:

- For compression, you may drop gzip and bz2 and use newer systems, like lz4, snappy, and Z-Standard, which provide better performance and random access.
- For storage formats, you may find that you want self-describing formats that are optimized for random access, metadata storage, and binary encoding, like Parquet, ORC, Zarr, HDF5, and GeoTIFF.
- When working on the cloud, you may find that some older formats may not work as well.
- You may want to partition or chunk your data in ways that align to common queries. In Dask DataFrame, this might mean choosing a column to sort by for fast selection and joins. For Dask Array, this might mean choosing chunk sizes that are aligned with your access patterns and algorithms.

### Processes and Threads

If you are doing mostly numeric work with NumPy, pandas, scikit-learn, Numba, and other libraries that release the GIL, then use mostly threads. If you are doing work on text data or Python collections/mappings, then use mostly processes.

If you are on powerful computers with a high thread count (greater than 10), then you should probably split things up into at least a few processes, regardless. Python can be highly productive with 10 threads per process with numeric work, but not 50 threads.

[Scheduling](https://docs.dask.org/en/stable/scheduling.html)

### Load Data With Dask

A common anti-pattern is creating large Python objects (e.g., a DataFrame or an Array on the client local machine) outside of Dask, and then embedding them into the computation. This means that Dask has to send those objects over the network multiple times, instead of just passing pointers to the data.

This incurs a lot of overhead and significantly slows down a computation, especially if the network connection between the client and the scheduler is slow. It can also overload the scheduler so that it fails with out-of-memory errors. Instead, you should use Dask methods to load the data and use Dask to control the results.

Embedding large objects like pandas DataFrames or Arrays into the computation is a frequent pain point for Dask users. It adds a significant delay until the scheduler has received and is able to start the computation and stresses the scheduler during the computation.

Using Dask to load these objects, instead, avoids these issues and significantly improves the performance of a computation.

### Avoid Repeatedly Calling `.compute()`

Calling `.compute()` will block the execution on the client until the Dask computation completes. Calling `.compute()` in a loop or sequentially on slightly different queries prohibits Dask from parallelizing different computations on the cluster and from sharing intermediate results between different queries.

```python
foo = ...
results = []

for i in range(...):
    results.append(foo.select(...).compute())
```

This holds execution every time that the iteration arrives at the `.compute()` call, computing one query at a time.

```python
foo = ...
results = []

for i in range(...):
    results.append(foo.select(...))  # No compute here

results = dask.compute(*results)
```

This allows Dask to compute the shared parts of the computation (like the `foo` object above) only once, rather than once per `.compute()` call. Also, it allows Dask to parallelize across the different selects, instead of running them sequentially.

## Dask DataFrame

Dask DataFrames coordinate many pandas DataFrames/Series arranged along the index. A Dask DataFrame is partitioned *row-wise*, grouping rows by index value for efficiency. These pandas objects may live on disk or on other computers.

As with all Dask collections, you trigger computation by calling the [`.compute()`](https://docs.dask.org/en/stable/generated/dask.dataframe.compute.html) method or persist data in distributed memory with the [`.persist()`](https://docs.dask.org/en/stable/generated/dask.dataframe.DataFrame.persist.html) method.

`.persist()` returns a copy for each of the Dask collections with their previously-lazy computations now submitted to run on the cluster. The task graphs of these collections now just point to the currently running [`Future`](https://docs.dask.org/en/stable/futures.html) objects.

So, if you persist a Dask DataFrame with 100 partitions, you get back a *Dask DataFrame* with 100 partitions, with each partition pointing to a `Future` currently running on the cluster.

`.compute()` returns a single `Future` for each collection. This `Future` refers to a single *Python object* result collected on one worker. Typically, this is used for small results. So, if you compute a `Dask.DataFrame` with 100 partitions, you get back a `Future` pointing to a single pandas DataFrame that holds all of the data.

Practically speaking, it [may be wise](https://stackoverflow.com/a/41807160) to use `.persist()` when your result is large and needs to be spread among many computers, and using `.compute()` when your result is small and you want it on just one computer.

You should probably stick with pandas when:

- Your data is small.
- Your computation is fast (subsecond).
- There are simpler ways to accelerate your computation, like avoiding `.apply()` or Python `for` loops and using built-in pandas methods instead.

### Load and Save Data

You can create a Dask DataFrame from various data storage formats, like CSV, HDF, Apache Parquet, and others. For most formats, this data can live on various storage systems, including disk, network file systems (NFS), the Hadoop Distributed File System (HDFS), Google Cloud Storage, and Amazon S3 (excepting HDF, which is only available on POSIX-like file systems).

#### API

The following functions provide access to convert between Dask DataFrames, file formats, and other Dask or Python collections.

File Formats:

- [`dask.dataframe.from_array()`](https://docs.dask.org/en/stable/generated/dask.dataframe.from_array.html#dask.dataframe.from_array) - Read any sliceable array into a Dask DataFrame.
- [`dask.dataframe.read_csv()`](https://docs.dask.org/en/stable/generated/dask.dataframe.read_csv.html#dask.dataframe.read_csv) - Read CSV files into a Dask.DataFrame.
- [`dask.dataframe.read_fwf()`](https://docs.dask.org/en/stable/generated/dask.dataframe.read_fwf.html#dask.dataframe.read_fwf) - Read fixed-width files into a Dask DataFrame.
- [`dask.dataframe.read_hdf()`](https://docs.dask.org/en/stable/generated/dask.dataframe.read_hdf.html#dask.dataframe.read_hdf) - Read HDF files into a Dask DataFrame.
- [`dask.dataframe.read_json()`](https://docs.dask.org/en/stable/generated/dask.dataframe.read_json.html#dask.dataframe.read_json) - Create a DataFrame from a set of JSON files.
- [`dask.dataframe.read_orc()`](https://docs.dask.org/en/stable/generated/dask.dataframe.read_orc.html#dask.dataframe.read_orc) - Read DataFrame from ORC file(s).
- [`dask.dataframe.read_parquet()`](https://docs.dask.org/en/stable/generated/dask.dataframe.read_parquet.html#dask.dataframe.read_parquet) - Read a Parquet file into a Dask DataFrame.
- [`dask.dataframe.read_sql()`](https://docs.dask.org/en/stable/generated/dask.dataframe.read_sql.html#dask.dataframe.read_sql) - Read SQL query or database table into a DataFrame.
- [`dask.dataframe.read_sql_query()`](https://docs.dask.org/en/stable/generated/dask.dataframe.read_sql_query.html#dask.dataframe.read_sql_query) - Read SQL query into a DataFrame.
- [`dask.dataframe.read_sql_table()`](https://docs.dask.org/en/stable/generated/dask.dataframe.read_sql_table.html#dask.dataframe.read_sql_table) - Read SQL database table into a DataFrame.
- [`dask.dataframe.read_table()`](https://docs.dask.org/en/stable/generated/dask.dataframe.read_table.html#dask.dataframe.read_table) - Read delimited files into a Dask DataFrame.
- [`dask.dataframe.to_csv()`](https://docs.dask.org/en/stable/generated/dask.dataframe.to_csv.html#dask.dataframe.to_csv) - Store Dask DataFrame to CSV files.
- [`dask.dataframe.to_hdf()`](https://docs.dask.org/en/stable/generated/dask.dataframe.to_hdf.html#dask.dataframe.to_hdf) - Store Dask DataFrame to Hierarchical Data Format (HDF) files.
- [`dask.dataframe.to_parquet()`](https://docs.dask.org/en/stable/generated/dask.dataframe.to_parquet.html#dask.dataframe.to_parquet) - Store Dask DataFrame to Parquet files.
- [`dask.dataframe.to_sql()`](https://docs.dask.org/en/stable/generated/dask.dataframe.to_sql.html#dask.dataframe.to_sql) - Store Dask DataFrame to a SQL table.

Dask Collections:

- [`dask.bag.Bag.to_dataframe()`](https://docs.dask.org/en/stable/generated/dask.bag.Bag.to_dataframe.html) - Create Dask DataFrame from a Dask Bag.
- [`dask.dataframe.DataFrame.to_bag()`](https://docs.dask.org/en/stable/generated/dask.dataframe.DataFrame.to_bag.html) - Create Dask Bag from a Dask DataFrame.
- [`dask.dataframe.DataFrame.to_delayed()`](https://docs.dask.org/en/stable/generated/dask.dataframe.DataFrame.to_delayed.html#dask.dataframe.DataFrame.to_delayed) - Convert into a list of `dask.delayed` objects, one per partition.
- [`dask.dataframe.from_dask_array()`](https://docs.dask.org/en/stable/generated/dask.dataframe.from_dask_array.html#dask.dataframe.from_dask_array) - Create a Dask DataFrame from a Dask Array.
- [`dask.dataframe.from_delayed()`](https://docs.dask.org/en/stable/generated/dask.dataframe.from_delayed.html#dask.dataframe.from_delayed) - Create Dask DataFrame from many Dask Delayed objects.
- [`dask.dataframe.from_map()`](https://docs.dask.org/en/stable/generated/dask.dataframe.from_map.html#dask.dataframe.from_map) - Create a DataFrame collection from a custom function map.
- [`dask.dataframe.to_records()`](https://docs.dask.org/en/stable/generated/dask.dataframe.to_records.html#dask.dataframe.to_records) - Create Dask Array from a Dask DataFrame.

pandas:

- [`dask.dataframe.DataFrame.from_dict()`](https://docs.dask.org/en/stable/generated/dask.dataframe.DataFrame.from_dict.html#dask.dataframe.DataFrame.from_dict) - Construct a Dask DataFrame from a Python dictionary.
- [`dask.dataframe.from_pandas()`](https://docs.dask.org/en/stable/generated/dask.dataframe.from_pandas.html#dask.dataframe.from_pandas) - Construct a Dask DataFrame from a pandas DataFrame.

Other file formats:

- [Bigquery](https://github.com/coiled/dask-bigquery)
- [Delta Lake](https://github.com/dask-contrib/dask-deltatable)
- [Snowflake](https://github.com/coiled/dask-snowflake)

#### Creating

##### Reading From CSV

You can use `.read_csv()` to read one or more CSV files into a Dask DataFrame. It supports loading multiple files at once using glob strings:

```python
ddf = dd.read_csv('ex_files.*.csv')
```

You can break up a single large file with the `blocksize` parameter:

```python
ddf = dd.read_csv('ex_large_file.csv', blocksize=25e6)  # 25MB chunks
```

Changing the `blocksize` parameter will change the number of partitions. A good rule-of-thumb when working with Dask DataFrames is to keep your partitions under 100 MB in size.

##### Read From Parquet

You can use `.read_parquet()` for reading one or more Parquet files. You can read in a single Parquet file or a directory of Parquet files:

```python
ddf = dd.read_parquet('/path/to/ex_data.parquet')

ddf = dd.read_parquet('/path/to/ex_parquet/')
```

[Dask DataFrame and Parquet](https://docs.dask.org/en/stable/dataframe-parquet.html)

##### Read From Cloud Storage

Dask can read from a variety of data stores, including cloud object stores. You can do this by prepending a protocol like `s3://` to paths used in common data access functions, like `.read_csv()`:

```python
ddf = dd.read_csv('s3://bucket/path/to/data_*.csv')
ddf = dd.read_parquet('gcs://bucket/path/to/data_*.parquet')
```

For remote systems like Amazon S3 or Google Cloud Storage, you may need to provide credentials. Usually, these are stored in a configuration file, but in some cases, you may want to pass storage-specific options through to the storage backend. You can do this with the `storage_options` parameter:

```python
ddf = dd.read_csv('s3://bucket_name/ex_data_*.csv',
                  storage_options={'anon': True})
ddf = dd.read_parquet('gs://dask_nyc_taxi/yellowtrip.parquet',
                      storage_options={'token': 'anon'})
```

- [Amazon S3](https://docs.dask.org/en/stable/how-to/connect-to-remote-data.html#connect-to-remote-data-s3)
- [Google Cloud Storage](https://docs.dask.org/en/stable/how-to/connect-to-remote-data.html#connect-to-remote-data-gc)

##### Mapping From a Function

For cases that are not covered by the above functions, but *can* be captured by a simple map operation, `.from_map()` is likely to be the most convenient means for DataFrame creation. For example, this API can be used to convert an arbitrary PyArrow `Dataset` object into a DataFrame collection by mapping fragments to DataFrame partitions:

```python
import pyarrow.dataset as ds

dataset = ds.dataset('hive_data_path', format='orc', partitioning='hive')
fragments = dataset.get_fragments()
func = lambda frag: frag.to_table().to_pandas()
ddf = dd.from_map(func, fragments)
```

##### Dask Delayed

[Dask Delayed](https://docs.dask.org/en/stable/delayed.html) is particularly useful when simple map operations are not sufficient to capture the complexity of your data layout. It lets you construct Dask DataFrames out of arbitrary Python function calls, which can be helpful to handle custom data formats or bake in particular logic around loading data.

[Working With Collections](https://docs.dask.org/en/stable/delayed-collections.html)

#### Storing

##### Writing Files Locally

You can save files locally, assuming each worker can access the same file system. The workers could be located on the same computer, or a network file system can be mounted and referenced at the same path location for every worker node.

[Local File System](https://docs.dask.org/en/stable/how-to/connect-to-remote-data.html#connect-to-remote-data-local)

##### Writing to Remote Locations

Dask can write to a variety of data stores, including cloud object stores. For example, you can write a `dask.dataframe` to an Azure storage blob:

```python
import pandas as pd  # Enable data structures and data analysis tools

data = {'col_1': [1, 2, 3, 4], 'col_2': [5, 6, 7, 8]}
ddf = dd.from_pandas(pd.DataFrame(data=data), npartitions=2)

dd.to_parquet(
    df=ddf,
    path='abfs://CONTAINER/FILE.parquet',
    storage_options={
        'account_name': 'ACCOUNT_NAME',
        'account_key': 'ACCOUNT_KEY'
    }
)
```

[Connect to Remote Data](https://docs.dask.org/en/stable/how-to/connect-to-remote-data.html)


### Internal Design

Dask DataFrames coordinate many pandas DataFrame/Series arranged along an index. A Dask DataFrame object is defined with the following components:

- A Dask graph with a special set of keys designating partitions (e.g., `('x', 0)`, `('x', 1)`).
- A name to identify which keys in the Dask graph refer to this DataFrame (e.g., `x`).
- An empty pandas object containing appropriate metadata (e.g., column names, dtypes).
- A sequence of partition boundaries along the index called `divisions`.

#### Metadata

Many DataFrame operations rely on knowing the name and dtype of columns. To keep track of this information, all Dask DataFrame objects have a `._meta` attribute, which contains an empty pandas object with the same dtypes and names.

```python
import pandas as pd  # Enable data structures and data analysis tools

df = pd.DataFrame({'a': [1, 2, 3], 'b': ['x', 'y', 'z']})
ddf = dd.from_pandas(df, npartitions=2)
ddf._meta
ddf._meta.dtypes
```

Internally, Dask DataFrame does its best to propagate this information through all operations. So, most of the time, a user should not have to worry about this. Usually, this is done by evaluating the operation on a small sample of fake data, which can be found on the `._meta_nonempty` attribute:

```python
ddf._meta_nonempty
```

Sometimes, this operation may fail in user-defined functions (e.g., when using `.apply()`) or may be prohibitively expensive. For these cases, many functions support an optional `meta` parameter, which allows specifying the metadata directly, avoiding the inference step. For convenience, this supports several options:

1. A pandas object with appropriate dtypes and names. If not empty, an empty slice will be taken:

        import pandas as pd  # Enable data structures and data analysis tools

        ddf.map_partitions(foo, meta=pd.DataFrame({'a': [1], 'b': [2]}))

2. A description of the appropriate names and dtypes. This can take several forms:

    - A `dict` of `{name: dtype}` or an iterable of `(name, dtype)` specifies a DataFrame. Note that order is important. The order of the names in `meta` should match the order of the columns.
    - A tuple of `(name, dtype)` specifies a series.

This parameter is available on all functions/methods that take user-provided callables (e.g., `.map_partitions()`, `.apply()`), as well as many creation functions (e.g., `.from_delayed()`).

#### Partitions

Internally, a Dask DataFrame is split into many partitions, where each partition is one pandas DataFrame. These DataFames are vertically split along the index. When the index is sorted and we know the values of the divisions of the partitions, then we can be efficient with expensive algorithms (e.g., groupbys, joins).

For example, if we have a time-series index, then the partitions might be divided by month: all of January will live in one partition, while all of February will live in the next. In these cases, operations like `.loc[]`, `.groupby()`, and `.join()/.merge()` along the index can be *much* more efficient than would otherwise be possible in parallel.

You can view the number of partitions and divisions of your DataFrame with the following attributes:

```python
ddf.npartitions
ddf.divisions
```

The number of partitions and the division values might change during optimization. The optimizer will try to create partitions with a sensible size to avoid straining the scheduler with many small partitions.

Divisions includes the minimum value of every partition's index and the maximum value of the last partition's index. In the example above, if the user searches for a specific datetime range, then we know which partitions we need to inspect and which we can drop:

```python
ddf.loc['2024-01-20': '2024-02-10']  # Must inspect first two partitions
```

Often, we do not have such information about our partitions. For example, when reading CSV files, we do not know (without extra user input) how the data is divided. In this case, `.divisions` will be all `None`.

In these cases, any operation that requires a cleanly partitioned DataFrame with known divisions will have to perform a sort. Generally, this can be achieved by calling [`.set_index()`](https://docs.dask.org/en/stable/generated/dask.dataframe.DataFrame.set_index.html).

#### `.groupby()`

By default, `.groupby()` will choose the number of output partitions based on a few different factors. It will look at the number of grouping keys to guess the cardinality of your data. It will use this information to calculate a factor based on the number of input partitions. You can override this behavior by specifying the number of output partitions using the `.split_out` parameter.

```python
result = ddf.groupby('id').value.mean()
result.npartitions  # Returns 1

result = ddf.groupby(['id', 'id_2']).value.mean()
result.npartitions  # Returns 5

result = ddf.groupby('id').value.mean(split_out=8)
result.npartitions  # Returns 8
```

Some groupby aggregation functions have a different `.split_out` default value. `split_out=True` will keep the number of partitions constant, which is useful for operations that do not reduce the number of rows much.

```python
result = ddf.groupby('id').value.nunique()
result.npartitions  # Returns same as ddf.npartitions
```

### Optimizer

#### Optimization Steps

Dask DataFrame will run several optimizations before executing the computation. These computations are aimed towards the efficiency of the query.

The optimizations include the following steps:

- **Projection Pushdown** - Select only the required columns in every step. This reduces the amount of data that needs to be read from storage, but also the amount of data that is processed along the way. Columns are dropped at the earliest stage in the query.
- **Filter Pushdown** - Push filters down as far as possible, potentially into the I/O step. Filters are executed in the earliest stage in the query.
- **Partition Pruning** - Partition selections are pushed down as far as possible, potentially into the I/O step.
- **Avoiding Shuffles** - Dask DataFrame will try to avoid shuffling data between workers as much as possible. This can be achieved if the column layout is already known, i.e., if the DataFrame was shuffled on the same column before. For example, executing a `ddf.groupby().apply()` after a merge operation will not shuffle the data again if the `.groupby()` happens on the merge columns.

    Similarly, performing two subsequent joins/merges on the same join column(s) will avoid shuffling the data again. The optimizer identifies that the partitioning of the DataFrame is already as expected and thus simplifies the operation to a single Shuffle and merge operation.

- **Automatically Resizing Partitions** - The I/O layers automatically adjust the partition count based on the column that is selected from the data set. Very small partitions negatively impact the scheduler and expensive operations (e.g., shuffling).

    This is addressed by automatically adjusting the partition count. For example, when selecting two columns that together have 40 MB per 200 MB file, the optimizer reduces the number of partitions by a factor of 5.

#### Exploring the Optimized Query

Dask will call `.optimize()` before executing the computation. This method applies the steps mentioned above and returns a new Dask DataFrame that represents the optimized query.

A rudimentary representation of the optimized query can be obtained by calling `.pprint()`. This will print the query plan in a human-readable format to the command line/console:

```python
import pandas as pd  # Enable data structures and data analysis tools

df = pd.DataFrame({'a': [1, 2, 3] * 5, 'b': [1, 2, 3] * 5})
ddf = dd.from_pandas(df, npartitions=2)
ddf = ddf.replace(1, 5)[['a']]

ddf.optimize().pprint()
```

A more advanced and easier to read representation can be obtained by calling `.explain()`. This method requires the [`graphviz`](https://pypi.org/project/graphviz/) and [crick](https://pypi.org/project/crick/) packages to be installed. The method will return a graph that represents the query plan and create an image from it.

```console
python3 -m pip install graphviz crick
```

```python
ddf.explain()
```

In both representations, `FromPandas` consumed the column projection, only selecting the column `a`.

The `.explain()` method is significantly easier to understand for more complex queries.

### Best Practices

#### Use pandas

For data that fits into RAM, pandas can often be faster and easier to use than Dask DataFrame. *Big Data* tools are almost always worse than normal tools while the latter remain appropriate.

#### Reduce, and Then Use pandas

Even if you have a large data set, there may be a point in your computation where you have reduced things to a more manageable level. You may want to switch to pandas at this point.

```python
ddf = dd.read_parquet('ex_giant_file.parquet')
ddf = ddf.loc[ddf['name'] == 'Kiki']  # Select a subsection
result = ddf.groupby('id').value.mean()  # Reduce to a smaller size
result = result.compute()  # Convert to pandas dataframe
result...  # Continue working with pandas
```

#### pandas Performance Tips Apply to Dask DataFrame

The usual pandas performance tips (e.g., avoiding `.apply()`, using vectorized operations, using categoricals), all apply to Dask DataFrame.

#### Use the Index

Dask DataFrame can be optionally sorted along a single index column. Some operations against this column can be very fast. For example, if your data set is sorted by time, you can quickly select data for a particular day or perform time series joins.

You can check if your data is sorted by looking at the `.known_divisions` attribute. You can set an index column using the `.set_index()` method. This operation is expensive, so use it sparingly:

```python
ddf = ddf.set_index('timestamp')  # Set the index to make some operations fast

ddf.loc['2023-01-05':'2023-01-12']  # This is very fast if you have an index
ddf.merge(ddf_2, left_index=True, right_index=True)  # This is also very fast
```

[DataFrame Partitions](https://docs.dask.org/en/stable/dataframe-design.html#dataframe-design-partitions)

#### Avoid Full-Data Shuffling

Setting an index is an important, but expensive operation. You should do it infrequently and you should *persist* afterwards.

Some operations (e.g., `.set_index()` and `.merge()`/`.join()`) are harder to do in a parallel or distributed setting than if they are in-memory on a single machine. In particular, *shuffling operations* that rearrange data become much more communication intensive.

For example, if your data is arranged by customer ID, but now you want to arrange it by time, all of your partitions will have to talk to each other to exchange shards of data. This can be an intensive process, particularly on a cluster.

So, definitely set the index, but do so infrequently.

```python
ddf = ddf.set_index('ex_col_name')
```

Additionally, `.set_index()` has a few options that can accelerate it in some situations. For example, if you know that your data set is sorted or you already know the values by which it is divided, you can provide these to accelerate the [`.set_index()`](https://docs.dask.org/en/latest/generated/dask_expr._collection.DataFrame.set_index.html#dask_expr._collection.DataFrame.set_index) operation.

```python
ddf_2 = ddf.set_index(df['timestamp'], sorted=True)
```

#### Intelligently Persist

If using Dask on distributed systems, the following section may be helpful.

`.persist()` has a number of drawbacks with the query optimizer. It will block all optimizations and prevent us from pushing column projections or filters into the I/O layer. Use `.persist()` sparingly, only when absolutely necessary, or when you need the full data set afterwards.

Normally, DataFrame workloads look like the following:

1. Load data from files
2. Filter data to a particular subset
3. Shuffle data to set an intelligent index
4. Run several complex queries on top of the indexed data

Often, it is ideal to load, filter, and shuffle data once and keep this result in memory. Afterwards, each of the several complex queries can be based off of this in-memory data, rather than have to repeat the full load-filter-shuffle process each time. To do this, use the `.persist()` method:

```python
ddf = dd.read_csv('s3://bucket/path/to/*.csv')
ddf = ddf[ddf['balance'] < 0]
ddf = client.persist(ddf)

ddf = ddf.set_index('timestamp')
ddf = client.persist(ddf)

ddf['customer_id'].nunique().compute()

ddf.groupby(df['city']).size().compute()
```

`.persist()` is important because Dask DataFrame is *lazy by default*. It is a way of telling the cluster that is should start executing the computations that you have defined so far, and that it should try to keep those results in memory. You will get back a new DataFrame that is semantically equivalent to your old DataFrame, but now points to running data. Your old DataFrame still points to lazy computations:

```python
# Do not do this
client.persist(ddf)  # Persist does not change the input in-place

# Do this instead
ddf = client.persist(ddf)  # Replace your old lazy DataFrame
```

#### Repartition to Reduce Overhead

A Dask DataFrame is split up into many pandas DataFrames. Sometimes, we call these *partitions*, and often the number of partitions is decided for you. For example, it might be the number of CSV files from which you are reading.

However, over time, as you reduce or increase the size of your pandas DataFrames by filtering or joining, it may be wise to reconsider how many partitions you need. There is a cost to having too many or having too few.

Partitions should comfortably fit in memory (smaller than a gigabyte), but also not be too many. Every operation on every partition takes the central scheduler a few hundred microseconds to process. If you have a few thousand tasks, this is barely noticeable, but it is preferable to reduce the number, if possible.

A common situation is that you load lots of data into reasonably sized partitions (Dask's defaults make decent choices), but then you filter down your data set to only a small fraction of the original. At this point, it is wise to regroup your many small partitions into a few larger ones. You can do this by using the [`dask.dataframe.DataFrame.repartition()`](https://docs.dask.org/en/stable/generated/dask.dataframe.DataFrame.repartition.html#dask.dataframe.DataFrame.repartition) method:

```python
ddf = dd.read_csv('s3://bucket/path/to/*.csv')
ddf = ddf[df['name'] == 'Kiki']  # Only 1/100th of the data
ddf = ddf.repartition(npartitions=ddf.npartitions // 100)

ddf = ddf.persist()  # If on a distributed system
```

This helps to reduce overhead and increase the effectiveness of vectorized pandas operations. You should aim for partitions that have around 100 MB of data each.

#### Joins

Joining two DataFrames can be either very expensive or very cheap, depending on the situation. It is cheap in the following cases:

- Joining a Dask DataFrame with a pandas DataFrame
- Joining a Dask DataFrame with another Dask DataFrame of a single partition
- Joining Dask DataFrames along their indexes

It expensive in the following case:

- Joining Dask DataFrames along columns that are not their index

The expensive case requires a shuffle. This is fine, and Dask DataFrame will complete the job well, but it will be more expensive than a typical linear-time operation:

```python
dd.merge(ddf, df)  # Fast
dd.merge(ddf, ddf_2, left_index=True, right_index=True)  # Fast
dd.merge(ddf, ddf_2, left_index=True, right_on='id')  # Half-fast, half-slow
dd.merge(ddf, ddf_2, left_on='id', right_on='id')  # Slow
```

#### User Parquet

[Apache Parquet](https://parquet.apache.org/) is a columnar binary format. It is the de-facto standard for the storage of large volumes of tabular data and the recommended storage solution for basic tabular data.

```python
ddf.to_parquet('path/to/ex_results/')
ddf = dd.read_parquet('path/to/ex_results/')
```

When compared to formats like CSV, Parquet brings the following advantages:

- It is faster to read and write, often by 4-10x.
- It is more compact to store, often by 2-5x.
- It has a schema, so there is no ambiguity about what types the columns are. This avoids confusing errors.
- It supports more advanced data types, like categoricals, proper datetimes, and more.
- It is more portable, and can be used with other systems, like databases or Apache Spark.
- Depending on how the data is partitioned, Dask can identify sorted columns, and sometimes pick out subsets of data more efficiently.

[Dask DataFrame and Parquet](https://docs.dask.org/en/stable/dataframe-parquet.html#dataframe-parquet)

## Dask Delayed

Sometimes, problems do not fit into one of the collections like `dask.array` or `dask.dataframe`. In these cases, users can parallelize custom algorithms using the simpler `dask.delayed` interface. This allows you to directly create graphs with a light annotation of normal Python code:

```python
# Enable a flexible parallel computing library for analytics
import dask

x = dask.delayed(inc)(1)
y = dask.delayed(inc)(2)
z = dask.delayed(add)(x, y)

z.compute()
z.visualize()
```

Consider the following:

```python
def inc(x):
    return x + 1


def double(x):
    return x * 2


def add(x, y):
    return x + y


data = [1, 2, 3, 4, 5]

output = []
for x in data:
    a = inc(x)
    b = double(x)
    c = add(a, b)
    output.append(c)

total = sum(output)
```

There is parallelism in this problem (e.g., many of the `inc()`, `double()`, and `add()` functions can be independently evaluated), but it is not clear how to convert this to an array or DataFrame computation. As written, this code sequentially runs in a single thread. However, we see that a lot of this could be executed in parallel.

The Dask [`delayed()`](https://docs.dask.org/en/stable/delayed-api.html#dask.delayed.delayed) function decorates your functions so that they operate *lazily*. Rather than executing your function immediately, it will defer execution, placing the function and its arguments into a task graph.

We slightly modify our code by wrapping functions in `delayed()`. This delays the execution of the function and generates a Dask graph, instead:

```python
# Enable a flexible parallel computing library for analytics
import dask

output = []
for x in data:
    a = dask.delayed(inc)(x)
    b = dask.delayed(double)(x)
    c = dask.delayed(add)(a, b)
    output.append(c)

total = dask.delayed(sum)(output)
```

We used the `dask.delayed()` function to wrap the function calls that we want to turn into tasks. None of the `inc()`, `double()`, `add()`, or `sum()` calls have happened yet. Instead, the object `total` is a `Delayed` result that contains a task graph of the entire computation.

From the graph, we can see clear opportunities for parallel execution. The [Dask schedulers](https://docs.dask.org/en/stable/scheduling.html) will exploit this parallelism, generally improving performance (but not in this example, because these functions are already small and fast).

```python
total.visualize()
```

Now, we can compute this lazy result to execute the graph in parallel:

```python
total.compute()
```

### Decorator

It is also common to see the delayed function used as a [decorator](https://docs.python.org/3/glossary.html#term-decorator). Here is a reproduction of the original problem as parallel code:

```python
# Enable a flexible parallel computing library for analytics
import dask


@dask.delayed
def inc(x):
    return x + 1


@dask.delayed
def double(x):
    return x * 2


@dask.delayed
def add(x, y):
    return x + y


data = [1, 2, 3, 4, 5]

output = []
for x in data:
    a = inc(x)
    b = double(x)
    c = add(a, b)
    output.append(c)

total = dask.delayed(sum)(output)
```

### Real Time

Sometimes, you may want to create and destroy work during execution or launch tasks from other tasks. For this, see the [Futures](https://docs.dask.org/en/stable/futures.html) interface.

### Best Practices

It is easy to get started with Dask Delayed, but [using it well ](https://docs.dask.org/en/stable/delayed-best-practices.html)does require some experience.

#### Call `delayed()` On the Function, Not the Result

Dask Delayed operates on functions like `dask.delayed(f)(x, y)`, not on their results like `dask.delayed(f(x, y))`. When you do the latter, Python first calculates `f(x, y)` before Dask has a chance to step in:

```python
# Do not do this
dask.delayed(f(x, y))  # This executes immediately
```

```python
# Do this
dask.delayed(f)(x, y)  # This makes a delayed function, acting lazily
```

#### Compute On Lots of Computation at Once

To improve parallelism, you want to include lots of computation in each compute call. Ideally, you want to make many `dask.delayed()` calls to define your computation and then call `dask.compute()` only at the end.

It is OK to call `dask.compute()` in the middle of your computation, as well, but everything will stop there as Dask computes those results before moving forward with your code.

```python
# Do not do this

# Avoid calling compute repeatedly
results = []
for x in L:
    y = dask.delayed(f)(x)
    results.append(y.compute())

results
```

```python
# Do this

# Collect many calls for one compute
results = []
for x in L:
    y = dask.delayed(f)(x)
    results.append(y)

results = dask.compute(*results)
```

Calling `y.compute()` within the loop would await the result of the computation every time, and so inhibit parallelism.

#### Do Not Mutate Inputs

Your functions should not directly change the inputs:

```python
# Do not do this

# Mutate inputs in functions
@dask.delayed
def f(x):
    x += 1
    return x
```

```python
# Do this

# Return new values or copies
@dask.delayed
def f(x):
    x = x + 1
    return x
```

If you need to use a mutable operation, then make a copy within your function first:

```python
@dask.delayed
def f(x):
    x = copy(x)
    x += 1
    return x
```

#### Avoid Global State

Ideally, your operations should not rely on global state. Using global state *might* work if you only use threads, but when you move to multiprocessing or distributed computing, then you will likely encounter confusing errors.

```python
# Do not do this

# This references global variable L
@dask.delayed
def f(x):
    L.append(x)


L = []
```

#### Do Not Rely On Side Effects

Delayed functions only do something if they are computed. You will always need to pass the output to something that eventually calls compute:

```python
# Do not do this

# Forget to call compute
dask.delayed(f)(1, 2, 3)

...
```

```python
# Do this

# Ensure delayed tasks are computed
x = dask.delayed(f)(1, 2, 3)

...
dask.compute(x, ...)
```

#### Break Up Computations Into Many Pieces

Every `dask.delayed()` function call is a single operation from Dask's perspective. You achieve parallelism by having many delayed calls, not by using only a single one.

Dask will not look inside a function decorated with `@dask.delayed` and parallelize that code internally. To accomplish that, it needs your help to find good places to break up a computation.

```python
# Do not do this

# One giant task
def load(filename):
    ...


def process(data):
    ...


def save(data):
    ...


@dask.delayed
def f(filenames):
    results = []
    for filename in filenames:
        data = load(filename)
        data = process(data)
        result = save(data)
        results.append(result)

    return results


dask.compute(f(filenames))
```

```python
# Do this


# Break up into many tasks
@dask.delayed
def load(filename):
    ...


@dask.delayed
def process(data):
    ...


@dask.delayed
def save(data):
    ...


def f(filenames):
    results = []
    for filename in filenames:
        data = load(filename)
        data = process(data)
        result = save(data)
        results.append(result)

    return results


dask.compute(f(filenames))
```

The first version only has one delayed task, and so cannot parallelize.

#### Avoid Too Many Tasks

Every delayed task has an overhead of a few hundred microseconds. Usually, this is OK, but it can become a problem if you apply `dask.delayed()` too finely. In this case, it is often best to break up your many tasks into batches or use one of the Dask collections to help you:

```python
# Do not do this

# Too many tasks
results = []
for x in range(10000000):
    y = dask.delayed(f)(x)
    results.append(y)
```

```python
# Do this

# Use collections
b = db.from_sequence(range(10000000), npartitions=1000)
b = b.map(f)
...
```

Here, we use `dask.bag` to automatically batch apply our function. We also could have constructed our own batching as follows:

```python
def batch(seq):
    sub_results = []
    for x in seq:
        sub_results.append(f(x))
    return sub_results


batches = []
for i in range(0, 10000000, 10000):
    result_batch = dask.delayed(batch)(range(i, i + 10000))
    batches.append(result_batch)
```

Here, we construct batches where each delayed function call computes for many data points from the original input.

#### Avoid Calling `dask.delayed()` Within Delayed Functions

Often, if you are new to using Dask Delayed, you place `dask.delayed()` calls everywhere and hope for the best. While this may actually work, it is usually slow and results in hard-to-understand solutions.

Usually, you never call `dask.delayed()` within `dask.delayed()` functions:

```python
# Do not do this

# Delayed function calls delayed
@dask.delayed
def process_all(L):
    result = []
    for x in L:
        y = dask.delayed(f)(x)
        result.append(y)
    return result
```

```python
# Do this

# Normal function calls delayed
def process_all(L):
    result = []
    for x in L:
        y = dask.delayed(f)(x)
        result.append(y)
    return result
```

Since the normal function only does delayed work, it is very fast, and so there is no reason to delay it.

#### Do Not Call `dask.delayed()` On Other Dask Collections

When you place a Dask array or Dask DataFrame into a delayed call, that function will receive the NumPy or pandas equivalent. Beware that if your array is large, then this might crash your workers.

Instead, it is more common to use methods like `da.map_blocks()`:

```python
# Do not do this

# Call delayed functions on Dask collections
df = dd.read_csv('/path/to/*.csv')

dask.delayed(train)(df)
```

```python
# Do this

# Use mapping methods if applicable
df = dd.read_csv('/path/to/*.csv')

df.map_partitions(train)
```

Alternatively, if the procedure does not fit into a mapping, you can always turn your arrays or DataFrames into *many* delayed objects:

```python
partitions = df.to_delayed()
delayed_values = [dask.delayed(train)(part) for part in partitions]
```

However, if you do not mind turning your Dask array/DataFrame into a single chunk, then this is OK:

```python
dask.delayed(train)(..., y=df.sum())
```

#### Avoid Repeatedly Putting Large Inputs Into Delayed Calls

Every time you pass a concrete result (anything that is not delayed), Dask will hash it by default to give it a name. This is fairly fast (around 500 MB/s), but can be slow if you do it over and over again. Instead, it is better to delay your data, as well.

This is especially important when using a distributed cluster to avoid sending your data separately for each function call.

```python
# Do not do this

x = np.array(...)  # Some large array

results = [dask.delayed(train)(x, i)
           for i in range(1000)]
```

```python
# Do this

x = np.array(...)  # Some large array
x = dask.delayed(x)  # Delay the data once
results = [dask.delayed(train)(x, i) for i in range(1000)]
```

Every call to `dask.delayed(train)(x, ...)` has to hash the NumPy array `x`, which slows things down.

### Indirect Dependencies

Sometimes, you amy want to add a dependency to a task that does not take the result of that dependency as an input. For example, when a task depends on the side-effect of another task. In these cases, you can use `dask.graph_manipulation.bind()`.

```python
# Enable a flexible parallel computing library for analytics
import dask
# Enable making children collections dependent on parents collections
from dask.graph_manipulation import bind


@dask.delayed
def inc(x):
    return x + 1


@dask.delayed
def add_data(x):
    DATA.append(x)


@dask.delayed
def sum_data(x):
    return sum(DATA) + x


DATA = []

a = inc(1)
b = add_data(a)
c = inc(3)
d = add_data(c)
e = inc(5)
f = bind(sum_data, [b, d])(e)

f.compute()
```

`sum_data()` will operate on `DATA` only after both the expected items have been appended to it. `bind()` can also be used along with direct dependencies passed through the function arguments.

### Execution

By default, Dask Delayed uses the threaded scheduler in order to avoid data transfer costs. You should consider using the [multi-processing](https://docs.dask.org/en/stable/scheduling.html#local-processes) sor [`dask.distributed`](https://distributed.dask.org/en/latest/) scheduler on a local machine, or on a cluster, if your code does not [release the GIL well](https://docs.dask.org/en/stable/graphs.html#avoid-holding-the-gil) (i.e., computations that are dominated by pure Python code, or computations wrapping external code and holding onto it).

### Working With Collections

Often, we want to do a bit of custom work with `dask.delayed` (e.g., for complex data ingest), then leverage the algorithms in `dask.array` or `dask.dataframe`, and then switch back to custom work. To this end, all collections support `from_delayed()` functions and `.to_delayed()` methods.

For example, consider the case where we store tabular data in a custom format not known by Dask DataFrame. This format is naturally broken apart into pieces and we have a function that reads one piece into a pandas DataFrame.

We use `dask.delayed()` to lazily read these files into pandas DataFrames, use `dd.from_delayed()` to wrap these pieces up into a single Dask DataFrame, use the complex algorithms within the DataFrame (e.g., `.groupby()`, `.join()`), and then switch back to `dask.delayed` to save our results back to the custom format:

```python
import dask.dataframe as dd  # Enable parallel pandas DataFrame
# Enable wrapping a function or object to produce a Delayed
from dask.delayed import delayed

from ex_custom_lib import load, save

filenames = ...
dfs = [delayed(load)(fn) for fn in filenames]

df = dd.from_delayed(dfs)
df = ...  # Do work with dask.dataframe

dfs = df.to_delayed()
writes = [delayed(save)(df, fn) for df, fn in zip(dfs, filenames)]

dd.compute(*writes)
```

`dask.delayed` provides a release valve for users to manage this complexity on their own and solve the last mile problem for custom formats and complex situations.

## Futures

Dask supports a real-time task framework that extends Python's [`concurrent.futures`](https://docs.python.org/3/library/concurrent.futures.html) interface. Dask futures allow you to scale generic Python workflows across a Dask cluster with minimal code changes.

This interface is good for arbitrary task scheduling (e.g., [`dask.delayed()`](https://docs.dask.org/en/stable/delayed.html)), but is eager rather than lazy, which provides more flexibility in situations where the computations may evolve over time. These features depend on the second generation task scheduler found in [`dask.distributed()`](https://distributed.dask.org/en/latest) (which, despite its name, runs well on a single machine).

### Start Dask Client

You must start a `Client` to use the futures interface. This tracks state among the various worker processes or threads.

```python
# Enable creating local Scheduler and Workers
from dask.distributed import LocalCluster

cluster = LocalCluster()  # Start local workers as processes
client = cluster.get_client()

# Or

cluster = LocalCluster(processes=False)  # Start local workers as threads
client = cluster.get_client()
```

If you have [Bokeh](https://pypi.org/project/bokeh/) installed, then this starts up a diagnostic dashboard at <http://127.0.0.1:8787>.

You can submit individual tasks using the `.submit()` method:

```python
def inc(x):
    return x + 1


def add(x, y):
    return x + y


a = client.submit(inc, 10)  # Calls inc(10) in background thread or process
b = client.submit(inc, 20)  # Calls inc(20) in background thread or process
```

The `.submit()` method returns a `Future`, which refers to a remote result. This result may not yet be completed:

```python
a
# <Future: status: pending, key: inc-b8aaf26b99466a7a1980efa1ade6701d>
```

Eventually, it will complete. The result stays in the remote thread/process/worker until you explicitly ask for it back:

```python
a
# <Future: status: finished, type: int, key: inc-b8aaf26b99466a7a1980efa1ade6701d>

a.result()  # Blocks until task completes and data arrives
# 11
```

You can pass `Future`s as inputs to `.submit()`. Dask automatically handles dependency tracking. Once all input `Future`s have completed, they will be moved onto a single worker (if necessary), and then the computation that depends on them will be started.

You do not need to wait for inputs to finish before submitting a new task. Dask will automatically handle this:

```python
c = client.submit(add, a, b)  # Calls add on the results of a and b
```

Similar to Python's `.map()`, you can use `Client.map()` to call the same function and many inputs:

```python
futures = client.map(inc, range(1000))
```

Note that each task comes with about 1ms of overhead. If you want to map a function over a large number of inputs, you might consider [`dask.bag`](https://docs.dask.org/en/stable/bag.html) or [`dask.dataframe`](https://docs.dask.org/en/stable/dataframe.html), instead.

### Move Data

Given any future, you can call the `.result()` method to gather the result. This will block until the future is done computing and then transfer the result back to your local process, if necessary:

```python
c.result()
# 32
```
You can gather many results concurrently using the `Client.gather()` method, This can be more efficient than calling `.result()` on each future sequentially:

```python
# results = [future.result() for future in futures]
results = client.gather(futures)  # This can be faster
```

If you have important local data that you want to include in your computation, you can either include it as a normal input to a `.submit()` or `.map()` call:

```python
import pandas as pd  # Enable data structures and data analysis tools

df = pd.read_csv('training-data.csv')
future = client.submit(ex_fn, df)
```

Alternatively, you can `.scatter()` it explicitly. Scattering moves your data to a worker and returns a future pointing to that data:

```python
remote_df = client.scatter(df)

remote_df
# <Future: status: finished, type: DataFrame, key: bbd0ca93589c56ea14af49cba470006e>

future = client.submit(ex_fn, remote_df)
```

Both of these accomplish the same result, but using `.scatter()` can sometimes be faster. This is especially true if you use processes of distributed workers (where data transfer is necessary) and you want to use `df` in many computations. Scattering the data beforehand avoids excessive data movement.

Calling scatter on a list scatters all elements individually. Dask will spread these elements evenly throughout workers in a round-robin fashion:

```python
client.scatter([1, 2, 3])
# [<Future: status: finished, type: int, key: c0a8a20f903a4915b94db8de3ea63195>,
#  <Future: status: finished, type: int, key: 58e78e1b34eb49a68c65b54815d1b158>,
#  <Future: status: finished, type: int, key: d3395e15f605bc35ab1bac6341a285e2>]
```

Dask will only compute and hold onto results for which there are active futures. In this way, your local variables define what is active in Dask. When a future is garbage collected by your local Python session, Dask will feel free to delete that data or stop ongoing computations that were trying to produce it:

```python
del future  # Deletes remote data once future is garbage collected
```

You can also explicitly cancel a task using the `Future.cancel()` or `Client.cancel()` methods:

```python
future.cancel()  # Deletes data, even if other futures point to it
```

If a future fails, then Dask will raise the remote exceptions and tracebacks if you try to get the result:

```python
a = client.submit(div, 1, 0)  # 1 / 0 raises a ZeroDivisionError

a
# <Future: status: error, key: div-3601743182196fb56339e584a2bf1039>

a.result()
# ZeroDivisionError: division by zero
```

All futures that depend on an erred future also err with the same exception:

```python
b = client.submit(inc, a)
b
# <Future: status: error, key: inc-15e2e4450a0227fa38ede4d6b1a952db>
```

You can collect the exception or traceback explicitly with the `Future.exception()` or `Future.traceback()` methods.

### Waiting On Futures

You can wait on a future or collection of futures using the `wait()` function:

```python
# Enable waiting until all/any futures are finished
from dask.distributed import wait

wait(futures)
```

This blocks until all futures are finished or have erred.

You can also iterate over the futures as they complete using the `as_completed()` function:

```python
# Enable returning futures in the order in which they complete
from dask.distributed import as_completed

futures = client.map(score, x_values)

best = -1
for future in as_completed(futures):
    y = future.result()
    if y > best:
        best = y
```

For greater efficiency, you can also ask `as_completed()` to gather the results in the background:

```python
for future, result in as_completed(futures, with_results=True):
    # y = future.result()  # Do not need this
    ...
```

Alternatively, you can collect all futures in batches that have arrived since the last iteration:

```python
for batch in as_completed(futures, with_results=True).batches():
    for future, result in batch:
        ...
```

Additionally, for iterative algorithms, you can add more futures into the `as_completed()` iterator *during* iteration:

```python
seq = as_completed(futures)

for future in seq:
    y = future.result()
    if condition(y):
        new_future = client.submit(...)
        seq.add(new_future)  # Add back into the loop
```

If you want to add multiple futures at once, use `seq.update(futures)` instead.

### Fire and Forget

Sometimes, we do not care about gathering the result of a task, and only care about side effects that it might have, like writing a result to a file:

```python
a = client.submit(load, filename)
b = client.submit(process, a)
c = client.submit(write, b, out_filename)
```

As noted above, Dask will stop work that does not have any active futures. It thinks that because no one has a pointer to this data that no one cares. You can tell Dask to compute a task anyway, even if there are no active futures, using the `fire_and_forget()` function:

```python
# Enable running tasks at least once, even if we release the futures
from dask.distributed import fire_and_forget

fire_and_forget(c)
```

This is particularly useful when a future may go out of scope, e.g., as part of a function:

```python
def process(filename):
    out_filename = 'out-' + filename

    a = client.submit(load, filename)
    b = client.submit(process, a)
    c = client.submit(write, b, out_filename)

    fire_and_forget(c)
    # Here, we lose the reference to c, but that is now OK


for filename in filenames:
    process(filename)
```

### Submit Task and Retrieve Results From a Different Process

Sometimes, we care about retrieving a result, but not necessarily from the same process:

```python
# Enable allowing multiple clients to share futures and data
# between each other with a single mutable variable
from distributed import Variable

var = Variable('ex_result')
fut = client.submit(...)
var.set(fut)
```

Using a `Variable` instructs Dask to remember the result of this task under the given name, so that it can be later retrieved without having to keep the `Client` alive in the meantime:

```python
var = Variable('ex_result')
fut = var.get()
result = fut.result()
```

### Submit Tasks From Tasks

*This is an advanced feature and is rarely necessary in the common case.*

Tasks can launch other tasks by getting their own client. This enables complex and highly dynamic workloads:

```python
# Enable getting a client while within a task
from dask.distributed import get_client


def ex_fn(x):
    ...

    # Get locally created client
    client = get_client()

    # Do normal client operations, asking cluster for computation
    a = client.submit(...)
    b = client.submit(...)
    a, b = client.gather([a, b])

    return a + b
```

It also allows you to set up long running tasks that watch other resources, like sockets or physical sensors:

```python
def monitor(device):
   client = get_client()
   while True:
       data = device.read_data()
       future = client.submit(process, data)
       fire_and_forget(future)


for device in devices:
    fire_and_forget(client.submit(monitor))
```

However, each running task takes up a single thread, and so if you launch many tasks that launch other tasks, then it is possible to deadlock the system (if you are not careful).

You can call the `secede()` function from within a task to have it remove itself from the dedicated thread pool into an administrative thread that does not take up a slot within the Dask worker:

```python
# Enable getting a client while within a task and
# seceding a task from the worker's thread pool
from dask.distributed import get_client, secede


def monitor(device):
    client = get_client()
    secede()  # Remove this task from the thread pool
    while True:
        data = device.read_data()
        future = client.submit(process, data)
        fire_and_forget(future)
```

If you intend to do more work in the same thread after waiting on client work, you may want to explicitly block until the thread is able to *rejoin* the thread pool. This allows some control over the number of threads that are created and stops too many threads from being active at once, over-saturating your hardware:

```python
def f(n):  # Assume that this runs as a task
   client = get_client()

   secede()  # Secede while we wait for results to come back
   futures = client.map(func, range(n))
   results = client.gather(futures)

   rejoin()  # Block until a slot is open in the thread pool
   result = analyze(results)

   return result
```

Alternatively, you can just use the normal `compute()` function *within* a task. This will automatically call `secede()` and `rejoin()` appropriately:

```python
def f(name, fn):
    ddf = dd.read_csv(fn)  # Note that this is a dask collection
    result = ddf[ddf['name'] == name].count()

    # This calls secede
    # Then, runs the computation on the cluster (including this worker)
    # Then, blocks on rejoin, and finally delivers the answer
    result = result.compute()

    return result
```

### Coordination Primitives

Sometimes, situations arise where tasks, workers, or clients need to coordinate with each other in ways beyond normal task scheduling with futures. In these cases, Dask provides additional primitives to help in complex situations.

Dask provides distributed versions of coordination primitives (e.g., locks, events, queues, global variables, and pub-sub systems) that, where appropriate, match their in-memory counterparts. These can be used to control access to external resources, track progress of ongoing computations, or share data in side-channels between many workers, clients, and tasks sensibly.

These features are rarely necessary for common use of Dask.

- [Queues](https://docs.dask.org/en/stable/futures.html#queues)
- [Global Variables](https://docs.dask.org/en/stable/futures.html#global-variables)
- [Locks](https://docs.dask.org/en/stable/futures.html#locks)
- [Events](https://docs.dask.org/en/stable/futures.html#events)
- [Semaphore](https://docs.dask.org/en/stable/futures.html#id1)
- [Publish-Subscribe](https://docs.dask.org/en/stable/futures.html#publish-subscribe)

### Actors

Actors allow workers to rapidly manage changing state without coordinating with the central scheduler. This has the advantage of reducing latency (worker-to-worker roundtrip latency is around 1ms), reducing pressure on the centralize scheduler (workers can coordinate actors entirely among each other), and also enabling workflows that require stateful or in-place memory manipulation.

However, these benefits come at a cost. The scheduler is unaware of actors, so they do not benefit from diagnostics, load balancing, or resilience. Once an actor is running on a worker, it is forever tied to that worker. If that worker becomes overburdened or dies, then there is no opportunity to recover the workload.

**Since actors avoid the central scheduler, they can be high-performing, but not resilient**.

#### Example: Counter

An actor is a class containing both state and methods that is submitted to a worker:

```python
class Counter:
    n = 0

    def __init__(self):
        self.n = 0

    def increment(self):
        self.n += 1
        return self.n


# Enable entry point for dask.distributed
from dask.distributed import Client

client = Client()

future = client.submit(Counter, actor=True)

counter = future.result()
counter
# <Actor: Counter, key=Counter-afa1cdfb6b4761e616fa2cfab42398c8>
```

Method calls on this object produce `ActorFutures`, which are similar to normal futures, but interact only with the worker holding the actor:

```python
future = counter.increment()
future
# <ActorFuture>

future.result()
# 1
```

Attribute access is synchronous and blocking:

```python
counter.n
# 1
```

#### Example: Parameter Server

This is a simple minimization. The Dask Actor will serve as the parameter server that will hold the model. The client will calculate the gradient of the loss function above.

```python
# Enable entry point for dask.distributed
from dask.distributed import Client
# Enable multi-dimensional arrays and matrices
import numpy as np


class ParameterServer:
    def __init__(self):
        self.data = dict()

    def put(self, key, value):
        self.data[key] = value

    def get(self, key):
        return self.data[key]


def train(params, lr=0.1):
    grad = 2 * (params - 1)  # Gradient of (params - 1)**2
    new_params = params - lr * grad
    return new_params


client = Client(processes=False)

ps_future = client.submit(ParameterServer, actor=True)
ps = ps_future.result()

ps.put('parameters', np.random.default_rng().random(1000))
for k in range(20):
    params = ps.get('parameters').result()
    new_params = train(params)
    ps.put('parameters', new_params)
    print(new_params.mean())
    # k=0: "0.5988202981316124"
    # k=10: "0.9569236575164062"
```

This example works and the loss function is minimized. If desired, this example could be adapted to machine learning with a more complex function to minimize.

#### Asynchronous Operation

All operations that require talking to the remote worker are awaitable:

```python
async def f():
    future = client.submit(Counter, actor=True)
    counter = await future  # Gather actor object locally

    counter.increment()  # Send off a request asynchronously
    await counter.increment()  # Or wait until it was received

    n = await counter.n  # Attribute access also must be awaited
```

Generally, all I/O operations that trigger computations (e.g., `.to_parquet()`) should be done using the `compute=False` parameter to avoid asynchronous blocking:

```python
await client.compute(ddf.to_parquet('/tmp/ex.parquet', compute=False))
```

## Machine Learning

Machine learning is a broad field involving many different workflows.

### Hyperparameter Optimization

#### Optuna

For state of the art hyperparameter optimization (HPO), it is recommended to use the [Optuna](https://optuna.org/) library, with the associated [Dask integration](https://optuna-integration.readthedocs.io/en/latest/reference/generated/optuna_integration.DaskStorage.html).

In Optuna, you construct an objective function that takes a trial object, which generates parameters from distributions that you define in code. Your objective function eventually produces a score. Optuna is smart about what values from the distribution it suggests, based on the scores it has received.

```python
def objective(trial):
    params = {
        'max_depth': trial.suggest_int('max_depth', 2, 10, step=1),
        'learning_rate': trial.suggest_float('learning_rate', 1e-8, 1.0,
                                             log=True)
        ...
    }
    model = train_model(train_data, **params)
    result = score(model, test_data)
    return result
```

Often, Dask and Optuna are used together by running many objective functions in parallel, and synchronizing the scores and parameter selection on the Dask scheduler. To do this, we use the `DaskStore` object found in Optuna.

```python
# Enable an automatic hyperparameter optimization framework
import optuna

storage = optuna.integration.DaskStorage()

study = optuna.create_study(
    direction='maximize',
    storage=storage  # This makes the study Dask-enabled
)
```

Then, we run many optimized methods in parallel:

```python
# Enable creating local Scheduler and Workers, and waiting until
# all/any futures are finished
from dask.distributed import LocalCluster, wait

# Replace this with some scalable cluster
cluster = LocalCluster(processes=False)
client = cluster.get_client()

futures = [
    client.submit(study.optimize, objective, n_trials=1, pure=False)
    for _ in range(500)
]
wait(futures)

print(study.best_params)
```

[Hyperparameter Optimization with XGBoost](https://docs.coiled.io/user_guide/hpo.html)

#### Dask Futures

For simpler situations, people often use [Dask Futures](https://docs.dask.org/en/stable/futures.html) to train the same model on lots of parameters. Dask Futures are a general purpose API that is used to run normal Python functions on various inputs.

```python
# Enable creating local Scheduler and Workers
from dask.distributed import LocalCluster


def train_and_score(params: dict) -> float:
    data = load_data()
    model = make_model(**params)
    train(model)
    score = evaluate(model)
    return score


# Replace this with some scalable cluster
cluster = LocalCluster(processes=False)
client = cluster.get_client()

params_list = [...]
futures = [
    client.submit(train_and_score, params)
    for params in params_list
]
scores = client.gather(futures)
best = max(scores)

best_params = params_list[scores.index(best)]
```

### Gradient Boosted Trees

Popular gradient boosted tree (GBT) libraries, like XGBoost and LightGBM, have native Dask support, which allows you to train models on very large data sets in parallel:

- [XGBoost](https://xgboost.readthedocs.io/en/stable/tutorials/dask.html)
- [LightGBM](https://lightgbm.readthedocs.io/en/latest/Parallel-Learning-Guide.html#dask)

The following example uses Dask DataFrame, XGBoost, and a local Dask cluster:

```python
import dask.dataframe as dd  # Enable parallel pandas DataFrame
# Enable creating local Scheduler and Workers
from dask.distributed import LocalCluster
# Enable an optimized distributed gradient boosting library
import xgboost as xgb

ddf = dask.datasets.timeseries()  # Randomly generated data
# ddf = dd.read_parquet(...)  # In practice, you would probably read data

train, test = ddf.random_split([0.80, 0.20])
X_train, y_train, X_test, y_test = ...

with LocalCluster() as cluster:
    with cluster.get_client() as client:
        d_train = xgb.dask.DaskDMatrix(client, X_train, y_train, 
                                       enable_categorical=True)
        model = xgb.dask.train(
            ...
            d_train,
        )
        predictions = xgb.dask.predict(client, model, X_test)
```

[XGBoost For Gradient Boosted Trees](https://docs.coiled.io/user_guide/xgboost.html)

### Batch Prediction

Once a model is trained, it is common to want to apply the model across lots of data. Most often this is done in two ways:

1. Using Dask Futures
2. Using [`DataFrame.map_partitions()`](https://docs.dask.org/en/stable/generated/dask.dataframe.DataFrame.map_partitions.html#dask.dataframe.DataFrame.map_partitions) or [`Array.map_blocks()`](https://docs.dask.org/en/stable/generated/dask.array.Array.map_blocks.html#dask.array.Array.map_blocks).

#### Dask Futures

Dask Futures are a general purpose API that lets you run arbitrary Python functions on Python data in parallel. This tool can be applied to solve the problem of batch prediction.

```python
# Enable creating local Scheduler and Workers
from dask.distributed import LocalCluster


def predict(filename, model):
    data = load(filename)
    result = model.predict(data)
    return result


# Replace this with some scalable cluster
cluster = LocalCluster(processes=False)
client = cluster.get_client()

filenames = [...]

model = client.submit(load_model, path_to_model)
predictions = client.map(predict, filenames, model=model)
results = client.gather(predictions)
```

[Dask and V100s For Fast, Distributed Batch Scoring of Computer Vision Workloads](https://developer.download.nvidia.com/video/gputechconf/gtc/2019/video/S9198/s9198-dask-and-v100s-for-fast-distributed-batch-scoring-of-computer-vision-workloads.mp4)

#### Dask DataFrame

Sometimes, we want to process our model with a higher level Dask API, like Dask DataFrame of Dask Array. This is more common with record data.

```python
import dask.dataframe as dd  # Enable parallel pandas DataFrame

ddf = dd.read_parquet("/path/to/ex/data.parquet")

model = load_model("/path/to/ex/model")

# pandas code
# predictions = model.predict(df)
# predictions.to_parquet("/path/to/results.parquet")

# Dask code
predictions = ddf.map_partitions(model.predict)
predictions.to_parquet("/path/to/results.parquet")
```

## Eager Methods

The following methods are *eager* by default:

- [`dask.dataframe.DataFrame.head()`](https://docs.dask.org/en/latest/generated/dask.dataframe.DataFrame.head.html)
- [`dask.dataframe.DataFrame.tail()`](https://docs.dask.org/en/latest/generated/dask.dataframe.DataFrame.tail.html)
- [`dask.dataframe.DataFrame.info()`](https://docs.dask.org/en/latest/generated/dask.dataframe.DataFrame.info.html)
    - The `verbose` and `memory_usage` arguments can be set for more helpful output (e.g., `.info(verbose=True, memory_usage=True)`).

## Sampling

Sampling by the number of items is not supported by Dask. Use the `frac` parameter, instead.

```python
ddf.sample(frac=0.01).compute()
```

<https://docs.dask.org/en/stable/generated/dask.dataframe.DataFrame.sample.html>

## Documentation

- [Dask Documentation](https://docs.dask.org/en/stable/)
    - [API Reference](https://docs.dask.org/en/stable/api.html)
        - [Array](https://docs.dask.org/en/stable/array-api.html)
        - [Bag](https://docs.dask.org/en/stable/bag-api.html)
        - [DataFrame](https://docs.dask.org/en/stable/dataframe-api.html)
        - [Delayed](https://docs.dask.org/en/stable/delayed-api.html)
        - [Futures](https://docs.dask.org/en/stable/futures.html)
    - [Dask DataFrame](https://docs.dask.org/en/stable/dataframe.html)
        - [Dask DataFrame Additional Information](https://docs.dask.org/en/stable/dataframe-extra.html)
    - [Dask Delayed](https://docs.dask.org/en/stable/delayed.html)
- [Dask Examples](https://examples.dask.org/)
    - [Custom Workloads With Futures](https://examples.dask.org/futures.html)
- [`dask.distributed` Documentation](https://distributed.dask.org/en/stable/)
- [`dask-ml` Documentation](https://ml.dask.org/)