# Data References

A collection of data reference materials.

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="da_ref/">Data Analysis Reference</a></td>
      <td>A collection of data analysis reference materials.</td>
    </tr>
    <tr>
      <td><a href="dask_ref/">Dask Reference</a></td>
      <td>A collection of Dask reference materials.</td>
    </tr>
    <tr>
      <td><a href="matplotlib_ref/">Matplotlib Reference</a></td>
      <td>A collection of Matplotlib reference materials.</td>
    </tr>
    <tr>
      <td><a href="ml_ref/">Machine Learning Reference</a></td>
      <td>A collection of machine learning reference materials.</td>
    </tr>
    <tr>
      <td><a href="pandas_ref/">pandas Reference</a></td>
      <td>A collection of pandas reference materials.</td>
    </tr>
    <tr>
      <td><a href="sql_ref/">SQL Reference</a></td>
      <td>A collection of SQL reference materials.</td>
    </tr>
    <tr>
      <td><a href="stats_ref/">Statistics Reference</a></td>
      <td>A collection of statistics reference materials.</td>
    </tr>
  </tbody>
</table>