# SQL Reference

A collection of SQL reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## SQL

The _Structured Query Language (SQL)_ is a standard language for defining, querying, and manipulating relational databases. Relational databases store data rows in _tables_.

SQL and a _database management system (DBMS)_ make it convenient to retrieve rows that match specific criteria.

Before you can fill a SQL database, you have to specify the table definitions:

- Names of the individual tables
- Names of the columns
- Nature of the values to be stored in a column
- Relationships between the objects

All of the table definitions of a database together are called a [database schema](https://en.wikipedia.org/wiki/Database_schema). There are large differences between the various SQL database systems in regard to table definitions.

Example SQLite schema:

```sql
CREATE TABLE person (
    id INTEGER PRIMARY KEY NOT NULL,
    first_name VARCHAR(20) NOT NULL,
    surname VARCHAR(20) NOT NULL,
    movie_id INTEGER NOT NULL
);

CREATE TABLE movie (
    id INTEGER PRIMARY KEY NOT NULL,
    title VARCHAR(40) NOT NULL,
    studio VARCHAR(20) NOT NULL,
    budget INTEGER NOT NULL
);

CREATE TABLE person_movie (
    id INTEGER PRIMARY KEY NOT NULL,
    person_id INTEGER NOT NULL,
    movie_id INTEGER NOT NULL
);
```

## Sublanguages

**Data Definition Language (DDL)**

- `CREATE TABLE`
- `ALTER TABLE`
- `DROP TABLE`

**Data Query Language (DQL)**

- `SELECT`

**Data Manipulation Language (DML)**

- `INSERT INTO`
- `UPDATE`
- `DELETE FROM`

**Data Control Language (DCL)**

- `GRANT`
- `REVOKE`

## CRUD

_Create_

- `CREATE TABLE`
- `ALTER TABLE` / `ADD COLUMN`

_Read_

- `SELECT`

_Update_

- `ALTER TABLE` / `ADD COLUMN`
- `ALTER TABLE` / `DROP COLUMN`
- `INSERT INTO` / `VALUES`
- `INSERT INTO` / `SELECT`/`FROM`
- `UPDATE` / `SET` / `WHERE`

_Delete_

- `DROP TABLE`
- `ALTER TABLE` / `DROP COLUMN`
- `DELETE FROM` / `WHERE`

## Operator Query Construction Order

1. Common Table Expressions (CTEs)
2. `SELECT`
3. `FROM`
4. `WHERE`
5. `UNION`
6. `GROUP BY`
7. `HAVING`
8. `ORDER BY`
9. `LIMIT`
10. `OFFSET`

## Operator Logical Processing Order

1. `FROM`
2. `ON`
3. `OUTER`
4. `WHERE`
5. `GROUP BY`
6. Aggregate Functions
7. `HAVING`
8. Window Functions
9. `SELECT`
10. `DISTINCT`
11. `UNION`, `INTERSECT`, `EXCEPT`
12. `ORDER BY`
13. `OFFSET`
14. `LIMIT`, `FETCH`, `TOP`

You can see how a DBMS processes your query by placing the `EXPLAIN` operator before your `SELECT` operator.

## Tables

A _table_ is a set of rows and columns. Usually, tables are named after the singular version of the entity they contain.

Each row contains information about one particular object.

The columns contain the object's features. Each field has a unique name and specific data type.

A _cell_ is a unit where a record and field (row and column) intersect.

All database tables need a [primary key](https://en.wikipedia.org/wiki/Primary_key) field in order to unambiguously define every record. All values in this field must be _unique_.

Some tables combine several columns to create the primary key, which is then called a _primary composite key_. Here, the values of individual columns might not be unique, but their combinations are. This allows you to unambiguously pinpoint each row.

[Foreign keys](https://en.wikipedia.org/wiki/Foreign_key) are used inside tables to refer to rows inside of other tables. It is common convention to give foreign keys the name of the table they point to with a suffix of `_id`.

Foreign keys are responsible for the relationship between tables.

### Table Relationships

There are three types of relationships:

1. _One-to-one_

    In a one-to-one relationship, each row in a table is connected with one and only one row in the other table. This is a rare type of relationship and is primarily used to increase security.

2. _One-to-many_

    In a one-to-many relationship, each row of a table matches multiple rows in another table.

3. _Many-to-many_

    In a many-to-many relationship, several rows from one table match several rows from another table. This type of relationship produces an _association table_, which combines the primary keys of both tables.

### Entity-Relationship Diagrams

The structure of databases can be shown with _entity-relationship (ER) diagrams_. They show tables and the relationships between them.

<figure>
    <img alt="" src="./assets/images/sql_hospital.png" width="800" height="422" />
    <figcaption><a href="https://commons.wikimedia.org/wiki/File:Sql_hospital.png">"Sql hospital.png"</a> by <a href="https://en.wikibooks.org/wiki/User:Borjasotomayor">Borjasotomayor</a> is licensed under a <a href="https://creativecommons.org/licenses/by-sa/3.0/deed.en">CC BY-SA 3.0</a> license</figcaption>
</figure>

Tables are shown as boxes with two parts. The table name goes in the upper part.  Lists of table columns with an indication of which keys they are related to (primary or foreign) go in the lower part. Usually, keys are marked `PK` (primary) or `FK` (foreign), but they can also be marked with a key icon, a `#`, or another symbol.

ER diagrams also show relationships. The end of the line connecting two tables indicates whether one or several values from one table match values from the other.

### JOINs

It is rare for all data to be stored in one table. Usually, data from multiple tables needs to be merged. The `JOIN` operator is used to perform these merges.

When you use the `JOIN` operator:

- Data is combined from multiple tables based on a matched condition between them.
- Data is combined into new columns.
- The number of columns selected from each table can be different.
- The data types of corresponding selected columns from each table can be different.
- Returned columns do not have to be distinct.

There are several ways to join tables:

- `INNER JOIN`
    - Self joins
- Outer joins
    - `FULL OUTER JOIN`
    - `LEFT OUTER JOIN`
    - `RIGHT OUTER JOIN`
- `CROSS JOIN`

#### INNER JOIN

The `INNER JOIN` returns only those rows that have matching values from table to table (i.e., the tables' intersection). The order in which the tables are joined does not affect the final result.

##### Self Joins

A _self join_ joins a table to itself. To perform a self join, a table should:

1. Have a column that acts as the primary key.
2. Have another column that stores values that can be matched with the values in the primary key column.

For any given row, the values in these columns do not need to match, and the value in the second column can also be `NULL`.

Self joins are used to:

- process a hierarchy
- list pairs within a table

#### OUTER JOIN

There are three types of outer joins:

1. `FULL OUTER JOIN`

    Returns all rows from both tables.

2. `LEFT OUTER JOIN`

    Selects all the data from the left table together with the rows from the right table that match the join condition.

3. `RIGHT OUTER JOIN`

    Selects all the data from the right table together with the rows from the left table that match the join condition.

With `LEFT OUTER JOIN` and `RIGHT OUTER JOIN`, the _order in which tables are listed is significant_.

#### CROSS JOIN

A `CROSS JOIN` is the [cartesian product](https://en.wikipedia.org/wiki/Cartesian_product) of two tables. Every row from the left table is joined with all of the rows from the right table.

#### Joining Multiple Tables

`JOIN`s can be chained together to join multiple tables using one query.

### Merging Data With UNION and UNION ALL

The `UNION` and `UNION ALL` operators bring together data from different queries.

When you use the `UNION` operator:

- The result set of two or more `SELECT` commands are combined.
- Data is combined into new rows.
- The number of columns selected from each table should be the same.
- The data types of corresponding selected columns from each table should be the same.
- Returned rows are distinct.

These conditions must be met for a `UNION` to work:

1. The first and second tables must match with respect to the number of columns selected and their data types.
2. Columns must be in the same order in the first and second tables.

`UNION` avoids duplicating rows when it generates a table.

`UNION ALL` includes duplicates in the resulting selection. It is useful when you know for sure that there are no matching values in the tables to be joined, or when you actually need the duplicate values.

## SQL Statements

To select data from tables, you need to write a _statement_, or _query_. A _statement_ is a request written according to SQL syntax. Your statement should specify what data to select and how to process it.

The `SELECT` clause takes the selection you need. It specifies the necessary columns from the database table (e.g., `*` to select all columns from a table). The `FROM` clause specifies the table from which the data should be taken.

- SQL _keywords_ are case-insensitive, but are usually written in uppercase (e.g., `SELECT, WHERE, FROM`).
- _Table names_ and _data strings_ are case-sensitive.
- Databases, tables, usernames, and text are written in lowercase to make them easier to distinguish.
- SQL _commands_ start with a keyword that is followed by a series of clauses to modify that keyword, and end with a semicolon (`;`). Line breaks usually follow each keyword.

        -- Select all columns from a table
        SELECT
            *
        FROM
            ex_tbl;

- Single line comments begin with two hyphens (`--`). Multi-line comments begin with `/*` and end with `*/`. Comments can be anywhere on a line, except within quotes.

    `-- A single-line comment`

        /* A multi-line comment
           has
           multiple
           lines */

- Single quotes (`''`) are for _user-entered data_. Double quotes (`""`) are for _table and column names_, which are used when the table or column name contains spaces or symbols. Otherwise, they are optional.

### Filtering Data

Data can be filtered with the `WHERE` clause:

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
WHERE
    ex_cond;
```

`WHERE` selects input rows _before_ groups and aggregates are computed, i.e., it controls which rows go into groups and aggregate computations.

### Concatenation Operator

The `||` operator concatenates two strings:

```sql
SELECT
    ex_col_1 || ', ' || ex_col_2
FROM
    ex_tbl;
```

### Comparison Operators

<table>
    <caption>Comparison Operators</caption>
    <thead>
        <tr>
            <th>Name</th>
            <th>Meaning</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>=</code></td>
            <td>Equal to</td>
        </tr>
        <tr>
            <td><code>!=</code>, <code><></code></td>
            <td>Not equal to</td>
        </tr>
        <tr>
            <td><code>></code></td>
            <td>Greater than</td>
        </tr>
        <tr>
            <td><code><</code></td>
            <td>Less than</td>
        </tr>
        <tr>
            <td><code>>=</code></td>
            <td>Greater than or equal to</td>
        </tr>
        <tr>
            <td><code><=</code></td>
            <td>Less than or equal to</td>
        </tr>
    </tbody>
</table>

```sql
SELECT
    ex_col_1,
    ex_col_2,
    ex_col_3
FROM
    ex_tbl
WHERE
    ex_col_3 >= 'yyyy-mm-dd'
    AND ex_col_3 <= 'yyyy-mm-dd';
```

### Logical Operators

SQL uses the logical operators `AND`, `OR`, and `NOT`.

<table>
    <caption>Logical Operators</caption>
    <thead>
        <tr>
            <th>Name</th>
            <th>Meaning</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>AND</code></td>
            <td>Select rows when both conditions are true</td>
        </tr>
        <tr>
            <td><code>OR</code></td>
            <td>Select rows when either or both conditions are true</td>
        </tr>
        <tr>
            <td><code>NOT</code></td>
            <td>Select rows when the condition is false</td>
        </tr>
    </tbody>
</table>

```sql
SELECT
    *
FROM
    ex_tbl
WHERE
    ex_cond_1
    AND ex_cond_2;
```

```sql
SELECT
    *
FROM
    ex_tbl
WHERE
    ex_cond_1
    OR ex_cond_2;
```

```sql
SELECT
    *
FROM
    ex_tbl
WHERE
    ex_cond_1
    AND NOT ex_cond_2;
```

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
WHERE
    ex_col_2 = 'foo'
    OR ex_col_2 = 'bar'
    OR ex_col_2 = 'baz';
```

The `IN` operator is followed by a list of values to be included in the result:

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
WHERE
    ex_col_2 IN ('foo', 'bar', 'baz');
```

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
WHERE
    ex_col_2 IN (1, 2, 3);
```

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
WHERE
    ex_col_2 IN ('yyyy-mm-dd', 'yyyy-mm-dd', 'yyyy-mm-dd');
```

You can negate the `IN` operator with the `NOT` keyword:

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
WHERE
    ex_col_2 NOT IN ('foo', 'bar', 'baz');
```

### Aggregate Functions

SQL has specific functions for calculating the total number of rows, sums, averages, and minimum and maximum values. These are referred to as _aggregate functions_. They collect, or _aggregate_, all of the objects within a group to calculate a single summary value.

```sql
-- ex_alias is the name of the column where the function's output
-- will be stored
SELECT
    EX_AGG_FUNC(ex_col) AS ex_alias
FROM
    ex_tbl;
```

The number of rows can be calculated in various ways depending on the task:

- `COUNT(*)` returns the total number of table rows
- `COUNT(ex_col)` returns the number of values in `ex_col`
- `COUNT(DISTINCT ex_col)` returns the number of _unique_ values in `ex_col`

`SUM(ex_col)` returns the sum of the values in `ex_col`. It ignores missing values and only works with numerical values.

`AVG(ex_col)` returns the average value of `ex_col` values. To calculate the average, you need numeric values.

The smallest and largest values can be found with the `MIN()` and `MAX()` functions.

### Converting Data Types

Data types can be converted with the `CAST()` function:

`CAST(ex_col AS ex_data_type)`

`ex_col` is the column whose data type is to be converted. `ex_data_type` is the desired type.

`CAST()` shorthand is written like so:

`ex_col::ex_data_type`

The supported data types will depend on the DBMS used:

- [MariaDB Data Types](https://mariadb.com/kb/en/data-types/)
- [MySQL Data Types](https://dev.mysql.com/doc/refman/8.0/en/data-types.html)
- [Oracle Data Types](https://docs.oracle.com/database/121/SQLRF/sql_elements001.htm#SQLRF30020)
- [PostgreSQL Data Types](https://www.postgresql.org/docs/current/datatype.html)
- [SQLite Data Types](https://www.sqlite.org/datatype3.html)

For most standard date/time formats, `CAST()` can handle the required string conversion. When it cannot, other [data type formatting functions](https://www.postgresql.org/docs/current/functions-formatting.html) are available.

### Grouping Data

The `GROUP BY` clause is used when data needs to be divided into groups according to the values of columns.

```sql
SELECT
    ex_col_1,
    ex_col_2,
    EX_AGG_FUNC(ex_col) AS ex_alias
FROM
    ex_tbl
GROUP BY
    ex_col_1,
    ex_col_2;
```

Once you know which columns you are grouping by, make sure that all of those columns are listed in both the `SELECT` clause _and_ the `GROUP BY` clause.

Aggregate functions _should not_ be included in the `GROUP BY` clause.

### Sorting Data

To sort data by a column, use the `ORDER BY` clause:

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
ORDER BY
    ex_col_3,
    ex_col_4;
```

Unlike `GROUP BY`, with `ORDER BY`, only those columns by which you want to sort the data should be listed in the `ORDER BY` clause.

Two modifiers can be used with the `ORDER BY` clause to sort the data in columns:

- `ASC` (the default) sorts data in ascending order.
- `DESC` sorts data in descending order.

The `ORDER BY` modifiers are placed after the column by which the data is sorted:

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
ORDER BY
    ex_col_3 DESC,
    ex_col_4 ASC;
```

### Limiting Output

The `LIMIT` clause sets a limit to the number of rows in the result:

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
ORDER BY
    ex_col_2 DESC
LIMIT
    ex_num;
```

### Filtering Grouped Data

The `HAVING` clause works like `WHERE` for data grouped by `GROUP BY` and aggregate functions:

```sql
SELECT
    ex_col_1,
    ex_col_2,
    EX_AGG_FUNC(ex_col) AS ex_alias
FROM
    ex_tbl
GROUP BY
    ex_col_1,
    ex_col_2
HAVING
    ex_cond;
```

The resulting selection will include only those rows for which the aggregate function produces results that meet the condition indicated in the `HAVING` block.

`HAVING` selects rows _after_ groups and aggregates are computed.

`HAVING` exists because the `WHERE` command is processed _before_ grouping and aggregations occur. This makes it impossible to set filtering parameters that use the results of an aggregate function with `WHERE`.

### Working With Dates

Two important functions for working with dates are:

1. `EXTRACT()`
2. `DATE_TRUNC()`

Both functions are called in the `SELECT` clause of a SQL statement.

`EXTRACT()` extracts the information that you need from the timestamp.

You can retrieve:

- `century`
- `day`
- `doy` - Day of the year, from `1` to `365/366`.
- `hour`
- `isodow` - Day of the week under ISO 8601, the international date and time format. Monday is `1`, Sunday is `7`.
- `milliseconds`
- `minute`
- `month`
- `quarter`
- `second`
- `week` - Week of the year.
- `year`

```sql
SELECT
    EXTRACT(ex_date_fragment FROM ex_col) AS ex_alias
FROM
    ex_tbl;
```

`DATE_TRUNC()` truncates the date when you only need a certain level of precision.

Unlike with `EXTRACT`, the resulting truncated date is given as a string. The column from which the full date is to be taken comes after a comma:

```sql
SELECT
    DATE_TRUNC('ex_date_fragment_to_truncate_to', ex_col) AS ex_alias
FROM
    ex_tbl;
```

You can use the following arguments with the `DATE_TRUNC()` function:

- `'century'`
- `'day'`
- `'decade'`
- `'hour'`
- `'microseconds'`
- `'milliseconds'`
- `'minute'`
- `'month'`
- `'quarter'`
- `'second'`
- `'week'`
- `'year'`

### Subqueries

A _subquery_ is a query inside a query. Subqueries can be used at various locations within a query.

If a subquery is inside the `FROM` block, `SELECT` will select data from the table that gets generated by the subquery. The name of the subquery table is indicated within the inner query, and the outer query refers to the subquery table's columns.

Subqueries are always put in parentheses:

```sql
SELECT
    subq.ex_col,
    subq.ex_col_2
FROM (
    SELECT
        ex_col,
        ex_col_2
    FROM
        ex_tbl
    WHERE
        ex_col = ex_val
) AS subq;
```

### Testing For Record Existence

The `EXISTS` operator tests for the existence of any record in a subquery. It returns `TRUE` if the subquery returns one or more rows.

```sql
SELECT
    ex_col
FROM
    ex_tbl
WHERE EXISTS (
    SELECT
        ex_col
    FROM
        ex_tbl
    WHERE
        ex_cond
); 
```

```sql
SELECT
    ex_col
FROM
    ex_tbl
WHERE NOT EXISTS (
    SELECT
        ex_col
    FROM
        ex_tbl
    WHERE
        ex_cond
); 
```

### Common Table Expressions (CTEs)

CTEs are [temporary named result sets](https://en.wikipedia.org/wiki/Hierarchical_and_recursive_queries_in_SQL#Common_table_expression) and are a nice alternative to subqueries.

```sql
WITH cte AS (
    SELECT
        ex_col,
        ex_col_2
    FROM
        ex_tbl
    WHERE
        ex_col = ex_val
)
SELECT
    cte.ex_col,
    cte.ex_col_2
FROM
    cte;
```

### Window Functions

Window functions are a special use of aggregate functions that calculate an aggregate value over the rows being returned in a group _as the individual result rows are processed_. For example, an aggregate function like `MIN()` tells you the lowest value of a column within a set of rows. Using `MIN()` as a window function tells you the lowest value of a column within a set of rows _as of that row_.

The _window_ is a sequence of rows on which the calculations are made. It can be either the entire table or, for example, the six rows above the current one. Working with windows is different from working with regular queries. Essentially, windows are _virtual tables_.

Window functions are only permitted in the `SELECT` and `ORDER BY` clauses of your queries. Also, window functions execute after non-window aggregate functions, i.e., it is valid to include an aggregate function in the arguments of a window function, but not vice versa.

```sql
SELECT
    ex_col_1,
    ex_col_2,
    ex_col_3 / EX_AGG_FUNC(ex_col_3) OVER() AS ex_alias
FROM
    ex_tbl;
```

The function preceding the `OVER` keyword is executed on the data inside the window. If you do not indicate any parameters (as in the example above), the entire result of the query will be used.

The `PARTITION BY` keyword groups data for window functions:

```sql
SELECT
    ex_col_1,
    ex_col_2,
    ex_col_3 / EX_AGG_FUNC(ex_col_3) OVER(PARTITION BY 
                                              ex_col_1) AS ex_alias
FROM
    ex_tbl;
```

Sorting parameters can be used to help the DBMS understand what order the data is in:

```sql
SELECT
    ex_col_1,
    ex_col_2,
    EX_AGG_FUNC(ex_col_3) OVER (ORDER BY
                                    ex_col_1
                                ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS ex_alias
FROM
    ex_tbl;
```

`ORDER BY` lets you define the sorting order of the rows through which the window will run. In `ROWS`, you indicate the window frames over which an aggregate function is to be calculated.

Frames can be indicated in several ways:

- `UNBOUNDED PRECEDING` - All rows that are above the current one.
- `N PRECEDING` - The _n_ rows above the current one.
- `CURRENT ROW` - The current row.
- `N FOLLOWING` - The _n_ rows below the current one.
- `UNBOUNDED FOLLOWING` - All rows below the current one.

The parameters themselves can be combined.

When dealing with multiple window functions, each windowing behavior can be named in a `WINDOW` clause and referenced in `OVER`:

```sql
SELECT
    EX_AGG_FUNC(ex_col_1) OVER ex_alias
FROM
    ex_tbl
WINDOW ex_alias AS (PARTITION BY
                        ex_col_2
                    ORDER BY
                        ex_col_1 DESC);
```

#### Special Aggregate Functions

In addition to `SUM`, `AVG`, and `COUNT`, there are special aggregate functions that can be used when working with windows.

##### Ranking Functions

The `RANK()` function returns the index number of a row in the current window. If several rows have a value assigned according to the rules of `ORDER BY`, they will be assigned the same number. This feature allows you to rank your data.

```sql
SELECT
    ex_col_1,
    ex_col_2,
    ex_col_3,
    RANK() OVER (PARTITION BY
                    ex_col_1
                 ORDER BY
                    ex_col_3 ASC) AS ex_alias
FROM
    ex_tbl;
```

##### Categorizing Functions

With the `NTILE()` function, you can put a given output row into a group. This is similar to partitioning data into quartiles. The number of groups the data is to be divided into is passed to the function.

```sql
SELECT
    ex_col_1,
    ex_col_2,
    ex_col_3,
    ntile(ex_num) OVER (ORDER BY ex_col_3)
FROM
    ex_tbl;
```

##### Offset Functions

Often, you may need to compare the current value with preceding or subsequent ones. The `LAG()` and `LEAD()` functions, respectively, can help with this.

You can pass the function the name of the column and the offset (i.e., the number of rows) over which the value is to be taken. If you do not indicate the offset, it will revert to the default, `1`.

```sql
SELECT
    ex_col_1,
    ex_col_2,
    ex_col_3,
    LAG(ex_col_3) OVER (PARTITION BY
                            ex_col_1
                        ORDER BY
                            ex_col_4 ASC) AS ex_alias
FROM
    ex_tbl;
```

### Searching For Empty Values

In SQL, empty cells are said to be `NULL`. The `IS NULL` operator searches for them:

```sql
SELECT
    *
FROM
    ex_tbl
WHERE
    ex_col IS NULL;
```

```sql
SELECT
    *
FROM
    ex_tbl
WHERE
    ex_col IS NOT NULL;
```

The `CASE` clause can be used to search for and replace values, including `NULL` values:

```sql
SELECT
    ex_col_1,
    CASE WHEN ex_cond_1 THEN
        ex_result_1
    WHEN ex_cond_2 THEN
        ex_result_2
    WHEN ex_cond_3 THEN
        ex_result_3
    ELSE
        ex_result_4
    END AS ex_alias
FROM
    ex_tbl;
```

### Searching For Data

The `LIKE` operator searches a table for values that follow a given pattern. You can search not only for a word, but also for a fragment of it. Indicate the necessary column before `LIKE` and follow it with a pattern:

`ex_col LIKE 'ex_pattern'`

Patterns in SQL slightly differ from those used in other contexts. In SQL, the `%` symbol represents any sequence of _zero or more characters_. The `_` symbol represents any character _one time_.

```sql
SELECT
    *
FROM
    ex_tbl
WHERE
    ex_col LIKE '%ex_str%';
```

The `NOT` operator gives you an _inverse_ query:

```sql
SELECT
    *
FROM
    ex_tbl
WHERE
    ex_col NOT LIKE '%ex_str%';
```

If you are looking for the literal `%` symbol, you can use the `ESCAPE` operator. It is passed a symbol that becomes the escape character.

Whatever escape character is assigned, in the pattern, it means that the symbol following it is _not_ a wildcard character, but the character itself, which the substring must include.

```sql
-- Find all substrings ending with a literal %
ex_col LIKE '%!%' ESCAPE '!'
```

### Formatting Recommendations

#### General Guidelines

- No tabs. Two or four spaces per indent.
- No trailing whitespace.
- Always capitalize SQL keywords (e.g., `SELECT` or `AS`).
- Variable names should be underscore separated (e.g., `SELECT COUNT(*) AS num_of_chars`).
- Comments should go near the top of your query, or at least near the closest `SELECT`.
- Try to only comment on things that are not obvious about the query (e.g., why a particular ID is hardcoded).
- Do not use single letter variable names. Be as descriptive as possible given the context (e.g., `SELECT characters.alignment AS char_align`).
- Use CTEs early and often, and name them well.

#### SELECT

- The `SELECT` keyword goes on its own line. Align all columns to the first column on their own line.
- Always rename aggregate functions and function-wrapped columns.
- Always use `AS` to rename columns.
- Long window functions should be split across multiple lines:
    - One for `PARTITION`
    - One for `ORDER` and frame clauses, aligned to the `PARTITION` keyword
    
    Partition keys should be one-per-line, aligned to the first.
    
    Order (`ASC`, `DESC`) should always be explicit.
    
    All window functions should be aliased.

#### FROM

Only one table should be in the `FROM` clause. Never use `FROM`-joins.

#### JOIN

- Explicitly use `INNER JOIN`, not just `JOIN`.
- Place the `ON` keyword and condition in the `INNER JOIN` on a new indented line.
- Place additional filters in the `INNER JOIN` on new indented lines.
- Begin with `INNER JOIN`s. Then, list `LEFT OUTER JOIN`s, order them semantically, and do not intermingle `LEFT OUTER JOIN`s with `INNER JOIN`s, unless necessary.

#### WHERE

Multiple `WHERE` clauses should go on different lines and begin with the SQL operator.

#### CTEs

- The body of a CTE must be one indent further than the `WITH` keyword.
- Open CTEs at the end of a line and close them on a new line.
- Multiple CTEs should be formatted accordingly:

        WITH cte_1 AS (
            ...
        ), cte_2 AS (
            ...
        ), cte_3 AS (
            ...
        )

- If possible, join CTEs inside subsequent CTEs, not in the main clause.
- Always use CTEs over inlined subqueries.

## SQL Commands

```sql
CREATE TABLE ex_tbl (
    ex_col_1 ex_type ex_options,
    ex_col_2 ex_type ex_options,
    PRIMARY KEY (ex_col_1)
);
```
Create a table.

The options give more context to the column and allow for better data consistency:

- `NOT NULL` Do not allow `NULL` values to be stored.
- `PRIMARY KEY` This column is a primary key that is indexed and must be unique for each row. Values in this column can serve as _foreign keys_ in records from other tables.
- `UNIQUE` This column is not the primary key, but is not allowed to have any duplicates.

`SELECT 'ex_str';`  
Ask for a string echoed back.

`SELECT ex_num + ex_num;`  
Run an expression.


```sql
SELECT
    *
FROM
    ex_tbl;
```
Get information from all columns in a table.

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl;
```
Get information from specific columns in a table.

```sql
SELECT
    *
FROM
    ex_tbl AS ex_alias
```
Create an alias for a table using the `AS` clause.

Once you alias a table, you will need to use the alias for that table for the rest of your query.

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
WHERE
    ex_col_1 ex_cond;
```
Filter results using the `WHERE` clause.

Numeric comparison operators:

- `=` Equals
- `!=`, `<>` Not equals
- `<` Less than
- `<=` Less than or equal to
- `>` Greater than
- `>=` Greater than or equal to

If there is no `WHERE` and no explicit `JOIN`, a `SELECT` clause spanning multiple tables yields the Cartesian product of all records of all the tables in question.

`HAVING` exists because the `WHERE` command is processed _before_ grouping and aggregations occur. This makes it impossible to set filtering parameters that use the results of an aggregate function with `WHERE`.

```sql
SELECT
    ex_col
FROM
    ex_tbl
WHERE
    ex_col BETWEEN ex_item_1 AND ex_item_2;
```
Search in a range between two items using the `BETWEEN` clause.

```sql
SELECT
    ex_col
FROM
    ex_tbl
WHERE
    ex_col IN (ex_item_1, ex_item_2);
```
Search for specific items in a set using the `IN` clause. The `IN` clause is shorthand for multiple `OR` conditions.

```sql
SELECT
    ex_col
FROM
    ex_tbl
WHERE
    ex_col LIKE 'ex_substr';
```
Perform a substring match on a column using the `LIKE` clause.

You can look for substrings within a column by using one of two metacharacters:

1. `%` matches zero or more characters.
2. `_` matches a single character.

`LIKE` behaves like `=` if there are no metacharacters.

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
WHERE
    (ex_col_1 LIKE 'ex_substr'
    AND ex_col_2 ex_cond)
    OR ex_col_3 ex_cond;
```
Filter using multiple conditions.

`AND` has a higher precedence than `OR`. Use parentheses to address ambiguities.

```sql
SELECT
    ex_col_1,
    CASE WHEN ex_cond_1 THEN
        ex_result_1
    WHEN ex_cond_2 THEN
        ex_result_2
    WHEN ex_cond_3 THEN
        ex_result_3
    ELSE
        ex_result_4
    END AS ex_alias
FROM
    ex_tbl;
```
Return different values based on multiple conditions using a `CASE` statement.

```sql
SELECT
    *
FROM
    ex_tbl
ORDER BY
    ex_col_1,
    ex_col_2;
```
Sort your results using the `ORDER BY` clause.

The `ORDER BY` clause expects a column name, or set of column names, on which to sort. The results are sorted by the first column, with the second column used for a tie breaker.

By default, sorts are performed in ascending order.

```sql
SELECT
    ex_col
FROM
    ex_tbl
ORDER BY
    ex_col DESC;
```
Sort by descending order using the `DESC` keyword.

```sql
SELECT
    ex_col
FROM
    ex_tbl
ORDER BY
    ex_col ASC;
```
Sort by ascending order using the `ASC` keyword.

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
LIMIT
    ex_num;
```
Limit the number of results you receive using the `LIMIT` clause.

```sql
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl
ORDER BY
    ex_col_2 DESC
LIMIT
    ex_num
OFFSET
    ex_num;
```
Skip results by using an offset with the `OFFSET` clause.

```sql
SELECT
    *
FROM
    ex_tbl
WHERE
    ex_col IS NULL;
```
Identify `NULL` values using the `IS NULL` clause.

```sql
SELECT
    *
FROM
    ex_tbl
WHERE
    ex_col IS NOT NULL;
```
Identify non-`NULL` values using the `IS NOT NULL` clause.

```sql
SELECT
    subq.ex_col,
    subq.ex_col_2
FROM (
    SELECT
        ex_col,
        ex_col_2
    FROM
        ex_tbl
    WHERE
        ex_col = ex_val
) AS subq;
```
Insert a second (child) query into a first (parent) query with a subquery.

```sql
SELECT
    ex_col_1,
    ex_col_2,
    EX_AGG_FUNC(ex_col_3) AS ex_alias
FROM
    ex_tbl
GROUP BY
    ex_col_1,
    ex_col_2;
```
Roll up rows based on similar columns and perform a calculation on them.

`GROUP BY` expects a comma-separated list of column names. Rows that have the same values in those columns are rolled up and counted by _aggregate functions_ in the `SELECT` clause.

```sql
SELECT
    ex_tbl_1.ex_col,
    ex_tbl_2.ex_col
FROM
    ex_tbl_1
INNER JOIN ex_tbl_2
    ON ex_tbl_1.ex_pri_key = ex_tbl_2.ex_fgn_key;
```
Join two tables with an inner join using the `INNER JOIN` clause.

`ex_tbl_1` is joined with `ex_tbl_2`, based on the condition that `ex_tbl_1` row's `ex_pri_key` value is the same as an `ex_tbl_2` row's `ex_fgn_key` value.

Without `ON`, every row from the left table will be matched with every row on the right table.

The inner join requires that each result row have a row from both the left and right tables. Otherwise, the row does not make it to the result.

```sql
SELECT
    ex_alias_1.ex_col_1,
    ex_alias_1.ex_col_2,
    ex_alias_1.ex_col_3,
    ex_alias_2.ex_col
FROM
    ex_tbl AS ex_alias_1
INNER JOIN ex_tbl AS ex_alias_2
    ON ex_alias_1.ex_pri_key = ex_alias_2.ex_fgn_key;
```
Self join a table using the `INNER JOIN` clause.

To perform a self join, a table should:

1. Have a column that acts as the primary key.
2. Have another column that stores values that can be matched with the values in the primary key column.

```sql
SELECT
    ex_tbl_1.ex_col,
    ex_tbl_2.ex_col
FROM
    ex_tbl_1
FULL OUTER JOIN ex_tbl_2
    ON ex_tbl_1.ex_pri_key = ex_tbl_2.ex_fgn_key;
```
Join two tables with a full outer join using the `FULL OUTER JOIN` clause.

The full outer join returns all rows from both tables, regardless of whether or not the other table has a matching row. If there is no match, the missing data is filled in with a `NULL` value.

Some database systems explicitly write the word `NULL` and some leave a blank. `NULL` means more than _information missing_. You cannot compare anything to `NULL`.

```sql
SELECT
    ex_tbl_1.ex_col,
    ex_tbl_2.ex_col
FROM
    ex_tbl_1
LEFT OUTER JOIN ex_tbl_2
    ON ex_tbl_1.ex_pri_key = ex_tbl_2.ex_fgn_key;
```
Join two tables with a left outer join using the `LEFT OUTER JOIN` clause.

The left outer join includes all rows from the left table and rows from the right table that match the `ON` condition. If there is no match on the right table, the missing data is filled in with a `NULL` value.

```sql
SELECT
    ex_tbl_1.ex_col,
    ex_tbl_2.ex_col
FROM
    ex_tbl_1
RIGHT OUTER JOIN ex_tbl_2
    ON ex_tbl_1.ex_pri_key = ex_tbl_2.ex_fgn_key;
```
Join two tables with a right outer join using the `RIGHT OUTER JOIN` clause.

The right outer join includes all rows from the right table and rows from the left table that match the `ON` condition. If there is no match on the left table, the missing data is filled in with a `NULL` value.

```sql
SELECT
    ex_tbl_1.ex_col,
    ex_tbl_2.ex_col
FROM
    ex_tbl_1
CROSS JOIN
    ex_tbl_2;
```
Cross join two tables using the `CROSS JOIN` clause.

Cross joins produce the cartesian product of two tables, i.e., every row from the left table is joined with all of the rows from the right table.

```sql
SELECT
    ex_col_1
FROM
    ex_tbl_1
UNION -- or UNION ALL
SELECT
    ex_col_1
FROM
    ex_tbl_2;
```
Bring together data from different queries.

These conditions must be met for a `UNION` to work:

1. The first and second tables must match with respect to the number of columns selected and their data types.
2. Columns must be in the same order in the first and second tables.

`UNION` avoids duplicating rows when it generates a table.

`UNION ALL` includes duplicates in the resulting selection. It is useful when you know for sure that there are no matching values in the tables to be joined, or when you actually need the duplicate values.

```sql
INSERT INTO
    ex_tbl (ex_col_1, ex_col_2)
VALUES
    (ex_val_1, ex_val_2);
```
Insert a row into a table using the `INSERT INTO` clause.

Unspecified column values will be `NULL`, if allowed.

An alternate form of `INSERT`  is to specify values for all of the columns, which eliminates the need to tell the DBMS which columns you will use:

```sql
INSERT INTO
    ex_tbl
VALUES
    (ex_val_1, ex_val_2, ex_val_3, ex_val_4);
```

If there are columns that you do not know what the values will be, enter `NULL` as the values.

```sql
INSERT INTO
    ex_tbl
VALUES
    (ex_val_1, ex_val_2, ex_val_3, ex_val_4),
    (ex_val_5, ex_val_6, ex_val_7, ex_val_8);
```
Insert many rows into a table in one command using the `INSERT INTO` clause.

```sql
INSERT INTO
    ex_tbl_1 (ex_col_1, ex_col_2)
SELECT
    ex_col_1,
    ex_col_2
FROM
    ex_tbl_2;
```
Insert data into a table from a `SELECT` clause.

```sql
UPDATE
    ex_tbl
SET
    ex_col = ex_val
WHERE
    ex_cond;
```
Update data in a table using the `UPDATE` clause.

`WHERE ex_cond` is used to target specific rows, which enables the use of `UPDATE` to target specific cells in a table.

```sql
ALTER TABLE
    ex_tbl;
```
Alter a table using the `ALTER TABLE` clause.

The `ALTER TABLE` clause is used to add, delete, or modify columns in an existing table.

```psql
-- SQLite/PostgreSQL
ALTER TABLE
    ex_tbl
ADD COLUMN
    ex_col ex_data_type;
```

```psql
-- SQLite/PostgreSQL
ALTER TABLE
    ex_tbl
DROP COLUMN
    ex_col;
```

```psql
-- SQLite/PostgreSQL
ALTER TABLE
    ex_tbl_old
RENAME TO
    ex_tbl_new;
```

```psql
-- SQLite/PostgreSQL
ALTER TABLE
    ex_tbl
RENAME COLUMN
    ex_col_old
TO
    ex_col_new
```

```psql
-- PostgreSQL
ALTER TABLE
    ex_tbl
ALTER COLUMN
    ex_col TYPE ex_data_type;
```

The `ALTER TABLE` statement is also used to [add](https://www.postgresql.org/docs/current/ddl-alter.html#DDL-ALTER-ADDING-A-CONSTRAINT) and [drop](https://www.postgresql.org/docs/current/ddl-alter.html#DDL-ALTER-REMOVING-A-CONSTRAINT) various [constraints](https://en.wikipedia.org/wiki/Relational_database#Constraints) on an existing table.

```sql
DELETE FROM
    ex_tbl
WHERE
    ex_cond
```
Delete data in a table that meets specific criteria using the `DELETE FROM` and `WHERE` clauses.

```sql
DELETE FROM
    ex_tbl
```
Delete all data in a table using the `DELETE FROM` clause.

```sql
DROP TABLE
    ex_tbl;
```
Delete a table using the `DROP TABLE` clause.

## Documentation

- [SQL Reserved Words](https://en.wikipedia.org/wiki/SQL_reserved_words)
- [SQL Syntax](https://en.wikipedia.org/wiki/SQL_syntax)
- [Structured Query Language WikiBook](https://en.wikibooks.org/wiki/Structured_Query_Language)
- [The SQL Language](https://www.postgresql.org/docs/current/sql.html)