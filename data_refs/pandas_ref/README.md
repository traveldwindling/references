# pandas Reference

A collection of pandas reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

pandas is an open source, BSD-licensed library providing high-performance, easy-to-use data structures and data analysis tools for the Python programming language.

## Import/Export

```python
import numpy as np  # Enable multi-dimensional arrays and matrices
import pandas as pd  # Enable data structures and data analysis tools
# Enable creating static, animated, and interactive visualizations
import matplotlib.pyplot as plt
```

pandas has various `read_*()` and `to_*()` [functions](https://pandas.pydata.org/docs/user_guide/io.html) to import and export data, respectively.

The default writer used by `.to_excel()` depends on the [chosen output file extension](https://github.com/pandas-dev/pandas/blob/main/pandas/io/excel/_util.py#L50):

- `xlsx` - `openpyxl` (`xlsxwriter` preferred if installed)
- `xlsm` - `openpyxl`
- `xlsb` - `pyxlsb`
- `ods` - `odf`

<!--  -->

- <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.to_excel.html>
- <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_excel.html>

```python
from typing import Any  # Enable special kind of type


def ctr_cont(val: Any) -> str:
    '''
    Center content.

    Parameters
    ----------------
    val : Any
        Value to apply style to.

    Returns
    ----------------
    str
        CSS attribute-value pair to apply.
    '''
    return 'text-align: center'


with pd.ExcelWriter(
    'ex_wb.xlsx',
    engine='xlsxwriter',
    engine_kwargs={
        'options': {
            'strings_to_urls': False,
            'strings_to_numbers': True
        }
    }
) as writer:
    (
        df.style
          .map(ctr_cont)
          .to_excel(writer, index=False)
    )
    wb = writer.book
    ws = writer.sheets['Sheet1']
    ws.autofit()
```

- <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.ExcelWriter.html>
- <https://xlsxwriter.readthedocs.io/working_with_pandas.html>
    - <https://xlsxwriter.readthedocs.io/workbook.html#constructor>
- <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.style.html>
    - <https://pandas.pydata.org/pandas-docs/stable/user_guide/style.html>

Extant workbooks can be modified with libraries like `openpyxl`:

```python
# Enable a Python library to read/write Excel 2010 xlsx/xlsm files
import openpyxl

wb = openpyxl.load_workbook('ex_wb.xlsx')

sheet_names = wb.sheetnames
# ['Sheet1', 'Sheet2']

ws = wb['Sheet2']

# Create map of column names to column indices
col_name_idx_map = {
    col[0].value: idx
    for idx, col in enumerate(ws.iter_cols(min_col=1, max_col=ws.max_column),
                              start=1)
}
# Print first six cells of third column
for row in ws.iter_rows(min_row=1, max_row=6):
    print(row[col_name_idx_map['ex_col_3'] - 1].value)

# Create map of column names to column letters
col_name_ltr_map = {
    col[0].value: openpyxl.utils.get_column_letter(idx)
    for idx, col in enumerate(ws.iter_cols(min_col=1, max_col=ws.max_column),
                              start=1)
}

# Convert column values to float and specific number format
for row in ws.iter_rows():
    for cell in row:
        cnd = (cell.column_letter == col_name_ltr_map['ex_col_3']
               and cell.row > 1)
        if cnd:
            try:
                ws[cell.coordinate] = float(cell.value)
            except Exception:
                pass
            else:
                ws[cell.coordinate].number_format = '0.00'

wb.save('ex_wb.xlsx')
```

- <https://openpyxl.readthedocs.io/en/stable/api/openpyxl.workbook.workbook.html>
- <https://openpyxl.readthedocs.io/en/stable/api/openpyxl.worksheet.worksheet.html>
- <https://openpyxl.readthedocs.io/en/stable/styles.html>
    - <https://openpyxl.readthedocs.io/en/stable/_modules/openpyxl/styles/numbers.html>

When importing large files where memory constraints are an issue, some `read_*()` functions offer a `chunksize` parameter, which can be used to load a set number of lines at a time. An iterable `TextFileReader` object is returned, which can be passed to `pandas.concat()` for conversion to a single DataFrame.

### Importing From a String

```python
from io import StringIO

data = '''h1|h2|h3|h4
one|two|three|four
uno|dos|tres|cuatro
ichi|ni|san|yon'''

delim = '|'

# StringIO
data_io = StringIO(data)
df_io = pd.read_csv(data_io, sep=delim)

# Split
headings = data.split('\n')[:1][0].split(delim)
rows = [row.split(delim) for row in data.split('\n')[1:]]
df_split = pd.DataFrame(rows, columns=headings)
```

## Data Structures

pandas has two data structures:

1. `Series` - 1D labeled homogeneously-typed array
2. `DataFrame` - General 2D labeled, size-mutable tabular structure with potentially heterogeneously-typed column

Think of pandas data structures as flexible containers for lower dimensional data. For example, a `DataFrame` is a container for `Series`, and a `Series` is a container for scalars. The idea is to be able to insert/remove objects from the containers in a dictionary-like fashion.

Most methods produce _new_ objects and leave the input data untouched.

pandas objects have many attributes that enable you to access their metadata. Usually, these attributes can be assigned to.

### Series

A `Series` object wraps two components:

1. A sequence of _values_
2. A sequence of _identifiers_ (which is the index)

Values and identifiers can be obtained by `.to_numpy()` and the `.index` attribute, respectively.

`.to_numpy()` returns the values in the `Series` as a NumPy array (`numpy.ndarray`).

`.to_numpy()` gives some control over the `dtype` of the resulting `numpy.ndarray`. For example, NumPy does not have a `dtype` to represent timezone-aware datetimes, so there are two possible ways to represent them:

1. An `object`-type `numpy.ndarray` with `Timestamp` objects, each with the correct timezone.
2. A `datetime64[ns]`-`dtype` `numpy.ndarray` where the values have been converted to UTC and the timezone discarded.

Timezones can be preserved or discarded by passing `dtype=object` or `dtype='datetime64[ns]'` to `.to_numpy()`, respectively.

<https://pandas.pydata.org/docs/reference/api/pandas.Series.to_numpy.html>

`.index` returns the _positional index_ as a `RangeIndex`. It is an integer index that is _implicitly_ defined.

A `Series` can also have an arbitrary type of index, i.e., a _label index_ (a label can also be a number). This index is _in addition to_ the implicit positional index.

- For a `Series` with only a positional index, the `.index` attribute returns a `RangeIndex`.
- For a `Series` with a label index, the `.index` attribute returns an `Index` of the labels.

Python's `in` membership operator can be used with `.index` for both types of `Series` to check if an index is in the `Series`.

<https://pandas.pydata.org/docs/reference/api/pandas.Series.index>

The `.dtype` attribute returns the data type of the `Series`.

<https://pandas.pydata.org/docs/reference/api/pandas.Series.dtype.html>

Optionally, a name can be given to a `Series` via the `.name` attribute.

<https://pandas.pydata.org/docs/reference/api/pandas.Series.name.html>

An important difference between `Series` and `numpy.ndarray` is that `Series` operations _automatically align_ data based on label. The result of operations between unaligned `Series` will have the **union** of the involved indices. If a label is not found in one `Series` or the other, the result is marked as missing (`NaN`, _not a number_):

```python
ser = pd.Series({'a': 0.0, 'b': 1.0, 'c': 2.0, 'd': 3.0}, name='align')

ser[1:] + ser[:-1]
```

```pycon
a    NaN
b    2.0
c    4.0
d    NaN
Name: align, dtype: float64
```

<https://pandas.pydata.org/docs/reference/api/pandas.Series.html>

#### Row Data

Access row data with the `.iloc[]` and `.loc[]` indexers.

- `.iloc[]` uses the positional index and supports _negative indexing_.

    Allowed inputs are:

    - An integer
    - A list or array of integers
    - A slice object with integers
    - A boolean array
    - A `callable` function with one argument (the calling `Series`) that returns valid output for indexing (one of the above)
    - A tuple of row/column indices, where the tuple elements are one of the above inputs

- `.loc[]` uses the label index.

    Allowed inputs are:

    - A single label (number labels are entered as integers, not strings, e.g., `2` instead of `'2'`)
    - A list or array of labels
    - A slice object with labels
    - A boolean array of the same length as the axis being sliced
    - An alignable boolean `Series`, where the index of the key will be aligned before masking
    - An alignable Index, where the index of the returned selection will be the input
    - A `callable` function with one argument (the calling `Series`) and that returns valid output for indexing (one of the above)

Regarding _slicing_:

- `.iloc[]` excludes the closing element.
- `.loc[]` _includes_ the closing element.

New values can be assigned to the selected data.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.iloc.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Series.loc.html>

`.loc[]` can also be used to add a row to a `Series`/`DataFrame`:

`df.loc[12, :] = ('gamma', 'qux', '2022-07-28 13:45:07.438016528', -1.32, 7)`

When you only need to access a _single value_, use `.iat[]` and `.at[]`, instead.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.iat.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Series.at.html>

### DataFrame

`.to_numpy()` returns the values in the `DataFrame` as a 2-dimensional NumPy array (`numpy.ndarray`), where each internal array represents a `DataFrame` row.

If a `DataFrame` contains homogeneously-typed data, the `numpy.ndarray` can be modified in-place and the changes will be reflected in the data structure.

If a `DataFrame` contains heterogenous data, the `dtype` of the resulting `numpy.ndarray` is chosen to accommodate all of the data involved. If the `DataFrame` contains strings, this usually means that the result is of the `object` `dtype`. If the `DataFrame` contains only integers and floats, the result is of the `float` `dtype`.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_numpy.html>

`.axes` returns the row and column _indices_ as a list. The axis marked with `0` (`df.axes[0]`) is the _row index_ and the axis marked with `1` (`df.axes[1]`) is the _column index_. Several `DataFrame` methods accept an `axis` parameter that refers to these indices.

A row action is broadcast across rows in each column. A column action is broadcast across columns in each row.

<https://pandas.pydata.org/docs/user_guide/basics.html#matching-broadcasting-behavior>

- For a `DataFrame` with only a positional index, `.axes[0]` returns a `RangeIndex`.
- For a `DataFrame` with a label index, `.axes[0]` returns an `Index` of the labels.
- `.axes[1]` returns an `Index` of the _column_ names.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.axes.html>

For a `DataFrame`, the `.columns` attribute returns an `Index` of the column names. 

Python's `in` membership operator can be used with `.columns` to check if a column name is in a `DataFrame`.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.columns.html>

The `.dtypes` attribute returns the data types of the `DataFrame`'s columns.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.dtypes.html>

A DataFrame can be transposed via the `.T` attribute.

`DataFrame` objects automatically align on _both the columns and the index (row labels)_. The resulting object has the **union** of the column and row labels.

When doing an operation between a `Series` and `DataFrame`, the default behavior is to align the `Series` index on the `DataFrame` columns, i.e., [broadcasting](https://numpy.org/doc/stable/user/basics.broadcasting.html) row-wise.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html>

#### Column Data

A `DataFrame` is like a dictionary whose values are `Series`. Access column data with the _indexing operator_ (`[]`) and column name:

`df['ex_col']`

If a column label is a valid Python variable name, it can also be accessed like an attribute (and can be tab-completed via the IPython completion mechanism):

`df.ex_col`

Add a column with `.assign()` or the assignment operator:

```python
df.assign(c=df['a'] + df['b'])
# Or
df['c'] = df['a'] + df['b']
```

`.assign()` can be used to simultaneously add multiple columns to a `DataFrame`. 

Passing a callable to `.assign()` is useful when you [do not have a reference](https://pandas.pydata.org/docs/reference/api/pandas.Index.insert.html?highlight=insert#pandas.Index.insert) to the `DataFrame` at hand. The function is computed on the `DataFrame` being assigned to (e.g., a filtered `DataFrame`).

The function signature for `.assign()` is `**kwargs`, which allows for dependent assignment, i.e., expressions later in `**kwargs` can refer to a column created earlier in the _same_ `.assign()` call.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.assign.html>

A column can be added to a specific location with `.insert()`:

- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.insert.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.insert.html>

#### Row Data

Access row data with the `.iloc[]` and `.loc[]` indexers.

For a `DataFrame`, `.iloc[]` and `.loc[]` accept a second parameter, which selects a column or list of columns.

- `.iloc[]`'s second parameter uses column positional indices.
- `.loc[]`'s second parameter uses column names.

Together, the `.iloc[]` and `.loc[]` arguments can be used to select a subset of rows and columns from a `DataFrame`.

Both `.iloc[]` and `.loc[]` can also target rows using [Boolean indexing](https://pandas.pydata.org/docs/user_guide/indexing.html#boolean-indexing), and `.loc[]` also supports conditionals:

- `df.loc[df['ex_col'] > 10]`
- `df.loc[df['ex_col'].notnull()]`
- `df.loc[df['ex_col'].str.endswith('.dev')]` (where `df['ex_col']` is a column with `object` data type values)

Multiple `.loc[]` conditions can be combined by placing each condition in parentheses and using the Bitwise AND (`&`) and Bitwise OR (`|`) operators:

```python
df.loc[
    (ex_cnd_1)
    & (ex_cnd_2)
    & (ex_cnd_3)
    | (ex_cnd_4)
]
```

New values can be assigned to the selected data.

- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.iloc.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.loc.html>

When you only need to access a single value, use `.iat[]` and `.at[]`, instead.

- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.iat.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.at.html>

#### Selection

The type of object returned from a `DataFrame` selection depends on the type of selection that was made.

<table>
  <thead>
    <tr>
      <th>Operation</th>
      <th>Syntax</th>
      <th>Result</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Select column</td>
      <td><code>df[ex_col]</code></td>
      <td><code>Series</code></td>
    </tr>
    <tr>
      <td>Select row by label</td>
      <td><code>df.loc[ex_lbl]</code></td>
      <td><code>Series</code></td>
    </tr>
    <tr>
      <td>Select row by integer location</td>
      <td><code>df.iloc[ex_loc]</code></td>
      <td><code>Series</code></td>
    </tr>
    <tr>
      <td>Slice rows</td>
      <td><code>df[ex_st:ex_end]</code></td>
      <td><code>DataFrame</code></td>
    </tr>
    <tr>
      <td>Select rows by boolean vector</td>
      <td><code>df[ex_bool_vec]</code></td>
      <td><code>DataFrame</code></td>
    </tr>
  </tbody>
</table>

For row selection that returns a `Series`, the index is the column names of the `DataFrame`.

### Creation

`Series` and `DataFrame`s can be made from different object types. Most simply, lists and dictionaries are used to create `Series` and `DataFrame`s, respectively:

```python
nums = [1, 2, 3]
ser = pd.Series(nums, name='numbers')
```

If creating a `Series` from a dictionary, the dictionary keys become the `Series`' label indices:

`pd.Series({'a': 0.0, 'b': 1.0, 'c': 2.0}, name='numbers')`

```pycon
a    0.0
b    1.0
c    2.0
Name: numbers, dtype: float64
```

If creating a `Series` from a scalar value, an index must be provided:

`ser = pd.Series(4.0, index=['a', 'b', 'c', 'd'], name='constant')`

<https://pandas.pydata.org/docs/reference/api/pandas.Series.html>

```python
# States are column names
state_cities = {
    'CA': ['Santa Cruz', 'San Jose', 'Petaluma'],
    'OR': ['Florence', 'Eugene', 'Portland'],
    'WA': ['Seattle', 'Redmond', 'Bellingham']
}
df = pd.DataFrame(state_cities)
```

```python
# States are label indices
state_cities = {
    'city_one': {'CA': 'Santa Cruz', 'OR': 'Eugene', 'WA': 'Seattle'},
    'city_two': {'CA': 'Petaluma', 'OR': 'Florence', 'WA': 'Bellingham'},
    'city_three': {'CA': 'San Jose', 'OR': 'Portland', 'WA': 'Redmond'}
}
df = pd.DataFrame(state_cities)
```

When creating a DataFrame with a single row (e.g., when preparing to use `.concat()` to add a row to an extant `DataFrame`), an alternative approach may be preferable:

```python
new_row = pd.DataFrame(['Sacramento', 'Salem', 'Olympia'],
                       columns=['CA', 'OR', 'WA'])
```

A MultiIndexed `DataFrame` can be created with a tuples dictionary:

```python
data = {
    'a': {('A', 'B'): 1, ('A', 'C'): 2, ('A', 'D'): 3},
    'b': {('A', 'B'): 4, ('A', 'C'): 5, ('A', 'D'): 6},
    'c': {('A', 'B'): 7, ('A', 'C'): 8, ('A', 'D'): 9},
    'd': {('A', 'B'): 10, ('A', 'C'): 11, ('A', 'D'): 12},
    'e': {('A', 'B'): 13, ('A', 'C'): 14, ('A', 'D'): 15}
}
df = pd.DataFrame(data)
```

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html>

An `Index` can be made from a list using `pandas.Index()`.

pandas supports non-unique index values. If an operation does not support duplicate index values, an exception is raised.

<https://pandas.pydata.org/docs/reference/api/pandas.Index.html>

A `MultiIndex` can be made from tuples using `pandas.MultiIndex()`:

```python
tuples = list(
    zip(
        ["bar", "bar", "baz", "baz", "foo", "foo", "qux", "qux"],
        ["one", "two", "one", "two", "one", "two", "one", "two"],
    )
)
index = pd.MultiIndex.from_tuples(tuples, names=['idx_1', 'idx_2'])
```

<https://pandas.pydata.org/docs/reference/api/pandas.MultiIndex.html>

## Data Types

pandas uses NumPy [arrays](https://numpy.org/doc/stable/user/basics.creation.html) and [data types](https://numpy.org/doc/stable/user/basics.types.html) for `Series` or individual columns of a `DataFrame`.  NumPy provides support for `float`, `int`, `bool`, `timedelta64[ns]`, and `datetime64[ns]`.

<table>
  <caption>pandas <code>dtype</code> Mappings</caption>
  <thead>
    <tr>
        <th>pandas <code>dtype</code></th>
        <th>NumPy Type</th>
        <th>Python Type</th>
        <th>Usage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>bool</code></td>
      <td><code>bool_</code></td>
      <td><code>bool</code></td>
      <td><code>True</code> and <code>False</code> values</td>
    </tr>
    <tr>
      <td><code>category</code></td>
      <td>N/A</td>
      <td>N/A</td>
      <td>Finite list of text values</td>
    </tr>
    <tr>
      <td><code>datetime64[ns]</code></td>
      <td><code>datetime64[ns]</code></td>
      <td>N/A</td>
      <td>Date and time values</td>
    </tr>
    <tr>
      <td><code>float64</code></td>
      <td><code>float_</code>, <code>float16</code>, <code>float32</code>, <code>float64</code></td>
      <td><code>float</code></td>
      <td>Floating point numbers</td>
    </tr>
    <tr>
      <td><code>int64</code></td>
      <td><code>int_</code>, <code>int8</code>, <code>int16</code>, <code>int32</code>, <code>int64</code>, <code>uint8</code>, <code>uint16</code>, <code>uint32</code>, <code>uint64</code></td>
      <td><code>int</code></td>
      <td>Integer numbers</td>
    </tr>
    <tr>
      <td><code>object</code></td>
      <td><code>string_</code>, <code>unicode_</code>, mixed types</td>
      <td><code>str</code> or mixed</td>
      <td>Text or mixed numeric and non-numeric values</td>
    </tr>
    <tr>
      <td><code>timedelta[ns]</code></td>
      <td>N/A</td>
      <td>N/A</td>
      <td>Differences between two datetimes</td>
    </tr>
  </tbody>
</table>

Numeric `dtypes` propagate and can coexist in `DataFrame`s. If a `dtype` is passed (either directly via the `dtype` parameter, a passed `ndarray`, or a passed `Series`), it will be preserved in `DataFrame` operations.

Different numeric `dtype`s will _not_ be combined.

By default, integer types are `int64` and float types are `float64`, regardless of platform (i.e., 32-bit or 64-bit). Note that NumPy will choose _platform-dependent_ types when creating arrays.

pandas and third-party libraries [extend](https://pandas.pydata.org/pandas-docs/stable/user_guide/basics.html#dtypes) NumPy’s type system in a few places.

<table>
  <caption>pandas Extension Types</caption>
  <thead>
    <tr>
      <th>Kind of Data</th>
      <th>Data Type</th>
      <th>Scalar</th>
      <th>Array</th>
      <th>String Aliases</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>tz-aware datetime</td>
      <td><code>DatetimeTZDtype</code></td>
      <td><code>Timestamp</code></td>
      <td><code>arrays.DatetimeArray</code></td>
      <td><code>datetime64[ns, &#60;tz&#62;]</code></td>
    </tr>
    <tr>
      <td>Categorical</td>
      <td><code>CategoricalDtype</code></td>
      <td>none</td>
      <td><code>Categorical</code></td>
      <td><code>category</code></td>
    </tr>
    <tr>
      <td>period (time spans)</td>
      <td><code>PeriodDtype</code></td>
      <td><code>Period</code></td>
      <td><code>arrays.PeriodArray</code>, <code>Period[&#60;freq&#62;]</code></td>
      <td><code>period[&#60;freq&#62;]</code></td>
    </tr>
    <tr>
      <td>sparse</td>
      <td><code>SparseDtype</code></td>
      <td>none</td>
      <td><code>arrays.SparseArray</code></td>
      <td><code>Sparse</code>, <code>Sparse[int]</code>, <code>Sparse[float]</code></td>
    </tr>
    <tr>
      <td>intervals</td>
      <td><code>IntervalDtype</code></td>
      <td><code>Interval</code></td>
      <td><code>arrays.IntervalArray</code></td>
      <td><code>interval</code>, <code>Interval</code>, <code>Interval[&#60;numpy_dtype&#62;]</code>, <code>Interval[datetime64[ns, &#60;tz&#62;]]</code>, <code>Interval[timedelta64[&#60;freq&#62;]]</code></td>
    </tr>
    <tr>
      <td>nullable integer</td>
      <td><code>Int64Dtype</code>, ...</td>
      <td>none</td>
      <td><code>arrays.IntegerArray</code></td>
      <td><code>Int8</code>, <code>Int16</code>, <code>Int32</code>, <code>Int64</code>, <code>UInt8</code>, <code>UInt16</code>, <code>UInt32</code>, <code>UInt64</code></td>
    </tr>
    <tr>
      <td>Strings</td>
      <td><code>StringDtype</code></td>
      <td><code>str</code></td>
      <td><code>arrays.StringArray</code></td>
      <td><code>string</code></td>
    </tr>
    <tr>
      <td>Boolean (with NA)</td>
      <td><code>BooleanDtype</code></td>
      <td><code>bool</code></td>
      <td><code>arrays.BooleanArray</code></td>
      <td><code>boolean</code></td>
    </tr>
  </tbody>
</table>

For example, pandas has a `category` data type.

<https://pandas.pydata.org/docs/user_guide/categorical.html>

The `category` data type is useful when:

- There is a string variable that only has a few different values. Converting this variable to `category` can save memory.
- The lexical order of a variable is not the same as the logical order (e.g., 'mercury', 'venus', 'earth'). By converting to a `category` data type and specifying a category order, sorting and min/max will use the logical order.
- You want to signal to other Python libraries that a column should be treated as a categorical variable (e.g., to use appropriate statistical methods or plot types).

`.cat.rename_categories()` is used to rename categories.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.cat.rename_categories.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.CategoricalIndex.rename_categories.html>

`.cat.set_categories()` sets the categories to the new specified categories. It can be used to simultaneously set the category as ordered or unordered (e.g., `ordered=True`), reorder the categories, and add missing categories.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.cat.set_categories.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.CategoricalIndex.set_categories.html>

A category can also be set as ordered or unordered via the `.as_ordered()` and `.as_unordered()` methods, respectively:

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.cat.as_ordered.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.CategoricalIndex.as_ordered.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Series.cat.as_unordered.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.CategoricalIndex.as_unordered.html>

Sorting of categories is done per order in the categories, _not_ in lexical order.

Grouping by a categorical column also includes empty categories.

pandas has two ways of storing strings:

1. `object` `dtype`, which can hold any Python object.
2. `StringDtype`, which is dedicated to strings.

Generally, `StringDtype` is recommended.

Arbitrary objects can be stored using the `objects` `dtype`, but it should be avoided, if possible, due to performance and interoperability issues with other libraries/methods.

### Conversion

Types can be potentially _upcasted_ when combined with other types, i.e., they are promoted from the current type to the other type (e.g., `int` to `float`).

`DataFrame.to_numpy()` returns the _lower-common-denominator_ of the `dtype`s, i.e., the `dtype` that can accommodate all of the types in the resulting homogenous dtyped NumPy array. This can force some upcasting.

`.astype()` explicitly converts `dtype`s from one to another. By default, copies are returned, even if the `dtype` was unchanged (use `copy=False` to change this). If the `.astype()` operation is invalid, an exception is raised.

`.astype()` upcasting is always done according to NumPy rules, i.e., if two different `dtype`s are involved in an operation, the more general one will be used as the result of the operation.

Specific columns can be simultaneously converted to distinct data types by passing a dictionary to `.astype()`:

`df = df.astype({'a': np.bool_, 'b': np.float64})`

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.astype.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.astype.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.astype.html>

pandas has various functions that try to force conversion of types from the `object` `dtype` to other types. If data is already of the correct type, but stored in an `object` array (e.g., after a transposition), `.infer_objects()` can be used to soft convert to the correct type.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.infer_objects.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.infer_objects.html>

Additional functions are available for one dimensional object arrays or scalars to perform hard conversion of objects to a specified type:

- [`pandas.to_numeric()`](https://pandas.pydata.org/docs/reference/api/pandas.to_numeric.html) converts to numeric `dtype`s.
- [`pandas.to_datetime()`](https://pandas.pydata.org/docs/reference/api/pandas.to_datetime.html) converts to datetime objects.
- [`pandas.to_timedelta()`](https://pandas.pydata.org/docs/reference/api/pandas.to_timedelta.html) converts to timedelta objects.

To force a conversion, utilize the `errors` parameter, which tells pandas how it should deal with elements that cannot be converted to the desired `dtype` or object.

- By default, `errors='raise'` will raise any errors encountered during the conversion process.
- `errors='coerce'` will ignore errors and convert problematic elements to `pd.NaT` (for datetime and timedelta) or `np.nan` (for numeric).
- `errors='ignore'` simply returns the passed in data if it encounters any errors with the conversion.

`pandas.to_numeric()` has another parameter, `downcast`, which provides the option of downcasting the newly (or already) numeric data to a smaller `dtype`, which can conserve memory.

The above hard conversion functions only apply to one-dimensional arrays, lists, or scalars, so they cannot be directly used on multi-dimensional objects like `DataFrame`s. However, you can use `.apply()` to efficiently apply these functions to each column:

`df.apply(pd.to_numeric)`

## Missing Data

pandas uses [different sentinel values](https://pandas.pydata.org/pandas-docs/stable/user_guide/missing_data.html) to represent missing data. The sentinel value used depends on the data type:

- `np.nan` for NumPy data types. `np.nan` values are of the `float` type and are _not_ included in computations.
- `pd.NaT` for `np.datetime64`, `np.timedelta64`, and `pd.PeriodDtype`.
- `pd.NA` for `pd.StringDtype`. `pd.Int64Dtype`, `pd.Float64Dtype`, `pd.BooleanDtype`, and `pd.ArrowDtype`.

`.isna()` is a conditional method that returns `True` where the values are NA (_not available_):

```python
nulls_df = df.loc[df['a'].isna()]

nulls_df = df.loc[df.isna().any(axis=1)]
```

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.isna.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.isna.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.isna.html>

`.notna()` is a conditional method that returns `True` where the values are _not_ NA:

```python
no_nulls_df = df.loc[df['a'].notna()]

no_nulls_df = df.loc[df.notna().all(axis=1)]
```

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.notna.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.notna.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.notna.html>

`.dropna()` returns a new object with missing values removed.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.dropna.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.dropna.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.dropna.html>

`.fillna()` fills missing values.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.fillna.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.fillna.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.fillna.html>

## Arithmetic and Logic

Mathematical and logical operator calculations are done element-wise in a _vectorized fashion_, i.e., all values in a column are operated on at once. Iterating through each row using a loop is unnecessary.

Flexible wrappers for arthimetic operators `+`, `-`, `*`, `/`, `%`, and `**` are `.add()`, `.sub()`, `.mul()`, `.div()`, `.mod()`, and `.pow()`, respectively, and include support to substitute a fill value for missing data in one of the inputs.

`Series` and `DataFrame` objects have the binary comparison methods, `.eq()`, `.ne()`, `.lt()`, `.gt()`, `.le()`, and `.ge()`. These operations produce a pandas object of the same type as the left-hand side input that is of the data type `bool`, which can be used in indexing operations.

For calculations with more advanced logic, use `.apply()`.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.apply.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.apply.html>

To evaluate a single-element pandas object in a boolean context, use `.bool()`.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.bool.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.bool.html>

The `.empty` attribute can be used to test if a pandas object is empty.

Keep in mind, tests like the following will produce errors because you are _trying to compare multiple values_:

```python
if df:
    pass
```

`df and df_2`

To address this, [explicitly choose what you want to do](https://pandas.pydata.org/docs/user_guide/gotchas.html#using-if-truth-statements-with-pandas) using `.empty`, `.any()`, or `.all()`. Alternatively, test if the pandas object is `None`.

Remember that `NaN`s do _not_ compare as equals. For example, if `df` contains `NaN` values, `(df + df == df * 2).all()` will produce a `Series` with `False` values.

Instead, use `.equals()` to test for equality. However, in order for equality to be `True`, object indices need to be in the _same order_.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.equals.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.equals.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.equals.html>

Element-wise comparisons can be done between array-like objects and a scalar value, and between array-like objects of the same length.

If the equality check should not include row/column indices, use `numpy.array_equal()`:

<https://numpy.org/doc/stable/reference/generated/numpy.array_equal.html>

To check for equality, but return the boolean result on an element-by-element basis, use `.eq()`:

- <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.eq.html>
- <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.eq.html>

To compare a `Series` or `DataFrame` to another, use `.compare()`:

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.compare.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.compare.html>

## Filtering Data

Filter column/row _labels_ with `.filter()`. Regular expressions are supported.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.filter.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.filter.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.filter.html>

`.isin()` is a conditional method that returns `True` for each row with a value in the provided list.

`fltr_df = df.loc[df['a'].isin([1, 2])]`

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.isin.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.isin.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.isin.html>

`.nlargest()` returns the first `n` rows with the largest values in the specified column(s) in descending order.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.nlargest.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.nlargest.html>

`.nsmallest()` returns the first `n` rows with the smallest values in the specified column(s) in ascending order.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.nsmallest.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.nsmallest.html>

`.between()` returns a boolean vector containing `True` wherever the corresponding `Series` element is between the boundary values `left` and `right`.

<https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.between.html>

`.rank()` can be used to find the top `n` rows per group:

```python
df.assign(
    rank=df.groupby(['state'])['city_pop']
           .rank(method='first', ascending=False)
).query('rank < 4').sort_values(['state', 'rank'])
```

<https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.rank.html>

`.any()` returns whether _any_ element is `True`.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.any.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.any.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.any.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.any.html>

`.all()` returns whether _all_ elements are `True`.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.all.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.all.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.all.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.all.html>

`.nunique()` returns the number of unique elements in an object:

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.nunique.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.nunique.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.nunique.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.nunique.html>

### Updating Data

Filter and assign:

```python
row_cond = (df['state'] == 'CA') & (df['city'] == 'Santa Cruz')
df.loc[row_cond, 'city'] = 63000
```

### Deleting Data

Select the rows that should _remain_ instead of deleting the rows that should be removed.

`df = df.loc[df['city_pop'] <= 200000]`

Also, `.drop()` can be used to drop rows and columns, and to drop indices from MultiIndex DataFrames.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.drop.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.drop.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.drop.html>

### If/Then Logic

A conditional formula can be achieved using NumPy's `.where()`:

```python
# Create new column with city size label based on population
df['city_size'] = np.where(df['city_pop'] < 200000, 'small', 'large')
```

<https://numpy.org/doc/stable/reference/generated/numpy.where.html>

If multiple conditions are required, NumPy's `.select()` can help:

```python
conditions = [
    ((df['city_pop'] >= 200000) & (df['city_pop'] < 400000)),
    (df['city_pop'] >= 400000)
]
choices = [
    'medium',
    'large'
]

df['city_size'] = np.select(conditions, choices, default='small')
```

<https://numpy.org/doc/stable/reference/generated/numpy.select.html>

### Selecting Columns Based On `dtype`

`DataFrame.select_dtypes()` implements subsetting of columns based on their `dtype`. It has two parameters, `include` and `exclude`, which allow you to specify which columns with these `dtype`s that you would or would not like returned, respectively.

Names passed to `include` and `exclude` can be `dtype` names in the [NumPy `dtype` hierarchy](https://numpy.org/doc/stable/reference/arrays.scalars.html) or generic `dtype`s (e.g., `number`).

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.select_dtypes.html>

To see all child `dtype`s of a generic `dtype`, you can use a recursive function that returns a tree of child `dtype`s:

```python
def subdtypes(data_type):
    subs = data_type.__subclasses__()
    if not subs:
        return data_type
    return [data_type, [subdtypes(d_type) for d_type in subs]]
```

All NumPy `dtype`s are subclasses of `numpy.generic`:

```pycon
[numpy.generic,
 [[numpy.number,
   [[numpy.integer,
     [[numpy.signedinteger,
       [numpy.int8,
        numpy.int16,
        numpy.int32,
        numpy.int64,
        numpy.longlong,
        numpy.timedelta64]],
      [numpy.unsignedinteger,
       [numpy.uint8,
        numpy.uint16,
        numpy.uint32,
        numpy.uint64,
        numpy.ulonglong]]]],
    [numpy.inexact,
     [[numpy.floating,
       [numpy.float16, numpy.float32, numpy.float64, numpy.float128]],
      [numpy.complexfloating,
       [numpy.complex64, numpy.complex128, numpy.complex256]]]]]],
  [numpy.flexible,
   [[numpy.character, [numpy.bytes_, numpy.str_]],
    [numpy.void, [numpy.record]]]],
  numpy.bool_,
  numpy.datetime64,
  numpy.object_]]
```

## Combining Data

With pandas, you can merge and concatenate `DataFrame` and named `Series` objects.

<https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html>

### `.merge()`

`.merge()` provides [join operations](https://pandas.pydata.org/pandas-docs/stable/getting_started/comparison/comparison_with_sql.html#compare-with-sql-join) that are similar to SQL relational database `JOIN`s. It _combines rows on common columns or indices_.

`pandas.merge(df_left, df_right, on=ex_keys)`

When using `.merge()`, you can expect the resulting `DataFrame` to have rows from the parent objects mixed in together, based on a commonality. Depending on the type of merge that is performed, rows that do not have matches in the other object may be lost.

- When joining columns on columns, object indices are ignored.
- When joining indices on indices, or indices on a column/columns, the index is passed on.
- When performing a cross merge (i.e., a `CROSS JOIN`), no column specifications to merge on are allowed.

By default, the `how` parameter is set to do an inner join, but other joins are supported:

<table>
  <caption><code>how</code> Parameter Value to SQL <code>JOIN</code> Mapping</caption>
  <thead>
    <tr>
      <th><code>how</code> Value</th>
      <th>SQL <code>JOIN</code> Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>left</code></td>
      <td><code>LEFT OUTER JOIN</code></td>
      <td>Use keys from left object only</td>
    </tr>
    <tr>
      <td><code>right</code></td>
      <td><code>RIGHT OUTER JOIN</code></td>
      <td>Use keys from right object only</td>
    </tr>
    <tr>
      <td><code>outer</code></td>
      <td><code>FULL OUTER JOIN</code></td>
      <td>Use union of keys from both objects</td>
    </tr>
    <tr>
      <td><code>inner</code></td>
      <td><code>INNER JOIN</code></td>
      <td>Use intersection of keys from both objects</td>
    </tr>
    <tr>
      <td><code>cross</code></td>
      <td><code>CROSS JOIN</code></td>
      <td>Create the cartesian product of rows of both objects</td>
    </tr>
  </tbody>
</table>

Other optional arguments of interest include:

- `on` - The columns (key columns) or indices (key indices) to join on. By default, columns from the two objects that share names are used as join keys.

    If you use `on`, the columns or indices that you specify must be present in both objects. It is probably safest to always merge data using `on`.

- `left_on`, `right_on` - The columns or indices to join on that are only present in the left and right object, respectively. Defaults to `None`.

    This is useful when the key columns in the left and right objects have different names.

- `left_index`, `right_index` - Use the index from the left and right object, respectively, as the join key. Defaults to `False`.

    If the index is a MultiIndex, the number of keys in the other object (either index or number of columns) must match the number of levels.

- `suffixes` - A length-2 sequence where each element is a string indicating the suffix to add to overlapping column names in the left and right object, respectively (e.g., `('_x', '_y')`). This enables you to keep track of column origins when they share the same name.

<https://pandas.pydata.org/docs/reference/api/pandas.merge.html>

### `pandas.concat()`

`pandas.concat()` concatenates objects along an axis, while performing optional set logic (union or intersection) of the indices (if any) on the other axes. Objects are _stitched together_ along either the row axis (`axis=0`, the default) or the column axis (`axis=1`).

`pandas.concat([df1, df2])`

Essentially, concatenation is a simpler way to combine objects and is often used to form a single larger data set from smaller data sets. A `pandas.concat()` action is analogous to a `UNION ALL` SQL operation, or a `UNION` SQL operation when `.drop_duplicates()` is also used.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.drop_duplicates.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.drop_duplicates.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.drop_duplicates.html>

_Keep in mind_:

- When concatenating along the row axis, if the column names in the objects are not the same, the columns are also added to the new `DataFrame`. `NaN` values are used to fill missing values, as applicable.
- When concatenating along the column axis, if the object indices are not the same, the extra indices (rows) are also added to the new `DataFrame`. `NaN` values are used to fill missing values, as applicable.

The `join` parameter is used to define how to handle indices for the axis that is _not_ used for the concatenation. By default, it is set to `outer`, which results in a [set union](https://en.wikipedia.org/wiki/Union_(set_theory)#Union_of_two_sets) (i.e., no duplicate indices), where all data is preserved.

Setting `join` to `inner` results in a [set intersection](https://en.wikipedia.org/wiki/Intersection_(set_theory)#Definition) (i.e., row/column data will only be preserved where axis labels match).

Other optional arguments of interest include:

- `ignore_index` - Defaults to `False`. If set to `True`, the new `DataFrame` is given a new index.

    This is useful if you are concatenating objects where the concatenation axis does not have meaningful indexing information.

- `keys` - Construct a hierarchical index using the passed keys as the outermost level. If multiple levels are passed, this should contain tuples.

    This enables you to create a new index, while preserving the original indices, so that you can determine which rows come from which original object.

Adding columns to a `DataFrame` is relatively fast. Adding rows requires a copy and may be expensive. When building a `DataFrame`, it is preferable to pass a pre-built list of records instead of iteratively appending records to it.

<https://pandas.pydata.org/docs/reference/api/pandas.concat.html>

### `.combine_first()`

`.combine_first()` combines two objects where missing values in one object are conditionally filled with like-labeled values from the other object.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.combine_first.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.combine_first.html>

`.combine_first()` utilizes `.combine()`.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.combine.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.combine.html>

## Reshaping Data

`Series.sort_values()` sorts a `Series` by its values.

<https://pandas.pydata.org/docs/reference/api/pandas.Series.sort_values.html>

`DataFrame.sort_values()` sorts a `DataFrame` by its column or row values. The optional `by` parameter may be used to specify one or more columns to use to determine the sort order, and its value can refer to either column or index level names (if a string matches both a column name and index level name, the column takes precedence).

When a column is a `MultiIndex`, you must be explicit about sorting and fully specify all levels to the `by` parameter:

`df.sort_values(by=('a', 'two'))`

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.sort_values.html>

For these methods, the index follows the row order, and treatment of NA values can be specified via the `na_position` parameter.

Also, `.sort_values()`'s `key` parameter can take a callable function to apply to the values _in-memory before being sorted_. For `Series`, `key` is given the `Series` of objects and returns a `Series` or array of the same shape with the sorted values. For `DataFrame`s, `key` is applied per column, so the key still expects a `Series` and returns a `Series`. The name or type of each column can be used to apply different functions to different columns.

`Index.sort_values()` returns a sorted copy of an index.

<https://pandas.pydata.org/docs/reference/api/pandas.Index.sort_values.html>

`DataFrame.sort_index()` returns a `DataFrame` sorted by index. Its `key` parameter takes a callable function to apply to the index being sorted. For `MultiIndex` objects, the key is applied per-level to the levels specified by the `level` parameter.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.sort_index.html>

`Series.searchsorted()` returns the indices where specified elements should be inserted to maintain order.

<https://pandas.pydata.org/docs/reference/api/pandas.Series.searchsorted.html>

`DataFrame.pivot()` reshapes data from a _long_ to _wide_ format. A single value for each index/column combination is required.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.pivot.html>

When multiple values need to be aggregated, `DataFrame.pivot_table()` can be used with an aggregation function that determines how to combine the values. When interested in the row/column subtotals/grand totals, set the `margins` parameter to `True`.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.pivot_table.html>

Using `.groupby()` with an aggregating function is an alternative to `.pivot_table()`. `.groupby()` is especially useful when additional computation or plotting is required.

`.reset_index()` resets an index. Resetting an index can be helpful after combining or before unpivoting data.

Also, using the `level` parameter, any level of an index can be converted to a column. This is useful when dealing with a `MultiIndex`.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.reset_index.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.reset_index.html>

`.stack()` [compresses a level](https://pandas.pydata.org/docs/user_guide/10min.html#stack) in a DataFrame's columns.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.stack.html>

`.unstack()` performs the inverse operation of `.stack()`. By default, it unstacks the last level.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.unstack.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.unstack.html>

`.melt()` reshapes data from a [_wide_ format to a _long_ format](https://pandas.pydata.org/docs/getting_started/intro_tutorials/07_reshape_table_layout.html#wide-to-long-format). The column headers become the variable names in newly created columns. All columns _not_ mentioned in the `id_vars` parameter are melted into two columns:

1. A column with the column header names (`variable`).
2. A column with the values themselves (`value`).

Parameters can be passed to `.melt()` to customize the returned, unpivoted `DataFrame`:

- `value_vars` defines which columns to melt together.
- `value_name` provides a custom column name for the values column instead of the default column name of `value`.
- `var_name` provides a custom column name for the column collecting the column header names. Otherwise, the index name or default column name of `variable` is used.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.melt.html>

## Copying

`.copy()` copies the underlying data (except the axis indices, since they are immutable) and returns a new object. _It is seldom necessary to copy objects._

For example, there are only several ways to alter a `DataFrame` in-place:

- Inserting, deleting, or modifying a column
- Assigning to the `.index` or `.columns` attributes
- For homogeneous data, directly modifying the values via the `.values` attribute or advanced indexing

No pandas method has the side effect of modifying data. Almost every method returns a _new_ object, leaving the original object unchanged. If data is modified, it is because you did so explicitly.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.copy.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.copy.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.copy.html>

## Reindexing and Relabeling

`.reindex()` can be used to conform an object to a new index (with optional filling logic), or to create a new index.

Reindexing [accomplishes several things](https://pandas.pydata.org/docs/user_guide/basics.html#reindexing-and-altering-labels):

- Reorders the existing data to match a new set of labels
- Inserts missing value (NA) markers in label locations where no data for that label existed
- If specified, fills data for missing labels using logic

For a `DataFrame`, you can simultaneously reindex the index and columns.

`Index` objects containing the axis labels can be _shared between objects_.

`.reindex()`'s `method` parameter takes a [filling method](https://pandas.pydata.org/docs/user_guide/basics.html#filling-while-reindexing) that supports the following options:

<table>
  <thead>
    <tr>
      <th>Method</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>pad<code />/<code>ffill<code /></td>
      <td>Fill values forward</td>
    </tr>
    <tr>
      <td><code>bfill<code />/<code>backfill<code /></td>
      <td>Fill values backward</td>
    </tr>
    <tr>
      <td><code>nearest<code /></td>
      <td>Fill from the nearest index value</td>
    </tr>
  </tbody>
</table>

Except for `method='nearest'`, `.fillna()` and `.interpolate()` can be used to achieve the same results.

`.reindex()` requires that the indices are _ordered_, either in increasing or decreasing order. `.fillna()` and `.interpolate()` do not perform any checks on index order.

The `limit` and `tolerance` parameters provide additional control over filling behavior while reindexing. `limit` specifies the maximum count of consecutive matches, while `tolerance` specifies the maximum distance between the index and indexer values.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.reindex.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.reindex.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.reindex.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Series.interpolate.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.interpolate.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.core.resample.Resampler.interpolate.html>

`.reindex_like()` reindexes the axes of an object using the labels of another object.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.reindex_like.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.reindex_like.html>

`.align()` is the fastest way to simultaneously align two objects. It supports a `join` parameter:

- `join='outer'`: Take the union of the indices (default)
- `join='left'`: Use the call object's index
- `join='right'`: Use the passed object's index
- `join='inner'`: Intersect the indices

When applied to `Series`, `.align()` returns a tuple with both of the reindexed `Series`. When applied to `DataFrame`s, `.align()` returns a tuple of the reindexed `DataFrame`s, and the `join` method is applied to both the index and columns by default (the `axis` parameter can be used to only align on a specified axis).

If a `Series` is passed to `.DataFrame.align()`, you can choose to align both objects either on the `DataFrame`'s index or columns using the `axis` parameter.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.align.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.align.html>

`.rename()` renames both row and column labels (or renames a `Series`). A dictionary or `Series` can be used to provide the mapping between the current column name and the new column name. If the mapping does not include a column/index label, it is not renamed.

The current column name and new column name mapping is not restricted to fixed names, i.e., a mapping function can be used, as well. For example, the following converts all column names to lowercase:

`df = df.rename(columns=str.lower)`

Keep in mind, when passing a function, it must return a value when called with any of the labels and must produce a set of unique values.

Calling `Series.rename()` with a scalar value sets the `Series.name` attribute.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.rename.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.rename.html>

`.rename_axis()` allows specific _names_ of an axis for the index or columns to be changed (as opposed to the index _labels_).

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.rename_axis.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.rename_axis.html>

## Iteration

Iteration behavior in pandas depends on the type. When iterating over a `Series`, it is regarded as array-like and iteration produces the values. `DataFrame`s follow the dictionary-like convention of iterating over the keys (column labels) of the objects.

pandas objects have the dictionary-like `.items()` method to iterate over key/value pairs.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.items.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.items.html>

To iterate over `DataFrame` rows, you can use `DataFrame.iterrows()` or `DataFrame.itertuples()`.

`DataFrame.iterrows()` iterates over the rows of a `DataFrame` as index/`Series` pairs. This converts the rows to `Series` objects, which can result in `dtype` changes and has performance implications.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.iterrows.html>

`DataFrame.itertuples()` iterates over the rows of a `DataFrame` as named tuples of the values. This is much faster than `DataFrame.iterrows()` and is usually preferable.

<https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.itertuples.html>

Generally, iterating through pandas objects is slow. Many times, manually iterating over rows _is not needed_ and can be avoided with one of several approaches:

- Look for a _vectorized_ solution.

        import numpy as np  # Enable multi-dimensional arrays and matrices
        import pandas as pd  # Enable data structures and data analysis tools


        def crt_case_enc(word: str) -> str:
            '''
            Return a case encoding for a word.

            Parameters
            ----------------
            word : str
                Word to create a case encoding for.

            Returns
            ----------------
            str
                A word's case encoding.
            '''
            return ''.join(
                'u'
                if char.isupper()
                else
                'l'
                if char.islower()
                else
                'n'
                for char in word
            )


        data = {'word': ['As', 'sure', 'as', 'I', 'know', 'anything,',
                        'I', 'know', 'this.']}
        df = pd.DataFrame(data)


        crt_case_enc = np.vectorize(crt_case_enc)
        df['wrd_enc'] = crt_case_enc(df['word'])

- Use `.apply()`.
- Consider writing the inner loop with [Cython or Numba](https://pandas.pydata.org/docs/user_guide/enhancingperf.html).

_Never_ modify something that you are iterating over, as this is not guaranteed to work in all cases. Depending on the data types involved, the iterator returns a copy, not a view. Writing to it will have no effect.

## Working With Text

A number of specialized string methods are available when using the `.str` accessor (e.g., `.str.lower()`, `.str.upper()`). In general, these methods have matching names with the equivalent built-in string methods for single elements, but are applied element-wise on each of the values in columns.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.str.lower.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Series.str.upper.html>

The following example splits strings on a comma and selects the first substring in the resulting list as the value for a new column:

`df['city'] = df['city_state'].str.split(',').str.get(0)`

An alternative approach expands the `Series` of lists into a `DataFrame` where each hit is placed in its own cell and a positional index is set for the column labels:

`df['city'] = df['city_state'].str.split(',', expand=True)[0]`

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.str.split.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Series.str.get.html>

`.str.contains()` performs substring search and returns for each of the values `True` or `False`. This output can be used to target these rows using conditional indexing:

`df.loc[df['city_state'].str.contains('Eugene')]`

`.str.contains()` accepts regular expressions, as well.

<https://pandas.pydata.org/docs/reference/api/pandas.Series.str.contains.html>

`.str.extract()` captures _groups_ in a regular expression pattern as columns in a `DataFrame`:

`df['state'].str.extract(r'(O..g.n)')`

<https://pandas.pydata.org/docs/reference/api/pandas.Series.str.extract.html>

`.str.find()` returns the lowest index corresponding to the position where a substring is located for each string in a `Series`:

`df['state'].str.find('gon')`

<https://pandas.pydata.org/docs/reference/api/pandas.Series.str.find.html>

`.str.len()` returns a `Series` with the lengths of each string in a column:

`df['city_state'].str.len()`

<https://pandas.pydata.org/docs/reference/api/pandas.Series.str.len.html>

`.idxmin()` returns the index of the minimum value and can be used to display the row with the shortest length:

`df.loc[df['state'].str.len().idxmin(), 'state']`

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.idxmin.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.idxmin.html>

Conversely, `.idxmax()` returns the index of the maximum value and can be used to display the row with the longest length:

`df.loc[df['state'].str.len().idxmax(), 'state']`

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.idxmax.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.idxmax.html>

`.str.replace()` can be used to replace a specific set of characters:

`df['state'] = df['state'].str.replace('Washington', 'WA')`

<https://pandas.pydata.org/docs/reference/api/pandas.Series.str.replace.html>

When mapping _multiple_ values, use `.replace()` instead:

`df['state'] = df['state'].replace({'California': 'CA', 'Washington': 'WA'})`

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.replace.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.replace.html>

Substrings can be sliced from each string in a `Series` using the `[]` notation:

`df['state'].str[0:2]`

## Console Display

Large `DataFrame`s are truncated in order to display them in the console.

`.to_string()` returns a string representation of a `DataFrame` in tabular form.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.to_string.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_string.html>

You can set how much of a `DataFrame` to print on a single row using the `display.width` option:

`pd.set_option('display.width', 100)  # Default is 80`

<https://pandas.pydata.org/docs/reference/api/pandas.set_option.html>

`display.max_colwidth` sets the maximum width of individual columns.

## Descriptive Statistics

Different descriptive statistics methods are available and can be applied to columns with numerical data (e.g., `.median()`, `.std()`). In general, operations operate across rows (`axis=0`) and exclude missing data (`skipna=True`).

<table>
  <thead>
    <tr>
      <th>Method</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>abs</code></td>
      <td>Absolute Value</td>
    </tr>
    <tr>
      <td><code>count</code></td>
      <td>Number of non-NA observations</td>
    </tr>
    <tr>
      <td><code>cummax</code></td>
      <td>Cumulative maximum</td>
    </tr>
    <tr>
      <td><code>cummin</code></td>
      <td>Cummulative minimum</td>
    </tr>
    <tr>
      <td><code>cumprod</code></td>
      <td>Cumulative product</td>
    </tr>
    <tr>
      <td><code>cumsum</code></td>
      <td>Cumulative sum</td>
    </tr>
    <tr>
      <td><code>kurt</code></td>
      <td>Sample kurtosis (4th moment)</td>
    </tr>
    <tr>
      <td><code>mad</code></td>
      <td>Mean absolute deviation</td>
    </tr>
    <tr>
      <td><code>max</code></td>
      <td>Maximum</td>
    </tr>
    <tr>
      <td><code>mean</code></td>
      <td>Mean of values</td>
    </tr>
    <tr>
      <td><code>median</code></td>
      <td>Arithmetic median of values</td>
    </tr>
    <tr>
      <td><code>min</code></td>
      <td>Minimum</td>
    </tr>
    <tr>
      <td><code>mode</code></td>
      <td>Mode</td>
    </tr>
    <tr>
      <td><code>prod</code></td>
      <td>Product of values</td>
    </tr>
    <tr>
      <td><code>quantile</code></td>
      <td>Sample quantile (value at %)</td>
    </tr>
    <tr>
      <td><code>sem</code></td>
      <td>Standard error of the mean</td>
    </tr>
    <tr>
      <td><code>skew</code></td>
      <td>Sample skewness (3rd moment)</td>
    </tr>
    <tr>
      <td><code>std</code></td>
      <td>Bessel-corrected sample standard deviation</td>
    </tr>
    <tr>
      <td><code>sum</code></td>
      <td>Sum of values</td>
    </tr>
    <tr>
      <td><code>var</code></td>
      <td>Unbiased variance</td>
    </tr>
  </tbody>
</table>

Generate descriptive statistics that summarize the central tendency, dispersion, and shape of a data set's distribution (excluding `NaN` values) using `.describe()`.

Specific percentiles can be requested via the `percentiles` parameter. By default, the median is always included.

For a non-numerical `Series`, `.describe()` gives a simple summary of the number of unique values and most frequently occurring values. For mixed-type `DataFrame`s, `.describe()` restricts the summary to include only numerical columns or, if none are present, only categorical columns. This behavior can be controlled via the `include` and `exclude` parameters.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.describe.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.describe.html>

Specific combinations of descriptive statistics for given columns can be defined using `.agg()`.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.agg.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.agg.html>

### Descriptive Statistics Grouped By Category

Calculating a given statistic for each category in a column is a common pattern. `.groupby()` is used to support these types of operations and support the _split-apply-combine_ pattern:

- **Split** the data into groups
- **Apply** a function to each group independently
- **Combine** the results into a data structure

Typically, the apply and combine steps are done together in pandas.

The indexing operator can be used on grouped data to select specific columns, and grouping can be done by multiple columns at the same time by providing a list of column names to `.groupby()`.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.groupby.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.groupby.html>

`.size()` computes group sizes (i.e., the number of rows in each group).

<https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.size.html>

`.value_counts()` counts the number of records for each category in a column. Set the `dropna` parameter to `False` if you want to include `NaN` values in the counts.

When used on a `DataFrame`, `.value_counts()` counts combinations across multiple columns. By default, all columns are used, but a subset can be selected with the `subset` parameter.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.value_counts.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.value_counts.html>

Additional calculation methods include:

- `.shift()`
    - <https://pandas.pydata.org/docs/reference/api/pandas.Series.shift.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.shift.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.SeriesGroupBy.shift.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.shift.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.Index.shift.html>
- `.rank()`
    - <https://pandas.pydata.org/docs/reference/api/pandas.Series.rank.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.rank.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.SeriesGroupBy.rank.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.rank.html>
- `.cumsum()`
    - <https://pandas.pydata.org/docs/reference/api/pandas.Series.cumsum.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.cumsum.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.SeriesGroupBy.cumsum.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.cumsum.html>
- `.cummax()`
    - <https://pandas.pydata.org/docs/reference/api/pandas.Series.cummax.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.cummax.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.SeriesGroupBy.cummax.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.cummax.html>
- `.cummin()`
    - <https://pandas.pydata.org/docs/reference/api/pandas.Series.cummin.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.cummin.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.SeriesGroupBy.cummin.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.cummin.html>
- `.cumprod()`
    - <https://pandas.pydata.org/docs/reference/api/pandas.Series.cumprod.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.cumprod.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.SeriesGroupBy.cumprod.html>
    - <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.cumprod.html>

## Discretization

`pandas.cut()` bins values into discrete intervals. This can be useful when converting a continuous variable into a categorical variable. It supports binning into an equal number of bins or a pre-specified array of bins.

`pd.cut(np.random.randn(20), 3)`

<https://pandas.pydata.org/docs/reference/api/pandas.cut.html>

`pandas.qcut()` is a quantile-based discretization function that discretizes a variable into equal-sized buckets based on rank or sample quantiles:

`pd.qcut(range(5), 3, labels=['good', 'better', 'best'])`

<https://pandas.pydata.org/docs/reference/api/pandas.qcut.html>

`pandas.factorize()` encodes an object as an enumerated type or categorical variable:

`codes, uniques = pd.factorize(np.array(['a', 'b', 'a', 'c', 'a'], dtype='O'))`

This is useful for obtaining a numeric representation of an array when all that matters is identifying distinct values.

<https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.factorize.html>

## Function Application

There are several ways to apply a custom function or another library's function to a pandas object. The approach taken depends on whether the function expects to operate on an entire `Series`/`DataFrame`, row-/column-wise, or element-wise.

### Tablewise Function Application

`Series` and `DataFrame`s can be passed into functions. However, if multiple functions need to operate on an object, [use the `.pipe()` method](https://pandas.pydata.org/docs/user_guide/basics.html#tablewise-function-application).

```python
import pandas as pd  # Enable data structures and data analysis tools


def uppercase_cols(df: pd.core.frame.DataFrame,
                   cols: list[str]) -> pd.core.frame.DataFrame:
    '''
    Uppercase column values.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        pandas DataFrame to process.
    cols : list
        List of column names to uppercase.

    Returns
    ----------------
    pd.core.frame.DataFrame
        Processed DataFrame.
    '''
    for col in cols:
        df[col] = df[col].str.upper()

    return df


def titlecase_cols(df: pd.core.frame.DataFrame,
                   cols: list[str]) -> pd.core.frame.DataFrame:
    '''
    Titlecase column values.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        pandas DataFrame to process.
    cols : list
        List of column names to titlecase.

    Returns
    ----------------
    pd.core.frame.DataFrame
        Processed DataFrame.
    '''
    for col in cols:
        df[col] = df[col].str.title()

    return df


data = {
    'first_name': ['scott', 'jean', 'ororo', 'james',
                   'scott', 'jean', 'ororo', 'james'],
    'last_name': ['summers', 'grey', 'munroe', 'howlett',
                  'summers', 'grey', 'munroe', 'howlett'],
    'course': ['biology', 'chemistry', 'ecology', 'psychology',
               'chemistry', 'ecology', 'psychology', 'biology'],
    'pass_or_fail': ['fail', 'pass', 'pass', 'fail',
                     'pass', 'fail', 'pass', 'pass'],
    'grade': [63, 92, 95, 60,
              75, 64, 92, 82],
}
df = pd.DataFrame(data)

upper_cols = ['pass_or_fail']
title_cols = ['first_name', 'last_name', 'course']
df = df.pipe(uppercase_cols, upper_cols).pipe(titlecase_cols, title_cols)
```

If the next function in a pipe does not accept as its first positional argument what the prior function returned, provide `.pipe()` with a tuple of `(callable, data_keyword)`. `.pipe()` will route the `Series`/`DataFrame` to the argument specified in the tuple.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.pipe.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.pipe.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.GroupBy.pipe.html>

### Row-Wise or Column-Wise Function Application

Arbitrary functions can be applied along the axes of a `DataFrame` (via the `axis` parameter) using the [`.apply()` method](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.apply.html).

```python
def dtrm_retake_sts(row: pd.core.series.Series) -> str:
    '''
    Determine a student's course retake status.

    Parameters
    ----------------
    row : pd.core.series.Series
        Row to determine course retake status from.

    Returns
    ----------------
    str
        Course retake status.
    '''
    cnd = (
        row['course'] in ('Ecology', 'Psychology')
        and row['pass_or_fail'] == 'FAIL'
    )
    if cnd:
        return 'yes'
    else:
        return 'no'


df['retake_sts'] = df.apply(dtrm_retake_sts, axis=1)
```

The return type of the function passed to `.apply()` affects the type of the final output from `.apply()`:

- If the applied function returns a `Series`, the final output is a `DataFrame`.
- If the applied function returns any other type, the final output is a `Series`.

The default behavior regarding the final return type can be configured with the `result_type` parameter.

Additional arguments can be passed to the function used with `.apply()` in addition to the `Series`. Positional arguments can be specified via `.apply()`'s `args` parameter, and keyword arguments can be passed to `.apply()` as is.

`Series` methods can be passed to `.apply()` to carry out `Series` operations on each column or row:

`df.apply(pd.Series.interpolate)`

### Aggregation API

The [aggregation API](https://pandas.pydata.org/docs/user_guide/basics.html#aggregation-api) allows you to express multiple aggregation operations in a single, concise way. The API is similar across pandas objects.

The entry point for aggregation is `.agg()`.

Using a single function with `.agg()` is equivalent to `.apply()`.

`df.groupby('course').agg(mean_grade=('grade', 'mean'))`

Multiple aggregations can be done by passing multiple aggregation arguments as a list. The results of each of the passed functions become a row in the resulting `DataFrame`. These are named from the aggregation function.

You can have specific functions apply to specific columns by passing a dictionary of column names to a scalar or list of scalars to `.agg()`:

`df.agg({'a': 'median', 'b': 'sum'})`

`df.agg({'a': ['min', 'max'], 'b': 'sum'})`

### Transform API

The `.transform()` method returns an object that is indexed the same (i.e., same size) as the original. This API allows you to provide _multiple_ operations at the same time.

```python
df['mean_grade'] = (
    df.groupby(['last_name', 'first_name'])['grade'].transform('mean')
)
```

Like with the `.agg()` API, `.transform()` can receive a single function, or a list of functions. Also like with `.agg()`, passing a dictionary of functions allows for selective transforming per column.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.transform.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.transform.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.SeriesGroupBy.transform.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.transform.html>

### Applying Element-Wise Functions

Not all functions can be vectorized (i.e., accept NumPy arrays and return another array or value). To address this, the `Series.map()` and `DataFrame.applymap()` methods can accept any Python function taking a single value and returning a single value.

```python
pass_or_fail_map = {'PASS': 'P', 'FAIL': 'F'}
df['pass_or_fail'] = df['pass_or_fail'].map(pass_or_fail_map)
```

Additionally, `Series.map()` can be used to map values defined by a [secondary series](https://pandas.pydata.org/docs/user_guide/basics.html#applying-elementwise-functions).

## Window Calculations

`.expanding()` provides expanding window calculations.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.expanding.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.expanding.html>

`.rolling()` provides rolling window calculations.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.rolling.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.rolling.html>

## Time Series

`pandas.to_datetime()` interprets strings and converts them to datetime objects. In pandas, these datetime objects are called as `pandas.Timestamp`.

Strings are formatted using a [specific set of designations](https://docs.python.org/library/datetime.html#strftime-and-strptime-format-codes) and the `format` parameter:

- `%B`: month as locale’s full name
- `%d`: day of the month (01 to 31)
- `%H`: hour in 24-hour format
- `%I`: hour in 12-hour format
- `%M`: minutes (00 to 59)
- `%m`: month (01 to 12)
- `%S`: seconds (00 to 59)
- `%Y`: four-digit year (2019)
- `Z`: standard separator for date and time

`pandas.to_datetime()` can also be used to get the current year, month, and day:

`now = pandas.to_datetime('today').normalize()`

Many `read_*()` functions have a `parse_dates` parameter. This parameter can automatically transform values to dates when reading the data, after being provided with a list of columns to read as `Timestamp`.

Using `pandas.Timestamp` objects for datetimes enable us to do calculations with date information, like getting the length of a time series:

`df['datetime'].max() - df['datetime'].min()`

The result of the above calculation would be a `pandas.Timedelta` object that defines a time duration.

- <https://pandas.pydata.org/docs/reference/api/pandas.to_datetime.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Timestamp.html>

`pandas.date_range()` creates a fixed frequency `DatetimeIndex`.

<https://pandas.pydata.org/docs/reference/api/pandas.date_range.html>

There are numerous [date offsets](https://pandas.pydata.org/docs/reference/offset_frequency.html) that can increment a date range.

pandas provides [many time-related properties](https://pandas.pydata.org/docs/user_guide/timeseries.html#time-date-components) (e.g., `month`, `year`, `date`) that are accessible via the `.dt`accessor (e.g., `df['datetime'].dt.month`). The `.dt` accessor works for date time, time span (period), and time delta `dtypes`.

<table>
  <caption>Time-Related Properties</caption>
  <thead>
    <tr>
      <th>Property</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>year</code></td>
      <td>The year of the datetime</td>
    </tr>
    <tr>
      <td><code>month</code></td>
      <td>The month of the datetime</td>
    </tr>
    <tr>
      <td><code>day</code></td>
      <td>The days of the datetime</td>
    </tr>
    <tr>
      <td><code>hour</code></td>
      <td>The hour of the datetime</td>
    </tr>
    <tr>
      <td><code>minute</code></td>
      <td>The minute of the datetime</td>
    </tr>
    <tr>
      <td><code>second</code></td>
      <td>The seconds of the datetime</td>
    </tr>
    <tr>
      <td><code>microsecond</code></td>
      <td>The microseconds of the datetime</td>
    </tr>
    <tr>
      <td><code>nanosecond</code></td>
      <td>The nanoseconds of the datetime</td>
    </tr>
    <tr>
      <td><code>date</code></td>
      <td>Returns <code>datetime.date</code> (does not contain timezone information)</td>
    </tr>
    <tr>
      <td><code>time</code></td>
      <td>Returns <code>datetime.time</code> (does not contain timezone information)</td>
    </tr>
    <tr>
      <td><code>timetz</code></td>
      <td>Returns <code>datetime.time</code> as local time with timezone information</td>
    </tr>
    <tr>
      <td><code>dayofyear</code></td>
      <td>The ordinal day of year</td>
    </tr>
    <tr>
      <td><code>day_of_year</code></td>
      <td>The ordinal day of year</td>
    </tr>
    <tr>
      <td><code>weekofyear</code></td>
      <td>The week ordinal of the year</td>
    </tr>
    <tr>
      <td><code>week</code></td>
      <td>The week ordinal of the year</td>
    </tr>
    <tr>
      <td><code>dayofweek</code></td>
      <td>The number of the day of the week with Monday=0, Sunday=6</td>
    </tr>
    <tr>
      <td><code>day_of_week</code></td>
      <td>The number of the day of the week with Monday=0, Sunday=6</td>
    </tr>
    <tr>
      <td><code>weekday</code></td>
      <td>The number of the day of the week with Monday=0, Sunday=6</td>
    </tr>
    <tr>
      <td><code>quarter</code></td>
      <td>Quarter of the date: Jan-Mar = 1, Apr-Jun = 2, etc.</td>
    </tr>
    <tr>
      <td><code>days_in_month</code></td>
      <td>The number of days in the month of the datetime</td>
    </tr>
    <tr>
      <td><code>is_month_start</code></td>
      <td>Logical indicating if first day of month (defined by frequency)</td>
    </tr>
    <tr>
      <td><code>is_month_end</code></td>
      <td>Logical indicating if last day of month (defined by frequency)</td>
    </tr>
    <tr>
      <td><code>is_quarter_start</code></td>
      <td>Logical indicating if first day of quarter (defined by frequency)</td>
    </tr>
    <tr>
      <td><code>is_quarter_end</code></td>
      <td>Logical indicating if last day of quarter (defined by frequency)</td>
    </tr>
    <tr>
      <td><code>is_year_start</code></td>
      <td>Logical indicating if first day of year (defined by frequency)</td>
    </tr>
    <tr>
      <td><code>is_year_end</code></td>
      <td>Logical indicating if last day of year (defined by frequency)</td>
    </tr>
    <tr>
      <td><code>is_leap_year</code></td>
      <td>Logical indicating if the date belongs to a leap year</td>
    </tr>
  </tbody>
</table>

In general, a `DataFrame` column can be set as an index using `.set_index()`. When an index is a datetime index (`DatetimeIndex`), time-related properties can be directly accessed without the need for the `dt` accessor.

```python
df = df.set_index('datetime').sort_index()
# Confirm that index values are equal or increasing
df.index.is_monotonic_increasing
```

- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.set_index.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.is_monotonic_increasing.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Index.is_monotonic_decreasing.html>

`.resample()` resamples time series data with a datetime index to another frequency (e.g., converting seconds data into 5-minute interval data). `.resample()` is similar to a groupby operation in that it provides a time-based grouping by [using a string](https://pandas.pydata.org/docs/user_guide/timeseries.html#timeseries-offset-aliases) (e.g., `5M`, `1h`) that defines the target frequency, and that it requires an aggregation function (e.g., `mean`, `max`).

```python
df = df.resample('1h').size().sort_index(ascending=False)
# Confirm that index values are equal or decreasing
df.index.is_monotonic_decreasing
```

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.resample.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.resample.html>

The frequency of a time series index is obtained via the `freq` attribute:

`df.index.freq`

`.tz_localize()` localizes to a time zone.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.tz_localize.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.tz_localize.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Timestamp.tz_localize.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DatetimeIndex.tz_localize.html>

`.tz_convert()` converts to another time zone.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.tz_convert.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.tz_convert.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Timestamp.tz_convert.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DatetimeIndex.tz_convert.html>

`.strftime()` formats datetime values as strings.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.dt.strftime.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Timestamp.strftime.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Period.strftime.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DatetimeIndex.strftime.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.PeriodIndex.strftime.html>

`.to_period()` casts to a period.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.to_period.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_period.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Timestamp.to_period.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DatetimeIndex.to_period.html>

`.to_timestamp()` casts to a timestamp.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.to_timestamp.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_timestamp.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.Period.to_timestamp.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.PeriodIndex.to_timestamp.html>

`pandas.period_range()` creates a fixed frequency `PeriodIndex`.

<https://pandas.pydata.org/docs/reference/api/pandas.period_range.html>

Arithmetic functions can be used when converting between periods and timestamps:

```python
# Convert a quarterly frequency with year end of December
# to 8 AM of the end of the month following the quarter end
per_rng = pd.period_range('2000Q1', '2010Q4', freq='Q-DEC')
ts = pd.Series(np.random.randn(len(per_rng)), per_rng)

ts.index = (per_rng.asfreq('M', 'E') + 1).asfreq('h', 'S') + 8
```

`pandas.timedelta_range()` creates a fixed frequency `TimedeltaIndex` with day as the default.

<https://pandas.pydata.org/docs/reference/api/pandas.timedelta_range.html>

## Plotting

If running under Jupyter Notebook, `.plot()` displays a plot.

- <https://pandas.pydata.org/docs/reference/api/pandas.Series.plot.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.plot.html>
- <https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.plot.html>

Otherwise, `matplotlib.pyplot.show()` displays all open figures.

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.show.html>

`matplotlib.pyplot.savefig` saves the current figure to a file.

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.savefig.html>

By default, calling `.plot()` on a `DataFrame` will create a line graph for each column with datetime or numerical data.

Many different plot types are available to `.plot()`:

- `area` for area plots
- `bar` or `barh` for bar plots
- `box` for boxplots
- `density` or `kde` for density plots
- `hexbin` for hexagonal bin plots
- `hist` for histograms
- `pie` for pie plots
- `scatter` for scatter plots

`.plot()` methods are available for `Series` and `DataFrame`s.

<https://pandas.pydata.org/docs/user_guide/visualization.html#visualization-other>

Many plot methods have a `subplots` parameter that can be used to display each `DataFrame` column in a separate subplot (`subplots=True`).

Each plot object created by pandas is a Matplotlib object. Making an explicit link between pandas and Matplotlib engenders greater plot customization options:

```python
# Create an empty Matplotlib figure/axes
fig, axes = plt.subplots(figsize=(12, 4))

# Use pandas to put the plot data on the prepared figure/axes
df.plot.area(ax=axes)

# Do Matplotlib customizations
axes.set_xlabel('City')
axes.set_ylabel('Parts Per Million (PPM)')

fig.savefig("city_ppm.png")  # Save the current figure
plt.show()  # Display the plot
```

## Documentation

[pandas Documentation](https://pandas.pydata.org/docs/)
- [Categorical Data](https://pandas.pydata.org/docs/user_guide/categorical.html)
- [Chart Visualization](https://pandas.pydata.org/docs/user_guide/visualization.html)
- [Cookbook](https://pandas.pydata.org/docs/user_guide/cookbook.html)
- [Essential Basic Functionality](https://pandas.pydata.org/docs/user_guide/basics.html)
- [Frequently Asked Questions (FAQ)](https://pandas.pydata.org/docs/user_guide/gotchas.html)
- [Group By: Split-Apply-Combine](https://pandas.pydata.org/docs/user_guide/groupby.html)
- [IO Tools](https://pandas.pydata.org/docs/user_guide/io.html)
- [Issue Tracker](https://github.com/pandas-dev/pandas/issues)
- [Merge, Join, Concatenate, and Compare](https://pandas.pydata.org/docs/user_guide/merging.html)
- [MultiIndex / Advanced Indexing](https://pandas.pydata.org/docs/user_guide/advanced.html)
- [pandas Arrays, Scalars, and Data Types](https://pandas.pydata.org/docs/reference/arrays.html)
- [PyArrow Functionality](https://pandas.pydata.org/pandas-docs/version/2.0/user_guide/pyarrow.html)
- [Reshaping and Pivot Tables](https://pandas.pydata.org/docs/user_guide/reshaping.html)
- [Scaling to Large Datasets](https://pandas.pydata.org/docs/user_guide/scale.html)
- [Ten Minutes to pandas](https://pandas.pydata.org/docs/user_guide/10min.html)
- [Time Series / Date Functionality](https://pandas.pydata.org/docs/user_guide/timeseries.html)
- [Working With Missing Data](https://pandas.pydata.org/docs/user_guide/missing_data.html)
- [Working With Text Data](https://pandas.pydata.org/docs/user_guide/text.html)