# Matplotlib Reference

A collection of Matplotlib reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

Matplotlib is a comprehensive library for creating static, animated, and interactive visualizations.

Matplotlib graphs data on `Figure`s that can contain one or more `Axes`, an area where points can be specified in terms of x-y coordinates (or theta-r in a polar plot, x-y-z in a 3D plot).

## Import/Export

```python
# Enable creating static, animated, and interactive visualizations
import matplotlib as mpl
import numpy as np  # Enable multi-dimensional arrays and matrices
import matplotlib.pyplot as plt  # Enable state-based interface to matplotlib
```

## Parts of a Figure

A Matplotlib `Figure` has [many components](https://matplotlib.org/stable/tutorials/introductory/quick_start.html#parts-of-a-figure).

### `Figure`

The _whole_ figure. `Figure` keeps track of all the child `Axes`, a group of special `Artist`s (e.g., titles, figure legends, colorbars), and nested subfigures.

<https://matplotlib.org/stable/api/figure_api.html>

### `Axes`

An `Axes` is an `Artist` attached to a `Figure` that contains a region for plotting data, and usually includes two (or three for 3D) `Axis` objects that provide ticks and tick labels to serve as scales for the data in the `Axes`.

<https://matplotlib.org/stable/api/axes_api.html>

Each `Axes` has a title, x-label, and y-label set via `.set_title()`, `.set_xlabel()`, and `.set_ylabel()`, respectively.

- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.set_title.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.set_xlabel.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.set_ylabel.html>

The `Axes` class and its member functions are the primary entry point to working with Matplotlib's Object-Oriented Programming (OOP) interface, and have most of the plotting methods defined on them.

### `Axis`

`Axis` objects set the scale and limits, and generate ticks (`Axis` marks) and ticklabels (strings labeling ticks). The location of the ticks is determined by a `Locator` object and the ticklabel strings are formatted by a `Formatter`. The correct `Locator`/`Formatter` combination yields fine control over the tick locations and labels.

<https://matplotlib.org/stable/api/axis_api.html>

### `Artist`

Most components of a figure are an `Artist`, including `Figure`, `Axes`, and `Axis` objects. When the figure is rendered, all `Artist`s are drawn to the _canvas_.

<https://matplotlib.org/stable/api/artist_api.html>

Most `Artist`s are tied to an `Axes`. Such an `Artist` cannot be shared by multiple `Axes`, or moved from one to another.

<table>
  <caption>Figure Components</caption>
  <thead>
    <tr>
      <th>Component</th>
      <th>Attribute / Function / Method</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Figure</td>
      <td><code>plt.figure()</code></td>
    </tr>
    <tr>
      <td>Axes</td>
      <td><code>fig.subplots()</code></td>
    </tr>
    <tr>
      <td>x Axis</td>
      <td><code>ax.xaxis</code></td>
    </tr>
    <tr>
      <td>y Axis</td>
      <td><code>ax.yaxis</code></td>
    </tr>
    <tr>
      <td>Spine</td>
      <td><code>ax.spines</code></td>
    </tr>
    <tr>
      <td>Major Tick</td>
      <td><code>ax.xaxis.set_major_locator()</code>, <code>ax.yaxis.set_major_locator()</code></td>
    </tr>
    <tr>
      <td>Minor Tick</td>
      <td><code>ax.xaxis.set_minor_locator()</code>, <code>ax.yaxis.set_minor_locator()</code></td>
    </tr>
    <tr>
      <td>Grid</td>
      <td><code>ax.grid</code></td>
    </tr>
    <tr>
      <td>xlabel</td>
      <td><code>ax.set_xlabel()</code></td>
    </tr>
    <tr>
      <td>ylabel</td>
      <td><code>ax.set_ylabel()</code></td>
    </tr>
    <tr>
      <td>Major Tick Label</td>
      <td><code>ax.xaxis.set_major_formatter()</code>, <code>ax.yaxis.set_major_formatter()</code></td>
    </tr>
    <tr>
      <td>Minor Tick Label</td>
      <td><code>ax.xaxis.set_minor_formatter()</code>, <code>ax.yaxis.set_minor_formatter()</code></td>
    </tr>
    <tr>
      <td>Line</td>
      <td><code>ax.plot()</code></td>
    </tr>
    <tr>
      <td>Markers</td>
      <td><code>ax.scatter()</code></td>
    </tr>
    <tr>
      <td>Legend</td>
      <td><code>ax.legend()</code></td>
    </tr>
    <tr>
      <td>Title</td>
      <td><code>ax.set_title()</code></td>
    </tr>
  </tbody>
</table>

## Plotting Function Inputs

Plotting functions expect `numpy.array` or `numpy.ma.masked_array` as input, or objects that can be passed to `numpy.asarray`. It is common convention to convert other types of objects to `numpy.array` objects prior to plotting:

```python
a = np.matrix([[1, 2], [3, 4]])
a_asarray = np.asarray(a)
```

Most methods also parse addressable objects like dictionaries, `numpy.recarray`s, or `pandas.DataFrame`s. Matplotlib provides the `data` keyword and allows you to generate plots by passing the strings corresponding to the x and y variables.

- <https://numpy.org/doc/stable/reference/generated/numpy.array.html>
- <https://numpy.org/doc/stable/reference/generated/numpy.ma.masked_array.html>
- <https://numpy.org/doc/stable/reference/generated/numpy.asarray.html>

## Coding Styles

There are two ways to use Matplotlib:

1. **The Explicit Interface** - Explicitly create `Figure`s and `Axes`, and call methods on them. Instances of `matplotlib.axes.Axes` are directly utilized to build up a visualization in an instance of `matplotlib.figure.Figure`. This is also referred to as the _object-oriented (OO)_ style.
2. **The Implicit Interface** - Rely on `pyplot` to implicitly create and manage the `Figure` and `Axes`, and use `matplotlib.pyplot` functions for plotting. This interface is inspired by and modeled on MATLAB.

Regardless of the interface used:

- `Figure` is the final image that may contain one or more `Axes`.
- `Axes` represent an individual plot.

In general, the _Explicit Interface_ is recommended, especially for complicated plots and functions/scripts intended for reuse in a larger project. The Implicit Interface can be used for quick, interactive work.

<https://matplotlib.org/stable/tutorials/introductory/quick_start.html#coding-styles>

### Explicit Interface

```python
x = np.linspace(0, 2, 100)  # Create sample data

# Create figure and Axes
fig, ax = plt.subplots(figsize=(10, 8), layout='constrained')

# Plot data on Axes
ax.plot(x, x, label='Linear')
ax.plot(x, x**2, label='Quadratic')
ax.plot(x, x**3, label='Cubic')

# Add Axes labels
ax.set_xlabel('x Label')
ax.set_ylabel('y Label')
ax.set_title('A Simple Plot')  # Add a title to the axes
ax.legend();  # Add a legend
```

The explicit interface uses [`matplotlib.pyplot.figure()`](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.figure.html), i.e., `fig, ax = plt.subplots()` is shorthand for:

```python
fig = plt.figure()
ax = fig.add_subplot(111)
```

The default figure size for a figure is `[6.4, 4.8]` and represents the number of inches for the width and height, respectively.

<https://matplotlib.org/stable/users/explain/api_interfaces.html#the-explicit-axes-interface>

### Implicit Interface

```python
x = np.linspace(0, 2, 100)  # Create sample data

# Create figure
plt.figure(figsize=(10, 8), layout='constrained')

# Plot data on (implicit) Axes
plt.plot(x, x, label='Linear')
plt.plot(x, x**2, label='Quadratic')
plt.plot(x, x**3, label='Cubic')

# Add labels to (implicit) Axes
plt.xlabel('x Label')
plt.ylabel('y Label')
plt.title('A Simple Plot')  # Add title to (implicit) Axes
plt.legend();  # Add legend
```

<https://matplotlib.org/stable/users/explain/api_interfaces.html#the-implicit-pyplot-interface>

## Styling `Artist`s

Most plotting methods have styling options for `Artist`s. These can be accessed either when a plotting method is called, or from a _setter_ on the `Artist`:

```python
data_1, data_2 = np.random.randn(2, 100)
x = np.arange(len(data_1))

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.plot(x, np.cumsum(data_1), color='blue', linewidth=3, linestyle='--')

line = ax.plot(x, np.cumsum(data_2), color='orange', linewidth=2)[0]
line.set_linestyle(':');
```

Matplotlib has a [flexible array of colors](https://matplotlib.org/stable/tutorials/colors/colors.html) that are accepted for most `Artist`s. Some `Artist`s take multiple colors (e.g., scatter plot markers).

```python
plt_colors = [
    'darkslategrey',
    'maroon',
    'sandybrown',
    'darkgoldenrod'
]

data_1, data_2, data_3, data_4 = np.random.randn(4, 100)
x = np.arange(len(data_1))

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

plt.gca().set_prop_cycle('color', plt_colors)

ax.plot(x, np.cumsum(data_1))
ax.plot(x, np.cumsum(data_2))
ax.plot(x, np.cumsum(data_3))
ax.plot(x, np.cumsum(data_4));
```

Typically, line widths are in typographic points (1 pt = 1/72 inch) and available for `Artist`s that have stroked lines. Stroked lines can have a linestyle.

Marker size depends on the method being used. `.plot()` specifies marker size in points and is generally the diameter/width of the marker. `.scatter()` specifies marker size as approximately proportional to the visual area of the marker. There is [an array of marker styles](https://matplotlib.org/stable/api/markers_api.html#module-matplotlib.markers) available as string codes, or you can [define your own `MarkerStyle`](https://matplotlib.org/stable/gallery/lines_bars_and_markers/marker_reference.html).

- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.plot.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.scatter.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.markers.MarkerStyle.html>

If a customization results in content being cut off in the final figure, you can tell Matplotlib to automatically make room for elements in the figure by setting the `autolayout` value of your `rcParams` to `True`:

`plt.rcParams.update({'figure.autolayout': True})`

<https://matplotlib.org/stable/tutorials/introductory/customizing.html>

## Labeling Plots

`.set_xlabel()`, `.set_ylabel()`, and `.set_title()` are used to add text to plots in the indicated locations. Also, text can be directly added to plots using `.text()`.

<https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.text.html>

All of the text functions return a `matplotlib.text.Text` instance. [Keyword arguments](https://matplotlib.org/stable/tutorials/text/text_props.html) can be passed into text functions to customize the properties.

<https://matplotlib.org/stable/api/text_api.html>

Matplotlib accepts [TeX equation expressions](https://matplotlib.org/stable/tutorials/text/mathtext.html) in any text expression:

`ax.set_title(r'$\sigma_i=15$')`

Matplotlib has a built-in TeX expression parser and layout engine, and ships its own math fonts. Also, you can [directly use LaTeX](https://matplotlib.org/stable/tutorials/text/usetex.html) to format your text and directly incorporate the output into your display figures or saved postscript.

Plot points can be annotated, and `matplotlib.pyplot.annotate()` makes annotations easier. Often, annotations involve connecting an arrow from a piece of text at `xytext` to a point at `xy` (both arguments are `(x, y)` tuples).

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.annotate.html>

```python
t = np.arange(0.0, 5.0, 0.01)
s = np.cos(2 * np.pi * t)

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.plot(t, s, lw=2)

ax.annotate('local max', xy=(2, 1), xytext=(3, 1.5),
            arrowprops=dict(facecolor='black', shrink=0.05))
ax.set_ylim(-2, 2);
```

There are other coordinate systems to choose from from, as well:

- <https://matplotlib.org/stable/tutorials/text/annotations.html#annotations-tutorial>
- <https://matplotlib.org/stable/tutorials/text/annotations.html#plotting-guide-annotation>

Plot lines/markers can be identified with a legend and the `label` parameter:

```python
data_1, data_2, data_3 = np.random.randn(3, 100)

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.plot(np.arange(len(data_1)), data_1, label='data_1')
ax.plot(np.arange(len(data_2)), data_2, label='data_2')
ax.plot(np.arange(len(data_3)), data_3, 'd', label='data_3')

ax.legend();
```

Legends are flexible in the layout, placement, and `Artist`s [they can represent](https://matplotlib.org/stable/tutorials/intermediate/legend_guide.html).

<https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.legend.html>

## `Axis` Scales and Ticks

Each `Axes` has two (or three) `Axis` objects representing the x- and y-axis. The `Axes` object controls the scale of the `Axis` objects, the tick _locators_, and the tick _formatters_. Additional `Axes` can be attached to display additional `Axis` objects.

In addition to the linear scale, Matplotlib provides non-linear scales, like the log-scale. Also, there are direct methods for log-scales, like `.loglog()`, `.semilogx()`, and `.semilogy()`. There are a [number of scales](https://matplotlib.org/stable/gallery/scales/scales.html).

```python
# Fixing random state for reproducibility
np.random.seed(93847592)

# Create data in the open interval (0, 1)
y = np.random.normal(loc=0.5, scale=0.4, size=1000)
y = y[(y > 0) & (y < 1)]
y.sort()
x = np.arange(len(y))

# Plot with various axes scales
fig, ((ax_1, ax_2), (ax_3, ax_4)) = plt.subplots(2, 2, figsize=(9, 6.7))

# Linear
ax_1.plot(x, y)
ax_1.set_yscale('linear')
ax_1.set_title('Linear')
ax_1.grid(True)

# Log
ax_2.plot(x, y)
ax_2.set_yscale('log')
ax_2.set_title('Log')
ax_2.grid(True)

# Symmetric log
ax_3.plot(x, y - y.mean())
ax_3.set_yscale('symlog', linthresh=0.01)
ax_3.set_title('Symlog')
ax_3.grid(True)

# Logit
ax_4.plot(x, y)
ax_4.set_yscale('logit')
ax_4.set_title('Logit')
ax_4.grid(True)

# Adjust the subplot layout, because logit plot may take more space
# than usual, due to y-tick labels like '1 - 10^{-3}'
fig.subplots_adjust(top=0.92, bottom=0.08,
                    left=0.10, right=0.95,
                    hspace=0.25, wspace=0.35);
```

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.subplots_adjust.html>

The scale sets the mapping from data values to spacing along the `Axis`. This happens in both directions and gets [combined into a transform](https://matplotlib.org/stable/tutorials/advanced/transforms_tutorial.html), which is how Matplotlib maps from data coordinates to `Axes`, `Figure`, or screen coordinates.

Each `Axis` has a tick _locator_ and _formatter_ that chooses where along the `Axis` objects to put tick marks. Different scales can have different [locators](https://matplotlib.org/stable/gallery/ticks/tick-locators.html)/[formatters](https://matplotlib.org/stable/gallery/ticks/tick-formatters.html).

Access to tick labels can be obtained via `matplotlib.axes.Axes.get_xticklabels()` and `matplotlib.axes.Axes.get_yticklabels()`:

```python
fig, ax = plt.subplots()

ax.barh(y, x)

labels = ax.get_xticklabels()
```

- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.get_xticklabels.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.get_yticklabels.html>

Matplotlib can handle plotting arrays of dates, strings, or floating point numbers. These arrays get special locators and formatters, as appropriate.

For [dates](https://matplotlib.org/stable/gallery/text_labels_and_annotations/date.html):

```python
dates = np.arange(np.datetime64('2022-11-15'), np.datetime64('2022-12-25'),
                  np.timedelta64(1, 'h'))
data = np.cumsum(np.random.randn(len(dates)))

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.plot(dates, data)

cdf = mpl.dates.ConciseDateFormatter(ax.xaxis.get_major_locator())
ax.xaxis.set_major_formatter(cdf);
```

For strings, we get [categorical plotting](https://matplotlib.org/stable/gallery/lines_bars_and_markers/categorical_variables.html):

```python
categories = ['a', 'b', 'c', 'd']

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.bar(categories, np.random.rand(len(categories)));
```

Plotting data of different magnitudes in one chart may require an additional y-axis. This axis [can be created](https://matplotlib.org/stable/gallery/subplots_axes_and_figures/two_scales.html) using `.twinx()` to add a new `Axes` with an invisible x-axis and a y-axis positioned at the right. Conversely, `.twiny()` adds a new `Axes` with an invisible y-axis and a x-axis positioned at the top.

Also, you can add a secondary x-axis or secondary y-axis having a different scale than the main `Axis` to represent data in different scales/units using `.secondary_xaxis()` and `.secondary_yaxis()`, respectively.

```python
fig, (ax_1, ax_2) = plt.subplots(1, 2, figsize=(9, 4.7), layout='constrained')

l1, = ax_1.plot(t, s)
ax_3 = ax_1.twinx()
l2, = ax_3.plot(t, range(len(t)), 'C1')
ax_3.legend([l1, l2], ['Sine (left)', 'Straight (right)'])

ax_2.plot(t, s)
ax_2.set_xlabel('Angle [rad]')
ax_4 = ax_2.secondary_xaxis('top', functions=(np.rad2deg, np.deg2rad))
ax_4.set_xlabel('Angle [°]');
```

- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.twinx.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.twiny.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.secondary_xaxis.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.secondary_yaxis.html>

## Color Mapped Data

Matplotlib has several plot types that enable a third dimension in the plot to be represented by colors in a colormap.

```python
x, y = np.meshgrid(np.linspace(-3, 3, 128), np.linspace(-3, 3, 128))
z = (1 - x / 2 + x**5 + y**3) * np.exp(-x**2 - y**2)

data_1 = np.random.randn(100)
data_2 = np.random.randn(100)
data_3 = np.random.randn(100)

fig, ((ax_1, ax_2), (ax_3, ax_4)) = plt.subplots(2, 2, figsize=(9, 6.7),
                                                 layout='constrained')

pcm = ax_1.pcolormesh(x, y, z, vmin=-1, vmax=1, cmap='RdBu_r')
fig.colorbar(pcm, ax=ax_1)
ax_1.set_title('pcolormesh()')

con_f = ax_2.contourf(x, y, z, levels=np.linspace(-1.25, 1.25, 11))
fig.colorbar(con_f, ax=ax_2)
ax_2.set_title('contourf()')

ims = ax_3.imshow(z**2 * 100, cmap='plasma',
                  norm=mpl.colors.LogNorm(vmin=0.01, vmax=100))
fig.colorbar(ims, ax=ax_3, extend='both')
ax_3.set_title('imshow() With LogNorm()')

sct = ax_4.scatter(data_1, data_2, c=data_3, cmap='RdBu_r')
fig.colorbar(sct, ax=ax_4, extend='both')
ax_4.set_title('scatter()');
```

The `Artist`s above are derived from `ScalarMappable` objects. They can all set a linear mapping between `vmin` and `vmax` into the colormap specified by `cmap`.

<https://matplotlib.org/stable/api/cm_api.html#matplotlib.cm.ScalarMappable>

Matplotlib has [many colormaps](https://matplotlib.org/stable/tutorials/colors/colormaps.html).

```python
import matplotlib.pyplot as plt  # Enable state-based interface to matplotlib

plt.colormaps()  # Return a list of all registered colormaps
```

A colormap class name can be passed to the `colormap` parameter of `matplotlib.pyplot`.

Also, you can [create your own colormap](https://matplotlib.org/stable/tutorials/colors/colormap-manipulation.html) or download a [third-party colormap package](https://matplotlib.org/mpl-third-party/#colormaps-and-styles).

To perform a non-linear mapping of data to a colormap, like in the `LogNorm` plot above, provide the `ScalarMappable` with the `norm` argument, instead of `vmin` and `vmax`. There are [more types of colormap normalizations](https://matplotlib.org/stable/tutorials/colors/colormapnorms.html) available.

Adding a `colorbar` gives a key to relate a color back to the underlying data. Colorbars are figure-level `Artist`s and are attached to a `ScalarMappable`, where they get their information about the norm and colormap. Usually, they take space from a parent `Axes`.

Placement of colorbars [can be complex](https://matplotlib.org/stable/gallery/subplots_axes_and_figures/colorbar_placement.html).

Also, you can change the appearance of colorbars with the `extend` keyword to add arrows to the ends, and control the size with the `shrink` and `aspect` keywords.

Colorbars have default _locators_ and _formatters_ appropriate to the norm. These can be changed as for the other `Axis` objects.

<https://matplotlib.org/stable/api/figure_api.html#matplotlib.figure.Figure.colorbar>

## Multiple `Figure`s and `Axes`

MATLAB and `matplotlib.pyplot` have the concept of the current `Figure` and `Axes`. All plotting functions apply to the current `Axes`. `matplotlib.pyplot.gcf()` returns the current `Figure`, and `matplotlib.pyplot.gca()` returns the current `Axes`.

- <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.gcf.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.gca.html>

```python
def f(t):
    return np.exp(-t) * np.cos(2*np.pi*t)


t1 = np.arange(0.0, 5.0, 0.1)
t2 = np.arange(0.0, 5.0, 0.02)

fig, (ax_1, ax_2) = plt.subplots(2, 1, figsize=(7, 4.7), layout='constrained')

ax_1.plot(t1, f(t1), 'bo', t2, f(t2), 'k')
ax_2.plot(t2, np.cos(2*np.pi*t2), 'r--');
```

The `matplotlib.pyplot.subplot()` call specifies `numrows, numcols, plot_number` where `plot_number` ranges from `1` to `numrows * numcols`. Commas in the `matplotlib.pyplot.subplot()` call are optional if `numrows * numcols < 10`.

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.subplot.html>

Multiple plot elements can be drawn on the same instance of `matplotlib.axes.Axes` by calling another plot method on that `Axes` object.

```python
def crt_curr_lbls(x, pos):
    '''
    Create currency labels.

    Parameters
    ----------------
    x : np.float64
        Currency value.
    pos : int
        The tick position.

    Returns
    ----------------
    str
        The formatted currency label.
    '''
    if x >= 1e6:
        s = f'${x * 1e-6:1.1f}M'
    else:
        s = f'${x * 1e-3:1.0f}K'

    return s


data = {
    'Betances LLC': 106562.40,
    'Cook, Roberts, and Holt': 102478.89,
    'Clark, Avila, and Cordle': 118351.28,
    'Quarles-Paxson': 114563.31,
    'Kendrick LLC': 103972.97,
    'Wagstaff Ltd': 104863.80,
    'Guinyard Inc': 138451.29,
    'Allen-Hughes': 125263.51,
    'Fortenberry-Hardesty': 134793.36,
    'Ledesma LLC': 108425.72
}
group_data = list(data.values())
group_names = list(data.keys())
group_mean = np.mean(group_data)

fig, ax = plt.subplots(figsize=(8, 8), layout='constrained')

ax.barh(group_names, group_data)
labels = ax.get_xticklabels()
plt.setp(labels, rotation=45, horizontalalignment='right')

ax.axvline(group_mean, ls='--', color='r')  # Add a vertical line

# Annotate new companies
for group in [3, 5, 8]:
    ax.text(145000, group, 'New Company', fontsize=10,
            verticalalignment='center')

ax.title.set(y=1.05)  # Move title up
ax.set(xlim=[-10000, 140000], xlabel='Total Revenue', ylabel='Company',
       title='Company Revenue')
ax.xaxis.set_major_formatter(crt_curr_lbls)
ax.set_xticks([0, 25e3, 50e3, 75e3, 100e3, 125e3]);
```

Multiple `Figure`s can be opened with multiple calls to either `fig = plt.figure()` using an increasing figure number (e.g., `fig = plt.figure(1)`, `fig = plt.figure(2)`), or `fig, ax = plt.subplots()`. By keeping object references, you can add `Artist`s to either `Figure`.

There are several ways to add multiple `Axes`, but the most basic way is `matplotlib.pyplot.subplots()`.

More complex layouts can be achieved using `matplotlib.pyplot.subplot_mosaic()`.

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.subplot_mosaic.html>

```python
fig, axes = plt.subplot_mosaic([['upleft', 'right'], ['lowleft', 'right']],
                               figsize=(7, 4.7), layout='constrained')

axes['upleft'].set_title('upleft')
axes['lowleft'].set_title('lowleft')
axes['right'].set_title('right');
```

- <https://matplotlib.org/stable/tutorials/intermediate/arranging_axes.html>
- <https://matplotlib.org/stable/tutorials/provisional/mosaic.html>

Each `Figure` can have an arbitrary number of subplots and `Axes`.

If you want to manually place an `Axes` (i.e., not on a rectangular grid), use `matplotlib.pyplot.axes`, which allows you to specify the location as `axes([left, bottom, width, height])`, where all values are in fractional (`0` to `1`) coordinates.

- <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.axes.html>
- <https://matplotlib.org/stable/gallery/subplots_axes_and_figures/subplot.html>
- <https://matplotlib.org/stable/gallery/subplots_axes_and_figures/axes_demo.html>

You can clear the current figure with `matplotlib.pyplot.clf()` and the current `Axes` with `matplotlib.pyplot.cla`.

- <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.clf.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.cla.html>

The memory required for a figure is not completely released until the figure is explicitly closed with `matplotlib.pyplot.close()`. `matplotlib.pyplot` maintains internal references until `matplotlib.pyplot.close()` is called.

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.close.html>

## Pyplot

`matplotlib.pyplot` is a collection of functions that make matplotlib act like [MATLAB](https://en.wikipedia.org/wiki/MATLAB). Each `matplotlib.pyplot` function makes a change to a `Figure`.

In `matplotlib.pyplot`, various states are preserved across function calls, so that it keeps track of things like the current figure and plotting area. Plotting functions are directed to the current `Axes`.

If you provide a single list or array to `matplotlib.pyplot.plot()`, matplotlib assumes it is a sequence of `y` values and automatically generates the `x` values for you. Since Python ranges start with `0`, the default `x` vector has the same length as `y`, but starts with `0`.

```python
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.plot([1, 2, 3, 4])

ax.set_ylabel('Some Numbers');
```

`matplotlib.pyplot.plot()` is versatile and takes an arbitrary number of arguments.

```python
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.plot([1, 2, 3, 4], [5, 6, 7, 8]);
```

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html>

### Formatting Plot Style

For every `x`, `y` pair of arguments, there is a third argument that is the format string, which indicates the color and line type of the plot. The letters and symbols of the format string are from MATLAB. The default format is `'b-'`, which is a solid blue line.

```python
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.plot([1, 2, 3, 4], [5, 6, 7, 8], 'ro')

ax.axis([0, 5, 0, 10]);
```

A complete list of line styles and format strings is in the [`matplotlib.pyplot.plot()`](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html) documentation. The `matplotlib.pyplot.axis()` function takes a list of `[xmin, xmax, ymin, ymax]` and specifies the viewport of the `Axes`.

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.axis.html>

Generally, Matplotlib is used with NumPy arrays, and all sequences are internally converted to NumPy arrays.

```python
# Evenly sampled time at 200ms intervals
t = np.arange(0., 5., 0.2)

# Red dashes, blue squares, green triangles
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^');
```

### Plotting With Keyword Strings

Sometimes, you have data in a format that lets you access variables with strings, e.g., `pandas.DataFrame`. Matplotlib allows you to provide such an object with the `data` parameter. When provided, you can generate plots with strings that correspond to these variables.

```python
data = {
    'a': np.arange(50),
    'c': np.random.randint(0, 50, 50),
    'd': np.random.randn(50)
}
data['b'] = data['a'] + (10 * np.random.randn(50))
data['d'] = np.abs(data['d']) * 100

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.scatter('a', 'b', s='d', c='c', data=data)

ax.set_xlabel('Entry a')
ax.set_ylabel('Entry b');
```

### Plotting With Categorical Variables

Matplotlib allows you to directly pass categorical variables to many plotting functions.

```python
names = ['group_a', 'group_b', 'group_c']
values = [1, 10, 100]

fig, (ax_1, ax_2, ax_3) = plt.subplots(1, 3, figsize=(10, 4),
                                       layout='constrained')

ax_1.bar(names, values)
ax_2.scatter(names, values)
ax_3.plot(names, values)

fig.suptitle('Categorical Plotting');
```

### Controlling Line Properties

Lines have [many attributes](https://matplotlib.org/stable/api/_as_gen/matplotlib.lines.Line2D.html) and there are several ways that they can be set:

- Use keyword arguments:

    `plt.plot(x, y, linewidth=2.0)`

- Use the setter methods of a `Line2D` instance. `matplotlib.pyplot.plot()` returns a list of `Line2D` objects, e.g., `line1, line2 = plt.plot(x1, y1, x2, y2)`. In the following example, there is only one line, so the list returned has a length of `1`:

    `line, = plt.plot(x, y, '-')`

- Use `matplotlib.pyplot.setp()`. `matplotlib.pyplot.setp()` transparently works with a list of objects or a single object. You can either use Python keyword arguments or MATLAB-style string/value pairs:

        x1 = [1, 5, 10]
        y1 = [2, 10, 20]

        x2 = [1, 5, 10]
        y2 = [2, 15, 30]

        fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

        lines = ax.plot(x1, y1, x2, y2)

        # Keyword arguments style
        plt.setp(lines, color='r', linewidth=2.0)
        # MATLAB style string value pairs
        plt.setp(lines, 'color', 'r', 'linewidth', 2.0);

To view a list of settable line properties, call `matplotlib.pyplot.setp()` with a line or lines as an argument(s), e.g., `plt.setp(lines)`.

### Working With Text

`matplotlib.pyplot.text()` can be used to add text in an arbitrary location. `matplotlib.pyplot.xlabel()`, `matplotlib.pyplot.ylabel()`, and `matplotlib.pyplot.title()` are used to add text in the indicated locations.

```python
mu, sigma = 0, 1
data = np.random.normal(mu, sigma, 10000)

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.hist(data, 50, density=True, alpha=0.75)

ax.set_xlabel('Random Value')
ax.set_ylabel('Probability')
ax.axis([-4.5, 4.5, 0, 0.45])
ax.grid(True)
ax.text(-4, 0.35, r'$\mu=0,\ \sigma=1$')
plt.title('Probability of Random Values From Standard Normal Distribution');
```

- <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.text.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.xlabel.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.ylabel.html>
- <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.title.html>

All `matplotlib.pyplot.text()` functions return a `matplotlib.text.Text` instance. You can [customize the properties](https://matplotlib.org/stable/tutorials/text/text_props.html) of the text by passing keyword arguments into the text function or use `matplotlib.pyplot.setp()`:

`t = plt.xlabel('Sample Data', fontsize=14, color='blue')`

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.setp.html>

## Images

### Importing Image Data Into NumPy Arrays

Matplotlib relies on the [Pillow](https://pillow.readthedocs.io/en/latest/) library to load image data.

```python
# Enable basic image loading, rescaling, and display operations
import matplotlib.image as mpimg
```

Many images are 24-bit RGB images (8 bits for each red, green, and blue channel). Other times, you may encounter a RGBA image, which allows for transparency (A for alpha channel), or single-channel grayscale (luminance) image.

[Example `stinkbug.png` image](https://raw.githubusercontent.com/matplotlib/matplotlib/main/doc/_static/stinkbug.png)

```python
img = mpimg.imread('stinkbug.png')
img
```

```pycon
array([[[0.40784314, 0.40784314, 0.40784314],
        [0.40784314, 0.40784314, 0.40784314],
        [0.40784314, 0.40784314, 0.40784314],
        ...,
        [0.42745098, 0.42745098, 0.42745098],
        [0.42745098, 0.42745098, 0.42745098],
        [0.42745098, 0.42745098, 0.42745098]],

       [[0.4117647 , 0.4117647 , 0.4117647 ],
        [0.4117647 , 0.4117647 , 0.4117647 ],
        [0.4117647 , 0.4117647 , 0.4117647 ],
        ...,
        [0.42745098, 0.42745098, 0.42745098],
        [0.42745098, 0.42745098, 0.42745098],
        [0.42745098, 0.42745098, 0.42745098]],

       [[0.41960785, 0.41960785, 0.41960785],
        [0.41568628, 0.41568628, 0.41568628],
        [0.41568628, 0.41568628, 0.41568628],
        ...,
        [0.43137255, 0.43137255, 0.43137255],
        [0.43137255, 0.43137255, 0.43137255],
        [0.43137255, 0.43137255, 0.43137255]],

       ...,

       [[0.4392157 , 0.4392157 , 0.4392157 ],
        [0.43529412, 0.43529412, 0.43529412],
        [0.43137255, 0.43137255, 0.43137255],
        ...,
        [0.45490196, 0.45490196, 0.45490196],
        [0.4509804 , 0.4509804 , 0.4509804 ],
        [0.4509804 , 0.4509804 , 0.4509804 ]],

       [[0.44313726, 0.44313726, 0.44313726],
        [0.44313726, 0.44313726, 0.44313726],
        [0.4392157 , 0.4392157 , 0.4392157 ],
        ...,
        [0.4509804 , 0.4509804 , 0.4509804 ],
        [0.44705883, 0.44705883, 0.44705883],
        [0.44705883, 0.44705883, 0.44705883]],

       [[0.44313726, 0.44313726, 0.44313726],
        [0.4509804 , 0.4509804 , 0.4509804 ],
        [0.4509804 , 0.4509804 , 0.4509804 ],
        ...,
        [0.44705883, 0.44705883, 0.44705883],
        [0.44705883, 0.44705883, 0.44705883],
        [0.44313726, 0.44313726, 0.44313726]]], dtype=float32)
```

Matplotlib rescaled the 8-bit data from each channel to floating point data between `0.0` and `1.0`. Matplotlib can handle `float32` and `uint8` data types. However, image reading/writing for any format other than PNG is limited to `uint8` data because Pillow can only work with `uint8`. Eight bits of data is used because most displays can only render 8 bits per channel worth of color gradation.

Each inner list in the 3-D array above represents a pixel. For a RGB image, there are three values, and since `stinkbug.png` is a black-and-white image, the values are similar. RGBA images have four values per inner list. A luminance image only has one value per inner list, resulting in a 2-D array.

For grayscale images, Matplotlib only supports the `float32` data type.

If you have an image whose resulting array does not meet any of the above descriptions, you will need to rescale it.

### Plotting NumPy Arrays As Images

A NumPy array can be rendered using `matplotlib.pyplot.imshow()`:

```python
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.imshow(img);
```

<https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.imshow.html>

Any NumPy array can be plotted.

### Applying Pseudocolor Schemes to Image Plots

Pseudocolor can be a useful tool for enhancing contrast and more easily visualizing data. Pseudocolor is only relevant to single-channel, grayscale, luminance images. Since each channel in `stinkbug.png` is similar, we can view a single channel with array slicing:

```python
lum_img = img[:, :, 0]

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.imshow(lum_img);
```

The above code yields a luminance (2D, no color) image and the default colormap (i.e., lookup table, LUT) is applied. The default colormap is called _viridis_, but there are [other values to choose from](https://matplotlib.org/stable/tutorials/colors/colormaps.html):

```python
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.imshow(lum_img, cmap='bone');
```

You can change colormaps on extant plot objects using `.set_cmap()`:

```python
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.imshow(lum_img).set_cmap('ocean');
```

### Color Scale Reference

The value that a color represents can be added to a figure with a color bar:

```python
from mpl_toolkits.axes_grid1 import make_axes_locatable

lum_img = img[:, :, 0]

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.3)

img_plot = ax.imshow(lum_img)
fig.colorbar(img_plot, cax=cax, orientation='vertical');
```

### Examining a Specific Data Range

Histograms can be used to find interesting regions in NumPy arrays.

```python
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.hist(lum_img.ravel(), bins=256, range=(0.0, 1.0), fc='b', ec='b');
```

<https://numpy.org/doc/stable/reference/generated/numpy.ravel.html>

Often, interesting parts of an image are around a peak. You can get extra contrast there by clipping the regions above and/or below the peak. For example, we can adjust the upper limit to _zoom in_ on part of the histogram by calling `.set_clim()` on the image plot object:

```python
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.imshow(lum_img).set_clim(0.0, 0.7);
```

```python
from mpl_toolkits.axes_grid1 import make_axes_locatable

fig, (ax_1, ax_2) = plt.subplots(1, 2, figsize=(9, 6.7), layout='constrained')

divider = make_axes_locatable(ax_1)
cax = divider.append_axes('bottom', size='5%', pad=0.3)
ax_1.imshow(lum_img)
ax_1.set_title('Before')
fig.colorbar(img_plot, cax=cax,
             ticks=[0.1, 0.3, 0.5, 0.7], orientation='horizontal')

divider = make_axes_locatable(ax_2)
cax = divider.append_axes('bottom', size='5%', pad=0.3)
ax_2.imshow(lum_img).set_clim(0.0, 0.7)
ax_2.set_title('After')
fig.colorbar(img_plot, cax=cax,
             ticks=[0.1, 0.3, 0.5, 0.7], orientation='horizontal');
```

<https://matplotlib.org/stable/api/cm_api.html#matplotlib.cm.ScalarMappable.set_clim>

### Array Interpolation Schemes

_Interpolation_ calculates what the color or value of a pixel should be, according to different mathematical schemes. Often, this is required when an image is resized. The number of pixels change, but the information should stay the same.

Since pixels are discrete, there is missing space. Interpolation is what is used to fill that space, and is why you sometimes see images that look pixelated after being blown up. The greater the difference between the original and expanded image, the greater the pixelation effect.

When an image is shrunk, pixels are effectively discarded. When plotted, the data gets blown up to the size on your screen. The old pixels are no longer there and interpolation occurs.

```python
from PIL import Image

img = Image.open('stinkbug.png')

img.thumbnail((64, 64))  # Resize image in-place

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.imshow(img);
```

The default interpolation scheme, `bilnear`, is used, but a different scheme can be provided to `matplotlib.pyplot.imshow()` via the `interpolation` parameter:

```python
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.imshow(img, interpolation='nearest');
```

Bicubic interpolation is often used when blowing up photos to avoid pixelation:

```python
fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.imshow(img, interpolation='bicubic');
```

### Saving a Figure

Matplotlib supports saving a figure to a drive in many formats. To view a list of available options, use:

`fig.canvas.get_supported_filetypes()`

Use `matplotlib.figure.Figure.savefig()` to save the figure to a drive:

`fig.savefig('o_stinkbug.png', bbox_inches='tight')`

`bbox_inches='tight'` ensures that Matplotlib tries to figure out the tight bounding box (whitespace) of the figure.

<https://matplotlib.org/stable/api/figure_api.html#matplotlib.figure.Figure.savefig>

### Saving Figures to a Multi-Page PDF

`matplotlib.backends.backend_pdf.PdfPages` can be used to save multiple figures to a multi-page PDF file:

```python
# Enable a single object containing all the information
# from a date object and a time object
from datetime import datetime as dt

# Enable creation of multi-page PDF files
from matplotlib.backends.backend_pdf import PdfPages

with PdfPages('multi_page.pdf') as pdf:
    fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')
    ax.plot(np.random.randint(0, 50, 10), 'r-o')
    plt.title('Page One')
    pdf.savefig()
    plt.close()

    fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')
    ax.plot(np.random.randint(0, 50, 10), 'b-o')
    plt.title('Page Two')
    pdf.savefig()
    plt.close()

    d = pdf.infodict()
    d['Title'] = 'Multi-Page PDF Example'
    d['Author'] = 'Eric Draven'
    d['Subject'] = 'How To Create a Multi-Page PDF File and Set Its Metadata'
    d['Keywords'] = 'multi-page PDF PdfPages'
    d['CreationDate'] = dt(2023, 12, 5)
    d['ModDate'] = dt.today()
```

- <https://matplotlib.org/stable/api/backend_pdf_api.html#matplotlib.backends.backend_pdf.PdfPages>
- <https://matplotlib.org/stable/gallery/misc/multipage_pdf.html>

## Customizing Matplotlib With Style Sheets and `rcParams`

There are three ways to customize Matplotlib, in order of precedence:

1. Setting `rcParams` at runtime
2. Using style sheets
3. Changing your `matplotlibrc` file

### Runtime rc Settings

rc settings can be dynamically changed via a Python script or interactively from the Python shell. All rc settings are stored in a dictionary-like variable called `matplotlib.rcParams`, which is global to the `matplotlib` package.

`rcParams` can be directly modified. Keep in mind, in order to change the usual plot color, you have to change the `prop_cycle` property of the `Axes`

```python
from cycler import cycler

data = np.random.randn(20)

mpl.rcParams['lines.linewidth'] = 2
mpl.rcParams['lines.linestyle'] = '--'
mpl.rcParams['axes.prop_cycle'] = cycler(color=['r', 'g', 'b', 'y'])

fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')

ax.plot(data);
```

<https://matplotlib.org/stable/api/matplotlib_configuration_api.html#matplotlib.rcParams>

Also, Matplotlib provides convenience functions for modifying rc settings. `matplotlib.rc()` can be used to simultaneously modify multiple settings in a single group using keyword arguments:

`mpl.rc('lines', linewidth=4, linestyle='-.')`

### Temporary rc Settings

`matplotlib.rcParams` can be temporarily changed using the `matplotlib.rc_context()` context manager:

```python
data = np.random.randn(20)

with mpl.rc_context({'lines.linewidth': 2, 'lines.linestyle': ':'}):
    fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')
    ax.plot(data)

plt.show;
```

`matplotlib.rc_context()` can also be used as a [decorator](https://docs.python.org/glossary.html#term-decorator) to modify the defaults of a function:

```python
@mpl.rc_context({'lines.linewidth': 3, 'lines.linestyle': '-'})
def plotting_function():
    fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')
    ax.plot(data)
```

`matplotlib.rcdefaults()` restores the standard Matplotlib default settings.

<https://matplotlib.org/stable/api/matplotlib_configuration_api.html#matplotlib.rcdefaults>

There is [some degree of validation](https://matplotlib.org/stable/api/rcsetup_api.html#module-matplotlib.rcsetup) when setting the values of `rcParams`.

### Using Style Sheets

Another way to customize the appearance of plots is to set `rcParams` in a style sheet and import the style sheet with `matplotlib.style.use()`. With this approach, you can easily switch between different styles by changing the imported style sheet.

<https://matplotlib.org/stable/api/style_api.html#matplotlib.style.use>

A style sheet looks like a `matplotlibrc` file, but in a style sheet, you can only set parameters that are related to the actual style of a plot. Other parameters, like `backend`, are ignored.

`matplotlibrc` files support all `rcParams`. This makes style sheets portable between different machines without having to worry about dependencies that might/might not be installed on another machine.

- [List of `rcParams`](https://matplotlib.org/stable/api/matplotlib_configuration_api.html#matplotlib.rcParams)
- [List of `rcParams` ignored in style sheets](https://matplotlib.org/stable/api/style_api.html#matplotlib.style.use)

Many different styles are available for Matplotlib plots, and they control various things, e.g., color, line widths, and backgrounds. A list of styles is provided by [`style`](https://matplotlib.org/stable/api/style_api.html#module-matplotlib.style):

`plt.style.available`

A style can be activated with `matplotlib.style.use`:

`plt.style.use('seaborn-v0_8')`

### Defining Your Own Style

You can create custom styles and use them by calling `matplotlib.style.use()` with the path or URL to the style sheet.

Alternatively, you can place your `ex_style_name.mplstyle` file into `mpl_configdir/stylelib/`. From there, you can load it with a call to `plt.style.use(ex_style_name)`.

By default, `mpl_configdir` should be located at `${HOME}/.config/matplotlib/`, but you can confirm the location with `matplotlib.get_configdir()`. You may need to create this directory.

<https://matplotlib.org/stable/api/matplotlib_configuration_api.html#matplotlib.get_configdir>

You can change the directory where Matplotlib looks for the `stylelib` folder by setting the `MPLCONFIGDIR` environment variable.

- <https://matplotlib.org/stable/users/faq/environment_variables_faq.html#envvar-MPLCONFIGDIR>
- <https://matplotlib.org/stable/users/faq/troubleshooting_faq.html#locating-matplotlib-config-dir>

A style sheet in `mpl_configdir/stylelib/` will override a style sheet defined by Matplotlib if the styles have the same name.

### Composing Styles

Often, style sheets are meant to be used together. Styles can be easily combined by passing a list of styles to `matplotlib.style.use()`:

`plt.style.use(['dark_background', 'presentation'])`

### Temporary Styling

If you only want to temporarily use a style for a specific code block, and do not want to change the global styling, use the `style` package's context manager to limit the scope of your changes:

```python
with plt.style.context('fivethirtyeight'):
    fig, ax = plt.subplots(figsize=(7, 4.7), layout='constrained')
    ax.plot(np.sin(np.linspace(0, 2 * np.pi)), 'b-o')

plt.show();
```

### The `matplotlibrc` File

Matplotlib uses [`matplotlibrc`](https://matplotlib.org/stable/tutorials/introductory/customizing.html#matplotlibrc-sample) files to customize many kinds of properties (rc settings/rc properties). Almost every property in Matplotlib has controllable defaults.

The `matplotlibrc` file is read at startup to configure Matplotlib. Matplotlib looks for `matplotlibrc` in four locations, in this order:

1. `matplotlibrc` in the current directory.
2. `${MATPLOTLIBRC}` if it is a file, else `${MATPLOTLIBRC}/matplotlibrc`.
3. `${HOME}/.config/matplotlib/matplotlibrc`, or `${XDG_CONFIG_HOME}/matplotlib/matplotlibrc` if you have customized your environment.
4. `INSTALL/matplotlib/mpl-data/matplotlibrc`, where `INSTALL` is something like `/usr/lib/python3.9/site-packages/`. Every time that you install `matplotlib`, this file is overwritten. If you want your customizations to persist, move this file to a user-specific `matplotlib` directory.

Once `matplotlibrc` is found, no other paths are searched.

To display where the currently active `matplotlibrc` file was loaded from, use `matplotlib.matplotlib_fname()`.

<https://matplotlib.org/stable/api/matplotlib_configuration_api.html#matplotlib.matplotlib_fname>

## Documentation

[Matplotlib Documentation](https://matplotlib.org/stable/index.html)
- [Backends](https://matplotlib.org/stable/users/explain/figure/backends.html)
- [List of Named Colors](https://matplotlib.org/stable/gallery/color/named_colors.html)