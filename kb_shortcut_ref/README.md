# Keyboard Shortcut Reference

A collection of keyboard shortcut reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## Terminal (Emacs Mode)

### Moving Around

**Ctrl+a** or **Home**  
Move to the beginning of the line.

**Ctrl+e** or **End**  
Move to the end of the line.

**Alt+b**, **Alt+←**  
Move one word back.

**Alt+f**, **Alt+→**  
Move one word forward.

**Ctrl+b**  
Move one character back.

**Ctrl+f**  
Move one character forward.

**Ctrl+xx**  
Move between the cursor position and the beginning of the line. You can use this shortcut to repeatedly move between the cursor position and the beginning of the line.

### Command Editing

**Ctrl+u**  
Cut the text from before the cursor position to the beginning of the line.

**Ctrl+k**  
Cut the text from after the cursor position to the end of the line.

**Ctrl+w**  
Cut the word before the cursor.

**Ctrl+y**  
Paste the buffer  contents at the cursor position.

**Alt+u**  
Capitalize every character from the cursor position to the end of the current word.

**Alt+l**  
Lowercase every character from the cursor position to the end of the current word.

**Alt+c**  
Capitalize the character under the cursor position. The cursor will then move to the end of the current word.

**Alt+t**  
Swap the current word with the previous word.

**Ctrl+t**  
Swap the character at the cursor with the character preceding it.

**Ctrl+Shift+/**  
Undo last key press.

**Ctrl+h** or **Backspace**  
Delete the character before the cursor.

**Ctrl+d** or **Delete**  
Delete the character under the cursor.

**Alt+d**  
Delete the current word.

### Screen Control

**Ctrl+l**  
Clear the screen.

**Ctrl+s**  
Stop output to the screen.

**Ctrl+q**  
Resume output to the screen.

### Job Control

**Ctrl+z**  
Suspend the foreground process. From here, you can unsuspend the process by entering **fg**, or place it in the background by entering **bg**.

**Ctrl+c**  
Kill the foreground process by sending it a `SIGINT` signal.

**Ctrl+d**  
Exit the current shell by sending an _End-of-file (EOF)_ marker to `bash`. This is similar to running the `exit` command.

### Command History

**↑** or **Ctrl+p**  
Go to the previous command in the command history.

**↓** or **Ctrl+n**  
Go to the next command in the command history.

**Alt+r**  
Revert the changes to a command that you have pulled from the command history.

**Ctrl+r**  
Activate _recursive search_. Recalls the last command in the command history that matches the characters that you provide. Can be pressed multiple times to move backward in the command history.

Enter **Ctrl+s** to move forward in the command history. Enter **Escape** to exit recursive search with the command that you have found placed onto the command line (but not executed).

**Ctrl+o**  
Run a command found with recursive search and place the next command in the command history on the command line (but not execute it) afterwards.

**Ctrl+g**  
Exit command recursive search mode without running any commands or placing any commands on the command line.

### Tab Completion

**Tab**  
Auto-complete files, directories, and binaries.

When you press the **Tab** key, the shell completes an incomplete input if the continuation can be uniquely identified. For the first word of a command, `bash` considers all executable programs. Within the rest of the command line, `bash` considers all files in the current or specified directory.

If several commands or files exist whose names start out equal, `bash` completes the name as far as possible and then acoustically signals that the command or filename may still be incomplete. Another **Tab** lists the remaining possibilities.

## less

`↓` or `e` or `j` or `Enter`  
Scroll down one line at a time.

`↑` or `y` or `k`  
Scroll up one line at a time.

`f` or `Space` or `Page Down`  
Scroll down one page at a time.

`b` or `Page Up`  
Scroll up one page at a time.

`g` or `Home`  
Move to the beginning of the output.

`G` or `End`  
Move to the end of the output.

`/ ex_term`  
Search forward for a term in the output.

You can move to the next instance of a term by pressing `n`. You can move to the previous instance of a term by pressing `N`.

`? ex_term`  
Search backward for a term in the output.

`q`  
Quit.

## vim

### Starting/Stopping Vim

`vim`  
Start Vim.

`$ vim ex_file...`  
Start Vim and edit a file.

`$ vim -p ex_file...`  
Start Vim with files in tabs.

`$ vim -S ex_session_file`  
Start Vim and source `ex_session_file` after loading the first file.

`$ vim +/ex_string ex_file`  
Open file with a search string (`ex_string`) and place cursor on first line with an instance of `ex_string` (e.g., `$ vim +/mangoes 'fruit.txt'`).

`$ vim +ex_line_num ex_file`  
Open file and place cursor on specific line (e.g., `$ vim +5 'fruit.txt'`).

`$ vim +ex_vim_option ex_file...`  
Set a Vim option from the command line.

`$ vim -r ex_file`  
Start Vim and edit a file in recovery mode after a system crash.

`$ vimdiff ex_file_1 ex_file_2`  
Compare two files to make edits.

[`:wq`](https://vimhelp.org/editing.txt.html#%3Awq)  
Write the current file and close the window.

[`:wqa`](https://vimhelp.org/editing.txt.html#%3Awqa)  
Write all changed buffers and exit Vim.

[`:q`](https://vimhelp.org/editing.txt.html#%3Aq)  
Quit the current window.

[`:qa`](https://vimhelp.org/editing.txt.html#%3Aqa)  
Exit Vim, unless there are some buffers that have been changed.

`:q!`  
Quit without writing, also when the current buffer has changes.

`:qa!`  
Exit Vim. Any changes to buffers are lost.

### Setting Options

[`:set`](https://vimhelp.org/options.txt.html#%3Aset)  
Show all options that differ from their default value.

`:set all`  
Show all, except terminal options.

`:set ex_option`  
For a Toggle option, switch it on. For a Number amd String option, show its value.

[`:set number`](https://vimhelp.org/options.txt.html#%27number%27)  
Print the line number in front of each line.

[`:set nonumber`](https://vimhelp.org/options.txt.html#%27nonumber%27)  
Do not print the line number in front of each line.

[`:set showmode`](https://vimhelp.org/options.txt.html#%27showmode%27)  
If in Insert, Replace, or Visual mode, put a message on the last line.

[`:set spell`](https://vimhelp.org/options.txt.html#%27spell%27)  
When on, spell checking will be done.

`:set nospell`  
Disable spell checking.

### Working With Files

[`:e ex_file`](https://vimhelp.org/editing.txt.html#%3Ae)  
Edit `ex_file`.

[`:r ex_file`](https://vimhelp.org/insert.txt.html#%3Ar)  
Insert the file `ex_file` below the cursor.

[`Ctrl+g`](https://vimhelp.org/editing.txt.html#CTRL-G)  
Prints the current file name, the cursor position, and the file status (readonly, modified, read errors, new file).

[`:w`](https://vimhelp.org/editing.txt.html#%3Aw)  
Write the whole buffer to the current file.

`:w ex_file`  
Write the specified lines to `ex_file`.

`:w! ex_file`  
Write the specified lines to `ex_file`. Overwrite an existing file.

[`:wa`](https://vimhelp.org/editing.txt.html#%3Awa)  
Write all changed buffers.

[`:syntax on`](https://vimhelp.org/syntax.txt.html#%3Asyntax)  
Turn syntax checking on.

`:syntax off`  
Turn syntax checking off.

[`:buffers`](https://vimhelp.org/windows.txt.html#%3Abuffers) or `:ls`  
Show all buffers.

Each buffer has a unique number that will not change.

Indicators include (characters in the same list are mutually exclusive):

- `u` - An unlisted buffer.
    - `%` - The buffer in the current window.
    - `#` - The alternate buffer for `:e #` and `Ctrl+^`
        - `a` - An active buffer that is loaded and visible.
        - `h` - A hidden buffer that is loaded, but not currently displayed in a window.
            - `-` - A buffer with `modifiable` off
            - `=` - A readonly buffer.
            - `R` - A terminal buffer with a running job.
            - `F` - A terminal buffer with a finished job.
            - `?` - A terminal buffer without a job.
                - `+` - A modified buffer.
                - `x` - A buffer with read errors.

[`:Nb`](https://vimhelp.org/windows.txt.html#%3Ab) (where `N` is the unique buffer number) or `:b ex_buffer_name`  
Edit buffer from the buffer list.

[`:bn`](https://vimhelp.org/windows.txt.html#%3Abn)  
Go to next buffer in the buffer list.

[`:bp`](https://vimhelp.org/windows.txt.html#%3Abp)  
Go to previous buffer in the buffer list.

[`:bd`](https://vimhelp.org/windows.txt.html#%3Abd)  
Unload current buffer and delete it from the buffer list.

A buffer number can be used with this command to unload/delete a specific buffer (`:Nbd`).

[`:mks ex_session_file`](https://vimhelp.org/starting.txt.html#%3Amks)  
Write a Vim script that restores the current editing session.

[`:so ex_session_file`](https://vimhelp.org/repeat.txt.html#%3Aso)  
Read Ex commands from `ex_session_file`. Restores an editing session.

### Moving Around

`Space` or [`l`](https://vimhelp.org/motion.txt.html#l)  
Move cursor one character right.

`Backspace` or [`h`](https://vimhelp.org/motion.txt.html#h)  
Move cursor one character left.

`↓` or [`j`](https://vimhelp.org/motion.txt.html#j)  
Move cursor down one line.

`3↓` or `3j`  
Use a force multiplier to move down three lines.

`↑` or [`k`](https://vimhelp.org/motion.txt.html#k)  
Move cursor up one line.

[`w`](https://vimhelp.org/motion.txt.html#w)  
Move forward one word.

[`b`](https://vimhelp.org/motion.txt.html#b)  
Move backward one word.

[`f{ex_char}`](https://vimhelp.org/motion.txt.html#f)  
Move to the next `{ex_char}` character on the line.

[`End`](https://vimhelp.org/motion.txt.html#%3CEnd%3E) or [`$`](https://vimhelp.org/motion.txt.html#%24)  
Move to the end of the line.

[`Home`](https://vimhelp.org/motion.txt.html#%3CHome%3E) or [`0`](https://vimhelp.org/motion.txt.html#0)  
Move to the first character of the line.

`NG` or `:N` (where `N` is the a line number)  
Move to a specific line (e.g., `10G` or `:10`).

[`G`](https://vimhelp.org/motion.txt.html#G) or [`:$`](https://vimhelp.org/cmdline.txt.html#%3A%24)  
Move to the first non-blank character of the last line of the file.

`1G` or `:0`  
Move to the first non-blank character of the first line of the file.

[`Page Down`](https://vimhelp.org/scroll.txt.html#%3CPageDown%3E) or `Ctrl+f`  
Move forward a page.

[`Page Up`](https://vimhelp.org/scroll.txt.html#%3CPageUp%3E) or `Ctrl+b`  
Move backward a page.

[`Ctrl+d`](https://vimhelp.org/scroll.txt.html#CTRL-D)  
Move forward a half-page.

[`Ctrl+u`](https://vimhelp.org/scroll.txt.html#CTRL-U)  
Move backward a half-page.

### Searching For Text

[`/ex_pattern`](https://vimhelp.org/pattern.txt.html#%2F)  
Search forward for `ex_pattern` (e.g., `/pineapples`).

`ex_pattern` can be a regular expression.

[`?ex_pattern`](https://vimhelp.org/pattern.txt.html#%3F)  
Search backward for `ex_pattern`.

[`n`](https://vimhelp.org/pattern.txt.html#n)  
Move to next occurrence of search pattern.

[`N`](https://vimhelp.org/pattern.txt.html#N)  
Move to previous occurrence of search pattern.

`/\<ex_pattern\>`  
Search for an exact, whole word.

Also, you can place the cursor anywhere on the word in question and press the asterisk key (`*`) to accomplish the same thing.

[`:s/ex_old_string/ex_new_string/`](https://vimhelp.org/change.txt.html#%3As)  
Find and replace the first instance of a string on the current line (e.g., `:s/plums/blueberries/`).

`ex_old_string` and `ex_new_string` can be regular expressions.

`:{ex_line_start},{ex_line_end}s/ex_old_string/ex_new_string/`  
Find and replace the first instance of a string on a specified set of lines (e.g., `:1,5s/plums/blueberries/`).

You can use a `.` to represent the current line, and a `$` to represent the last line of the file (e.g., `:.,$s/plums/blueberries/`).

The `&` character can also be used to refer back to the text matched by your regular expression.

Example:

`:s/straw/&berry/`

The example above changes `straw` to `strawberry`.

`:s/ex_old_string/ex_new_string/g`  
Find and replace all instances of a string on the current line.

`:%s/ex_old_string/ex_new_string/g`  
Find and replace all instances of a string in an entire file.

### Working With Text

[`a`](https://vimhelp.org/insert.txt.html#a)  
Append text after the cursor.

[`A`](https://vimhelp.org/insert.txt.html#A)  
Append text at the end of the line.

[`i`](https://vimhelp.org/insert.txt.html#i)  
Insert text before the cursor.

[`I`](https://vimhelp.org/insert.txt.html#I)  
Insert text before the first non-blank in the line.

[`o`](https://vimhelp.org/insert.txt.html#o)  
Begin a new line below the cursor and insert text.

[`O`](https://vimhelp.org/insert.txt.html#O)  
Begin a new line above the cursor and insert text.

[`s`](https://vimhelp.org/change.txt.html#s)  
Delete character into [unnamed [register](https://vimhelp.org/change.txt.html#registers)] and start insert.

This command adds new text at the cursor and pushes existing text to the right of the cursor down.

[`S`](https://vimhelp.org/change.txt.html#S)  
Delete line into unnamed register and start insert.

This command wipes out the entire line and lets you enter new text.

[`r`](https://vimhelp.org/change.txt.html#r)  
Replace the character under the cursor.

[`R`](https://vimhelp.org/change.txt.html#R)  
Enter [Replace](https://vimhelp.org/insert.txt.html#Replace) mode. Each character you type replaces an existing character, starting with the character under the cursor.

[`x`](https://vimhelp.org/change.txt.html#x)  
Delete character under the cursor into unnamed register.

`Nx`  
Delete `N` characters under the cursor into unnamed register.

[`X`](https://vimhelp.org/change.txt.html#X)  
Delete character before the cursor into unnamed register.

`NX`  
Delete `N` characters before the cursor into unnamed register.

`dw`  
Delete characters under the cursor to the end of the current word into unnamed register, removing the space after the word.

`de`  
Delete characters under the cursor to the end of the current word into unnamed register, keeping the space after the word.

`df{ex_char}`  
Delete characters under the cursor to the next occurrence of the `{ex_char}` character into unnamed register.

[`D`](https://vimhelp.org/change.txt.html#D) or `d$`  
Delete the characters under the cursor until the end of the line into unnamed register.

`d0`  
Delete the characters before the cursor to the beginning of the line into unnamed register.

[`dd`](https://vimhelp.org/change.txt.html#dd)  
Delete line into unnamed register.

`Ndd`  
Delete `N` lines into unnamed register.

`dL`  
Delete the characters under the cursor to the end of the screen into unnamed register.

`dG`  
Delete the characters under the cursor to the end of the file into unnamed register.

`d1G`  
Delete the characters under the cursor to the beginning of the file.

[`J`](https://vimhelp.org/change.txt.html#J)  
Join line below with line where cursor is placed.

[`u`](https://vimhelp.org/undo.txt.html#u)  
Undo the last operation.

[`Ctrl+r`](https://vimhelp.org/undo.txt.html#CTRL-R)  
Redo last undone change.

`:e!`  
Edit the current file always. Discard any changes to the current buffer. This is useful if you want to start all over again.

[`yy`](https://vimhelp.org/change.txt.html#yy)  
Yank (copy) line into unnamed register.

`Nyy`  
Yank `N` lines into unnamed register.

`yw`  
Yank characters under the cursor to the end of the word into unnamed register.

[`p`](https://vimhelp.org/change.txt.html#p)  
Put (paste) text from unnamed register after the cursor. 

[`P`](https://vimhelp.org/change.txt.html#P)  
Put text from unnamed register before the cursor.

`"ayy`  
Overwrite the `a` buffer with the current line.

`"Ayy`  
Append to the `a` buffer with the current line.

`"ap`  
Put the contents of the `a` buffer after the cursor.

`"aP`  
Put the contents of the `a` buffer before the cursor.

[`v`](https://vimhelp.org/visual.txt.html#v)  
Start [Visual](https://vimhelp.org/visual.txt.html#Visual) mode per character.

This command puts you into _Visual Selection_ mode (select the characters that you want to affect; the cursor should end on the last character to be affected). Here, you can perform searches and other types of operations (e.g., press `d` to cut a selection, `y` to copy a selection, `p` to paste a selection after the cursor, and `P` to paste a selection before the cursor).

[`V`](https://vimhelp.org/visual.txt.html#V)  
Start Visual mode linewise.

Also, Vim has a special [Visual Block](https://vimhelp.org/visual.txt.html#visual-block) mode that you can use to insert text into multiple lines simultaneously. To do this:

- Press `Ctrl+v`.
- Select the lines that you want to affect. The cursor should end on the last line to be affected.
- Press `I` to enter a special version of Insert Mode.
- Enter your desired text.
- Press `Esc`.

After a brief pause, your new text should be inserted into the lines you selected.

### Window/Tab Management

[`:sp`](https://vimhelp.org/windows.txt.html#%3Asplit)  
Split current window horizontally.

A file can be passed to this command as an argument. The original file is placed on the bottom of the editor.

The height of the new window can be set by passing a number to the command like so, `:{ex_num}sp`.

[`:vs`](https://vimhelp.org/windows.txt.html#%3Avsplit)  
Split current window vertically.

A file can be passed to this command as an argument. The original file is placed on the right side of the editor.

[`Ctrl+w`](https://vimhelp.org/index.txt.html#CTRL-W)  
Activate _window_ mode for split windows (move by using direction keys or pressing `w`).

After activating window mode, additional commands are available:

- `s` - Open new horizontal split.
- `v` - Open new vertical split.
- `c` - Close window.
- `o` - Make current window the only one on the screen. All other windows are closed.

Once you switch to a window, that window becomes active and ready for you to begin issuing commands.

[`:close`](https://vimhelp.org/windows.txt.html#%3Aclose)  
Close selected split window.

[`:redraw!`](https://vimhelp.org/various.txt.html#%3Aredraw)  
Clear and redraw the screen right now.

[`:tabnew`](https://vimhelp.org/tabpage.txt.html#%3Atabnew)  
Open a new tab page with an empty window, after the current tab page.

`:tabnew ex_file`  
Open a new tab page and edit `ex_file`.

[`gt`](https://vimhelp.org/tabpage.txt.html#gt)  
Go to the next tab page. A tab number can be used with this command to go to a specific tab (`:Ngt`).

[`gT`](https://vimhelp.org/tabpage.txt.html#gT)  
Go to the previous tab page.

[`:tabclose`](https://vimhelp.org/tabpage.txt.html#%3Atabclose)  
Close current tab page.

### External Commands

[`:sh`](https://vimhelp.org/various.txt.html#%3Ash)  
Start a shell.

[`:! ex_cmd`](https://vimhelp.org/various.txt.html#%3A%21)  
Execute `ex_cmd` with the shell (e.g., `:! wc %`; here, the `%` is a placeholder representing the current file you are working on in Vim).

[`:r! ex_cmd`](https://vimhelp.org/insert.txt.html#%3Ar%21)  
Execute `ex_cmd` and insert its standard output below the cursor or the specified line.

## Text Editors

### Working With Words

**Ctrl+←**  
Move to beginning of previous word.

**Ctrl+→**  
Move to beginning of next word.

**Ctrl+↑**  
Move to beginning of paragraph.

**Ctrl+↓**  
Move to end of paragraph.

### Moving the Cursor

**Home**  
Move to beginning of the current line.

**End**  
Move to end of the current line.

**Ctrl+Home**  
Move to top of field.

**Ctrl+End**  
Move to end of field.

### Selecting Text

**Shift+↑**  
Select line above, one line at a time.

**Shift+↓**  
Select line below, one line at a time.

**Shift+Ctrl+Left**  
Select previous word, one word at a time.

**Shift+Ctrl+Right**  
Select next word, one word at a time.

**Shift+Ctrl+Up**  
Select paragraph above, one paragraph at a time.

**Shift+Ctrl+Down**  
Select paragraph below, one paragraph at a time.