# Python References

A collection of Python reference materials.

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="api_ref/">API Reference</a></td>
      <td>A collection of API reference materials.</td>
    </tr>
    <tr>
      <td><a href="py_lang_ref/">Python Language Reference</a></td>
      <td>A collection of Python language reference materials.</td>
    </tr>
    <tr>
      <td><a href="sql_alc_ref">SQLAlchemy Reference</a></td>
      <td>A collection of SQLAlchemy reference materials.</td>
    </tr>
  </tbody>
</table>