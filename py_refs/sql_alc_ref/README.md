# SQLAlchemy Reference

A collection of SQLAlchemy reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## Import

`import sqlalchemy as sa  # Enable database abstraction library`

## Engine Creation

The `Engine` acts as a central source of connections to a specific database. It provides both a factory and holding space ([connection pool](https://docs.sqlalchemy.org/core/pooling.html)) for database connections.

Typically, the engine is a global object that is created once for a specific database and is configured with either a URL string or URL object. The `create_engine()` function creates an engine.

```python
# Lazy initialization of engine, with debugging information
url = 'sqlite:////var/tmp/sqlalchemy/chinook.sqlite'

engine = sa.create_engine(url, pool_pre_ping=True, echo=True)
```

```python
# Lazy initialization of engine, without debugging information
url = sa.URL.create(
    drivername='sqlite',
    database='/var/tmp/sqlalchemy/chinook.sqlite'
)

engine = sa.create_engine(url, pool_pre_ping=True)
```

- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.Engine>
- <https://docs.sqlalchemy.org/core/engines.html#sqlalchemy.engine.URL.create>
- <https://docs.sqlalchemy.org/core/engines.html#sqlalchemy.create_engine>

If you need to extract the database URL string from the engine, you can do so using the `.url` attribute:

`engine.url.render_as_string(hide_password=False)`

- <https://docs.sqlalchemy.org/core/engines.html#sqlalchemy.engine.URL>
- <https://docs.sqlalchemy.org/core/engines.html#sqlalchemy.engine.URL.render_as_string>

## Transaction and the DBAPI

### Opening/Closing the Database Connection

The `Connection` object is how all interaction with the database is done. It represents an open resource against the database. The scope of this object is limited via the use of a Python context manager.

```python
try:
    with engine.connect():
        pass
except Exception as exc:
    print(exc)
```

```python
query = (
    'SELECT'
    "'Ohayou gozaimasu'"
)

try:
    with engine.connect() as conn:
        result = conn.execute(sa.text(query))
except Exception as exc:
    print(exc)
else:
    print(result.all())
# [('Ohayou gozaimasu',)]
```

The `text()` construct allows us to write SQL statements as textual SQL. In day-to-day SQLAlchemy use, using textual SQL is the exception, not the rule.

- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.Connection>
- <https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.text>

### Committing Changes

#### Commit As You Go

`Engine.connect()` frames the operation inside of a transaction. The transaction is _not automatically committed_. If you want to commit data, call `Connection.commit()`.

This style is referred to as **commit as you go**.

```python
query = (
    'INSERT INTO '
    'Genre '
    '(GenreId, Name) '
    "VALUES "
    "(:GenreId, :Name)"
)
data = [
    {
        'GenreId': '26',
        'Name': 'Chiptune'
    },
    {
        'GenreId': '27',
        'Name': 'Trip Hop'
    }
]

try:
    with engine.connect() as conn:
        conn.execute(sa.text(query), data)
        conn.commit()
except Exception as exc:
    print(exc)
```

- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.Engine.connect>
- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.Connection.commit>

#### Begin Once

`Engine.begin()` manages both the scope of the `Connection` and encloses everything inside of a transaction with `COMMIT` at the end (assuming a successful block) or `ROLLBACK` (in case of an exception).

This style is referred to as **begin once**. It is less flexible for demonstrative purposes, but its succinct style often makes it preferable.

```python
query = (
    'INSERT INTO '
    'Playlist '
    '(PlaylistId, Name) '
    "VALUES "
    "(:PlaylistId, :Name)"
)
data = [
    {
        'PlaylistId': '19',
        'Name': 'Battle of the Bits'
    },
    {
        'PlaylistId': '20',
        'Name': 'Portishead'
    }
]

try:
    with engine.begin() as conn:
        conn.execute(sa.text(query), data)
except Exception as exc:
    print(exc)
```

<https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.Engine.begin>


### Fetching Rows

```python
query = (
    'SELECT '
    'MediaTypeId, '
    'Name '
    'FROM '
    'MediaType'
)

try:
    with engine.begin() as conn:
        result = conn.execute(sa.text(query))
except Exception as exc:
    print(exc)
else:
    output = ''

    for row in result:
        output += (
            f'MediaTypeId: {row.MediaTypeId}\n'
            f'Name: {row.Name}\n\n'
        )

    print(output.rstrip())
```

```pycon
MediaTypeId: 1
Name: MPEG audio file

MediaTypeId: 2
Name: Protected AAC audio file

MediaTypeId: 3
Name: Protected MPEG-4 video file

MediaTypeId: 4
Name: Purchased AAC audio file

MediaTypeId: 5
Name: AAC audio file
```

The `Result` object represents an iterable object of result rows. `Result` has many fetching and transforming methods (e.g., `Result.all()`, which returns a list of all `Row` objects).

- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.Result>
- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.Result.all>

`Row` objects are intended to act like Python [named tuples](https://docs.python.org/library/collections.html#collections.namedtuple). There are various ways to access rows.

#### Tuple Assignment

```python
query = (
    'SELECT '
    'MediaTypeId, '
    'Name '
    'FROM '
    'MediaType'
)

try:
    with engine.begin() as conn:
        result = conn.execute(sa.text(query))
except Exception as exc:
    print(exc)
else:
    for media_type_id, name in result:
        print(media_type_id, name, sep=', ')
```

```pycon
1, MPEG audio file
2, Protected AAC audio file
3, Protected MPEG-4 video file
4, Purchased AAC audio file
5, AAC audio file
```

#### Integer Index

```python
query = (
    'SELECT '
    'MediaTypeId, '
    'Name '
    'FROM '
    'MediaType'
)

try:
    with engine.begin() as conn:
        result = conn.execute(sa.text(query))
except Exception as exc:
    print(exc)
else:
    for row in result:
        name = row[1]
        print(name)
```

```pycon
MPEG audio file
Protected AAC audio file
Protected MPEG-4 video file
Purchased AAC audio file
AAC audio file
```

#### Attribute Name

```python
query = (
    'SELECT '
    'MediaTypeId, '
    'Name '
    'FROM '
    'MediaType'
)

try:
    with engine.begin() as conn:
        result = conn.execute(sa.text(query))
except Exception as exc:
    print(exc)
else:
    for row in result:
        print(row.MediaTypeId)
```

```pycon
1
2
3
4
5
```

#### Mapping Access

```python
query = (
    'SELECT '
    'MediaTypeId, '
    'Name '
    'FROM '
    'MediaType'
)

try:
    with engine.begin() as conn:
        result = conn.execute(sa.text(query))
except Exception as exc:
    print(exc)
else:
    for dict_row in result.mappings():
        media_type_id = dict_row['MediaTypeId']
        name = dict_row['Name']

        print(media_type_id, name, sep='|')
```

```pycon
1|MPEG audio file
2|Protected AAC audio file
3|Protected MPEG-4 video file
4|Purchased AAC audio file
5|AAC audio file
```

### Sending Parameters

`Connection.execute()` accepts parameters that are referred to as [bound parameters](https://docs.sqlalchemy.org/glossary.html#term-bound-parameters), which are used in order to use fixed SQL statements that can have the driver properly sanitize the passed values. Parameters are passed in the format of `:ex_param`, and the actual value of `:ex_param` is passed as the second argument to `Connection.execute()` as a dictionary.

```python
query = (
    'SELECT '
    'PlaylistId, '
    'Name '
    'FROM '
    'Playlist '
    "WHERE "
    "PlaylistId = :PlaylistId "
    "OR Name = :Name"
)
params = {
    'PlaylistId': 19,
    'Name': 'Portishead'
}

try:
    with engine.begin() as conn:
        result = conn.execute(sa.text(query), params)
except Exception as exc:
    print(exc)
else:
    output = ''

    for row in result:
        output += (
            f'PlaylistId: {row.PlaylistId}\n'
            f'Name: {row.Name}\n\n'
        )

    print(output.rstrip())
```

```pycon
PlaylistId: 19
Name: Battle of the Bits

PlaylistId: 20
Name: Portishead
```

Depending on the database management system used, the bound parameter will be turned into a character (e.g., `?` for SQLite) defined by one of six different formats allowed by the DBAPI specification. SQLAlchemy abstracts these formats into one, which is the _named_ format using a colon.

### Sending Multiple Parameters

For [Data Manipulation Language (DML)](https://docs.sqlalchemy.org/glossary.html#term-DML) statements (e.g., `INSERT`, `UPDATE`, `DELETE`), we can send multiple rows into a database at once. This is done by sending a list of dictionaries (versus a single dictionary) as the second argument to `Connection.execute()`, which indicates that the single SQL statement should be invoked multiple times, once for each parameter set.

This execution style is known as [executemany](https://docs.sqlalchemy.org/glossary.html#term-executemany), which was previously demonstrated in the [Commit As You Go](#commit-as-you-go) and [Begin Once](#begin-once) sections. The `INSERT` operations in these examples are equivalent to running the given `INSERT` statement once for each parameter set, except that the operation is optimized for better performance across many rows.

A key difference between _execute_ and _executemany_ is that the latter does not support the returning of result rows, except when using a Core `insert()` construct.

<https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.insert>

## Working With Database Metadata

The central element of SQLAlchemy Core and its Object Relational Mapper (ORM) is the SQL Expression Language that allows for fluent, composable construction of SQL queries. The foundation for these queries are Python objects that represent database concepts like tables and columns. These objects are collectively known as [database metadata](https://docs.sqlalchemy.org/glossary.html#term-database-metadata).

The most common objects for database metadata in SQLAlchemy are `MetaData`, `Table`, and `Column`.

- <https://docs.sqlalchemy.org/core/metadata.html#sqlalchemy.schema.MetaData>
- <https://docs.sqlalchemy.org/core/metadata.html#sqlalchemy.schema.Table>
- <https://docs.sqlalchemy.org/core/metadata.html#sqlalchemy.schema.Column>

### Table Objects

In a relational database, the basic data-holding structure we query is a _table_. In SQLAlchemy, the database _table_ is represented by a Python object named `Table`.

To start using the SQLAlchemy Expression Language, we construct `Table` objects that represent all of the database tables we are interested in working with. The `Table` is programmatically constructed, either directly by using the `Table` constructor, or indirectly by using ORM Mapped classes. Also, there is the option to load some or all table information from an existing database using [reflection](https://docs.sqlalchemy.org/glossary.html#term-reflection).

Regardless of the approach used, we always start out with a collection that will be where we place our tables known as the `MetaData` object. Essentially, this object is a [facade](https://docs.sqlalchemy.org/glossary.html#term-facade) around a Python dictionary that stores a series of `Table` objects keyed to their string name.

```python
# Create MetaData container for an application
md = sa.MetaData()
# Load all available table definitions from the database
md.reflect(bind=engine)
# Or...
# md.reflect(bind=engine, schema='ex_schema', views=True)
```

<https://docs.sqlalchemy.org/core/metadata.html#sqlalchemy.schema.MetaData.reflect>

Having a single `MetaData` object for an application is the most common case, represented as a module-level variable in a single place in an application. Also, it is common that the `MetaData` object is accessed via an ORM-centric `registry` or [Declarative Base](https://docs.sqlalchemy.org/tutorial/metadata.html#tutorial-orm-declarative-base) base class, so that this same `MetaData` is shared among ORM- and Core-declared `Table` objects.

<https://docs.sqlalchemy.org/orm/mapping_api.html#sqlalchemy.orm.registry>

There can be multiple `MetaData` collections, as well. `Table` objects can refer to `Table` objects in other collections without restrictions. However, for groups of `Table` objects that are related to each other, it is more straightforward to have them set up within a single `MetaData` collection.

#### `Table` Components

The tables for a database that has been reflected into a `MetaData` object can be accessed via the `MetaData.tables` attribute:

```pycon
>>> md.tables
FacadeDict({'Album': Table('Album', MetaData(), Column('AlbumId', INTEGER(), table=<Album>, primary_key=True, nullable=False), Column('Title', NVARCHAR(length=160), table=<Album>, nullable=False), Column('ArtistId', INTEGER(), ForeignKey('Artist.ArtistId'), table=<Album>, nullable=False), schema=None), 'Artist': Table('Artist', MetaData(), Column('ArtistId', INTEGER(), table=<Artist>, primary_key=True, nullable=False), Column('Name', NVARCHAR(length=120), table=<Artist>), schema=None), 'Customer': Table('Customer', MetaData(), Column('CustomerId', INTEGER(), table=<Customer>, primary_key=True, nullable=False), Column('FirstName', NVARCHAR(length=40), table=<Customer>, nullable=False), Column('LastName', NVARCHAR(length=20), table=<Customer>, nullable=False), Column('Company', NVARCHAR(length=80), table=<Customer>), Column('Address', NVARCHAR(length=70), table=<Customer>), Column('City', NVARCHAR(length=40), table=<Customer>), Column('State', NVARCHAR(length=40), table=<Customer>), Column('Country', NVARCHAR(length=40), table=<Customer>), Column('PostalCode', NVARCHAR(length=10), table=<Customer>), Column('Phone', NVARCHAR(length=24), table=<Customer>), Column('Fax', NVARCHAR(length=24), table=<Customer>), Column('Email', NVARCHAR(length=60), table=<Customer>, nullable=False), Column('SupportRepId', INTEGER(), ForeignKey('Employee.EmployeeId'), table=<Customer>), schema=None), 'Employee': Table('Employee', MetaData(), Column('EmployeeId', INTEGER(), table=<Employee>, primary_key=True, nullable=False), Column('LastName', NVARCHAR(length=20), table=<Employee>, nullable=False), Column('FirstName', NVARCHAR(length=20), table=<Employee>, nullable=False), Column('Title', NVARCHAR(length=30), table=<Employee>), Column('ReportsTo', INTEGER(), ForeignKey('Employee.EmployeeId'), table=<Employee>), Column('BirthDate', DATETIME(), table=<Employee>), Column('HireDate', DATETIME(), table=<Employee>), Column('Address', NVARCHAR(length=70), table=<Employee>), Column('City', NVARCHAR(length=40), table=<Employee>), Column('State', NVARCHAR(length=40), table=<Employee>), Column('Country', NVARCHAR(length=40), table=<Employee>), Column('PostalCode', NVARCHAR(length=10), table=<Employee>), Column('Phone', NVARCHAR(length=24), table=<Employee>), Column('Fax', NVARCHAR(length=24), table=<Employee>), Column('Email', NVARCHAR(length=60), table=<Employee>), schema=None), 'Genre': Table('Genre', MetaData(), Column('GenreId', INTEGER(), table=<Genre>, primary_key=True, nullable=False), Column('Name', NVARCHAR(length=120), table=<Genre>), schema=None), 'Invoice': Table('Invoice', MetaData(), Column('InvoiceId', INTEGER(), table=<Invoice>, primary_key=True, nullable=False), Column('CustomerId', INTEGER(), ForeignKey('Customer.CustomerId'), table=<Invoice>, nullable=False), Column('InvoiceDate', DATETIME(), table=<Invoice>, nullable=False), Column('BillingAddress', NVARCHAR(length=70), table=<Invoice>), Column('BillingCity', NVARCHAR(length=40), table=<Invoice>), Column('BillingState', NVARCHAR(length=40), table=<Invoice>), Column('BillingCountry', NVARCHAR(length=40), table=<Invoice>), Column('BillingPostalCode', NVARCHAR(length=10), table=<Invoice>), Column('Total', NUMERIC(precision=10, scale=2), table=<Invoice>, nullable=False), schema=None), 'InvoiceLine': Table('InvoiceLine', MetaData(), Column('InvoiceLineId', INTEGER(), table=<InvoiceLine>, primary_key=True, nullable=False), Column('InvoiceId', INTEGER(), ForeignKey('Invoice.InvoiceId'), table=<InvoiceLine>, nullable=False), Column('TrackId', INTEGER(), ForeignKey('Track.TrackId'), table=<InvoiceLine>, nullable=False), Column('UnitPrice', NUMERIC(precision=10, scale=2), table=<InvoiceLine>, nullable=False), Column('Quantity', INTEGER(), table=<InvoiceLine>, nullable=False), schema=None), 'Track': Table('Track', MetaData(), Column('TrackId', INTEGER(), table=<Track>, primary_key=True, nullable=False), Column('Name', NVARCHAR(length=200), table=<Track>, nullable=False), Column('AlbumId', INTEGER(), ForeignKey('Album.AlbumId'), table=<Track>), Column('MediaTypeId', INTEGER(), ForeignKey('MediaType.MediaTypeId'), table=<Track>, nullable=False), Column('GenreId', INTEGER(), ForeignKey('Genre.GenreId'), table=<Track>), Column('Composer', NVARCHAR(length=220), table=<Track>), Column('Milliseconds', INTEGER(), table=<Track>, nullable=False), Column('Bytes', INTEGER(), table=<Track>), Column('UnitPrice', NUMERIC(precision=10, scale=2), table=<Track>, nullable=False), schema=None), 'MediaType': Table('MediaType', MetaData(), Column('MediaTypeId', INTEGER(), table=<MediaType>, primary_key=True, nullable=False), Column('Name', NVARCHAR(length=120), table=<MediaType>), schema=None), 'Playlist': Table('Playlist', MetaData(), Column('PlaylistId', INTEGER(), table=<Playlist>, primary_key=True, nullable=False), Column('Name', NVARCHAR(length=120), table=<Playlist>), schema=None), 'PlaylistTrack': Table('PlaylistTrack', MetaData(), Column('PlaylistId', INTEGER(), ForeignKey('Playlist.PlaylistId'), table=<PlaylistTrack>, primary_key=True, nullable=False), Column('TrackId', INTEGER(), ForeignKey('Track.TrackId'), table=<PlaylistTrack>, primary_key=True, nullable=False), schema=None)})
>>> list(md.tables.keys())
['Album', 'Artist', 'Customer', 'Employee', 'Genre', 'Invoice', 'InvoiceLine', 'Track', 'MediaType', 'Playlist', 'PlaylistTrack']
>>> 
```

A `Table` object can be created from the `MetaData` object with the table data:

```pycon
>>> artist = md.tables['Artist']
>>> artist
Table('Artist', MetaData(), Column('ArtistId', INTEGER(), table=<Artist>, primary_key=True, nullable=False), Column('Name', NVARCHAR(length=120), table=<Artist>), schema=None)
>>> 
```

There many useful attributes for a `Table` object:

```pycon
>>> for attr in dir(artist):
...    if not attr.startswith('_'):
...        print(attr)
add_is_dependent_on
alias
allows_lambda
append_column
append_constraint
argument_for
autoincrement_column
c
columns
comment
compare
compile
constraints
corresponding_column
create
create_drop_stringify_dialect
delete
description
dialect_kwargs
dialect_options
dispatch
drop
entity_namespace
exported_columns
foreign_key_constraints
foreign_keys
fullname
get_children
implicit_returning
indexes
info
inherit_cache
insert
is_clause_element
is_derived_from
is_dml
is_selectable
join
key
kwargs
lateral
memoized_attribute
memoized_instancemethod
metadata
name
named_with_column
outerjoin
params
primary_key
replace_selectable
schema
select
selectable
self_group
stringify_dialect
supports_execution
table_valued
tablesample
to_metadata
tometadata
unique_params
update
uses_inspection
>>> 
```

A `Table` object's columns can be examined via the `Table.columns` (or `Table.c`) attribute, which returns an associative array:

```pycon
>>> list(artist.c)  # View all Column objects
[Column('ArtistId', INTEGER(), table=<Artist>, primary_key=True, nullable=False),
 Column('Name', NVARCHAR(length=120), table=<Artist>)]
>>> artist.c.Name  # View specific Column object
Column('Name', NVARCHAR(length=120), table=<Artist>)
>>> artist.c.keys()  # Obtain column names as strings
['ArtistId', 'Name']
>>> 
```

Usually, the `Column` includes a string name and a type object. The `TEXT` and `INTEGER` classes represent SQL datatypes. They can be passed to a `Column`, with or without being instantiated, when using the `Table` constructor.

- <https://docs.sqlalchemy.org/core/type_basics.html#sqlalchemy.types.Text>
- <https://docs.sqlalchemy.org/core/type_basics.html#sqlalchemy.types.Integer>

#### Simple `Table` Constraints

The `ArtistId` column includes the `Column.primary_key` parameter that is a shorthand technique for indicating that this `Column` is/should be part of the primary key for this table. Normally, the primary key itself is implicitly declared and is represented by the `PrimaryKeyConstraint` construct, which we can see via the `Table.primary_key` attribute of the `Table` object:

```pycon
>>> artist.primary_key
PrimaryKeyConstraint(Column('ArtistId', INTEGER(), table=<Artist>, primary_key=True, nullable=False))
>>> 
```

A `ForeignKeyConstraint` that only involves a single column on the column table is typically declared using a column-level shorthand notation via the `ForeignKey` object. When using the `ForeignKey` object within a `Column` definition, we can omit the datatype for that `Column`. It is automatically inferred from that of the related column.

<https://docs.sqlalchemy.org/core/constraints.html#sqlalchemy.schema.ForeignKey>

The table above also has a third kind of constraint, which in SQL is the `NOT NULL` constraint, indicated using the `Column.nullable` parameter.

<https://docs.sqlalchemy.org/core/metadata.html#sqlalchemy.schema.Column.params.nullable>

If you have created a non-reflected `MetaData` object and `Table` objects, you can use `MetaData.create_all()` to send `CREATE TABLE` statements (or [Data Definition Language (DDL)](https://docs.sqlalchemy.org/glossary.html#term-DDL)) to the target database.

`md.create_all(engine)`

<https://docs.sqlalchemy.org/core/metadata.html#sqlalchemy.schema.MetaData.create_all>

#### Emitting DDL to the Database

The DDL create process includes database-specific statements that test for the existence of each table before emitting a `CREATE`. The full series of steps are also included within a `BEGIN`/`COMMIT` pair to accommodate for transactional DDL.

The create process also takes care of emitting `CREATE` statements in the correct order.

In addition, the `MetaData` object features a `MetaData.drop_all()` method that will emit `DROP` statements in the reverse order as it would emit `CREATE` statements in order to drop schema elements.

For management of an application database schema over a long term, a schema management tool like [Alembic](https://alembic.sqlalchemy.org/), which builds upon SQLAlchemy, is recommended, as it can manage and orchestrate the process of incrementally altering a fixed database schema over time as the application design changes.

### Table Reflection

Table [reflection](https://docs.sqlalchemy.org/core/reflection.html) refers to the process of generating `Table` and related objects by reading the current state of a database. Instead of declaring `Table` objects in Python and then emitting DDL to the database to generate a schema, the reflection process does these steps in reverse, starting from an existing database and generating in-Python data structures to represent the schemas within that database.

## Using `INSERT` Statements

When using Core or the ORM, a SQL `INSERT` statement is directly generated  using the `.insert()` method. This method generates a new instance of `Insert`, which represents an `INSERT` statement in SQL that adds new data into a table.

<https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Insert>

### The `.insert()` SQL Expression Construct

```pycon
>>> genre = md.tables['Genre']
>>> query = sa.insert(genre).values(
...     GenreId=28,
...     Name='Soul'
... )
>>> print(query)
INSERT INTO "Genre" ("GenreId", "Name") VALUES (:GenreId, :Name)
>>> 
```

The above `query` is an instance of `Insert`. Most SQL expressions can be stringified in place as a means of seeing the general form of what is being produced. The stringified form is created by producing a `Compiled` form of the object, which includes a database-specific string SQL representation of the statement.

This object can be directly acquired using the `ClauseElement.compile()` method:

`compiled = query.compile()`

- <https://docs.sqlalchemy.org/core/internals.html#sqlalchemy.engine.Compiled>
- <https://docs.sqlalchemy.org/core/foundation.html#sqlalchemy.sql.expression.ClauseElement.compile>

The `Insert` construct is an example of a _parameterized_ construct. The bound parameters can be viewed via the `.params` attribute:

```pycon
>>> compiled.params
{'GenreId': 28, 'Name': 'Soul'}
>>> 
```

### Executing the Statement

`query` can be used to `INSERT` a row into `genre`:

```python
try:
    with engine.begin() as conn:
        result = conn.execute(query)
except Exception as exc:
    print(exc)
```

In its simple form, the `INSERT` does not return any rows. If only a single row is inserted, it will usually include the ability to return information about column-level default values that were generated during the `INSERT` of that row, most commonly an integer primary key value. We can examine the inserted primary key using the `CursorResult.inserted_primary_key` accessor:

```pycon
>>> result.inserted_primary_key
(28,)
>>> 
```

<https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.CursorResult.inserted_primary_key>

`CursorResult.inserted_primary_key` returns a tuple because a primary key may contain multiple columns (i.e., a [composite primary key](https://docs.sqlalchemy.org/glossary.html#term-composite-primary-key)).

### `VALUES` Clause Generation

The example above used the `Insert.values()` method to explicitly create the `Values` clause of the SQL `INSERT` statement. If we do not use `Insert.values()`, we get an `INSERT` for every column in the table:

```pycon
>>> print(sa.insert(genre))
INSERT INTO "Genre" ("GenreId", "Name") VALUES (:GenreId, :Name)
>>> 
```

<https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Insert.values>

If we take an `Insert` construct that has not had `Insert.values()` called upon it and execute it, the statement will be compiled to a string based on the parameters that we passed to the `Connection.execute()` method, and only include columns relevant to the parameters that were passed.

This is the usual way that `Insert` is used to insert rows without having to type out an explicit `VALUES` clause.


```python
query = sa.insert(genre)
data = [
    {'GenreId': '29', 'Name': 'Jazz Rock'},
    {'GenreId': '30', 'Name': 'Swing'}
]

try:
    with engine.begin() as conn:
        result = conn.execute(query, data)
except Exception as exc:
    print(exc)
```

The execution above features the _executemany_ form previously demonstrated. However, unlike when using the `text()` construct, we did not have to delineate our SQL. By passing a dictionary or list of dictionaries to `Connection.execute()` in conjunction with the `Insert` construct, the `Connection` ensured that the column names that were passed were expressed in the `VALUES` clause of the `INSERT` construct automatically.

If the database backend supports it (e.g., SQLite), an _empty_ `INSERT` that only inserts the default values for a table without including any explicit values can be generated by using `Insert.values()` without any arguments:

```pycon
>>> print(sa.insert(genre).values().compile(engine))
INSERT INTO "Genre" DEFAULT VALUES
>>> 
```

### `INSERT`...`RETURNING`

For supported backends, the `RETURNING` clause is automatically used to retrieve the last inserted primary key value, as well as the values for server defaults. However, the `RETURNING` clause may also be explicitly used via the `Insert.returning()` method. For this example, the `Result` object that is returned when the statement is executed has rows that can be fetched:

```pycon
>>> query = sa.insert(genre).returning(genre.c.GenreId, genre.c.Name)
>>> print(query)
INSERT INTO "Genre" ("GenreId", "Name") VALUES (:GenreId, :Name) RETURNING "Genre"."GenreId", "Genre"."Name"
>>> 
```

<https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Insert.returning>

It can also be combined with `Insert.from_select()`.

<https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Insert.from_select>

The `RETURNING` feature is also supported by `UPDATE` and `DELETE` statements.

For `INSERT` statements, the `RETURNING` feature may be used for both single-row statements, as well as for statements that `INSERT` multiple-row rows at once. Support for multiple row `INSERT` with `RETURNING` is dialect specific. However, it is [supported for all the dialects](https://docs.sqlalchemy.org/core/connections.html#engine-insertmanyvalues) that are included in SQAlchemy that support `RETURNING`.

Bulk `INSERT` with or without `RETURNING` is also [supported by the ORM](https://docs.sqlalchemy.org/orm/queryguide/dml.html#orm-queryguide-bulk-insert).

### `INSERT`...`SELECT`

The `Insert` construct can compose an `INSERT` that directly gets rows from a `SELECT` using the `Insert.from_select()` method. This method accepts a `select()` construct, along with a list of column names to be targeted in the `INSERT`.

- <https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Insert.from_select>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.select>

This construct is used to copy data from another part of the database directly into a new set of rows, without actually fetching and re-sending the data from the client.

## Using `SELECT` Statements

For both Core and ORM, the `select()` function generates a `Select` construct, which is used for all `SELECT` queries via passing to methods like `Connection.execute()` in Core and `Session.execute()` in ORM. A `SELECT` statement is emitted in the current transaction and the result rows available via the returned `Result` object.

<https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select>

### The `select()` SQL Expression Construct

The `select()` construct builds up a statement in the same way that `insert()` does, using a [generative](https://docs.sqlalchemy.org/glossary.html#term-generative) approach where each method builds more state onto the object. Like other SQL constructs, it can be stringified in place:

```pycon
>>> artist = md.tables['Artist']
>>> query = (
...     sa.select(artist)
...       .where(artist.c.Name == 'Emerson String Quartet')
... )
>>> print(query)
SELECT "Artist"."ArtistId", "Artist"."Name" 
FROM "Artist" 
WHERE "Artist"."Name" = :Name_1
>>> track = md.tables['Track']
>>> query = (
...     sa.select(track)
...       .where(sa.not_(track.c.Name.icontains('now')))
... )
>>> print(query)
SELECT "Track"."TrackId", "Track"."Name", "Track"."AlbumId", "Track"."MediaTypeId", "Track"."GenreId", "Track"."Composer", "Track"."Milliseconds", "Track"."Bytes", "Track"."UnitPrice" 
FROM "Track" 
WHERE (lower("Track"."Name") NOT LIKE '%' || lower(:Name_1) || '%')
>>> print(sa.select(track.c.GenreId).distinct())
SELECT DISTINCT "Track"."GenreId" 
FROM "Track"
>>> print(sa.select(sa.func.count(sa.distinct(track.c.GenreId))))
SELECT count(DISTINCT "Track"."GenreId") AS count_1 
FROM "Track"
>>> 
```

- <https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.not_>
- <https://docs.sqlalchemy.org/core/metadata.html#sqlalchemy.schema.Column.icontains>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.distinct>
- <https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.distinct>

Like with the other statement-level SQL constructs, we run the statement by passing it to an execution method. Since a `SELECT` statement returns rows, we can iterate the result object to get `Row` objects:

```python
try:
    with engine.begin() as conn:
        for row in conn.execute(query):
            print(row)
except Exception as exc:
    print(exc)
# (272, 'Emerson String Quartet')
```

<https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.Row>

### Setting the `COLUMNS` and `FROM` Clause

The `select()` function accepts positional elements representing any number of `Column` and/or `Table` expressions, as well as a wide range of compatible objects, which are resolved into a list of SQL expressions to be `SELECT`ed from, which will be returned as columns in the result set. In simpler cases, these elements also serve to create the `FROM` clause, which is inferred from the columns and table-like expressions passed:

```pycon
>>> track = md.tables['Track']
>>> print(sa.select(track))
SELECT "Track"."TrackId", "Track"."Name", "Track"."AlbumId", "Track"."MediaTypeId", "Track"."GenreId", "Track"."Composer", "Track"."Milliseconds", "Track"."Bytes", "Track"."UnitPrice" 
FROM "Track"
>>> 
```

To `SELECT` from individual columns using a Core approach, the `Column` objects are accessed from the `Table.c` accessor and can be directly sent. The `FROM` clause will be inferred as the set of all `Table` and other `FromClause` objects that are represented by those columns:

```pycon
>>> print(sa.select(track.c.Name, track.c.Composer))
SELECT "Track"."Name", "Track"."Composer" 
FROM "Track"
>>> 
```

- <https://docs.sqlalchemy.org/core/metadata.html#sqlalchemy.schema.Table.c>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.FromClause>

Alternatively, when using the `FromClause.c` collection of any `FromClause` (e.g., `Table`), multiple columns may be specified for a `select()`:

```pycon
>>> print(sa.select(track.c['Name', 'Composer']))
SELECT "Track"."Name", "Track"."Composer" 
FROM "Track"
>>> 
```

<https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.FromClause.c>

### Selecting From Labeled SQL Expressions

The `ColumnElement.label()` method provides a SQL label of a column or expression, allowing it to have a specific name in a result set. This can be helpful when referring to arbitrary SQL expressions in a result row by name:

```python
fltr = (
    (track.c.TrackId == 2368)
    | (track.c.TrackId == 2380)
    | (track.c.TrackId == 2393)
)
query = (
    sa.select(('Track: ' + track.c.Name).label('track_name'))
      .where(fltr)
      .order_by(track.c.TrackId)
)

try:
    with engine.begin() as conn:
        for row in conn.execute(query):
            print(f'{row.track_name}')
except Exception as exc:
    print(exc)
```

```pycon
Track: Under The Bridge
Track: The Zephyr Song
Track: Scar Tissue
```

<https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.ColumnElement.label>

Created label names [may also be referred to](https://docs.sqlalchemy.org/tutorial/data_select.html#tutorial-order-by-label) in the `ORDER BY` or `GROUP BY` clause of the `Select`.

### Selecting With Textual Column Expressions

When we construct a `Select` object using the `select()` function, we are normally passing it a series of `Table` and `Column` objects that were defined using [table metadata](https://docs.sqlalchemy.org/tutorial/metadata.html#tutorial-working-with-metadata), or when using the ORM, we may be sending ORM-mapped attributes that represent table columns. However, there is sometimes a need to manufacture arbitrary SQL blocks inside of statements (e.g., constant string expressions, or arbitrary SQL) that is faster to literally write.

The `text()` construct can be directly embedded into a `Select` construct:

```python
query = (
    sa.select(sa.text("'who is'"), track.c.Name)
      .where(track.c.TrackId == 3099)
)

try:
    with engine.begin() as conn:
        print(conn.execute(query).all())
except Exception as exc:
    print(exc)
# [('who is', 'Josephina')]
```

While the `text()` construct can be used in most places to inject literal SQL phrases, we are usually dealing with textual units that each represent an individual column expression. In this case, we can get more functionality out of our textual fragment using the `literal_column()` construct, instead.

This object is similar to `text()`, except that instead of representing arbitrary SQL of any form, it explicitly represents a single _column_, and can then be labeled and referred to in subqueries and other expressions:

```python
query = (
    sa.select(sa.literal_column("'who is'").label('q'), track.c.Name)
      .where(track.c.TrackId == 3099)
)

try:
    with engine.begin() as conn:
        for row in conn.execute(query):
            print(f'{row.q.title()} {row.Name}?')
except Exception as exc:
    print(exc)
# Who Is Josephina?
```

<https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.literal_column>

Whether using `text()` or `literal_column()`, we are writing _syntactical SQL expressions_ and not literal values. Therefore, we have to include whatever quoting or syntaxes are required for the SQL we want to see rendered.

### The `WHERE` Clause

SQLAlchemy allows us to compose SQL expressions by making use of standard Python operators in conjunction with `Column` and similar objects. For boolean expressions, most Python operators generate new SQL Expression objects, rather than plain boolean `True`/`False` values:

```pycon
>>> print(track.c.Name == 'Paint It Black')
"Track"."Name" = :Name_1
>>> print(track.c.TrackId > 2000)
"Track"."TrackId" > :TrackId_1
>>> query = (
...     sa.select(track)
...       .where(track.c.TrackId.between(10, 20))
... )
>>> print(query)
SELECT "Track"."TrackId", "Track"."Name", "Track"."AlbumId", "Track"."MediaTypeId", "Track"."GenreId", "Track"."Composer", "Track"."Milliseconds", "Track"."Bytes", "Track"."UnitPrice" 
FROM "Track" 
WHERE "Track"."TrackId" BETWEEN :TrackId_1 AND :TrackId_2
>>> 
```

- <https://docs.sqlalchemy.org/core/metadata.html#sqlalchemy.schema.Column.between>

These expressions can be used to generate the `WHERE` clause by passing the resulting objects to the `Select.where()` method:

```pycon
>>> print(sa.select(track).where(track.c.Name == 'Paint It Black'))
SELECT "Track"."TrackId", "Track"."Name", "Track"."AlbumId", "Track"."MediaTypeId", "Track"."GenreId", "Track"."Composer", "Track"."Milliseconds", "Track"."Bytes", "Track"."UnitPrice" 
FROM "Track" 
WHERE "Track"."Name" = :Name_1
>>> 
```

The `Select.where()` method can be invoked multiple times to produce multiple expressions joined by `AND`:

```python
query = (
    sa.select(track)
      .where(track.c.TrackId == 2260)
      .where(track.c.Name == "Don't Stop Me Now")
)

try:
    with engine.begin() as conn:
        print(conn.execute(query).all())
except Exception as exc:
    print(exc)
# [(2260, "Don't Stop Me Now", 185, 1, 1, 'Mercury, Freddie', 211826, 6896666, Decimal('0.99'))]
```

A single call to `Select.where()` also accepts multiple expressions with the same effect:

```python
query = (
    sa.select(track)
      .where(track.c.TrackId == 2260, track.c.Name == "Don't Stop Me Now")
)

try:
    with engine.begin() as conn:
        print(conn.execute(query).all())
except Exception as exc:
    print(exc)
# [(2260, "Don't Stop Me Now", 185, 1, 1, 'Mercury, Freddie', 211826, 6896666, Decimal('0.99'))]
```

`AND` and `OR` conjunctions are both directly available using the `and_()` and `or_()` functions:

```python
query = (
    sa.select(track)
      .where(
          sa.and_(
              sa.or_(track.c.Name == 'Alive', track.c.Name == 'Jeremy'),
              track.c.AlbumId == 181
          )
      )
)

try:
    with engine.begin() as conn:
        print(conn.execute(query).all())
except Exception as exc:
    print(exc)
# [(2195, 'Alive', 181, 1, 1, 'Stone Gossard', 341080, 11176623, Decimal('0.99')), (2198, 'Jeremy', 181, 1, 1, 'Jeff Ament', 318981, 10447222, Decimal('0.99'))]
```

- <https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.and_>
- <https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.or_>

For simple _equality_ comparisons against a single entity, there is `Select.filter_by()`, which accepts keyword arguments that match to column keys or ORM attribute names. It will filter against the leftmost `FROM` clause or the last entity joined:

```pycon
>>> print(sa.select(track).filter_by(Name='Bullet With Butterfly Wings',
                                     Composer='Billy Corgan'))
SELECT "Track"."TrackId", "Track"."Name", "Track"."AlbumId", "Track"."MediaTypeId", "Track"."GenreId", "Track"."Composer", "Track"."Milliseconds", "Track"."Bytes", "Track"."UnitPrice" 
FROM "Track" 
WHERE "Track"."Name" = :Name_1 AND "Track"."Composer" = :Composer_1
>>> 
```

<https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.filter_by>

### Explicit `FROM` Clauses and `JOIN`s

Usually, the `FROM` clause is inferred based on the expression that was set in the columns clause, as well as other elements of the `Select`. If we set a single column from a specific `Table` in the `COLUMNS` clause, it puts that `Table` in the `FROM` clause, as well:

```pycon
>>> print(sa.select(album.c.Title))
SELECT "Album"."Title" 
FROM "Album"
>>> 
```

If we put columns from two tables, we get a comma-separated `FROM` clause:

```pycon
>>> print(sa.select(album.c.Title, artist.c.Name))
SELECT "Album"."Title", "Artist"."Name" 
FROM "Album", "Artist"
>>> 
```

In order to `JOIN` these tables together, we use one of two methods on `Select`. The first is the `Select.join_from()` method, which allows us to explicitly indicate the left and right side of the `JOIN`:

```pycon
>>> print(sa.select(album.c.Title, artist.c.Name)
...         .join_from(album, artist))
SELECT "Album"."Title", "Artist"."Name" 
FROM "Album" JOIN "Artist" ON "Artist"."ArtistId" = "Album"."ArtistId"
>>> 
```

<https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.join_from>

The other is the `Select.join()` method, which indicates only the right side of the `JOIN` (the left side is inferred):

```pycon
>>> print(sa.select(album.c.Title, artist.c.Name).join(artist))
SELECT "Album"."Title", "Artist"."Name" 
FROM "Album" JOIN "Artist" ON "Artist"."ArtistId" = "Album"."ArtistId"
>>> 
```

<https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.join>

When using `Select.join_from()` or `Select.join()`, the `ON` clause of the join is also inferred in simple foreign key cases.

Also, we have the option to explicitly add elements to the `FROM` clause, if it is not inferred the way we want from the columns clause. We use the `Select.select_from()` method to achieve this:

```pycon
>>> print(sa.select(artist.c.Name).select_from(album).join(artist))
SELECT "Artist"."Name" 
FROM "Album" JOIN "Artist" ON "Artist"."ArtistId" = "Album"."ArtistId"
>>> 
```

<https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.select_from>

Another time `Select.select_from()` may be useful is if our columns do not have enough information to provide for a `FROM` clause. For example, to `SELECT` from the SQL expression `COUNT(*)`, we use the SQLAlchemy element known as `sqlalchemy.sql.expression.func` to produce the SQL `COUNT()` function:

```pycon
>>> print(sa.select(sa.func.count('*')).select_from(artist))
SELECT count(:count_2) AS count_1 
FROM "Artist"
>>> 
```

<https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.func>

### Setting the `ON` Clause

Previously, the `Select` construct joined two tables and automatically produced the `ON` clause because the `album` and `artist` `Table` objects included a single `ForeignKeyConstraint` definition, which was used to form the `ON` clause.

If the left and right targets of the join do not have such a constraint, or there are multiple constraints in place, we need to explicitly specify the `ON` clause. Both `Select.join()` and `Select.join_from()` accept an additional argument for the `ON` clause:

```pycon
>>> print(sa.select(artist.c.Name)
...         .select_from(album)
...         .join(artist, album.c.ArtistId == artist.c.ArtistId)
... )
SELECT "Artist"."Name" 
FROM "Album" JOIN "Artist" ON "Album"."ArtistId" = "Artist"."ArtistId"
>>> 
```

<https://docs.sqlalchemy.org/core/constraints.html#sqlalchemy.schema.ForeignKeyConstraint>

### `OUTER` and `FULL` Join

Both the `Select.join()` and `Select.join_from()` methods accept keyword arguments `Select.join.isouter` and `Select.join.full`, which will render `LEFT OUTER JOIN` and `FULL OUTER JOIN`, respectively:

```pycon
>>> print(sa.select(album).join(artist, isouter=True))
SELECT "Album"."AlbumId", "Album"."Title", "Album"."ArtistId" 
FROM "Album" LEFT OUTER JOIN "Artist" ON "Artist"."ArtistId" = "Album"."ArtistId"
>>> print(sa.select(album).join(artist, full=True))
SELECT "Album"."AlbumId", "Album"."Title", "Album"."ArtistId" 
FROM "Album" FULL OUTER JOIN "Artist" ON "Artist"."ArtistId" = "Album"."ArtistId"
>>> 
```

- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.join.params.isouter>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.join.params.full>

Also, there is a method `Select.outerjoin()` that is equivalent to using `.join(..., isouter=True)`.

<https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.outerjoin>

SQL has a `RIGHT OUTER JOIN`, but SQLAlchemy does not directly render this. Instead, reverse the order of the tables and use `LEFT OUTER JOIN`.

### `ORDER BY`, `GROUP BY`, `HAVING`

The `SELECT` SQL statement includes a clause called `ORDER BY`, which is used to return the selected rows within a given ordering.

The `GROUPBY` clause is similarly constructed to the `ORDER BY` clause, and has the purpose of sub-dividing the selected rows into specific groups upon which aggregate functions may be invoked. The `HAVING` clause is usually used with `GROUPBY` and is of a similar form as the `WHERE` clause, except that it is applied to the aggregated functions used within groups.

#### `ORDER BY`

The `ORDER BY` clause is constructed in terms of SQL Expression constructs typically based on `Column` or similar objects. The `Select.order_by()` method accepts one or more of these expressions positionally:

```pycon
>>> print(sa.select(album).order_by(album.c.Title))
SELECT "Album"."AlbumId", "Album"."Title", "Album"."ArtistId" 
FROM "Album" ORDER BY "Album"."Title"
>>> 
```

<https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.order_by>

Ascending/descending is available from the `ColumnElement.asc()` and `ColumnElement.desc()` modifiers, which are present from ORM-bound attributes, as well:

```pycon
>>> print(sa.select(album).order_by(album.c.Title.desc()))
SELECT "Album"."AlbumId", "Album"."Title", "Album"."ArtistId" 
FROM "Album" ORDER BY "Album"."Title" DESC
>>> 
```

- <https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.ColumnElement.asc>
- <https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.ColumnElement.desc>

#### Aggregate Functions With `GROUP BY`/`HAVING`

In SQL, aggregate functions allow column expressions across multiple rows to be aggregated together to produce a single result. SQLAlchemy provides for SQL functions in an open-ended way using a namespace known as `func`. This is a special constructor that will create new instances of `Function` when given the name of a particular SQL function (which can have any name), as well as zero or more arguments to pass to the function, which are SQL Expression constructs.

```pycon
>>> count_fn = sa.func.count(album.c.AlbumId)
>>> print(count_fn)
count("Album"."AlbumId")
>>> 
```

<https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.Function>

When using aggregate functions in SQL, the `GROUP BY` clause is essential in that it allows rows to be partitioned into groups where aggregate functions will be individually applied to each group. When requesting non-aggregated columns in the `COLUMNS` clause of a `SELECT` statement, SQL requires that these columns all be subject to a `GROUP BY` clause, either directly or indirectly based on a primary key association.

THe `HAVING` clause is used in a similar manner as the `WHERE` clause, except that it filters out rows based on aggregated values rather than direct row contents.

SQLAlchemy provides for these two clauses using the `Select.group_by()` and `Select.having()` methods.

```python
try:
    with engine.begin() as conn:
        result = conn.execute(
            sa.select(artist.c.Name,
                      sa.func.count(album.c.AlbumId).label('count'))
              .join(album)
              .group_by(artist.c.Name)
              .having(sa.func.count(album.c.AlbumId) > 1)
        )
except Exception as exc:
    print(exc)
else:
    print(result.all())
```

```pycon
[('AC/DC', 2), ('Accept', 2), ('Amy Winehouse', 2), ('Antônio Carlos Jobim', 2), ('Audioslave', 3), ('Battlestar Galactica', 2), ('Berliner Philharmoniker & Herbert Von Karajan', 3), ('Black Label Society', 2), ('Black Sabbath', 2), ('Caetano Veloso', 2), ('Chico Science & Nação Zumbi', 2), ('Cidade Negra', 2), ('Creedence Clearwater Revival', 2), ('Cássia Eller', 2), ('Deep Purple', 11), ('Djavan', 2), ('English Concert & Trevor Pinnock', 2), ('Eric Clapton', 2), ('Eugene Ormandy', 3), ('Faith No More', 4), ('Foo Fighters', 4), ('Gilberto Gil', 3), ('Green Day', 2), ("Guns N' Roses", 3), ('Iron Maiden', 21), ('Jamiroquai', 3), ('Kiss', 2), ('Led Zeppelin', 14), ('Legião Urbana', 2), ('Lost', 4), ('Lulu Santos', 2), ('Metallica', 10), ('Michael Tilson Thomas & San Francisco Symphony', 2), ('Miles Davis', 3), ('Milton Nascimento', 2), ('Nirvana', 2), ('Os Paralamas Do Sucesso', 3), ('Ozzy Osbourne', 6), ('Pearl Jam', 5), ('Queen', 3), ('R.E.M.', 3), ('Red Hot Chili Peppers', 3), ('Santana', 3), ('Skank', 2), ('Smashing Pumpkins', 2), ('Spyro Gyra', 2), ('The Black Crowes', 2), ('The Cult', 2), ('The Office', 3), ('The Rolling Stones', 3), ('The Tea Party', 2), ('Tim Maia', 2), ('Titãs', 2), ('U2', 10), ('Van Halen', 4), ('Various Artists', 4)]
```

- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.group_by>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.having>

#### Ordering or Grouping By a Label

An important technique is the ability to `ORDER BY` or `GROUP BY` an expression that is already stated in the columns clause without re-stating the expression in the `ORDER BY` or `GROUP BY` clause, and instead using the column name or labeled name from the `COLUMNS` clause.

This form is available by passing the string text of the name to the `Select.order_by()` or `Select.group_by()` method. The text passed in is _not directly rendered_. Instead, the name given to an expression in the columns clause is rendered as that expression name in context, raising an error if no match is found. The unary modifiers `asc()` and `desc()` may also be used in this form:

```pycon
>>> stmt = (
...    sa.select(album.c.ArtistId,
...              sa.func.count(album.c.AlbumId).label('num_albums'))
...                .group_by('AlbumId')
...                .order_by('AlbumId', sa.desc('num_albums'))
... )
... print(stmt)
SELECT "Album"."ArtistId", count("Album"."AlbumId") AS num_albums 
FROM "Album" GROUP BY "Album"."AlbumId" ORDER BY "Album"."AlbumId", num_albums DESC
>>> 
```

### Using Aliases

To refer to the same table multiple times in the `FROM` clause of a statement, use SQL _aliases_, which are a syntax that supplies an alternative name to a table or subquery from which it can be referred to in the statement.

In the SQLAlchemy Expression Language, these _names_ are represented by `FromClause` objects known as the `Alias` construct, which is constructed in code using the `FromClause.alias()` method. An `Alias` construct is like a `Table` construct in that it also has a namespace of `Column` objects within the `Alias.c` collection.

```pycon
>>> artist_alias_1 = artist.alias()
>>> artist_alias_2 = artist.alias()
>>> print(
...     sa.select(artist_alias_1.c.Name, artist_alias_2.c.Name)
...       .join_from(artist_alias_1, artist_alias_2,
...                  artist_alias_1.c.ArtistId > artist_alias_2.c.ArtistId)
... )
SELECT "Artist_1"."Name", "Artist_2"."Name" AS "Name_1" 
FROM "Artist" AS "Artist_1" JOIN "Artist" AS "Artist_2" ON "Artist_1"."ArtistId" > "Artist_2"."ArtistId"
>>> 
```

- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Alias>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.FromClause.alias>

### Subqueries and CTEs

A subquery in SQL is a `SELECT` statement that is rendered within parenthesis and placed within the context of an enclosing statement, typically (but not necessarily) a `SELECT` statement.

SQLAlchemy uses the `Subquery` object to represent a subquery and the `CTE` object to represent a Common Table Expression (CTE), usually obtained from the `Select.subquery()` and `Select.cte()` methods, respectively. Either object can be used as a `FROM` element inside of a larger `select()` construct.

- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Subquery>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.CTE>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.subquery>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Select.cte>

We can construct a `Subquery` that will select an aggregate count of rows from the `artist` table:

```python
subq = (
    sa.select(sa.func.count(album.c.AlbumId).label('count'),
              album.c.ArtistId)
      .group_by(album.c.ArtistId)
      .subquery()
)
```

Stringifying the subquery by itself without it being embedded inside of another `Select` or other statement produces the plain `SELECT` statement without any enclosing parenthesis:

```pycon
>>> print(subq)
SELECT count("Album"."AlbumId") AS count, "Album"."ArtistId" 
FROM "Album" GROUP BY "Album"."ArtistId"
>>> 
```

The `Subquery` object behaves like any other `FROM` object (e.g., a `Table`), notably that it includes a `Subquery.c` namespace of the columns that it selects. We can use this namespace to refer to both the `ArtistId` colum, as well as our custom labeled `count` expression:

```pycon
>>> print(sa.select(subq.c.ArtistId, subq.c.count))
print(sa.select(subq.c.ArtistId, subq.c.count))
SELECT anon_1."ArtistId", anon_1.count 
FROM (SELECT count("Album"."AlbumId") AS count, "Album"."ArtistId" AS "ArtistId" 
FROM "Album" GROUP BY "Album"."ArtistId") AS anon_1
>>> 
```

With a selection of rows contained within the `subq` object, we can apply the object to a larger `Select` that will join the data to the `artist` table:

```pycon
>>> stmt = (
...     sa.select(artist.c.Name, artist.c.ArtistId, subq.c.count)
...       .join_from(artist, subq)
... )
>>> print(stmt)
SELECT "Artist"."Name", "Artist"."ArtistId", anon_1.count 
FROM "Artist" JOIN (SELECT count("Album"."AlbumId") AS count, "Album"."ArtistId" AS "ArtistId" 
FROM "Album" GROUP BY "Album"."ArtistId") AS anon_1 ON "Artist"."ArtistId" = anon_1."ArtistId"
>>> 
```

In order to join from `artist` to `album`, we made use of the `Select.join_from()` method. The `ON` clause of this join was _inferred_ based on foreign key constraints.

Even though a SQL subquery does not itself have any constraints, SQLAlchemy can act upon constraints represented on the columns by determining that the `subq.c.ArtistId` column is _derived_ from the `album.c.ArtistId` column, which does express a foreign key relationship back to the `artist.c.ArtistId` column, which is then used to generate the `ON` clause.

#### Common Table Expressions (CTEs)

Usage of the `CTE` construct in SQLAlchemy is virtually the same as how the `Subquery` construct is used. By changing the invocation of the `Select.subquery()` method to use `Select.cte()` instead, we can use the resulting object as a `FROM` element in the same way, but the SQL rendered is very different common table expression syntax:

```pycon
>>> subq = (
...     sa.select(sa.func.count(album.c.AlbumId).label('count'), album.c.ArtistId)
...       .group_by(album.c.ArtistId)
...       .cte()
... )
>>> stmt = (
...     sa.select(artist.c.Name, artist.c.ArtistId, subq.c.count)
...       .join_from(artist, subq)
... )
>>> print(stmt)
WITH anon_1 AS 
(SELECT count("Album"."AlbumId") AS count, "Album"."ArtistId" AS "ArtistId" 
FROM "Album" GROUP BY "Album"."ArtistId")
 SELECT "Artist"."Name", "Artist"."ArtistId", anon_1.count 
FROM "Artist" JOIN anon_1 ON "Artist"."ArtistId" = anon_1."ArtistId"
>>> 
```

The `CTE` construct also features the ability to be used in a _recursive_ style and, in more elaborate cases, be composed from the `RETURNING` clause of an `INSERT`, `UPDATE`, or `DELETE` statement.

In both cases, the subquery and CTE were named at the SQL level using an _anonymous_ name. In the Python code, we do not need to provide these names at all. The object identity of the `Subquery` or `CTE` instances serves as the syntactical identity of the object when rendered. A name that will be rendered in the SQL can be provided by passing it as the first argument of the `Select.subquery()` or `Select.cte()` methods.

### Scalar and Correlated Subqueries

A scalar subquery is a subquery that returns exactly zero or one row, and exactly one column. Then, the subquery is used in the `COLUMNS` or `WHERE` clause of an enclosing `SELECT` statement and is different than a regular subquery in that it is not used in the `FROM` clause. A [correlated subquery](https://docs.sqlalchemy.org/glossary.html#term-correlated-subquery) is a scalar subquery that refers to a label in the enclosing `SELECT` statement.

SQLAlchemy represents the scalar subquery using the `ScalarSelect` construct, which is part of the `ColumnElement` expression hierarchy, in contrast to the regular subquery that is represented by the `Subquery` construct, which is in the `FromClause` hierarchy.

Scalar subqueries are often, but not necessarily, used with aggregate functions. A scalar subquery is indicated explicitly by making use of the `Select.scalar_subquery()` method. When stringified, its default string form by itself renders as an ordinary `SELECT` statement that is selecting from two tables.

```pycon
>>> subq = (
...     sa.select(sa.func.count(album.c.AlbumId))
...       .where(artist.c.ArtistId == album.c.ArtistId)
...       .scalar_subquery()
... )
... print(subq)
(SELECT count("Album"."AlbumId") AS count_1 
FROM "Album", "Artist" 
WHERE "Artist"."ArtistId" = "Album"."ArtistId")
>>> 
```

The above `subq` object now falls within the `ColumnElement` SQL expression hierarchy, in that it may be used like any other column expression:

```pycon
>>> print(subq == 5)
(SELECT count("Album"."AlbumId") AS count_1 
FROM "Album", "Artist" 
WHERE "Artist"."ArtistId" = "Album"."ArtistId") = :param_1
>>> 
```

Although the scalar subquery renders both `artist` and `album` in its `FROM` clause when stringified by itself, when embedding it into an enclosing `select()` construct that deals with the `artist` table, the `artist` table is automatically _correlated_, meaning it does not render in the `FROM` clause of the subquery:

```pycon
>>> stmt = sa.select(artist.c.Name, subq.label('album_count'))
>>> print(stmt)
SELECT "Artist"."Name", (SELECT count("Album"."AlbumId") AS count_1 
FROM "Album" 
WHERE "Artist"."ArtistId" = "Album"."ArtistId") AS album_count 
FROM "Artist"
```

Simple correlated subqueries usually do what is desired. However, in the case where the correlation is ambiguous, SQLAlchemy will let us know that more clarity is needed:

```pycon
>>> stmt = (
...     sa.select(
...         artist.c.Name,
...         album.c.Title,
...         subq.label('album_count'),
...     )
...       .join_from(artist, album)
...       .order_by(artist.c.ArtistId, album.c.AlbumId)
... )
>>> print(stmt)
---------------------------------------------------------------------------
Traceback (most recent call last)
...
InvalidRequestError: Select statement '<sqlalchemy.sql.selectable.Select object at 0x7f5ee9a99d00>' returned no FROM clauses due to auto-correlation; specify correlate(<tables>) to control correlation manually.
>>> 
```

To specify that the `artist` table is the one we seek to correlate, we specify this using the `ScalarSelect.correlate()` or `ScalarSelect.correlate_except()` methods:

```pycon
>>> subq = (
...     sa.select(sa.func.count(album.c.AlbumId))
...       .where(artist.c.ArtistId == album.c.ArtistId)
...       .scalar_subquery()
...       .correlate(artist)
... )
>>> 
```

- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.ScalarSelect.correlate>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.ScalarSelect.correlate_except>

Then, the statement can return the data for this column like any other:

```pycon
>>> with engine.connect() as conn:
...     result = conn.execute(
...         sa.select(
...             artist.c.Name,
...             album.c.Title,
...             subq.label('album_count'),
...         )
...           .join_from(artist, album)
...           .order_by(artist.c.ArtistId, album.c.AlbumId)
...     )
>>> print(result.all())
[('AC/DC', 'For Those About To Rock We Salute You', 2), ('AC/DC', 'Let There Be Rock', 2), ('Accept', 'Balls to the Wall', 2), ('Accept', 'Restless and Wild', 2), ('Aerosmith', 'Big Ones', 1), ('Alanis Morissette', 'Jagged Little Pill', 1),...]
>>> 
```

### `UNION`, `UNION ALL`, and Other Set Operations

In SQL, `SELECT` statements can be merged together using the `UNION` or `UNION ALL` SQL operations, which produce the set of rows produced by one or more statements together. Other set operations like `INTERSECT [ALL]` and `EXPECT [ALL]` are also possible.

SQLAlchemy's `Select` construct supports compositions of this nature using functions like `union()`, `intersect()`, and `except()`, as well as the _all_ counterparts `union_all()`, `intersect_all()`, and `except_all()`. These functions all accept an arbitrary number of sub-selectables, which are typically `Select` constructs, but may also be an existing composition.

- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.union>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.intersect>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.except_>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.union_all>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.intersect_all>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.except_all>

The construct produced by these functions is the `CompoundSelect`, which is used in the same manner as the `Select` construct, except that is has fewer methods. For example, the `CompoundSelect` produced by `union_all()` may be directly invoked using `Connection.execute()`:

```pycon
>>> stmt1 = sa.select(artist).where(artist.c.Name == 'Audioslave')
>>> stmt2 = sa.select(artist).where(artist.c.Name == 'Smashing Pumpkins')
>>> u = sa.union_all(stmt1, stmt2)
>>> with engine.connect() as conn:
...     result = conn.execute(u)
...     print(result.all())
[(8, 'Audioslave'), (131, 'Smashing Pumpkins')]
>>> 
```

<https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.CompoundSelect>

To use a `CompoundSelect` as a subquery, just like `Select`, it provides a `SelectBase.subquery` method that will produce a `Subquery` object with a `FromClause.c` collection that may be referred to in an enclosing `select()`:

```pycon
>>> u_subq = u.subquery()
>>> stmt = (
...     sa.select(u_subq.c.Name, album.c.Title)
...       .join_from(album, u_subq)
...       .order_by(u_subq.c.Name, album.c.Title)
... )
>>> with engine.connect() as conn:
...     result = conn.execute(stmt)
...     print(result.all())
[('Audioslave', 'Audioslave'), ('Audioslave', 'Out Of Exile'), ('Audioslave', 'Revelations'), ('Smashing Pumpkins', 'Judas 0: B-Sides and Rarities'), ('Smashing Pumpkins', 'Rotten Apples: Greatest Hits')]
>>> 
```

<https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.SelectBase.subquery>

### `EXISTS` Subqueries

The SQL `EXISTS` keyword is an operator that is used with scalar subqueries to return a boolean true or false depending on if the `SELECT` statement would return a row. SQLAlchemy includes a variant of the `ScalarSelect` object called `Exists`, which will generate an `EXISTS` subquery and is most conveniently generated using the `SelectBase.exists` method.

```pycon
>>> subq = (
...     sa.select(sa.func.count(album.c.AlbumId))
...       .where(artist.c.ArtistId == album.c.ArtistId)
...       .group_by(album.c.ArtistId)
...       .having(sa.func.count(album.c.AlbumId) > 1)
... ).exists()
>>> with engine.connect() as conn:
...     result = conn.execute(sa.select(artist.c.Name).where(subq))
...     print(result.all()) 
[('AC/DC',), ('Accept',), ('Antônio Carlos Jobim',), ('Audioslave',), ('Black Label Society',), ('Black Sabbath',), ('Caetano Veloso',), ('Chico Science & Nação Zumbi',), ('Cidade Negra',), ('Various Artists',), ('Led Zeppelin',), ('Gilberto Gil',), ('Milton Nascimento',), ('Metallica',), ('Queen',), ('Kiss',), ('Spyro Gyra',), ('Green Day',), ('Deep Purple',), ('Santana',), ('Miles Davis',), ('Creedence Clearwater Revival',), ('Cássia Eller',), ('Djavan',), ('Eric Clapton',), ('Faith No More',), ('Foo Fighters',), ("Guns N' Roses",), ('Iron Maiden',), ('Jamiroquai',), ('Legião Urbana',), ('Lulu Santos',), ('Nirvana',), ('Os Paralamas Do Sucesso',), ('Ozzy Osbourne',), ('Pearl Jam',), ('R.E.M.',), ('Red Hot Chili Peppers',), ('Skank',), ('Smashing Pumpkins',), ('The Black Crowes',), ('The Cult',), ('The Rolling Stones',), ('The Tea Party',), ('Tim Maia',), ('Titãs',), ('Battlestar Galactica',), ('Lost',), ('U2',), ('Van Halen',), ('The Office',), ('English Concert & Trevor Pinnock',), ('Eugene Ormandy',), ('Michael Tilson Thomas & San Francisco Symphony',), ('Berliner Philharmoniker & Herbert Von Karajan',), ('Amy Winehouse',)]
>>> 
```

- <https://docs.sqlalchemy.org//core/selectable.html#sqlalchemy.sql.expression.ScalarSelect>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.Exists>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.SelectBase.exists>

Often, the `EXISTS` construct is used as a negation (e.g., `NOT EXISTS`), as it provides a SQL-efficient form of locating rows for which a related table has no rows.

```pycon
>>> subq = (
...     sa.select(album.c.AlbumId)
...       .where(artist.c.ArtistId == album.c.ArtistId)
... ).exists()
>>> with engine.connect() as conn:
...     result = conn.execute(sa.select(artist.c.Name).where(~subq))
...     print(result.all())
[('Milton Nascimento & Bebeto',), ('Azymuth',), ('João Gilberto',), ('Bebel Gilberto',), ('Jorge Vercilo',), ('Baby Consuelo',), ('Ney Matogrosso',), ('Luiz Melodia',), ('Nando Reis',), ('Pedro Luís & A Parede',), ('Banda Black Rio',), ('Fernanda Porto',), ('Os Cariocas',), ('A Cor Do Som',), ('Kid Abelha',), ('Sandra De Sá',), ('Hermeto Pascoal',), ('Barão Vermelho',), ('Edson, DJ Marky & DJ Patife Featuring Fernanda Porto',), ('Santana Feat. Dave Matthews',), ('Santana Feat. Everlast',), ('Santana Feat. Rob Thomas',), ('Santana Feat. Lauryn Hill & Cee-Lo',), ('Santana Feat. The Project G&B',), ('Santana Feat. Maná',), ('Santana Feat. Eagle-Eye Cherry',), ('Santana Feat. Eric Clapton',), ('Vinícius De Moraes & Baden Powell',), ('Vinícius E Qurteto Em Cy',), ('Vinícius E Odette Lara',), ('Vinicius, Toquinho & Quarteto Em Cy',), ('Motörhead & Girlschool',), ('Peter Tosh',), ('R.E.M. Feat. KRS-One',), ('Simply Red',), ('Whitesnake',), ('Christina Aguilera featuring BigElf',), ("Aerosmith & Sierra Leone's Refugee Allstars",), ('Los Lonely Boys',), ('Corinne Bailey Rae',), ('Dhani Harrison & Jakob Dylan',), ('Jackson Browne',), ('Avril Lavigne',), ('Big & Rich',), ("Youssou N'Dour",), ('Black Eyed Peas',), ('Jack Johnson',), ('Ben Harper',), ('Snow Patrol',), ('Matisyahu',), ('The Postal Service',), ('Jaguares',), ('The Flaming Lips',), ("Jack's Mannequin & Mick Fleetwood",), ('Regina Spektor',), ('Xis',), ('Nega Gizza',), ('Gustavo & Andres Veiga & Salazar',), ('Rodox',), ('Charlie Brown Jr.',), ('Pedro Luís E A Parede',), ('Los Hermanos',), ('Mundo Livre S/A',), ('Otto',), ('Instituto',), ('Nação Zumbi',), ('DJ Dolores & Orchestra Santa Massa',), ('Seu Jorge',), ('Sabotage E Instituto',), ('Stereo Maracana',), ('Academy of St. Martin in the Fields, Sir Neville Marriner & William Bennett',)]
>>> 
```

### Working With SQL Functions

The `func` object serves as a factory for creating new `Function` objects, which when used in a construct like `select()`, produce a SQL function display. Typically, this display consists of a name, some parenthesis, and some arguments.

Examples of typical SQL functions include:

- The `count()` function, an aggregate function that counts how many rows are returned:

        >>> print(sa.select(sa.func.count()).select_from(artist))
        SELECT count(*) AS count_1 
        FROM "Artist"
        >>> 

- The `lower()` function, a string function that converts a string to lower case:

        >>> bullet_with_butterfly_wings = (
        ...     'Despite All My Rage, I Am Still Just A Rat In A CAGE'
        ... )
        >>> print(sa.select(sa.func.lower(bullet_with_butterfly_wings)))
        SELECT lower(:lower_2) AS lower_1
        >>> 

- The `now()` function, which provides for the current date and time. SQLAlchemy knows how to render this differently for each backend:

        >>> stmt = sa.select(sa.func.now())
        >>> with engine.connect() as conn:
        ...     result = conn.execute(stmt)
        ...     print(result.all())
        [(datetime.datetime(2023, 5, 27, 13, 16, 3),)]
        >>> 

As most database backends feature potentially hundreds of different SQL functions, `func` tries to be as liberal as possible in what it accepts. Any name that is accessed from this namespace is automatically considered to be a SQL function that will render in a generic way.

```pycon
>>> print(sa.select(sa.func.some_crazy_function(artist.c.Name, 17)))
SELECT some_crazy_function("Artist"."Name", :some_crazy_function_2) AS some_crazy_function_1 
FROM "Artist"
>>> 
```

Simultaneously, a relatively small set of extremely common SQL functions (e.g., `count`, `now`, `max`, `concat`) include pre-packaged versions of themselves that provide for proper typing information, as well as backend-specific SQL generation, in some cases.

```pycon
>>> from sqlalchemy.dialects import postgresql
>>> print(sa.select(sa.func.now()).compile(dialect=postgresql.dialect()))
SELECT now() AS now_1
>>> from sqlalchemy.dialects import oracle
>>> print(sa.select(sa.func.now()).compile(dialect=oracle.dialect()))
SELECT CURRENT_TIMESTAMP AS now_1 FROM DUAL
>>> 
```

- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.count>
- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.now>
- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.max>
- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.concat>

#### Functions Have Return Types

As functions are column expressions, they also have SQL [datatypes](https://docs.sqlalchemy.org/core/types.html) that describe the data type of a generated SQL expression. In SQLAlchemy, these are referred to as _SQL return types_, in reference to the type of SQL value that is returned by the function in the context of a database-side SQL expression, as opposed to the _return type_ of a Python function.

The SQL return type of any SQL function may be accessed, typically for debugging purposes, by referring to the `Function.type` attribute:

```pycon
>>> sa.func.now().type
DateTime()
>>> 
```

SQL return types are significant when making use of the function expression in the context of a larger expression, i.e., math operators will work better when the datatype of the expression is something like `Integer` or `Numeric` (in order to work, JSON operators need to be using a type like `JSON`).

- <https://docs.sqlalchemy.org/core/type_basics.html#sqlalchemy.types.Integer>
- <https://docs.sqlalchemy.org/core/type_basics.html#sqlalchemy.types.Numeric>
- <https://docs.sqlalchemy.org/core/type_basics.html#sqlalchemy.types.JSON>

Certain classes of function return entire rows instead of column values, where there is a need to refer to specific columns. These functions are referred to as [table valued functions](https://docs.sqlalchemy.org/en/20/tutorial/data_select.html#tutorial-functions-table-valued).

The SQL return type of the function may also be significant when executing a statement and getting rows back, for those cases where SQLAlchemy has to apply result-set processing. For example, in SQLite date-related functions, SQLAlchemy's `DateTime` and related datatype take on the role of converting from string values to Python `datetime()` objects as result rows are received.

- <https://docs.sqlalchemy.org/core/type_basics.html#sqlalchemy.types.DateTime>

To apply a specific type to a function, we pass it using the `Function.type_` parameter. The type argument may be either a `TypeEngine` class or an instance.

```pycon
>>> from sqlalchemy import JSON
>>> function_expr = sa.func.json_object('{a, 1, b, "2", c, 3}', type_=JSON)
>>> 
```

- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.Function.params.type_>
- <https://docs.sqlalchemy.org/core/type_api.html#sqlalchemy.types.TypeEngine>

By creating a JSON function with the `JSON` datatype, the SQL expression object takes on JSON-related features, like that of accessing elements:

```pycon
>>> stmt = sa.select(function_expr['2'])
>>> print(stmt)
SELECT json_object(:json_object_1)[:json_object_2] AS anon_1
>>> 
```

#### Built-In Functions Have Pre-Configured Return Types

For common aggregate functions like `count`, `max`, and `min` (as well as a small number of date functions like `now` and string functions like `concat`), the SQL return type is appropriately set up, sometimes based on usage. The `max` function and similar aggregate filtering functions will set up the SQL return type based on the argument given:

```pycon
>>> m1 = sa.func.max(sa.Column('an_int', sa.Integer))
>>> m1.type
Integer()
>>> m2 = sa.func.max(sa.Column('a_str', sa.String))
>>> m2.type
String()
>>> 
```

<https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.min>

Typically, date and time functions correspond to SQL expressions described by `DateTime`, `Date` or `Time`:

```pycon
>>> sa.func.now().type
DateTime()
>>> sa.func.current_date().type
Date()
>>> 
```

<https://docs.sqlalchemy.org/core/type_basics.html#sqlalchemy.types.Time>

A known string function like `concat` will know that a SQL expression would be of type `String`:

```pycon
>>> sa.func.concat('x', 'y').type
String()
>>> 
```

However, for the majority of SQL functions, SQLAlchemy does not have them explicitly present in its small list of known functions. For example, while there is typically no issue using SQL functions `func.lower()` and `func.upper()` to convert the casing of strings, SQLAlchemy does not actually know about these functions, so they have a _null_ SQL return type:

```pycon
>>> sa.func.upper('lowercase').type
NullType()
>>> 
```

For simple functions like `upper` and `lower`, the issue is not usually significant, as string values may be received from the database without any special type handling on the SQLAlchemy side, and SQLAlchemy's type coercion rules can often correctly guess intent, as well. For example, the Python `+` operation will be correctly interpreted as the string concatenation operator based on looking at both sides of the expression:

```pycon
>>> print(sa.select(sa.func.upper('lowercase') + ' suffix'))
SELECT upper(:upper_1) || :upper_2 AS anon_1
>>> 
```

Overall, the scenario where the `Function.type_` parameter is likely necessary is:

1. The function is not already a SQLAlchemy built-in function. This can be evidenced by creating the function and observing the `Function.type` attribute:

        >>> sa.func.count().type
        Integer()
        >>> 

    Versus:

        >>> sa.func.json_object("{'a', 'b'}").type
        NullType()
        >>> 

2. Function-aware expression support is needed. Typically, this refers to special operators related to datatypes like `JSON` or `ARRAY`.
3. Result value processing is needed, which may include types like `DateTime`, `Boolean`, `Enum`, or special datatypes like `JSON` and `ARRAY`.
    - <https://docs.sqlalchemy.org/core/type_basics.html#sqlalchemy.types.ARRAY>
    - <https://docs.sqlalchemy.org/core/type_basics.html#sqlalchemy.types.Boolean>
    - <https://docs.sqlalchemy.org/core/type_basics.html#sqlalchemy.types.Enum>

#### Advanced SQL Function Techniques

##### Using Window Functions

A window function is a special use of a SQL aggregate function that calculates the aggregate value over the rows being returned in a group as the individual result rows are processed. Whereas a function like `MAX()` will give you the highest value of a column within a set of rows, using the same function as a _window function_ will give you the highest value for each row _as of that row_.

In SQL, window functions allow one to specify the rows over which the function should be applied, a _partition_ value that considers the window over different sub-sets of rows, and an _order by_ expression that indicates the order in which rows should be applied to the aggregate function.

In SQLAlchemy, all SQL functions generated by the `func` namespace include a method `FunctionElement.over()`, which grants the window function, or _OVER_, syntax. The produced construct is the `Over` construct.

- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.FunctionElement.over>
- <https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.Over>

A common function that is used with window functions is the `row_number()` function, which simply counts rows. We may partition this row count against artist name to the number of albums of individual artists:

```pycon
>>> stmt = (
...     sa.select(
...         sa.func.row_number().over(partition_by=artist.c.Name),
...         artist.c.Name,
...         album.c.Title,
...     )
...       .select_from(artist)
...       .join(album)
... )
>>> with engine.connect() as conn:
...     result = conn.execute(stmt)
...     print(result.all())
[(1, 'AC/DC', 'For Those About To Rock We Salute You'), (2, 'AC/DC', 'Let There Be Rock'), (1, 'Aaron Copland & London Symphony Orchestra', 'A Copland Celebration, Vol. I'), (1, 'Aaron Goldberg', 'Worlds'), (1, 'Academy of St. Martin in the Fields & Sir Neville Marriner', 'The World of Classical Favourites'), (1, 'Academy of St. Martin in the Fields Chamber Ensemble & Sir Neville Marriner', 'Sir Neville Marriner: A Celebration'),...]
```

Above, the `FunctionElement.over.partition_by` parameter is used so that the `PARTITION BY` clause is rendered within the `OVER` clause. We may also make use of the `ORDER BY` clause using `FunctionElement.over.order_by`:

```pycon
>>> stmt = (
...     sa.select(
...         sa.func.count().over(order_by=artist.c.Name),
...         artist.c.Name,
...         album.c.Title,
...     )
...       .select_from(artist)
...       .join(album)
... )
>>> with engine.connect() as conn:
...     result = conn.execute(stmt)
...     print(result.all())
[(2, 'AC/DC', 'For Those About To Rock We Salute You'), (2, 'AC/DC', 'Let There Be Rock'), (3, 'Aaron Copland & London Symphony Orchestra', 'A Copland Celebration, Vol. I'), (4, 'Aaron Goldberg', 'Worlds'), (5, 'Academy of St. Martin in the Fields & Sir Neville Marriner', 'The World of Classical Favourites'),...]
>>> 
```

- <https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.over.params.partition_by>
- <https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.over.params.order_by>

Further options for window functions include usage of ranges.

Note that the `FunctionElement.over()` method only applies to those SQL functions that are in fact aggregate functions. While the `Over` construct will render itself for any SQL function given, the database will reject the expression if the function itself is not a SQL aggregate function.

#### Special Modifiers `WITHIN GROUP`, `FILTER`

The `WITHIN GROUP` SQL syntax is used in conjunction with an _ordered set_ or a _hypothetical set_ aggregate function. Common ordered set functions include `percentile_cont()` and `rank()`. SQLAlchemy includes built in implementations `rank`, `dense_rank`, `mode`, `percentile_cont`, and `percentile_disc`, which include a `FunctionElement.within_group()` method:

```pycon
>>> print(
...     sa.func.unnest(
...         sa.func.percentile_disc([0.25, 0.5, 0.75, 1])
...           .within_group(artist.c.Name)
...     )
... )
unnest(percentile_disc(:percentile_disc_1) WITHIN GROUP (ORDER BY "Artist"."Name"))
```

- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.rank>
- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.dense_rank>
- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.mode>
- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.percentile_cont>
- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.percentile_disc>
- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.FunctionElement.within_group>

`FILTER` is supported by some backends to limit the range of an aggregate function to a particular subset of rows compared to the total range of rows returned, available using the `FunctionElement.filter()` method:

```pycon
>>> stmt = (
...     sa.select(
...         sa.func.count(album.c.Title)
...           .filter(artist.c.Name == 'Red Hot Chili Peppers'),
...         sa.func.count(album.c.Title).filter(
...             artist.c.Name == 'Smashing Pumpkins'
...         ),
...     )
...       .select_from(artist)
...       .join(album)
... )
>>> with engine.connect() as conn:
...     result = conn.execute(stmt)
...     print(result.all())
[(3, 2)]
>>> 
```

<https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.FunctionElement.filter>

#### Table-Valued Functions

Table-valued functions support a scalar representation that contains named sub-elements. They are often used for JSON and ARRAY-oriented functions, as well as functions like `generate_series()`.

The table-valued function is specified in the `FROM` clause and is then referred to as a table, or sometimes as a column. Functions of this form are prominent in the PostgreSQL database. However, some forms of table valued functions are also supported by SQLite, Oracle, and SQL Server.

SQLAlchemy provides the `FunctionElement.table_valued()` method as the basic _table valued function_ construct, which will convert a `func` object into a `FROM` clause containing a series of named columns, based on string names passed positionally. This returns a `TableValuedAlias` object, which is a function-enabled `Alias` construct that may be used as any other `FROM` clause.

```pycon
>>> one_two_three = (
...     sa.func.json_each('["one", "two", "three"]')
...       .table_valued('value')
... )
>>> stmt = (
...     sa.select(one_two_three)
...       .where(one_two_three.c.value.in_(['two', 'three'])) 
... )
>>> with engine.connect() as conn:
...     result = conn.execute(stmt)
...     print(result.all())
... 
[('two',), ('three',)]
>>> 
```

- <https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.FunctionElement.table_valued>
- <https://docs.sqlalchemy.org/core/selectable.html#sqlalchemy.sql.expression.TableValuedAlias>

Above, we used the `json_each()` JSON function supported by SQLite and PostgreSQL to generate a table valued expression with a single column referred to as `value`, and then selected two of its three rows.

#### Column Valued Functions - Table Valued Function as a Scalar Column

A special syntax supported by PostgresSQL and Oracle is that of referring to a function in the `FROM` clause, which then delivers itself as a single column in the columns clause of a `SELECT` statement or other column expression context. SQLAlchemy refers to this as a _column valued_ function and is available by applying the `FunctionElement.column_valued()` modifier to a `Function` construct:

```pycon
>>> from sqlalchemy import select, func
>>> stmt = sa.select(sa.func.json_array_elements('["one", "two"]')
...                    .column_valued('x'))
>>> print(stmt)
SELECT x 
FROM json_array_elements(:json_array_elements_1) AS x
>>> 
```

<https://docs.sqlalchemy.org/core/functions.html#sqlalchemy.sql.functions.FunctionElement.column_valued>

The _column valued_ form is also supported by the Oracle dialect, where it is usable for custom SQL functions:

```pycon
>>> from sqlalchemy.dialects import oracle
>>> stmt = sa.select(sa.func.scalar_strings(5).column_valued('s'))
>>> print(stmt.compile(dialect=oracle.dialect()))
SELECT s.COLUMN_VALUE 
FROM TABLE (scalar_strings(:scalar_strings_1)) s
>>> 
```

### Data Casts and Type Coercion

In SQL, we often need to explicitly indicate the datatype of an expression, either to tell the database what type is an otherwise ambiguous expression, or in some cases when we want to convert the implied datatype of a SQL expression into something else.

The SQL `CAST` keyword is used for this task, which in SQLAlchemy is provided by the `cast()` function. This function accepts a column expression and a data type object as arguments.

```pycon
>>> from sqlalchemy import cast
>>> stmt = sa.select(cast(artist.c.ArtistId, sa.String))
... with engine.connect() as conn:
...     result = conn.execute(stmt)
...     print(result.all())
[('1',), ('2',), ('3',), ('4',), ('5',), ('6',), ('7',), ('8',), ('9',), ('10',), ('11',), ('12',), ('13',), ('14',), ('15',), ('16',),...]
>>> 
```

<https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.cast>

The `cast()` function not only renders the SQL `CAST` syntax, but also produces a SQLAlchemy column expression that will act as the given datatype on the Python side, as well. A string expression that is `cast()` to `JSON` will gain JSON subscript and comparison operators:

```pycon
>>> from sqlalchemy import JSON
>>> print(cast("{'a': 'b'}", JSON)['a'])
CAST(:param_1 AS JSON)[:param_2]
>>> 
```

#### `type_coerce()` - A Python-Only _Cast_

Sometimes, there is the need to have SQLAlchemy know the datatype of an expression, but to not render the `CAST` expression itself on the SQL side, where it may interfere with a SQL operation that already works without it. For this common use case, there is another function, `type_coerce()`, which is closely related to `cast()`, in that it sets up a Python expression as having a specific SQL database type, but does not render the `CAST` keyword or datatype on the database side.

<https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.type_coerce>

`type_coerce()` is particularly important when dealing with the `JSON` datatype, which usually has an intricate relationship with string-oriented datatypes on different platforms and may not even be an explicit datatype, such as on SQLite and MariaDB.

```pycon
>>> import json
>>> from sqlalchemy import JSON
>>> from sqlalchemy import type_coerce
>>> from sqlalchemy.dialects import mysql
>>> s = sa.select(type_coerce({"a_key": {"foo": "bar"}}, JSON)['a_key'])
>>> print(s.compile(dialect=mysql.dialect()))
SELECT JSON_EXTRACT(%s, %s) AS anon_1
>>> 
```

Above, MySQL's `JSON_EXTRACT` SQL function was invoked because we used `type_coerce()` to indicate that our Python dictionary should be treated as `JSON`. The Python `.__getitem__` operator (i.e., `["a_key"]`) became available as a result and allowed a `JSON_EXTRACT` path expression to be rendered.

## Using `UPDATE` and `DELETE` Statements

### The `update()` SQL Expression Construct

The `update()` function generates a new instance of `Update` that represents an `UPDATE` statement in SQL, which will update existing data in a table.

- <https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.update>
- <https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Update>

Like the `insert()` construct, there is a _traditional_ form of `update()`, which emits `UPDATE` against a single table at a time and does not return any rows. However, some backends support an `UPDATE` statement that may modify multiple tables at once, and the `UPDATE` statement also supports `RETURNING` such that columns contained in matched rows may be returned in the result set.

```pycon
>>> from sqlalchemy import update
>>> stmt = (
...     sa.update(artist)
...       .where(artist.c.Name == 'Pearl Jam')
...       .values(Name='Green River')
... )
>>> print(stmt)
UPDATE "Artist" SET "Name"=:Name WHERE "Artist"."Name" = :Name_1
>>> 
```

The `Update.values()` method controls the contents of the `SET` elements of the `UPDATE` statement. This is the same method shared by the `Insert` construct. Parameters can normally be passed using the column names as keyword arguments.

<https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Update.values>

`UPDATE` supports all the major SQL forms of `UPDATE`, including updates against expressions, where we can make use of `Column` expressions:

```pycon
>>> stmt = sa.update(artist).values(Name='Name: ' + artist.c.Name)
>>> print(stmt)
UPDATE "Artist" SET "Name"=(:Name_1 || "Artist"."Name")
>>> 
```

To support `UPDATE` in an _executemany_ context, where many parameter sets will be invoked against the same statement, the `bindparam()` construct may be used to set up bound parameters. These replace the places that literal values would normally go:

```pycon
>>> from sqlalchemy import bindparam
>>> stmt = (
...     sa.update(artist)
...       .where(artist.c.Name == bindparam('oldname'))
...       .values(Name=bindparam('newname'))
... )
>>> print(stmt)
UPDATE "Artist" SET "Name"=:newname WHERE "Artist"."Name" = :oldname
>>> 
```

<https://docs.sqlalchemy.org/core/sqlelement.html#sqlalchemy.sql.expression.bindparam>

Other techniques that may be applied to `UPDATE` include:

- **Correlated Updates** - An `UPDATE` statement can make use of rows in other tables by using a [correlated subquery](https://docs.sqlalchemy.org/tutorial/data_select.html#tutorial-scalar-subquery). A subquery may be used anywhere a column expression might be placed:

        >>> scalar_subq = (
        ...     sa.select(album.c.Title)
        ...       .where(album.c.ArtistId == artist.c.ArtistId)
        ...       .order_by(album.c.AlbumId)
        ...       .limit(1)
        ...       .scalar_subquery()
        ... )
        >>> update_stmt = sa.update(artist).values(Name=scalar_subq)
        >>> print(update_stmt)
        UPDATE "Artist" SET "Name"=(SELECT "Album"."Title" 
        FROM "Album" 
        WHERE "Album"."ArtistId" = "Artist"."ArtistId" ORDER BY "Album"."AlbumId"
        LIMIT :param_1)
        >>> 

- **`UPDATE..FROM`** - Some databases (e.g., PostgreSQL, MySQL) support a syntax, `UPDATE FROM`, where additional tables may be directly stated in a special `FROM` clause. This syntax will be implicitly generated when additional tables are located in the `WHERE` clause of the statement:

        >>> update_stmt = (
        ...     sa.update(artist)
        ...       .where(artist.c.ArtistId == album.c.ArtistId)
        ...       .where(album.c.Title == 'Supernatural')
        ...       .values(Name='Santana')
        ... )
        >>> print(update_stmt)
        UPDATE "Artist" SET "Name"=:Name FROM "Album" WHERE "Artist"."ArtistId" = "Album"."ArtistId" AND "Album"."Title" = :Title_1
        >>> 

    There is also a MySQL specific syntax that can `Update` multiple tables. This requires we refer to `Table` objects in the `VALUES` clause in order to refer to additional tables:

        >>> from sqlalchemy.dialects import mysql
        >>> update_stmt = (
        ...     sa.update(artist)
        ...       .where(artist.c.ArtistId == album.c.ArtistId)
        ...       .where(album.c.Title == 'Supernatural')
        ...       .values(
        ...         {
        ...             artist.c.Name: 'Santana',
        ...             album.c.Title: 'Natural'
        ...         }
        ...     )
        ... )
        >>> print(update_stmt.compile(dialect=mysql.dialect()))
        UPDATE `Artist`, `Album` SET `Album`.`Title`=%s, `Artist`.`Name`=%s WHERE `Artist`.`ArtistId` = `Album`.`ArtistId` AND `Album`.`Title` = %s
        >>> 

- **Parameter Ordered Updates** - Another MySQL-only behavior is that the order of parameters in the `SET` clause of an `UPDATE` actually impacts the evaluation of each expression. For this use case, the `Update.ordered_values()` method accepts a sequence of tuples so that this order may be controlled:

        >>> update_stmt = (
                sa.update(a_table)
                  .ordered_values((a_table.c.y, 20), (a_table.c.x, a_table.c.y + 10))
            )
        >>> print(update_stmt)
        UPDATE a_table SET y=:y, x=(a_table.y + :y_1)
        >>> 

    <https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Update.ordered_values>

    While Python dictionaries are [guaranteed to be insert ordered](https://mail.python.org/pipermail/python-dev/2017-December/151283.html) as of Python 3.7, the `Update.ordered_values()` method still provides an additional measure of clarity of intent when it is essential that the `SET` clause of a MySQL `UPDATE` statement proceed in a specific way.

### The `delete()` SQL Expression Construct

The `delete()` function generates a new instance of `Delete` that represents a `DELETE` statement in SQL, which will delete rows from a table.

The `delete()` statement from an API is similar to that of the `update()` construct, traditionally returning no rows, but allowing for a `RETURNING` variant on some database backends.

```pycon
>>> stmt = sa.delete(artist).where(artist.c.Name == 'Gene Krupa')
>>> print(stmt)
DELETE FROM "Artist" WHERE "Artist"."Name" = :Name_1
>>> 
```

- <https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.delete>
- <https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Delete>

#### Multiple Table Deletes

Like `Update`, `Delete` supports the use of correlated subqueries in the `WHERE` clause, as well as backend-specific multiple table syntaxes, e.g., `DELETE FROM...USING` on MySQL:

```pycon
>>> from sqlalchemy.dialects import mysql
>>> delete_stmt = (
...     sa.delete(artist)
...       .where(artist.c.ArtistId == album.c.ArtistId)
...       .where(album.c.Title == 'Somewhere in Time')
... )
>>> print(delete_stmt.compile(dialect=mysql.dialect()))
DELETE FROM `Artist` USING `Artist`, `Album` WHERE `Artist`.`ArtistId` = `Album`.`ArtistId` AND `Album`.`Title` = %s
>>> 
```

### Getting Affected Row Count From `UPDATE`, `DELETE`

Both `Update` and `Delete` support the ability to return the number of rows matched after the statement proceeds for statements that are invoked using Core `Connection`, i.e., `Connection.execute()`. This value is available from the `CursorResult.rowcount` attribute:

```pycon
>>> with engine.begin() as conn:
...     result = conn.execute(
...         sa.update(artist)
...           .values(Name='Barry Wordsworth')
...           .where(artist.c.Name == 'Barry Wordsworth & BBC Concert Orchestra')
...     )
...     print(result.rowcount)
1
>>> 
```

- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.Connection.execute>
- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.CursorResult.rowcount>

The `CursorResult` class is a subclass of `Result`, which contains additional attributes that are specific to the DBAPI `cursor` object. An instance of this subclass is returned when a statement is invoked via the `Connection.execute()` method. When using the ORM, the `Session.execute()` method returns an object of this type for all `INSERT`, `UPDATE`, and `DELETE` statements.

- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.CursorResult>
- <https://docs.sqlalchemy.org/orm/session_api.html#sqlalchemy.orm.Session.execute>

Regarding `CursorResult.rowcount`:

- The value returned is the number of rows _matched_ by the `WHERE` clause of the statement. It does not matter if the rows were actually modified or not.
- `CursorResult.rowcount` is not necessarily available for an `UPDATE` or `DELETE` statement that uses `RETURNING`.
- For an `executemany` execution, `CursorResult.rowcount` may not be available either, which depends highly on the DBAPI module in use, as well as configured options. The attribute `CursorResult.supports_sane_multi_rowcount` indicates if this value will be available for the current backend in use.
- Some drivers, particularly third party dialects for non-relational databases may not support `CursorResult.rowcount` at all. The `CursorResult.supports_sane_rowcount` will indicate this.
- _rowcount_ is used by the ORM [unit of work](https://docs.sqlalchemy.org/glossary.html#term-unit-of-work) process to validate that an `UPDATE` or `DELETE` statement matched the expected number of rows, and is also essential for the [ORM versioning feature](https://docs.sqlalchemy.org/orm/versioning.html#mapper-version-counter).

<br />

- <https://docs.sqlalchemy.org/tutorial/dbapi_transactions.html#tutorial-multiple-parameters>
- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.CursorResult.supports_sane_multi_rowcount>
- <https://docs.sqlalchemy.org/core/connections.html#sqlalchemy.engine.CursorResult.supports_sane_rowcount>

### Using `RETURNING` With `UPDATE`, `DELETE`

Like the `Insert` construct, `Update` and `Delete` also support the `RETURNING` clause, which is added by using the `Update.returning()` and `Delete.returning()` methods. When these methods are used on a backend that supports `RETURNING`, selected columns from all rows that match the `WHERE` criteria of the statement will be returned in the `Result` object as rows that can be iterated:

```pycon
>>> update_stmt = (
...     sa.update(artist)
...       .where(artist.c.Name == 'Barry Wordsworth & BBC Concert Orchestra')
...       .values(Name='Barry Wordsworth')
...       .returning(artist.c.ArtistId, artist.c.Name)
... )
>>> print(update_stmt)
UPDATE "Artist" SET "Name"=:Name WHERE "Artist"."Name" = :Name_1 RETURNING "Artist"."ArtistId", "Artist"."Name"
>>> delete_stmt = (
...     sa.delete(artist)
...       .where(artist.c.Name == 'Barry Wordsworth & BBC Concert Orchestra')
...       .returning(artist.c.ArtistId, artist.c.Name)
... )
>>> print(delete_stmt)
DELETE FROM "Artist" WHERE "Artist"."Name" = :Name_1 RETURNING "Artist"."ArtistId", "Artist"."Name"
>>> 
```

- <https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Update.returning>
- <https://docs.sqlalchemy.org/core/dml.html#sqlalchemy.sql.expression.Delete.returning>

## Documentation

- [SQLAlchemy Documentation](https://docs.sqlalchemy.org/)
    - [Engine Configuration](https://docs.sqlalchemy.org/core/engines.html)
        - [Creating URLs Programmatically](https://docs.sqlalchemy.org/core/engines.html#creating-urls-programmatically)
        - [Database URLs](https://docs.sqlalchemy.org/core/engines.html#database-urls)
    - [Glossary](https://docs.sqlalchemy.org/glossary.html)
    - [Operator Reference](https://docs.sqlalchemy.org/core/operators.html)
    - [Python DBAPIs](https://docs.sqlalchemy.org/core/engines.html#backend-specific-urls)
        - [pg8000](https://github.com/tlocke/pg8000)
        - [PyMySQL](https://pymysql.readthedocs.io/en/latest/)
        - [pyodbc](https://github.com/mkleehammer/pyodbc/wiki)
        - [python-oracledb](https://python-oracledb.readthedocs.io/en/latest/index.html)
        - [sqlite3](https://docs.python.org/library/sqlite3.html)
    - [Reflecting Database Objects](https://docs.sqlalchemy.org/core/reflection.html)
    - [Sending Parameters](https://docs.sqlalchemy.org/tutorial/dbapi_transactions.html#sending-parameters)
    - [The Type Hierarchy](https://docs.sqlalchemy.org/core/type_basics.html)