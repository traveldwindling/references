# Python Language Reference

A collection of Python language reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

Python is a high-level general-purpose programming language that is interactive and object-oriented.

It incorporates:

- modules
- exceptions
- dynamic typing
- high-level dynamic data types
- strong typing
- classes

It supports multiple programming paradigms, including procedural and functional programming.

Python can interface with many system calls and libraries, as well as various window systems. It is extensible in C or C++, and is usable as an extension language for applications that need a programmable interface.

Python is portable, i.e., it runs on many UNIX variants (e.g., GNU/Linux and macOS) and on Windows.

## Packages

### Debian

`sudo apt install -y python3-pip python3-venv`

### Fedora

`sudo dnf install -y python3-pip`

## Virtual Environments

There are times when one Python installation may not meet the requirements of multiple applications with different needs:

- An application uses packages and modules that do not come as part of the standard library
- A specific version of a library is required

This can lead to conflicts and an unstable Python installation.

One solution is a [virtual environment](https://docs.python.org/glossary.html#term-virtual-environment), which is a self-contained directory tree that contains a Python installation for a specific version of Python and a number of additional packages. Different applications can use different virtual environments.

When installing software via `pip`, it is often best to do so inside of a virtual environment.

### Creating Virtual Environments

Virtual environments are created and managed with the [`venv`](https://docs.python.org/library/venv.html#module-venv) module. By default, `venv` will usually install the most recent version of Python available on your system.

If you have multiple versions of Python installed, you can select a specific Python version by running `python3` or whichever command is associated with the version of Python that you desire.

To create a virtual environment, run the `venv` module as a script and provide it a directory path to create the virtual environment in:

`python3 -m venv ex_dir`

This command will create `ex_dir` if it does not exist, as well as directories inside of it containing a symbolic link to the Python interpreter and various supporting files.

### Working With Virtual Environments

A virtual environment creates a self-contained directory tree. For example:

```text
├── bin
│   ├── activate
│   ├── activate.csh
│   ├── activate.fish
│   ├── Activate.ps1
│   ├── pip
│   ├── pip3
│   ├── pip3.10
│   ├── pip3.9
│   ├── python -> python3
│   ├── python3 -> /usr/bin/python3
│   └── python3.9 -> python3
├── include
├── lib
│   └── python3.9
├── lib64 -> lib
├── pyvenv.cfg
└── share
    └── python-wheels
```

- `bin/` contains the Python interpreter for the virtual environment (which manifests itself as a symbolic link to the base system's version of Python) and the `activate` shell script.
- The `pyvenv.cfg` file contains the [virtual environment's configuration](https://docs.python.org/library/venv.html#creating-virtual-environments):

        $ cat pyvenv.cfg
        home = /usr/bin
        include-system-site-packages = false
        version = 3.9.2

    - `home` represents the Python installation directory from which the `python3 -m venv` command was run to create the virtual environment.
    - `include-system-site-packages` determines whether the global [`site-packages`](https://docs.python.org/install/#how-installation-works) location should be included (defaults to `false`, i.e., the virtual environment is isolated from the base system's Python environment).
    - `version` confirms the virtual environment's Python version.

- `lib/` contains the `pythonX.X/site-packages` subdirectory where software installed to the virtual environment is stored.

A virtual environment can be activated by loading its `bin/activate` file into the current shell environment with the `.` command:

`. ex_dir/bin/activate`

Activating the virtual environment is not technically required to use it, but activation does several important things, including:

- Changing the shell’s prompt to show the virtual environment that you are using (e.g., `(py_proj) $ `).
- Modifying the environment so that running `python` gets you the specific version and installation of Python that you used to create the virtual environment with (e.g., `${HOME}/venvs/py_proj/bin/python` -> `${HOME}/venvs/py_proj/bin/python3` -> `/usr/bin/python3`).
- Prepending the virtual environment's `bin/` directory to the `PATH` environment variable.

    This ensures that software you install in the virtual environment is given precedence in your shell's environment and enables you to just use the name of the software to invoke it, versus using its [path](https://en.wikipedia.org/wiki/Path_(computing)#Absolute_and_relative_paths).

- Setting the `VIRTUAL_ENV` environment variable to your virtual environment's directory.
- Registering the `deactivate` shell function, which undoes the above steps.

A virtual environment can be deactivated from any file system location by entering `deactivate`, or by exiting the shell from which you activated the virtual environment.

### Upgrading Virtual Environments

Virtual environments created by `venv` are linked to the version of Python used on the base system when they were created. When the base version changes, the virtual environments will no longer work, as they will be referencing the wrong version of Python.

You can update these virtual environments to use the current version of Python on the base system like so:

```console
python3 -m venv --upgrade ex_ve_dir... &&
    python3 -m venv --upgrade-deps ex_ve_dir...
```

### Virtual Environment Recipe

The following set of commands is useful for:

- Creating a new virtual environment
- Updating the environment's core dependencies via the `--upgrade-deps` option
- Moving to and activating the virtual environment

```console
python3 -m venv --upgrade-deps ex_dir &&
    . "ex_dir/bin/activate"
```

## pip

`pip` is the package installer for Python. It can be used to install packages from the [Python Package Index](https://pypi.org/) and other indices.

### Installation

#### Debian

`sudo apt install -y python3-pip`

#### Fedora

`sudo dnf install -y python3-pip`

### Commands

`pip index versions ex_pkg`  
Display all versions of a package from a package index.

`pip list`  
List installed packages.

`--user` option supported.

`pip show ex_pkg...`  
Show information about installed package.

`pip freeze > requirements.txt`  
Output installed packages in requirements format.

`--user` option supported.

For Debian, use `pip freeze | grep -v 'pkg_resources' > requirements.txt`.

`pip wheel ex_req_specifier...`  
Build wheel archives for your requirements and dependencies.

`pip list --outdated`  
List outdated packages.

`--user` option supported.

`pip install -U ex_pkg...`  
Upgrade a package to the newest available version.

`--user` option supported.

`pip install ex_pkg...`  
Install a package.

`--user` option supported.

`pip install ex_pkg==ex_version...`  
Install a specific version of a package.

`--user` option supported.

`pip install ex_pkg==ex_version... --force-reinstall`  
Reinstall specific version of a package even if it is already up-to-date.

`--user` option supported.

`pip install -r requirements.txt`  
Install from the given requirements file.

`--user` option supported.

`pip uninstall ex_pkg...`  
Uninstall a package.

`pip uninstall ex_pkg==ex_version...`  
Uninstall a specific version of a package.

## Internals

Python has several runtimes. [CPython](https://docs.python.org/glossary.html#term-CPython) is the _official_ (reference) implementation of Python. It contains both a runtime and the Python language specification (i.e., the document that defines the Python language).

CPython is written in human-readable C and Python code. The compiler is written in pure C, while many of the standard library modules are written in pure Python, or a combination of C and Python.

The role of the CPython runtime is to compile Python source code into low-level intermediary language [bytecode](https://docs.python.org/glossary.html#term-bytecode) that CPython understands. The bytecode is stored in `.pyc` files in a directory (`__pycache__`), where it is cached for execution.

## Interpreter

### Built-Ins

- [Built-In Functions](https://docs.python.org/library/functions.html)
- [Built-In Constants](https://docs.python.org/library/constants.html)
- [Built-In Types](https://docs.python.org/library/stdtypes.html)
- [Built-In Exceptions](https://docs.python.org/library/exceptions.html)

### Location

```console
$ which python3
/usr/bin/python3
```

### Interactive Start

```console
$ python3
Type "help", "copyright", "credits" or "license" for more information.
>>> 
```

### Non-Interactive Command Start

`python3 -c 'ex_cmd'`

Exit:

**Ctrl+d** or `>>> quit()`.

### Invoke Module As Script

`python3 -m ex_module`

Enter interactive mode after running script:

`python3 -i -m ex_module`

### Script Arguments

Script name and arguments are turned into a list of strings and assigned to [argv](https://docs.python.org/library/sys.html#sys.argv) in the [sys](https://docs.python.org/library/sys.html) module.

The length of `sys.argv[0]` is always one and will be an empty string if no script and arguments are provided:

```console
$ python3
Type "help", "copyright", "credits" or "license" for more information.
>>> 
```

```pycon
>>> import sys
>>> sys.argv
['']
>>> 
```

### Source Code Encoding

Each character is given a unique number (code) so that it can be stored in computer memory.

There are many character encoding standards:

- [ASCII](https://en.wikipedia.org/wiki/ASCII) - American Standard Code for Information Interchange
- [UTF-8](https://en.wikipedia.org/wiki/UTF-8) - 8-bit Unicode Transformation Format
- [UTF-16](https://en.wikipedia.org/wiki/UTF-16) - 16-bit Unicode Transformation Format
- [UTF-32](https://en.wikipedia.org/wiki/UTF-32) - 32-bit Unicode Transformation Format

By default, Python source code files are treated as encoded in [UTF-8](https://en.wikipedia.org/wiki/UTF-8). This ensures that characters of most languages can be simultaneously used in string literals, identifiers, and comments.

UTF-8 uses one to four bytes to encode each character. The first 128 characters of UTF-8 are encoded using one byte with the same binary value as ASCII.

The standard library only uses [ASCII](https://en.wikipedia.org/wiki/ASCII) characters for identifiers.

To properly display all characters, your editor must recognize that the file is UTF-8 and use a font that supports all characters in the file.

To declare an encoding other than the default one, add a special comment line to the _first_ line of the file:

`# -*- coding: ex_encoding -*-`

`ex_encoding` should be a valid [codec](https://docs.python.org/library/codecs.html#module-codecs) supported by Python.

If the source code file starts with a shebang, the encoding declaration should be added as the second line of the file:

```python
#!/usr/bin/env python3
# -*- coding: ex_encoding -*-
```

#### Character Encoding Functions

- [`chr(i)`](https://docs.python.org/library/functions.html#chr) - Return the string representing a character whose Unicode code point is the integer `i`.
- [`ord(c)`](https://docs.python.org/library/functions.html#ord) - Given a string representing one Unicode character, return an integer representing the Unicode code point of that character.

#### Escape Sequences

- `\\` - Backslash
- `\b` - Backspace
- `\a` - Bell (alert)
- `\"` - Double quote
- `\0` - End-of-string
- `\f` - Form feed (newpage)
- `\t` - Horizontal tab
- `\n` - Newline
- `\r` - Return
- `\'` - Single quote
- `\v` - Vertical tab
- `\uxxxx` - 16-bit hex value Unicode character
- `\Uxxxxxxxx` - 32-bit hex value Unicode character
- `\xhh` - Prints character based on its hex value

### Continuation Lines

Continuation lines start with the _secondary prompt_, `...`:

```pycon
>>> flag = True
>>> if flag:
...     print('Three consecutive periods are an ellipsis.')
... 
Three consecutive periods are an ellipsis.
>>> 
```

### Input

The built-in [`input()`](https://docs.python.org/library/functions.html#input) function reads a line from input, converts it to a string (stripping a trailing newline), and returns it.

An optional prompt parameter is supported:

```pycon
>>> prompt = 'A very, very, very, very long prompt: '
>>> res = input(prompt).lower().strip()
A very, very, very, very long prompt: 
```

## The Zen of Python

```pycon
>>> import this
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
>>> 
```

## Styling Guidelines

[PEP8](https://peps.python.org/pep-0008/) provides coding conventions for the Python code in the standard library. It denotes good best practices, but can be overridden by project-specific guides.

### Indentation

Use four spaces per indentation level.

Continuation lines can either:

- Vertically wrap elements using Python's implicit line joining inside of parentheses, brackets, and braces.
- Use a hanging indent.

    No arguments should be listed on the first line and further indentation should be used to distinguish it as a continuation line.

```python
def ex_long_func_name(var_1, var_2,
                      var_3, var_4):
    print(var_1)


def ex_long_func_name(
        var_1, var_2,
        var_3, var_4):
    print(var_1)
```

Long `if`-statements that span multiple lines can either have extra indentation or not:

```python
if (ex_cond_1
        and ex_cond_2):
    ex_func()

if (ex_cond_1
    and ex_cond_2):
    ex_func()
```

The closing parenthesis/bracket/brace may be lined up under the first character of the line that starts the multiline construct:

```python
ex_list = [
    1, 2, 3,
    4, 5, 6,
]

result = ex_func(
    'a', 'b', 'c',
    'd', 'e', 'f',
)
```

### Spaces/Tabs

Spaces are the preferred Python indentation method.

Tabs can be used to remain consistent with code that is already indented with tabs.

Mixing spaces and tabs is not allowed in Python.

### Line Length

Limit documentation strings (docstrings) and comments to a maximum of 72 characters.

All other lines should be limited to a maximum of 79 characters.

Preferably, use implied line continuation inside parentheses/brackets/braces to wrap long lines. However, using a backslash may be appropriate, too. Ensure that the continued line is appropriately indented.

### Binary Operator Line Breaks

It is OK to break either before or after a binary operator, as long as it is done so consistently. However, breaking before a binary operator is preferred.

```python
income = (gross_wages
          + taxable_interest
          + (dividends - qualified_dividends)
          - ira_deduction
          - student_loan_interest)
```

### Blank Lines

Function and class definitions should be surrounded with two blank lines.

Method definitions inside of a class should be surrounded with one blank line.

Extra blank lines can be sparingly used to separate groups of related functions, or to indicate logical sections inside of functions.

### Imports

Usually, imports go on separate lines:

```python
import os
import sys
```

Multiple imports from the same object can go on the same line:

`from subprocess import Popen, PIPE`

Imports should be placed at the top of a file, immediately after module docstrings/comments, and before module globals/constants.

Group imports in the following order:

1. Standard library imports
2. Related third-party imports
3. Local application/library-specific imports

A blank line should be placed between import groups.

Absolute imports are recommended:

```python
import ex_pkg.sibling
from ex_pkg import sibling
from ex_pkg.sibling import example
```

Explicit relative imports are OK, especially when a complex package layout would make using absolute imports verbose.

Wildcard imports (`from ex_module import *`) should be avoided, except when republishing an internal interface as part of a public API.

### Module-Level Dunder Names

Module level dunders (e.g., `__all__`, `__author__`, `__version__`) should be placed after the module docstring, but before any import statements (except for `from __future__` imports):

```python
'''
This is an example module.

It does stuff.
'''

from __future__ import annotations

__all__ = ['a', 'b', 'c']
__version__ = '1.0'
__author__ = 'Moash'

import os
import sys
```

### String Quotes

Single- or double-quote strings are OK, but be consistent in your usage. When a string contains a single or double quote, use the other type of quote for the string to avoid using backslashes.

Inside of triple-quoted strings, always use double quotes.

### Whitespace

Avoid gratuitous whitespace:

- Immediately inside of parentheses/brackets/braces
- Between a trailing comma and a following close parenthesis
- Immediately before a comma, semicolon, or colon
- Immediately before the open parenthesis that starts the argument list of a function call
- Immediately before the open parenthesis that starts an indexing or slicing
- Around an assignment (or other) operator to align it with another

In a slice, the colon acts like a binary operator. It should have equal amounts of whitespace on either side. In an extended slice, both colons should have the same amount of spacing applied. An exception is when a slice parameter is omitted. Then, the space is omitted, too.

```python
foo[1:9], foo[1:9:3], foo[:9:3], foo[1::3], foo[1:9:]
foo[lower:upper], foo[lower:upper:], foo[lower::step]
foo[lower+offset : upper+offset]
foo[: upper_fn(x) : step_fn(x)], foo[:: step_fn(x)]
foo[lower + offset : upper + offset]
```

Additional whitespace recommendations include:

- Avoid trailing whitespace.
- Surround the following binary operators with a single space:

    - `=`
    - `+=`, `-=`, ...
    - `==`, `<`, `>`, `!=`, `<=`, `>=`, `in`, `not in`, `is`, `is not`
    - `and`, `or`, `not`

- Function annotations should use the normal colon rules and always have spaces around an arrow (`->`), if present.
- Do not use spaces around the `=` character when it is used to indicate a keyword argument or a default value for an _unannotated_ function parameter (e.g., `def ex_func(input: str, sep: str = None, limit=1000):`).
- Generally, compound statements (multiple statements on the same line) are discouraged.

### Trailing Commas

Usually, trailing commas are optional, except when making a tuple with one element (it is recommended to surround this type of tuple with parentheses, for clarity).

When trailing commas are redundant, they can can still be helpful when a collection of values, arguments, or imported items is expected to be extended. Put each value on a line by itself, add a trailing comma, and add the closing parentheses/brackets/braces on the next line:

```python
FILES = [
    'setup.cfg',
    'tox.ini',
]
```

### Comments

Prioritize keeping comments up-to-date after code changes.

The first word in a comment should be capitalized, unless it is an identifier that begins with a lowercase letter.

Generally, block comments are one or more paragraphs of complete sentences. They usually apply to some (or all) code that follows them, and are indented to the same level as that code. Each line in a block comment starts with a single `#` and a single space. Paragraphs inside of a block comment are separated by a line containing a single `#`.

An inline comment is a comment on the same line as a statement. They should be separated by at least two spaces from the statement, and start with a `#` and a single space. Use inline comments sparingly.

### Documentation Strings

A documentation string (docstring) is a string literal that becomes the `.__doc__` special attribute of the object that it documents. All scripts, public modules, functions, classes, and methods should have docstrings.

Docstrings are not required for non-public methods, but there should be a comment that describes what the method does. The comment should appear after the `def` line.

#### One-Line Docstrings

For one-line docstrings:

- Use triple quotes. This makes it easy to later expand, if necessary.
- There should be no blank line either before or after the docstring.
- The docstring should prescribe the effect of the object that it documents as an imperative sentence.

```python
def square_num(num):
    '''Return a number squared.'''
    return num**2
```

#### Multi-Line Docstrings

Multi-line docstrings have a summary line like a one-line docstring, followed by a blank line and a more elaborate description. The summary line may be on the same line as the opening quotes, or on the next line. The docstring should be indented the same as the quotes at the first line.

##### Function Docstrings

Function docstrings should summarize function behavior and document its arguments, return values, side effects, raised exceptions, and call restrictions (all if applicable). Optional arguments and whether keyword arguments are supported should be indicated.

##### Class Docstrings

Insert a blank line after the docstring, whether it is a one-line or multi-line docstring. Class methods should be separated from each other by a single blank line.

Class docstrings should summarize class behavior, and list its public methods and instance variables. The class constructor should be documented in the docstring for its `.__init__()` method. Individual methods should have their own docstrings. If a class is going to be subclassed and has an additional interface for subclasses, the interface should be listed separately in the docstring.

If a class subclasses another class and its behavior is mostly inherited from that class, its docstring should denote this and summarize the differences. Use the term _override_ to indicate that a subclass method replaces a superclass method. Use the term _extend_ to indicate that a subclass method calls the superclass method, in addition to its own behavior.

##### Script/Program Docstrings

Script/Program docstrings should be usable as the program's _usage_ message, printed when the script is invoked with incorrect/missing arguments, or when help is requested. The docstring should denote the program's function and command line syntax, environment variables, and files.

##### Module Docstrings

Module docstrings should list the classes, exceptions, and functions (and any other objects) that are exported by the module, with a one-line summary of each.

##### Package Docstrings

The docstring of the package's `__init__.py` module.

Package docstrings should list the classes, exceptions, functions, modules, and subpackages (and any other objects) that are exported by the package, with a one-line summary of each.

### Naming Conventions

Do not use the characters `l`, `O`, or `I` as single character variable names. In some fonts, these characters may be ambiguous.

Identifiers should be ASCII compatible.

#### Underscores

In the Python interpreter, a `_` returns the value of the last executed expression value:

```pycon
>>> a = 4
>>> b = 6
>>> _
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name '_' is not defined
>>> a + b
10
>>> _
10
>>> _ * 2
20
>>> _
20
>>> _ / 2
10.0
>>> 
```

`_` is also used as a throwaway variable name:

```python
for _ in range(5):
    print ('Jerome, Jerome, the metronome.')
```

```python
# Here, a function returns four items, but we only care about the first two
a, b, _, _ = ex_func(ex_var)
```

There are several special naming forms using leading or trailing underscores:

- `_single_leading_underscore` - A weak _internal use_ indicator. Serves as an indicator to other programmers that a class object is for internal use only.

    Does not create a truly private variable.

        class Prefix:
            def __init__(self):
                self.public = 10
                self._private = 12


        ex_inst = Prefix()

        print(ex_inst.public)
        # 10
        print(ex_inst._private)
        # 12

    However, `from ex_module import *` does not import variables whose names start with `_`.

- `single_trailing_underscore_` - A convention to avoid conflicts with Python keywords.
- `__double_leading_underscore` - Invokes _name mangling_ when naming a class attribute. Helps avoid name conflicts with names defined by a class's subclasses (e.g., inside class `FooBar`, `__boo` becomes `_FooBar__boo`).

        class Cache:
            def __init__(self, iterable):
                self.items = []
                self.__update(iterable)

            def update(self, iterable):
                for item in iterable:
                    self.items.append(item)

            __update = update  # Private copy of original .update() method


        class CacheSubclass(Cache):
            # New signature for .update(), without breaking .__init__()
            def update(self, keys, values):
                for item in zip(keys, values):
                    self.items.append(item)


        sequence = [1, 2, 3]
        mapping = {
            'one': 1,
            'two': 2,
            'three': 3
        }

        inst_1 = Cache(sequence)
        inst_2 = CacheSubclass(sequence)

        print(f'Cache instance items after instantiation: {inst_1.items}')
        # Cache instance items after instantiation: [1, 2, 3]
        print(f'CacheSubclass instance items after instantiation: {inst_2.items}')
        # CacheSubclass instance items after instantiation: [1, 2, 3]

        inst_2.update(mapping.keys(), mapping.values())

        print(f'CacheSubclass instance items after .update(): {inst_2.items}')
        # CacheSubclass instance items after .update(): [1, 2, 3, ('one', 1), ('two', 2), ('three', 3)]

- `__double_leading_and_trailing_underscore__` - _Magic_ objects/attributes that live in user-controlled namespaces (e.g., `.__init__()`, `.__import__()`). Used to emulate the behavior of built-in types for user-defined objects.

    Ensures that custom objects can work with Python built-ins and operators (i.e., if the behavior of a built-in function/operator is not defined in a user-defined class by the special method, a `TypeError` will occur). This practice is referred to as _overloading_.

        class Order:
            def __init__(self, cart, customer):
                self.cart = list(cart)
                self.customer = customer
                
            def __len__(self):
                return len(self.cart)


        order = Order(['contacts', 'fingertip sachets', 'shoes'],
                      'Vincent Freeman')

        print(len(order))
        # 3

#### Module and Package Names

Modules and packages should have short, lowercase names. Underscores can be used for module names, but are discouraged for package names.

If an extension module is written in C/C++ and includes a module that provides a higher-level interface, the C/C++ module should have a leading underscore (e.g., `_socket`).

#### Class Names

Class names should use the CapWords convention (`ExClass`).

Be aware that there is a separate convention for _builtin names_. Most builtin names are single words (or two combined words), with the CapWords convention only used for exception names and builtin constants.

#### Function and Variable Names

Function and variable names should be lowercase, with words separated by underscores.

#### Constants

Usually, constants are defined on the module level and written in all capital letters with underscores separating words.

### Recommendations

- Comparison to [singletons](https://en.wikipedia.org/wiki/Singleton_pattern) (e.g., `None`) should be done with `is` or `is not`, not the equality operators. Also, be aware of using `if x`, when you mean `if x is not None`.
- When implementing ordering operations with rich comparisons, implement all six operations (`__eq__`, `__ne__`, `__lt__`, `__le__`, `__gt__`, `__ge__`).
- Derive exceptions from `Exception`, rather than `BaseException`. Add the suffix `Error` to your exception classes if the exception is an error.
- When catching exceptions, be explicit instead of using a bare `except:` clause. If you want to catch all exceptions, use `except Exception:`.
- For `try`/`except` statements, limit the `try` clause to the absolute minimum amount of code necessary to avoid masking bugs.
- When a resource is local to a specific section of code, use a `with` statement or a `try`/`except` statement to ensure it is cleaned up.
- Be consistent in `return` statements, i.e., either all `return` statements in a function should return an expression, or none of them should. Any `return` statements where no value is returned should explicitly state `return None`.
- Use `''.startswith()` or `''.endswith()` instead of slicing to check for prefixes or suffixes, respectively.
- Object type comparison should use `isinstance()` instead of directly comparing types.
- For sequences (e.g., strings, lists, tuples), use the fact that empty sequences evaluate to `False` (e.g., `if seq:`, `if not seq:`).
- Do not write string literals that rely on significant trailing whitespace, as some editors may trim them.
- Do not compare boolean values to `True` or `False` using `==`.
- Use of flow control statements (`return`/`break`/`continue`) in the `finally` clause of a `try...finally` block is discouraged, as such statements will implicitly cancel any active exception propagating through the `finally` clause.

## Grammar

### Identifiers

[Identifiers](https://docs.python.org/reference/lexical_analysis.html#identifiers) are names given to entities like variables, functions, and objects. Python identifiers are:

- Case-sensitive
- Can only contain letters, numbers, and underscores
- Cannot start with a number
- Cannot be the same as Python keywords

Certain identifiers, besides keywords, have a special meaning in Python. These classes are denoted by leading/trailing underscore characters:

- `_*` - Not imported by `from ex_module import *`
- `_` - In a `case` pattern in a [`match`](https://docs.python.org/reference/compound_stmts.html#match) statement, `_` denotes a wildcard.
- `__*__` - [System-defined names](https://docs.python.org/reference/datamodel.html#specialnames) (_dunder_ names).
- `__*` - Class-private names. When used within the context of a class definition, these names are re-written to use a mangled form to avoid name clashes between _private_ attributes of base and derived classes.

You can confirm that an identifier is valid using the [`.isidentifier()`](https://docs.python.org/library/stdtypes.html#str.isidentifier) method:

```pycon
>>> 'abc'.isidentifier()
True
>>> '123abc'.isidentifier()
False
>>> '_'.isidentifier()
True
>>> 
```

To test whether a string is a reserved keyword, use [`keyword.iskeyword()`](https://docs.python.org/library/keyword.html#keyword.iskeyword):

```pycon
>>> from keyword import iskeyword
>>> 'abc'.isidentifier(), iskeyword('abc')
(True, False)
>>> 'def'.isidentifier(), iskeyword('def')
(True, True)
>>> 
```

Python automatically allocates the memory space for each object. Virtually every item of data in a Python program is an object of a specific type or class.

The type of an object can be determined with the [`type()`](https://docs.python.org/library/functions.html#type) function:

```pycon
>>> type(1997)
<class 'int'>
>>> type('Now, we have discrimination down to a science.')
<class 'str'>
>>> 
```

### Statements

A [statement](https://docs.python.org/glossary.html#term-statement) is part of a suite (block of code). It is either an expression or one of several constructs with a keyword (e.g., `if`, `while`, `for`).

An assignment operation is an example of a statement, i.e., the `=` sign is used to assign an expression to an identifier, which creates a statement.

### Expressions

Any syntax that can be evaluated to a value is an [expression](https://docs.python.org/glossary.html#term-expression), i.e., an expression is a collection of expression elements (e.g., identifiers, literals, operators) that all return a value.

```pycon
>>> 4
4
>>> 'titan'
'titan'
>>> True
True
>>> 
```

There are also [expression statements](https://docs.python.org/reference/simple_stmts.html#expression-statements) that are used (usually interactively) to compute and write a value, or to call a procedure (i.e., a function that returns no meaningful result):

```pycon
>>> 9 * 5
45
>>> bool(None)
False
>>> 
```

Keep in mind, there are statements that cannot be used as expressions (e.g., [simple statements](https://docs.python.org/reference/simple_stmts.html) like `return` and `import`, and [compound statements](https://docs.python.org/reference/compound_stmts.html) like `if` statements and function definitions).

### Operators

_Operators_ are symbols that tell Python to do a specific kind of operation:

- A _binary operator_ acts on two operands.
- A _unary operator_ acts on one operand.

Some operators can act as a unary or binary operator, depending on how an expression is written, e.g., in `-88 - 6`, the first `-` is a unary operator and the second `-` is a binary operator.

There are several types of operators in Python.

#### Mathematical

- `+` - Addition or unary plus (`x + y`).
- `-` - Subtraction or unary minus (`x - y`).
- `*` - Multiplication (`x * y`).
- `/` - Division (`x / y`). The result of a division is always a `float`.
- `%` - Modular arithmetic (`x % y`, i.e., the remainder of `x / y`). If an operand is a `float`, the result will be a `float`.

    Often used to determine if a number is even or odd (i.e., when a number is divisible by another number, and the remainder is `0`).

- `//` - [Floor division](https://docs.python.org/glossary.html#term-floor-division), i.e., mathematical division that rounds down to the nearest integer (`x // y`). If an operand is a `float`, the result will be a `float`.
- `**` Exponent (`x ** y`).

#### Bitwise

Bitwise operators treat numbers as strings of bits written in [two's-complement binary](https://en.wikipedia.org/wiki/Two%27s_complement).

- `&` - _Bitwise AND_ (`x & y`). Performs [logical conjunction](https://en.wikipedia.org/wiki/Logical_conjunction).

    For each corresponding pair of bits in the operands, returns a `1` only when both bits are `1`.

- `|` - _Bitwise OR_ (`x | y`). Performs [logical disjunction](https://en.wikipedia.org/wiki/Logical_disjunction).

    For each corresponding pair of bits in the operands, returns a `1` if at least one operand is `1`.

- `^` - _Bitwise Exclusive OR_ (`x ^ y`). Performs [exclusive disjunction](https://en.wikipedia.org/wiki/Exclusive_or).

    For each corresponding pair of bits in the operands, returns `1` only when a bit pair contains opposing bit values.

- `~` - _Bitwise NOT (`~x`). Performs [logical negation](https://en.wikipedia.org/wiki/Negation).

    For each bit in the operand, returns the opposing bit value.

- `<<` - Shift left `y` places (`x << y`). Returns `x` with the bits shifted to the left by `y` places (and new bits on the right-hand side are `0`s). Equivalent to `x * (2 ** y)`.
- `>>` - Shift right `y` places (`x >> y`). Returns `x` with the bits shifted to the right by `y` places. Equivalent to `x // (2 ** y)`.

#### Augmented Assignment

Most mathematical and bitwise operators support [augmented assignment](https://docs.python.org/reference/simple_stmts.html#augmented-assignment-statements) notation:

```python
+=      -=      *=      /=      //=     %=      @=
&=      |=      ^=      >>=     <<=     **=
```

#### Relational

- `>` - Greater than (`x > y`).
- `<` - Less than (`x < y`).
- `==` - Equal to (`x == y`).
- `!=` - Not equal to (`x != y`).
- `>=` - Greater than or equal to (`x >= y`).
- `<=` - Less than or equal to (`x <= y`).

Equality tests are _case-sensitive_.

#### Logical

- `and` - `True` if both operands are true (`bool(x and y)`).
- `or` - `True` either operand is true (`bool(x or y)`)
- `not` - Inverts the operand (`not x`).

#### Membership

- `in` - Checks to see if a value is in a collection (`a in [a, t, c, g]`).
- `not in` - Checks to see if a value is not in a collection (`a not in [a, t, c, g]`).

#### Identity

- `is` - Evaluates to `True` if the operands point to the same object, and `False` otherwise.
- `is not` - Evaluates to `False` if the operands point to the same object, and `True` otherwise.

The identify of a Python object can be obtained with the built-in [`id()`](https://docs.python.org/library/functions.html#id) function.

#### Operator Precedence

_Precedence_ rules determine the order of operations. Operators with the same precedence are evaluated from left to right.

The precedence operators, from highest to lowest precedence:

1. `()` - Parentheses (Grouping)
2. `f(args...)`, `x[i:i]`, `x[i]`, `x.attr` - Function Call, Slicing, Subscript, Dot
3. `**` - Exponentiation
4. `~`, `+`, `-` - Bitwise NOT, Unary Positive, Unary Negative
5. `*`, `/`, `%`, `//` - Multiplication, Division, Modulo, Floor Division
6. `+`, `-` - Addition, Subtraction
7. `<<`, `>>` - Left Bitwise Shift, Right Bitwise Shift
8. `&` - Bitwise AND
9. `^` - Bitwise Exclusive OR
10. `|` - Bitwise OR
11. `<=`, `<`, `>`, `>=` - Relational Operators
12. `==`, `!=` - Equality Operators
13. `=`, `%=`, `/=`, `//=`, `-=`, `+=`, `*=`, `**=` - Assignment Operators
14. `is`, `is not` - Identity Operators
15. `in`, `not in` - Membership Operators
16. `not` - Boolean NOT
17. `and` - Boolean AND
18. `or` - Boolean OR
19. `lambda` - Lambda Expression

Be explicit in what you intend and use parentheses, when needed.

### Delimiters

[Delimiters](https://docs.python.org/reference/lexical_analysis.html#delimiters) separate different elements in Python statements and expressions:

```python
(       )       [       ]       {       }
,       :       .       ;       @       =       ->
```

_Augmented assignment operators_ serve as delimiters, but also perform an operation:

```python
+=      -=      *=      /=      //=     %=      @=
&=      |=      ^=      >>=     <<=     **=
```

### Keywords

[Keywords](https://docs.python.org/3.9/reference/lexical_analysis.html#keywords) are special reserved words that have specific meanings and cannot be used as ordinary identifiers:

```python
and        continue   finally    is         raise
as         def        for        lambda     return
assert     del        from       None       True
async      elif       global     nonlocal   try
await      else       if         not        while
break      except     import     or         with
class      False      in         pass       yield
```

- `and` - The logical AND. Used to determine if both the left and right operands are [truthy or falsy](https://en.wikipedia.org/wiki/Truth_value#Computing).

    If both operands are truthy, then the result will be truthy. If one is falsy, then the result will be falsy.

    `cond_1 and cond_2`

- `as` - Used to alias an imported module or tool. It is used together with the Python keywords `import` and `from` to change the name of the thing being imported.

        import ex_module as ex_alias
        from ex_module import ex_obj as ex_alias

    Can also be used to capture an exception message (but not an _exception type_) into a variable.

        except Exception as exc:
            ...

- `assert` - Used to specify an `assert` statement or an assertion about an expression. Results in a [no-op](https://en.wikipedia.org/wiki/NOP_(code)) (no operation) if the expression is truthy, and an `AssertionError` if the expression is falsy.

    `assert ex_expr`

- `async` - Used to introduce a coroutine.

    [Coroutines](https://docs.python.org/glossary.html#term-coroutine) are a generalized form of subroutine that can suspend their execution before reaching `return`, and can indirectly pass control to another coroutine for some time.

        import asyncio


        async def ex_coro():
            await ex_func()

- `await` - Pass function control back to the [event loop](https://docs.python.org/library/asyncio-eventloop.html).
- `break` - Used to to exit a loop early. Works in both `for` and `while` loops.

        for ex_elem in ex_ctnr:
            if ex_cond:
                break

- `class` - Used to define a class.

        class ExClass():
            ...

- `continue` - Used to skip to the next loop iteration. Allows you to stop executing the current loop iteration and move on to the next iteration.

        for ex_elem in ex_ctnr:
            if ex_cond:
                continue

- `def` - Used to define a function or a class method.

        def ex_func(ex_params):
            ...

- `del` - Used in Python to unset a name. Often used to unset variables, and to remove indices from lists and dictionaries.

        del ex_name_1
        del ex_name_2[ex_idx]

- `elif` - Like an `if` statement, with some key differences:
    1. Use of `elif` is only valid after an `if` statement or another `elif`.
    2. Multiple `elif` statements are permitted.

        if ex_cond_1:
            ...
        elif ex_cond_2:
            ...
        elif ex_cond_3:
            ...

- `else` - In conjunction with `if` and `elif`, denotes a block of code that should only be executed if the other conditional blocks are all falsy:

        if ex_cond_1:
            ...
        elif ex_cond_2:
            ...
        else:
            ...

    Also, used with `for` statements to indicate when the `for` loop completes without encountering a `break`, or with a `while` loop once its condition becomes falsy:

        for ex_elem in ex_ctnr:
            ...
            if ex_cond:
                break
        else:
            ...

        while ex_cond:
            ...
        else:
            ...

- `except` - Used with `try` to define what to do when specific exceptions are raised. Multiple `except` blocks with a single `try` are permitted.

        try:
            ...
        except ex_exc:
            ...

- `False` - The Boolean false value. `False` is equal to `0`.

        >>> x = False
        >>> x is False
        True
        >>> False == 0
        True
        >>> 

- `finally` - Used with `try` to specify code that should always run, regardless of what happens in the `try`, `except`, or `else` blocks.

        try:
            ...
        except Exception:
            ...
        finally:
            ...

- `for` - Used to loop over a known number of elements.

        for ex_elem in ex_ctnr:
            ...

- `from` - Used with `import` to import something specific from a module.

    `from ex_module import ex_obj`

- `global` - Used when you need to modify a variable that is not defined in a function, but is defined in the _global scope_.

        def set_global():
            global x
            x = 10


        x = 20
        print(x)  # 20
    
        set_global()
        print(x)  # 10

- `if` - Used to start a conditional statement that is only executed if the expression after `if` is truthy.

        if ex_cond:
            ...

- `import` - Used to import (include) a module.

    `import ex_module`

- `in` - A membership operator. Given an element to find and a container to search, returns `True` or `False`, indicating whether the element was found (or not) in the container.

    `ex_elem in ex_ctnr`

- `is` - An identity operator. Determines whether two objects are the exact same objects in memory.

    `ex_obj_1 is ex_obj_2`

- `lambda` - Used to define an anonymous inline function consisting of a single expression, which is evaluated when the function is called.

        lambda ex_arg: ex_expr  # One argument
        lambda ex_arg_1, ex_arg_2: ex_expr  # Multiple arguments

    By default, a lambda function is not applied. To apply it to an argument, surround it and its argument with parentheses:

        >>> (lambda x: x + 1)(2)
        3
        >>> 

    Lambda functions are commonly used to specify a different behavior for another function:

        >>> idxs = ('idx_1', 'idx_2', 'idx_30', 'idx_3', 'idx_20', 'idx_10')
        >>> sorted(idxs)
        ['idx_1', 'idx_10', 'idx_2', 'idx_20', 'idx_3', 'idx_30']
        >>> sorted(idxs, key=lambda x: int(x[4:]))
        ['idx_1', 'idx_2', 'idx_3', 'idx_10', 'idx_20', 'idx_30']
        >>> 

- `None` - Represents no value. Also, the default value returned by a function without a `return` statement.

        >>> def sequenced():
        ...     print("It seems you're everything they say you are and more.")
        ... 
        >>> irene = sequenced()
        It seems you're everything they say you are and more.
        >>> print(irene)
        None
        >>> 

- `nonlocal` - Used when you need to modify a variable that is defined in a _parent scope_ (i.e., an enclosing, non-global scope). Can only be used in a nested/enclosed function.

        def parent():
            def child():
                nonlocal x
                x = 5

            x = 10
            print(f'parent x: {x}')

            child()
            print(f'parent x after calling child: {x}')


        x = 20
        print(f'global x: {x}')

        parent()

    The code above yields the following output:

        global x: 20
        parent x: 10
        parent x after calling child: 5

- `not` - The logical NOT. Used to get the opposite Boolean value.

        >>> ex_val = ''
        >>> bool(ex_val)
        False
        >>> not ex_val
        True
        >>> ex_val = 87
        >>> bool(ex_val)
        True
        >>> not ex_val
        False
        >>> 

- `or` - The logical OR. Used to determine if at least one of the operands is truthy. 

    `ex_cond_1 or ex_cond_2`

- `pass` - Used to specify that a block is intentionally left blank. Equivalent to a no-op.

        def ex_func():
            pass


        class ExClass:
            pass


        if True:
            pass

- `raise` - Raise an exception.

    `raise ex_exc`

- `return` - Valid only as part of a function defined with `def`. When encountered, Python exits the function and returns the results of whatever comes after the `return` keyword.

        def ex_func():
            return ex_expr

    Can be used to arbitrarily break out of a function at a specific point.

- `True` - The Boolean true value. `True` is equal to `1`.

        >>> x is True
        True
        >>> x is not True
        False
        >>> True == 1
        True
        >>> 

- `try` - Used with `except`, `else`, and `finally` to create exception-handling blocks.

        try:
            ...
        except ex_exc:
            ...
        else:
            ...
        finally:
            ...

- `while` - Used to create a while loop. As long as the condition that follows the `while` keyword is truthy, the block following the `while` statement will continue to be repeatedly executed.

        while ex_cond:
            ...

- `with` - Used to create [context managers](https://docs.python.org/glossary.html#term-context-manager). Gives you a way to define code to be executed within the context manager’s scope.

        with ex_expr as ex_tgt_var:
            ...

    Often used when working with file I/O.

- `yield` - Similar to `return` in that it specifies what gets returned from a function. However, when a function has a `yield` statement, it is a [generator](https://docs.python.org/glossary.html#index-19) that returns a [generator iterator](https://docs.python.org/glossary.html#term-generator-iterator). The generator iterator can be passed to the built-in [`next()`](https://docs.python.org/library/functions.html#next) function to get its next value.

        def ex_func():
            yield ex_expr

    The following example is a simple generator function that returns the same set of values:

        >>> def family():
        ...     yield 'Antonio'
        ...     yield 'Marie'
        ...     yield 'Vincent'
        ...     yield 'Anton'
        ... 
        >>> names = family()
        >>> names
        <generator object family at 0x7f5d2576e820>
        >>> next(names)
        'Antonio'
        >>> next(names)
        'Marie'
        >>> next(names)
        'Vincent'
        >>> next(names)
        'Anton'
        >>> next(names)
        Traceback (most recent call last):
        File "<stdin>", line 1, in <module>
        StopIteration
        >>> 

    Once the `StopIteration` exception is raised, the generator is done returning values. In order to go through the names again, you would need to call `family()` again and get a new generator.
    
    Most of the time, a generator function is called as part of a `for` loop, which handles the `next()` calls and `StopIteration` exception for you:

        >>> names = family()
        >>> for name in names:
        ...     print(name)
        ... 
        Antonio
        Marie
        Vincent
        Anton
        >>> 

## Variables

A Python _variable_ is a name that is a reference to an object. Once an object is assigned to a variable, you can refer to the object by that name, but the data itself is still contained within the object, i.e., a variable is just a label for an actual object in memory.

Every object has a number that uniquely identifies it. No two objects have the same identifier during any period in which their lifetimes overlap. Once an object’s reference count drops to zero and it is [garbage collected](https://docs.python.org/glossary.html?highlight=glossary#term-garbage-collection), its identifying number becomes available and may be used again.

The built-in function `id()` returns an object’s integer identifier. By using the `id()` function (or _identity operators_), you can verify that two variables point to the same object:

```pycon
>>> x = 'eugene'
>>> type(x)
<class 'str'>
>>> id(x)
140546249949040
>>> y = x
>>> id(y)  # x and y reference the same str object
140546249949040
>>> x = 'jerome eugene morrow'
>>> id(x)  # x now points to a new str object, with a new ID
140546250652608
>>> x is y
False
>>> 
```

### Objects

There are two types of objects in Python:

1. **Mutable**

    A mutable object is an object whose state can change. Examples of mutable objects are those of the `list`, `set`, `dict`, and (usually) custom class types. 
    
    Mutable objects are recommended when there is a need to change the size or content of an object.

2. **Immutable**

    An immutable object is an object whose state cannot change. Examples of immutable objects are the `int`, `float`, `bool`, `string`, `unicode`, and `tuple` types.
    
    Immutable objects are faster to access and are expensive to change because you need to create a _copy_ to effect a change.

    An immutable object (like a `tuple`) can contain a mutable object (like a `list`).

In general, [primitive-like](https://en.wikipedia.org/wiki/Primitive_data_type) types are immutable and container-like types are mutable.

A Python object is considered [hashable](https://docs.python.org/glossary.html#term-hashable) if it has a hash value that never changes during its lifetime (it needs a `.__hash__()` method) and can be compared to other objects (it needs an `.__eq__()` method). Hashable objects that compare equal must have the same hash value.

Most of Python’s immutable built-in objects are hashable. Mutable containers (`list`, `dict`) are not. Immutable containers (`tuple`, `frozenset`) are only hashable if their elements are hashable.

By default, objects that are instances of user-defined classes are hashable. They all compare unequal (except with themselves), and their hash value is derived from their ID.

Hashability makes an object usable as a dictionary key and a set member because these data structures internally use the hash value.

## Built-In Types

Python has many [built-in types](https://docs.python.org/library/stdtypes.html).

Also, there are [specialized data types](https://docs.python.org/library/datatypes.html):

- [`array`](https://docs.python.org/library/array.html) - Efficient arrays of numeric values.
- [`bisect`](https://docs.python.org/library/bisect.html) - Array bisection algorithm.
- [`calendar`](https://docs.python.org/library/calendar.html) - General calendar-related functions.
- [`collections`](https://docs.python.org/library/collections.html) - Container data types.
- [`collections.abc`](https://docs.python.org/library/collections.abc.html) - Abstract base classes for containers.
- [`copy`](https://docs.python.org/library/copy.html) - Shallow and deep copy operations.
- [`datetime`](https://docs.python.org/library/datetime.html) - Basic date and time types.
- [`enum`](https://docs.python.org/library/enum.html) - Support for enumerations.
- [`graphlib`](https://docs.python.org/library/graphlib.html) - Functionality to operate with graph-like structures.
- [`heapq`](https://docs.python.org/library/heapq.html) - Heap queue algorithm.
- [`pprint`](https://docs.python.org/library/pprint.html) - Data pretty printer.
- [`reprlib`](https://docs.python.org/library/reprlib.html) - Alternate `repr()` implementation.
- [`types`](https://docs.python.org/library/types.html) - Dynamic type creation and names for built-in types.
- [`weakref`](https://docs.python.org/library/weakref.html) - Weak references.
- [`zoneinfo`](https://docs.python.org/library/zoneinfo.html) - IANA time zone support.

A [singleton](https://en.wikipedia.org/wiki/Singleton_pattern) is when there is one and only one object of a particular type (e.g., `None`).

An [iterable](https://docs.python.org/glossary.html#term-iterable) is an object capable of returning its members one at a time (e.g., `str`, `list`, `dict`).

### Numeric

There are three distinct numeric types:

1. [`int`](https://docs.python.org/library/functions.html#int) - Holds signed integers of non-limited length.

    Booleans are a subtype of integers.

2. [`float`](https://docs.python.org/library/functions.html#float) - Holds floating precision numbers and is accurate up to `15` decimal places.
3. [`complex`](https://docs.python.org/library/functions.html#complex) - Holds [complex numbers](https://en.wikipedia.org/wiki/Complex_number).

The standard library also includes these additional numeric types:

- [`fractions.Fraction`](https://docs.python.org/library/fractions.html#fractions.Fraction) - For [rational numbers](https://en.wikipedia.org/wiki/Rational_number).
- [`decimal.Decimal`](https://docs.python.org/library/decimal.html#decimal.Decimal) - For floating-point numbers with user-definable precision.

Numbers are created by numeric literals, or as the result of built-in functions and operators.

Mixed arithmetic is supported, i.e., when a binary arithmetic operator has operands of different numeric types, the operand with the _narrower_ type is widened to that of the other (where `int` is narrower than `float`, and `float` is narrower than `complex`).

The constructors `int()`, `float()`, and `complex()` can be used to produce numbers of a specific type.

There are additional methods for both [`int`](https://docs.python.org/library/stdtypes.html#additional-methods-on-integer-types) and [`float`](https://docs.python.org/library/stdtypes.html#additional-methods-on-float) types.

### Boolean

Boolean values are the two constant objects `False` and `True`, which represent truth values. In numeric contexts, they behave like the integers `0` and `1`, respectively.

The built-in [`bool()`](https://docs.python.org/library/functions.html#bool) can be used to convert any value to a Boolean, if the value can be interpreted as a truth value.

### Sequence

Most mutable and immutable sequence types support the operations in the table below. The [`collections.abc.Sequence`](https://docs.python.org/library/collections.abc.html#collections.abc.Sequence) abstract base class (ABC) makes it easier to correctly implement these operations on custom sequence types.

Operations are listed in ascending priority.

In the table:

- `s` and `t` are sequences of the same type.
- `n`, `i`, `j`, and `k` are integers.
- `x` is an arbitrary object that meets any type and value restrictions imposed by `s`.

<table>
  <caption>Common Sequence Operations</caption>
  <thead>
    <tr>
      <th>Operation</th>
      <th>Result</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>x in s</code></td>
      <td><code>True</code> if an item of <code>s</code> is equal to <code>x</code>, else <code>False</code></td>
    </tr>
    <tr>
      <td><code>x not in s</code></td>
      <td><code>False</code> if an item of <code>s</code> is equal to <code>x</code>, else <code>True</code></td>
    </tr>
    <tr>
      <td><code>s + t</code></td>
      <td>The concatenation of <code>s</code> and <code>t</code></td>
    </tr>
    <tr>
      <td><code>s * n</code> or <code>n * s</code></td>
      <td>Equivalent to adding <code>s</code> to itself <code>n</code> times</td>
    </tr>
    <tr>
      <td><code>s[i]</code></td>
      <td><code>i</code>th item of <code>s</code>, origin <code>0</code></td>
    </tr>
    <tr>
      <td><code>s[i:j]</code></td>
      <td>Slice of <code>s</code> from <code>i</code> to <code>j</code></td>
    </tr>
    <tr>
      <td><code>s[i:j:k]</code></td>
      <td>Slice of <code>s</code> from <code>i</code> to <code>j</code> with step <code>k</code></td>
    </tr>
    <tr>
      <td><code>len(s)</code></td>
      <td>Length of <code>s</code></td>
    </tr>
    <tr>
      <td><code>min(s)</code></td>
      <td>Smallest item of <code>s</code></td>
    </tr>
    <tr>
      <td><code>max(s)</code></td>
      <td>Largest item of <code>s</code></td>
    </tr>
    <tr>
      <td><code>s.index(x[, i[, j]])</code></td>
      <td>Index of the first occurrence of <code>x</code> in <code>s</code> (at or after index <code>i</code> and before index <code>j</code>)</td>
    </tr>
    <tr>
      <td><code>s.count(x)</code></td>
      <td>Total number of occurrences of <code>x</code> in <code>s</code></td>
    </tr>
  </tbody>
</table>

Sequences of the same type support comparisons. Lists and tuples are compared lexicographically by comparing corresponding elements, i.e., to compare equal, every element must compare equal, and the two sequences must be of the same _type_ and have the same _length_.

Forward and reversed iterators over mutable sequences access values using an index. The index continues to move forward (or backward) even if the underlying sequence is changed. The iterator only terminates when an `IndexError` or `StopIteration` is encountered (or when the index drops below zero).

Immutable sequence types support [`hash()`](https://docs.python.org/library/functions.html#hash), which allows them to be used as `dict` keys and stored in `set` and `frozenset` instances.

Mutable sequence types support the operations in the table below. The [`collections.abc.MutableSequence`](https://docs.python.org/library/collections.abc.html#collections.abc.MutableSequence) ABC makes it easier to correctly implement these operations on custom sequence types.

In the table:

- `s` is an instance of a mutable sequence type.
- `t` is any iterable object.
- `x` is an arbitrary object that meets any type and value restrictions imposed by `s`.

<table>
  <caption>Mutable Sequence Operations</caption>
  <thead>
    <tr>
      <th>Operation</th>
      <th>Result</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>s[i] = x</code></td>
      <td>Item <code>i</code> of <code>s</code> is replaced by <code>x</code></td>
    </tr>
    <tr>
      <td><code>s[i:j] = t</code></td>
      <td>Slice of <code>s</code> from <code>i</code> to <code>j</code> is replaced by the contents of the iterable <code>t</code></td>
    </tr>
    <tr>
      <td><code>del s[i:j]</code></td>
      <td>Equivalent to <code>s[i:j] = []</code></td>
    </tr>
    <tr>
      <td><code>s[i:j:k] = t</code></td>
      <td>The elements of <code>s[i:j:k]</code> are replaced by those of <code>t</code></td>
    </tr>
    <tr>
      <td><code>del s[i:j:k]</code></td>
      <td>Removes the elements of <code>s[i:j:k]</code> from the list</td>
    </tr>
    <tr>
      <td><code>s.append(x)</code></td>
      <td>Appends <code>x</code> to the end of the sequence (equivalent to <code>s[len(s):len(s)] = [x]</code>)</td>
    </tr>
    <tr>
      <td><code>s.clear()</code></td>
      <td>Removes all items from <code>s</code> (equivalent to <code>del s[:]</code>)</td>
    </tr>
    <tr>
      <td><code>s.copy()</code></td>
      <td>Creates a shallow copy of <code>s</code> (equivalent to <code>s[:]</code>)</td>
    </tr>
    <tr>
      <td><code>s.extend(t)</code> or <code>s += t</code></td>
      <td>Extends <code>s</code> with the contents of <code>t</code></td>
    </tr>
    <tr>
      <td><code>s *= n</code></td>
      <td>Updates <code>s</code> with its contents repeated <code>n</code> times</td>
    </tr>
    <tr>
      <td><code>s.insert(i, x)</code></td>
      <td>Inserts <code>x</code> into <code>s</code> at the index given by <code>i</code> (equivalent to <code>s[i:i] = [x]</code>)</td>
    </tr>
    <tr>
      <td><code>s.pop()</code> or <code>s.pop(i)</code></td>
      <td>Retrieves the item at <code>i</code> and also removes it from <code>s</code></td>
    </tr>
    <tr>
      <td><code>s.remove(x)</code></td>
      <td>Removes the first item from <code>s</code> where <code>s[i]</code> is equal to <code>x</code></td>
    </tr>
    <tr>
      <td><code>s.reverse()</code></td>
      <td>Reverses the items of <code>s</code> in place</td>
    </tr>
  </tbody>
</table>

#### Strings

Textual data is handled with [`str`](https://docs.python.org/library/stdtypes.html#str) objects, which are immutable [sequences](https://docs.python.org/library/stdtypes.html#typesseq) of Unicode code points.

A string is simply a collection of characters. There is no separate _character_ type. Indexing a string produces strings of length `1`, i.e., for a non-empty string, `s[0] == s[0:1]`.

String literals are created in several ways:

- Single quotes: `'allows embedded "double" quotes'`
- Double quotes: `"allows embedded 'single' quotes"`
- Triple quotes: `'''three single quotes'''`, `"""three double quotes"""`

    These types of string literals can span multiple lines. All associated whitespace is included in the string literal.

Strings may also be created from other objects using the `str()` constructor.

The `r` (raw) prefix disables most [escape sequence processing](https://docs.python.org/reference/lexical_analysis.html#strings).

String literals that are part of a single expression and only have whitespace between them are implicitly converted into a single string literal, e.g., `'You must be drunk ' 'to call me Vincent.' == 'You must be drunk to call me Vincent.'`.

There is no mutable string type, but [`str.join()`](https://docs.python.org/library/stdtypes.html#str.join) or [`io.StringIO`](https://docs.python.org/library/io.html#io.StringIO) can be used to construct strings from multiple fragments.

A subset of helpful string methods are:

- [`str.capitalize()`](https://docs.python.org/library/stdtypes.html#str.capitalize) - Return a copy of the string with its first character capitalized and the rest lowercased.
- [`str.casefold()`](https://docs.python.org/library/stdtypes.html#str.casefold) - Return a casefolded copy of the string.
- [`str.center(width[, fillchar])`](https://docs.python.org/library/stdtypes.html#str.center) - Return centered in a string of length width.
- [`str.count(sub[, start[, end]])`](https://docs.python.org/library/stdtypes.html#str.count) - Return the number of non-overlapping occurrences of substring `sub` in the range from `start` to `end`.
- [`str.encode(encoding='utf-8', errors='strict')`](https://docs.python.org/library/stdtypes.html#str.encode) - Return the string encoded to `bytes`.
- [`str.endswith(suffix[, start[, end]])`](https://docs.python.org/library/stdtypes.html#str.endswith) - Return `True` if the string ends with the specified suffix, otherwise return `False`.
- [`str.find(sub[, start[, end]])`](https://docs.python.org/library/stdtypes.html#str.find) - Return the lowest index in the string where substring `sub` is found within the slice `sub[start:end]`.

    If you need to check whether a substring is inside of a string, use the [`in`](https://docs.python.org/reference/expressions.html#in) operator.

- [`str.isalnum()`](https://docs.python.org/library/stdtypes.html#str.isalnum) - Return `True` if all characters in the string are alphanumeric and there is at least one character, `False` otherwise.
- [`str.isalpha()`](https://docs.python.org/library/stdtypes.html#str.isalpha) - Return `True` if all characters in the string are alphabetic and there is at least one character, `False` otherwise.
- [`str.isidentifier()`](https://docs.python.org/library/stdtypes.html#str.isidentifier) - Return `True` if the string is a valid identifier according to the language definition.

    Use `keyword.iskeyword()` to test whether string `s` is a reserved identifier.

- [`str.islower()`](https://docs.python.org/library/stdtypes.html#str.islower) - Return `True` if all cased characters in the string are lowercase and there is at least one cased character, `False` otherwise.
- [`str.isnumeric()`](https://docs.python.org/library/stdtypes.html#str.isnumeric) - Return `True` if all characters in the string are numeric characters and there is at least one character, `False` otherwise.
- [`str.isspace()`](https://docs.python.org/library/stdtypes.html#str.isspace) - Return `True` if there are only whitespace characters in the string and there is at least one character, `False` otherwise.
- [`str.istitle()`](https://docs.python.org/library/stdtypes.html#str.istitle) - Return `True` if the string is a titlecased string and there is at least one character. Return `False` otherwise.
- [`str.isupper()`](https://docs.python.org/library/stdtypes.html#str.isupper) - Return `True` if all cased characters in the string are uppercase and there is at least one cased character, `False` otherwise.
- [`str.join(iterable)`](https://docs.python.org/library/stdtypes.html#str.join) - Return a string which is the concatenation of the strings in `iterable`.
- [`str.ljust(width[, fillchar])`](https://docs.python.org/library/stdtypes.html#str.ljust) - Return the string left justified in a string of length `width`.
- [`str.lower()`](https://docs.python.org/library/stdtypes.html#str.lower) - Return a copy of the string with all the cased characters converted to lowercase.
- [`str.lstrip([chars])`](https://docs.python.org/library/stdtypes.html#str.lstrip) - Return a copy of the string with leading characters removed.
- [`str.partition(sep)`](https://docs.python.org/library/stdtypes.html#str.partition) - Split the string at the first occurrence of `sep`, and return a 3-tuple containing the part before the separator, the separator itself, and the part after the separator.
- [`str.removeprefix(prefix, /)`](https://docs.python.org/library/stdtypes.html#str.removeprefix) - If the string starts with the `prefix` string, return `string[len(prefix):]`. Otherwise, return a copy of the original string.
- [`str.removesuffix(suffix, /)`](https://docs.python.org/library/stdtypes.html#str.removesuffix) - If the string ends with the `suffix` string and that `suffix` is not empty, return `string[:-len(suffix)]`. Otherwise, return a copy of the original string.
- [`str.replace(old, new[, count])`](https://docs.python.org/library/stdtypes.html#str.replace) - Return a copy of the string with all occurrences of substring `old` replaced by `new`.
- [`str.rfind(sub[, start[, end]])`](https://docs.python.org/library/stdtypes.html#str.rfind) - Return the highest index in the string where substring `sub` is found, such that `sub` is contained within `s[start:end]`.
- [`str.rjust(width[, fillchar])`](https://docs.python.org/library/stdtypes.html#str.rjust) - Return the string right justified in a string of length `width`.
- [`str.rpartition(sep)`](https://docs.python.org/library/stdtypes.html#str.rpartition) - Split the string at the last occurrence of `sep`, and return a 3-tuple containing the part before the separator, the separator itself, and the part after the separator.
- [`str.rsplit(sep=None, maxsplit=- 1)`](https://docs.python.org/library/stdtypes.html#str.rsplit) - Return a list of the words in the string, using `sep` as the delimiter string.
- [`str.rstrip([chars])`](https://docs.python.org/library/stdtypes.html#str.rstrip) - Return a copy of the string with trailing characters removed.
- [`str.split(sep=None, maxsplit=- 1)`](https://docs.python.org/library/stdtypes.html#str.split) - Return a list of the words in the string, using `sep` as the delimiter string.
- [`str.splitlines(keepends=False)`](https://docs.python.org/library/stdtypes.html#str.splitlines) - Return a list of the lines in the string, breaking at line boundaries.
- [`str.startswith(prefix[, start[, end]])`](https://docs.python.org/library/stdtypes.html#str.startswith) - Return `True` if string starts with the `prefix`, otherwise return `False`.
- [`str.strip([chars])`](https://docs.python.org/library/stdtypes.html#str.strip) - Return a copy of the string with the leading and trailing characters removed.
- [`str.swapcase()`](https://docs.python.org/library/stdtypes.html#str.swapcase) - Return a copy of the string with uppercase characters converted to lowercase and vice versa.
- [`str.title()`](https://docs.python.org/library/stdtypes.html#str.title) - Return a titlecased version of the string where words start with an uppercase character and the remaining characters are lowercase.
- [`str.translate(table)`](https://docs.python.org/library/stdtypes.html#str.translate) - Return a copy of the string in which each character has been mapped through the given translation table.
- [`str.upper()`](https://docs.python.org/library/stdtypes.html#str.upper) - Return a copy of the string with all the cased characters converted to uppercase.
- [`str.zfill(width)`](https://docs.python.org/library/stdtypes.html#str.zfill) - Return a copy of the string left filled with ASCII `'0'` digits to make a string of length `width`.

##### Formatted String Literals

A [formatted string literal (f-string)](https://docs.python.org/glossary.html?highlight=glossary#term-f-string) is a string literal prefixed with `f` or `F`. These strings may contain replacement fields that are expressions delimited by curly braces (`{}`). While string literals have constant values, f-strings are expressions evaluated at runtime.

Escape sequences are decoded as in ordinary string literals, except when a literal is also marked as a raw string.

The general format for an f-string is:

`f'ex_str{ex_replc_fld}ex_str'`

Any string outside of the curly braces is treated literally, except for doubled curly braces (`{{` or `}}`), which are replaced with a single curly brace.

The replacement field consists of a field expression, optionally followed by:

- `=` - Used to display both the expression text and its value after evaluation. This may be useful during debugging.

        >>> w = 'wind'
        >>> f"I'm sorry. The {w} caught it."
        "I'm sorry. The wind caught it."
        >>> f"I'm sorry. The {w=} caught it."
        "I'm sorry. The w='wind' caught it."
        >>> 

    Spaces after the opening brace (`{`) within the expression and after the `=` are retained in the output. By default, `=` produces the `repr()` of the expression, unless a format is specified. When a format is specified, it defaults to the `str()` of the expression, unless a `!r` conversion is declared.

- `!` - Used to introduce a _conversion field_ (`!s` for `str()`, `!r` for `repr()`, `!a` for `ascii()`). The result of evaluating the expression is converted before formatting.

        >>> y = 'year'
        >>> f'A {y} is a long time.'
        'A year is a long time.'
        >>> f'A {y!r} is a long time.'
        "A 'year' is a long time."
        >>> 

- `:` - Used to append a _format specifier_. Formatting uses the [`format()`](https://docs.python.org/library/functions.html#format) protocol and the [format specifier mini-language](https://docs.python.org/library/string.html#formatspec).

        >>> l = 'Not so long. Just once around the sun.'
        >>> f'{l:~^50}'
        '~~~~~~Not so long. Just once around the sun.~~~~~~'
        >>> 

    Top-level format specifiers may include nested replacement fields. Nested fields may include their own conversion fields and format specifiers, but may not include more deeply nested replacement fields.

Expressions in f-strings are treated like regular Python expressions surrounded by parentheses. There are a few exceptions:

- An empty expression is not allowed.
- Both [lambda](https://docs.python.org/reference/expressions.html#lambda) and [assignment expressions](https://docs.python.org/reference/expressions.html#assignment-expressions) (`:=`) must be surrounded by parentheses.
- Replacement expressions can contain line breaks, but they cannot contain comments.

Each expression is evaluated in context, where the f-string appears, in order from left to right.

f-strings may be concatenated, but replacement fields cannot be split across literals.

Characters in replacement fields must not conflict with the quoting used in the outer formatted string literal, i.e., do not use the same type of quotation mark on the outside of the f-string as you are using in the expression.

Backslashes (`\`) are not allowed in format expressions and raise an error. To include a value that requires a backslash escape, create a temporary variable:

```pycon
>>> b = "He's one of our best.\n"
>>> print(f"{b}I'm certain he has nothing to do with this business.")
He's one of our best.
I'm certain he has nothing to do with this business.
>>> 
```

f-strings cannot be used as docstrings, even if they do not include expressions.

##### Format Specification Mini-Language

The general form of a [standard format specifier](https://docs.python.org/library/string.html#format-specification-mini-language) is:

`[[fill]align][sign][z][#][0][width][grouping_option][.precision][type]`

- `fill` - any character (except a curly brace, `{` or `}`, when using f-strings or `str.format()`)
- `align` - `<` | `>` | `=` | `^`
- `sign` - `+` | `-` | `space`
- `width` - one or more digits
- `grouping_option` - `_` | `,`
- `precision` - one or more digits
- `type` - `b` | `c` | `d` | `e` | `E` | `f` | `F` | `g` | `G` | `n` | `o` | `s` | `x` | `X` | `%`

<table>
  <caption>Alignment Options</caption>
  <thead>
    <tr>
      <th>Option</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code><</code></td>
      <td>Force the field to be left-aligned within the available space</td>
    </tr>
    <tr>
      <td><code>></code></td>
      <td>Force the field to be right-aligned within the available space</td>
    </tr>
    <tr>
      <td><code>=</code></td>
      <td>Force the padding to be placed after the sign (if any), but before the digits (only valid for numeric types)</td>
    </tr>
    <tr>
      <td><code>^</code></td>
      <td>Force the field to be centered within the available space</td>
    </tr>
  </tbody>
</table>

Unless a minimum field width is defined, the field width is always the same size as the data to fill it (i.e., the alignment option has no meaning unless a width is set).

<table>
  <caption>Sign Options</caption>
  <thead>
    <tr>
      <th>Option</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>+</code></td>
      <td>Indicate that a sign should be used for both positive and negative numbers</td>
    </tr>
    <tr>
      <td><code>-</code></td>
      <td>Indicate that a sign should only be used for negative numbers (the default behavior)</td>
    </tr>
    <tr>
      <td><code>space</code></td>
      <td>Indicate that a leading space should be used on positive numbers, and a minus sign on negative numbers</td>
    </tr>
  </tbody>
</table>

The `z` option (only valid for floating-point presentation types) coerces negative zero floating-point values to positive zero after rounding to the format precision.

The `#` option (only valid for integer, float, and complex types) causes the _alternate form_ to be used for the conversion. The alternate form is differently defined for different types.

`width` is a decimal integer that defines the minimum total field width. It includes any prefixes, separators, and other formatting characters. When no explicit alignment is given, preceding the `width` by a `0` enables sign-aware zero-padding for numeric types.

The `_` grouping option signals the use of an underscore for a thousands separator for floating point presentation types and for integer presentation type `d`. For integer presentation types `b`, `o`, `x`, and `X`, underscores are inserted every four digits.

The `,` grouping option signals the use of a comma for a thousands separator.

`precision` is a decimal integer indicating how many digits should be displayed after the decimal point for presentation types `f` and `F`, or before and after the decimal point for presentation types `g` or `G`. For string presentation types, the field indicates the maximum field size (e.g., how many characters will be used from the field content).

`type` determines how the data should be presented.

<table>
  <caption>String Presentation Types</caption>
  <thead>
    <tr>
      <th>Type</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>s</code></td>
      <td>String format (default type for strings)</td>
    </tr>
    <tr>
      <td><code>None</code></td>
      <td>Same as <code>s</code></td>
    </tr>
  </tbody>
</table>

<table>
  <caption>Integer Presentation Types</caption>
  <thead>
    <tr>
      <th>Type</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>b</code></td>
      <td>Binary format (outputs numbers in base 2)</td>
    </tr>
    <tr>
      <td><code>c</code></td>
      <td>Character (converts integer to corresponding unicode character before printing)</td>
    </tr>
    <tr>
      <td><code>d</code></td>
      <td>Decimal integer (outputs numbers in base 10)</td>
    </tr>
    <tr>
      <td><code>o</code></td>
      <td>Octal format (outputs numbers in base 8)</td>
    </tr>
    <tr>
      <td><code>x</code></td>
      <td>Hex format (outputs numbers in base 16, using lower-case letter for digits above <code>9</code>)</td>
    </tr>
    <tr>
      <td><code>X</code></td>
      <td>Hex format (outputs numbers in base 16, using upper-case letters for the digits above <code>9</code>)</td>
    </tr>
    <tr>
      <td><code>n</code></td>
      <td>Number (same as <code>d</code>, except that it uses the current locale setting to insert the appropriate number separator characters)</td>
    </tr>
    <tr>
      <td><code>None</code></td>
      <td>Same as <code>d</code></td>
    </tr>
  </tbody>
</table>

<table>
  <caption>Float and Decimal Presentation Types</caption>
  <thead>
    <tr>
      <th>Type</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>e</code></td>
      <td>Scientific notation</td>
    </tr>
    <tr>
      <td><code>E</code></td>
      <td>Scientific notation (same as <code>e</code>, except that it uses an upper case <code>E</code> as the separator character)</td>
    </tr>
    <tr>
      <td><code>f</code></td>
      <td>Fixed-point notation</td>
    </tr>
    <tr>
      <td><code>F</code></td>
      <td>Fixed-point notation (same as <code>f</code>, but converts <code>nan</code> to <code>NAN</code>, and <code>inf</code> to <code>INF</code>)</td>
    </tr>
    <tr>
      <td><code>g</code></td>
      <td>General format</td>
    </tr>
    <tr>
      <td><code>G</code></td>
      <td>General format (same as <code>g</code>, except that it switches to <code>E</code> if the number gets too large)</td>
    </tr>
    <tr>
      <td><code>n</code></td>
      <td>Number (same as <code>g</code>, except that it uses the current locale setting to insert the appropriate number separator characters)</td>
    </tr>
    <tr>
      <td><code>%</code></td>
      <td>Percentage</td>
    </tr>
    <tr>
      <td><code>None</code></td>
      <td>Match the output of <code>str()</code> as altered by the other format modifiers</td>
    </tr>
  </tbody>
</table>

The above presentation types (except for `n` and `None`) can also be used to format integers (`float()` converts the integer to a floating point number before formatting).

#### Binary

The core built-in types for manipulating binary Data are [`bytes`](https://docs.python.org/library/stdtypes.html#bytes) and [`bytearray`](https://docs.python.org/library/stdtypes.html#bytearray). These types are supported by [`memoryview`](https://docs.python.org/library/stdtypes.html#memoryview), which uses the [buffer protocol](https://docs.python.org/c-api/buffer.html#bufferobjects) to access the memory of other binary objects without needing to make a copy.

`bytes` objects are immutable sequences of single bytes. Many binary protocols are based on the ASCII text encoding, so `bytes` objects offer methods that are only valid when working with ASCII compatible data.

The syntax for `bytes` literals is largely the same as that for string literals, except that a `b` prefix is added:

- Single quotes: `b'allows embedded "double" quotes'`
- Double quotes: `b"allows embedded 'single' quotes"`
- Triple quotes: `b'''three single quotes'''`, `b"""three double quotes"""`

`bytes` objects can be created in several other ways using the `bytes()` built-in:

- A zero-filled `bytes` object of a specified length: `bytes(ex_len)`
- From an iterable of integers: `bytes(range(ex_len))`
- Copying existing binary data via the buffer protocol: `bytes(ex_obj)`

The `r` prefix disables most escape sequence processing.

Since `bytes` objects are sequences of integers, for a `bytes` object `b`, `b[0]` yields an integer, while `b[0:1]` yields a `bytes` object of length `1`. This contrasts with text strings, where both indexing and slicing produce a string of length `1`.

`bytearray` objects are a mutable counterpart to `bytes` objects.

There is no literal syntax for `bytearray` objects. They are always created by calling the `bytearray()` constructor:

- Creating an empty instance: `bytearray()`
- Creating a zero-filled instance with a given length: `bytearray(ex_len)`
- From an iterable of integers: `bytearray(range(ex_len))`
- Copying existing binary data via the buffer protocol: `bytearray(b'ex_str')`

Since `bytearray` objects are sequences of integers, for a `bytearray` object `b`, `b[0]` yields an integer, while `b[0:1]` yields a `bytearray` object of length `1`. This contrasts with text strings, where both indexing and slicing produce a string of length `1`.

`bytearray` objects use the `bytes` literal format (`bytearray(b'...')`). A `bytearray` object can always be turned into a list of integers (`list(b)`).

Both `bytes` and `bytearray` objects support the common sequence operations. However, they can interoperate not just with operands of the same type, but with any [bytes-like object](https://docs.python.org/glossary.html#term-bytes-like-object). The return type of the result may depend on the order of the operands.

[`bytes` and `bytearray` Operations/Methods](https://docs.python.org/library/stdtypes.html#bytes-and-bytearray-operations)

[`memoryview`](https://docs.python.org/library/stdtypes.html#memoryview) objects allow Python code to access the internal data of an object that supports the buffer protocol without copying.

A `memoryview` has the notion of an _element_, which is the atomic memory unit handled by the originating `object`. For `bytes` and `bytearray`, an element is a single byte.

`len(view)` is equivalent to the length of `view.tolist()`. If `view.ndim = 0`, the length is `1`. If `view.ndim = 1`, the length is equal to the number of elements in the view. For higher dimensions, the length is equal to the length of the nested list representation of the view. The `.itemsize` attribute provides the number of bytes in a single element.

`memoryview` objects support slicing and indexing to expose their data. A one-dimensional slice results in a subview.

If [`view.format`](https://docs.python.org/library/stdtypes.html#memoryview.format) is a native format specifier from the [`struct`](https://docs.python.org/library/struct.html#module-struct) module, indexing with an integer (or tuple of integers) is supported and returns a single element with the correct type.

One-dimensional memoryviews can be indexed with an integer or a one-integer tuple. Multi-dimensional memoryviews can be indexed with tuples of exactly _ndim_ integers, where _ndim_ is the number of dimensions.

Zero-dimensional memoryviews can be indexed with the empty tuple.

If the underlying object is writable, `memoryview` supports one-dimensional slice assignment (resizing not allowed).

One-dimensional `memoryview` objects of hashable (read-only) types with formats `B`, `b`, or `c` are hashable. The hash is defined as `hash(m) == hash(m.tobytes())`.

[`memoryview` Methods](https://docs.python.org/library/stdtypes.html#memoryview.__eq__)

#### Lists

[Lists](https://docs.python.org/library/stdtypes.html#list) are mutable sequences that are usually used to store collections of homogenous items.

Lists can be constructed in several ways:

- Using a pair of square brackets to denote an empty list: `[]`
- Using square brackets, separating items with commas: `[x]`, `[x, y, z]`
- Using a list comprehension: `[x for x in ex_iter]`
- Using the `list()` constructor: `list()` or `list(ex_iter)`

Many other operations produce lists, including [`sorted()`](https://docs.python.org/library/functions.html#sorted).

Lists implement all of the common and mutable sequence operations. They also provide the [`.sort()`](https://docs.python.org/library/stdtypes.html#list.sort) method.

#### Tuples

[Tuples](https://docs.python.org/library/stdtypes.html#tuple) are immutable sequences that are usually used to store collections of heterogenous data. They are also used when there is a need for an immutable sequence of homogeneous data (e.g., allowing storage in a `dict`).

Tuples can be constructed in several ways:

- Using a pair of parentheses to denote an empty tuple: `()`
- Using a trailing comma for a singleton tuple: `x,`, `(x,)`
- Separating items with commas: `x, y, z` or `(x, y, z)`
- Using the `tuple()` constructor: `tuple()` or `tuple(ex_iter)`

Note that it is the **comma** that makes the tuple, _not_ the parentheses. The parentheses are optional, except in the empty tuple case, or when they are needed to avoid syntactic ambiguity.

Tuples implement all common sequence operations.

For heterogenous collections of data where access by name is preferable to access by index, [`collections.namedtuple()`](https://docs.python.org/library/collections.html#collections.namedtuple) may be more appropriate.

#### Ranges

The [`range`](https://docs.python.org/library/stdtypes.html#range) type represents an immutable sequence of numbers and is usually used for looping a specific number of times in `for` loops.

Ranges implement all of the common sequence operations, except for concatenation and repetition. `range` objects can only represent sequences that follow a strict pattern, and concatenation/repetition usually violate that pattern.

A `range` is preferable to a `list` (or `tuple`) for its purpose because it always takes the same (small) amount of memory, regardless of the size of the range it represents.

`range` objects implement the [`collections.abc.Sequence`](https://docs.python.org/library/collections.abc.html#collections.abc.Sequence) ABC, and provide features like:

- Containment tests
- Element index lookup
- Slicing
- Support for negative indices

### Set

A `set` object is an unordered collection of _distinct hashable objects_. These objects are commonly used for:

- Membership testing
- Duplicate removal from sequences
- Computing [mathematical operations](https://en.wikipedia.org/wiki/Set_theory) (e.g., [intersection](https://en.wikipedia.org/wiki/Intersection_(set_theory)), [union](https://en.wikipedia.org/wiki/Union_(set_theory)), [difference](https://en.wikipedia.org/wiki/Complement_(set_theory)#Relative_complement))

Like other collections, `set` objects support `x in set`, `len(set)`, and `for x in set`. Since sets are unordered, they do not record element position or order of insertion. Therefore, they do not support indexing, slicing, or other sequence-like behavior.

There are two built-in set types:

1. [`set`](https://docs.python.org/library/stdtypes.html#set) - Mutable. Has no hash value and cannot be used as either a dictionary key or as an element of another set.
2. [`frozenset`](https://docs.python.org/library/stdtypes.html#frozenset) - Immutable and hashable. Can be used as a dictionary key or as an element of another set.

A non-empty `set` can be created by placing a comma-separated list of elements within braces (`{x, y}`). Also, you can use the `set()` constructor.

```pycon
>>> {'a', 'c', 't', 'g', 'a', 't'}
{'c', 't', 'g', 'a'}
>>> set(['a', 'c', 't', 'g', 'a', 't'])
{'g', 't', 'c', 'a'}
```

When a set is defined with curly braces, each element becomes its own distinct element in the set, even if it is an iterable. The argument to `set()` is an iterable.

```pycon
>>> {'actgat'}
{'actgat'}
>>> set('actgat')
{'g', 't', 'c', 'a'}
>>> 
```

The `set()` and `frozenset()` constructors work the same.

`set()` and `frozenset()` instances provide the following operations:

<table>
  <caption><code>set</code> and <code>frozenset</code> Operations</caption>
  <thead>
    <tr>
      <th>Operation</th>
      <th>Result</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>len(s)</code></td>
      <td>Return the number of elements in set <code>s</code></td>
    </tr>
    <tr>
      <td><code>x in s</code></td>
      <td>Test <code>x</code> for membership in <code>s</code></td>
    </tr>
    <tr>
      <td><code>x not in s</code></td>
      <td>Test <code>x</code> for non-membership in <code>s</code></td>
    </tr>
    <tr>
      <td><code>isdisjoint(other)</code></td>
      <td>Return <code>True</code> if the set has no elements in common with <code>other</code> (sets are disjoint if an only if their intersection is the empty set)</td>
    </tr>
    <tr>
      <td><code>issubset(other)</code><br /><code> set <= other</code></td>
      <td>Test whether every element in the set is in <code>other</code></td>
    </tr>
    <tr>
      <td><code>set < other</code></td>
      <td>Test whether the set is a proper subset of <code>other</code> (i.e., <code>set <= other and set != other</code>)</td>
    </tr>
    <tr>
      <td><code>issuperset(other)</code><br /><code>set >= other</code></td>
      <td>Test whether every element in <code>other</code> is in the set</td>
    </tr>
    <tr>
      <td><code>set > other</code></td>
      <td>Test whether the set is a proper superset of <code>other</code> (i.e., <code>set >= other and set != other</code>)</td>
    </tr>
    <tr>
      <td><code>union(*others)</code><br /><code> set | other | ...</code></td>
      <td>Return a new set with elements from the set and all others</td>
    </tr>
    <tr>
      <td><code>intersection(*others)</code><br /><code> set & other & ...</code></td>
      <td>Return a new set with elements common to the set and all others</td>
    </tr>
    <tr>
      <td><code>difference(*others)</code><br /><code>set - other - ...</code></td>
      <td>Return a new set with elements in the set that are not in the others</td>
    </tr>
    <tr>
      <td><code>symmetric_difference(other)</code><br /><code>set ^ other</code></td>
      <td>Return a new set with elements in either the set or <code>other</code>, but not both</td>
    </tr>
    <tr>
      <td><code>copy()</code></td>
      <td>Return a shallow copy of the set</td>
    </tr>
  </tbody>
</table>

The [`.union()`](https://docs.python.org/library/stdtypes.html#frozenset.union), [`.intersection()`](https://docs.python.org/library/stdtypes.html#frozenset.intersection), [`.difference()`](https://docs.python.org/library/stdtypes.html#frozenset.difference), [`.symmetric_difference()`](https://docs.python.org/library/stdtypes.html#frozenset.symmetric_difference), [`.issubset()`](https://docs.python.org/library/stdtypes.html#frozenset.issubset), and [`.issuperset()`](https://docs.python.org/library/stdtypes.html#frozenset.issuperset) methods accept any iterable as an argument. Their operator-based counterparts require their arguments to be sets.

`set()` and `frozenset()` support set-to-set comparisons. Two sets are only equal if every element of each set is contained in the other (i.e., each set is a subset of the other). A set is less than another set only if the first set is a proper subset of the second set (a nonequal subset). A set is greater than another set only if the first set is a proper superset of the second set (a nonequal superset).

Instances of `set` are compared to instances of `frozenset` based on their _members_.

The subset and equality comparisons do not generalize to a total ordering function, e.g., any two non-empty [disjoint sets](https://en.wikipedia.org/wiki/Disjoint_sets) are not equal and are not subsets of each other.

Like dictionary keys, set elements must be hashable.

Binary operations that mix `set` instances with `frozenset` return the type of the first operand.

The following operations are available for `set` instances, but do not apply to `frozenset` instances:

<table>
  <caption><code>set</code> Operations</caption>
  <thead>
    <tr>
      <th>Operation</th>
      <th>Result</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>update(*others)</code><br /><code>set |= other | ...</code></td>
      <td>Update the set, adding elements from all others</td>
    </tr>
    <tr>
      <td><code>intersection_update(*others)</code><br /><code>set &= other & ...</code></td>
      <td>Update the set, keeping only elements found in it and all others</td>
    </tr>
    <tr>
      <td><code>difference_update(*others)</code><br /><code>set -= other | ...</code></td>
      <td>Update the set, removing elements found in others</td>
    </tr>
    <tr>
      <td><code>symmetric_difference_update(other)</code><br /><code>set ^= other</code></td>
      <td>Update the set, keeping only elements found in either set, but not in both</td>
    </tr>
    <tr>
      <td><code>add(elem)</code></td>
      <td>Add element <code>elem</code> to the set</td>
    </tr>
    <tr>
      <td><code>remove(elem)</code></td>
      <td>Remove element <code>elem</code> from the set (raises <code>KeyError</code> if <code>elem</code> is not contained in the set)</td>
    </tr>
    <tr>
      <td><code>discard(elem)</code></td>
      <td>Remove element <code>elem</code> from the set if it is present</td>
    </tr>
    <tr>
      <td><code>pop()</code></td>
      <td>Remove and return an arbitrary element from the set (raises <code>KeyError</code> if the set is empty)</td>
    </tr>
    <tr>
      <td><code>clear()</code></td>
      <td>Remove all elements from the set</td>
    </tr>
  </tbody>
</table>

The [`.update()`](https://docs.python.org/library/stdtypes.html#frozenset.update), [`.intersection_update()`](https://docs.python.org/library/stdtypes.html#frozenset.intersection_update), [`.difference_update()`](https://docs.python.org/library/stdtypes.html#frozenset.difference_update), and [`.symmetric_difference_update()`](https://docs.python.org/library/stdtypes.html#frozenset.symmetric_difference_update) methods accept any iterable as an argument.

The `elem` argument to the `.__contains__()`, [`.remove()`](https://docs.python.org/library/stdtypes.html#frozenset.remove), and [`.discard()`](https://docs.python.org/library/stdtypes.html#frozenset.discard) methods may be a set.

### Mapping

A [mapping](https://docs.python.org/glossary.html#term-mapping) object maps hashable values to arbitrary objects. Mappings are mutable objects.

The _dictionary_ is Python's mapping object. Any arbitrary, hashable value can be used as a dictionary key. Values that compare equal (e.g., `1`, `1.0`, `True`) can be interchangeably used to index the same dictionary entry.

Dictionaries can be created in several ways:

- Use a comma-separated list of `key: value` pairs within braces: `{'one': 1, 'two': 2, 'three': 3}` or `{1: 'one', 2: 'two', 3: 'three'}`
- Use a dictionary comprehension: `{}`, `{x: x ** 3 for x in range(5)}`
- Use the `dict()` constructor: `dict()`, `dict([('one', 1), ('two', 2)])`, `dict(one=1, two=2)`

If no positional argument is given, an empty dictionary is created. If a positional argument is given and it is a mapping object, a dictionary is created with the same key/value pairs as the mapping object.

Otherwise, the positional argument must be an iterable object. Each item in the iterable must itself be an iterable object with two objects, the first of which becomes a key in the new dictionary, and the second of which becomes the corresponding value. If a key occurs more than once, the last value for that key becomes the corresponding value in the new dictionary.

If keyword arguments are also provided (e.g., `dict([('one', 1)], two=2)`), the arguments and their values are added to the dictionary created from the positional argument. If a key being added is already present, the value from the keyword argument replaces the value from the positional argument.

When providing key/value pairs using keyword arguments, the keys must be _valid Python identifiers_. Otherwise, any valid keys can be used.

<table>
  <caption>Mapping Operations</caption>
  <thead>
    <tr>
      <th>Operation</th>
      <th>Result</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>list(d)</code></td>
      <td>Return a list of all the keys used in the dictionary <code>d</code></td>
    </tr>
    <tr>
      <td><code>len(d)</code></td>
      <td>Return the number of items in the dictionary <code>d</code></td>
    </tr>
    <tr>
      <td><code>d[key]</code></td>
      <td>Return the item of <code>d</code> with key <code>key</code> (raises a <code>KeyError</code> if <code>key</code> is not in the map)</td>
    </tr>
    <tr>
      <td><code>d[key] = value</code></td>
      <td>Set <code>d[key]</code> to <code>value</code></td>
    </tr>
    <tr>
      <td><code>del d[key]</code></td>
      <td>Remove <code>d[key]</code> from <code>d</code> (raises a <code>KeyError</code> if <code>key</code> is not in the map)</td>
    </tr>
    <tr>
      <td><code>key in d</code></td>
      <td>Return <code>True</code> if <code>d</code> has a key <code>key</code>, else <code>False</code></td>
    </tr>
    <tr>
      <td><code>key not in d</code></td>
      <td>Equivalent to <code>not key in d</code></td>
    </tr>
    <tr>
      <td><code>iter(d)</code></td>
      <td>Return an iterator over the keys of the dictionary (equivalent to <code>iter(d.keys())</code>)</td>
    </tr>
    <tr>
      <td><code>clear()</code></td>
      <td>Remove all items from the dictionary</td>
    </tr>
    <tr>
      <td><code>copy()</code></td>
      <td>Return a shallow copy of the dictionary</td>
    </tr>
    <tr>
      <td><code>dict.fromkeys()</code></td>
      <td>Create a new dictionary with keys from <code>iterable</code> and values set to <code>value</code></td>
    </tr>
    <tr>
      <td><code>get(key[, default])</code></td>
      <td>Return the value for <code>key</code> if <code>key</code> is in the dictionary, else <code>default</code> (if <code>default</code> is not given, default to <code>None</code>)</td>
    </tr>
    <tr>
      <td><code>items()</code></td>
      <td>Return a new view of the dictionary's items (<code>(key, value)</code> pairs)</td>
    </tr>
    <tr>
      <td><code>keys()</code></td>
      <td>Return a new view of the dictionary's keys</td>
    </tr>
    <tr>
      <td><code>pop(key[, default])</code></td>
      <td>If <code>key</code> is in the dictionary, remove it and return its value, else return <code>default</code> (if <code>default</code> is not given and <code>key</code> is not in the dictionary, a <code>KeyError</code> is raised)</td>
    </tr>
    <tr>
      <td><code>popitem()</code></td>
      <td>Remove and return a <code>(key, value)</code> pair from the dictionary (pairs are returned in LIFO order)</td>
    </tr>
    <tr>
      <td><code>reversed(d)</code></td>
      <td>Return a reverse iterator over the keys of the dictionary (equivalent to <code>reversed(d.keys())</code>)</td>
    </tr>
    <tr>
      <td><code>setdefault(key[, default])</code></td>
      <td>If <code>key</code> is in the dictionary, return its value, else insert <code>key</code> with a value of <code>default</code> and return <code>default</code> (<code>default</code> defaults to <code>None</code>)</td>
    </tr>
    <tr>
      <td><code>update([other])</code></td>
      <td>Update the dictionary with the key/value pairs from <code>other</code>, overwriting existing keys, and return <code>None</code></td>
    </tr>
    <tr>
      <td><code>values()</code></td>
      <td>Return a new view of the dictionary's values</td>
    </tr>
    <tr>
      <td><code>d | other</code></td>
      <td>Create a new dictionary with the merged keys and values of <code>d</code> and <code>other</code>, which must both be dictionaries (the values of <code>other</code> take priority when <code>d</code> and <code>other</code> share keys)</td>
    </tr>
    <tr>
      <td><code>d |= other</code></td>
      <td>Update the dictionary <code>d</code> with keys and values from <code>other</code>, which may be either a mapping or an iterable of key/value pairs (the values of <code>other</code> take priority when <code>d</code> and <code>other</code> share keys)</td>
    </tr>
  </tbody>
</table>

Dictionaries only compare equal if they have the same `(key, value)` pairs _regardless of ordering_. Order comparisons raise a `TypeError`.

Dictionaries preserve insertion order. Updating a key does not affect the order. Keys added after deletion are inserted at the end.

Dictionaries and dictionary views are reversible.

Objects returned by [`dict.keys()`](https://docs.python.org/library/stdtypes.html#dict.keys), [`dict.values()`](https://docs.python.org/library/stdtypes.html#dict.values), and [`dict.items()`](https://docs.python.org/library/stdtypes.html#dict.items) are _view objects_. They provide a dynamic view on the dictionary's entires, i.e., when the dictionary changes, the view reflects the changes.

If order is important for your output, you can sort the views of key-value pairs, keys, or values that are returned using the `sorted()` function.

Dictionary views can be iterated over to display their data and support membership tests:

<table>
  <caption>Dictionary View Operations</caption>
  <thead>
    <tr>
      <th>Operation</th>
      <th>Result</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>len(dictview)</code></td>
      <td>Return the number of entries in the dictionary</td>
    </tr>
    <tr>
      <td><code>iter(dictview)</code></td>
      <td>Return an iterator over the keys, values, or items (represented as tuples of <code>(key, value)</code>) in the dictionary (keys/values are iterated over in insertion order)</td>
    </tr>
    <tr>
      <td><code>x in dictview</code></td>
      <td>Return <code>True</code> if <code>x</code> is in the underlying dictionary's keys, values, or items (for the latter case, <code>x</code> should be a <code>(key, value)</code> tuple)</td>
    </tr>
    <tr>
      <td><code>reversed(dictview)</code></td>
      <td>Return a reverse iterator over the keys, values, or items of the dictionary (the view will be iterated in reverse order of the insertion)</td>
    </tr>
    <tr>
      <td><code>dictview.mapping</code></td>
      <td>Return a <a href="https://docs.python.org/library/types.html#types.MappingProxyType"><code>types.MappingProxyType</code></a> that wraps the original dictionary to which the view refers</td>
    </tr>
  </tbody>
</table>

Key views are like sets in that their entries are unique and hashable. If all _values_ are hashable, so that `(key, value)` pairs are unique and hashable, then the items view is also set-like. For set-like views, all operations defined for the ABC [`collections.abc.Set`](https://docs.python.org/library/collections.abc.html#collections.abc.Set) are available.

```python
>>> russell_banks = {
...     'continental_drift': 1985,
...     'affliction': 1989,
...     'foregone': 2021,
...     'cloudsplitter': 1998
... }
>>> keys = russell_banks.keys()
>>> values = russell_banks.values()
>>> 
>>> # Iteration
>>> for novel in keys:
...     print(novel)
... 
continental_drift
affliction
foregone
cloudsplitter
>>> list(values)
[1985, 1989, 2021, 1998]
>>> 
>>> 
>>> # Set operations
>>> keys & {'continental_drift', 'the_new_world','cloudsplitter'}
{'cloudsplitter', 'continental_drift'}
>>> keys ^ {'foregone', 'snow'}
{'cloudsplitter', 'affliction', 'snow', 'continental_drift'}
>>> 
>>> # Obtain a read-only proxy for the original dictionary
>>> values.mapping
mappingproxy({'continental_drift': 1985, 'affliction': 1989, 'foregone': 2021, 'cloudsplitter': 1998})
>>> 
```

#### Nesting

You can nest a set of dictionaries inside of a list, a list of items inside of a dictionary, or a dictionary inside of another dictionary.

It is common to store a number of dictionaries in a list when each dictionary contains many kinds of information about one object. In this case, all of the dictionaries in the list should have an identical structure, so that you can loop through the list and work with each dictionary object in the same way.

You can nest a list inside of a dictionary any time that you want more than one value associated with a single key in a dictionary.

When you want more than one value associated with a single key in a dictionary, you can nest a list inside of a dictionary.

If you need an object that contains objects with multiple attributes, you can use an anonymous dictionary (i.e., a dictionary with no name associated with it) inside of a dictionary. It is best to make sure that each internal dictionary has the same structure.

### Module

[Module](https://docs.python.org/glossary.html?highlight=glossary#term-module) attributes can be assigned to.

Supports `m.name`, where `m` is a module and `name` accesses a name defined in `m`'s symbol table.

`m.__dict__` contains the module's symbol table. Modifying this dictionary changes the module's symbol table, but direct assignment is not possible (i.e., `m.__dict__['a'] = 1` is possible, while `m.__dict__ = {}` is not). Directly modifying `.__dict__` is not recommended.

Modules built into the interpreter are written as:

`<module 'sys' (built-in)>`

Modules loaded from files are written as:

`<module 'os' from '/usr/local/lib/pythonX.Y/os.pyc'>`

### Class/Class Instance

[Classes](https://docs.python.org/glossary.html?highlight=glossary#term-class) are callable and act as creators of new instances of themselves. Arguments of the call are passed to [`.__new__()`](https://docs.python.org/reference/datamodel.html#object.__new__) and (usually) to [`.__init__()`](https://docs.python.org/reference/datamodel.html#object.__init__) to initialize a new instance.

Class instances are instances of arbitrary classes and can be made callable by defining a [`.__call__()`](https://docs.python.org/reference/datamodel.html#object.__call__) method in their class.

- [Objects, Values, and Types](https://docs.python.org/reference/datamodel.html#objects-values-and-types)
- [Class Definitions](https://docs.python.org/reference/compound_stmts.html#class)

### Function

[Function](https://docs.python.org/reference/compound_stmts.html#function) objects are created by function definitions. Functions are callable:

`ex_func(ex_args)`

There are two types of functions:

1. Built-in functions
2. User-defined functions

Both are callable, but the implementation differs, which is why they have different object types.

```pycon
>>> def ex_func():
...     pass
... 
>>> type(abs)
<class 'builtin_function_or_method'>
>>> type (ex_func)
<class 'function'>
>>> 
```

A subset of helpful built-in functions are:

- [`abs(x)`](https://docs.python.org/library/functions.html#abs) - Return the absolute value of a number.
- [`all(iterable)`](https://docs.python.org/library/functions.html#all) - Return `True` if all elements of `iterable` are true (or if `iterable` is empty).
- [` any(iterable)`](https://docs.python.org/library/functions.html#any) - Return `True` if any element of `iterable` is true. If `iterable` is empty, return `False`.
- [`ascii(object)`](https://docs.python.org/library/functions.html#ascii) - As `repr()`, return a string containing a printable representation of an object, but escape the non-ASCII characters in the string returned by `repr()` using `\x`, `\u`, or `\U` escapes.
- [`class bool(x=False)`](https://docs.python.org/library/functions.html#bool) - Return a Boolean value, i.e. one of `True` or `False`.
- [`breakpoint(*args, **kwargs)`](https://docs.python.org/library/functions.html#breakpoint) - This function drops you into the debugger at the call site.
- [`callable(object)`](https://docs.python.org/library/functions.html#callable) - Return `True` if the object argument appears callable, `False` if not.
- [`chr(i)`](https://docs.python.org/library/functions.html#chr) - Return the string representing a character whose Unicode code point is the integer `i`.
- [`delattr(object, name)`](https://docs.python.org/library/functions.html#delattr) - The function deletes the named attribute (provided as a string), provided the object allows it.
- [`class dict(**kwarg)`, `class dict(mapping, **kwarg)`, `class dict(iterable, **kwarg)`](https://docs.python.org/library/stdtypes.html#dict) - Create a new dictionary.
- [`dir()`, `dir(object)`](https://docs.python.org/library/functions.html#dir) - Without arguments, return the list of names in the current local scope. With an argument, attempt to return a list of valid attributes for that object.
- [`divmod(a, b)`](https://docs.python.org/library/functions.html#divmod) - Take two (non-complex) numbers as arguments and return a pair of numbers consisting of their quotient and remainder when using integer division. With mixed operand types, the rules for binary arithmetic operators apply.
- [`enumerate(iterable, start=0)`](https://docs.python.org/library/functions.html#enumerate) - Return an enumerate object. The `.__next__()` method of the iterator returned by `enumerate()` returns a tuple containing a count (from `start` which defaults to `0`) and the values obtained from iterating over iterable.
- [`eval(expression, globals=None, locals=None)`](https://docs.python.org/library/functions.html#eval) - The `expression` argument is parsed and evaluated as a Python expression using the globals and locals dictionaries as global and local namespace.
- [`exec(object, globals=None, locals=None, /, *, closure=None)`](https://docs.python.org/library/functions.html#exec) - This function supports dynamic execution of Python code.
- [`filter(function, iterable)`](https://docs.python.org/library/functions.html#filter) - Construct an iterator from those elements of `iterable` for which `function` returns `True`.
- [`class float(x=0.0)`](https://docs.python.org/library/functions.html#float) - Return a floating point number constructed from a number or string `x`.
- [` class frozenset(iterable=set())`](https://docs.python.org/library/stdtypes.html#frozenset) - Return a new `frozenset` object, optionally with elements taken from `iterable`.
- [`getattr(object, name)`, `getattr(object, name, default)`](https://docs.python.org/library/functions.html#getattr) - Return the value of the named attribute of `object` (where `name` is a string).  If the named attribute does not exist, `default` is returned if provided, otherwise `AttributeError` is raised.
- [`globals()`](https://docs.python.org/library/functions.html#globals) - Return the dictionary implementing the current module namespace.
- [`hasattr(object, name)`](https://docs.python.org/library/functions.html#hasattr) - The result is `True` if the `name` string is the name of one of the object’s attributes, `False` if not.
- [`hash(object)`](https://docs.python.org/library/functions.html#hash) - Return the hash value of `object` (if it has one).
- [`help()`, `help(request)`](https://docs.python.org/library/functions.html#help) - Invoke the built-in help system.

     If no argument is given, the interactive help system starts on the interpreter console. If the argument is a string, then the string is looked up as the name of a module, function, class, method, keyword, or documentation topic, and a help page is printed on the console. If the argument is any other kind of object, a help page on the object is generated.

- [`id(object)`](https://docs.python.org/library/functions.html#id) - Return the _identity_ of an object, i.e., an integer that is guaranteed to be unique and constant for `object` during its lifetime.
- [`input()`, `input(prompt)`](https://docs.python.org/library/functions.html#input) - If `prompt` is present, the function writes it to the standard output without a trailing newline. Then, it reads a line from input, converts it to a string (stripping a trailing newline), and returns that. When EOF is read, an `EOFError` is raised.
- [`class int(x=0)`, `class int(x, base=10)`](https://docs.python.org/library/functions.html#int) - Return an integer object constructed from a number or string `x`, or return `0` if no arguments are given.
- [`isinstance(object, classinfo)`](https://docs.python.org/library/functions.html#isinstance) - Return `True` if the `object` argument is an instance of the `classinfo` argument, or of a (direct, indirect, or virtual) subclass thereof. If `object` is not an object of the given type, the function always returns `False`.
- [`issubclass(class, classinfo)`](https://docs.python.org/library/functions.html#issubclass) - Return `True` if class is a subclass (direct, indirect, or virtual) of `classinfo`. Otherwise, a `TypeError` exception is raised.
- [`iter(object)`, `iter(object, sentinel)`](https://docs.python.org/library/functions.html#iter) - Return an iterator object.

    Without a second argument, `object` must be a collection object that supports the iterable protocol (the `.__iter__()` method), or it must support the sequence protocol (the `.__getitem__()` method with integer arguments starting at `0`). If it does not support either of those protocols, `TypeError` is raised.
    
    If `sentinel` is given, then `object` must be a callable object. The iterator created in this case will call `object` with no arguments for each call to its `.__next__()` method. If the value returned is equal to `sentinel`, `StopIteration` will be raised, otherwise the value will be returned.

- [`len(s)`](https://docs.python.org/library/functions.html#len) - Return the length (the number of items) of an object.
- [` class list`, `class list(iterable)`](https://docs.python.org/library/stdtypes.html#list) - Create a new list.
- [`locals()`](https://docs.python.org/library/functions.html#locals) - Update and return a dictionary representing the current local symbol table.
- [`map(function, iterable, *iterables)`](https://docs.python.org/library/functions.html#map) - Return an iterator that applies `function` to every item of `iterable`, yielding the results.

     If additional iterables arguments are passed, `function` must take that many arguments and is applied to the items from all iterables in parallel. With multiple iterables, the iterator stops when the shortest iterable is exhausted.

- [`max(iterable, *, key=None)`, `max(iterable, *, default, key=None)`, `max(arg1, arg2, *args, key=None)`](https://docs.python.org/library/functions.html#max) - Return the largest item in an iterable or the largest of two or more arguments.
- [`min(iterable, *, key=None)`, ` min(iterable, *, default, key=None)`, ` min(arg1, arg2, *args, key=None)`](https://docs.python.org/library/functions.html#min) - Return the smallest item in an iterable, or the smallest of two or more arguments.
- [`next(iterator)`, `next(iterator, default)`](https://docs.python.org/library/functions.html#next) - Retrieve the next item from `iterator` by calling its `.__next__()` method. If `default` is given, it is returned if the iterator is exhausted, otherwise `StopIteration` is raised.
- [`class object`](https://docs.python.org/library/functions.html#object) - Return a new featureless object.
- [`open(file, mode='r', buffering=- 1, encoding=None, errors=None, newline=None, closefd=True, opener=None)`](https://docs.python.org/library/functions.html#open) - Open `file` and return a corresponding file object. If `file` cannot be opened, an `OSError` is raised.

    <table>
      <caption>File Open Modes</caption>
      <thead>
        <tr>
            <th>Character</th>
            <th>Meaning</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><code>r</code></td>
          <td>open for reading (default)</td>
        </tr>
        <tr>
          <td><code>w</code></td>
          <td>open for writing, truncating the file first</td>
        </tr>
        <tr>
          <td><code>x</code></td>
          <td>open for exclusive creation, failing if the file already exists</td>
        </tr>
        <tr>
          <td><code>a</code></td>
          <td>open for writing, appending to the end of the file if it exists</td>
        </tr>
        <tr>
          <td><code>b</code></td>
          <td>binary mode</td>
        </tr>
        <tr>
          <td><code>t</code></td>
          <td>text mode (default)</td>
        </tr>
        <tr>
          <td><code>+</code></td>
          <td>open for updating (reading and writing)</td>
        </tr>
      </tbody>
    </table>

- [`ord(c)`](https://docs.python.org/library/functions.html#ord) - Given a string representing one Unicode character, return an integer representing the Unicode code point of that character.
- [`print(*objects, sep=' ', end='\n', file=None, flush=False)`](https://docs.python.org/library/functions.html#print) - Print `objects` to the text stream file, separated by `sep` and followed by `end`. `sep`, `end`, `file`, and `flush`, if present, must be given as keyword arguments.
- [` class range(stop)`, `class range(start, stop, step=1)`](https://docs.python.org/library/stdtypes.html#range) - Create a new range. If `step` is omitted, it defaults to `1`. If `start` is omitted, it defaults to `0`.
- [`repr(object)`](https://docs.python.org/library/functions.html#repr) - Return a string containing a printable representation of an object.
- [`reversed(seq)`](https://docs.python.org/library/functions.html#reversed) - Return a reverse iterator.
- [`round(number, ndigits=None)`](https://docs.python.org/library/functions.html#round) - Return `number` rounded to `ndigits` precision after the decimal point. If `ndigits` is omitted or is `None`, it returns the nearest integer to its input.
- [` class set`, `class set(iterable)`](https://docs.python.org/library/stdtypes.html#set) - Return a new set object, optionally with elements taken from `iterable`.
- [` setattr(object, name, value)`](https://docs.python.org/library/functions.html#setattr) - The function assigns the `value` to the string `name` attribute, provided the `object` allows it.
- [`sorted(iterable, /, *, key=None, reverse=False)`](https://docs.python.org/library/functions.html#sorted) - Return a new sorted list from the items in `iterable`.

    `key` specifies a function of one argument that is used to extract a comparison key from each element in `iterable` (e.g., `key=str.lower`). The default value is `None` (i.e., compare the elements directly).

    `reverse` is a boolean value. If set to `True`, then the list elements are sorted as if each comparison were reversed.

- [` class str(object='')`, `class str(object=b'', encoding='utf-8', errors='strict')`](https://docs.python.org/library/stdtypes.html#str) - Return a string version of `object`.
- [`sum(iterable, /, start=0)`](https://docs.python.org/library/functions.html#sum) - Sums `start` and the items of `iterable` from left to right and returns the total.
- [`class super`, `class super(type, object_or_type=None)`](https://docs.python.org/library/functions.html#super) - Return a proxy object that delegates method calls to a parent or sibling class of `type`. This is useful for accessing inherited methods that have been overridden in a class.

    `object_or_type` determines the method resolution order to be searched. The search starts from the class right after the `type`.

- [`class tuple`, `class tuple(iterable)`](https://docs.python.org/library/stdtypes.html#tuple) - Return a new tuple.
- [`class type(object)`, `class type(name, bases, dict, **kwargs)`](https://docs.python.org/library/functions.html#type) - With one argument, return the type of `object`.

    With three arguments, return a new type object. Keyword arguments are passed to the appropriate metaclass machinery (usually `.__init_subclass__()`) in the same way that keywords in a class definition (besides `metaclass`) would.

- [`vars()`, `vars(object)`](https://docs.python.org/library/functions.html#vars) - Return the `.__dict__` attribute for a module, class, instance, or any other object with a `.__dict__` attribute.

    Without an argument, `vars()` acts like `locals()`.

- [`zip(*iterables, strict=False)`](https://docs.python.org/library/functions.html#zip) - Iterate over several iterables in parallel, producing tuples with an item from each one.

### Method

A [method](https://docs.python.org/glossary.html?highlight=glossary#term-method) is a function that is called using the attribute notation.

There are two types of methods:

1. Built-in methods
2. Class instance methods

If you access a method through an instance, a _bound method (instance method)_ is received. When called, an instance method adds the `self` argument to the argument list.

Bound methods have two special read-only attributes:

1. `m.__self__` - The object on which the method operates.
2. `m.__func__` - The function implementing the method.

Calling `m(arg_1, arg_2, ..., arg_n)` is equivalent to calling `m.__func__(m.__self__, arg_1, arg_2, ..., arg_n)`.

Like a function object, an instance method supports getting arbitrary attributes. However, since method attributes are stored on the underlying function object (`m.__func__`), setting method attributes on instance methods is not allowed.

In order to set a method attribute, you need to explicitly set it on the underlying function object:

```pycon
>>> class C:
...     def m(self):
...             pass
... 
>>> inst = C()
>>> inst.m.n = 'method name'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'method' object has no attribute 'n'
>>> inst.m.__func__.n = 'method name'
>>> inst.m.n
'method name'
>>> 
```

### Type

[Type](https://docs.python.org/glossary.html?highlight=glossary#term-type) objects represent various object types, which are accessed by the built-in [`type()`](https://docs.python.org/library/functions.html#type) function.

The standard module [`types`](https://docs.python.org/library/types.html#module-types) defines names for all standard built-in types.

Types are written like `<class 'str'>`.

### Null

This singleton object, `None`, is returned by functions that do not explicitly return a value.

It is written as `None`.

### Ellipsis

The ellipsis object, [`Ellipsis`](https://docs.python.org/library/constants.html#Ellipsis), is a singleton object that is commonly used by [slicing](https://docs.python.org/reference/expressions.html#slicings).

It is written as `Ellipsis` or `...`.

### Special Attributes

There are a few special read-only attributes for several object types. Some of these attributes are not reported by the [`dir()`](https://docs.python.org/library/functions.html#dir) built-in function.

- `object.__dict__` - A dictionary or other mapping object used to store an object's (writable) attributes.
- `instance.__class__` - The class to which a class instance belongs.
- `class.__bases__` - The tuple of base classes of a class object.
- `definition.__name__` - The name of the class, function, method, descriptor, or generator instance.
- `definition.__qualname__` - The [qualified name](https://docs.python.org/glossary.html#term-qualified-name) of the class, function, method, descriptor, or generator instance.
- `class.__mro__` - A tuple of classes that are considered when looking for base classes during method resolution.
- `class.mro()` - A method that can be overridden by a metaclass to customize the method resolution order for its instances. It is called at class instantiation and its result is stored in `.__mro__`.
- `class.__subclasses__()` - Classes keep a list of weak references to their immediate subclasses. This method returns a list of those references that are still alive.

## Working With Numbers

Division always returns a `float`:

```pycon
>>> 15 / 3
5.0
>>> 
```

Floor division will discard fractional results and return an `int`:

```pycon
>>> 19 / 3
6.333333333333333
>>> 19 // 3
6
>>> 
```

Unassigned variables throw [NameError](https://docs.python.org/library/exceptions.html#NameError) exceptions:

```pycon
>>> d
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'd' is not defined
>>> 
```

`int` operands mixed with `float` operands are automatically converted to `float`:

```pycon
>>> 4 * 3.5
14.0
>>>
```

In interactive mode, the last printed expression is saved to the `_` variable:

```pycon
>>> 29 // 4
7
>>> 2 * _
14
>>> 
```

### Handling Precision Values

There are several ways to handle precision data and decimals in output:

- Python f-string
- Python `round()` function
- `math` module's `ceil()` and `floor()` functions
- `math` module's `trunc()` function

f-strings and the `round()` function [round a value](https://docs.python.org/library/functions.html#round) to the closest multiple of `10` to the power minus the `ndigits` (number of digits) precision that you specify. Keep in mind, this [may not always produce](https://docs.python.org/tutorial/floatingpoint.html#tut-fp-issues) the exact result you are expecting:

```pycon
>>> num = 35.87688431866441
>>> f'{num:.2f}'
'35.88'
>>> round(num, 2)
35.88
>>> f'{num:.0f}'
'36'
>>> round(num)
36
>>> from math import ceil, floor, trunc
>>> ceil(num)  # Round up to the nearest integer
36
>>> floor(num)  # Round down to the nearest integer
35
>>> trunc(num)  # Remove all digits after a decimal point
35
>>> 
```

## Working With Strings

In interactive mode, output strings are enclosed in quotes and special characters are escaped with backslashes:

```pycon
>>> '"I can\'t believe what you say, because I see what you do."\n\n- James Baldwin'
'"I can\'t believe what you say, because I see what you do."\n\n- James Baldwin'
>>> 
```

The [print()](https://docs.python.org/library/functions.html#print) function produces more readable output by omitting enclosing quotes and processing escaped/special characters:

```pycon
>>> print('"Fires can\'t be made with dead embers, nor can enthusiasm be stirred by spiritless men."\n\n- James Baldwin')
"Fires can't be made with dead embers, nor can enthusiasm be stirred by spiritless men."

- James Baldwin
>>> 
```

Interpretation of escaped/special characters can be disabled by using _raw strings_, delineated by an `r` before the string:

```pycon
>>> print('"History is not a procession of illustrious people. It\'s about what happens to a people. Millions of anonymous people is what history is about."\n\n- James Baldwin')
"History is not a procession of illustrious people. It's about what happens to a people. Millions of anonymous people is what history is about."

- James Baldwin
>>> print(r'"History is not a procession of illustrious people. It\'s about what happens to a people. Millions of anonymous people is what history is about."\n\n- James Baldwin')
"History is not a procession of illustrious people. It\'s about what happens to a people. Millions of anonymous people is what history is about."\n\n- James Baldwin
>>> 
```

String literals can span multiple lines using triple quotes:

```pycon
>>> print('''\
... A mass of tears have transformed to stones now
... Sharpened on suffering and woven into slings
... Hope lies in the rubble of this rich fortress
... Taking today what tomorrow never brings
... 
... - Brad J. Wilk, Thomas B. Morello, Timothy Commerford, Zack M. De La Rocha\
... ''')
A mass of tears have transformed to stones now
Sharpened on suffering and woven into slings
Hope lies in the rubble of this rich fortress
Taking today what tomorrow never brings

- Brad J. Wilk, Thomas B. Morello, Timothy Commerford, Zack M. De La Rocha
>>> 
```

Triple quotes are also useful when a string contains both single and double quotes:

```pycon
>>> sharma_v_minister = '''\
... "As Australian adults know their country, Australia will be lost and the world
... as we know it gone as well.
... 
... The physical environment will be harsher, far more extreme and devastatingly
... brutal when angry.
... 
... As for the human experience – quality of life opportunities to partake in
... nature's treasures, the capacity to grow and prosper – all will be greatly
... diminished.
... 
... Lives will be cut short. Trauma will be far more common and good health harder
... to hold and maintain.
... 
... None of this will be the fault of nature itself.
... 
... It will largely be inflicted by the inaction of this generation of adults, in
... what might fairly be described as the greatest inter-generational injustice
... ever inflicted by one generation of humans upon the next."
... 
... - Mordecai Bromberg\
... '''
>>> print(sharma_v_minister)
"As Australian adults know their country, Australia will be lost and the world
as we know it gone as well.

The physical environment will be harsher, far more extreme and devastatingly
brutal when angry.

As for the human experience – quality of life opportunities to partake in
nature's treasures, the capacity to grow and prosper – all will be greatly
diminished.

Lives will be cut short. Trauma will be far more common and good health harder
to hold and maintain.

None of this will be the fault of nature itself.

It will largely be inflicted by the inaction of this generation of adults, in
what might fairly be described as the greatest inter-generational injustice
ever inflicted by one generation of humans upon the next."

- Mordecai Bromberg
>>> 
```

Strings can be concatenated with `+` and repeated with `*`:

```pycon
>>> str_1 = 'Hope is not passive. '
>>> str_2 = 'Hope is not ' + ('blah ' * 3).rstrip() + '. '
>>> str_3 = 'Hope is taking action.\n\n- Greta Thunberg'
>>> print(str_1 + str_2 + str_3)
Hope is not passive. Hope is not blah blah blah. Hope is taking action.

- Greta Thunberg
```

Adjacent _string literals_ are automatically concatenated:

```pycon
>>> shevek = (
...     'You do not believe in change, in chance, in evolution.\n'
...     'You would destroy us rather than admit our reality, rather than admit that there is hope!\n'
...     'We cannot come to you. We can only wait for you to come to us.\n\n'
...     '- Ursula K. Le Guin'
... )
>>> print(shevek)
You do not believe in change, in chance, in evolution.
You would destroy us rather than admit our reality, rather than admit that there is hope!
We cannot come to you. We can only wait for you to come to us.

- Ursula K. Le Guin
>>> 
```

Concatenating a string literal and a variable(s) requires the use of `+`:

```pycon
>>> houwha = (
...     'Say nothing to me of innocent bystanders, unearned suffering, heartless vengeance.\n'
...     'When a comm builds atop a fault line, do you blame its walls when they inevitably crush the people inside?\n'
...     'No; you blame whoever was stupid enough to think they could defy the laws of nature forever.\n'
...     'Well, some worlds are built on a fault line of pain, held up by nightmares.\n'
...     "Don't lament when those worlds fall. Rage that they were built doomed in the first place.\n\n"
... )
>>> print(houwha '- N. K. Jemisin')
  File "<stdin>", line 1
    print(houwha '- N. K. Jemisin')
                 ^
SyntaxError: invalid syntax
>>> 
```

```pycon
>>> print(houwha + '- N. K. Jemisin')
Say nothing to me of innocent bystanders, unearned suffering, heartless vengeance.
When a comm builds atop a fault line, do you blame its walls when they inevitably crush the people inside?
No; you blame whoever was stupid enough to think they could defy the laws of nature forever.
Well, some worlds are built on a fault line of pain, held up by nightmares.
Don't lament when those worlds fall. Rage that they were built doomed in the first place.

- N. K. Jemisin
>>> 
```

Strings are _indexed_, i.e., they are a [sequence](https://docs.python.org/glossary.html#term-sequence) of characters:

```pycon
>>> name = 'Ashitaka'
>>> name[0]
'A'
>>> name[7]
'a'
>>> 
```

To count from the right, use negative numbers:

```pycon
>>> name = 'Ashitaka'
>>> name[-1]
'a'
>>> name[-8]
'A'
>>> 
```

Negative indices start at `-1` because `-0` is equivalent to `0`.

Slicing is supported to obtain substrings:

```pycon
>>> name = 'Ashitaka'
>>> name[0:4]
'Ashi'
>>> name[4:8]
'taka'
>>> 
```

An omitted first index defaults to `0`, and an omitted last index defaults to the size of the string being sliced:

```pycon
>>> name = 'Ashitaka'
>>> name[:4]
'Ashi'
>>> name[4:]
'taka'
>>> name[-4:]
'taka'
>>> 
```

The start point is always included in the resulting substring slice, and the end point is always excluded, i.e., `ex_str[:i] + ex_str[i:]` always equals `ex_str`:

```pycon
>>> name = 'Ashitaka'
>>> name[:4] + name[4:]
'Ashitaka'
>>> 
```

Think of indices as pointing _between_ characters, where the left edge of the first character has an index of `0` and the right edge of the last character of a string of `n` characters has an index of `n`:

```pycon
 +---+---+---+---+---+---+---+---+
 | A | s | h | i | t | a | k | a |
 +---+---+---+---+---+---+---+---+
 0   1   2   3   4   5   6   7   8
-8  -7  -6  -5  -4  -3  -2  -1
```

For positive indices, the length of a slice is the difference of the indices, if both are within bounds.

```pycon
>>> nums = '123456'
>>> nums[:]  # Count from left implicit
'123456'
>>> nums[0:6:1]  # Count from left explicit
'123456'
>>> nums[::-1]  # Count from right implicit
'654321'
>>> nums[-1:-7:-1]  # Count from right explicit
'654321'
>>> 
```

Python strings are [immutable](https://docs.python.org/glossary.html#term-immutable):

```pycon
>>> name = 'Ashitaka'
>>> name[0] = 'a'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'str' object does not support item assignment
>>> 
```

When you need a new string, create a new one:

```pycon
>>> name = 'Ashitaka'
>>> 'a' + name[1:]
'ashitaka'
>>> 
```

The [len()](https://docs.python.org/library/functions.html?#len) function returns the length of a string:

```pycon
>>> prescient = 'We lost the war. Welcome to the world of tomorrow.'
>>> len(prescient)
50
>>> 
```

## Working With Lists

_Compound_ data types group values together. A versatile Python compound type is the [list](https://docs.python.org/glossary.html#term-list). Lists can contain different item types, but are usually used to group items of the same type.

```pycon
>>> words = ['apathy', 'greed', 'corruption', 'power', 'hope']
>>> words
['apathy', 'greed', 'corruption', 'power', 'hope']
>>> 
```

A list is also a [sequence](https://docs.python.org/glossary.html#term-sequence) type, which means that it can be indexed and sliced:

```pycon
>>> words = ['apathy', 'greed', 'corruption', 'power', 'hope']
>>> words[0]
'apathy'
>>> words[-1]
'hope'
>>> words[1:3]
['greed', 'corruption']
>>> 
```

List slice operations return a new list with the requested items. The returned list is a [shallow copy](https://docs.python.org/library/copy.html#shallow-vs-deep-copy) of the list:

```pycon
>>> word_sets = [
...     ['apathy', 'greed', 'corruption', 'power', 'hope'],
...     ['courage', 'honor', 'justice']
... ]
>>> word_sets_sc = word_sets[:]
>>> del word_sets_sc[1][1]
>>> word_sets_sc
[['apathy', 'greed', 'corruption', 'power', 'hope'], ['courage', 'justice']]
>>> word_sets
[['apathy', 'greed', 'corruption', 'power', 'hope'], ['courage', 'justice']]
>>> 
```

Lists support concatenation:

```pycon
>>> money = ['PRISM', 'Upstream', 'MUSCULAR']
>>> power = ['Treasure Map', 'MARINA', 'MAINWAY']
>>> control = ['Pinwale', 'NUCLEON', 'XKeyscore', 'ICREACH']
>>> money + power + control
['PRISM', 'Upstream', 'MUSCULAR', 'Treasure Map', 'MARINA', 'MAINWAY', 'Pinwale', 'NUCLEON', 'XKeyscore', 'ICREACH']
>>> 
```

Unlike strings, which are immutable, lists are a [mutable](https://docs.python.org/glossary.html#term-mutable) type:

```pycon
>>> the_new_world = ['Diego Velázquez', 'Hatuey', 'Bartolome']
>>> the_new_world[2] = 'Bartolomé de las Casas'
>>> the_new_world
['Diego Velázquez', 'Hatuey', 'Bartolomé de las Casas']
>>> 
```

New items can be added to the end of a list with the `.append()` method:

```pycon
>>> lyrics = [
...     'Stars when you shine you know how I feel',
...     'Scent of the pine you know how I feel',
...     'Oh freedom is mine'
... ]
>>> lyrics.append('And I know how I feel')
>>> lyrics
['Stars when you shine you know how I feel', 'Scent of the pine you know how I feel', 'Oh freedom is mine', 'And I know how I feel']
>>> 
```

Slices can be assigned to:

```pycon
>>> team = ['adrian', 'laurie', 'edward', 'walter', 'j', 'd']
>>> team
['adrian', 'laurie', 'edward', 'walter', 'j', 'd']
>>> team[4:6] = ['jon', 'daniel']
>>> team
['adrian', 'laurie', 'edward', 'walter', 'jon', 'daniel']
>>> team[2:4] = []
>>> team
['adrian', 'laurie', 'jon', 'daniel']
>>> team[:] = []
>>> team
[]
>>> 
```

The `len()` function applies to lists:

```pycon
>>> who_speaks_for_earth = ['Lapérouse', 'Tlingit', 'Cortés', 'Aztecs']
>>> len(who_speaks_for_earth)
4
>>> 
```

Lists can contain other lists. For example, a 2-dimensional list is essentially a table. The first element in the list can be a row of column names, and the subsequent elements in the list can be the rows in the table.

```pycon
>>> import locale
>>> locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
'en_US.UTF-8'
>>> executive_enrichment = [
...     ['company', 'buyback_share_value'],
...     ['apple', 24700000000],
...     ['alphabet', 15300000000],
...     ['meta', 6600000000],
...     ['microsoft', 5500000000]
... ]
>>> for row in executive_enrichment[1:]:
...     print(f'{row[0].title():>9} Q3 2022 buyback value: {locale.currency(row[1], grouping=True):>18}')
... 
    Apple Q3 2022 buyback value: $24,700,000,000.00
 Alphabet Q3 2022 buyback value: $15,300,000,000.00
     Meta Q3 2022 buyback value:  $6,600,000,000.00
Microsoft Q3 2022 buyback value:  $5,500,000,000.00
>>> 
```

## Control Flow

### Nested Loops

A nested loop is when a loop is inside of another loop.

Any type of loop can be nested inside of any other type of loop.

_For every iteration of an outer loop, the inner loop will run its full range anew._

### `while` Statement

The [while](https://docs.python.org/reference/compound_stmts.html#the-while-statement) statement executes as long as its condition is `True`:

```pycon
>>> step, count = 1, 5
>>> while count > 0:
...     print(count)
...     count -= step
... 
5
4
3
2
1
>>> 
```

`step, count = 1, 5` demonstrates _multiple assignment_, where `step` and `count` are simultaneously associated with the integers `1` and `5`, respectively. Expressions on the right side of `=` are evaluated first, prior to assignment, from left to right.

Any non-zero integer value is `True`. Zero is `False`:

```pycon
>>> bool(3)
True
>>> bool(0)
False
>>> 
```

The condition can be any sequence (e.g., string, list). Any sequence with a non-zero length is `True`. Empty sequences are `False`.

```pycon
>>> bool('Sparrowhawk')
True
>>> bool('')
False
>>> shadows = ['Ged', 'Estraven', 'Shevek']
>>> bool(shadows)
True
>>> shadows.clear()
>>> bool(shadows)
False
>>> 
```

For simple programs, a `while` loop can be used to let a user determine when to quit a program:

```python
prompt = '\nEnter text to repeat (q to quit): '
text = ''

while text != 'q':
    text = input(prompt).strip()

    if text != 'q':
        print(text)
```

For complicated programs that should run when many conditions are true, a _flag_ can be defined (i.e., a variable that determines whether or not the program is active):

```python
prompt = '\nEnter text to repeat (q to quit): '
active = True

while active:
    text = input(prompt).strip()

    if text == 'q':
        active = False
    else:
        print(text)
```

A `while` loop is an effective way to modify a list or dictionary as you work through it.

`while True:` is a common idiom in Python for creating an infinite loop. Often, it is used in a `try`/`except` statement (where an explicit keyword like `break` gets you out of the loop) or in generator functions, where an explicit `break` is not required.

### `if` Statement

The [if](https://docs.python.org/reference/compound_stmts.html#the-if-statement) statement can be used to conditionally execute code depending on multiple conditions:

```pycon
>>> choice = int(input(f'Enter a number in {tuple(range(1, 4))}: '))
Enter a number in (1, 2, 3): 5
>>> if choice == 1:
...     print('One')
... elif choice == 2:
...     print('Two')
... elif choice == 3:
...     print('Three')
... else:
...     print('Chosen number not in tuple.')
... 
Chosen number not in tuple.
>>> 
```

There can be zero or more `elif` clauses and the `else` clause is optional.

### `match` Statement

If you need to compare the same value to several constants, or check for specific types/attributes, consider using the [match](https://docs.python.org/reference/compound_stmts.html#the-match-statement) statement:

```pycon
>>> choice = int(input(f'Enter a number in {tuple(range(1, 5))}: '))
Enter a number in (1, 2, 3, 4): 3
>>> match choice:
...     case 1:
...             print('One')
...     case 2:
...             print('Two')
...     case 3 | 4:
...             print('Three or Four')
...     case _:
...             print('Chosen number not in tuple.')
... 
Three or Four
>>> 
```

Only the first pattern that matches gets executed and it can also extract components from the value into variables (e.g., sequence elements, object attributes). If no case matches, none of the branches are executed.

Multiple literals can be combined into a single pattern using the Bitwise OR operator (`|`).

The `_` variable name acts as a _wildcard_ and never fails to match.

### `for` Statement

Python's [for](https://docs.python.org/reference/compound_stmts.html#the-for-statement) statement iterates over the items of any sequence in the order that they appear in the sequence:

```pycon
>>> internets_own_boy = [
...     'If you look at Steve Jobs and Steve Wozniak, they started by selling a Blue box, which was a thing designed to defraud the phone company.',
...     'If you look at Bill Gates and Paul Allen, they initially started their business by using computer time at Harvard, which was pretty clearly against the rules.',
...     'The difference between Aaron and the people I’ve just mentioned is that Aaron wanted to make the world a better place.',
...     'He didn’t just want to make money.',
...     '\n– Robert Swartz'
... ]
>>> for line in internets_own_boy:
...     print(line)
... 
If you look at Steve Jobs and Steve Wozniak, they started by selling a Blue box, which was a thing designed to defraud the phone company.
If you look at Bill Gates and Paul Allen, they initially started their business by using computer time at Harvard, which was pretty clearly against the rules.
The difference between Aaron and the people I’ve just mentioned is that Aaron wanted to make the world a better place.
He didn’t just want to make money.

– Robert Swartz
>>> 
```

### `range()` Constructor

The [range()](https://docs.python.org/library/stdtypes.html#range) constructor generates an immutable sequence that is its own type. Like all sequences, it is iterable:

```pycon
>>> for num in range(5):
...     print(num)
... 
0
1
2
3
4
>>> type(range(5))
<class 'range'>
>>> 
```

The end point of `range()` is never part of the generated sequence and the generated sequences are the legal indices of the sequence.

A different start point and increment can be chosen for a range:

```pycon
>>> list(range(15, 20))
[15, 16, 17, 18, 19]
>>> list(range(1, 20, 2))
[1, 3, 5, 7, 9, 11, 13, 15, 17, 19]
>>> list(range(-1, -20, -2))
[-1, -3, -5, -7, -9, -11, -13, -15, -17, -19]
>>> 
```

If you need both the value of a sequence and its index, use the [enumerate()](https://docs.python.org/library/functions.html#enumerate) function:

```pycon
>>> seq = ['To', 'create', 'is', 'to', 'live', 'twice.', '- Albert Camus']
>>> for index, value in enumerate(seq):
...     print(index, value)
... 
0 To
1 create
2 is
3 to
4 live
5 twice.
6 - Albert Camus
>>> 
```

Alternatively, you can use `len()` in conjunction with `range()`:

```pycon
>>> seq = ['Nations', 'are', 'not', 'communities', 'and', 'never', 'have', 'been.', '- Howard Zinn']
>>> for index in range(len(seq)):
...     print(index, seq[index])
... 
0 Nations
1 are
2 not
3 communities
4 and
5 never
6 have
7 been.
8 - Howard Zinn
>>> 
```

### `break` and `continue` Statements

The [break](https://docs.python.org/reference/simple_stmts.html#break) statement breaks out of the innermost enclosing `for` or `while` loop.

An optional `else` clause is executed when the loop terminates through exhaustion of the `for` loop iterable, or when the `while` loop condition becomes `False`, but _not_ when the loop is terminated by a `break` statement:

```pycon
>>> target = 'Jon Osterman'
>>> for name in ('adrian', 'edward', 'jon'):
...     for surname in ('osterman', 'veidt', 'blake'):
...             if f'{name} {surname}'.title() == target:
...                     print(f'{target} has been found.')
...                     break
...             else:
...                     print(f'{name} {surname}'.title() + ' is not the target.')
...     else:
...             print('Trying next name...')
... 
Adrian Osterman is not the target.
Adrian Veidt is not the target.
Adrian Blake is not the target.
Trying next name...
Edward Osterman is not the target.
Edward Veidt is not the target.
Edward Blake is not the target.
Trying next name...
Jon Osterman has been found.
>>> 
```

When used with a loop, the `else` clause is similar to an `else` clause used with a [try](https://docs.python.org/reference/compound_stmts.html#try) statement. The `try` statement's `else` clause runs when no exception occurs, and a loop's `else` clause runs when no `break` occurs.

The [continue](https://docs.python.org/reference/simple_stmts.html#continue) statement continues with the next iteration of the loop:

```pycon
>>> names = ('adrian', 'laurie', 'edward', 'walter', 'jon', 'daniel')
>>> targets = ('laurie', 'daniel', 'walter')
>>> for name in names:
...     if name in targets:
...             print(f'Found {name.title()}.')
...             continue
...     print(f'{name.title()} is not a target.')
... 
Adrian is not a target.
Found Laurie.
Edward is not a target.
Found Walter.
Jon is not a target.
Found Daniel.
>>> 
```

### `pass` Statement

The [pass](https://docs.python.org/reference/simple_stmts.html#pass) statement does nothing and is used when a statement is syntactically required, but the program requires no action.

For example, `pass` may be used to wait for a keyboard interrupt signal:

```pycon
>>> while True:
...     pass
... 
^CTraceback (most recent call last):
  File "<stdin>", line 2, in <module>
KeyboardInterrupt
>>> 
```

Also, `pass` can be used to create a minimal class:

```pycon
>>> class MinimalClass:
...     pass
... 
>>> 
```

### Functions

_Functions_ are named blocks of code that are designed to do one specific job. When you want to perform a particular task that you have defined in a function, you _call_ the name of the function responsible for it.

When writing a function, you need to decide if the function needs arguments or not, and if the function needs to return something or not.

The [def](https://docs.python.org/reference/compound_stmts.html#def) keyword introduces a function _definition_. It must be followed by the function name and parenthesized list of function parameters (arguments). Statements that form the body of the function must be indented.

```pycon
>>> def print_num_range(max):
...     '''Print numbers from zero to max.'''
...     for num in range(max):
...             print(num)
... 
>>> print_num_range(5)
0
1
2
3
4
>>> 
```

The execution of a function introduces a new symbol table used for the local variables of the function. Variable assignments in the function are stored in the local symbol table.

Variables references are searched for in the following order:

1. Local symbol table
2. Local symbol tables of enclosing functions
3. Global symbol table
4. Table of built-in names

Global variables and enclosing function variables may be referenced in a function, but they cannot be directly assigned unless the [global](https://docs.python.org/reference/simple_stmts.html#global) or [nonlocal](https://docs.python.org/reference/simple_stmts.html#nonlocal) statements are used, respectively.

The parameters to a function call are added to the local symbol table of the called function when it is called, i.e., arguments are passed using _call by value_, where the _value_ is an object _reference_, **not the value of the object**.

In other words, Python passes arguments neither by reference nor by value, but by *assignment* ([1], [2]). That is, when you call a Python function, each function argument becomes a variable to which the passed value is assigned, and that value is an object reference.

[1]: https://docs.python.org/3/faq/programming.html#how-do-i-write-a-function-with-output-parameters-call-by-reference
[2]: https://realpython.com/python-pass-by-reference/#defining-pass-by-reference

When a function calls another function or recursively calls itself, a new local symbol table is created for that call.

Function definitions associate function names with function objects in the current symbol table, i.e., the Python interpreter recognizes that the object pointed to by that name is a user-defined function. Other names can be associated with the same function object and be used to call that function:

```pycon
>>> def print_num_range(max):
...     '''Print numbers from zero to max.'''
...     for num in range(max):
...             print(num)
...
>>> pnr = print_num_range
>>> pnr(10)
0
1
2
3
4
5
6
7
8
9
>>> 
```

Functions without a [return](https://docs.python.org/reference/simple_stmts.html#return) statement return `None`, a built-in name. The interpreter suppresses the writing of `None` if it would be the only written value, but you can force its display by using `print()`:

```pycon
>>> print(pnr(10))
0
1
2
3
4
5
6
7
8
9
None
>>> 
```

We can redo the previously defined `print_num_range()` function to return a number list, instead:

```pycon
>>> def create_num_list(max):
...     '''Return list of numbers from zero to max.'''
...     num_list = [num for num in range(max)]
...     return num_list
... 
>>> num_list = create_num_list(5)
>>> num_list
[0, 1, 2, 3, 4]
>>> 
```

User-defined functions have [special attributes](https://docs.python.org/reference/datamodel.html#the-standard-type-hierarchy) (_dunder attributes_).

#### Variable Number of Arguments

There are three ways to define functions that can be called with a variable number of arguments, and the approaches can be combined:

1. Arbitrary Arguments
2. Default Argument Values
3. Keyword Arguments

##### Arbitrary Arguments

A function can be called with an arbitrary number of arguments, which are captured in a [tuple](https://docs.python.org/library/stdtypes.html#tuple):

```pycon
>>> def merge_strs(*args):
...     '''Return a merged string.'''
...     return ' '.join(args)
... 
>>> merge_strs('one', 'two', 'three')
'one two three'
>>> 
```

[Variadic](https://en.wikipedia.org/wiki/Variadic) arguments (e.g., `*args`) are placed after positional arguments and capture all remaining input arguments that are passed to a function. Any parameters that occur after the `*args` argument in a function definition are keyword parameters (named parameters), i.e., they can only be used as keyword arguments and not as positional arguments.

##### Default Argument Values

By assigning a function parameter a default value in the function definition, you make it optional in the function call:

```pycon
>>> def opt_params(a, b, c='three'):
...     '''Demonstrate optional parameters.'''
...     print(a, b, c)
... 
>>> opt_params('one', 'two')
one two three
>>> opt_params('one', 'two', '3')
one two 3
>>> 
```

Default values are evaluated at the point of the function definition in the _defining_ scope:

```python
quote = '''\
All we ever see of stars are their old photographs.

- Alan Moore\
'''

def print_quote(quote=quote):
    '''Print a quote.'''
    print(quote)


quote = '''\
"What happened to the American Dream?" It came true! You\'re lookin\' at it.

- Alan Moore\
'''

print_quote()
```

The code above prints:

```console
All we ever see of stars are their old photographs.

- Alan Moore
```

A default value is only evaluated _once_. This will matter when the default parameter is a _mutable_ object (e.g., a list, dictionary, or most class instances) in that modifications to the default value outside of the function will permanently alter it across future function calls:

```python
def name_collector(name, name_list=[]):
    '''Collect and return a list of names.'''
    name_list.append(name.title())
    return name_list


print(name_collector('ada lovelace'))
print(name_collector('rachel carson'))
print(name_collector('rosalind franklin'))
```

The code above prints:

```console
['Ada Lovelace']
['Ada Lovelace', 'Rachel Carson']
['Ada Lovelace', 'Rachel Carson', 'Rosalind Franklin']
```

Use _immutable_ objects (e.g., `None`, `True`, `False`, numbers, strings) when setting default values.

Remember that the following values are interpreted as false:

- `False`
- `None`
- numeric zero of all types
- empty strings and containers (e.g., strings, tuples, lists, dictionaries, sets, frozensets)

All other values are [interpreted as true](https://docs.python.org/reference/expressions.html#boolean-operations).

When using `None` as the default value, use it with the _identity operator_:

```python
def ex_func(a, b=None):
    if b is None:
        b = []
```

If you want function code that tests for the presence of a user-supplied argument, a common idiom is to use a unique, private instance of `object`:

```pycon
>>> _no_val = object()
>>> def ex_func(a, b=_no_val):
...     if b is _no_val:
...             print('No b value supplied.')
... 
>>> ex_func('b supplied?')
No b value supplied.
>>> 
```

##### Keyword Arguments

Functions can be called with [keyword arguments](https://docs.python.org/glossary.html#term-keyword-argument):

```python
def bird(num_of_birds, action='flying', color='black', b_type='Sparrowhawk'):
    '''Print a bird message.'''
    if num_of_birds.lower() == 'one':
        print(f'{num_of_birds.title()} {color} {b_type} is {action}.')
    else:
        print(f'{num_of_birds.title()} {color} {b_type}s are {action}.')
```

The `bird()` function can be called in myriad ways:

```python
bird('three')  # 1 positional argument
bird(num_of_birds='one')  # 1 keyword argument
bird(color='grey', num_of_birds='five')  # 2 keyword arguments
bird('two', 'eating', 'brown')  # 3 positional arguments
bird('three', b_type='raven')  # 1 positional argument, 1 keyword argument
```

The resulting output would be:

```console
Three black Sparrowhawks are flying.
One black Sparrowhawk is flying.
Five grey Sparrowhawks are flying.
Two brown Sparrowhawks are eating.
Three black ravens are flying.
```

However, the following function calls would fail:

```python
bird()  # Required positional argument missing
bird(color='grey', 'three')  # Keyword argument before a positional argument
bird('three', num_of_birds='three')  # Duplicate value for the same argument
bird(weight=45)  # Unknown keyword argument
```

In function calls, keyword arguments must follow positional arguments. Keyword arguments must match one of the parameters accepted by the function and, as long as they are following positional arguments, their order in the function call does not matter.

A function can be written to accept arbitrary numbers of keyword arguments (e.g., `**kwargs`), which are stored in a [dictionary](https://docs.python.org/glossary.html#term-dictionary):

```python
def arb_args(req_pos, *args, **kwargs):
    '''Demonstrate arbitrary argument usage.'''
    print(f'Required positional argument: {req_pos}', end='\n\n')

    for idx, arg in enumerate(args):
        print(f'Arbitrary argument {idx}: {arg}')

    print()

    for key, value in kwargs.items():
        print(f'Arbitrary keyword argument {key}: {value}')
```

`arb_args()` can be called like so:

```python
arb_args('one',
         'zero', 'one', 'two',
         zero='zero', one='one', two='two')
```

The resulting output would be:

```console
Required positional argument: one

Arbitrary argument 0: zero
Arbitrary argument 1: one
Arbitrary argument 2: two

Arbitrary keyword argument zero: zero
Arbitrary keyword argument one: one
Arbitrary keyword argument two: two
```

The order in which the keyword arguments are printed is guaranteed to match the order in which they were given in the function call.

##### Argument Passing Restrictions

Arguments may be passed to Python functions by either position or keyword. To improve readability and performance, the way arguments can be passed to functions is restricted:

```text
def func(pos_1, pos_2, /, pos_or_kwd, *, kwd_1, kwd_2):
         -------------    ----------     ------------
           |               |                   |
           |          Positional or keyword    |
           |                                   - Keyword only
           -- Positional only
```

`/` and `*` are optional and serve to indicate the kind of parameter being passed to the function.

```pycon
>>> def ex_func(x, *, y):
...     return x, y
... 
>>> ex_func(1, 2)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: ex_func() takes 1 positional argument but 2 were given
>>> ex_func(1, y=2)
(1, 2)
>>> 
```

- By default, if `/` and `*` are not present in a function definition, arguments may be passed by _position_ or _keyword_.
- If a function definition is positional-only, the parameters' _order_ matters and parameters cannot be passed by keyword.

##### Unpacking Argument Lists

The `*` operator can be used to to unpack an arbitrary number of arguments from a list or tuple at the time of a function _call_ that requires separate positional arguments:

```pycon
>>> list(range(1, 6))
[1, 2, 3, 4, 5]
>>> args = [1, 6]
>>> list(range(*args))
[1, 2, 3, 4, 5]
>>> 
```

Dictionaries can yield keyword arguments with the `**` operator:

```pycon
>>> def bird(num_of_birds, action='flying', color='black', b_type='Sparrowhawk'):
...     '''Print a bird message.'''
...     if num_of_birds.lower() == 'one':
...             print(f'{num_of_birds.title()} {color} {b_type} is {action}.')
...     else:
...             print(f'{num_of_birds.title()} {color} {b_type}s are {action}.')
... 
>>> d = {
...     'num_of_birds': 'five',
...     'action': 'singing',
...     'color': 'blue',
...     'b_type': 'Belted Kingfisher'
... }
>>> bird(**d)
Five blue Belted Kingfishers are singing.
>>> 
```

#### Recursive Functions

_Recursion_ means breaking down a problem into increasingly smaller sub-problems until you get to a small enough problem that can be easily solved. Usually, recursion involves a function calling itself.

**The Three Laws of Recursion**

1. A recursive function must have a _base case_ (the condition that allows the algorithm to stop recursing).
2. A recursive function must change its state and move towards the base case.
3. A recursive function must call itself, recursively.

Recursion is like an implied form of iteration that can be used to solve complex problems with a relatively simple solution.

```python
def factorial(num):
    if num < 1:  # Base case
        return 1
    else:  # Recursive call
        return num * factorial(num - 1)


x = 12
y = factorial(x)

print(f'Factorial of {x} is {y}')
# Factorial of 12 is 479001600
```

#### Lambda Expressions

The [lambda](https://docs.python.org/reference/expressions.html#lambda) keyword is used to create small anonymous functions that are syntactically restricted to a _single expression_. Essentially, they are [syntactic sugar](https://en.wikipedia.org/wiki/Syntactic_sugar) for normal function definitions.

For example, `lambda num: num + 2` is equivalent to:

```python
def add_two(num):
    return num + 2
```

Lambda functions consist of:

1. The `lambda` keyword.
2. _Bound_ variables, i.e., the parameters for the lambda function.
3. A _body_, i.e., the single expression that contains _free_ variables, which may be constants or variables defined in the enclosing scope of the function.

To apply a lambda function to an argument, surround the function and its arguments with parentheses:

```pycon
>>> (lambda num: num + 2)(4)
6
>>> 
```

Since a lambda function is an expression, it can be provided a name:

```pycon
>>> add_two = lambda num: num + 2
>>> add_two(6)
8
>>> 
```

Multiple arguments can be provided to lambda functions by separating them with commas, and conditionals (e.g., [conditional expressions](https://docs.python.org/reference/expressions.html#conditional-expressions), which are also referred to as the _ternary operator_) are allowed:

```pycon
>>> sen = ''
>>> build_sen = lambda sen, string: f'{sen} {string}' if sen else string
>>> sen = build_sen(sen, 'A clock')
>>> sen = build_sen(sen, 'without')
>>> sen = build_sen(sen, 'a craftsman.')
>>> sen = build_sen(sen, '\n\n- Alan Moore')
>>> print(sen)
A clock without a craftsman. 

- Alan Moore
>>> 
```

Note that lambda expression variables are bound at _runtime_, not definition time (i.e., the value of a lambda expression variable is whatever the value of the variable is at the _time of execution_):

```pycon
>>> x = 5
>>> f1 = lambda y: x + y
>>> x = 10
>>> f2 = lambda y: x + y
>>> f1(5)
15
>>> f2(5)
15
>>> 
```

To have a lambda expression capture a value at the point of definition, include the value as a default value:

```pycon
>>> x = 5
>>> f1 = lambda y, x=x: x + y
>>> x = 10
>>> f2 = lambda y, x=x: x + y
>>> f1(5)
10
>>> f2(5)
15
>>> 
```

The following terms are used interchangeably:

- Anonymous functions
- Function literals
- Lambda functions
- Lambda expressions
- Lambda abstractions
- Lambda form

When deciding whether to use a lambda expression or not in Python, keep in mind:

> Unlike lambda forms in other languages, where they add functionality, Python lambdas are only a shorthand notation if you’re too lazy to define a function.
>
> Functions are already first class objects in Python, and can be declared in a local scope. Therefore the only advantage of using a lambda instead of a locally defined function is that you don’t need to invent a name for the function – but that’s just a local variable to which the function object (which is exactly the same type of object that a lambda expression yields) is assigned!
>
> \- <cite>[Why can’t lambda expressions contain statements?](https://docs.python.org/faq/design.html#why-can-t-lambda-expressions-contain-statements)</cite>

#### Function Docstrings

The first line of a function documentation string (docstring) should be a short, concise summary of the function's purpose. The line should begin with a capital letter and end with a period.

If additional lines are needed, the second line should be blank, followed by one or more paragraphs describing the function and its calling conventions.

Docstrings are available via the special `.__doc__` attribute. If no documentation is defined, `None` is returned.

```pycon
>>> def docstring_test():
...     '''
...     A docstring example.
...     
...     The example continues...
...     '''
...     pass
... 
>>> print(docstring_test.__doc__)

    A docstring example.
    
    The example continues...
    
>>> 
```

#### Function Annotations

[Function annotations](https://docs.python.org/reference/compound_stmts.html#function) are optional metadata information about the types used by user-defined functions.

[Annotations](https://docs.python.org/glossary.html#term-function-annotation) are stored in the special `.__annotations__` attribute as a `dict`.

```pycon
>>> def create_order(count: int, menu_item: str, stay_or_go: str = 'go') -> str:
...     order = (
...                     f'Item Count: {count}\n'
...                     f'Menu Item: {menu_item}\n'
...                     f'Stay or Go: {stay_or_go}'
...             )
...     return order
... 
>>> print(create_order(3, 'espresso'))
Item Count: 3
Menu Item: espresso
Stay or Go: go
>>> print(create_order(1, 'latte', 'stay'))
Item Count: 1
Menu Item: latte
Stay or Go: stay
>>> create_order.__annotations__
{'count': <class 'int'>, 'menu_item': <class 'str'>, 'stay_or_go': <class 'str'>, 'return': <class 'str'>}
>>> 
```

_Type hints are not enforced at runtime. They are only hints:_

```pycon
>>> print(create_order('three', 'espresso'))
Item Count: three
Menu Item: espresso
Stay or Go: go
>>> 
```

To identify type errors, use a code editor that has a type checker or a tool like [mypy](https://github.com/python/mypy).

#### Decorators

A [decorator](https://docs.python.org/glossary.html#term-decorator) is a function that returns another function. Usually, the decorator takes another function and extends the function's behavior without explicitly modifying it.

To understand what a decorator does, consider the following examples:

```pycon
>>> def woke(word):
...     return f'{word.title()} is important. '
... 
>>> def imperialism(word):
...     output = (
...         f'But {word}, when devoid of a political agenda \n'
...         'that fights the oppressor on behalf of the oppressed, is '
...         'window dressing.\nIt is about incorporating a tiny segment of '
...         'those marginalized by\nsociety into unjust structures '
...         'to perpetuate them.'
...     )
...     return output
... 
>>> def chris_hedges(quote_func):
...     print(quote_func('diversity'), end='')
... 
>>> chris_hedges(woke) ; chris_hedges(imperialism) ; print()
Diversity is important. But diversity, when devoid of a political agenda 
that fights the oppressor on behalf of the oppressed, is window dressing.
It is about incorporating a tiny segment of those marginalized by
society into unjust structures to perpetuate them.
>>> 
```

Notice that the `woke()` and `imperialism()` functions take strings as arguments, but the `chris_hedges()` function take a _function object_ as its argument.

Functions can be defined inside of other functions:

```python
def continental_drift():
    output = (
        'The more a man trades off his known life, the one in front of him \n'
        'that came to him by birth and the accidents and happenstance of \n'
        'youth, the more of that he trades for dreams of a new life, \n'
        'the less power he has.\n'
    )
    print(output)

    def russell():
        output = (
            "Bob Dubois believes this now. But he's fallen to a dark, cold \n"
            'place where the walls are sheer and slick, and all the exits \n'
            'have been sealed.\n'
        )
        print(output)

    def banks():
        output = (
            "He's alone. He's going to have to live here, if he's going to \n"
            'live at all. This is how a good man loses his goodness.'
        )
        print(output)

    russell()
    banks()


continental_drift()
```

The resulting output would be:

```pycon
The more a man trades off his known life, the one in front of him 
that came to him by birth and the accidents and happenstance of 
youth, the more of that he trades for dreams of a new life, 
the less power he has. 

Bob Dubois believes this now. But he's fallen to a dark, cold 
place where the walls are sheer and slick, and all the exits 
have been sealed. 

He's alone. He's going to have to live here, if he's going to 
live at all. This is how a good man loses his goodness.
```

Functions can be return values, as well:

```python
def chris_hedges(num):
    def class_():
        sen = (
            'The rich, throughout history, have found ways to '
            'subjugate and re-subjugate the masses.'
        )
        return sen

    def struggle():
        sen = (
            'And the masses, throughout history, have cyclically awoken '
            'to throw off their chains.'
        )
        return sen

    if num == 1:
        return class_
    else:
        return struggle


first_sen = chris_hedges(1)
second_sen = chris_hedges(2)

print(first_sen)
# <function main.<locals>.chris_hedges.<locals>.class_ at 0x7f83bd3bbe50>
print(second_sen)
# <function main.<locals>.chris_hedges.<locals>.struggle at 0x7f83bd3bbf70>
```

Above, the `first_sen` and `second_sen` labels are references to the `class_()` and `struggle()` functions inside of `chris_hedges()`. Since they are function handles, we can call them:

```python
print(first_sen())
# The rich, throughout history, have found ways to subjugate and re-subjugate the masses.
print(second_sen())
# And the masses, throughout history, have cyclically awoken to throw off their chains.
```

All of the above concepts can be used to create a decorator:

```python
def decorator(func):
    def wrapper():
        output = (
            'Father believed that the universe was a '
            'gigantic clockworks, brilliantly lit.\n'
        )
        print(output)

        func()

        output = (
            "It's an endless sea of darkness moving beneath a dark sky, \n"
            'between which, isolate bits of light, we constantly '
            'rise and fall.'
        )
        print(output)

    return wrapper


def cloudsplitter():
    print("But it's not.\n")


cloudsplitter = decorator(cloudsplitter)

cloudsplitter()
```

The resulting output would be:

```pycon
Father believed that the universe was a gigantic clockworks, brilliantly lit.

But it's not.

It's an endless sea of darkness moving beneath a dark sky, 
between which, isolate bits of light, we constantly rise and fall.
```

After decoration, `cloudsplitter()` points to the `wrapper()` function:

```python
print(cloudsplitter)
# <function main.<locals>.decorator.<locals>.wrapper at 0x7fe4b697bdc0>
print(cloudsplitter.__name__)
# wrapper
```

However, `wrapper()` has a reference to the original `cloudsplitter()` as `func` and calls that function between the two `print()` calls. Therefore, `decorator()` is used to wrap `cloudsplitter()` and modify its behavior _without_ modifying `cloudsplitter()` itself.

Decorating functions like `cloudsplitter = decorator(cloudsplitter)` is verbose, so Python allows for using a simpler syntax using the `@` symbol, referred to as the [pie-decorator](https://peps.python.org/pep-0318/#background) syntax:

```python
def decorator(func):
    def wrapper():
        output = (
            'Father believed that the universe was a '
            'gigantic clockworks, brilliantly lit.\n'
        )
        print(output)

        func()

        output = (
            "It's an endless sea of darkness moving beneath a dark sky, \n"
            'between which, isolate bits of light, we constantly '
            'rise and fall.'
        )
        print(output)

    return wrapper


@decorator
def cloudsplitter():
    print("But it's not.\n")


cloudsplitter()
```

This simpler syntax is an application of [syntactic sugar](https://en.wikipedia.org/wiki/Syntactic_sugar).

[Introspection](https://en.wikipedia.org/wiki/Type_introspection) is the ability of an object to know about its own attributes at runtime. For example, Python functions know their names and documentation:

```pycon
>>> input
<built-in function input>
>>> input.__name__
'input'
>>> input.__doc__
'Read a string from standard input.  The trailing newline is stripped.\n\nThe prompt string, if given, is printed to standard output without a\ntrailing newline before reading input.\n\nIf the user hits EOF (*nix: Ctrl-D, Windows: Ctrl-Z+Return), raise EOFError.\nOn *nix systems, readline is used if available.'
>>> 
```

As demonstrated with the `cloudsplitter()` example above, after a function is decorated, it reports being the wrapper function inside of your decorator. To address this, decorators should use the `@functools.wraps` decorator, which preserves information about the original function:

```python
import functools


def decorator(func):
    @functools.wraps(func)
    def wrapper():
        output = (
            'Father believed that the universe was a '
            'gigantic clockworks, brilliantly lit.\n'
        )
        print(output)

        func()

        output = (
            "It's an endless sea of darkness moving beneath a dark sky, \n"
            'between which, isolate bits of light, we constantly '
            'rise and fall.'
        )
        print(output)

    return wrapper


@decorator
def cloudsplitter():
    print("But it's not.\n")


print(cloudsplitter)
# <function main.<locals>.cloudsplitter at 0x7f802c1d0dc0>
print(cloudsplitter.__name__)
# cloudsplitter
```

The `@functools.wraps` decorator uses the function `functools.update_wrapper()` to update special attributes like `.__name__` and `.__doc__` that are used in the introspection.

A simplified decorator template may look like this:

```python
import functools


def ex_dec(ex_func):
    @functools.wraps(ex_func)
    def ex_wrap():
        ...
        ex_func()
        ...
    return ex_wrap


@ex_dec
def ex_func():
    ...
```

If the decorated function takes arguments or returns a value, your wrapper function will need to account for this:

```python
import functools


def ex_dec(ex_func):
    @functools.wraps(ex_func)
    def ex_wrap(*args, **kwargs):
        ...
        ex_val = ex_func(*args, **kwargs)
        ...
        return ex_val
    return ex_wrap


@ex_dec
def ex_func(ex_args):
    ...
    return ex_val
```

For example:

```python
import functools


def decorator(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        max_len = len('Wrap Start')
        print('Wrap Start', max_len * '~', sep='\n')
        rtn_msg = func(*args, **kwargs)
        print(max_len * '~', f"{'Wrap End': ^{max_len}}", sep='\n')

        return f'\nReturn message: {rtn_msg}'

    return wrapper


@decorator
def msg(word):
    print(f'{word} is a recursive acronym.')

    return f"It means {word}'s not UNIX!"


print(msg('GNU'))
```

The resulting output would be:

```pycon
Wrap Start
~~~~~~~~~~
GNU is a recursive acronym.
~~~~~~~~~~
 Wrap End 

Return message: It means GNU's not UNIX!
```

## Data Structures

### Lists

Lists have many useful methods:

```pycon
>>> lords_of_war = ['Lockheed Martin', 'Boeing', 'Northrop Grumman', 'Lockheed Martin', 'Raytheon Technologies', 'General Dynamics', 'Boeing']
>>> lords_of_war.count('Lockheed Martin')  # Return number of item occurrences
2
>>> lords_of_war.count('BAE Systems')
0
>>> lords_of_war.index('Boeing')  # Return zero-based index of the first occurrence of an item
1
>>> lords_of_war.index('Boeing', 2)  # Start search at index 2
6
>>> lords_of_war.reverse()  # Reverse list elements in place
>>> lords_of_war
['Boeing', 'General Dynamics', 'Raytheon Technologies', 'Lockheed Martin', 'Northrop Grumman', 'Boeing', 'Lockheed Martin']
>>> lords_of_war.append('Aviation Industry Corporation of China')
>>> lords_of_war
['Boeing', 'General Dynamics', 'Raytheon Technologies', 'Lockheed Martin', 'Northrop Grumman', 'Boeing', 'Lockheed Martin', 'Aviation Industry Corporation of China']
>>> lords_of_war.sort()  # Sort list elements in place
>>> lords_of_war
['Aviation Industry Corporation of China', 'Boeing', 'Boeing', 'General Dynamics', 'Lockheed Martin', 'Lockheed Martin', 'Northrop Grumman', 'Raytheon Technologies']
>>> lords_of_war.pop()  # Remove and return last list item
'Raytheon Technologies'
>>> 
```

Not all data can be sorted or compared:

```pycon
>>> no_sort = ['Booz Allen Hamilton', None, 4]
>>> no_sort.sort()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: '<' not supported between instances of 'NoneType' and 'str'
>>> 2j > 3j
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: '>' not supported between instances of 'complex' and 'complex'
>>> 
```

### Lists As Stacks

List methods enable using lists as stacks, where the last element added is the first element retrieved (last-in, first-out):

```pycon
>>> ka_tet = ['roland', 'eddie', 'susannah']
>>> ka_tet.append('jake')
>>> ka_tet.append('oy')
>>> ka_tet
['roland', 'eddie', 'susannah', 'jake', 'oy']
>>> ka_tet.pop()
'oy'
>>> ka_tet.pop()
'jake'
>>> ka_tet
['roland', 'eddie', 'susannah']
>>> 
```

### Lists As Queues

Lists are not efficient for queues, where the first element added is the first element retrieved (first-in, first-out), i.e., appends and pops from the end of a list are fast, but doing inserts or pops from the beginning of a list are slow because all other elements need to be shifted by one.

[collections.deque](https://docs.python.org/library/collections.html#collections.deque) is designed to have fast appends and pops from both ends:

```pycon
>>> from collections import deque
>>> ka_tet = deque(['roland', 'eddie', 'susannah'])
>>> ka_tet.append('jake')
>>> ka_tet.append('oy')
>>> ka_tet.popleft()
'roland'
>>> ka_tet.pop()
'oy'
>>> ka_tet
deque(['eddie', 'susannah', 'jake'])
>>> 
```

### List Comprehensions

[List comprehensions](https://docs.python.org/glossary.html#term-list-comprehension) are efficient ways to create lists. They are especially useful when you:

- Need to make a new list where each element is the result of an operation applied to each member of another iterable.
- Need to create a subsequence of elements that satisfy a specific condition.

```pycon
>>> the_right_stuff = (
...     'valentina tereshkova',
...     'sally ride',
...     'chiaki mukai',
...     'claudie haigneré',
...     'liu yang',
...     'samantha cristoforetti'
... )
>>> space_women = [name.title() for name in the_right_stuff]
>>> space_women
['Valentina Tereshkova', 'Sally Ride', 'Chiaki Mukai', 'Claudie Haigneré', 'Liu Yang', 'Samantha Cristoforetti']
>>> 
```

A list comprehension consists of brackets (`[]`) containing an expression, followed by a `for` clause and zero or more `for` or `if` clauses. The result is a new list resulting from evaluation of the given expression.

If the expression is a tuple, it must be parenthesized.

```pycon
>>> list_of_tuples = [
...     (x, y)
...     for x in [1, 2, 3]
...     for y in [3, 2, 1]
...     if x <= y
... ]
>>> list_of_tuples
[(1, 3), (1, 2), (1, 1), (2, 3), (2, 2), (3, 3)]
>>> flattened_list = [
...     num
...     for pair in list_of_tuples
...     for num in pair
... ]
>>> flattened_list
[1, 3, 1, 2, 1, 1, 2, 3, 2, 2, 3, 3]
>>> 
```

List comprehensions can contain complex expressions and nested functions.

```python
ex_iter = [elem_1, elem_2, elem_3]

# Traditional list creation
ex_list = []
for ex_elem in ex_iter:
    ex_list_expr

# List comprehension
ex_list = [ex_expr for ex_elem in ex_iter]

# More legible list comprehension
ex_list = [
    ex_expr
    for ex_elem in ex_iter
]
```

```python
ex_iter = [elem_1, elem_2, elem_3]

# Traditional list creation with a conditional
ex_list = []
for ex_elem in ex_iter:
    if ex_cnd:
        ex_list_expr

# List comprehension with a conditional
ex_list = [ex_expr for ex_elem in ex_iter if ex_cnd]

# More legible list comprehension with a conditional
ex_list = [
    ex_expr
    for ex_elem in ex_iter
    if ex_cnd
]
```

```python
ex_iter = [elem_1, elem_2, elem_3]

# Traditional list creation with if...else
ex_list = []
for ex_elem in ex_iter:
    if ex_cnd:
        ex_if_list_expr
    else:
        ex_else_list_expr

# List comprehension with if...else
ex_list = [
    ex_if_expr
    if ex_cnd
    else
    ex_else_expr
    for ex_elem in ex_iter
]
```

```python
# Traditional list creation with multiple if...else
ex_list = []
for ex_elem in ex_iter:
    if ex_cnd_1:
        ex_if_list_expr_1
    elif ex_cnd_2:
        ex_if_list_expr_2
    else:
        ex_else_list_expr

# List comprehension with multiple if...else
ex_list = [
    ex_if_expr_1
    if ex_cnd_1
    else
    ex_cnd_2
    if ex_cnd_2
    else
    ex_else_expr
    for ex_elem in ex_iter
]
```

```python
ex_iter_outer = [(elem_1, elem_2), (elem_1, elem_2), (elem_1, elem_2)]

# Traditional list creation with multiple for loops
ex_list = []
for ex_iter_inner in ex_iter_outer:
    for ex_elem_inner in ex_iter_inner:
        ex_list_expr

# List comprehension with multiple for loops
ex_list = [
    ex_expr
    for ex_iter_inner in ex_iter_outer
    for ex_elem_inner in ex_iter_inner
]
```

#### Nested List Comprehensions

The initial expression in a list comprehension can be another list comprehension:

```pycon
>>> letters = [
...     ('a', 'b', 'c'),
...     ('d', 'e', 'f'),
...     ('h', 'i', 'j')
... ]
>>> transposed_letters = [
...     [
...         row[i]
...         for row in letters
...     ]
...     for i in range(len(letters))
... ]
>>> transposed_letters
[['a', 'd', 'h'], ['b', 'e', 'i'], ['c', 'f', 'j']]
>>> 
```

However, it is probably better to utilize built-in functions:

```pycon
>>> list(zip(*letters))
[('a', 'd', 'h'), ('b', 'e', 'i'), ('c', 'f', 'j')]
>>> 
```

### The `del` Statement

The `del` statement can be used to remove an item from a list without returning its value:

```pycon
>>> five_eyes = ['australia', 'canada', 'new zealand', 'united kingdom', 'united states']
>>> del five_eyes[1]
>>> five_eyes
['australia', 'new zealand', 'united kingdom', 'united states']
>>> del five_eyes[2:4]
>>> five_eyes
['australia', 'new zealand']
>>> del five_eyes[:]
>>> five_eyes
[]
>>> 
```

### Tuples

A tuple is a [standard Python sequence type](https://docs.python.org/library/stdtypes.html#typesseq) that consists of a number of values separated by commas:

```pycon
>>> items = 1, 'two', 3
>>> items
(1, 'two', 3)
>>> items[0]
1
>>> 
```

Tuples can contain other collections:

```pycon
>>> more_items = items, (4, 'five', 6)
>>> more_items
((1, 'two', 3), (4, 'five', 6))
>>> 
```

Unlike lists, tuples are immutable:

```pycon
>>> letters = 'a', 'b', 'c'
>>> letters[0] = 'A'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment
>>> 
```

However, tuples can contain mutable objects:

```pycon
>>> tuple_of_lists = ([1, 'two', 3], [4, 'five', 6])
>>> tuple_of_lists
([1, 'two', 3], [4, 'five', 6])
>>> tuple_of_lists[0][1] = 2
>>> tuple_of_lists
([1, 2, 3], [4, 'five', 6])
>>> 
```

Output tuples are always enclosed in parentheses. This ensures that nested tuples are correctly interpreted.

Input tuples may or may not use surrounding parentheses. However, parentheses are often required, e.g., when the tuple is part of a larger expression.

Often, tuples are used in different situations than lists, e.g., to contain heterogenous sequences of elements that are accessed via unpacking or indexing.

Compared to other data types, tuples have idiosyncrasies. For example, an empty tuple is created with an empty pair of parentheses and a tuple with one item is the value followed by a comma:

```pycon
>>> empty_tup = ()
>>> len(empty_tup)
0
>>> empty_tup
()
>>> singleton = 'one',
>>> len(singleton)
1
>>> singleton
('one',)
>>> 
```

_Tuple packing_ (assignment) packs together items into a tuple:

`items = 1, 'two', 3`

The reverse operation is _sequence unpacking_, which works for any sequence on the right-hand side:

```pycon
>>> items = 1, 'two', 3
>>> a, b, c = items
>>> a
1
>>> b
'two'
>>> c
3
>>> items = [4, 5, 6]
>>> a, b, c = items
>>> a
4
>>> b
5
>>> c
6
>>> 
```

Sequence unpacking requires that there are as many variables on the left side of the assignment operator as there are elements in the sequence. _Multiple assignment_ is the combination of tuple packing and sequence unpacking.

### Sets

A [set](https://docs.python.org/library/stdtypes.html#set) is an unordered collection with no duplicate elements. Sets are used for membership testing, data deduplication, and mathematical operations like union, intersection, difference, and symmetric difference.

Sets are created with curly braces (`{}`) or the `set()` constructor.

Like tuples, sets have idiosyncrasies, i.e., empty sets must be created with the `set()` constructor.

```pycon
>>> moons_of_jupiter = {'io', 'europa', 'io', 'ganymede', 'europa', 'callisto'}
>>> moons_of_jupiter
{'europa', 'callisto', 'ganymede', 'io'}
>>> 'europa' in moons_of_jupiter
True
>>> 'titan' in moons_of_jupiter
False
>>> moons = set(('io', 'ganymede', 'titan', 'iapetus', 'ganymede', 'dione', 'io'))
>>> moons
{'io', 'ganymede', 'titan', 'iapetus', 'dione'}
>>> moons_of_jupiter - moons  # Moons in moons_of_jupiter, but not moons
{'europa', 'callisto'}
>>> moons_of_jupiter | moons  # Moons in moons_of_jupiter, moons, or both
{'dione', 'ganymede', 'callisto', 'titan', 'iapetus', 'europa', 'io'}
>>> moons_of_jupiter & moons  # Moons in both moons_of_jupiter and moons
{'ganymede', 'io'}
>>> moons_of_jupiter ^ moons  # Moons in moons_of_jupiter or moons, but not both
{'dione', 'titan', 'iapetus', 'europa', 'callisto'}
>>> 
```

Set comprehensions are supported:

```pycon
>>> moon_subset = {
...     moon
...     for moon in ('io', 'europa', 'io', 'ganymede', 'europa', 'callisto')
...     if moon not in ('io', 'callisto')
... }
>>> moon_subset
{'europa', 'ganymede'}
>>> 
```

### Dictionaries

A [dictionary](https://docs.python.org/library/stdtypes.html#dict) is a set of _key: value_ pairs, where every key in the dictionary must be unique. Keys can be any immutable type (e.g., strings, numbers).

Tuples can be used as keys as long as they only contain strings, numbers, or tuples. If a tuple contains any mutable object, either directly or indirectly, it cannot be used as a key.

A pair of braces (`{}`) creates an empty dictionary and placing a comma-separated list of `key: value` pairs in the braces adds initial dictionary _key-value_ pairs. This is also how dictionaries are displayed in output.

Dictionaries are mainly used to store a value with a key and extract the value when the key is provided.

A key-value pair can be deleted with the `del` statement.

If you store a value with an extant key, the old value associated with the key is replaced. An error is generated when you try to extract a value using a non-existent key.

Passing a dictionary to `list()` returns a list of dictionary keys, in insertion order. Passing a dictionary to `sorted()` returns a sorted list of dictionary keys.

Use the `in` keyword to verify if a key is in a dictionary.

```pycon
>>> moon_diameters = {
...     'mimas': 296,
...     'enceladus': 504,
... }
>>> moon_diameters
{'mimas': 296, 'enceladus': 504}
>>> moon_diameters['mimas'] = 396
>>> moon_diameters
{'mimas': 396, 'enceladus': 504}
>>> moon_diameters['tethys'] = 1062
>>> moon_diameters
{'mimas': 396, 'enceladus': 504, 'tethys': 1062}
>>> list(moon_diameters)
['mimas', 'enceladus', 'tethys']
>>> sorted(moon_diameters)
['enceladus', 'mimas', 'tethys']
>>> 'enceladus' in moon_diameters
True
>>> 'tethys' not in moon_diameters
False
>>> del moon_diameters['mimas']
>>> moon_diameters
{'enceladus': 504, 'tethys': 1062}
>>> 
```

The [dict()](https://docs.python.org/library/stdtypes.html#dict) constructor builds dictionaries from sequences of key-value pairs:

```pycon
>>> dict([('mimas', 396), ('enceladus', 504), ('tethys', 1062)])
{'mimas': 396, 'enceladus': 504, 'tethys': 1062}
>>> 
```

Sometimes, if keys are simple strings, it may be easier to create a dictionary using keyword arguments:

```pycon
>>> dict(mimas=396, enceladus=504, tethys=1062)
{'mimas': 396, 'enceladus': 504, 'tethys': 1062}
>>> 
```

#### Dictionary Comprehensions

[Dictionary comprehensions](https://docs.python.org/glossary.html#term-dictionary-comprehension) can be used to create dictionaries from arbitrary key-value expressions:

```pycon
>>> {
...     x: x * 2
...     for x in (1, 3, 5)
... }
{1: 2, 3: 6, 5: 10}
>>> 
```

```python
ex_iter = [elem_1, elem_2, elem_3]

# Traditional dictionary creation
ex_dict = {}
for ex_elem in ex_iter:
    ex_dict_expr

# Dictionary comprehension
ex_dict = {
    ex_expr: ex_expr
    for ex_elem in ex_iter
}
```

```python
ex_iter = [elem_1, elem_2, elem_3]

# Traditional dictionary creation
ex_dict = {}
for ex_elem in ex_iter:
    if ex_cnd:
        ex_if_expr
    else:
        ex_else_expr

# Dictionary comprehension
ex_dict = {
    ex_expr: (
        ex_if_expr
        if ex_cnd
        else
        ex_else_expr
    )
    for ex_elem in ex_iter
}
```

### Looping Techniques

When looping through dictionaries, keys and their associated values can be simultaneously retrieved with the `.items()` method:

```pycon
>>> payoffs_2019_2020 = {
...     'Bloomberg LP': 158.9,
...     'National Association of Realtors': 154.3,
...     'Fahr LLC': 70.7
... }
>>> for lobbyist, payoff in payoffs_2019_2020.items():
...     print(f'Purchasing political clout cost {lobbyist}',
...           f'{payoff} million dollars across 2019 and 2020.')
... 
Purchasing political clout cost Bloomberg LP 158.9 million dollars across 2019 and 2020.
Purchasing political clout cost National Association of Realtors 154.3 million dollars across 2019 and 2020.
Purchasing political clout cost Fahr LLC 70.7 million dollars across 2019 and 2020.
>>> 
```

When looping through sequences, the positional index and value can be simultaneously retrieved using the [`enumerate()`](https://docs.python.org/3.10/library/functions.html#enumerate) function:

```pycon
>>> for index, planet in enumerate(('venus', 'earth', 'mars')):
...     print(index, planet.title())
... 
0 Venus
1 Earth
2 Mars
>>> 
```

Simultaneously looping over two or more sequences can be accomplished using the [zip()](https://docs.python.org/library/functions.html#zip) function:

```pycon
>>> cowboys = ('Spike Spiegel', 'Jet Black', 'Faye Valentine', 'Radical Edward')
>>> heights = (185, 188, 168, 154)
>>> for cowboy, height in zip(cowboys, heights):
...     print(f'{cowboy} is {height} centimeters tall.')
... 
Spike Spiegel is 185 centimeters tall.
Jet Black is 188 centimeters tall.
Faye Valentine is 168 centimeters tall.
Radical Edward is 154 centimeters tall.
>>> 
```

To loop over a sequence in reverse, use the [reversed()](https://docs.python.org/library/functions.html#reversed) function:

```pycon
>>> for word in reversed(('Faye.', 'eyes,', 'my', 'at', 'Look')):
...     print(word)
... 
Look
at
my
eyes,
Faye.
>>> 
```

To loop over a sequence in sorted order, use the `sorted()` function, which returns a new sorted list:

```pycon
>>> distros = ('EndeavourOS', 'Fedora', 'Debian')
>>> for distro in sorted(distros):
...     print(distro)
... 
Debian
EndeavourOS
Fedora
>>> 
```

`set()` and `sorted()` can be used together to loop over the unique elements of a sequence:

```pycon
>>> distros = ('EndeavourOS', 'Fedora', 'Debian', 'Fedora', 'EndeavourOS')
>>> for distro in sorted(set(distros)):
...     print(distro)
... 
Debian
EndeavourOS
Fedora
>>> 
```

It is simpler and safer to create a new list versus modifying a list as you loop over it:

```pycon
>>> fruits = ['lemon', 'mango', 'lime', 'peach']
>>> sweet_fruits = []
>>> for fruit in fruits:
...     if fruit in ('mango', 'peach'):
...         sweet_fruits.append(fruit)
... 
>>> sweet_fruits
['mango', 'peach']
>>> 
```

### Conditions

The conditions used in `while` and `if` statements can contain any operators.

The comparison operators `in` and `not in` test membership and determine whether a value is in (or not in) a container. The comparison operators `is` and `is not` compare whether or not two objects are the same object.

All comparison operators have the same priority, which is lower than all numerical operators.

Comparison may be combined using the Boolean operators `and` and `or`. The outcome of a comparison, or any other Boolean expression, may be negated with `not`. Boolean operators have a lower priority than comparison operators, with `not` having the highest priority and `or` the lowest priority.

The Boolean `and` and `or` operators are [short-circuit operators](https://en.wikipedia.org/wiki/Short-circuit_evaluation), i.e., their arguments are evaluated from left to right, and evaluation stops as soon as the outcome is determined. When used as a general value versus a Boolean, the return value of the short-circuit operator is the last evaluated argument.

The result of a comparison or Boolean expression can be saved to a variable:

```pycon
>>> lang_1, lang_2, lang_3 = '', 'JavaScript', 'Python'
>>> lang = lang_1 and lang_2 or lang_3
>>> lang
'Python'
>>> 
```

Assignment inside of expressions _must be done explicitly_  with the [walrus operator](https://docs.python.org/faq/design.html#why-can-t-i-use-an-assignment-in-an-expression), `:=`. This avoids problems like using `=` in an expression when `==` was intended.

### Comparing Sequences and Other Types

Typically, sequence objects may be compared to other objects of the same sequence type. Comparisons use [lexicographical ordering](https://en.wikipedia.org/wiki/Lexicographic_order).

First, the first two items are compared. If they differ, this determines the comparison of the outcome. If they are equal, the next two items are compared. This continues until either sequence is exhausted.

If two items being compared are sequences of the same type themselves, the lexicographical comparison is carried out recursively.

If all items of two sequences compare equal, the sequences are considered equal.

If one sequence is an initial sub-sequence of the other, the short sequence is considered the lesser one.

Lexicographical ordering for strings uses the Unicode code point number to order individual characters.

```pycon
>>> (8, 9, 10) < (10, 9, 8)
True
>>> [8, 9, 10] < [10, 9, 8]
True
>>> 'xyz' < 'z' < 'zebrafish' < 'zeitgeist'
True
>>> (8, 9, 10, 11) < (10, 9, 8)
True
>>> (8, 9) < (10, 9, -8)
True
>>> (8, 9, 10) == (8.0, 9.0, 10.0)
True
>>> (8, 9, ('xx', 'xy')) < (8, 9, ('xyz', 'x'), 10)
True
>>> 
```

Comparing objects of different types with `<` or `>` is OK, provided that the objects have appropriate comparison methods (e.g., `0` equals `0.0`). Otherwise, the interpreter will raise a [TypeError](https://docs.python.org/library/exceptions.html#TypeError) exception.

## Modules

A Python _module_ is a way to put your definitions in a file and use them in a script or interactive interpreter session. Module definitions can be imported into other modules or into the _main_ module (i.e., the collection of variables that you have access to in a script at the top level and in interactive mode).

A module is a file that contains Python definitions and statements. Its name is the module name with the suffix `.py`. Inside a module, the module's name (as a string) is available as the value of the global variable `__name__`.

For example, assume a `sigint_philosopher.py` file in the current directory:

```python
'''SIGINT Philosopher quotes module'''


def gentlemen():
    print(
        'One philosophical SIGINT conundrum that faces many of us SIGINTers '
        "is the \nfeeling famously expressed by Herbert Hoover's Secretary of "
        'State, \nHenry Stimson, that "gentlemen do not read other '
        '''gentlemen's mail."'''
    )


def dystopia():
    print(
        'I found myself wishing that my life would be constantly and '
        'completely \nmonitored. It might seem odd that a self-professed '
        'libertarian would wish \nan Orwellian dystopia on himself, but here '
        'was my rationale: If people knew \na few things about me, I might '
        'seem suspicious. But if people knew \neverything about me, '
        "they'd see they had nothing to fear."
    )
```

In the Python interpreter, import the module:

`import sigint_philosopher`

Function names directly defined in `sigint_philosopher` are _not_ added to the current [namespace](https://docs.python.org/glossary.html#term-namespace). Only the module name `sigint_philosopher` is added.

The module name can be used to access the functions:

```pycon
>>> import sigint_philosopher
>>> sigint_philosopher.gentlemen()
One philosophical SIGINT conundrum that faces many of us SIGINTers is the 
feeling famously expressed by Herbert Hoover's Secretary of State, 
Henry Stimson, that "gentlemen do not read other gentlemen's mail."
>>> sigint_philosopher.dystopia()
I found myself wishing that my life would be constantly and completely 
monitored. It might seem odd that a self-professed libertarian would wish 
an Orwellian dystopia on himself, but here was my rationale: If people knew 
a few things about me, I might seem suspicious. But if people knew 
everything about me, they'd see they had nothing to fear.
>>> 
```

If you are going to frequently use a function, you can assign it a local name:

```pycon
>>> dystopia = sigint_philosopher.dystopia
>>> dystopia()
I found myself wishing that my life would be constantly and completely 
monitored. It might seem odd that a self-professed libertarian would wish 
an Orwellian dystopia on himself, but here was my rationale: If people knew 
a few things about me, I might seem suspicious. But if people knew 
everything about me, they'd see they had nothing to fear.
>>> 
```

Module statements are intended to initialize the module and are _only_ executed the first time the module name is encountered in an `import` statement. Statements are also run if the file is executed as a script.

Each module has its own private namespace, which is used as the global namespace by all functions defined in the module. This prevents a module author's global variables from interfering with a user's global variables. Also, it allows a user to refer to a module's global variables with the `ex_module.ex_item_name` notation.

Modules can import other modules.

It is customary to place all `import` statements at the beginning of a module or script. Imported module names placed at the top level of a module (i.e., outside of any functions or classes) are added to the module's global namespace.

There are different variants of the `import` statement.

The following variant directly imports names from a module, which does not introduce the module name (`sigint_philosopher`) into the local namespace:

```pycon
>>> from sigint_philosopher import gentlemen, dystopia
>>> gentlemen()
One philosophical SIGINT conundrum that faces many of us SIGINTers is the 
feeling famously expressed by Herbert Hoover's Secretary of State, 
Henry Stimson, that "gentlemen do not read other gentlemen's mail."
>>> 
```

This variant imports all names that a module defines (except those beginning with an underscore, `_`):

```pycon
>>> from sigint_philosopher import *
>>> dystopia()
I found myself wishing that my life would be constantly and completely 
monitored. It might seem odd that a self-professed libertarian would wish 
an Orwellian dystopia on himself, but here was my rationale: If people knew 
a few things about me, I might seem suspicious. But if people knew 
everything about me, they'd see they had nothing to fear.
>>> 
```

Usually, the `import *` variant is avoided as it introduces an unknown set of names into the interpreter, potentially hiding (overwriting) items you have already defined.

If a module name is followed by `as`, then the name following `as` is directly bound to the imported module:

```pycon
>>> import sigint_philosopher as sip
>>> sip.gentlemen()
One philosophical SIGINT conundrum that faces many of us SIGINTers is the 
feeling famously expressed by Herbert Hoover's Secretary of State, 
Henry Stimson, that "gentlemen do not read other gentlemen's mail."
>>> 
```

Effectively, this import is equivalent to `import sigint_philosopher`, with the only difference being that it is available as `sip`.

`from` can also be used with `as`:

```pycon
>>> from sigint_philosopher import dystopia as dys
>>> dys()
I found myself wishing that my life would be constantly and completely 
monitored. It might seem odd that a self-professed libertarian would wish 
an Orwellian dystopia on himself, but here was my rationale: If people knew 
a few things about me, I might seem suspicious. But if people knew 
everything about me, they'd see they had nothing to fear.
>>> 
```

Since each module is only imported once per interpreter session, if you change modules, you must restart the interpreter. Alternatively, if you are only interactively testing a single module, use [importlib.reload()](https://docs.python.org/library/importlib.html#importlib.reload), e.g., `import importlib ; importlib.reload(ex_module)`.

### Executing Modules as Scripts

When you run a Python module like `python3 sigint_philosopher.py ex_args`, the code in the module is executed, but with the `__name__` set to `__main__`. By adding `if __name__ == '__main__':` to your module, you can make your file usable as both a script and a module, i.e., the code that parses the command line only runs if the module is executed as the _main_ file:

```python
'''SIGINT Philosopher quotes module'''


def gentlemen():
    print(
        'One philosophical SIGINT conundrum that faces many of us SIGINTers '
        "is the feeling famously expressed by Herbert Hoover's Secretary of "
        'State, Henry Stimson, that "gentlemen do not read other '
        '''gentlemen's mail."'''
    )


def dystopia():
    print(
        'I found myself wishing that my life would be constantly and '
        'completely monitored. It might seem odd that a self-professed '
        'libertarian would wish an Orwellian dystopia on himself, but here '
        'was my rationale: If people knew a few things about me, I might '
        'seem suspicious. But if people knew everything about me, '
        "they'd see they had nothing to fear."
    )


def honor_bound():
    print(
        'If we are going to work on targets that fall short of being '
        'technically \n"enemies" but are rather informative for our policy '
        "makers -- and we are -- \nthen even looking at it from the target's "
        'perspective, we are honor-bound \nto do more and better '
        'monitoring rather than less.'
    )


if __name__ == '__main__':
    honor_bound()
```

```console
$ python3 sigint_philosopher.py
If we are going to work on targets that fall short of being technically 
"enemies" but are rather informative for our policy makers -- and we are -- 
then even looking at it from the target's perspective, we are honor-bound 
to do more and better monitoring rather than less.
$ 
```

If the module is imported, it will not run:

```pycon
>>> import sigint_philosopher
>>> 
```

Often, this technique is used to provide a user interface to a module or for testing purposes (e.g., running the module as a script executes a test suite).

### The Module Search Path

When a module `ex_module` is imported, the Python interpreter searches for a built-in module with that name. These module names are listed in [sys.builtin_module_names](https://docs.python.org/library/sys.html#sys.builtin_module_names). If the module is not found, the interpreter then searches for `ex_module.py` in a list of directories provided by the [sys.path](https://docs.python.org/library/sys.html#sys.path) variable.

`sys.path` is initialized from the following locations:

- The directory containing the input script (or the current directory when no file is specified).
    - On file systems that support symbolic links, the directory containing the input script is calculated _after_ the symlink is followed, i.e., the directory containing the symlink is not added to the module search path.
- [PYTHONPATH](https://docs.python.org/using/cmdline.html#envvar-PYTHONPATH) (a list of directory names with the same syntax as the `PATH` shell variable).
- The installation-dependent default (by convention, including a `site-packages` directory that is handled by the [site](https://docs.python.org/library/site.html#module-site) module).

After initialization, Python programs can modify `sys.path`. The directory that contains the running script is placed at the beginning of the search path, ahead of the standard library path, i.e., scripts in this directory will be loaded instead of modules of the same name in the library directory.

### _Compiled_ Python Files

Python caches compiled versions of each module in the `__pycache__` directory under the name `ex_module_ex_version.pyc`, where the version encodes the format of the compiled file. Generally, it contains the Python version number.

Via an automatic process, Python checks the modification date of the source against the compiled version to see if it is outdated and needs to be recompiled. Compiled modules are platform-independent, i.e., the same library can be shared among systems with different architectures.

Python does not check the cache when:

1. The module is directly loaded from the command line.
2. There is no source module.

### Standard Modules

Python comes with a library of standard modules. Some modules are built into the interpreter and provide access to operations that are not a part of the core language. The set of modules that are available is configurable in a platform-dependent manner (e.g., [winreg](https://docs.python.org/library/winreg.html#module-winreg) is only provided on Windows systems).

Every Python interpreter has access to the [sys](https://docs.python.org/library/sys.html#module-sys) module.

The `sys.ps1` and `sys.ps2` variables define the primary and secondary interpreter prompts, respectively:

```pycon
>>> import sys
>>> sys.ps1
'>>> '
>>> sys.ps2
'... '
>>> sys.ps1 = '$ '
$ print('Not Bash!')
Not Bash!
$ 
```

`sys.ps1` and `sys.ps2` are only available in interactive mode.

The `sys.path` list of strings can be modified like any other list:

```pycon
>>> import sys
>>> sys.path.append('/home/monty/lib/python')
```

The `sys` module also offers different ways to determine the Python version:

```pycon
>>> import sys
>>> sys.version
'3.11.2 (main, Mar 19 2023, 12:18:29) [GCC 12.2.0]'
>>> sys.version_info
sys.version_info(major=3, minor=11, micro=2, releaselevel='final', serial=0)
>>> 
```

Alternative ways to obtain the Python version are offered by the [`platform`](https://docs.python.org/library/platform.html#module-platform) module:

```pycon
>>> from platform import python_version, python_version_tuple
>>> python_version()
'3.11.2'
>>> python_version_tuple()
('3', '11', '2')
>>> 
```

### The `dir()` Function

The built-in [dir()](https://docs.python.org/library/functions.html#dir) function displays currently defined names:

```pycon
>>> dir()
['__annotations__', '__builtins__', '__doc__', '__loader__', '__name__', '__package__', '__spec__']
>>> 
```

`dir()` shows all types of names (e.g., variables, modules, functions), but does not include the names of built-in functions and variables, which are defined in the standard module [builtins](https://docs.python.org/library/builtins.html#module-builtins).

To see the attributes for `builtins` (or any other object), pass it as an argument to `dir()`:

```pycon
>>> import builtins
>>> dir(builtins)
['ArithmeticError', 'AssertionError', 'AttributeError', 'BaseException', 'BlockingIOError', 'BrokenPipeError', 'BufferError', 'BytesWarning', 'ChildProcessError', 'ConnectionAbortedError', 'ConnectionError', 'ConnectionRefusedError', 'ConnectionResetError', 'DeprecationWarning', 'EOFError', 'Ellipsis', 'EnvironmentError', 'Exception', 'False', 'FileExistsError', 'FileNotFoundError', 'FloatingPointError', 'FutureWarning', 'GeneratorExit', 'IOError', 'ImportError', 'ImportWarning', 'IndentationError', 'IndexError', 'InterruptedError', 'IsADirectoryError', 'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError', 'ModuleNotFoundError', 'NameError', 'None', 'NotADirectoryError', 'NotImplemented', 'NotImplementedError', 'OSError', 'OverflowError', 'PendingDeprecationWarning', 'PermissionError', 'ProcessLookupError', 'RecursionError', 'ReferenceError', 'ResourceWarning', 'RuntimeError', 'RuntimeWarning', 'StopAsyncIteration', 'StopIteration', 'SyntaxError', 'SyntaxWarning', 'SystemError', 'SystemExit', 'TabError', 'TimeoutError', 'True', 'TypeError', 'UnboundLocalError', 'UnicodeDecodeError', 'UnicodeEncodeError', 'UnicodeError', 'UnicodeTranslateError', 'UnicodeWarning', 'UserWarning', 'ValueError', 'Warning', 'ZeroDivisionError', '__build_class__', '__debug__', '__doc__', '__import__', '__loader__', '__name__', '__package__', '__spec__', 'abs', 'all', 'any', 'ascii', 'bin', 'bool', 'breakpoint', 'bytearray', 'bytes', 'callable', 'chr', 'classmethod', 'compile', 'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'divmod', 'enumerate', 'eval', 'exec', 'exit', 'filter', 'float', 'format', 'frozenset', 'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'int', 'isinstance', 'issubclass', 'iter', 'len', 'license', 'list', 'locals', 'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct', 'open', 'ord', 'pow', 'print', 'property', 'quit', 'range', 'repr', 'reversed', 'round', 'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str', 'sum', 'super', 'tuple', 'type', 'vars', 'zip']
>>> 
```

### Packages

Packages are Python's way of structuring the module namespace by using _dotted module names_, e.g., `ex_pkg.ex_submod`. This saves multi-module package authors from having to be concerned about each other's module names.

Note that a package is still a module.

```text
ex_pkg/                         Top-level package
      __init__.py               Initialize the ex_pkg package
      ex_submod_1.py
      ex_submod_2.py
      ...
```

```text
ex_pkg/                         Top-level package
      __init__.py               Initialize the ex_pkg package
      ex_subpkg_1/              Subpackage 1
              __init__.py
              ex_submod_1.py
              ex_submod_2.py
              ...
      ex_subpkg_2/              Subpackage 2
              __init__.py
              ex_submod_3.py
              ex_submod_4.py
              ...
      ex_subpkg_3/              Subpackage 3
              __init__.py
              ex_submod_5.py
              ex_submod_6.py
              ...
```

When a package is imported, Python searches through the directories in the `sys.path` list to find the correct package directory/subdirectory. `__init__.py` files in each package directory/subdirectory are required to make Python treat directories containing the files as packages (except when creating a [namespace package](https://docs.python.org/glossary.html#term-namespace-package)).

`__init__.py` can be an empty file, can contain execution initialization code for the package, or code to set the `__all__` variable.

Individual modules can be imported from a package:

`import ex_pkg.ex_subpkg.ex_submod`

After import, the submodule will need to be referenced by its full name.

An alternative `import` format can be used so that the submodule can be referenced without its package prefix (i.e., `ex_pkg.ex_subpkg.`):

`from ex_pkg.ex_subpkg import ex_submod`

After import, `ex_submod` can be referenced as `ex_submod`.

Submodule variables/functions can be directly imported:

`from ex_pkg.ex_subpkg.ex_submod import ex_func`

When using the form `from ex_pkg import ex_item`, `ex_item` can be either a submodule (or subpackage) of the package or another name defined in the package (e.g., a function, class, or variable). The `import` statement first tests whether the item is defined in the package. If not, it assumes that it is a module and tries to load it. If this fails, an [ImportError](https://docs.python.org/library/exceptions.html#ImportError) exception is raised.

When using the `import ex_pkg.ex_subitem.ex_subsubitem` format, each item except for the last must be a package. The last item can be a module or a package.

#### Importing `*` From a Package

If a package directory's `__init__.py` file defines a list named `__all__`, `from ex_pkg.ex_subpkg import *` imports all modules included in the `__all__` list.

If `__all__` is not defined, `from ex_pkg.ex_subpkg import *` only imports `ex_pkg.ex_subpkg`, followed by the names defined in the package, including any names defined by (and submodules explicitly loaded by) `__init__.py`. Submodules of the package that were explicitly loaded by previous `import` statements are also included.

The recommended notation for imports is `from ex_pkg import ex_submod`, unless the importing module needs to use submodules with the same name from different packages.

#### Intra-Package References

When packages are structured into subpackages, you can use absolute imports to refer to submodules of sibling packages, e.g., if `ex_pkg.ex_subpkg_3.ex_submod_6` needs to use `ex_submod_3` in `ex_subpkg_2`, it can use `from ex_pkg.ex_subpkg_2 import ex_submod_3`.

The `from ex_pkg import ex_item` form can also use relative imports. These imports use leading dots (`.`) to indicate the current and parent packages involved in the relative import.

For example, these imports could be included in `ex_submod_3.py`:

```python
from . import ex_submod_4
from .. import ex_subpkg_1
from ..ex_subpkg_3 import ex_submod_5
```

#### Packages in Multiple Directories

Packages support the [`.__path__`](https://docs.python.org/reference/import.html#path__) special attribute, which is a list containing the name of the directory containing the package's `__init__.py` file before the code in that file was executed. `.__path__` can be modified, and doing so affects future searches for modules/submodules in the package.

## Input and Output

Data can be printed in a human-readable form or written to a file for later use.

The [`io`](https://docs.python.org/library/io.html#module-io) module provides Python's facilities for dealing with various types of I/O. There are three main types of I/O:

1. [`Text I/O`](https://docs.python.org/library/io.html#text-i-o) - Expects and produces `str` objects.
2. [`Binary I/O`](https://docs.python.org/library/io.html#binary-i-o) - Expects [bytes-like objects](https://docs.python.org/glossary.html#term-bytes-like-object) and produces `bytes` objects. Also referred to as _buffered I/O_.
3. [`Raw I/O`](https://docs.python.org/library/io.html#raw-i-o) - Generally, used as a low-level building-block for binary and text streams. Also referred to as _unbuffered I/O_.

The above types are generic categories and various backing stores can be used for each of them. A concrete object belonging to any of these categories is a [file object](https://docs.python.org/glossary.html#term-file-object). Other common terms for file object are _stream_ and _file-like object_.

### Prettier Output Formatting

For more control over the formatting of your output, you can:

- Use [formatted string literals](https://docs.python.org/tutorial/inputoutput.html#tut-f-strings).
- Use string slicing and concatenation operations to create a custom output layout.

For a fast way to display a variable's content, you can convert any value to a string using the [str()](https://docs.python.org/library/stdtypes.html#str) constructor or the [repr()](https://docs.python.org/library/functions.html#repr) function.

`str()` returns human-readable representations of values. `repr()` returns representations that can be read by the interpreter (or will generate a [SyntaxError](https://docs.python.org/library/exceptions.html#SyntaxError) if there is not equivalent syntax).

For objects that do not have human-readable forms, `str()` returns the same value that `repr()` does. Many values (e.g., numbers, lists, dictionaries) have the same representation using either function. Specifically, strings have two distinct representations.

```pycon
>>> jasper = "So, why bother if life's going to make its own choices?\n\n- David Arata, Alfonso Cuarón, Timothy J. Sexton"
>>> str(jasper)
"So, why bother if life's going to make its own choices?\n\n- David Arata, Alfonso Cuarón, Timothy J. Sexton"
>>> repr(jasper)
'"So, why bother if life\'s going to make its own choices?\\n\\n- David Arata, Alfonso Cuarón, Timothy J. Sexton"'
>>> print(jasper)
So, why bother if life's going to make its own choices?

- David Arata, Alfonso Cuarón, Timothy J. Sexton
>>> str(3 / 4)
'0.75'
>>> x = 1
>>> y = 2
>>> repr((x, y, ('trois', 'quatre')))
"(1, 2, ('trois', 'quatre'))"
>>> 
```

#### Formatted String Literals

[Formatted string literals](https://docs.python.org/reference/lexical_analysis.html#f-strings) (f-strings) let you include Python expression values inside of a string by prefixing the string with `f` (or `F`) and writing expressions as `{ex_expr}`. An optional [format specifier](https://docs.python.org/library/string.html#formatspec) that uses the _Format Specification Mini-Language_ can follow the expression to allow greater control over how the value is formatted.

The following example uses a format specifier that sets a fill character, an alignment, and a character width:

```pycon
>>> line_1 = 'Tell them they can be great someday, like us.'
>>> line_2 = 'Tell them they belong among us, no matter how we treat them.'
>>> line_3 = 'Tell them they must earn the respect which everyone else receives by default.'
>>> line_4 = 'N. K. Jemisin'
>>> erlsset = (
...     f'\n{line_1:+<90}'
...     f'\n{line_2:+^90}'
...     f'\n{line_3:+>90}'
...     f'\n\n{line_4:*^90}'
... )
>>> print(erlsset)

Tell them they can be great someday, like us.+++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++Tell them they belong among us, no matter how we treat them.+++++++++++++++
+++++++++++++Tell them they must earn the respect which everyone else receives by default.

**************************************N. K. Jemisin***************************************
>>> 
```

Other modifiers can be used to convert values before formatting (e.g., `!r` applies `repr()`):

```pycon
>>> dashed = 'open'
>>> print(f'There is nothing {dashed} about Open APIs.\n\n- Aral Balkan')
There is nothing open about Open APIs.

- Aral Balkan
>>> print(f'There is nothing {dashed!r} about Open APIs.\n\n- Aral Balkan')
There is nothing 'open' about Open APIs.

- Aral Balkan
>>> 
```

### Reading and Writing Files

[open()](https://docs.python.org/library/functions.html#open) returns a [file object](https://docs.python.org/glossary.html#term-file-object) and is commonly used with two positional arguments and one keyword argument:

`f = open(ex_file, ex_mode, encoding=ex_encoding)`

- `ex_file` is a string containing the filename.
- `ex_mode` is another string containing characters describing the way in which the file will be used. This argument is optional and, if omitted, is assumed to be `r`.
- If `ex_encoding` is not specified, the default is platform-dependent. UTF-8 is the de-facto standard and `encoding='utf-8'` is recommended, unless you know that you need a different encoding.

    When opening a file in _byte mode_, you can not specify an encoding (i.e., data is read/written as [bytes](https://docs.python.org/library/stdtypes.html#bytes) objects.

    <table>
      <caption>File Access Modes</caption>
      <thead>
          <tr>
              <th>Mode</th>
              <th>Description</th>
          </tr>
      </thead>
      <tbody>
        <tr>
          <td><code>a</code></td>
          <td>Open a file for appending. The file pointer is at the end of the file if the file exists.</td>
        </tr>
        <tr>
          <td><code>a+</code></td>
          <td>Open a file for both appending and reading.</td>
        </tr>
        <tr>
          <td><code>ab</code></td>
          <td>Open a file for appending in binary format.</td>
        </tr>
        <tr>
          <td><code>ab+</code></td>
          <td>Open a file for both appending and reading in binary format.</td>
        </tr>
        <tr>
          <td><code>r</code></td>
          <td>Open a file for reading only. This is the default mode.</td>
        </tr>
        <tr>
          <td><code>r+</code></td>
          <td>Open a file for both reading and writing.</td>
        </tr>
        <tr>
          <td><code>rb</code></td>
          <td>Open a file for reading only in binary format.</td>
        </tr>
        <tr>
          <td><code>rb+</code></td>
          <td>Open a file for both reading and writing in binary format.</td>
        </tr>
        <tr>
          <td><code>w</code></td>
          <td>Open a file for writing only. Overwrites the file if the file exists. Creates a new file if it does not exist.</td>
        </tr>
        <tr>
          <td><code>w+</code></td>
          <td>Open a file for both writing and reading. Overwrites the file if the file exists. Creates a new file if it does not exist.</td>
        </tr>
        <tr>
          <td><code>wb</code></td>
          <td>Open a file for writing only in binary format.</td>
        </tr>
        <tr>
          <td><code>wb+</code></td>
          <td>Open a file for both writing and reading in binary format.</td>
        </tr>
      </tbody>
    </table>

When reading/writing strings from/to a file in _text mode_, the default when reading is to convert platform-specific line endings (`\n` on GNU/Linux and UNIX, `\r\n` on Windows) to `\n`. When writing in text mode, the default is to convert occurrences of `\n` back to platform-specific line endings.

It is best practice to use the [with](https://docs.python.org/reference/compound_stmts.html#with) keyword when dealing with file objects. This ensures that the file is properly closed after its `open()` block is complete, even if an exception is raised:

```pycon
>>> with open('quote.txt', 'r', encoding='utf-8') as f:
...     data = f.read()
... 
>>> f.closed
True
>>> 
```

If `with` is not used, make sure to call `.close()` on the file object when it is no longer required. The file will be closed and resources used by it will be immediately freed.

Regardless of how a file object is closed, attempts to use it after it is closed will automatically fail.

Some helpful [`IOBase`](https://docs.python.org/library/io.html#io.IOBase) methods include:

- [`io.IOBase.close()`](https://docs.python.org/library/io.html#io.IOBase.close) - Flush and close this stream.
- [`io.IOBase.readline(size=- 1, /)`](https://docs.python.org/library/io.html#io.IOBase.readline) - Read and return one line from the stream.
- [`io.IOBase.readlines(hint=- 1, /)`](https://docs.python.org/library/io.html#io.IOBase.readlines) - Read and return a list of lines from the stream.
- [`io.IOBase.seek(offset, whence=SEEK_SET, /)`](https://docs.python.org/library/io.html#io.IOBase.seek) - Change the stream position to the given byte `offset`.
- [`io.IOBase.tell()`](https://docs.python.org/library/io.html#io.IOBase.tell) - Return the current stream position.

Each `io` type has its own `.read()` and `.write()` methods:

- `io.RawIOBase`
    - [`io.RawIOBase.read(size=- 1, /)`](https://docs.python.org/library/io.html#io.RawIOBase.read) - Read up to `size` bytes from the object and return them.
    - [`io.RawIOBase.write(b, /)`](https://docs.python.org/library/io.html#io.RawIOBase.write) - Write the given bytes-like object, `b`, to the underlying raw stream, and return the number of bytes written.
- `io.BufferedIOBase`
    - [`io.BufferedIOBase.read1(size=- 1, /)`](https://docs.python.org/library/io.html#io.BufferedIOBase.read1) - Read and return up to `size` bytes, with at most one call to the underlying raw stream’s [.read()](https://docs.python.org/library/io.html#io.RawIOBase.read) (or [.readinto()](https://docs.python.org/library/io.html#io.RawIOBase.readinto)) method.
    - [`io.BufferedIOBase.write(b, /)`](https://docs.python.org/library/io.html#io.BufferedIOBase.write) - Write the given bytes-like object, `b`, and return the number of bytes written (always equal to the length of `b` in bytes, since if the write fails an [`OSError`](https://docs.python.org/library/exceptions.html#OSError) will be raised).
- `io.TextIOBase`
    - [`io.TextIOBase.read(size=- 1, /)`](https://docs.python.org/library/io.html#io.TextIOBase.read) - Read and return at most `size` characters from the stream as a single `str`.
    - [`io.TextIOBase.write(s, /)`](https://docs.python.org/library/io.html#io.TextIOBase.write) - Write the string `s` to the stream and return the number of characters written.

#### Methods of File Objects

Read a file's contents with `.read(ex_size)`, which reads a quantity of data (`ex_size`) and returns it as a string (text mode) or `bytes` object (binary mode). `ex_size` is optional and if omitted, `.read()` returns the contents of the file.

If the end of a file has been reached, `.read()` returns an empty string (`''`).

```pycon
>>> f = open('quote.txt', 'r', encoding='utf-8')
>>> f.read()
'As the world fell, each of us in our own way was broken. It was hard to know who was more crazy. Me, or everyone else.\n\n- Nick Lathouris, Brendan McCarthy, George Miller\n'
>>> f.close()
>>> 
```

`.readline()` reads a single line from a file. A newline character (`\n`) is left at the end of the string and is only omitted on the last line of the file if the file does not end in a newline. This is done to make the return value unambiguous, i.e., if `.readline()` returns an empty string, the end of the file has been reached, while a blank line is represented by `'\n'`.

```pycon
>>> f = open('quote.txt', 'r', encoding='utf-8')
>>> f.readline()
'As the world fell, each of us in our own way was broken. It was hard to know who was more crazy. Me, or everyone else.\n'
>>> f.readline()
'\n'
>>> f.readline()
'- Nick Lathouris, Brendan McCarthy, George Miller\n'
>>> f.readline()
''
>>> f.close()
>>> 
```

Each line in a file can be efficiently processed by looping over the file object:

```pycon
>>> f = open('mark_zuckerberg_2004.txt')
>>> for line in f:
...     print(line, end='')
... 
ZUCK: yea so if you ever need info about anyone at harvard
ZUCK: just ask
ZUCK: i have over 4000 emails, pictures, addresses, sns
FRIEND: what!? how’d you manage that one?
ZUCK: people just submitted it
ZUCK: i don’t know why
ZUCK: they “trust me”
ZUCK: dumb fucks
>>> f.close()
>>> 
```

To read all lines of a file as a list, use `.readlines()` or pass the file object to `list()`:

```pycon
>>> f = open('quote.txt')
>>> quote_lines = f.readlines()
>>> quote_lines
['I start from the supposition that the world is topsy-turvy, that things are all\n', 'wrong, that the wrong people are in jail and the wrong people are out of jail,\n', 'that the wrong people are in power and the wrong people are out of power,\n', 'that the wealth is distributed in this country and the world in such a way as\n', 'not simply to require small reform, but to require a drastic reallocation\n', "of wealth. I start from the supposition that we don't have to say too much\n", 'about this because all we have to do is think about the state of the world\n', 'today and realize that things are all upside down.\n', '\n', '- Howard Zinn']
>>> print(''.join(quote_lines).rstrip())
I start from the supposition that the world is topsy-turvy, that things are all
wrong, that the wrong people are in jail and the wrong people are out of jail,
that the wrong people are in power and the wrong people are out of power,
that the wealth is distributed in this country and the world in such a way as
not simply to require small reform, but to require a drastic reallocation
of wealth. I start from the supposition that we don't have to say too much
about this because all we have to do is think about the state of the world
today and realize that things are all upside down.

- Howard Zinn
>>> 
```

`.write(ex_str)` writes `ex_str` to a file and returns the number of characters written:

```pycon
>>> letter_to_mlk_jr = 'King, there is only one thing left for you to do. You know what it is...There is but one way out for you. You better take it before your filthy, abnormal fraudulent self is bared to the nation.\n\n- The Federal Bureau of Investigation'
>>> f = open('quote.txt', 'w')
>>> f.write(letter_to_mlk_jr)
232
>>> f.close()
>>> 
```

`.tell()` returns an integer that gives the file object's current position in the file represented as the number of bytes from the beginning of the file (binary mode) or an opaque number (text mode).

Use `.seek(ex_offset, ex_whence)` to change the file object's position. The position is computed from adding `ex_offset` to a reference point, which is selected by `ex_whence`. A `ex_whence` value of `0` measures from the beginning of the file, `1` uses the current file position, and `2` uses the end of the file as the reference point.

`ex_whence` is optional and defaults to `0`.

In text mode, only seeks relative to the beginning of the file are allowed (except when seeking to the end of the file with `.seek(0, 2)`), and the only `ex_offset` values are those returned from `.tell()` or zero. Other `ex_offset` values produce undefined behavior.


```pycon
>>> f = open('quote.txt', 'r')
>>> f.tell()
0
>>> f.readline()
'He laughs a little, opening his eyes to roll them toward her. She can tell it’s another of those laughs he does when he really wants to express something other than humor. Misery this time, or maybe weary resignation. He’s always bitter. How he shows it is just a matter of degree.\n'
>>> f.readline()
'\n'
>>> f.readline()
'- N.K. Jemisin'
>>> f.seek(0)
0
>>> f.readline()
'He laughs a little, opening his eyes to roll them toward her. She can tell it’s another of those laughs he does when he really wants to express something other than humor. Misery this time, or maybe weary resignation. He’s always bitter. How he shows it is just a matter of degree.\n'
>>> f.seek(0, 2)
301
>>> f.readline()
''
>>> f.close()
>>> 
```

#### Saving Structured Data With `json`

`.read()` only returns strings, i.e., strings like `'987'` will need to be passed to [int()](https://docs.python.org/library/functions.html#int) to return their numeric value. To save more complex data types (e.g., nested lists, dictionaries), Python allows the use of [JavaScript Object Notation (JSON)](http://json.org/).

The standard module [json](https://docs.python.org/library/json.html#module-json) can take Python data hierarchies and convert them to string representations. This process is referred to as _serializing_. Reconstructing data from a string representation is called _deserializing_.

[`.dumps()`](https://docs.python.org/library/json.html#json.dumps) can be used to view an object's JSON string representation:

```pycon
>>> import json
>>> counting = ('uno', 'dos', 'tres')
>>> json.dumps(counting)
'["uno", "dos", "tres"]'
>>> 
```

[`.dump()`](https://docs.python.org/library/json.html#json.dump) serializes an object to a [text file](https://docs.python.org/glossary.html#term-text-file):

```pycon
>>> import json
>>> counting = ('ichi', 'ni', 'san')
>>> f = open('counting.txt', 'w')
>>> json.dump(counting, f)
>>> f.close()
>>> 
```

[`.load()`](https://docs.python.org/library/json.html#json.load) decodes the object:

```pycon
>>> import json
>>> f = open('counting.txt', 'r')
>>> counting = json.load(f)
>>> counting
['ichi', 'ni', 'san']
>>> f.close()
>>> 
```

JSON files must be encoded in UTF-8.

The serialization techniques above can be used for lists and dictionaries. Serializing arbitrary custom classes requires [additional effort](https://docs.python.org/library/json.html#basic-usage).

## Errors and Exceptions

Generally, there are two kinds of errors:

1. syntax errors
2. exceptions

### Syntax Errors

Syntax errors (parsing errors) are the most common kind of error you will likely encounter.

```pycon
>>> quote = 'None are more hopelessly enslaved than those who falsely believe they are free.\n\n- Johann Wolfgang von Goethe'
>>> printquote)
  File "<stdin>", line 1
    printquote)
              ^
SyntaxError: unmatched ')'
>>> 
```

The parser repeats the erroneous line and displays a caret (`^`) pointing at the earliest point in the line where the error was detected. The error is detected at the token _preceding_ the arrow.

The filename and line number are printed in case the input came from a script.

### Exceptions

Syntactically correct code may still cause an error when executed. These types of errors are _exceptions_ and are not necessarily fatal.

```pycon
>>> '3' + 4
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str
>>> print(monty)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'monty' is not defined
>>> 20 / 0
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: division by zero
>>> 
```

Exceptions come in myriad types and the type of exception encountered is indicated as part of the message. The string printed as the exception type is the name of the [built-in exception](https://docs.python.org/library/exceptions.html#bltin-exceptions) that occurred. This is true for all built-in exceptions, but not necessarily for user-defined exceptions. The rest of the line provides details regarding what caused the exception.

Standard exception names are built-in _identifiers_, not reserved keywords.

The part of the exception output prior to the exception type shows the context where the exception occurred in the form of a [stack traceback](https://en.wikipedia.org/wiki/Stack_trace). Usually, it contains a stack traceback listing source lines and will not display lines from the standard input.

The traceback includes the error message, the line number of the line that caused the error, and the sequence of function calls that led to the error. This sequence is called the _call stack_.

To obtain the traceback as a string, use [`traceback.format_exc()`](https://docs.python.org/library/traceback.html#traceback.format_exc):

```python
import traceback

try:
    raise Exception(
        'This is the ugly future, the one we never wanted, the one that we '
        'fought to prevent. We failed.'
    )
except:
    err_log = '/tmp/frank_rieger_2005.txt'
    with open(err_log, 'w', encoding='utf-8') as f:
        f.write(traceback.format_exc())
    print(f'The traceback was written to {err_log}.')
    # The traceback was written to /tmp/frank_rieger_2005.txt
```

The above example has the program write traceback information to a log file when an exception occurs and keeps the program running. The traceback text is written to `/tmp/frank_rieger_2005.txt`:

```text
Traceback (most recent call last):
  File "world_of_tomorrow.py", line 61, in <module>
    raise Exception(
Exception: This is the ugly future, the one we never wanted, the one that we fought to prevent. We failed.
```

If an application is being served, it is not wise to let users see tracebacks. They may contain sensitive information that could be used to attack serving resources.

The class hierarchy for built-in exceptions is:

```text
BaseException
 ├── BaseExceptionGroup
 ├── GeneratorExit
 ├── KeyboardInterrupt
 ├── SystemExit
 └── Exception
      ├── ArithmeticError
      │    ├── FloatingPointError
      │    ├── OverflowError
      │    └── ZeroDivisionError
      ├── AssertionError
      ├── AttributeError
      ├── BufferError
      ├── EOFError
      ├── ExceptionGroup [BaseExceptionGroup]
      ├── ImportError
      │    └── ModuleNotFoundError
      ├── LookupError
      │    ├── IndexError
      │    └── KeyError
      ├── MemoryError
      ├── NameError
      │    └── UnboundLocalError
      ├── OSError
      │    ├── BlockingIOError
      │    ├── ChildProcessError
      │    ├── ConnectionError
      │    │    ├── BrokenPipeError
      │    │    ├── ConnectionAbortedError
      │    │    ├── ConnectionRefusedError
      │    │    └── ConnectionResetError
      │    ├── FileExistsError
      │    ├── FileNotFoundError
      │    ├── InterruptedError
      │    ├── IsADirectoryError
      │    ├── NotADirectoryError
      │    ├── PermissionError
      │    ├── ProcessLookupError
      │    └── TimeoutError
      ├── ReferenceError
      ├── RuntimeError
      │    ├── NotImplementedError
      │    └── RecursionError
      ├── StopAsyncIteration
      ├── StopIteration
      ├── SyntaxError
      │    └── IndentationError
      │         └── TabError
      ├── SystemError
      ├── TypeError
      ├── ValueError
      │    └── UnicodeError
      │         ├── UnicodeDecodeError
      │         ├── UnicodeEncodeError
      │         └── UnicodeTranslateError
      └── Warning
           ├── BytesWarning
           ├── DeprecationWarning
           ├── EncodingWarning
           ├── FutureWarning
           ├── ImportWarning
           ├── PendingDeprecationWarning
           ├── ResourceWarning
           ├── RuntimeWarning
           ├── SyntaxWarning
           ├── UnicodeWarning
           └── UserWarning
```

### Handling Exceptions

Exceptions can be handled (the following code runs until the user enters **Ctrl+c**):

```pycon
>>> while True:
...     try:
...         text = input('\nEnter text to convert to lowercase: ')
...         print(text.lower())
...     except KeyboardInterrupt:
...         print('\n\nExiting gracefully...')
...         break
... 

Enter text to convert to lowercase: MONTY
monty

Enter text to convert to lowercase: PYTHON
python

Enter text to convert to lowercase: 

Exiting gracefully...
>>> 
```

The [`try`](https://docs.python.org/reference/compound_stmts.html#try) statement works as follows:

- The `try` clause is executed. The only code that should go in the `try` clause is code that might cause an exception to be raised.
- If no exception occurs, the `except` clause is skipped and execution of the `try` clause is complete.
- If an exception occurs during execution of the `try` clause, the rest of the clause is skipped. Then, if the exception type matches the named exception after the `except` keyword, the `except` clause is executed. Next, execution continues after the `try`/`except` clause.
- If an exception occurs that does not match the named exception in the `except` clause, it is passed to outer `try` statements. If no handler is found, it is an _unhandled exception_ and execution stops with an exception message.

`try` statements can have more than one `except` clause to specify handlers for different exceptions. At most, one handler is executed. Also, multiple exceptions can be passed to an `except` clause as a parenthesized tuple (e.g., `except (TypeError, NameError, ZeroDivisionError):`).

Handlers only handle exceptions that occur in the corresponding `try` clause, _not_ in other handlers of the same `try` statement.

A class in an `except` clause is compatible with an exception _if its the same class or a base class_ thereof. For example, in the code below:

- class `first_child` inherits from the `Exception` class
- class `second_child` inherits from the `first_child` class
- class `third_child` inherits from the `second_child` class

```python
class first_child(Exception):
    pass


class second_child(first_child):
    pass


class third_child(second_child):
    pass


for cls in (first_child, second_child, third_child):
    try:
        raise cls()
    except third_child:
        print('third_child')
    except second_child:
        print('second_child')
    except first_child:
        print('first_child')
```

The resulting output would be:

```pycon
first_child
second_child
third_child
```

Note that if `except first_child:` was the first exception, the output would have been:

```pycon
first_child
first_child
first_child
```

That is, `second_child` and `third_child` are child classes (derived classes) of `first_child`, their parent class (base class), and only the _first matching `except` clause_ is triggered.

All exceptions inherit from [`BaseException`](https://docs.python.org/library/exceptions.html#BaseException), so when you have multiple `except` clauses, it can serve as a default clause:

```pycon
>>> while True:
...     try:
...         text = input('\nEnter text to convert to integer: ')
...         print(int(text))
...     except KeyboardInterrupt:
...         print('\n\nExiting gracefully...')
...         break
...     except BaseException as exc:
...         print(f'\nUnexpected {exc=}\n{type(exc)=}\n')
...         raise
... 

Enter text to convert to integer: 923
923

Enter text to convert to integer: ALABASTER

Unexpected exc=ValueError("invalid literal for int() with base 10: 'ALABASTER'")
type(exc)=<class 'ValueError'>

Traceback (most recent call last):
  File "<stdin>", line 4, in <module>
ValueError: invalid literal for int() with base 10: 'ALABASTER'
>>> 
```

The `try`/`except` clause has an optional `else` clause. When present, it must follow all `except` clauses and is useful for code that must be executed if the `try` clause does not raise an exception:

```pycon
>>> poem = '''\
... What happens to a dream deferred? Does it dry up like a raisin in the sun? Or fester like a sore - And then run?
... 
... Does it stink like rotten meat?  Or crust and sugar over - like a syrupy sweet?
... 
... Maybe it just sags like a heavy load.
... 
... Or does it explode?\
... '''
>>> try:
...     print(poem)
... except NameError:
...     print("Check your code's variable names.")
... else:
...     print('\n- Langston Hughes')
... 
What happens to a dream deferred? Does it dry up like a raisin in the sun? Or fester like a sore - And then run?

Does it stink like rotten meat?  Or crust and sugar over - like a syrupy sweet?

Maybe it just sags like a heavy load.

Or does it explode?

- Langston Hughes
>>> 
```

Using an `else` clause is preferable to adding additional code to the `try` clause because it avoids catching exceptions that were not raised by the code being protected by the `try`/`except` clause.

When an exception occurs, it may have an associated value, which is known as the exception _argument_. The presence and type of the argument depend on the exception's type.

The `except` clause may specify a variable after the exception name, which is bound to an exception instance with the arguments stored in `.args`. The exception instance defines `.__str__()`, i.e., the arguments can be directly printed without having to reference `.args`:

```pycon
>>> try:
...     raise Exception('uno', 'due', 'tre')
... except Exception as inst:
...     print(f'{inst.args=}')
...     print(f'{inst=}')
...     one, two, three = inst.args
...     print(
...         f'{one=}',
...         f'{two=}',
...         f'{three=}',
...         sep='\n'
...     )
... 
inst.args=('uno', 'due', 'tre')
inst=Exception('uno', 'due', 'tre')
one='uno'
two='due'
three='tre'
>>> 
```

An exception can also be instantiated before raising it. Attributes can be added to the instance as desired.

For unhandled exceptions, if the exception has arguments, they are printed last in the exception message.

Exception handlers handle exceptions inside functions that are called directly or indirectly in the `try` clause:

```pycon
>>> def dsp_quote(quote):
...     print(text)
... 
>>> try:
...     quote = 'One of the principal functions of a friend is to suffer (in a milder and symbolic form) the punishments that we should like, but are unable, to inflict upon our enemies.\n\n- Aldous Huxley'
...     dsp_quote(quote)
... except NameError as exc:
...     print(f'Handling name error: {exc}')
... 
Handling name error: name 'text' is not defined
>>> 
```

Any time a program depends on something external, there is a higher probability of an exception being raised. Experience will help you know where to include exception handling blocks in your programs and how much information to report to users about errors that occur.

### Raising Exceptions

The [`raise`](https://docs.python.org/reference/simple_stmts.html#raise) statement allows you to force a specified exception to occur:

```pycon
>>> raise NameError("It's dangerous to go alone!")
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: It's dangerous to go alone!
>>> 
```

The argument provided to `raise` must be either an exception instance or an exception class (i.e., a class that is derived from [`Exception`](https://docs.python.org/library/exceptions.html#Exception)).

If an exception class is passed, it can be implicitly instantiated by calling its constructor with no arguments:

`raise NameError`

If you need to determine whether an exception was raised, but do not need to handle it, you can re-raise the exception by using `raise`:

```pycon
>>> try:
...     raise NameError("It's dangerous to go alone!")
... except NameError:
...     print('Take this.')
...     raise
... 
Take this.
Traceback (most recent call last):
  File "<stdin>", line 2, in <module>
NameError: It's dangerous to go alone!
>>> 
```

Often, `raise` statements are placed inside of a function, and the `try` and `except` clauses are placed in the code that calls the function:

```python
def print_sen(sen):
    if not sen:
        raise Exception('Sentence has no content.')
    elif len(sen) < 5:
        raise Exception('Sentence is too short.')
    elif len(sen) > 50:
        raise Exception('Sentence is too long.')

    print(sen)


sentences = (
    'Accelerated Mobile Pages was bad.',
    'Accelerated Mobile Pages was ALWAYS bad.',
    'Did you promote Accelerated Mobile Pages?'
)
for sen in sentences:
    try:
        print_sen(sen)
    except Exception as err_msg:
        print('An exception occurred: ' + str(err_msg))
```

The program above uses the `except Exception as err_msg` form of the `except` statement. If an `Exception` object is returned from `print_sen()`, this `except` statement stores it in a variable named `err_msg`.

### Exception Chaining

An optional [`from`](https://docs.python.org/reference/simple_stmts.html#raise) enables the chaining of exceptions:

```pycon
>>> try:
...     raise ConnectionError
... except ConnectionError as exc:
...     raise RuntimeError('Failed to connect') from exc
... 
Traceback (most recent call last):
  File "<stdin>", line 2, in <module>
ConnectionError

The above exception was the direct cause of the following exception:

Traceback (most recent call last):
  File "<stdin>", line 4, in <module>
RuntimeError: Failed to connect
>>> 
```

Exception chaining automatically happens when exceptions are raised inside of an [`except`](https://docs.python.org/reference/compound_stmts.html#except) or [`finally`](https://docs.python.org/reference/compound_stmts.html#finally) section. This can be overridden by using `from None`:

```pycon
>>> try:
...     open('games.sqlite')
... except OSError:
...     raise RuntimeError from None
... 
Traceback (most recent call last):
  File "<stdin>", line 4, in <module>
RuntimeError
>>> 
```

### User-Defined Exceptions

Python programs can name their own exceptions by creating a new exception class. Typically, exceptions should be derived from the `Exception` class.

Exception classes can be defined to do anything a class can do. Usually, they are kept simple and only offer attributes that allow information about an error to be extracted by handlers for the exception.

Exceptions should have names that end in `Error`, similar to the naming of the standard exceptions.

Many standard modules define their own exceptions that report errors that may occur in the functions that they define.

### Defining Clean-Up Actions

The `try` statement has an optional clause, `finally`, that can be used to define clean-up actions that must always be executed:

```pycon
>>> try:
...     raise KeyboardInterrupt
... finally:
...     print('Later!')
... 
Later!
Traceback (most recent call last):
  File "<stdin>", line 2, in <module>
KeyboardInterrupt
>>> 
```

The `finally` clause executes as the last task before the `try` statement completes. It runs whether or not the `try` statement produces an exception.

- If an exception occurs during execution of the `try` clause, it may be handled by an `except` clause. If the exception is not handled by an `except` clause, the exception is re-raised after the `finally` clause has been executed.
- An exception could occur during execution of an `except` or `else` clause. The exception is re-raised after the `finally` clause has been executed.
- If the `finally` clause executes a `break`, `continue`, or `return` statement, exceptions are not re-raised.
- If the `try` statement reaches a `break`, `continue`, or `return` statement, the `finally` clause will execute just prior to the `break`, `continue`, or `return` statement's execution.
- If a `finally` clause includes a `return` statement, the returned value will be the one from the `finally` clause's `return` statement, not the value from the `try` clause's `return` statement.

```pycon
>>> def silence_is_betrayal():
...     try:
...         return (
...             'Increasingly, by choice or by accident, this is the role our \n'
...             'nation has taken -- the role of those who make peaceful revolution \n'
...             'impossible by refusing to give up the privileges and the pleasures \n'
...             'that come from the immense profits of overseas investment.'
...         )
...     finally:
...         return (
...             'I am convinced that if we are to get on the right side of the \n'
...             'world revolution, we as a nation must undergo a radical revolution \n'
...             'of values. We must rapidly begin the shift from a "thing-oriented" \n'
...             'society to a "person-oriented" society. When machines and computers, \n'
...             'profit motives and property rights are considered more important \n'
...             'than people, the giant triplets of racism, materialism, and \n'
...             'militarism are incapable of being conquered.\n\n- Martin Luther King Jr.'
...         )
... 
>>> print(silence_is_betrayal())
I am convinced that if we are to get on the right side of the 
world revolution, we as a nation must undergo a radical revolution 
of values. We must rapidly begin the shift from a "thing-oriented" 
society to a "person-oriented" society. When machines and computers, 
profit motives and property rights are considered more important 
than people, the giant triplets of racism, materialism, and 
militarism are incapable of being conquered.

- Martin Luther King Jr.
>>> 
```

In practical use, the `finally` clause is often used for releasing external resources (e.g., files, network connections), regardless of whether the use of the resource was successful.

### Pre-Defined Cleanup Actions

Some objects define standard cleanup actions to be executed when the object is no longer needed, regardless of whether the operation using the object succeeded or failed.

For example, the `with` statement allows objects like files to be used in a way that ensures they are correctly cleaned up after they are used (i.e., the file object is closed after the `with` statement completes).

Objects, like files, that provide defined clean-up actions should indicate this in their documentation.

## Assertions

An _assertion_ is a sanity check to confirm that your code is not doing something wrong. Assertions are done via [`assert`](https://docs.python.org/reference/simple_stmts.html#assert) statements. If the assertion fails, an [`AssertionError`](https://docs.python.org/library/exceptions.html#AssertionError) is raised.

An `assert` statement consists of:

1. The `assert` keyword
2. A condition (i.e., an expression that evaluates to `True` or `False`)
3. A comma
4. A string to display when the condition is `False`

```pycon
>>> frank_rieger = 'democracy is already over'
>>> assert frank_rieger == 'democracy is already over', 'By its very nature the western democracies have become a playground for lobbyists, industry interests and conspiracies that have absolutely no interest in real democracy.'
>>> frank_rieger = 'The liberty of a democracy is not safe if the people tolerated the growth of private power to a point where it becomes stronger than the democratic state itself.'
>>> assert frank_rieger == 'democracy is already over', 'By its very nature the western democracies have become a playground for lobbyists, industry interests and conspiracies that have absolutely no interest in real democracy.'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AssertionError: By its very nature the western democracies have become a playground for lobbyists, industry interests and conspiracies that have absolutely no interest in real democracy.
>>> 
```

Unlike exceptions, your code should not handle `assert` statements with `try`/`except` clauses. If an `assert` fails, your program _should_ crash. By failing fast, you shorten the time between the original cause of the error and when you first notice the problem. This reduces the amount of code that you will need to check before finding the code that is causing the issue.

In other words, assertions are for _programmer_ errors, not _user_ errors. For errors that can be recovered from (e.g., file not found, invalid user data entry), raise an exception instead of detecting it with an `assert` statement.

Sometimes, after writing/testing a program, you may want to disable assertions at runtime. This can be accomplished by passing the `-O` option to the Python interpreter:

`python3 -O`

## Classes

Classes enable you to bundle data and functionality. Creating a new class creates a new _type_ of object, which allows new _instances_ of that type to be made. Each class instance can have attributes attached to it to maintain its state, and methods (defined by its class) for modifying its state.

Think of a class as a set of instructions for how to make an instance.

Python classes provide all of the standard features of [object-oriented programming](https://en.wikipedia.org/wiki/Object-oriented_programming):

- The class inheritance mechanism allows multiple base classes.
- A derived class can override any methods of its base class or classes.
- A method can call the method of a base class with the same name.
- Objects can contain arbitrary amounts of any kind of data.

As with modules, classes are created at runtime and can be further modified after creation.

Normally, class members are _public_ and all member functions are _virtual_. There are no shorthands for referencing an object's members from its methods. The method function is declared with an explicit first argument representing the object (i.e., `self`), which is implicitly provided by the call.

Classes themselves are objects, which provides semantics for importing and renaming.

Built-in types can be used as base classes for extension by the user. Most built-in operators with special syntax can be redefined for class instances.

### Names and Objects

Objects have individuality and multiple names (in multiple scopes) can be bound with the same object (i.e., _aliasing_). This can be safely ignored when dealing with immutable objects (e.g., numbers, strings, tuples), but can have unexpected effects on the semantics of Python code involving mutable objects (lists, dictionaries, most other types).

Usually, these effects are used to the benefit of the program (e.g., passing an object has little cost, since only a pointer is passed by the implementation; if a function modifies an object passed as an argument, the caller will see the change).

### Scopes and Namespaces

A _namespace_ is a mapping from names to objects. In Python, most namespaces are implemented as dictionaries. Examples of namespaces are:

- The set of built-in names
- The global names in a module
- The local names in a function invocation

There is _no relation_ between names in different namespaces.

An _attribute_ is any name following a dot:

`ex_obj.ex_attr`

Attributes may be read-only or writable. The latter may also be deleted (i.e., removed from the object) with the [`del`](https://docs.python.org/reference/simple_stmts.html#del) statement.

A _property (managed attribute)_ is a special kind of attribute that allows you to change the underlying implementation of a class's attributes without changing its public API. Essentially, a property represents an intermediate functionality between an attribute and a method. The [`property()`](https://docs.python.org/library/functions.html#property) class constructor allows you to turn class attributes into properties.

Namespaces are created at different times and have different lifetimes.

- The built-in names namespace is created when the Python interpreter starts and is never deleted. Built-in names also exist in a module called `builtins`.
- The global namespace for a module is created when the module definition is read in. The module namespace lasts until the interpreter quits.

    Statements executed by the top-level invocation of the interpreter (either read from a script or interactively) are considered part of a module called [`__main__`](https://docs.python.org/library/__main__.html#module-__main__), and have their own global namespace.

The local namespace for a function is created when the function is called and deleted when the function returns (or raises) an unhandled exception. Recursive function invocations each have their own local namespace.

A _scope_ is a textual region of a Python program where a namespace is directly accessible, i.e., an unqualified reference to a name attempts to find the name in the namespace.

Scopes are statically determined, but dynamically used. At any time during program execution, there are three or four nested scopes whose name spaces are directly accessible:

_Local > Enclosed > Global > Built-In (LEGB)_

1. **Local** - The innermost scope. This scope is searched first and contains the local names.
2. **Enclosed** - The scopes of enclosing functions, which are searched starting with the nearest scope. This scope contains non-local and non-global names.
3. **Global** - The penultimate scope contains the current module's global names.
4. **Built-In** - The outermost scope is searched last and is the namespace containing built-in names.

If a name is declared global, all references and assignments directly go to the middle scope containing the module's global names. Variables found outside of the innermost scope can be rebound using the [`nonlocal`](https://docs.python.org/reference/simple_stmts.html#nonlocal) statement.

If not declared nonlocal, variables found outside of the innermost scope are read-only, i.e., attempts to write to them create _new_ local variables in the innermost scope, leaving the identically named outer variable unchanged.

Scopes are textually determined, i.e., the global scope of a function defined in a module is that module's namespace, regardless of where or by what alias the function is called. The actual search for names is dynamically done at run time.

Class definitions place another namespace in the local scope.

If no `global` or `nonlocal` statement is used, assignments to names always go into the innermost scope. Assignments _do not copy data_, they just bind names to objects. The same logic applies for deletions, i.e., the statement `del ex_name` removes the binding of `ex_name` from the namespace referenced by the local scope.

All operations that introduce new names use the local scope. Specifically, `import` statements and function definitions bind the module/function name in the local scope.

The `global` statement can be used to indicate that specific variables live in the global scope and should be re-bound there. The `nonlocal` statement indicates that specific variables live in an enclosing scope and should be re-bound there.

```python
def dem_scope():
    '''Demonstrate scope.'''
    def do_local():
        name = 'local Essun'

    def do_nonlocal():
        nonlocal name
        name = 'nonlocal Essun'

    def do_global():
        global name
        name = 'global Essun'

    name = 'default Essun'

    do_local()
    print(f'name after local assignment: {name}')

    do_nonlocal()
    print(f'name after nonlocal assignment: {name}')

    do_global()
    print(f'name after global assignment: {name}')


dem_scope()
print(f'name in the global scope: {name}')
```

The resulting output would be:

```pycon
name after local assignment: default Essun
name after nonlocal assignment: nonlocal Essun
name after global assignment: nonlocal Essun
name in the global scope: global Essun
```

### A Closer Look At Classes

Classes introduce new syntax, object types, and semantics.

#### Class Definition Syntax

A simple class definition:

```python
class ExClassName:
    ...
```

Like function definitions, class definitions must be _executed_ before they have any effect.

When a class definition is entered, a new namespace is created and used as the local scope.

When a class definition is normally left (via the end), a class object is created. Essentially, this is a wrapper around the contents of the namespace created by the class definition. Then, the original local scope is re-instated and the class object is bound here to the class name provided in the class definition header.

#### Class Objects

Class objects support two operation types:

1. attribute references
2. instantiation

_Attribute references_ utilize the standard syntax used for all Python attribute references, `ex_obj.ex_attr`. Valid attribute names are all of the names that were in the class's namespace when the class object was created.

```python
class Element:
    '''Model an element.'''

    num = 27

    def ret_elem_name(self):
        return 'cobalt'
```

Given the class `Element`, `Element.num` and `Element.ret_elem_name` are valid attribute references that return an integer and string, respectively. Class attributes can be changed by assignment.

`.__doc__` is a valid attribute and returns the docstring of a class.

Class _instantiation_ uses function notation (i.e., parentheses `()`). Think of a class object as a parameter-less function that returns a new instance of the class (no explicit `return` statement is required). For example, `elem = Element()` assigns a new _instance_ of the `Element` class to the local variable `elem`.

Many classes create objects with instances customized to a specific initial state using a special method (_constructor method_), `.__init__()`:

```python
class Element:
    '''Model an element.'''

    def __init__(self, num, name):
        self.num = num
        self.name = name

    def ret_elem_name(self):
        return self.name
```

When a class defines a `.__init__()` method, class instantiation automatically invokes `.__init__()` for the newly created class instance. Arguments provided to the class instantiation operator are passed to `.__init__()`.

```pycon
>>> class Element:
...     '''Model an element.'''
...     def __init__(self, num, name):
...             self.num = num
...             self.name = name
...     
...     def ret_elem_name(self):
...             return self.name
... 
>>> elem = Element(34, 'selenium')
>>> elem.num, elem.name
(34, 'selenium')
```

The `self` parameter is required in the `.__init__()` method definition and it must come first, before the other parameters. It must be included in the definition because when Python calls a method object, it automatically passes the instance object itself as the first argument of the function.

Passing the instance in this way gives it access to the attributes and methods in the class. Since `self` is automatically passed to its associated class, we do not need to specify it as an argument when creating an instance of the class.

Inside of a class definition, any variable prefixed with `self` becomes an attribute that is available to every method in the class, as well as to any instance created from the class.

#### Instance Objects

The only operations understood by instance objects are attribute references, and there are two kinds of valid attribute names:

1. data attributes
2. methods

_Data attributes_  do not need to be declared. Like local variables, they are automatically created upon assignment.

_Methods_ are functions that belong to an object. Valid method names of an instance depend on its class.

By definition, all attributes of a class that are function objects define corresponding methods of its instances. For example, `elem.ret_elem_name` is a valid method reference, since `Element.ret_elem_name` is a function. However, `elem.ret_elem_name` is not equivalent to `Element.ret_elem_name`, i.e., `elem.ret_elem_name` is a _method object_, not a function object.

Instances have [special read-only attributes](https://docs.python.org/reference/datamodel.html#the-standard-type-hierarchy).

#### Method Objects

Usually, methods are called immediately after being bound:

`elem.ret_elem_name()`

However, it is not required to immediately call a method. The `elem.ret_elem_name` method object can be stored and later called:

```python
elem_m_obj = elem.ret_elem_name
print(elem_m_obj())
```

For methods, the instance object itself is automatically (and transparently) passed as the first argument of the function (i.e., `self`). In other words, `elem.ret_elem_name()` is equivalent to `Element.ret_elem_name(elem)`. In general, calling a method with a list of `n` arguments is the same as calling the corresponding function with an argument list that is created by inserting the method's instance object before the first argument.

When a non-data attribute of an instance is referenced, the instance's class is searched. If the name denotes a valid class attribute that is a function object, a method object is created by packing the instance object and the function object into an abstract object, i.e., the method object.

When the method object is called with an argument list, a new argument list is made from the instance object and the argument list, and the function object is called with this new argument list.

#### Class and Instance Variables

In general, instance variables are for data specific to each instance. Class variables are for attributes and methods shared by all instances of a class.

```python
class Element:
    '''Model an element.'''

    source = 'Periodic Table of Elements'

    def __init__(self, num, name):
        self.num = num
        self.name = name

    def ret_elem_name(self):
        return self.name


h = Element(1, 'hydrogen')
he = Element(2, 'helium')

print(h.source)  # Shared by all elements
print(he.source)  # Shared by all elements
print(h.name)  # Unique to h
print(he.name)  # Unique to he
```

The resulting output would be:

```pycon
Periodic Table of Elements
Periodic Table of Elements
hydrogen
helium
```

### More On Classes

If the same attribute name occurs in both an instance and in a class, attribute lookup prioritizes the instance:

```pycon
>>> class Album:
...     title = 'After Midnight'
...     genre = 'Jazz'
... 
>>> a1 = Album()
>>> print(f'{a1.title}, {a1.genre}')
After Midnight, Jazz
>>> a2 = Album()
>>> a2.title = 'Satchmo The Great'
>>> print(f'{a2.title, a2.genre}')
('Satchmo The Great', 'Jazz')
>>> 
```

Data attributes may be referenced by methods, as well as by ordinary users (clients) of an object, i.e., classes cannot be used to implement pure [abstract data types](https://en.wikipedia.org/wiki/Abstract_data_type). Nothing in Python enables data hiding enforcement. Everything is based upon _convention_.

Clients should use data attributes with care, i.e., clients may disrupt invariants maintained by methods by altering their data attributes.

Clients may add data attributes to an instance object without affecting method validity, as long as name conflicts are avoided.

Often, the first argument of a method is `self`. This is only a convention and the name `self` does not have any special meaning in Python. However, not following convention may make your code less readable.

Function definitions do not need to be textually enclosed in the class definition to become an attribute of the class:

```python
def q1(self):
    print(
        'What the climate needs to avoid collapse is a contraction in '
        "humanity's use of resources;\nwhat our economic model demands to "
        'avoid collapse is unfettered expansion.'
    )


class Author:
    q1a = q1

    def q2(self):
        print(
            "Only one of these sets of rules can be changed, and it's not "
            'the laws of nature.'
        )

    q2a = q2


naomi_klein = Author()
naomi_klein.q1a()
naomi_klein.q2()
naomi_klein.q2a()
```

The resulting output would be:

```pycon
What the climate needs to avoid collapse is a contraction in humanity's use of resources;
what our economic model demands to avoid collapse is unfettered expansion.
Only one of these sets of rules can be changed, and it's not the laws of nature.
Only one of these sets of rules can be changed, and it's not the laws of nature.
```

`q1a`, `q2`, and `q2a` are all attributes of the `Author` class and refer to function objects. Consequently, they are all methods of instances of `Author`. Keep in mind, this practice may confuse readers of your program.

Methods may call methods by using method attributes of the `self` argument:

```python
class ArithOp:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def add(self):
        return self.x + self.y

    def dsp_sum(self):
        print(self.add())


arith = ArithOp(5, 10)
arith.dsp_sum()  # Displays 15
```

Methods can reference global names in the same way as regular functions. The global scope of a method is the module containing its definition. Usually, it is rare to use global data in a method, but there are legitimate reasons (e.g., to use functions/modules imported into the global scope, or to use functions/classes defined in the global scope).

Each _value_ is an object and has a class, also referred to as its _type_. The class is stored as `ex_obj.__class__`.

### Inheritance

The syntax for a derived class definition is:

```python
class ExDerivedClassName(ExBaseClassName):
    ...
```

The first task that Python has when creating an instance from a derived class is assigning values to all attributes in the base class:

```python
class ExDerivedClassName(ExBaseClassName):
    def __init__(self, ex_param_1, ex_param_2):
        super().__init__(ex_arg_1, ex_arg_2)
```

The [`super()`](https://docs.python.org/library/functions.html#super) function is a special function that helps Python make connections between the base and derived classes. It tells Python to call the `.__init__()` method from the derived class's base class, which gives a derived class instance all of the attributes and methods of its base class. The name _super_ comes from the convention of calling the base class a _superclass_ and the derived class a _subclass_.

`ExBaseClassName` must be defined in a scope containing the derived class definition.

Aside from `ExBaseClassName`, other arbitrary expressions are allowed. This can be helpful when the base class is defined in another module:

`class ExDerivedClassName(ex_mod_name.ExBaseClassName):`

The execution of a derived class definition proceeds in the same manner as for a base class. When the class object is constructed, the base class is remembered (this is used for resolving attribute references).

The derived class inherits every attribute and method from the base class, and is free to define new attributes and methods of its own. However, the attributes and methods of the derived class are not available to the base class.

If a requested attribute is not found in the class, the base class is searched. This rule is recursively applied if the base class itself is derived from another class.

Derived classes are instantiated like base classes, e.g., `ExDerivedClassName()` creates a new instance of the class.

Method references are resolved as follows:

- The corresponding class attribute is searched, descending down the chain of base classes, if necessary.
- The method reference is considered valid if the search yields a function object.

Derived classes can override base class methods, e.g., a method of a base class that calls another method defined in the same base class may end up calling a method of a derived class that overrides it.

An overriding method in a derived class may want to extend a base class method of the same name. To directly call a base class method, call `ExBaseClassName.ex_mtd_name(self, ex_args)` (`ExBaseClassName` must be accessible in the global scope).

An object instance can serve as an object's attribute:

```python
class Computer:
    def __init__(self, make, model, price):
        self.make = make
        self.model = model
        self.price = price


class Battery:
    def __init__(self, watt_hours=57):
        self.watt_hours = watt_hours


class Laptop(Computer):
    def __init__(self, make, model, price):
        super().__init__(make, model, price)
        self.battery = Battery()


thinkpad = Laptop('lenovo', 'thinkpad t14s gen 2', 1172)
print(thinkpad.battery.watt_hours)
# 57
print(type(thinkpad).mro())  # View method resolution order
# [<class '__main__.Laptop'>, <class '__main__.Computer'>, <class 'object'>]
```

Python has two built-in functions that deal with inheritance:

- [`isinstance()`](https://docs.python.org/library/functions.html#isinstance) checks an instance's type.
- [`issubclass()`](https://docs.python.org/library/functions.html#issubclass) checks class inheritance.

#### Multiple Inheritance

Python supports a type of multiple inheritance. A class definition with multiple base classes is:

```python
class ExDerivedClassName(ExBaseClass_1, ExBaseClass_2, ExBaseClass_3):
    ...
```

Think of the search for attributes inherited from a parent class as:

- depth-first
- left-to-right
- not searching twice in the same class where there is an overlap in the hierarchy

In the simplest case, if an attribute is not found in `ExDerivedClassName`, it is searched for in `ExBaseClass_1`, then (recursively) in the base classes of `ExBaseClass_1`, and if not found there, in `ExBaseClass_2`, and so on.

In reality, the method resolution order dynamically changes to support cooperative calls to [`super()`](https://docs.python.org/library/functions.html#super). This approach is referred to as _call-next-method_ in some multiple-inheritance languages.

Dynamic ordering is required because all cases of multiple inheritance demonstrate one or more [diamond relationships](https://en.wikipedia.org/wiki/Multiple_inheritance#The_diamond_problem), where at least one of the parent classes can be accessed through multiple paths from the bottommost class (e.g., all classes inherit from [`object`](https://docs.python.org/library/functions.html#object), so multiple inheritance will provide more than one path to `object`).

To prevent base classes from being accessed multiple times, the dynamic algorithm:

- linearizes the search order in a way that preserves the left-to-right ordering specified in each class
- only calls each parent once
- is monotonic (i.e., a class can be subclassed without affecting the precedence order of its parents)

### System-Defined Names

Some of Python's [reserved class of identifiers](https://docs.python.org/reference/lexical_analysis.html#reserved-classes-of-identifiers) are _system-defined names_ (informally known as _dunder_ names because they are denoted with double underscores).

Python's built-in operators and functions call [special method names](https://docs.python.org/reference/datamodel.html#special-method-names), i.e., each built-in function or operator has a corresponding special method. Note that not all special methods are tied to built-in operators/functions.

Special methods are used to emulate the behavior of Python built-ins and operators for user-defined objects. That is, if the behavior of a built-in function/operator is not defined in a user-defined class by the special method, a `TypeError` will occur. This practice is referred to as _overloading_.

<table>
  <caption>Special Method Names For Built-in Operators/Functions</caption>
  <thead>
    <tr>
      <th>Special Method Name</th>
      <th>Built-In Operator/Function, Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__abs__"><code>__abs__</code></a></td>
      <td><code>abs()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__add__"><code>__add__</code></a></td>
      <td><code>+</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__aenter__"><code>__aenter__</code></a></td>
      <td><code>async with</code> (enter an asynchronous context manager)</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__aexit__"><code>__aexit__</code></a></td>
      <td><code>async with</code> (exit an asynchronous context manager)</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__aiter__"><code>__aiter__</code></a></td>
      <td><code>aiter()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__and__"><code>__and__</code></a></td>
      <td><code>&</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__anext__"><code>__anext__</code></a></td>
      <td><code>anext()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__await__"><code>__await__</code></a></td>
      <td><code>await</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__bool__"><code>__bool__</code></a></td>
      <td><code>bool()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__bytes__"><code>__bytes__</code></a></td>
      <td><code>bytes()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__call__"><code>__call__</code></a></td>
      <td><code>callable()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__ceil__"><code>__ceil__</code></a></td>
      <td><code>math.ceil()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__complex__"><code>__complex__</code></a></td>
      <td><code>complex()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__contains__"><code>__contains__</code></a></td>
      <td><code>in</code>, <code>not in</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__del__"><code>__del__</code></a></td>
      <td>Called when an object instance is about to be destroyed.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__delattr__"><code>__delattr__</code></a></td>
      <td>Delete an object attribute (<code>del obj.name</code>).</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__delete__"><code>__delete__</code></a></td>
      <td>Called to delete the attribute on an instance of the owner class.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__delitem__"><code>__delitem__</code></a></td>
      <td>Called to implement deletion of <code>self[key]</code> (<code>del obj.key</code>).</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__dir__"><code>__dir__</code></a></td>
      <td><code>dir()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__divmod__"><code>__divmod__</code></a></td>
      <td><code>divmod()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__enter__"><code>__enter__</code></a></td>
      <td><code>with</code> (enter context manager)</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__eq__"><code>__eq__</code></a></td>
      <td><code>==</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__exit__"><code>__exit__</code></a></td>
      <td><code>with</code> (exit context manager)</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__float__"><code>__float__</code></a></td>
      <td><code>float()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__floor__"><code>__floor__</code></a></td>
      <td><code>math.floor()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__floordiv__"><code>__floordiv__</code></a></td>
      <td><code>//</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__format__"><code>__format__</code></a></td>
      <td><code>format()</code>, <code>str.format()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__ge__"><code>__ge__</code></a></td>
      <td><code>>=</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__get__"><code>__get__</code></a></td>
      <td>Called to get the attribute of the owner class (class attribute access) or of an instance of that class (instance attribute access).</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__getattr__"><code>__getattr__</code></a></td>
      <td>Called when the default attribute access fails with an <code>AttributeError</code>.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__getattribute__"><code>__getattribute__</code></a></td>
      <td>Called unconditionally to implement attribute accesses for instances of the class.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__getitem__"><code>__getitem__</code></a></td>
      <td>Called to implement evaluation of <code>self[key]</code>.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__gt__"><code>__gt__</code></a></td>
      <td><code>></code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__hash__"><code>__hash__</code></a></td>
      <td><code>hash()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__iadd__"><code>__iadd__</code></a></td>
      <td><code>+=</code>, <code>operator.iadd()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__iand__"><code>__iand__</code></a></td>
      <td><code>&=</code>, <code>operator.iand()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__ifloordiv__"><code>__ifloordiv__</code></a></td>
      <td><code>//=</code>, <code>operator.ifloordiv()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__ilshift__"><code>__ilshift__</code><a></td>
      <td><code><<=</code>, <code>operator.ilshift()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__imatmul__"><code>__imatmul__</code></a></td>
      <td><code>@=</code>, <code>operator.imatmul()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__imod__"><code>__imod__</code></a></td>
      <td><code>%=</code>, <code>operator.imod()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/functions.html#import__"><code>__import__</code></a></td>
      <td>Import a library by name.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__imul__"><code>__imul__</code></a></td>
      <td><code>*=</code>, <code>operator.imul()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__index__"><code>__index__</code></a></td>
      <td>Called to implement <code>operator.index()</code> and whenever Python needs to losslessly convert a numeric object to an integer object.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__init__"><code>__init__</code></a></td>
      <td>Called after the instance has been created (by <code>.__new__()</code>), but before it is returned to the caller.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__init_subclass__"><code>__init_subclass__</code></a></td>
      <td>Called whenever the containing class is subclassed.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#class.__instancecheck__"><code>__instancecheck__</code></a></td>
      <td><code>isinstance()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__int__"><code>__int__</code></a></td>
      <td><code>int()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__invert__"><code>__invert__</code></a></td>
      <td><code>~</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__ior__"><code>__ior__</code></a></td>
      <td><code>|=</code>, <code>operator.ior()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__ipow__"><code>__ipow__</code></a></td>
      <td><code>**=</code>, <code>operator.ipow()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__irshift__"><code>__irshift__</code></a></td>
      <td><code>>>=</code>, <code>operator.irshift()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__isub__"><code>__isub__</code></a></td>
      <td><code>-=</code>, <code>operator.isub()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__iter__"><code>__iter__</code></a></td>
      <td>Called when an iterator is required for a container (<code>iter(obj)</code>, <code>for ... in obj</code>).</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__itruediv__"><code>__itruediv__</code></a></td>
      <td><code>/=</code>, <code>operator.itruediv()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__ixor__"><code>__ixor__</code></a></td>
      <td><code>^=</code>, <code>operator.ixor()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__le__"><code>__le__</code></a></td>
      <td><code><=</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__lshift__"><code>__lshift__</code></a></td>
      <td><code><<</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__lt__"><code>__lt__</code></a></td>
      <td><code><</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__matmul__"><code>__matmul__</code></a></td>
      <td><code>@</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__missing__"><code>__missing__</code></a></td>
      <td>Called by <code>dict.__getitem__()</code> to implement <code>self[key]</code> for <code>dict</code> subclasses when <code>key</code> is not in the dictionary.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__mod__"><code>__mod__</code></a></td>
      <td><code>%</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__mul__"><code>__mul__</code></a></td>
      <td><code>*</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__ne__"><code>__ne__</code></a></td>
      <td><code>!=</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__neg__"><code>__neg__</code></a></td>
      <td><code>-</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__new__"><code>__new__</code></a></td>
      <td>Called to create a new instance of a class.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/stdtypes.html#iterator.__next__"><code>__next__</code></a></td>
      <td>Return the next item from the iterator.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__or__"><code>__or__</code></a></td>
      <td><code>|</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__pow__"><code>__pow__</code></a></td>
      <td><code>**</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__radd__"><code>__radd__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>+</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rand__"><code>__rand__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>&</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rdivmod__"><code>__rdivmod__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>divmod()</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__repr__"><code>__repr__</code></a></td>
      <td><code>repr()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__reversed__"><code>__reversed__</code></a></td>
      <td><code>reversed()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rfloordiv__"><code>__rfloordiv__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>//</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rlshift__"><code>__rlshift__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code><<</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rmatmul__"><code>__rmatmul__</code></a></td>
      <td>Called to implement the matmul operation <code>@</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rmod__"><code>__rmod__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>%</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rmul__"><code>__rmul__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>*</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__ror__"><code>__ror__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>|</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__round__"><code>__round__</code></a></td>
      <td><code>round()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rpow__"><code>__rpow__</code></a></td>
      <td>Called to implement the arithmetic multiplication operation <code>**</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rrshift__"><code>__rrshift__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>>></code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rshift__"><code>__rshift__</code></a></td>
      <td><code>>></code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rsub__"><code>__rsub__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>–</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rtruediv__"><code>__rtruediv__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>/</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__rxor__"><code>__rxor__</code></a></td>
      <td>Called to implement the binary arithmetic operation <code>^</code> with reflected (swapped) operands. Only called if the left operand does not support the corresponding operation and the operands are of different types.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__set__"><code>__set__</code></a></td>
      <td>Called to set the attribute on an instance of the owner class to a new value.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__set_name__"><code>__set_name__</code></a></td>
      <td>Automatically called at the time the owning class is created. The object has been assigned to the provided name in that class.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__setattr__"><code>__setattr__</code></a></td>
      <td><code>setattr()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__setitem__"><code>__setitem__</code></a></td>
      <td>Called to implement assignment to <code>self[key]</code>.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__str__"><code>__str__</code></a></td>
      <td><code>str()</code>, <code>format()</code>, <code>print()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__sub__"><code>__sub__</code></a></td>
      <td><code>-</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#class.__subclasscheck__"><code>__subclasscheck__</code></a></td>
      <td>Return <code>True</code> if the provided subclass should be considered a (direct or indirect) subclass of a class. If defined, called to implement <code>issubclass(subclass, class)</code>.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__truediv__"><code>__truediv__</code></a></td>
      <td><code>/</code>, where <code>2/3</code> is <code>.66</code>, rather than <code>0</code>.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__trunc__"><code>__trunc__</code></a></td>
      <td><code>math.trunc()</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/reference/datamodel.html#object.__xor__"><code>__xor__</code></a></td>
      <td><code>^</code></td>
    </tr>
  </tbody>
</table>

<table>
  <caption>Special Attribute Names</caption>
  <thead>
    <tr>
      <th>Special Attribute Name</th>
      <th>Type</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>__annotations__</code></td>
      <td><code>dict</code></td>
      <td>A dictionary with annotations.<br /><br />Present for user-defined functions, modules, and classes.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/stdtypes.html#class.__bases__"><code>__bases__</code></a></td>
      <td><code>tuple</code></td>
      <td>A tuple containing the base classes, in the order of their occurrence in the base class list.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/stdtypes.html#instance.__class__"><code>__class__</code></a></td>
      <td><code>type</code></td>
      <td>A class instance's class.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/stdtypes.html#object.__dict__"><code>__dict__</code></a></td>
      <td><code>dict</code></td>
      <td>The namespace.<br /><br />Present for user-defined functions, modules, classes, and class instances.</td>
    </tr>
    <tr>
      <td><code>__doc__</code></td>
      <td><code>str</code> or <code>None</code></td>
      <td>The documentation as a string. If no documentation is defined, <code>None</code>.<br /><br />Present for user-defined functions, instance methods, built-in functions, modules, and classes.</td>
    </tr>
    <tr>
      <td><code>__func__</code></td>
      <td><code>function</code></td>
      <td>The function object corresponding to the instance method.</td>
    </tr>
    <tr>
      <td><code>__module__</code></td>
      <td><code>str</code></td>
      <td>The name of the module where the object was defined, or <code>None</code>.<br /><br />Present for user-defined functions, instance methods, built-in functions, and classes.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/stdtypes.html#class.__mro__"><code>__mro__</code></a></td>
      <td><code>tuple</code></td>
      <td>A tuple of classes that are considered when looking for base classes during method resolution.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/stdtypes.html#definition.__name__"><code>__name__</code></a></td>
      <td><code>str</code></td>
      <td>The object's name.<br /><br />Present for user-defined functions, instance methods, built-in functions, modules, and classes.</td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/stdtypes.html#definition.__qualname__"><code>__qualname__</code></a></td>
      <td><code>str</code></td>
      <td>A user-defined function's <a href="https://docs.python.org/glossary.html#term-qualified-name">qualified name</a>.</td>
    </tr>
    <tr>
      <td><code>__self__</code></td>
      <td><code>object</code></td>
      <td>An object itself, or <code>None</code>.<br /><br />When a instance method object is created by retrieving a user-defined function object from a class via an instance, its <code>__self__</code> attribute is the instance.<br /><br />When an instance method object is created by retrieving a class method object from a class or instance, its <code>__self__</code> attribute is the class.<br /><br />For a built-in function, <code>__self__</code> is set to <code>None</code>.<br /><br />For a built-in method, <code>__self__</code> is the object on which the method is called.</td>
    </tr>
  </tbody>
</table>

### Private Variables

Truly _private_ instance variables that cannot be accessed except from inside of an object do not exist in Python, but there is a convention that any name prefixed with an underscore (`_`) should be treated as a private part of the API (whether it is a function, method, or data member). The private instance variable should be considered an implementation detail that is subject to change without notice.

A valid use case for class-private members is to avoid name clashes with names defined by subclasses. To address this, Python has limited support for _name mangling_.

Any identifier prefixed by at least two underscores (`__`) and suffixed with at most one underscore (e.g., `__ex_id`) is textually replaced with `_exClassName__ex_id`, where `exClassName` is the current class name with the leading underscore(s) removed. Mangling is done without regard to the syntactic position of the identifier, as long as it occurs inside the definition of a class.

Name mangling is useful for letting subclasses override methods without breaking intraclass method calls:

```python
class AuthorBase:
    def __init__(self, phrases):
        self.phrase_list = []
        self.__bld_quote(phrases)

    def bld_quote(self, phrases):
        for phrase in phrases:
            self.phrase_list.append(phrase)

    __bld_quote = bld_quote  # Private copy of original .bld_quote() method


class AuthorDerived(AuthorBase):
    # Override inherited .bld_quote() method,
    # but do not break .__init__()
    def bld_quote(self, indices, phrases):
        for pair in zip(indices, phrases):
            self.phrase_list.append(pair)


phrases = (
    'Growing up, I slowly had this process of realizing that \n',
    'all the things around me that people had told me were just \n',
    'the natural way things were, the way things always would be, \n',
    'they weren’t natural at all.'
)
aaron_swartz = AuthorBase(phrases)
print(f'Base class instance phrase list:\n{aaron_swartz.phrase_list}\n')


aaron_swartz_sub = AuthorDerived(phrases)
print(f'Derived class instance phrase list:\n{aaron_swartz_sub.phrase_list}\n')

indices = (0, 1, 2)
new_phrases = (
    '\n\nThey were things that could be changed, and they were things \n',
    'that, more importantly, were wrong and should change, and once \n',
    'I realized that, there was really no going back.'
)
aaron_swartz_sub.bld_quote(indices, new_phrases)
print('Derived class instance phrase list after calling updated '
      f'bld_quote():\n{aaron_swartz_sub.phrase_list}')

output = '\n'
for item in aaron_swartz_sub.phrase_list:
    if isinstance(item, str):
        output += item
    else:
        output += item[1]
print(output)
```

The resulting output would be:

```pycon
Base class instance phrase list:
['Growing up, I slowly had this process of realizing that \n', 'all the things around me that people had told me were just \n', 'the natural way things were, the way things always would be, \n', 'they weren’t natural at all.']

Derived class instance phrase list:
['Growing up, I slowly had this process of realizing that \n', 'all the things around me that people had told me were just \n', 'the natural way things were, the way things always would be, \n', 'they weren’t natural at all.']

Derived class instance phrase list after calling updated bld_quote():
['Growing up, I slowly had this process of realizing that \n', 'all the things around me that people had told me were just \n', 'the natural way things were, the way things always would be, \n', 'they weren’t natural at all.', (0, '\n\nThey were things that could be changed, and they were things \n'), (1, 'that, more importantly, were wrong and should change, and once \n'), (2, 'I realized that, there was really no going back.')]

Growing up, I slowly had this process of realizing that 
all the things around me that people had told me were just 
the natural way things were, the way things always would be, 
they weren’t natural at all.

They were things that could be changed, and they were things 
that, more importantly, were wrong and should change, and once 
I realized that, there was really no going back.
```

Even if `AuthorDerived` introduced a `__bld_quote` identifier, the above example would still work, since `__bld_quote` would be replaced with `_AuthorBase__bld_quote` in the `AuthorBase` class and `_AuthorDerived__bld_quote` in the `AuthorDerived` class, respectively.

Keep in mind, code passed to `exec()` or `eval()` does not consider the class name of the invoking class to be the current class. The same restriction applies to `getattr()`, `setattr()`, `delattr()`, and when directly referencing `__dict__.`

### Miscellaneous

An empty class can be a handy way to group named data items:

```python
class Author:
    pass


rachel = Author()

rachel.name = 'Rachel Carson'
rachel.yob = '1907'
rachel.quote = (
    'The question is whether any civilization can wage relentless war on '
    'life without destroying itself, and without losing the right to '
    'be called civilized.'
)
```

A class that imitates the methods of a specific abstract data type can be created and passed to a piece of code that expects the abstract data type. For example, a custom class with `.read()` and `.readline()` methods that takes input from a string buffer can be used with Python code that expects data from a file object.

Instance method objects have their own attributes. For example, `ex_inst.ex_mtd.__self__` is the instance object with the method `.ex_mtd()` and `ex_inst.ex_mtd.__func__` is the function object corresponding to the method.

### Iterators

Iterators pervade Python. For example, the [`for`](https://docs.python.org/reference/compound_stmts.html#for) statement works by using the [`iter()`](https://docs.python.org/library/functions.html#iter) function on a container object.

`iter()` returns an iterator object that defines a [`.__next__()`](https://docs.python.org/library/stdtypes.html#iterator.__next__) method, which accesses container elements one at a time. When there are no more elements, `.__next__()` raises a [`StopIteration`](https://docs.python.org/library/exceptions.html#StopIteration) exception that tells the `for` loop to terminate.

The `.__next__()` method can be called with the [`next()`](https://docs.python.org/library/functions.html#next) built-in function:

```pycon
>>> count = ('один', 'два', 'три')
>>> iterator = iter(count)
>>> iterator
<tuple_iterator object at 0x7fbaa3ccbbe0>
>>> next(iterator)
'один'
>>> next(iterator)
'два'
>>> next(iterator)
'три'
>>> next(iterator)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
>>> 
```

To add iterator behavior to a custom class, define an `.__iter__()` method that returns an object with a `.__next__()` method. If the class defines `.__next__()`, `.__iter__()` can return `self`:

```python
class Author:
    def __init__(self, phrases):
        self.phrases = phrases
        self.index = len(phrases)

    def __iter__(self):
        return self

    def __next__(self):
        if self.index == 0:
            raise StopIteration
        self.index -= 1
        return self.phrases[self.index]


phrases = (
    'Telling us that we need to evolve.',
    'A powerful message — spoken in the language of fires, floods, \n'
    'droughts, and extinctions — telling us that we need an entirely \n'
    'new economic model and a new way of sharing this planet.',
    'It is a civilizational wake-up call.',
    '''Climate change isn't an "issue" to add to the list of things to \n'''
    'worry about, next to health care and taxes.'
)
naomi_klein = Author(phrases)

print(iter(naomi_klein), end='\n\n')

for phrase in naomi_klein:
    print(phrase)
```

The resulting output would be:

```pycon
<__main__.Author object at 0x7f42c94ebfd0>

Climate change isn't an "issue" to add to the list of things to 
worry about, next to health care and taxes.
It is a civilizational wake-up call.
A powerful message — spoken in the language of fires, floods, 
droughts, and extinctions — telling us that we need an entirely 
new economic model and a new way of sharing this planet.
Telling us that we need to evolve.
```

### Generators

[Generators](https://docs.python.org/glossary.html#term-generator) are a simple, efficient way to create iterators. In Python, a generator produces a [lazy iterator](https://en.wikipedia.org/wiki/Lazy_evaluation). These are objects that you can loop over like a list, but (unlike lists) do not store their contents in memory.

Primarily, there are two ways to create generators:

1. Generator Functions
2. Generator Expressions

#### Generator Functions

A generator function is created like a regular function, but uses the [`yield`](https://docs.python.org/reference/simple_stmts.html#yield) statement (instead of `return`) when it needs to return data.

Each time [`next()`](https://docs.python.org/library/functions.html#next) is called on the generator object, it resumes where it left off (i.e., it remembers all of the data values and the last executed statement).

A `for` loop implicitly calls `next()` on the generator:

```pycon
>>> def quote_gen(quotes):
...     '''Build a generator from a collection of quotes.'''
...     for quote in quotes:
...         yield quote
... 
>>> quotes = (
...     'In the depth of winter, I finally learned that within me there lay an invincible summer.\n',
...     "I may not have been sure about what really did interest me, but I was absolutely sure about what didn't.\n",
...     'Seeking what is true is not seeking what is desirable.\n',
...     '- Albert Camus'
... )
>>> # Consume generator
>>> for quote in quote_gen(quotes):
...     print(quote)
... 
In the depth of winter, I finally learned that within me there lay an invincible summer.

I may not have been sure about what really did interest me, but I was absolutely sure about what didn't.

Seeking what is true is not seeking what is desirable.

- Albert Camus
>>> 
```

Alternatively, you can directly call `next()` on the generator object:

```pycon
>>> def inf_seq():
...     num = 0
...     while True:
...         yield num
...         num += 1
... 
>>> gen = inf_seq()
>>> next(gen)
0
>>> next(gen)
1
>>> next(gen)
2
>>> next(gen)
3
>>> 
```

`yield` indicates where a value is sent back to the caller. Unlike `return`, you do not exit the function afterward. Instead, the _state_ of the function is retained. In the example above, when `next()` is called on the generator object, the previously yielded variable `num` is incremented, and then yielded again.

Anything that can be done with a generator can also be done with a class-based iterator. However, with generators, `.__iter__()` and [`.__next__()`](https://docs.python.org/reference/expressions.html#generator.__next__) are automatically created.

Also, with generators, local variables and execution state are automatically saved between calls. Finally, when generators terminate, they automatically raise `StopIteration`.

#### Generator Expressions

Simple generators can be tersely coded using the following syntax:

```pycon
>>> words = ('Everything', 'is', 'optimized', 'for', 'capital,',
...          'until', 'it', 'runs', 'out', 'of', 'world', 'to', 'consume.',
...          '\n\n- Douglas Rushkoff')
>>> print(' '.join(word for word in words))
Everything is optimized for capital, until it runs out of world to consume. 

- Douglas Rushkoff
>>> 
```

Generator expressions are meant for situations where the generator is immediately used by an enclosing function.

In general, generator expressions are more compact and less versatile than generator functions, but are more memory-efficient than list comprehensions.

## The Standard Library

The [Python Standard Library](https://docs.python.org/3/library/index.html#the-python-standard-library) is a set of modules included with every Python installation. You can use any function or class in the standard library by including an `import` statement at the top of your file.

### Operating System Interface

#### `os`

The [`os`](https://docs.python.org/library/os.html#module-os) module provides functions for interacting with an operating system in a portable way:

```pycon
>>> import os
>>> os.getcwd()
'/home/dolores'
>>> os.chdir('/home/caleb')
>>> os.getcwd()
'/home/caleb'
>>> os.system("echo 'I would rather live in chaos than in a world controlled by you.'")
I would rather live in chaos than in a world controlled by you.
0
>>> 
```

[`os.listdir()`](https://docs.python.org/library/os.html#os.listdir) returns a list containing the names of the entries in the provided directory (by default, `.`, the current directory):

```pycon
>>> os.getcwd()
'/home/chris_hedges'
>>> os.chdir('/tmp/inverted/totalitarianism')
>>> os.listdir()
['wolin.txt', 'sheldon.txt']
>>> 
```

[`os.walk()`](https://docs.python.org/library/os.html#os.walk) can be used to walk a directory tree and is passed a single value (i.e., the directory path to traverse). Like how a `for` loop and `range()` function can be use to walk over a range of numbers, a `for` loop can be used with `os.walk()` to walk a directory tree.

For every iteration of the loop, `os.walk` returns three values:

1. A string of the current directory's name
2. A list of strings of the directories in the current directory
3. A list of strings of the files in the current directory

```python
import os

output = ''
for dir_name, sub_dirs, f_names in os.walk('/tmp'):
    output += (
        f'Current directory: {dir_name}\n'
        f'List of subdirectories: {sub_dirs}\n'
        f'List of files: {f_names}\n\n'
    )
print(output.rstrip())
```

[`os.makedirs()`](https://docs.python.org/library/os.html#os.makedirs) creates new directories, including any necessary intermediate directories required to ensure that the full path exists:

```pycon
>>> import os
>>> os.makedirs('/tmp/corporate/coup/detat')
>>> 
```

[`os.rmdir()`](https://docs.python.org/library/os.html#os.rmdir) deletes a provided directory. However, if the directory does not exist or is not empty, a [`FileNotFound`](https://docs.python.org/library/exceptions.html#FileNotFoundError) or [`OSError`](https://docs.python.org/library/exceptions.html#OSError), respectively, is raised:

```pycon
>>> os.rmdir('/tmp/inverted/totalitarianism')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
OSError: [Errno 39] Directory not empty: '/tmp/inverted/totalitarianism'
>>> 
```

[`os.unlink`](https://docs.python.org/library/os.html#os.unlink) deletes a file:

```pycon
>>> os.listdir()
['wolin.txt', 'sheldon.txt']
>>> os.unlink('wolin.txt')
>>> os.listdir()
['sheldon.txt']
>>> 
```

The [`os.path`](https://docs.python.org/library/os.path.html) module contains many helpful functions related to filenames and file paths:

```pycon
>>> os.getcwd()
'/tmp'
>>> dir_path = os.path.abspath('inverted')
>>> dir_path
'/tmp/inverted'
>>> os.path.isabs(dir_path)
True
>>> os.path.relpath('/tmp/inverted/totalitarianism', '/var/')
'../tmp/inverted/totalitarianism'
>>> os.path.dirname('/tmp/inverted/totalitarianism/sheldon.txt')
'/tmp/inverted/totalitarianism'
>>> os.path.basename('/tmp/inverted/totalitarianism/sheldon.txt')
'sheldon.txt'
>>> os.path.split('/tmp/inverted/totalitarianism/sheldon.txt')
('/tmp/inverted/totalitarianism', 'sheldon.txt')
>>> '/tmp/inverted/totalitarianism/sheldon.txt'.split(os.path.sep)
['', 'tmp', 'inverted', 'totalitarianism', 'sheldon.txt']
>>> 
```

`os.path` provides functions for finding the size of a file system object in bytes:

```pycon
>>> os.getcwd()
'/tmp'
>>> files = os.listdir('/tmp/corporate/coup/detat')
>>> files
['war.txt', 'chronic_instability.txt', 'joblessness.txt', 'financial_speculation.txt', 'food_insecurity.txt', 'constant_surveillance.txt']
>>> os.path.getsize('/tmp/corporate/coup/detat/war.txt')
255
>>> total_size = 0
>>> for file in files:
...     file_size = os.path.getsize(os.path.join('/tmp/corporate/coup/detat', file))
...     total_size += file_size
... 
>>> total_size
3023
>>> 
```

`os.path` also provides functions that determine if a given path exists, and whether it is a file or directory:

```pycon
>>> os.path.exists('/tmp/corporate/coup/detat')
True
>>> os.path.isfile('/tmp/corporate/coup/')
False
>>> os.path.isdir('/tmp/corporate/coup/detat')
True
>>> 
```

#### `shutil`

For file and directory management tasks, [`shutil`](https://docs.python.org/library/shutil.html#module-shutil) provides an easy-to-use high-level interface:

```pycon
>>> import shutil
>>> shutil.copy2('humans.csv', '/home/bernard/humans.csv')
'/home/bernard/humans.csv'
>>> shutil.move('humans.csv', 'hosts.csv')  # Rename file
'hosts.csv'
>>> shutil.move('hosts.csv', '/home/dolores/')  # Move file
'/home/dolores/hosts.csv'
>>> shutil.move('escaton', '/home/maeve/')  # Move directory
'/home/maeve/escaton'
>>> 
```

[`shutil.copytree()`](https://docs.python.org/library/shutil.html#shutil.copytree) copies an entire directory tree:

```pycon
>>> import shutil
>>> shutil.copytree('another_world', '/home/juliet/another_world')
'/home/juliet/another_world'
>>> 
```

[`shutil.rmtree()`](https://docs.python.org/library/shutil.html#shutil.rmtree) removes directory trees. Unlike, `os.rmdir()`, it can remove directories that are not empty:

```pycon
>>> import shutil
>>> shutil.rmtree('/home/robert/men_of_stone/')
>>> 
```

### `shelve`

Python variables can be saved to binary shelf files using the [`shelve`](https://docs.python.org/library/shelve.html#module-shelve) module.

```python
import shelve

jurisdictions = (
    'british virgin islands',
    'cayman islands',
    'bermuda',
    'netherlands',
    'switzerland',
    'luxembourg',
    'hong kong',
    'jersey',
    'singapore',
    'united arab emirates'
)

with shelve.open('tax_haven_index_2021') as shelf:
    shelf['tax_havens'] = jurisdictions
    # tax_haven_index_2021.db created in the current directory
```

After creating a shelf object, changes can be made to it as if it were a dictionary. When you are done, call `.close()` on the shelf object. The `.db` binary file that is created contains the data stored in your shelf object.

The shelf file can be reopened to retrieve its data:

```pycon
>>> import shelve
>>> shelf = shelve.open('tax_haven_index_2021')
>>> type(shelf)
<class 'shelve.DbfilenameShelf'>
>>> shelf['tax_havens']
('british virgin islands', 'cayman islands', 'bermuda', 'netherlands', 'switzerland', 'luxembourg', 'hong kong', 'jersey', 'singapore', 'united arab emirates')
>>> 
```

Like with dictionaries. shelf objects have `.keys()` and `.values()` methods that return the keys and values in the shelf, respectively:

```pycon
>>> import shelve
>>> shelf = shelve.open('tax_haven_index_2021')
>>> jurisdictions = [
...     jurisdiction
...     for value in shelf.values()
...     for jurisdiction in value
... ]
>>> for jurisdiction in jurisdictions:
...     print(jurisdiction)
... 
british virgin islands
cayman islands
bermuda
netherlands
switzerland
luxembourg
hong kong
jersey
singapore
united arab emirates
>>> shelf.close()
>>> 
```

The `shelve` module builds upon [`pickle`](https://docs.python.org/library/pickle.html), which can be used to serialize and de-serialize individual Python objects:

```python
# Enable binary protocols for serializing and de-serializing
# a Python object structure
import pickle as pkl

# Write pickled representation of object to file
with open('ex_f_path', 'wb') as f:
    pkl.dump(ex_obj, f)

# Read pickled representation of object from file
with open('ex_f_path', 'rb') as f:
    ex_obj = pkl.load(f)
```

### File Wildcards

The [`glob`](https://docs.python.org/library/glob.html#module-glob) module's `.glob()` method enables making file lists from directory wildcard searches:

```pycon
>>> import glob
>>> glob.glob('*.csv')
['humans.csv', 'hosts.csv', 'hybrids.csv']
>>> 
```

### Command Line Arguments

Command line arguments are stored in the `sys` module's `argv` attribute as a list:

```console
$ names.py 'william' 'maeve' 'robert'
```

```pycon
>>> import sys
>>> print(sys.argv)
['names.py', 'william', 'maeve', 'robert']
```

The [`argparse`](https://docs.python.org/library/argparse.html#module-argparse) module provides a sophisticated method to process command line arguments. For example, the following script uses `argparse` to process arguments, which allows it to determine how many beginning lines to extract from a file or files:

```python
import argparse

parser = argparse.ArgumentParser(
    prog='head',
    description='Display top lines from files.'
)
parser.add_argument('filenames', nargs='+')
parser.add_argument('--lines', '-l', type=int, default=10)
args = parser.parse_args()
```

After running `python3 head.py -l 3 hosts.csv hybrids.csv`, the script sets `args.lines` to `3` and `args.filenames` to `['hosts.csv', 'hybrids.csv']`, i.e., long option names become `args` attributes that store the options' arguments.

By default, options specified with argparse assume a single argument will be provided. To prevent this behavior, use the `action` parameter:

`parser.add_argument('-s', '--single', action='store_false')`

### Error Output Redirection and Program Termination

The `sys` module has attributes for `stdin`, `stdout`, and `stderr`:

```pycon
>>> sys.stderr.write('Error encountered. Try again.\n')
Error encountered. Try again.
30
>>> 
```

A direct way to terminate a script is to use `sys.exit()`, which raises the built-in [`SystemExit`](https://docs.python.org/3/library/exceptions.html#SystemExit) exception.

### String Pattern Matching

The [`re`](https://docs.python.org/library/re.html#module-re) module provides regular expression tools that enable advanced string processing:

```pycon
>>> import re
>>> quote = '''\
... He who has a why to live for can bear almost any how.
... 
... - Friedrich Nietzsche\
... '''
>>> pat = re.compile(r'\b(?:w|ho)[a-z]{1,2}')
>>> pat.findall(quote)
['who', 'why', 'how']
>>> quote = '''\
... You ain't gonna get justice with a badge, Will Reeves. You're gonna get it with that hood.
... 
... - June Reeves\
... '''
>>> pat = re.compile(r'(?<=- ).+')
>>> print(re.sub(pat, 'Damon Lindelof', quote))
You ain't gonna get justice with a badge, Will Reeves. You're gonna get it with that hood.

- Damon Lindelof
>>> 
```

If only a simple operation is required, string methods are preferred for their legibility:

```pycon
>>> quote = '''\
... The man of knowledge must be able not only to love his enemies but also to loathe his friends.
... 
... - Friedrich Nietzsche\
... '''
>>> print(quote.replace('loathe', 'hate'))
The man of knowledge must be able not only to love his enemies but also to hate his friends.

- Friedrich Nietzsche
>>> 
```

### Mathematics

The [`math`](https://docs.python.org/library/math.html#module-math) module provides access to underlying C library functions for floating point math:

```pycon
>>> import math
>>> math.cos(math.pi / 8)
0.9238795325112867
>>> math.log(4096, 2)
12.0
>>> 
```

The [`random`](https://docs.python.org/library/random.html#module-random) module provides tools for making random selections:

```pycon
>>> import random
>>> random.choice(('jean', 'barbara', 'james'))
'jean'
>>> random.sample(range(10), 2)  # Sampling without replacement
[2, 9]
>>> random.random()  # Random float between 0.0 and 1.0
0.7702300523115606
>>> random.randrange(100)  # Random integer from provided range
91
>>> 
```

The [`statistics`](https://docs.python.org/library/statistics.html#module-statistics) module calculates basic statistical properties of numeric data:

```pycon
>>> import statistics
>>> data = [round(random.random(), 2) for i in range(10)]
>>> data
[0.44, 0.91, 0.99, 0.9, 0.67, 0.37, 0.6, 0.47, 0.22, 0.14]
>>> statistics.mean(data)
0.571
>>> statistics.median(data)
0.5349999999999999
>>> statistics.variance(data)
0.08734333333333334
>>> 
```

### Internet Access

There are numerous modules for accessing Internet and processing protocols. [`urllib.request`](https://docs.python.org/library/urllib.request.html#module-urllib.request) can retrieve data from URLs and [`smtplib`](https://docs.python.org/library/smtplib.html#module-smtplib) can send emails:

```pycon
>>> from urllib.request import urlopen
>>> url = 'https://www.gnu.org/philosophy/philosophy.html'
>>> with urlopen(url) as rsp:
...     for line in rsp:
...             line = line.decode()
...             if line.find('four essential freedoms') != -1:
...                     print(line.rstrip())
... 
the <a href="/philosophy/free-sw.html">four essential freedoms</a>:
>>> 
```

If you have a mail server running on `localhost`, you can do something like this:

```pycon
>>> import smtplib
>>> server = smtplib.SMTP('localhost')
>>> sender = 'cd@friedrich.org'
>>> receiver = 'fp@schubert.org'
>>> msg = '''\
... Subject: Guten Abend
... 
... Diese Nachricht wird von Python gesendet.\
... '''
>>> server.sendmail(sender, receiver, msg)
>>> server.quit()
>>> 
```

To authenticate with an external mail server to send a plain text email where SSL is required from the beginning of the connection, you can do something like:

```python
# Enable the base class for the email object model
from email.message import EmailMessage
# Enable an SMTP client session object
import smtplib

# pip install keyring
# Enable storing and accessing of passwords safely
import keyring as kr

# Define credential information to retrieve from keyring
SRV_NAME = 'ex_srv_name'
USERNAME = 'ex_username'

creds = kr.get_credential(SRV_NAME, USERNAME)

SERVER = 'ex_smtp_server'  # e.g., smtp.mail.com
PORT = ex_port  # e.g., 465

SENDER = 'ex_sender_addr'
RECIPIENTS = [
    'ex_recipient_addr_1',
    'ex_recipient_addr_2'
]
SUBJECT = 'ex_subject'
BODY = 'ex_body'

msg = EmailMessage()
msg['From'] = SENDER
msg['To'] = RECIPIENTS
msg['Subject'] = SUBJECT
msg.set_content(BODY)

msg_text = msg.as_string()

with smtplib.SMTP_SSL(SERVER, PORT) as server:
    server.login(creds.username, creds.password)
    server.sendmail(SENDER, RECIPIENTS, msg_text)
```

If using SSL from the beginning of the connection is not required, you can modify the above example like so:

```python
# Enable the base class for the email object model
from email.message import EmailMessage
# Enable an SMTP client session object
import smtplib
# Enable access to Transport Layer Security encryption
# and peer authentication facilities
import ssl


# pip install keyring
# Enable storing and accessing of passwords safely
import keyring as kr

# Define credential information to retrieve from keyring
SRV_NAME = 'ex_srv_name'
USERNAME = 'ex_username'

creds = kr.get_credential(SRV_NAME, USERNAME)

SERVER = 'ex_smtp_server'  # e.g., smtp.mail.com
PORT = ex_port  # e.g., 587

SENDER = 'ex_sender_addr'
RECIPIENTS = [
    'ex_recipient_addr_1',
    'ex_recipient_addr_2'
]
SUBJECT = 'ex_subject'
BODY = 'ex_body'

msg = EmailMessage()
msg['From'] = SENDER
msg['To'] = RECIPIENTS
msg['Subject'] = SUBJECT
msg.set_content(BODY)

msg_text = msg.as_string()

# Create a secure SSL context
context = ssl.create_default_context()

with smtplib.SMTP(SERVER, PORT) as server:
    # Tell server to communicate with TLS encryption
    server.starttls(context=context)

    server.login(creds.username, creds.password)
    server.sendmail(SENDER, RECIPIENTS, msg_text)
```

A plain text email with attachments requires additional configuration:

```python
from email import encoders  # Enable convenient encoders
# Enable the base class for all the MIME-specific subclasses of Message
from email.mime.base import MIMEBase
# Enable an intermediate base class for MIME messages that are multipart
from email.mime.multipart import MIMEMultipart
# Enable a class that is used to create MIME objects of major type text
from email.mime.text import MIMEText
# Enable an SMTP client session object
import smtplib

# pip install keyring
# Enable storing and accessing of passwords safely
import keyring as kr

# Define credential information to retrieve from keyring
SRV_NAME = 'ex_srv_name'
USERNAME = 'ex_username'

creds = kr.get_credential(SRV_NAME, USERNAME)

SERVER = 'ex_smtp_server'  # e.g., smtp.mail.com
PORT = ex_port  # e.g., 465

SENDER = 'ex_sender_addr'
RECIPIENTS = ','.join(
    [
        'ex_recipient_addr_1',
        'ex_recipient_addr_2'
    ]
)
SUBJECT = 'ex_subject'
BODY = 'ex_body'

# Create a multipart email and set headers
msg = MIMEMultipart()
msg['From'] = SENDER
msg['To'] = RECIPIENTS
msg['Subject'] = SUBJECT
msg.attach(MIMEText(BODY, 'plain'))

# Define attachment paths
f_path_1 = f'attachment_path_1'
f_path_2 = f'attachment_path_2'

with open(f_path_1, 'rb') as attachment:
    # Add file as application/octet-stream
    part = MIMEBase('application', 'octet-stream')
    part.set_payload(attachment.read())
encoders.encode_base64(part)  # Encode file in ASCII characters
# Add header as key/value pair to attachment part
part.add_header(
    'Content-Disposition',
    f'attachment; filename={os.path.basename(f_path_1)}'
)
msg.attach(part)  # Add attachment to email

with open(f_path_2, 'rb') as attachment:
    part = MIMEBase('application', 'octet-stream')
    part.set_payload(attachment.read())
encoders.encode_base64(part)
part.add_header(
    'Content-Disposition',
    f'attachment; filename={os.path.basename(f_path_2)}'
)
msg.attach(part)

msg_text = msg.as_string()  # Convert email to string

with smtplib.SMTP_SSL(SERVER, PORT) as server:
    server.login(creds.username, creds.password)
    server.sendmail(SENDER, RECIPIENTS, msg_text)
```

### Dates and Times

The [`datetime`](https://docs.python.org/library/datetime.html#module-datetime) module provides classes for manipulating dates and times. Date and time arithmetic is supported, but `datetime` focuses on efficient member extraction for output formatting and manipulation.

`datetime` also supports timezone-aware objects.

```pycon
>>> from datetime import date
>>> now = date.today()
>>> now
datetime.date(2022, 8, 7)
>>> now.strftime('%m/%d/%Y is a %A in %B.')
'08/07/2022 is a Sunday in August.'
>>> nt_bday = date(1856, 7, 10)
>>> diff = now - nt_bday
>>> diff.days
60658
>>> 
```

The `datetime` module has its own [`datetime`](https://docs.python.org/library/datetime.html#datetime.datetime) data type. `datetime` values represent a specific moment in time:

```pycon
>>> from datetime import datetime as dt
>>> manuel_esteban_paez_teran = dt(2023, 1, 18, 9, 0, 0)
>>> manuel_esteban_paez_teran.year, manuel_esteban_paez_teran.month, manuel_esteban_paez_teran.day
(2023, 1, 18)
>>> manuel_esteban_paez_teran.hour, manuel_esteban_paez_teran.minute, manuel_esteban_paez_teran.second
(9, 0, 0)
>>> 
```

`datetime` objects can be compared using comparison operators to find out which one precedes the other. The later `datetime` object is the _greater_ value:

```pycon
>>> from datetime import datetime as dt
>>> now = dt.now()
>>> zuccotti_park = dt(2011, 11, 15, 1, 0)
>>> zuccotti_park != now
True
>>> zuccotti_park > now
False
>>> diff = now - zuccotti_park
>>> diff.days
4120
>>> 
```

[`date`](https://docs.python.org/library/datetime.html#datetime.date.strftime), [`datetime`](https://docs.python.org/library/datetime.html#datetime.datetime.strftime), and [`time`](https://docs.python.org/library/datetime.html#datetime.time.strftime) objects support a `.strftime()` method to create a string representing the time under the control of an explicit format string.

The following table lists [format codes](https://docs.python.org/library/datetime.html#strftime-and-strptime-format-codes) required by the 1989 C standard, which work on all platforms with a standard C implementation.

<table>
  <caption><code>.strftime()</code> and <code>.strptime()</code> Format Codes</caption>
  <thead>
    <tr>
      <th>Directive</th>
      <th>Meaning</th>
      <th>Example</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>%a</code></td>
      <td>Weekday as locale's abbreviate name.</td>
      <td>Sun, Mon, ..., Sat (en_US);<br />So, Mo, ..., Sa (de_DE)</td>
    </tr>
    <tr>
      <td><code>%A</code></td>
      <td>Weekday as locale's full name.</td>
      <td>Sunday, Monday, ..., Saturday (en_US);<br />Sonntag, Montag, ..., Samstag (de_DE)</td>
    </tr>
    <tr>
      <td><code>%w</code></td>
      <td>Weekday as a decimal number, where 0 is Sunday and 6 is Saturday.</td>
      <td>0, 1, ..., 6</td>
    </tr>
    <tr>
      <td><code>%d</code></td>
      <td>Day of the month as a zero-padded decimal number.</td>
      <td>01, 02, ..., 31</td>
    </tr>
    <tr>
      <td><code>%b</code></td>
      <td>Month as locale’s abbreviated name.</td>
      <td>Jan, Feb, ..., Dec (en_US);<br />Jan, Feb, ..., Dez (de_DE)</td>
    </tr>
    <tr>
      <td><code>%B</code></td>
      <td>Month as locale’s full name.</td>
      <td>January, February, ..., December (en_US);<br />Januar, Februar, ..., Dezember (de_DE)</td>
    </tr>
    <tr>
      <td><code>%m</code></td>
      <td>Month as a zero-padded decimal number.</td>
      <td>01, 02, ..., 12</td>
    </tr>
    <tr>
      <td><code>%y</code></td>
      <td>Year without century as a zero-padded decimal number.</td>
      <td>00, 01, ..., 99</td>
    </tr>
    <tr>
      <td><code>%Y</code></td>
      <td>Year with century as a decimal number.</td>
      <td>0001, 0002, ..., 2013, 2014, ..., 9998, 9999</td>
    </tr>
    <tr>
      <td><code>%H</code></td>
      <td>Hour (24-hour clock) as a zero-padded decimal number.</td>
      <td>00, 01, ..., 23</td>
    </tr>
    <tr>
      <td><code>%I</code></td>
      <td>Hour (12-hour clock) as a zero-padded decimal number.</td>
      <td>01, 02, ..., 12</td>
    </tr>
    <tr>
      <td><code>%p</code></td>
      <td>Locale’s equivalent of either AM or PM.</td>
      <td>AM, PM (en_US);<br />am, pm (de_DE)</td>
    </tr>
    <tr>
      <td><code>%M</code></td>
      <td>Minute as a zero-padded decimal number.</td>
      <td>00, 01, ..., 59</td>
    </tr>
    <tr>
      <td><code>%S</code></td>
      <td>Second as a zero-padded decimal number.</td>
      <td>00, 01, ..., 59</td>
    </tr>
    <tr>
      <td><code>%f</code></td>
      <td>Microsecond as a decimal number, zero-padded to 6 digits.</td>
      <td>000000, 000001, ..., 999999</td>
    </tr>
    <tr>
      <td><code>%z</code></td>
      <td>UTC offset in the form <code>±HHMM[SS[.ffffff]]</code> (empty string if the object is naive).</td>
      <td>(empty), +0000, -0400, +1030, +063415, -030712.345216</td>
    </tr>
    <tr>
      <td><code>%Z</code></td>
      <td>Time zone name (empty string if the object is naive).</td>
      <td>(empty), UTC, GMT</td>
    </tr>
    <tr>
      <td><code>%j</code></td>
      <td>Day of the year as a zero-padded decimal number.</td>
      <td>001, 002, ..., 366</td>
    </tr>
    <tr>
      <td><code>%U</code></td>
      <td>Week number of the year (Sunday as the first day of the week) as a zero-padded decimal number. All days in a new year preceding the first Sunday are considered to be in week 0.</td>
      <td>00, 01, ..., 53</td>
    </tr>
    <tr>
      <td><code>%W</code></td>
      <td>Week number of the year (Monday as the first day of the week) as a zero-padded decimal number. All days in a new year preceding the first Monday are considered to be in week 0.</td>
      <td>00, 01, ..., 53</td>
    </tr>
    <tr>
      <td><code>%c</code></td>
      <td>Locale’s appropriate date and time representation.</td>
      <td>Tue Aug 16 21:30:00 1988 (en_US);<br />Di 16 Aug 21:30:00 1988 (de_DE)</td>
    </tr>
    <tr>
      <td><code>%x</code></td>
      <td>Locale’s appropriate date representation.</td>
      <td>08/16/88 (None);<br />08/16/1988 (en_US);<br />16.08.1988 (de_DE)</td>
    </tr>
    <tr>
      <td><code>%X</code></td>
      <td>Locale’s appropriate time representation.</td>
      <td>21:30:00 (en_US);<br />21:30:00 (de_DE)</td>
    </tr>
    <tr>
      <td><code>%%</code></td>
      <td>A literal <code>%</code> character.</td>
      <td>%</td>
    </tr>
  </tbody>
</table>

Additional directives not required by the 1989 C standard are included for convenience.

<table>
  <caption>Convenience <code>.strftime()</code> and <code>.strptime()</code> Format Codes</caption>
  <thead>
    <tr>
      <th>Directive</th>
      <th>Meaning</th>
      <th>Example</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>%G</code></td>
      <td>ISO 8601 year with century representing the year that contains the greater part of the ISO week (<code>%V</code>).</td>
      <td>0001, 0002, ..., 2013, 2014, ..., 9998, 9999</td>
    </tr>
    <tr>
      <td><code>%u</code></td>
      <td>ISO 8601 weekday as a decimal number where 1 is Monday.</td>
      <td>1, 2, ..., 7</td>
    </tr>
    <tr>
      <td><code>%V</code></td>
      <td>ISO 8601 week as a decimal number with Monday as the first day of the week. Week 01 is the week containing Jan 4.</td>
      <td>01, 02, ..., 53</td>
    </tr>
  </tbody>
</table>

```pycon
>>> from datetime import datetime as dt
>>> congress = dt(2012, 1, 17, 9, 0)
>>> congress.strftime('%Y/%m/%d %H:%M')
'2012/01/17 09:00'
>>> congress.strftime('%I:%M %p')
'09:00 AM'
>>> congress.strftime("%B of '%y")
"January of '12"
>>> 
```

[`datetime.datetime.strptime()`](https://docs.python.org/library/datetime.html#datetime.datetime.strptime) converts a date string into a `datetime` object:

```pycon
>>> from datetime import datetime as dt
>>> eric_garner = (
...     ('July 17, 2014', '%B %d, %Y'),
...     ('2014/07/17 15:30', '%Y/%m/%d %H:%M')
... )
>>> dt.strptime(eric_garner[0][0], eric_garner[0][1])
datetime.datetime(2014, 7, 17, 0, 0)
>>> dt.strptime(eric_garner[1][0], eric_garner[1][1])
datetime.datetime(2014, 7, 17, 15, 30)
>>> 
```

### Data Compression

Common data compression and archiving formats are directly support via several modules:

- [`zlib`](https://docs.python.org/library/zlib.html#module-zlib)
- [`gzip`](https://docs.python.org/library/gzip.html#module-gzip)
- [`bz2`](https://docs.python.org/library/bz2.html#module-bz2)
- [`lzma`](https://docs.python.org/library/lzma.html#module-lzma)
- [`tarfile`](https://docs.python.org/library/tarfile.html#module-tarfile)
- [`zipfile`](https://docs.python.org/library/zipfile.html#module-zipfile)

```pycon
>>> quote = b'''\
... When it comes to paying contractors, the sky is the limit;
... when it comes to financing the basic functions of the state,
... the coffers are empty.
... 
... - Naomi Klein\
... '''
>>> len(quote)
157
>>> comp_q = zlib.compress(quote)
>>> len(comp_q)
119
>>> zlib.decompress(comp_q)
b'When it comes to paying contractors, the sky is the limit;\nwhen it comes to financing the basic functions of the state,\nthe coffers are empty.\n\n- Naomi Klein'
>>> zlib.crc32(quote)
1698538711
>>> zlib.crc32(comp_q)
2335973812
>>>
```

The `zipfile` module can be used to read, extract from, create, and add to ZIP archives. Conceptually, [`ZipFile`](https://docs.python.org/library/zipfile.html#zipfile-objects) objects are similar to `File` objects returned by the `open()` function, in that they are objects through which a program interacts with a file.

[`zipfile.ZipFile()`](https://docs.python.org/library/zipfile.html#zipfile-objects) reads from a `ZipFile` object:

```pycon
>>> import zipfile
>>> zf = zipfile.ZipFile('xu_lizhi.zip')
>>> zf.namelist()
['on_my_deathbed.txt', 'rented_room.txt', 'the_last_graveyard.txt']
>>> rr_info = zf.getinfo('rented_room.txt')
>>> rr_info.file_size
317
>>> rr_info.compress_size
192
>>> percent_diff = (rr_info.file_size - rr_info.compress_size) / rr_info.file_size
>>> f'Compression resulted in a {percent_diff:.0%} decrease.'
'Compression resulted in a 39% decrease.'
>>> zf.close()
>>> 
```

[`ZipFile.namelist()`](https://docs.python.org/library/zipfile.html#zipfile.ZipFile.namelist) returns a list of archive members by name. These strings can be passed to [`ZipFile.getinfo()`](https://docs.python.org/library/zipfile.html#zipfile.ZipFile.getinfo) to return a [`ZipInfo`](https://docs.python.org/library/zipfile.html#zipinfo-objects) object about that particular file system object.

`ZipInfo` objects have their own attributes, e.g., `.file_size` and `.compress_size` in bytes, which hold integers of the original size and compressed size, respectively.

[`ZipFile.extractall()`](https://docs.python.org/library/zipfile.html#zipfile.ZipFile.extractall) extracts all items from a ZIP archive into the current directory:

```pycon
>>> import zipfile
>>> zf = zipfile.ZipFile('xu_lizhi.zip')
>>> zf.extractall('/tmp/')  # Extract to /tmp/ directory, instead
>>> zf.close()
>>> 
```

[`ZipFile.extract()`](https://docs.python.org/library/zipfile.html#zipfile.ZipFile.extract) extracts a single item from an archive:

```pycon
>>> import zipfile
>>> zf = zipfile.ZipFile('xu_lizhi.zip')
>>> zf.extract('the_last_graveyard.txt', '/tmp/')
'/tmp/the_last_graveyard.txt'
>>> zf.close()
>>> 
```

To create a ZIP archive, utilize `zipfile.ZipFile()`'s write mode (`w`):

```pycon
>>> import zipfile
>>> zf = zipfile.ZipFile('poems.zip', 'w')
>>> zf.write('my_life_s_journey.txt')
>>> zf.namelist()
['my_life_s_journey.txt']
>>> zf.close()
>>> 
```

To add file system objects to a ZIP archive, open it in append mode (`a`):

```pycon
>>> import zipfile
>>> zf = zipfile.ZipFile('poems.zip', 'a')
>>> zf.write('a_moon_made_of_iron.txt')
>>> zf.namelist()
['my_life_s_journey.txt', 'a_moon_made_of_iron.txt']
>>> zf.close()
>>> 
```

### Performance Measurement

Python provides measurement tools for gauging how performant code is.

For example, the [`timeit`](https://docs.python.org/library/timeit.html#module-timeit) module can help demonstrate performance differences:

```pycon
>>> from timeit import Timer
>>> Timer('carl_sagan = q1 + q2', "q1, q2 = 'To read is ', 'to voyage through time.'").timeit()
0.06277312799647916
>>> Timer("carl_sagan = f'{q1}{q2}'", "q1, q2 = 'To read is ', 'to voyage through time.'").timeit()
0.08103058999404311
>>> 
```

An alternative way to measure the [wall time](https://en.wikipedia.org/wiki/Elapsed_real_time) of a code block is to use the `DateTime` module:

```python
import datetime as dt

start = dt.datetime.now()

jeff_goodell = (
    'You can argue that Texas has done this to itself. '
    'The planet is getting hotter because of the burning of fossil fuels. '
    'This is a simple truth, as clear as the moon in the night sky. '
    'No state has profited more from fossil fuels than Texas.'
)

for i in range(20000):
    len(jeff_goodell)**i * 27**i

end = dt.datetime.now()

secs_elapsed = (end - start).total_seconds()
mins_elapsed = secs_elapsed / 60
hours_elapsed = mins_elapsed / 60

print(
    f'Execution time: {dt.timedelta(seconds=secs_elapsed)}',
    f'Execution time: {hours_elapsed:.2f} (hours)',
    f'Execution time: {mins_elapsed:.4f} (minutes)',
    f'Execution time: {secs_elapsed:.6f} (seconds)',
    sep='\n'
)
```

The [`profile`](https://docs.python.org/library/profile.html#module-profile) and [`pstats`](https://docs.python.org/library/profile.html#module-pstats) modules provide tools for identifying time critical sections in larger code blocks.

### Quality Control

Testing proves that your code works as it is supposed to in response to all of the input types it is designed to receive.

#### `doctest`

The [`doctest`](https://docs.python.org/library/doctest.html#module-doctest) module provides tools for scanning a module and validating tests embedded in docstrings. A docstring test can be as simple as a typical call, along with its result:

```python
import doctest


def dsp_quote(quote):
    '''
    Display a quote.

    >>> the_broken_earth = 'Winter, Spring, Summer, Fall; Death is the fifth, and master of all.'
    >>> dsp_quote(the_broken_earth)
    Winter, Spring, Summer, Fall; Death is the fifth, and master of all.
    '''
    if isinstance(quote, str):
        print(quote)
    else:
        raise TypeError('A non-string object type was provided.')


doctest.testmod()  # Automatically validate embedded tests
```

#### `unittest`

The [`unittest`](https://docs.python.org/library/unittest.html#module-unittest) module allows more comprehensive sets of tests to be maintained in a separate file.

A _unit test_ verifies that one aspect of a function's behavior is correct.

A _test case_ is a collection of unit tests that together prove that a function behaves as it is supposed to, within the full range of situations that it is expected to handle. A good test case considers all possible kinds of input a function could receive and includes tests to represent each of these situations.

A test case with _full coverage_ includes a full range of unit tests covering all of possible ways that you can use a function. Often, it is good enough to write tests for critical functionality and then aim for full coverage if the project starts sees widespread use.

To test case a function, import the `unittest` module and the function that you want to test. Then, create a class that inherits from [`unittest.TestCase`](https://docs.python.org/library/unittest.html#unittest.TestCase) (i.e., a subclass) and write a series of methods to test different aspects of your function's behavior.

For example, assume a file `concat.py`, which contains the `concat_strs()` function:

```python
def concat_strs(strs):
    '''Concatenate a collection of strings.'''
    return ' '.join(strs)
```

Then, in the same directory, assume a separate `test_concat.py` file with a unit test:

```python
#!/usr/bin/env python3
import unittest as ut

from concat import concat_strs


class TestConcat(ut.TestCase):
    def test_concat_strs(self):
        '''Test return of correct concatenated string.'''
        strs = ('Have you ever heard of', 'something called a moon?')
        self.assertEqual(concat_strs(strs), f'{strs[0]} {strs[1]}')


if __name__ == '__main__':
    ut.main()
```

At the command line, run `test_concat.py` as a script to confirm that the test passes:

```console
$ python3 'test_concat.py'
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
```

The `TestConcat` class is created to contain a series of unit tests for the `concat_strs()` function. The class can have any name, but it is best to call it something related to the function to test and to use the word _Test_ in the name. This class must inherit from the `unittest.TestCase` class, so that Python knows how to run the tests that you write.

Any methods inside of `TestConcat` that start with `.test_` are automatically run when `test_concat.py` is run. Inside of `test_concat_strs()`, the `.assertEqual()` method is passed the `concat_strs()` function call (which returns the value to test) and the known correct value.

It is fine to have long method names in your `unittest.TestCase` classes. They need to be descriptive, so that you can make sense of the output when your tests fail. Python automatically calls them, so you will never have to write code that calls these methods.

Calling [`unittest.main()`](https://docs.python.org/library/unittest.html#unittest.main) tells Python to run the tests in `test_concat.py`. The dot on the first line of the console output denotes that a single test passed. Then, the number of tests and test duration is shown. Finally, the `OK` line denotes that all unit tests in the test case passed.

A failed test looks something like this:

```console
E
======================================================================
ERROR: test_concat_strs (__main__.TestConcat)
Test return of correct concatenated string.
----------------------------------------------------------------------
Traceback (most recent call last):
  File "test_concat.py", line 10, in test_concat_strs
    self.assertEqual(concat_strs(strs), f'{strs[0]} {strs[1]}')
  File "concat.py", line 3, in concat_strs
    return ' '.join(strs)
TypeError: sequence item 1: expected str instance, int found

----------------------------------------------------------------------
Ran 1 test in 0.000s

FAILED (errors=1)
```

The single `E` in the output indicates that one unit test in the test case resulted in an error. Next, we can see that the source of the error is `test_concat_strs()` in `TestConcat`. Further down, a standard traceback is provided, which reports that a `TypeError` occurred when `concat_strs()` tried to join two strings, but received a string and an integer, instead.

Then, we see that one test was run and that, overall, the test case failed with a single error.

When a test case is running, Python prints one character for each unit test as it is completed. A passing test prints a dot (`.`), a test that results in an error prints an `E`, and a test that results in a failed assertion prints an `F`.

If a test case takes a long time to run because it contains many unit tests, you can watch these results to get a sense of how many tests are passing.

Testing functions that _print their output_ requires additional work. This example uses multiple unit tests for a function that prints a provided quote:

```python
# quotes.py file


def dsp_quote(quote):
    '''Display a quote.'''
    if isinstance(quote, str):
        print(quote)
    else:
        raise TypeError('A non-string object type was provided.')
```

```python
#!/usr/bin/env python3
# test_quotes.py file with unit tests
from unittest.mock import patch
from io import StringIO
import unittest as ut

from quotes import dsp_quote


class TestQuotes(ut.TestCase):
    def test_quote(self):
        '''Test printing of correct string.'''
        the_broken_earth = 'Winter, Spring, Summer, Fall; Death is the fifth, '
        the_broken_earth += 'and master of all.'
        with patch('sys.stdout', new=StringIO()) as mock_out:
            dsp_quote(the_broken_earth)
            self.assertEqual(mock_out.getvalue().rstrip(), the_broken_earth)

    def test_int(self):
        '''Test identification of non-string integer input type.'''
        with self.assertRaises(TypeError):
            dsp_quote(94)


if __name__ == '__main__':
    ut.main()
```

The test results:

```console
$ python3 'test_quotes.py'
..
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

Python provides a number of assert methods in the `unittest.TestCase` class.

<table>
  <caption>Assert Methods</caption>
  <thead>
    <tr>
      <th>Method</th>
      <th>Checks</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertEqual"><code>assertEqual(a, b)</code></a></td>
      <td><code>a == b</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertNotEqual"><code>assertNotEqual(a, b)</code></a></td>
      <td><code>a != b</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertTrue"><code>assertTrue(x)</code></a></td>
      <td><code>bool(x) is True</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertFalse"><code>assertFalse(x)</code></a></td>
      <td><code>bool(x) is False</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertIs"><code>assertIs(a, b)</code></a></td>
      <td><code>a is b</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertIsNot"><code>assertIsNot(a, b)</code></a></td>
      <td><code>a is not b</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertIsNone"><code>assertIsNone(x)</code></a></td>
      <td><code>x is None</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertIsNotNone"><code>assertIsNotNone(x)</code></a></td>
      <td><code>x is not None</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertIn"><code>assertIn(a, b)</code></a></td>
      <td><code>a in b</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertNotIn"><code>assertNotIn(a, b)</code></a></td>
      <td><code>a not in b</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertIsInstance"><code>assertIsInstance(a, b)</code></a></td>
      <td><code>isinstance(a, b)</code></td>
    </tr>
    <tr>
      <td><a href="https://docs.python.org/library/unittest.html#unittest.TestCase.assertNotIsInstance"><code>assertNotIsInstance(a, b)</code></a></td>
      <td><code>not isinstance(a, b)</code></td>
    </tr>
  </tbody>
</table>

Testing a class is similar to a function, but there are differences.

For example, assume a class that collects and displays a prompt/sentences:

```python
# major_general.py


class SmedleyButler():
    def __init__(self, prompt):
        self.prompt = prompt
        self.sens = []

    def dsp_prompt(self):
        print(self.prompt)

    def save_sen(self, sen):
        self.sens.append(sen)

    def dsp_sens(self):
        print(self.prompt, '\n')

        for sen in self.sens:
            print(sen)
```

The `unittest.TestCase` class has a [`.setUp()`](https://docs.python.org/library/unittest.html#unittest.TestCase.setUp) method that allows you to create an instance of the class to test once and use it in each of your test methods. When you include a `.setUp()` method in a `TestCase` class, Python runs the `.setUp()` method before running each `.test_` method. Any objects created in the `.setUp()` method are available in each test method that you write.

```python
#!/usr/bin/env python3
# test_major_general.py
import unittest as ut

from major_general import SmedleyButler


class TestSmedleyButler(ut.TestCase):
    def setUp(self):
        prompt = 'War Is a Racket'
        self.marine = SmedleyButler(prompt)
        self.sens = [
            'WAR is a racket. It always has been.',
            ('It is possibly the oldest, easily the most profitable, surely '
             'the most vicious.'),
            'It is the only one international in scope.',
            ('It is the only one in which the profits are reckoned in '
             'dollars and the losses in lives.')
        ]

    def test_single_sentence(self):
        self.marine.save_sen(self.sens[0])
        self.assertIn(self.sens[0], self.marine.sens)

    def test_multiple_sentences(self):
        for sen in self.sens:
            self.marine.save_sen(sen)
        for sen in self.sens:
            self.assertIn(sen, self.marine.sens)


if __name__ == '__main__':
    ut.main()
```

```console
$ python3 test_major_general.py
..
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

Above, `.setUp()` does two things:

1. Create a `SmedleyButler` instance.
2. Create a list of sentences for the instance.

When testing your own classes, `.setUp()` can make your test methods easier to write. You make one set of instances and attributes in `.setUp()`, and then use these instances in all of your test methods.

### Batteries Included

Python has a _batteries included_ philosophy, i.e., Python comes with a large library of helpful modules.

Example modules include:

- [`xmlrpc.client`](https://docs.python.org/library/xmlrpc.client.html#module-xmlrpc.client) and [`xmlrpc.server`](https://docs.python.org/library/xmlrpc.server.html#module-xmlrpc.server) to help implement remote procedure calls.
- [`email`](https://docs.python.org/library/email.html#module-email) for managing email messages, enabling the building or decoding of complex message structures, and the implementing of Internet encoding and header protocols.
- [`json`](https://docs.python.org/library/json.html#module-json) for providing JSON parsing support.
- [`csv`](https://docs.python.org/library/csv.html#module-csv) for supporting direct reading and writing of `.csv` files.
- [`xml.etree.ElementTree`](https://docs.python.org/library/xml.etree.elementtree.html#module-xml.etree.ElementTree), [`xml.dom`](https://docs.python.org/library/xml.dom.html#module-xml.dom), and [`xml.sax`](https://docs.python.org/library/xml.sax.html#module-xml.sax) for supporting XML processing.
- [`sqlite3`](https://docs.python.org/library/sqlite3.html#module-sqlite3) for providing a persistent database that can be updated and accessed using slightly non-standard SQL syntax.
- [`gettext`](https://docs.python.org/library/gettext.html#module-gettext), [`locale`](https://docs.python.org/library/locale.html#module-locale), and [`codecs`](https://docs.python.org/library/codecs.html#module-codecs) for supporting internationalization.

### Output Formatting

The [`reprlib`](https://docs.python.org/library/reprlib.html#module-reprlib) module provides a version of `repr()` customized for abbreviated displays of large or deeply nested containers:

```pycon
>>> import reprlib
>>> reprlib.repr(set('howlongnotlongcausewhatyoureapiswhatyousow'))
"{'a', 'c', 'e', 'g', 'h', 'i', ...}"
>>> 
```

The [`pprint`](https://docs.python.org/library/pprint.html#module-pprint) module provides sophisticated control over both built-in and user-defined objects that is readable by the interpreter. When the result is longer than one line, the _pretty printer_ adds line breaks and indentation to reveal data structure:

```pycon
>>> import pprint as pp
>>> nums = [[[(9, 3), 2, (13, 11)], [(15, 14), 17]]]
>>> pp.pprint(nums, width=20)
[[[(9, 3),
   2,
   (13, 11)],
  [(15, 14), 17]]]
>>> 
```

Instead of printing the formatted content, [`pprint.pformat()`](https://docs.python.org/library/pprint.html#pprint.pformat) returns the formatted string. `pprint.pformat()` can be used to write a variable to a file, which can be imported as a module:

```python
import pprint as pp

country_rankings = [
    {'rank': 1, 'country': 'united states'},
    {'rank': 2, 'country': 'switzerland'},
    {'rank': 3, 'country': 'singapore'},
    {'rank': 4, 'country': 'hong kong'},
    {'rank': 5, 'country': 'luxembourg'},
    {'rank': 6, 'country': 'japan'},
    {'rank': 7, 'country': 'germany'},
    {'rank': 8, 'country': 'united arab emirates'},
    {'rank': 9, 'country': 'british virgin islands'},
    {'rank': 10, 'country': 'guernsey'}
]

with open('financial_secrecy_2022.py', 'w', encoding='utf-8') as f:
    f.write(f'country_rankings = {pp.pformat(country_rankings)}\n')
```

```pycon
>>> import financial_secrecy_2022 as fs2022
>>> fs2022.country_rankings[0]
{'country': 'united states', 'rank': 1}
>>> fs2022.country_rankings[0]['country']
'united states'
>>> 
```

Saving a variable in this fashion is advantageous in that the output file is a text file, which can be read by any text editor. However, saving data using the `shelve` module is preferable for most applications (not all data types can be written to a file as plain text).

The [`textwrap`](https://docs.python.org/library/textwrap.html#module-textwrap)
module formats paragraphs of text to fit a given screen width:

```pycon
>>> import textwrap
>>> quote = '''You can make things safer. You can be more careful. You can be more clever, and there's nothing wrong with that. But at the end of the day, you have to recognize, if you're trying to eliminate all risks from your life, what you're actually doing is eliminating all possibility from your life. You're trying to collapse the universe of outcomes such that what you've lost is freedom.'''
>>> print(textwrap.fill(quote, width=50), '- Edward Snowden', sep='\n\n')
You can make things safer. You can be more
careful. You can be more clever, and there's
nothing wrong with that. But at the end of the
day, you have to recognize, if you're trying to
eliminate all risks from your life, what you're
actually doing is eliminating all possibility from
your life. You're trying to collapse the universe
of outcomes such that what you've lost is freedom.

- Edward Snowden
>>> 
```

The [`locale`](https://docs.python.org/library/locale.html#module-locale) module accesses a database of culture-specific data formats. Its `format_string()` function provides a way to format numbers with group separators:

```pycon
>>> import locale
>>> locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
'en_US.UTF-8'
>>> convs = locale.localeconv()
>>> mil_spending_2021 = {
...     'United States': 767780130000,
...     'China': 270016550000,
...     'Russia': 63485060000
... }
>>> for country, budget in mil_spending_2021.items():
...     print(f"{country:>13} 2021 military spending: {locale.format_string('%s%.*f', (convs['currency_symbol'], convs['frac_digits'], budget), grouping=True):>19}")
... 
United States 2021 military spending: $767,780,130,000.00
        China 2021 military spending: $270,016,550,000.00
       Russia 2021 military spending:  $63,485,060,000.00
>>> 
```

If you are dealing with currency values, `locale.currency()` provides a less verbose approach:

```pycon
>>> import locale
>>> locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
'en_US.UTF-8'
>>> mil_spending_2021 = {
...     'United States': 767780130000,
...     'China': 270016550000,
...     'Russia': 63485060000
... }
>>> for country, budget in mil_spending_2021.items():
...     print(f'{country:>13} 2021 military spending: {locale.currency(budget, grouping=True):>19}')
... 
United States 2021 military spending: $767,780,130,000.00
        China 2021 military spending: $270,016,550,000.00
       Russia 2021 military spending:  $63,485,060,000.00
>>> 
```

### Templating

The [`string`](https://docs.python.org/library/string.html#module-string) module includes a [`Template`](https://docs.python.org/library/string.html#string.Template) class with a simplified syntax suitable for modification by end users. It allows customization of an application without having to alter the application.

Template strings are preferable to f-strings when dealing with _formatted strings generated by end users_ because the formatting mini-language used by f-strings may introduce [security vulnerabilities](https://lucumr.pocoo.org/2016/12/29/careful-with-str-format/) into a program.

The `Template` format uses placeholder names formed by a `$` with valid Python identifiers (i.e., alphanumeric characters and underscores). If the placeholder is surrounded with braces, it can be followed by more alphanumeric letters with no intervening spaces. A literal `$` can be specified with an additional `$` (i.e., `$$`).

```pycon
>>> from string import Template
>>> edward_snowden = Template("It's not $noun that's being exploited, it's people that are being ${verb}.")
>>> edward_snowden.substitute(noun='data', verb='exploited')
"It's not data that's being exploited, it's people that are being exploited."
>>> 
```

`.substitute()` raises a `KeyError` when a placeholder is not provided in a dictionary or keyword argument. Sometimes, user-supplied data may be incomplete and the `.safe_substitute()` method is more appropriate (i.e., it will leave placeholders unchanged if data is missing).

Template _subclasses_ can specify a custom delimiter as an alternative to `$`.

Templating may also be used to separate program logic from the details of multiple output formats, which makes it possible to substitute custom templates for XML files, plain text reports, and HTML web reports.

### Binary Data Record Layouts

The [`struct`](https://docs.python.org/library/struct.html#module-struct) module provides the [`pack()`](https://docs.python.org/library/struct.html#struct.pack) and [`unpack()`](https://docs.python.org/library/struct.html#struct.unpack) functions for working with variable length binary record formats.

### Multi-Threading

Threading is a method for decoupling tasks that are not sequentially dependent. [Threads](https://en.wikipedia.org/wiki/Thread_(computing)) can be used to improve the responsiveness of applications that accept user input while other background tasks run. An alternative use case is running input/output in parallel with computations in another thread.

The high-level [`threading`](https://docs.python.org/library/threading.html#module-threading) module can run tasks in the background, while the main program continues to run.

Coordinating threads that share data/resources when using multi-threaded applications may be difficult. `threading` addresses this by providing synchronization primitives (e.g., locks, events, condition variables, semaphores).

The preferred approach to task coordination is to concentrate all access to a resource in a single thread and use the [`queue`](https://docs.python.org/library/queue.html#module-queue) module to feed that thread with requests from other threads.

### Logging

The [`logging`](https://docs.python.org/library/logging.html#module-logging) module offers a full-featured and flexible logging system. At its simplest, log messages are sent to a file or `sys.stderr`.

By default, information and debugging messages are suppressed and output is sent to the standard error.

Alternative output options include:

- routing messages via email
- datagrams
- sockets
- HTTP server

New filters can select different routing based on the message priority (i.e., `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`).

The logging system can be directly configured via Python or loaded from a user-editable configuration file for customized logging without application alterations.

#### Using the `logging` Module

The following example configures logging via Python:

```python
import logging
from random import randrange
from time import sleep

logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
date_fmt = '%Y-%m-%d %H:%M:%S'
logging.basicConfig(level=logging.DEBUG, format=logging_fmt,
                    datefmt=date_fmt)

logging.debug('RATM Start')

testify_1 = 'Who controls the past now controls the future'
testify_2 = 'Who controls the present now controls the past'
testify_3 = 'Who controls the present now?'
sleep(randrange(2, 5))

logging.debug('The Future')
print(testify_1)
sleep(randrange(2, 5))

logging.debug('The Past')
print(testify_2)
sleep(randrange(2, 5))

logging.debug('The Present')
print(testify_3)
sleep(randrange(2, 5))

logging.debug('RATM End')
```

The resulting output would be something like this:

```console
2023-02-15 19:53:52 - DEBUG- RATM Start
2023-02-15 19:53:56 - DEBUG- The Future
Who controls the past now controls the future
2023-02-15 19:53:59 - DEBUG- The Past
Who controls the present now controls the past
2023-02-15 19:54:03 - DEBUG- The Present
Who controls the present now?
2023-02-15 19:54:06 - DEBUG- RATM End
```

When Python logs an event, it creates a [`LogRecord`](https://docs.python.org/library/logging.html#logging.LogRecord) object that holds information about the event. [`logging.basicConfig()`](https://docs.python.org/library/logging.html#logging.basicConfig) lets you specify what details about the `LogRecord` object that you want to see and how those details are displayed.

Via the `format` parameter, different [attributes](https://docs.python.org/library/logging.html#logrecord-attributes) can be used to merge data into the final format string.

For example, `%(asctime)s` returns the human-readable time when the `LogRecord` was created in the form `2023-02-15 19:53:52,248`, where the numbers after the comma are milliseconds. A different date format can be specified via the `datefmt` parameter.

Other common attributes include `%(levelname)s` and `%(message)s`, which return the text logging level for the message and the logged message, respectively.

[`logging.debug()`](https://docs.python.org/library/logging.html#logging.debug) is used to print log information. The printed information is in the format specified by `logging.basicConfig()` and includes any string messages passed to it.

#### Logging Levels

[Logging levels](https://docs.python.org/library/logging.html#levels) provide a way to categorize log messages by importance.

<table>
  <caption>Logging Levels</caption>
  <thead>
    <tr>
      <th>Level</th>
      <th>Numeric Value</th>
      <th>Logging Function</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><em>CRITICAL</em></td>
      <td>50</td>
      <td><a href="https://docs.python.org/library/logging.html#logging.critical"><code>logging.critical(msg, *args, **kwargs)</code></a></td>
      <td>The highest level. Used to indicate a fatal error that has caused or is about to cause the program to stop running entirely.</td>
    </tr>
    <tr>
      <td><em>ERROR</em></td>
      <td>40</td>
      <td><a href="https://docs.python.org/library/logging.html#logging.error"><code>logging.error(msg, *args, **kwargs)</code></a></td>
      <td>Used to record an error that caused the program to fail to do something.</td>
    </tr>
    <tr>
      <td><em>WARNING</em></td>
      <td>30</td>
      <td><a href="https://docs.python.org/library/logging.html#logging.warning"><code>logging.warning(msg, *args, **kwargs)</code></a></td>
      <td>Used to indicate a potential problem that does not prevent the program from working, but might do so in the future.</td>
    </tr>
    <tr>
      <td><em>INFO</em></td>
      <td>20</td>
      <td><a href="https://docs.python.org/library/logging.html#logging.info"><code>logging.info(msg, *args, **kwargs)</code></a></td>
      <td>Used to record information on general events in your program or confirm that things are working at their point in the program.</td>
    </tr>
    <tr>
      <td><em>DEBUG</em></td>
      <td>10</td>
      <td><a href="https://docs.python.org/library/logging.html#logging.debug"><code>logging.debug(msg, *args, **kwargs)</code></a></td>
      <td>The lowest level. Used for small details. Usually, you can read about these messages only when diagnosing problems.</td>
    </tr>
    <tr>
      <td><em>NOTSET</em></td>
      <td>0</td>
      <td></td>
      <td>Causes all messages to be processed when the logger is the root logger, or delegation to the parent when the logger is a non-root logger.</td>
    </tr>
  </tbody>
</table>

Log messages are passed to log level functions as strings. Log levels are suggestions. It is up to you to decide which category your log message belongs to.

```pycon
>>> import logging
>>> logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
>>> logging.debug('So raise your fists')
2023-02-15 20:41:19,372 - DEBUG - So raise your fists
>>> logging.info('And march around')
2023-02-15 20:41:25,146 - INFO - And march around
>>> logging.warning("Just don't take what you need")
2023-02-15 20:41:31,568 - WARNING - Just don't take what you need
>>> logging.error("I'll jail and bury those committed")
2023-02-15 20:41:36,622 - ERROR - I'll jail and bury those committed
>>> logging.critical('And smother the rest in greed')
2023-02-15 20:41:42,952 - CRITICAL - And smother the rest in greed
>>> 
```

You can change the priority of logging messages that you want to see by using `logging.basicConfig()`'s `level` parameter (e.g., `level=logging.ERROR`).

#### Disable Logging

`logging.disable()` disables log messages. Instead of manually removing all logging calls, you can pass `logging.disable()` a logging level, and all log messages at that level or lower are suppressed.

To completely disable logging, add [`logging.disable(logging.CRITICAL)`](https://docs.python.org/library/logging.html#logging.disable) to your program. Since `logging.disable()` disables all messages _after_ it, add it near the `import logging` line in your program.

#### Logging to a File

Instead of displaying log messages, you can write them to a text file via `logging.basicConfig()`'s `filename` parameter:

```python
import logging

logging.basicConfig(
    filename='/var/log/log.txt',
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
```

### Weak References

Python performs automatic memory management (i.e., reference counting for most objects, [garbage collection](https://docs.python.org/glossary.html#term-garbage-collection) to eliminate cycles). Memory is freed shortly after the last reference to an object has been eliminated.

Usually, this is sufficient for most applications. However, if there is a need to track objects only as long as they are being used, the [`weakref`](https://docs.python.org/library/weakref.html#module-weakref) module provides tools for tracking objects _without creating a reference_.

When the object is no longer needed, it is automatically removed from a weakref table and a callback is triggered for weakref objects.

A typical use case for a weak reference is to cache objects that are expensive to create:

```python
import weakref, gc


class Quote:
    def __init__(self, quote):
        self.quote = quote

    def __repr__(self):
        cls = type(self)
        return f'{cls.__name__}(quote={self.quote!r})'


# Create a mapping class that weakly references values
weak_dict = weakref.WeakValueDictionary()

proverb = "You can't awaken someone who's pretending to be asleep."
# Create a strong reference for instance
strong_ref = Quote(proverb)

# Display instance's reference count; strong_ref usage as argument
# for sys.getrefcount() counts as a (temporary) reference too
print(sys.getrefcount(strong_ref))

weak_dict['permanent_record'] = strong_ref  # Create a weak reference entry
print(sys.getrefcount(strong_ref))  # Reference count does not change

del strong_ref  # Remove reference
gc.collect()  # Immediately run garbage collection
print(weak_dict['permanent_record'])  # Weak reference automatically removed
```

The resulting output would be:

```pycon
2
2
Traceback (most recent call last):
  File "/home/ed/weak_refs.py", line 52, in <module>
    print(weak_dict['permanent_record'])  # Weak reference automatically removed
  File "/usr/lib/python3.9/weakref.py", line 134, in __getitem__
    o = self.data[key]()
KeyError: 'permanent_record'
```

### Tools For Working With Lists

Sometimes, an alternative to the built-in list type is needed.

The [`array`](https://docs.python.org/library/array.html#module-array) module provides an [`array()`](https://docs.python.org/library/array.html#array.array) object that is similar to a list, but only stores homogeneous data and stores it more compactly. When creating an array, you must specify a _typecode_, which restricts the items in the array to that type.

For example, the following creates an array of numbers stored as two-byte unsigned binary numbers (typecode `H`):

```pycon
>>> from array import array
>>> nums_array = array('H', [17, 2, 12, 20, 10])
>>> nums_array[2:4]
array('H', [12, 20])
>>> 
```

The [`bisect`](https://docs.python.org/library/bisect.html#module-bisect) module offers functions for manipulating sorted lists:

```pycon
>>> import bisect
>>> lang_nums = [(1, 'python'), (2, 'bash'), (4, 'c++'), (5, 'javascript')]
>>> bisect.insort(lang_nums, (3, 'rust'))
>>> lang_nums
[(1, 'python'), (2, 'bash'), (3, 'rust'), (4, 'c++'), (5, 'javascript')]
>>> 
```

The [`heapq`](https://docs.python.org/library/heapq.html#module-heapq) module provides functions for implementing heaps based on normal lists. The lowest-valued entry is always kept at position `0`. This is useful for applications that repeatedly access the smallest element, but do not want to run a full list sort.

```pycon
>>> from heapq import heapify, heappop, heappush
>>> nums = [14, 12, 15, 4, 7]
>>> heapify(nums)
>>> heappush(nums, 2)
>>> [heappop(nums) for i in range(3)]
[2, 4, 7]
>>> 
```

### Decimal Floating Point Arithmetic

The [`decimal`](https://docs.python.org/library/decimal.html#module-decimal) module offers a [`Decimal`](https://docs.python.org/library/decimal.html#decimal.Decimal) data type for decimal floating point arithmetic. This is preferable over the built-in `float` implementation of binary floating point for:

- financial applications or anything that requires exact decimal representations
- precision control
- rounding control
- significant decimal place tracking
- applications where users expect results to match calculations done by hand

For example, consider calculating a 5% tax on a $0.90 charge and rounding to the nearest cent. Different results are given for decimal floating point and binary floating point:

```pycon
>>> from decimal import Decimal
>>> round(Decimal('0.90') * Decimal('1.05'), 2)
Decimal('0.94')
>>> round(0.90 * 1.05, 2)
0.95
>>> 
```

`Decimal` results keep a trailing zero, automatically inferring four place significance from multiplicands with two place significance. `Decimal` reproduces mathematics as done by hand, and avoids issues that may arise when binary floating point cannot precisely represent decimal quantities.

Precise representation enables `Decimal` to perform modulo calculations and equality tests that are unfit for binary floating point:

```pycon
>>> from decimal import Decimal
>>> Decimal('1.00') % Decimal('.20')
Decimal('0.00')
>>> 1.00 % 0.20
0.19999999999999996
>>> sum([Decimal('0.1')] * 10) == Decimal('1.0')
True
>>> sum([0.1] * 10) == 1.0
False
>>> 
```

The `decimal` module enables arithmetic with as much precision as required:

```pycon
>>> from decimal import Decimal, getcontext
>>> getcontext().prec = 14
>>> Decimal(1) / Decimal(89)
Decimal('0.011235955056180')
>>> 
```

## Debugging

`pdb` is Python's interactive source code debugger. It is part of the standard library.

To enter the debugger, use the `breakpoint()` function in your source code. `breakpoint()` automatically imports `pdb` and calls `pdb.set_trace()`.

Alternatively, you can start `pdb` without modifying your source code by passing `-m pdb` on the command line:

`python3 -m pdb ex.py ex_arg_1 ex_arg_2`

### Definitions

**Current Frame**  
The current function where `pdb` has stopped execution, i.e., where your application is currently paused (and the frame of reference for `pdb` commands). Indicated by a `->` in a stack trace.

**Frame**  
A data structure that Python creates when a function is called and deletes when the function returns.

**Stack**  
An ordered list of frames or function calls at any point in time. The stack gets larger and smaller throughout the life of an application as functions are called and then return.

**Stack Trace**  
An ordered list of all the frames that Python has created to keep track of function calls.

### `pdb` Commands

`a`  
Print the argument list of the current function.

`b`  
With no arguments, list all breaks.

With a line number argument, set a breakpoint at this line in the current file. If a line number argument is not prepended with `filename:`, the current source file is used.

If you need to break an expression with a variable name located _inside_ of a function (i.e., a variable name not included in the function's argument list), specify the line number.

A function name can be used instead of a line number to set a breakpoint. Provided expressions should only use function arguments or global variables that are available at the time that the function is entered. Otherwise, the breakpoint will stop execution inside of the function, regardless of the expression's value.

A break _condition_ can be passed as an optional second argument.

`c`  
Continue execution and only stop when a breakpoint is encountered.

`cl ex_file:ex_line_num`, `cl ex_bp_num...`  
Delete a breakpoint (`cl` is short for _clear_).

`d`  
Move the current frame count (default one) levels down in the stack trace (to a newer frame).

`display ex_expr`  
Display the value of `ex_expr` if it changed, each time execution stops in the current frame.

Without `ex_expr`, list all display expressions for the current frame. After entering multiple `display` expressions, this can be used to verify the values of multiple variables and is a nice alternative to `p`.

`enable ex_bp_num`/`disable ex_bp_num`  
Enable/disable breakpoint, where `ex_bp_num` is the breakpoint number from the breakpoints list’s 1st column `Num`.

`h`  
See a list of available commands.

A command/topic can be provided to this command as an argument to show help for that command/topic.

`h pdb` can be used to show the full `pdb` documentation.

`l`  
List source code for the current file. Without arguments, list 11 lines around the current line or continue the previous listing.

Pass `.` as an argument to always list 11 lines around the current line.

`ll`  
List the whole source code for the current function or frame.

`n`  
Continue execution until the next line in the _current_ function is reached or it returns (i.e., _Step Over_).

`p ex_expr`  
Print the value of an expression.

`pp ex_expr`  
Pretty-print the value of an expression.

`q`  
Quit the debugger and exit.

`s`  
Execute the current line and stop at the first possible occasion (either in a function that is called or in the current function), i.e., _Step Into_.

If execution is stopped in another function, `s` will print `--Call--`.

`tbreak`  
Set a temporary breakpoint. It is automatically removed when it is first hit.

`u`  
Move the current frame count (default one) levels up in the stack trace (to an older frame).

`undisplay ex_expr`  
Do not display `ex_expr` any more in the current frame.

Without `ex_expr`, clear all display expressions for the current frame.

`unt`  
Continue execution until the line with a number greater than the current one is reached (similar to `n`, except that `n` stops at the next _logically_ executed line). 

With a line number argument, continue execution until a line with a number greater or equal to that number is reached (like `c` with a line number argument).

Use `unt` when you want to continue execution and stop farther down in the current source file.

`w`  
Print a stack trace, with the most recent frame at the bottom. An arrow indicates the current frame, which determines the context of most commands.

### `pdb` Usage

You can press **Enter** inside of the `pdb` command line to repeat the last command.

When `pdb` stops at the end of a function before it returns (e.g., when `n` and `s` are used), it also prints the return value for you (`-> ex_rtn_value`).

## Refactoring

Sometimes, you may notice characteristics of your code that indicate potential problems (e.g., a long function doing multiple things, duplicated code). This is referred to as [code smell](https://en.wikipedia.org/wiki/Code_smell) and is often the basis for a [code refactoring](https://en.wikipedia.org/wiki/Code_refactoring).

_Refactoring_ is the process of reorganizing your code to improve its design, structure, and implementation without actually changing its functionality. Refactoring code makes it cleaner, easier to understand, and easier to extend.

## Distribution

Python code can come in a variety of formats:

- **Scripts** - Top-level files intended for execution by the user.
- **Modules** - Collections of related code intended to be imported. Modules extend the power of scripts.
- **Packages** - A directory of modules. Packages allow the hierarchical structure of the module namespace. Like how files are organized on a drive into folders and sub-folders, modules are organized into packages and subpackages.

    To be considered a package (or subpackage), a directory must contain a file named `__init__.py`. Usually, this file includes the initialization code for the corresponding package.

    Note that a package is still a module.

- **Libraries** - An umbrella term referring to a reusable collection of code. Usually, a Python library is a directory of related modules and packages.

There are different approaches that can be used to distribute code:

- Python library
- Standalone program
- Python web application

### Python Library

Publishing a package [involves a flow](https://packaging.python.org/en/latest/flow/) from developer source code to the end user's Python environment.

- A [source tree](https://packaging.python.org/en/latest/flow/#the-source-tree) contains the package.
- A [configuration file](https://packaging.python.org/en/latest/flow/#the-configuration-file) (e.g., `pyproject.toml`) describes the package metadata and how to create [build artifacts](https://packaging.python.org/en/latest/flow/#build-artifacts).
- Build artifacts are built by a build tool and the configuration file. Afterwards, they are sent to a package distribution service (e.g., PyPI) as a [source distribution (sdist)](https://packaging.python.org/en/latest/glossary/#term-Source-Distribution-or-sdist) and one (or more) [built distributions (wheels)](https://packaging.python.org/en/latest/glossary/#term-Built-Distribution).
- `pip` is used to install the package and its dependencies.

Distributing code like this keeps it close to the original script and only adds what is necessary for others to run it. However, using this approach also means that users will need to run the code with Python. Many people who want to use a script’s functionality may not have Python installed or be familiar with the processes required to directly work with the code.

#### Source Distributions

A source distribution (sdist) contains enough to install a package from source in an end user's Python environment. It needs the package source, but may also include tests and documentation. Source distributions are good for end users that want to develop your sources and for where some local compilation is required (e.g., as a C extension).

The [`build`](https://packaging.python.org/en/latest/key_projects/#build) package creates a source distribution like so:

`python3 -m build --sdist ex_src_dir_tree`

#### Built Distributions

A build distribution (wheels) contains only files needed for an end user's Python environment. During installation, no compilation steps are required and the wheel file can be unpacked into the appropriate directory, making installs faster and more convenient for end users.

A wheel is a ZIP-format archive (`.zip`) with a specially formatted file name (which tells installers what Python versions and platforms the wheel will support) and the `.whl` extension. As a type of built distribution, the wheel is a ready-to-install format that allows you to skip the build stage required with source distributions.

Pure Python packages usually need only one _generic_ wheel. A package with compiled binary extensions needs a wheel for each supported combination of Python interpreter, operating system, and CPU architecture that it supports. If a suitable wheel is not available, `pip` falls back to installing the source distribution.

The `build` package creates a built distribution like so:

`python3 -m build --wheel ex_src_dir_tree`

Keep in mind, the default `build` behavior is to make both a sdist and wheel from the source code in the current directory.

#### Package Distribution Service Upload

A tool like [twine](https://packaging.python.org/en/latest/key_projects/#twine) can be used to upload build artifacts to PyPI for distribution:

`twine upload dist/ex_pkg_name_ver.tar.gz dist/ex_pkg_name_ver-py3-none-any.whl`

### Standalone Program

A script can be turned into a standalone program by either packing the code or building a program graphical user interface (GUI).

The following projects help with packing code:

- [Briefcase](https://briefcase.readthedocs.io/en/latest/)
- [PyInstaller](https://pyinstaller.org/en/stable/)

Both projects turn Python scripts into executable programs that can be used without requiring users to explicitly run the Python interpreter.

The following projects help with building Python program GUIs:

- [BeeWare Project](https://beeware.org/project/)
- [Tkinter](https://docs.python.org/library/tkinter.html)
- [wxPython](https://www.wxpython.org/)

### Python Web Application

A Python-driven web application is platform-independent and created using a [Python web framework](https://wiki.python.org/moin/WebFrameworks). Some of the more popular frameworks include:

**Full-Stack Frameworks**

- [Dash](https://plotly.com/dash/)
- [Django](https://www.djangoproject.com/)
- [Masonite](https://docs.masoniteproject.com/)
- [Streamlit](https://streamlit.io/)

**Non Full-Stack Frameworks**

- [Brython](https://brython.info/)
- [FastAPI](https://fastapi.tiangolo.com/)
- [Flask](https://flask.palletsprojects.com/en/2.2.x/)
- [Pyramid](https://trypyramid.com/)
- [PyScript](https://pyscript.net/)

#### Offline PyScript

*Last Updated: February 24, 2024*

Both PyScript core and the interpreter used to run the code need to be served with the application. As of this writing, PyScript supports two interpreters:

- [MicroPython](https://micropython.org/)
- [Pyodide](https://pyodide.org/en/stable/)

##### npm

If necessary, install `npm`:

**Debian**  
`sudo apt install npm`

**Fedora**  
`sudo dnf install npm`

##### PyScript Core

Locally install `@pyscript/core`:

```console
$ mkdir 'offline_pyscript' &&
    cd 'offline_pyscript' &&
    echo '{}' > 'package.json' &&
    npm install @pyscript/core
```

The empty `package.json` file is needed to ensure the project directory will include the local `npm_modules/` directory, instead of placing assets elsewhere.

Create `public/pyscript/` and copy the contents of `node_modules/@pyscript/core/dist/` there:

```console
$ mkdir -p 'public/pyscript/' &&
    cp -R node_modules/@pyscript/core/dist/* 'public/pyscript/'
```

Create `public/index.html`:

```
$ content='<html lang="en-US">
  <head>
    <title>Offline PyScript</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="referrer" content="no-referrer">
    <link rel="stylesheet" href="/pyscript/core.css">
    <script type="module" src="/pyscript/core.js"></script>
  </head>
  <body ontouchstart="">
    <script type="mpy">
      from pyscript import document

      document.body.append('\''PyScript is running'\'')
    </script>
  </body>
</html>' && echo "${content}" > 'public/index.html'
```

Confirm that PyScript core locally works:

`python3 -m http.server -d 'public/'`

At this point, a network connection is still required as MicroPython is loaded via a Content Delivery Network (CDN).

To proceed, you can locally install either MicroPython or Pyodide, depending on your needs. As of this writing, MicroPython does not yet have a package manager, while Pyodide does (i.e., `micropip`).

##### MicroPython

Locally install `@micropython/micropython-webassembly-pyscript`:

`npm install @micropython/micropython-webassembly-pyscript`

Create `public/micropython/` and copy germane files from `node_modules/@micropython/micropython-webassembly-pyscript/` there:

```console
$ mkdir 'public/micropython/' &&
    cp \
        node_modules/@micropython/micropython-webassembly-pyscript/micropython.* \
        'public/micropython/'
```

Update `public/index.html` to specify the local MicroPython as the desired interpreter:

```
$ content='<html lang="en-US">
  <head>
    <title>Offline PyScript</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="referrer" content="no-referrer">
    <link rel="stylesheet" href="/pyscript/core.css">
    <script type="module" src="/pyscript/core.js"></script>
  </head>
  <body ontouchstart="">
    <mpy-config>
      interpreter = "/micropython/micropython.mjs"
    </mpy-config>
    <script type="mpy">
      from pyscript import document

      document.body.append('\''PyScript is running'\'')
    </script>
  </body>
</html>' && echo "${content}" > 'public/index.html'
```

Now, PyScript should be functional offline.

##### Pyodide

Locally install `pyodide`:

`npm install pyodide`

Create `public/pyodide/` and copy germane files from `node_modules/pyodide/` there:

```console
$ mkdir 'public/pyodide/' &&
    cp \
        node_modules/pyodide/pyodide* \
        'node_modules/pyodide/python_stdlib.zip' \
        'public/pyodide/'
```

Update `public/index.html` to specify the local Pyodide as the desired interpreter:

```
$ content='<html lang="en-US">
  <head>
    <title>Offline PyScript</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="referrer" content="no-referrer">
    <link rel="stylesheet" href="/pyscript/core.css">
    <script type="module" src="/pyscript/core.js"></script>
  </head>
  <body ontouchstart="">
    <py-config>
      interpreter = "/pyodide/pyodide.mjs"
    </py-config>
    <script type="py">
      from pyscript import document

      document.body.append('\''PyScript is running'\'')
    </script>
  </body>
</html>' && echo "${content}" > 'public/index.html'
```

Now, PyScript should be functional offline.

###### Local Pyodide Packages

To locally use third-party libraries, download a packages bundle from [Pyodide releases](https://github.com/pyodide/pyodide/releases) (e.g., `pyodide-0.25.0.tar.bz2 `) *that matches the version of Pyodide you installed with `npm`*.

In the extracted `pyodide/` directory, copy the `micropip-*.whl` and `packaging-*.whl` files into `public/pyodide/`. Then, copy the wheel file for the third-party library of interest into `public/pyodide/`, as well.

Update `public/index.html` to include the package of interest by appending a line to the `<py-config>` tag:

`packages = ["ex_pkg"]`

Run PyScript and examine the **Network** tab in the browser developer tools for errors. Likely, the third-party library will require other wheel files to function. Copy each required wheel file to `public/pyodide/` and refresh the page. Repeat until you no longer see wheel-related errors in the **Network** tab.

## Data Extraction

[Web scraping](https://en.wikipedia.org/wiki/Web_scraping) is [data scraping](https://en.wikipedia.org/wiki/Data_scraping) used for downloading and processing content from websites.

The standard library's [`webbrowser.open()`](https://docs.python.org/library/webbrowser.html#webbrowser.open) can be used to launch a new browser instance to a specified URL:

```pycon
>>> import webbrowser as wb
>>> wb.open('https://www.chrislhedges.com/')
True
>>> 
```

### Requests

For a high-level HTTP client interface that lets you download web content, the third-party [Requests](https://pypi.org/project/requests/) library is available (the standard library's [`urllib`](https://docs.python.org/library/urllib.html#module-urllib) package collects lower-level modules for working with URLs):

`python3 -m pip install requests`

[`requests.get()`](https://requests.readthedocs.io/en/latest/api/#requests.get) sends a GET request. This returns a [`Response`](https://requests.readthedocs.io/en/latest/api/#requests.Response) object, which contains the response that the web server provided for your request:

```pycon
>>> import requests as reqs, textwrap
>>> rsp = reqs.get('https://theintercept.com/2018/08/15/nsa-snowden-documents-climate-change/')
>>> type(rsp)
<class 'requests.models.Response'>
>>> rsp.status_code == reqs.codes.ok
True
>>> len(rsp.text)
140202
>>> print(textwrap.fill(rsp.text[68085:69771], width=79))
The NSA’s eco-spying coincided with repeated findings within the intelligence
community that environmental concerns had national security implications. The
military has long recognized climate change as a major threat, and over the
years, the Defense Department has framed it as a “</span><a
class="Internet_20_link"
href="https://www.acq.osd.mil/eie/Downloads/CCARprint_wForward_e.pdf">threat
multiplier</a><span class="T1">,” enflaming conflicts by adding to the mix
issues like drought, loss of access to drinking water or irrigation, rising sea
levels, migration and die-offs of wild game, wildfires, catastrophic storms,
and the human displacement that comes with all such issues. A previously
published NSA document, dated May 14, 2007, </span><a class="Internet_20_link"
href="https://www.information.dk/udland/2014/01/the-nsa-espionage-was-a-means-
to-strengthen-the-us-position-in-climate-negotiations">quoted</a><span
class="T1"> then-Under Secretary of Defense for Intelligence James Clapper at
an internal NSA conference saying, “Increasingly, the environment is becoming
an adversary for us. And I believe that the capabilities and assets of the
Intelligence Community are going to be brought to bear increasingly in
assessing the environment as an adversary.” </span></p> <p class="P3"><span
class="T1">The U.S. intelligence community’s Worldwide Threat Assessment,
released in February 2018, dedicates a section to the issue of climate change.
“The impacts of the long-term trends toward a warming climate, more air
pollution, biodiversity loss, and water scarcity are likely to fuel economic
and social discontent—and possibly upheaval—through 2018,” the assessment said.
>>> 
```

A straightforward way to check for success when downloading web content is to use [`Response.raise_for_status()`](https://requests.readthedocs.io/en/latest/api/#requests.Response.raise_for_status), which raises an [`HTTPError`](https://requests.readthedocs.io/en/latest/api/#requests.HTTPError), if one occurred:


```python
import requests as reqs

rsp = reqs.get('https://www.igsd.org/threat-multiplier/')

try:
    rsp.raise_for_status()
except reqs.HTTPError as exc:
    print(f'Exception occurred: {exc}')
    # Exception occurred: 404 Client Error: Not Found for url: https://www.igsd.org/threat-multiplier/
```

Web content can be saved to a file after download using `open()` and `.write()`:

```python
import re
import textwrap

import requests as reqs

PAT = re.compile(f'<.+?>')

URL = (
    'https://arstechnica.com/science/2018/09/pharma-ceo-jacks-drug-price-'
    '400-citing-moral-requirement-to-make-money/'
)
rsp = reqs.get(URL)

try:
    rsp.raise_for_status()
except reqs.HTTPError as exc:
    print(f'Exception occurred: {exc}')
else:
    with open('sociopaths.html', 'wb') as f:
        for chunk in rsp.iter_content(100000):
            f.write(chunk)

    with open('sociopaths.html', 'r', encoding='utf-8') as f:
        text = f.read()
        text = re.sub(PAT, '', text)

        start = text.find('The chief executive')
        end = text.find('the highest price,”') + len('the highest price,”')
        text = text[start:end]

        paras, len_paras = text.split('\n'), len(text.split('\n'))

        for i in range(len_paras):
            if i == (len_paras - 1):
                print(textwrap.fill(paras[i], width=79) + '...')
            else:
                print(textwrap.fill(paras[i], width=79) + '\n')
```

```pycon
The chief executive of a small pharmaceutical company defended hiking the price
of an essential antibiotic by more than 400 percent and told the Financial
Times that he thinks “it is a moral requirement to make money when you can.”

Nirmal Mulye, CEO of the small Missouri-based drug company Nostrum
Laboratories, raised the price of bottle of nitrofurantoin from $474.75 to
$2,392 last month. The drug is a decades-old antibiotic used to treat urinary-
tract infections caused by Escherichia coli and certain other Gram-negative
bacteria. The World Health Organization lists nitrofurantoin as an essential
medicine.

In an interview with the FT, Mulye went on to say it was also a “moral
requirement” to “sell the product for the highest price,”...
```

Above, `sociopaths.html` is opened for writing only in binary format. A `for` loop and [`Response.iter_content()`](https://requests.readthedocs.io/en/latest/api/#requests.Response.iter_content) is used to read into memory _chunks_ of content with a max size of 100,000 bytes (generally, a good size to process content with).

### Beautiful Soup

[Beautiful Soup](https://pypi.org/project/beautifulsoup4/) (often used with the [lxml](https://pypi.org/project/lxml/) library) is a Python library for pulling data out of HTML and XML files:

`python3 -m pip install beautifulsoup4 lxml`

A [`BeautifulSoup`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#beautifulsoup) object is passed the content to parse (a string or open filehandle), as well as an optional XML parser (by default, `BeautifulSoup` uses an HTML parser):

```python
import bs4, requests as reqs

rsp = reqs.get('https://en.wikipedia.org/wiki/Python_(programming_language)')

try:
    rsp.raise_for_status()
except reqs.HTTPError as exc:
    print(f'HTTP error occurred: {exc}')
else:
    py_lang = bs4.BeautifulSoup(rsp.text, 'lxml')

print(type(py_lang))
# <class 'bs4.BeautifulSoup'>

# Alternative parsing from local file
with open('Python_(programming_language).html', 'r', encoding='utf-8') as f:
    py_lang = bs4.BeautifulSoup(f, 'lxml')
    print(type(py_lang))
    # <class 'bs4.BeautifulSoup'>
```

[`BeautifulSoup.select()`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#css-selectors) uses [SoupSieve](https://facelessuser.github.io/soupsieve/) to run provided CSS selectors against a parsed document and returns all matching elements:

```pycon
>>> import bs4, requests as reqs
>>> rsp = reqs.get('https://en.wikipedia.org/wiki/Python_(programming_language)')
>>> try:
...     rsp.raise_for_status()
... except reqs.HTTPError as exc:
...     print(f'HTTP error occurred: {exc}')
... else:
...     py_lang = bs4.BeautifulSoup(rsp.text, 'lxml')
... 
>>> imgs = py_lang.select('img')
>>> for img in imgs[:3]:
...     print(img)
... 
<img alt="" aria-hidden="true" class="mw-logo-icon" height="50" src="/static/images/icons/wikipedia.png" width="50"/>
<img alt="Wikipedia" class="mw-logo-wordmark" src="/static/images/mobile/copyright/wikipedia-wordmark-en.svg" style="width: 7.5em; height: 1.125em;"/>
<img alt="The Free Encyclopedia" class="mw-logo-tagline" height="13" src="/static/images/mobile/copyright/wikipedia-tagline-en.svg" style="width: 7.3125em; height: 0.8125em;" width="117"/>
>>> py_powered = py_lang.select('div.thumb:nth-child(127)')
>>> py_powered
[<div class="thumb tright"><div class="thumbinner" style="width:222px;"><a class="image" href="/wiki/File:Python_Powered.png"><img class="thumbimage" data-file-height="794" data-file-width="1123" decoding="async" height="156" src="//upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Python_Powered.png/220px-Python_Powered.png" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Python_Powered.png/330px-Python_Powered.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Python_Powered.png/440px-Python_Powered.png 2x" width="220"/></a> <div class="thumbcaption"><div class="magnify"><a class="internal" href="/wiki/File:Python_Powered.png" title="Enlarge"></a></div>Python Powered</div></div></div>]
>>> 
```

Each object returned by `BeautifulSoup.select()` is a [`Tag`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#tag) object, which is how Beautiful Soup represents an HTML element:

```pycon
>>> type(imgs[0])
<class 'bs4.element.Tag'>
>>> imgs[0]
<img alt="" aria-hidden="true" class="mw-logo-icon" height="50" src="/static/images/icons/wikipedia.png" width="50"/>
>>> imgs[0].attrs
{'class': ['mw-logo-icon'], 'src': '/static/images/icons/wikipedia.png', 'alt': '', 'aria-hidden': 'true', 'height': '50', 'width': '50'}
>>> 
```

The text of a document or tag (inner text) can be retrieved using the [`.get_text()`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#get-text) method:

```pycon
>>> py_lang.get_text()[:200]
'\n\n\nPython (programming language) - Wikipedia\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nJump to content\n\n\n\n\n\nToggle sidebar\n\n\n\n\n\n\n\n\n\n\n\n\nSearch\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nCreate accountLog in\n\n\n\n\n\nPersonal tools\n\n\n\n\n Cre'
>>> pkgs = py_lang.select('#mw-content-text > div:nth-child(1) > p:nth-child(78)')
>>> len(pkgs)
1
>>> pkgs[0].get_text()
'As of 14\xa0November\xa02022,[update] the Python Package Index (PyPI), the official repository for third-party Python software, contains over 415,000[121] packages with a wide range of functionality, including:\n'
>>> 
```

`Tag.get()` allows attribute value access from an element:

```pycon
>>> pkgs[0]
<p>As of 14 November 2022,<sup class="plainlinks noexcerpt noprint asof-tag update" style="display:none;"><a class="external text" href="https://en.wikipedia.org/w/index.php?title=Python_(programming_language)&amp;action=edit">[update]</a></sup> the <a href="/wiki/Python_Package_Index" title="Python Package Index">Python Package Index</a> (PyPI), the official repository for third-party Python software, contains over 415,000<sup class="reference" id="cite_ref-Modulecounts_2022_121-0"><a href="#cite_note-Modulecounts_2022-121">[121]</a></sup> packages with a wide range of functionality, including:
</p>
>>> pkgs[0].select_one('a')
<a class="external text" href="https://en.wikipedia.org/w/index.php?title=Python_(programming_language)&amp;action=edit">[update]</a>
>>> pkgs[0].select_one('a').get('href')
'https://en.wikipedia.org/w/index.php?title=Python_(programming_language)&action=edit'
>>> 
```

### Selenium

[Selenium](https://pypi.org/project/selenium/) lets Python directly control the browser programmatically. In this way, Selenium allows you to interact with web pages in a more advanced way than Requests and Beautiful Soup. Also, it can account for dynamic content asynchronously loaded via JavaScript and can be run in a headless mode.

On the other hand, since Selenium launches a web browser, it is slower and can be overkill if you simply need to download some content from the Web.

`python3 -m pip install selenium`

In addition to `selenium`, a [Selenium client driver](https://github.com/SeleniumHQ/selenium/blob/trunk/py/docs/source/index.rst#drivers) needs to be installed for your browser of choice. For example, Firefox uses [geckodriver](https://github.com/mozilla/geckodriver/releases).

Where you need to move the extracted driver to on your system depends on your setup and how many people need to use `selenium`:

- `/usr/local/bin` - Multiple `selenium` users
- `${HOME}/.local/bin` - Single user
- `bin` - Python virtual environment

Make sure that the driver file is located in your system's [`PATH`](https://en.wikipedia.org/wiki/PATH_(variable)#Unix_and_Unix-like) and [executable](https://en.wikipedia.org/wiki/Chmod) (e.g., `chmod a+x geckodriver`).

The following example loads the homepage for Selenium:

```python
from time import sleep

from selenium import webdriver

try:
    browser = webdriver.Firefox()
    # Loads Firefox instance
except Exception as exc:
    print(f'Exception occurred: {exc}')
else:
    print(type(browser))
    # <class 'selenium.webdriver.firefox.webdriver.WebDriver'>

    browser.get('https://www.selenium.dev/')
    # Loads selenium.dev in Firefox instance
    sleep(5)  # Give time to view page before closing browser
finally:
    browser.quit()  # Ensure browser properly closed before program end
```

[`selenium.webdriver`](https://www.selenium.dev/documentation/webdriver/) refers to both the language bindings and the implementations of the individual browser controlling code. 

Since the Web has an intrinsically asynchronous nature, Webdriver does not track the active, real-time state of the [Document Object Model (DOM)](https://en.wikipedia.org/wiki/Document_Object_Model). This can cause some [challenges](https://www.selenium.dev/documentation/webdriver/waits/).

An `Options` object can be [created and used to configure](https://www.selenium.dev/documentation/webdriver/browsers/) a [`WebDriver`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webdriver.WebDriver) object. For example, the following sets up Firefox to run in headless mode (i.e., a web browser without a graphical user interface):

```python
from selenium import webdriver

opts = webdriver.FirefoxOptions()
opts.headless = True

try:
    browser = webdriver.Firefox(options=opts)
except Exception as exc:
    print(f'Exception occurred: {exc}')
else:
    print('Headless browser started.')
finally:
    browser.quit()
    print('Headless browser closed.')
```

There are [various strategies](https://selenium-python.readthedocs.io/locating-elements.html) for locating elements on a page. Strategies utilize the [`WebDriver.find_element()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webdriver.WebDriver.find_element) and [`WebDriver.find_elements()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webdriver.WebDriver.find_elements) methods.

`WebDriver.find_element()` returns a single [`WebElement`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement) object, representing the first element on the page that matches your query. `WebDriver.find_elements()` returns a list of `WebElement_*` objects for every matching element on the page.

The [`selenium.webdriver.common.by.By`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.by.By) class is used with these methods to locate elements within a document. Supported locator strategies include:

- [`By.CLASS_NAME`](https://selenium-python.readthedocs.io/locating-elements.html#locating-elements-by-class-name) - Locate an element by class name.
- [`By.CSS_SELECTOR`](https://selenium-python.readthedocs.io/locating-elements.html#locating-elements-by-css-selectors) - Locate an element using CSS selector syntax.
- [`By.ID`](https://selenium-python.readthedocs.io/locating-elements.html#locating-by-id) - Locate by `id` attribute of an element.
- [`By.LINK_TEXT`](https://selenium-python.readthedocs.io/locating-elements.html#locating-hyperlinks-by-link-text) - Locate by the link text used within an anchor tag. Element link text must be a _complete match_ to the provided text.
- [`By.NAME`](https://selenium-python.readthedocs.io/locating-elements.html#locating-by-name) - Locate by the `name` attribute of an element.
- [`By.PARTIAL_LINK_TEXT`](https://selenium-python.readthedocs.io/locating-elements.html#locating-hyperlinks-by-link-text) - Locate by the link text used within an anchor tag. Element link text _only needs to contain_ the provided text.
- [`By.TAG_NAME`](https://selenium-python.readthedocs.io/locating-elements.html#locating-elements-by-tag-name) - Locate an element by tag name.

If an element is not found, `selenium` raises a [`NoSuchElement`](https://selenium-python.readthedocs.io/api.html#selenium.common.exceptions.NoSuchElementException) exception.

Some helpful `WebElement` object attributes and methods include:

- [`WebElement.clear()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.clear) - Clears the text if it is a text entry element.
- [`WebElement.get_attribute()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.get_attribute) - Gets the given attribute or property of the element.
- [`WebElement.is_displayed()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.is_displayed) - Whether the element is visible to a user. Returns `True` if the element is visible, otherwise `False`.
- [`WebElement.is_enabled()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.is_enabled) - Returns whether the (input) element is enabled. Returns `True` if the element is enabled, otherwise `False`.
- [`WebElement.is_selected()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.is_selected) - Returns whether the (checkbox or radio buttons) element is selected. Returns `True` if the element is enabled, otherwise `False`.
- [`WebElement.location`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.location) - The location of the element in the renderable canvas. A dictionary with keys `'x'` and `'y'` for the position of the element in the page.
- [`WebElement.tag_name`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.tag_name) - This element's `tagName` property (e.g., `'a'` for an `<a>` element).
- [`WebElement.text`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.text) - The text of the element.

The following example attempts to find the _Selenium Level Sponsors_ `div` on <https://www.selenium.dev/> and reports if it was found:

```python
from selenium import webdriver
from selenium.webdriver.common.by import By

CSS_SEL = 'div.row:nth-child(7)'

try:
    browser = webdriver.Firefox()
except Exception as exc:
    print(f'Driver creation exception: {exc}')
else:
    browser.get('https://www.selenium.dev/')

try:
    elem = browser.find_element(By.CSS_SELECTOR, CSS_SEL)
except Exception as exc:
    print(f'Element search exception: {exc}')
else:
    print(f'Found {elem.tag_name} element with {CSS_SEL} selector.')
    # Found div element with div.row:nth-child(7) selector.
finally:
    browser.quit()
```

[`WebElement.click()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.click) clicks an element. This example loads <https://www.selenium.dev/> and clicks the _Documentation_ page link:

```python
from time import sleep

from selenium import webdriver
from selenium.webdriver.common.by import By

LK_TXT = 'Documentation'

try:
    browser = webdriver.Firefox()
except Exception as exc:
    print(f'Driver creation exception: {exc}')
else:
    browser.get('https://www.selenium.dev/')

try:
    elem = browser.find_element(By.LINK_TEXT, LK_TXT)
except Exception as exc:
    print(f'Element search exception: {exc}')
else:
    elem.click()
    # Loads Documentation page
    sleep(10)  # Gives time to view the page before closing the browser
finally:
    browser.quit()
```

[`WebElement.send_keys()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.send_keys) simulates typing into an element. The following opens <https://www.selenium.dev/>, selects the search box, and enters a search for the term _webdriver_:

```python
from time import sleep

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

IDS = (
    'docsearch',
    'docsearch-input'
)
SRCH_TERM = 'webdriver'

try:
    browser = webdriver.Firefox()
except Exception as exc:
    print(f'Driver creation exception: {exc}')
else:
    browser.get('https://www.selenium.dev/')

try:
    elem = browser.find_element(By.ID, IDS[0])
except Exception as exc:
    print(f'Search box find exception: {exc}')
else:
    elem.click()

try:
    elem = browser.find_element(By.ID, IDS[1])
except Exception as exc:
    print(f'Search form find exception: {exc}')
else:
    elem.send_keys(SRCH_TERM)
    sleep(2)  # Provide time for search results to populate
    elem.send_keys(Keys.ENTER)
    sleep(10)
finally:
    browser.quit()
```

If a form has a submit button, [`WebElement.submit()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webelement.WebElement.submit) can be used to submit the form. In the example above, `Keys.ENTER` is used to simulate pressing **Enter** on the keyboard to launch the search, instead.

`Keys.ENTER` is one of many special keys available in [`selenium.webdriver.common.keys.Keys`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys). Some of the more common keys include:

- [`Keys.DOWN`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.DOWN), [`Keys.UP`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.UP), [`Keys.LEFT`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.LEFT), [`Keys.RIGHT`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.RIGHT) - The keyboard arrow keys.
- [`Keys.ENTER`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.ENTER), [`Keys.RETURN`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.RETURN) - The **Enter** and **Return** keys.
- [`Keys.HOME`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.HOME), [`Keys.END`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.END), [`Keys.PAGE_DOWN`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.PAGE_DOWN), [`Keys.PAGE_UP`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.PAGE_UP) - The **Home**, **End**, **Pagedown**, and **Pageup** keys.
- [`Keys.ESCAPE`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.ESCAPE), [`Keys.BACK_SPACE`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.BACK_SPACE), [`Keys.DELETE`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.DELETE) - The **Escape**, **Backspace**, and **Delete** keys.
- [`Keys.F1`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.F1)...[`Keys.F12`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.F12) - The **F1** through **F12** keys.
- [`Keys.TAB`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.common.keys.Keys.TAB) - The **Tab** key.

Since the `<html>` tag is the base tag in HTML files, calling `browser.find_element(By.TAG_NAME, 'html')` is a good place to send keys to the general web page.

Selenium can also simulate various browser actions via `WebDriver` methods:

- [`WebDriver.back()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webdriver.WebDriver.back) - Goes one step backward in the browser history.
- [`WebDriver.forward()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webdriver.WebDriver.forward) - Goes one step forward in the browser history.
- [`WebDriver.refresh()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webdriver.WebDriver.refresh) - Refreshes the current page.
- [`WebDriver.quit()`](https://selenium-python.readthedocs.io/api.html#selenium.webdriver.remote.webdriver.WebDriver.quit) - Quits the driver and closes every associated window.

## Documentation

- [Beautiful Soup Documentation](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
- [BeeWare Project](https://beeware.org/project/)
    - [Tutorial](https://docs.beeware.org/en/latest/)
    - [briefcase](https://pypi.org/project/briefcase/)
        - [Briefcase Documentation](https://briefcase.readthedocs.io/en/stable/)
            - [Reference](https://briefcase.readthedocs.io/en/stable/reference/index.html)
    - [toga](https://pypi.org/project/toga/)
        - [Examples](https://github.com/beeware/toga/tree/main/examples)
        - [Toga Documentation](https://toga.readthedocs.io/en/stable/)
            - [Reference](https://toga.readthedocs.io/en/latest/reference/index.html)
- [Flake8](https://flake8.pycqa.org/en/latest/)
    - [Configuration](https://flake8.pycqa.org/en/latest/user/configuration.html)
    - [Error / Violation Codes](https://flake8.pycqa.org/en/latest/user/error-codes.html)
    - [Full Listing of Options and Their Descriptions](https://flake8.pycqa.org/en/latest/user/options.html)
- [Flake8 Rules](https://www.flake8rules.com/)
- [mypy Documentation](https://mypy.readthedocs.io/en/stable/index.html)
- [PEP Index](https://peps.python.org/pep-0000/)
    - [Docstring Conventions](https://peps.python.org/pep-0257/)
    - [Style Guide](https://peps.python.org/pep-0008/)
    - [The Wheel Binary Package Format](https://peps.python.org/pep-0427/)
    - [Type Hints](https://peps.python.org/pep-0484/)
- [pip Documentation](https://pip.pypa.io/en/stable/)
- [Pyodide](https://pyodide.org/en/stable/)
    - [Pyodide Documentation](https://pyodide.org/en/stable/index.html)
    - [Pyodide Terminal Emulator](https://pyodide.org/en/stable/console.html)
- [PyScript](https://pyscript.net/)
    - [PyScript Documentation](https://pyscript.github.io/docs/latest/)
    - [Examples](https://pyscript.com/@examples)
- [Python Developer’s Guide](https://devguide.python.org/)
- [Python Documentation](https://docs.python.org/)
    - [`argparse`](https://docs.python.org/3/library/argparse.html)
        - [`argparse` Tutorial](https://docs.python.org/3/howto/argparse.html)
    - [Data Model](https://docs.python.org/reference/datamodel.html)
    - [Glossary](https://docs.python.org/glossary.html)
    - [HOWTOs](https://docs.python.org/howto/index.html)
    - [Installing Python Modules](https://docs.python.org/3/installing/index.html#installing-python-modules)
    - [Language Reference](https://docs.python.org/reference/index.html)
    - [Standard Library](https://docs.python.org/library/index.html)
    - [Typing](https://docs.python.org/library/typing.html)
    - [Unit Testing Framework](https://docs.python.org/library/unittest.html)
        - [`unittest.mock`](https://docs.python.org/library/unittest.mock.html)
- [Python-Markdown Documentation](https://python-markdown.github.io/)
- [Python Packaging User Guide](https://packaging.python.org/en/latest/)
    - [Glossary](https://packaging.python.org/en/latest/glossary/)
    - [Guides](https://packaging.python.org/en/latest/guides/)
        - [Writing your `pyproject.toml`](https://packaging.python.org/en/latest/guides/writing-pyproject-toml/#writing-pyproject-toml)
    - [Overview of Python Packaging](https://packaging.python.org/en/latest/overview/)
    - [PyPA Specifications](https://packaging.python.org/en/latest/specifications/)
        - [Dependency Specifiers](https://packaging.python.org/en/latest/specifications/dependency-specifiers/#dependency-specifiers)
        - [`pyproject.toml` Specification](https://packaging.python.org/en/latest/specifications/pyproject-toml/)
    - [The Packaging Flow](https://packaging.python.org/en/latest/flow/)
    - [Tutorials](https://packaging.python.org/en/latest/tutorials/)
        - [Installing Packages](https://packaging.python.org/en/latest/tutorials/installing-packages/)
- [Python Wiki](https://wiki.python.org/moin/)
- [Requests Documentation](https://requests.readthedocs.io/en/latest/)
- [Selenium Documentation (Official)](https://www.selenium.dev/documentation/)
- [Selenium Documentation (Unofficial)](https://selenium-python.readthedocs.io/)
- [setuptools](https://pypi.org/project/setuptools/)
    - [Setuptools Documentation](https://setuptools.pypa.io/en/latest/)
- [The Hitchhiker's Guide to Python](https://python-guide.readthedocs.io/en/latest/)
- [wheel](https://pypi.org/project/wheel/)
    - [wheel Documentation](https://wheel.readthedocs.io/en/stable/index.html)