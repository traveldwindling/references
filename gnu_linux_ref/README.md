# GNU/Linux Reference

A collection of GNU/Linux reference materials.

<details>
    <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## Packages

### Debian

**CLI System**

<details>
    <summary>Show/Hide Debian CLI System Commands</summary>

```console
sudo apt install -y \
    at \
    bash-completion \
    binutils \
    curl \
    dnsutils \
    efibootmgr \
    firewalld \
    flatpak \
    ftp \
    genisoimage \
    git \
    kpartx \
    libimage-exiftool-perl \
    locate \
    lsb-release \
    lshw \
    lsscsi \
    mokutil \
    ncal \
    nmap \
    nodejs \
    npm \
    ocrmypdf \
    pipx \
    python3-pip \
    python3-venv \
    rsync \
    shellcheck \
    speedtest-cli \
    sqlite3 \
    tcpdump \
    tesseract-ocr \
    traceroute \
    tree \
    vim \
    wget \
    whois
```

```console
echo 'fawltydeps flake8 ipython pipdeptree pipreqs radicale yt-dlp' |
    xargs -n 1 pipx install
```

</details>

**GUI System**

<details>
    <summary>Show/Hide Debian GUI System Commands</summary>

```console
sudo apt install -y \
    dconf-editor \
    debian-reference-en \
    enscript \
    ffmpeg \
    firewall-config \
    gedit \
    jpegoptim \
    optipng \
    pandoc \
    pdfarranger \
    pdfgrep \
    texlive \
    texlive-xetex \
    virt-manager \
    wkhtmltopdf \
    wl-clipboard
```

</details>

Add yourself to the `libvert` group:

`sudo adduser "${USER}" libvirt`

### Fedora

**CLI System**

<details>
    <summary>Show/Hide Fedora CLI System Commands</summary>

```console
sudo dnf install -y \
    genisoimage \
    lshw \
    nmap \
    nodejs \
    npm \
    ocrmypdf \
    perl-Image-ExifTool.noarch \
    pipx \
    policycoreutils-python-utils \
    python3-pip \
    redhat-lsb-core \
    ShellCheck \
    speedtest-cli \
    sysstat \
    tesseract \
    vim-enhanced
```

```console
pipx install \
    fawltydeps \
    flake8 \
    ipython \
    pipdeptree \
    pipreqs
```

</details>

**GUI System**

<details>
    <summary>Show/Hide Fedora GUI System Commands</summary>

```console
sudo dnf install -y \
    enscript \
    firewall-config \
    gedit \
    jpegoptim \
    optipng \
    pandoc \
    pdfgrep \
    pdfshuffler \
    seahorse \
    texlive \
    texlive-scheme-full \
    texlive-xetex \
    virt-manager \
    wkhtmltopdf \
    wl-clipboard
```

</details>

Add yourself to the `libvert` group:

`sudo usermod -a -G libvirt "${USER}"`

## Vim

### Vim Modes

Vim has three foundational [modes](https://vimhelp.org/intro.txt.html#vim-modes):

1. Normal
2. Insert
3. Command-line

#### Normal Mode

[Normal](https://vimhelp.org/intro.txt.html#Normal) mode is the default mode for Vim, i.e., when you first start Vim, you will be placed in this mode. In Normal Mode, key presses are interpreted as normal editor commands.

You can always get to Normal Mode by pressing `Esc`.

#### Insert Mode

[Insert](https://vimhelp.org/insert.txt.html#Insert) mode is used to enter text into a file. It is the mode that is most like how you would use a normal word processing program.

You can enter Insert Mode by pressing either `i` or the `INS` key on a keyboard (if applicable).

#### Command-line Mode

[Command-line](https://vimhelp.org/cmdline.txt.html#Command-line) mode is used to enter line editing commands that are inherited from older line editors, like [ed](https://en.wikipedia.org/wiki/Ed_(text_editor)). If you do not have experience with line editors, this mode will probably take the most getting used to.

Here, each key press is an external command. These commands can include operations like writing a file's contents to disk or exiting Vim.

Command-line commands start with a `:` ([Ex](https://vimhelp.org/intro.txt.html#Ex) commands), `/`/`?` (search patterns), or `!` ([filter](https://vimhelp.org/change.txt.html#filter) commands).

### Vim Concepts

When working with Vim, there are [several key concepts](https://vimhelp.org/windows.txt.html#windows-intro) that it helps to internalize:

**Buffer**  
The in-memory text of a file. Each opened file gets its own buffer.

**Window**  
A viewport on a buffer.

**Tab Page**  
A collection of windows. Windows are comparable to layouts or templates.

It may help to think of windows and buffers as an X-Y-Z coordinate system. The X- and Y-axis create a window, and the buffers are stacked on the Z-axis.

### Keyboard Shortcuts

The best way to learn Vim is to start using it. This section has keyboard shortcuts for accomplishing useful tasks in Vim.

For the following examples, if an entry starts with a `$`, that means it is a command that you enter at the GNU/Linux command line. If an entry does not start with a `$`, it is a command that is entered _inside_ of the Vim program.

Some entries have actual examples to help clarify what a real command looks like.

#### Starting/Stopping Vim

`vim`  
Start Vim.

`$ vim ex_file...`  
Start Vim and edit a file.

`$ vim -p ex_file...`  
Start Vim with files in tabs.

`$ vim -S ex_session_file`  
Start Vim and source `ex_session_file` after loading the first file.

`$ vim +/ex_string ex_file`  
Open file with a search string (`ex_string`) and place cursor on first line with an instance of `ex_string` (e.g., `$ vim +/mangoes 'fruit.txt'`).

`$ vim +ex_line_num ex_file`  
Open file and place cursor on specific line (e.g., `$ vim +5 'fruit.txt'`).

`$ vim +ex_vim_option ex_file...`  
Set a Vim option from the command line.

`$ vim -r ex_file`  
Start Vim and edit a file in recovery mode after a system crash.

`$ vimdiff ex_file_1 ex_file_2`  
Compare two files to make edits.

[`:wq`](https://vimhelp.org/editing.txt.html#%3Awq)  
Write the current file and close the window.

[`:wqa`](https://vimhelp.org/editing.txt.html#%3Awqa)  
Write all changed buffers and exit Vim.

[`:q`](https://vimhelp.org/editing.txt.html#%3Aq)  
Quit the current window.

[`:qa`](https://vimhelp.org/editing.txt.html#%3Aqa)  
Exit Vim, unless there are some buffers that have been changed.

`:q!`  
Quit without writing, also when the current buffer has changes.

`:qa!`  
Exit Vim. Any changes to buffers are lost.

#### Setting Options

[`:set`](https://vimhelp.org/options.txt.html#%3Aset)  
Show all options that differ from their default value.

`:set all`  
Show all, except terminal options.

`:set ex_option`  
For a Toggle option, switch it on. For a Number amd String option, show its value.

[`:set number`](https://vimhelp.org/options.txt.html#%27number%27)  
Print the line number in front of each line.

[`:set nonumber`](https://vimhelp.org/options.txt.html#%27nonumber%27)  
Do not print the line number in front of each line.

[`:set showmode`](https://vimhelp.org/options.txt.html#%27showmode%27)  
If in Insert, Replace, or Visual mode, put a message on the last line.

[`:set spell`](https://vimhelp.org/options.txt.html#%27spell%27)  
When on, spell checking will be done.

`:set nospell`  
Disable spell checking.

#### Working With Files

[`:e ex_file`](https://vimhelp.org/editing.txt.html#%3Ae)  
Edit `ex_file`.

[`:r ex_file`](https://vimhelp.org/insert.txt.html#%3Ar)  
Insert the file `ex_file` below the cursor.

[`Ctrl+g`](https://vimhelp.org/editing.txt.html#CTRL-G)  
Prints the current file name, the cursor position, and the file status (readonly, modified, read errors, new file).

[`:w`](https://vimhelp.org/editing.txt.html#%3Aw)  
Write the whole buffer to the current file.

`:w ex_file`  
Write the specified lines to `ex_file`.

`:w! ex_file`  
Write the specified lines to `ex_file`. Overwrite an existing file.

[`:wa`](https://vimhelp.org/editing.txt.html#%3Awa)  
Write all changed buffers.

[`:syntax on`](https://vimhelp.org/syntax.txt.html#%3Asyntax)  
Turn syntax checking on.

`:syntax off`  
Turn syntax checking off.

[`:buffers`](https://vimhelp.org/windows.txt.html#%3Abuffers) or `:ls`  
Show all buffers.

Each buffer has a unique number that will not change.

Indicators include (characters in the same list are mutually exclusive):

- `u` - An unlisted buffer.
    - `%` - The buffer in the current window.
    - `#` - The alternate buffer for `:e #` and `Ctrl+^`
        - `a` - An active buffer that is loaded and visible.
        - `h` - A hidden buffer that is loaded, but not currently displayed in a window.
            - `-` - A buffer with `modifiable` off
            - `=` - A readonly buffer.
            - `R` - A terminal buffer with a running job.
            - `F` - A terminal buffer with a finished job.
            - `?` - A terminal buffer without a job.
                - `+` - A modified buffer.
                - `x` - A buffer with read errors.

[`:Nb`](https://vimhelp.org/windows.txt.html#%3Ab) (where `N` is the unique buffer number) or `:b ex_buffer_name`  
Edit buffer from the buffer list.

[`:bn`](https://vimhelp.org/windows.txt.html#%3Abn)  
Go to next buffer in the buffer list.

[`:bp`](https://vimhelp.org/windows.txt.html#%3Abp)  
Go to previous buffer in the buffer list.

[`:bd`](https://vimhelp.org/windows.txt.html#%3Abd)  
Unload current buffer and delete it from the buffer list.

A buffer number can be used with this command to unload/delete a specific buffer (`:Nbd`).

[`:mks ex_session_file`](https://vimhelp.org/starting.txt.html#%3Amks)  
Write a Vim script that restores the current editing session.

[`:so ex_session_file`](https://vimhelp.org/repeat.txt.html#%3Aso)  
Read Ex commands from `ex_session_file`. Restores an editing session.

#### Moving Around

`Space` or [`l`](https://vimhelp.org/motion.txt.html#l)  
Move cursor one character right.

`Backspace` or [`h`](https://vimhelp.org/motion.txt.html#h)  
Move cursor one character left.

`↓` or [`j`](https://vimhelp.org/motion.txt.html#j)  
Move cursor down one line.

`3↓` or `3j`  
Use a force multiplier to move down three lines.

`↑` or [`k`](https://vimhelp.org/motion.txt.html#k)  
Move cursor up one line.

[`w`](https://vimhelp.org/motion.txt.html#w)  
Move forward one word.

[`b`](https://vimhelp.org/motion.txt.html#b)  
Move backward one word.

[`f{ex_char}`](https://vimhelp.org/motion.txt.html#f)  
Move to the next `{ex_char}` character on the line.

[`End`](https://vimhelp.org/motion.txt.html#%3CEnd%3E) or [`$`](https://vimhelp.org/motion.txt.html#%24)  
Move to the end of the line.

[`Home`](https://vimhelp.org/motion.txt.html#%3CHome%3E) or [`0`](https://vimhelp.org/motion.txt.html#0)  
Move to the first character of the line.

`NG` or `:N` (where `N` is the a line number)  
Move to a specific line (e.g., `10G` or `:10`).

[`G`](https://vimhelp.org/motion.txt.html#G) or [`:$`](https://vimhelp.org/cmdline.txt.html#%3A%24)  
Move to the first non-blank character of the last line of the file.

`1G` or `:0`  
Move to the first non-blank character of the first line of the file.

[`Page Down`](https://vimhelp.org/scroll.txt.html#%3CPageDown%3E) or `Ctrl+f`  
Move forward a page.

[`Page Up`](https://vimhelp.org/scroll.txt.html#%3CPageUp%3E) or `Ctrl+b`  
Move backward a page.

[`Ctrl+d`](https://vimhelp.org/scroll.txt.html#CTRL-D)  
Move forward a half-page.

[`Ctrl+u`](https://vimhelp.org/scroll.txt.html#CTRL-U)  
Move backward a half-page.

#### Searching For Text

[`/ex_pattern`](https://vimhelp.org/pattern.txt.html#%2F)  
Search forward for `ex_pattern` (e.g., `/pineapples`).

`ex_pattern` can be a regular expression.

[`?ex_pattern`](https://vimhelp.org/pattern.txt.html#%3F)  
Search backward for `ex_pattern`.

[`n`](https://vimhelp.org/pattern.txt.html#n)  
Move to next occurrence of search pattern.

[`N`](https://vimhelp.org/pattern.txt.html#N)  
Move to previous occurrence of search pattern.

`/\<ex_pattern\>`  
Search for an exact, whole word.

Also, you can place the cursor anywhere on the word in question and press the asterisk key (`*`) to accomplish the same thing.

[`:s/ex_old_string/ex_new_string/`](https://vimhelp.org/change.txt.html#%3As)  
Find and replace the first instance of a string on the current line (e.g., `:s/plums/blueberries/`).

`ex_old_string` and `ex_new_string` can be regular expressions.

`:{ex_line_start},{ex_line_end}s/ex_old_string/ex_new_string/`  
Find and replace the first instance of a string on a specified set of lines (e.g., `:1,5s/plums/blueberries/`).

You can use a `.` to represent the current line, and a `$` to represent the last line of the file (e.g., `:.,$s/plums/blueberries/`).

The `&` character can also be used to refer back to the text matched by your regular expression.

Example:

`:s/straw/&berry/`

The example above changes `straw` to `strawberry`.

`:s/ex_old_string/ex_new_string/g`  
Find and replace all instances of a string on the current line.

`:%s/ex_old_string/ex_new_string/g`  
Find and replace all instances of a string in an entire file.

#### Working With Text

[`a`](https://vimhelp.org/insert.txt.html#a)  
Append text after the cursor.

[`A`](https://vimhelp.org/insert.txt.html#A)  
Append text at the end of the line.

[`i`](https://vimhelp.org/insert.txt.html#i)  
Insert text before the cursor.

[`I`](https://vimhelp.org/insert.txt.html#I)  
Insert text before the first non-blank in the line.

[`o`](https://vimhelp.org/insert.txt.html#o)  
Begin a new line below the cursor and insert text.

[`O`](https://vimhelp.org/insert.txt.html#O)  
Begin a new line above the cursor and insert text.

[`s`](https://vimhelp.org/change.txt.html#s)  
Delete character into [unnamed [register](https://vimhelp.org/change.txt.html#registers)] and start insert.

This command adds new text at the cursor and pushes existing text to the right of the cursor down.

[`S`](https://vimhelp.org/change.txt.html#S)  
Delete line into unnamed register and start insert.

This command wipes out the entire line and lets you enter new text.

[`r`](https://vimhelp.org/change.txt.html#r)  
Replace the character under the cursor.

[`R`](https://vimhelp.org/change.txt.html#R)  
Enter [Replace](https://vimhelp.org/insert.txt.html#Replace) mode. Each character you type replaces an existing character, starting with the character under the cursor.

[`x`](https://vimhelp.org/change.txt.html#x)  
Delete character under the cursor into unnamed register.

`Nx`  
Delete `N` characters under the cursor into unnamed register.

[`X`](https://vimhelp.org/change.txt.html#X)  
Delete character before the cursor into unnamed register.

`NX`  
Delete `N` characters before the cursor into unnamed register.

`dw`  
Delete characters under the cursor to the end of the current word into unnamed register, removing the space after the word.

`de`  
Delete characters under the cursor to the end of the current word into unnamed register, keeping the space after the word.

`df{ex_char}`  
Delete characters under the cursor to the next occurrence of the `{ex_char}` character into unnamed register.

[`D`](https://vimhelp.org/change.txt.html#D) or `d$`  
Delete the characters under the cursor until the end of the line into unnamed register.

`d0`  
Delete the characters before the cursor to the beginning of the line into unnamed register.

[`dd`](https://vimhelp.org/change.txt.html#dd)  
Delete line into unnamed register.

`Ndd`  
Delete `N` lines into unnamed register.

`dL`  
Delete the characters under the cursor to the end of the screen into unnamed register.

`dG`  
Delete the characters under the cursor to the end of the file into unnamed register.

`d1G`  
Delete the characters under the cursor to the beginning of the file.

[`J`](https://vimhelp.org/change.txt.html#J)  
Join line below with line where cursor is placed.

[`u`](https://vimhelp.org/undo.txt.html#u)  
Undo the last operation.

[`Ctrl+r`](https://vimhelp.org/undo.txt.html#CTRL-R)  
Redo last undone change.

`:e!`  
Edit the current file always. Discard any changes to the current buffer. This is useful if you want to start all over again.

[`yy`](https://vimhelp.org/change.txt.html#yy)  
Yank (copy) line into unnamed register.

`Nyy`  
Yank `N` lines into unnamed register.

`yw`  
Yank characters under the cursor to the end of the word into unnamed register.

[`p`](https://vimhelp.org/change.txt.html#p)  
Put (paste) text from unnamed register after the cursor. 

[`P`](https://vimhelp.org/change.txt.html#P)  
Put text from unnamed register before the cursor.

`"ayy`  
Overwrite the `a` buffer with the current line.

`"Ayy`  
Append to the `a` buffer with the current line.

`"ap`  
Put the contents of the `a` buffer after the cursor.

`"aP`  
Put the contents of the `a` buffer before the cursor.

[`v`](https://vimhelp.org/visual.txt.html#v)  
Start [Visual](https://vimhelp.org/visual.txt.html#Visual) mode per character.

This command puts you into _Visual Selection_ mode (select the characters that you want to affect; the cursor should end on the last character to be affected). Here, you can perform searches and other types of operations (e.g., press `d` to cut a selection, `y` to copy a selection, `p` to paste a selection after the cursor, and `P` to paste a selection before the cursor).

[`V`](https://vimhelp.org/visual.txt.html#V)  
Start Visual mode linewise.

Also, Vim has a special [Visual Block](https://vimhelp.org/visual.txt.html#visual-block) mode that you can use to insert text into multiple lines simultaneously. To do this:

- Press `Ctrl+v`.
- Select the lines that you want to affect. The cursor should end on the last line to be affected.
- Press `I` to enter a special version of Insert Mode.
- Enter your desired text.
- Press `Esc`.

After a brief pause, your new text should be inserted into the lines you selected.

#### Window/Tab Management

[`:sp`](https://vimhelp.org/windows.txt.html#%3Asplit)  
Split current window horizontally.

A file can be passed to this command as an argument. The original file is placed on the bottom of the editor.

The height of the new window can be set by passing a number to the command like so, `:{ex_num}sp`.

[`:vs`](https://vimhelp.org/windows.txt.html#%3Avsplit)  
Split current window vertically.

A file can be passed to this command as an argument. The original file is placed on the right side of the editor.

[`Ctrl+w`](https://vimhelp.org/index.txt.html#CTRL-W)  
Activate _window_ mode for split windows (move by using direction keys or pressing `w`).

After activating window mode, additional commands are available:

- `s` - Open new horizontal split.
- `v` - Open new vertical split.
- `c` - Close window.
- `o` - Make current window the only one on the screen. All other windows are closed.

Once you switch to a window, that window becomes active and ready for you to begin issuing commands.

[`:close`](https://vimhelp.org/windows.txt.html#%3Aclose)  
Close selected split window.

[`:redraw!`](https://vimhelp.org/various.txt.html#%3Aredraw)  
Clear and redraw the screen right now.

[`:tabnew`](https://vimhelp.org/tabpage.txt.html#%3Atabnew)  
Open a new tab page with an empty window, after the current tab page.

`:tabnew ex_file`  
Open a new tab page and edit `ex_file`.

[`gt`](https://vimhelp.org/tabpage.txt.html#gt)  
Go to the next tab page. A tab number can be used with this command to go to a specific tab (`:Ngt`).

[`gT`](https://vimhelp.org/tabpage.txt.html#gT)  
Go to the previous tab page.

[`:tabclose`](https://vimhelp.org/tabpage.txt.html#%3Atabclose)  
Close current tab page.

#### External Commands

[`:sh`](https://vimhelp.org/various.txt.html#%3Ash)  
Start a shell.

[`:! ex_cmd`](https://vimhelp.org/various.txt.html#%3A%21)  
Execute `ex_cmd` with the shell (e.g., `:! wc %`; here, the `%` is a placeholder representing the current file you are working on in Vim).

[`:r! ex_cmd`](https://vimhelp.org/insert.txt.html#%3Ar%21)  
Execute `ex_cmd` and insert its standard output below the cursor or the specified line.

### `.vimrc` Configuration

<details>
    <summary>Show/Hide .vimrc Configuration</summary>

```vim
" Copy indent from current line when starting a new line
set autoindent

" Allow backspacing over indention, line breaks, and insertion start
set backspace=indent,eol,start

" Display a confirmation dialog when performing actions that would normally
" fail because of unsaved actions, like closing an unsaved file
set confirm

" Highlight the text line of the cursor
set cursorline

" Display as much as possible of the last line in a window
set display+=lastline

" Enable search highlighting
set hlsearch

" Enable incremental search
set incsearch

" Avoid wrapping a line in the middle of a word
set linebreak

" Increase max memory to use for pattern matching
set maxmempattern=100000

" Disable modelines
set nomodeline

" Show the line and column number of the cursor position, separated by a comma
set ruler

" If in Insert, Replace, or Visual mode, put a message on the last line
set showmode

" Enable syntax highlighting
syntax on

" Set the window's title
set title

" Speed up scrolling
set ttyfast

" Enable wild menu
set wildmenu

" Enable line wrapping
set wrap
```

</details>

## The Shell

### View Internal (Built-In) Commands

`help | less`

Force use of internal version of a command that also has a corresponding external version:

`builtin ex_cmd`

Useful when you wish to reimplement a shell builtin as a shell function, but need to execute the builtin within the function.

### Variables

All variables are shell variables, and some shell variables are also _environment variables_.

View all shell variables (and functions):

`set | less`

`set` can also set [Bash's positional parameters](https://www.gnu.org/software/bash/manual/html_node/Positional-Parameters.html#Positional-Parameters):

```console
$ set a b c d
$ echo "${2}"
b
```

Environment variables are copied to (i.e., available to) child processes, whereas regular shell variables are not. By convention, [internal shell variable](https://tldp.org/LDP/abs/html/internalvariables.html) and environment variable names are capitalized.

View all environment variables:

`env | sort | less`

Promote a shell variable to an environment variable:

`export ex_var...`

Revert an environment variable back to a shell variable:

`export -n ex_var...`

View variable types (attributes):

`help declare | less`

Set a variable type by giving it an attribute:

`declare -ex_attr_opt ex_var`

Remove a non-read-only variable or function from the environment:

`unset ex_obj`

Important shell variables include:

`BASH`  
The path used to execute the current instance of Bash.

`BASHOPTS`  
A colon-separated list of enabled options that were that were set using the `shopt` command. This can be useful for finding out if the shell environment will operate in the way that you want it to.

`BASH_SOURCE`  
An array variable whose members are the source filenames where the corresponding shell function names in the `FUNCNAME` array variable are defined. The shell function `${FUNCNAME[$i]}` is defined in the file `${BASH_SOURCE[$i]}` and called from `${BASH_SOURCE[$i+1]}`.

`BASH_SUBSHELL`  
The subshell level. This value is incremented by one within each subshell or subshell environment when the shell begins executing that environment. The initial value is `0`.

`BASH_VERSION`  
The version of Bash being executed, in human-readable form.

`BASH_VERSINFO`  
A read-only array that holds version information for the current instance of Bash being executed. You can view its contents by running `echo "${BASH_VERSINFO[@]}"`.

`COLUMNS`  
The number of columns wide that are being used to draw output on the screen. Used by the `select` command.

`DIRSTACK`  
The stack of directories that are available with the `pushd` and `popd` commands. Can also be displayed with the `dirs` command.

`EDITOR`  
Name of the current user's preferred editor.

`HISTCMD`  
The history number of the current command to be entered.

`HISTCONTROL`  
If set to `ignorespace`, lines that start with a space are not added to the history file. If set to `ignoredups`, a line that duplicates the previous line is ignored, regardless of the times it is repeated.

`HISTFILE`  
Defaults to `${HOME}/.bash_history` and is set in the environment at log in.

`HISTFILESIZE`  
Maximum number of lines stored in the command history file.

`HISTIGNORE`  
Specifies which command lines can be unsaved.

`HISTSIZE`  
Maximum number of commands allowed in command history memory.

`HISTTIMEFORMAT`  
If set and not null, its value is used as a format string for `strftime` to print the time stamp associated with each history entry displayed by the `history` command. Time stamps are written to the history file so that they may be preserved across shell sessions.

`HOME`  
The current user’s home directory.

`HOSTNAME`  
The hostname of the computer at this time.

`IFS`  
The _internal field separator_ to separate input on the command line. By default, a space.

`LANG`  
The current language and localization settings, including character encoding. Used to determine the locale category for any category not specifically selected with a `LC_` variable.

`LINES`  
The number of lines your display has. Used by the `select` command.

`LS_COLORS`  
Defines color codes that are used to optionally add colored output to the `ls` command. Colors are used to distinguish different file types.

`LOGNAME`  
Current user's username. Same as the `USER` variable.

`MAIL`  
The path to the current user’s mailbox.

`OLDPWD`  
The previous working directory as set by the `cd` command. This is kept by the shell in order to switch back to your previous directory by running `cd -`.

`PAGER`  
Governs which program the `man` command (and others) use to display textual output.

`PATH`  
List of directories containing executable programs that are eligible as external commands. When a user types in a command, the system will check directories in this order for the executable. `PATH` does not include the current directory (`.`), so if you want to run a program in the current directory, you can use the `./` syntax (this represents a null directory, which indicates the current directory at any given time), e.g., `./ex_program`.

`PIPESTATUS`  
An array of exit status values for the last foreground pipe (which may contain only a single command) that was executed. You can view its contents by running `echo "${PIPESTATUS[@]}"`.

`PS1`  
The primary command prompt definition (Prompt Statement 1). This is used to define what your prompt looks like when you start a shell session.

`PS2`  
Used to declare secondary prompts for when a command spans multiple lines.

`PS3`  
Used as the prompt for the `select` command. If not set, `select` prompts with a `#`.

`PWD`  
The current working directory as set by the `cd` command.

`RANDOM`  
Generates a random integer between `0` and `32767`. Assigning a value to this variable seeds the random number generator.

`SHELL`  
The full pathname to the shell. If not set at shell start, Bash assigns it the full pathname of the current user's login shell.

`SHELLOPTS`  
A colon-separated list of enabled shell options that were set with the `set` command.

`SHLVL`  
Shell level. A count of how deeply your Bash shell is nested. The count is incremented by one each time a new instance of Bash is started.

`SUDO_EDITOR`  
Name of the current user's favorite `sudo` editor.

`TERM`  
This specifies the type of terminal to emulate when running the shell. Different hardware terminals can be emulated for different operating requirements.

`UID`  
The user ID of the current user.

`USER`  
Current user's username; same as `LOGNAME`.

### Aliases

Aliases are created with the built-in `alias` command:

`alias ex_alias_name=ex_cmd`

View all aliases:

`alias | less`

Remove all aliases:

`unalias`

Remove a specific alias(es):

`unalias ex_alias_name...`

Aliases have special properties:

- They take priority over commands found in the shell environment's `PATH`, which is why they can be used to redefine extant commands. To temporarily negate an alias, escape it with a backslash (e.g., `\ex_alias_name`) or use the absolute path to the command in question.
- They are stored in the shell's memory, so they are only accessible to the _current_ shell (i.e., other programs and child processes of the same shell cannot access them).
- They are executed by the current shell and do not require the creation of a new process.
- They cannot have local variables or process arguments.

### Functions

Functions are created as follows:

```bash
function ex_func_name() {
    ex_cmds
}
```

Functions share the same special properties outlined for aliases, with the exception that they support local variables and the processing of arguments.

View all functions (and shell variables):

`set | less`

View only function names (does not include function _content_):

`declare -F | less`

### Command Precedence

1. Aliases
2. Keywords (e.g., `if`, `for`)
3. Functions
4. Internal commands (e.g., `cd`, `type`)
5. Scripts and external commands

### Options

View shell options:

`help set | less`

Set a shell option:

`set -o ex_opt`

Unset a shell option:

`set +o ex_opt`

### Shell Types

1. _Login shells_
    - The shell that you immediately obtain after logging into your system.

        Confirm that you are using a login shell by seeing if the process's name starts with a `-`:
        
        `echo "${0}"`

        Open a login shell for a user account:

        `sudo -i ex_username`

2. _Interactive shells_
    - The shell that you receive when you start a shell without any arguments, and its [standard input](https://en.wikipedia.org/wiki/Standard_streams#Standard_input_(stdin)) and [standard output](https://en.wikipedia.org/wiki/Standard_streams#Standard_output_(stdout)) channels are connected to a terminal.

        Terminal emulators in graphical environments are interactive shells.

        Open an interactive non-login shell for a user account:

        `sudo -s ex_username`

3. _Non-interactive shells_
    -  Shells that do not process any files when started or terminated.
    
        A shell is non-interactive if it is used to execute a shell script, or if a program uses a shell to run another program.

### Shells, Child Shells, and Subshells

Shell scripts are _child shells_ (i.e., child processes) that are run in non-interactive shells. Like all child processes, they inherit a copy of their parent's environment (i.e., copies of their parent's environment variables).

A _subshell_ is a _copy of a parent shell_. It has access to all of the available resources that the parent shell has access to (i.e., shell variables, aliases, functions).

Subshells are created when you:

- Execute a command in the background with an ampersand (`&`).
- Create a pipeline with a pipe (`|`). The pipe creates two subshells, one for the left side of the pipe and one for the right side of the pipe, and waits for both sides to terminate. If you set Bash's `lastpipe` option, the right side of the pipe will run in the original shell (i.e., the pipeline will create only one subshell).
- Use process substitution (`<()`, `>()`). `<()` creates a subshell with its standard output set to a pipe and expands to the name of the pipe. The parent (or another process) may open the pipe to communicate with the subshell. `>()` accomplishes the same, but with the pipe on the standard input.
- Create a coprocess with the `coproc` internal command. `coproc` creates a subshell and does not wait for it to terminate. The subshell's standard input and standard output are set to a pipe with the parent being connected to the other end of each pipe.
- Use the `-C ex_cmd` option with the internal `complete` command.
- Use command substitution (`$()`).

The shell variables `SHLVL` and `BASH_SUBSHELL` can be used to demonstrate the distinction between child shells and subshells. `SHLVL` starts at `1` and is incremented each time you move into a child shell. `BASH_SUBSHELL` starts at `0` and is incremented each time you move into a subshell.

```console
$ echo "${SHLVL}"
1
$ echo "${BASH_SUBSHELL}"
0
$ bash
$ echo "${SHLVL}"
2
$ echo "${BASH_SUBSHELL}"
0
$ exit
exit
$ echo "${SHLVL}"
1
$ echo "${BASH_SUBSHELL}"
0
$ echo "$(echo "${SHLVL}")"
1
$ echo "$(echo "${BASH_SUBSHELL}")"
1
$ 
```

Shell internal commands (e.g., `alias`, `cd`, `echo`) are not run as separate child processes, as they are part of the shell itself. They have access to all of the resources of the shell they are run from.

### Special Parameters

`0`  
Resolves to the name of the shell or shell script. If the first character of this parameter's contents is a `-`, your shell is a login shell.

`-`  
Resolves to the current options set for the shell. This is equivalent to what you see when run `echo "${SHELLOPTS}"`.

`#`  
Resolves to the number of positional parameters (arguments) that were passed on the command line.

`_`  
At shell startup, resolves to the absolute pathname used to start the shell or script that is executed, as specified in the environment or argument list. After startup, resolves to the last argument of the previous foreground command (also set to the full pathname used to start each executed command and placed in the environment exported to that command). When checking mail, set to the name of the mail file currently being checked.

`*`  
Resolves to all positional parameters, starting from `1`. When variable substitution occurs within double quotes, it resolves to a single word, with the value of each parameter separated by the first character of the _Internal Field Separator (IFS)_ variable. If the IFS is not set, the parameters are separated by spaces (e.g., `{$1 $2 $3}`).

`@`  
Resolves to all positional parameters, starting from `1`. When variable substitution occurs within double quotes, each parameter expands to a separate word (e.g., `$1 $2 $3`). This is an exception to the rule, i.e., _word splitting_ never normally occurs within quotes.

`$`  
Resolves to the process ID (PID) of the shell.

`!`  
Resolves to the PID of the most recently executed background process.

`?`  
Resolves to the [exit status](https://en.wikipedia.org/wiki/Exit_status) of the most recently executed foreground process. Can be used to check whether your command successfully completed or not (`0` denotes success and `1` denotes failure).

### Special Shell Characters

<table>
  <caption>Special Shell Characters</caption>
  <thead>
    <tr>
      <th>Character(s)</th>
      <th>Use</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>&#92;</code></td>
      <td>Escape next character</td>
    </tr>
    <tr>
      <td><code>&#60;</code></td>
      <td>Standard input redirection</td>
    </tr>
    <tr>
      <td><code>&#62;</code></td>
      <td>Standard output and standard error redirection</td>
    </tr>
    <tr>
      <td><code>&#124;</code></td>
      <td>Pipeline, logical OR</td>
    </tr>
    <tr>
      <td><code>&#47;</code></td>
      <td>Pathname directory separator</td>
    </tr>
    <tr>
      <td><code>&#42;</code></td>
      <td>Filename expansion (search pattern wildcard)</td>
    </tr>
    <tr>
      <td><code>&#63;</code></td>
      <td>Filename expansion (single character search pattern wildcard)</td>
    </tr>
    <tr>
      <td><code>&#91;</code> and <code>&#93;</code></td>
      <td>Character sets, test condition</td>
    </tr>
    <tr>
      <td><code>&#123;</code> and <code>&#125;</code></td>
      <td>Brace expansion, variable substitution, code blocks, placeholders</td>
    </tr>
    <tr>
      <td><code>&#126;</code></td>
      <td>Tilde expansion (current user home directory)</td>
    </tr>
    <tr>
      <td><code>&#36;</code></td>
      <td>Variable substitution, command substitution, represent the command prompt for non-root user</td>
    </tr>
    <tr>
      <td><code>&#61;</code></td>
      <td>Variable assignment</td>
    </tr>
    <tr>
      <td><code>&#40;</code> and <code>&#41;</code></td>
      <td>Command substitution, integer math, code blocks</td>
    </tr>
    <tr>
      <td><code>&#33;</code></td>
      <td>Command history, logical NOT</td>
    </tr>
    <tr>
      <td><code>&#39;</code></td>
      <td>Strong quotes</td>
    </tr>
    <tr>
      <td><code>&#34;</code></td>
      <td>Weak quotes</td>
    </tr>
    <tr>
      <td><code>&#35;</code></td>
      <td>Comments, represent the command prompt for root user</td>
    </tr>
    <tr>
      <td><code>&#38;</code></td>
      <td>Multi-stream redirection, logical AND, background a process</td>
    </tr>
    <tr>
      <td><code>&#59;</code></td>
      <td>Separate commands</td>
    </tr>
  </tbody>
</table>

### Expansion

#### Filename Expansion

Search patterns are expanded by the shell to match _extant_ object names.

`?`  
Matches any single character (except `/`) that must exist (1 repetition).

`*`  
Matches any string of characters (except `/`) that are optional (0 to many repetitions).

##### Character Sets

A _character set_ is denoted by square brackets (`[]`) and matches any _single_ character specified in the set of characters.

Match any single character not specified in the character set by adding a `!` to the beginning of the character set (`[!]`).

Specify a range within a character set by using a `-`.

#### Brace Expansion

_Brace expansion_ is used to generate strings on the command line from a pattern that you specify.

```console
$ touch {red,yellow,blue}.txt
$ ls
blue.txt  red.txt  yellow.txt
```

```console
$ echo {a,b,c}{1,2,3}.dat
a1.dat a2.dat a3.dat b1.dat b2.dat b3.dat c1.dat c2.dat c3.dat
```

```console
$ touch file_{1..5}.txt
$ ls
file_1.txt  file_2.txt  file_3.txt  file_4.txt  file_5.txt
```

### Command History

Run previous command  
`!!`

Previous command's last argument  
`!$`

### Quoting

_Quotes_ are used to denote strings on the command line.

#### Strong Quotes

Single quotes (`''`) are used for strict quoting and are used to specify literal strings. Inside of single quotes, expansion, substitution, and word splitting do not occur.

If you want to use backslash escape sequences in a literal string, precede it with a dollar sign (e.g., `$'This sentence ends with a newline.\n'`). Run `man 1 bash` for more information.

#### Weak Quotes

Double quotes (`""`) are less strict. Inside of double quotes, filename expansion, brace expansion, tilde expansion, and word splitting do _not_ occur, but variable substitution and command substitution _do_ occur. Also, [newline characters](https://en.wikipedia.org/wiki/Newline) are preserved.

#### No Quotes

Without quotes, two things happen at the command line:

1. The result of a variable or command substitution is split into words wherever it contains white space. Specifically, the result of the expansion is split at each character that appears in the value of the _Internal Field Separator (IFS)_ variable.

    If a sequence of separator characters contains white space (e.g., space, tab, or newline), the white space is counted as a single character.

2. Each field that results from splitting is interpreted as a search pattern (i.e., a _glob_) if it contains one of the following characters `\[*?`. If that pattern matches one or more file system objects, the pattern is replaced by the list of matching object names, i.e., filename expansion occurs.

An unquoted variable substitution, like `${ex_var}`, is known as the _split+glob operator_, where split refers to _word splitting_ and glob refers to _filename expansion_ (i.e., `${ex_var}` is replaced with the variable's value through variable substitution, and that value is then subject to word splitting and filename expansion).

In contrast, `"${ex_var}"` is just replaced with the variable's value through variable substitution. No word splitting or filename expansion follows.

The same goes for command substitution. `$(ex_cmd)` is a command substitution followed by a split+glob, while `"$(ex_cmd)"` is just a command substitution.

## The Environment

### Login Shell Environment Sourcing

1. `/etc/profile`
2. `/etc/profile.d/` files
3. `${HOME}/.bash_profile` or `${HOME}/.bash_login` or `${HOME}/.profile`
4. `${HOME}/.bashrc`
5. `${HOME}/.bash_logout` (upon quitting a login shell, and only if the file exists)

### Non-Login Shell Environment Sourcing

1. `/etc/bash.bashrc` or `/etc/bashrc` (which file is used depends on your GNU/Linux distribution)
2. `~/.bashrc`

When you quit an interactive non-login shell, no files are processed.

### Shell Environment Configuration

System-wide settings that apply to all users:

- `/etc/profile` for login shells
-  `/etc/bash.bashrc` or `/etc/bashrc` for non-login shells

User-specific settings:

- `${HOME}/.bash_profile`, `${HOME}/.bash_login`, or `${HOME}/.profile` for environment variables
- `${HOME}/.bashrc` for shell variables that customize the shell's behavior, shell options, aliases, and functions

Have `${HOME}/.bash_profile`, `${HOME}/.bash_login`, or `${HOME}/.profile` source `${HOME}/.bashrc` to enable user-specific settings in login shells.

## `test`

The `test` command can be used to compare either numbers or strings, and to check file properties via numerous tests:

`test ex_expression`

`[[ ex_expression ]]`

Each test returns either a `0` (`true`) or `1` (`false`).

The mapping of `0` to `true` and `1` to `false` is the opposite of what is observed in most programming languages. This is because the shell is both a user interface and a script interpreter. Since there are many potential ways for things to go wrong, there needs to be many ways to express an error, which makes non-zero values appropriate for error codes.

An expression can be negated by placing the logical NOT operator before it (`!`), e.g., `test ! ex_expression`.

The logical AND (`&&`) and logical OR (`||`) operators can be used to join multiple expressions, e.g., `test ex_expression_1 && test ex_expression_2`. The logical AND operator has precedence over the logical OR operator.

### Tests

The `test` command supports:

- Arithmetic tests
- File tests
- Shell tests
- String tests

#### Arithmetic Tests

`ex_integer_1 -eq ex_integer_2`  
`ex_integer_1` is equal to `ex_integer_2`.

`ex_integer_1 -ge ex_integer_2`  
`ex_integer_1` is greater than or equal to `ex_integer_2`.

`ex_integer_1 -gt ex_integer_2`  
`ex_integer_1` is greater than `ex_integer_2`.

`ex_integer_1 -le ex_integer_2`  
`ex_integer_1` is less than or equal to `ex_integer_2`.

`ex_integer_1 -lt ex_integer_2`  
`ex_integer_1` is less than `ex_integer_2`.

`ex_integer_1 -ne ex_integer_2`  
`ex_integer_1` is not equal to `ex_integer_2`.

_BEDMAS_ is observed:

1. Brackets/Braces/Parentheses
2. Exponents
3. Division
4. Multiplication
5. Addition
6. Subtraction

#### File Tests

Unary operator file tests:

`-a ex_file`  
True if `ex_file` exists. Equivalent to `-e`. This test is for use with the internal version of the `test` command.

`-b ex_file`  
True if `ex_file` exists and is a block special file.

`-c ex_file`  
True if `ex_file` exists and is a character special file.

`-d ex_file`  
True if `ex_file` exists and is a directory.

`-e ex_file`  
True if `ex_file` exists. Equivalent to `-a`.

`-f ex_file`  
True if `ex_file` exists and is a regular file.

`-g ex_file`  
True if `ex_file` exists and its setgid bit is set.

`-G ex_file`  
True if `ex_file` exists and is owned by the effective group ID (EGID).

`-h ex_file`  
True if `ex_file` exists and is a symbolic link. Equivalent to `-L`.

`-k ex_file`  
True if `ex_file` exists and its _sticky_ bit is set.

`-L ex_file`  
True if `ex_file` exists and is a symbolic link. Equivalent to `-h`.

`-N ex_file`  
True if `ex_file` exists and has been modified since it was last read.

`-O ex_file`  
True if `ex_file` exists and is owned by the effective user ID (EUID).

`-p ex_file`  
True if `ex_file` exists and is a named pipe (_FIFO, First In First Out_).

`-r ex_file`  
True if `ex_file` exists and is readable.

`-s ex_file`  
True if `ex_file` exists and its size is greater than zero.

`-S ex_file`  
True if `ex_file` exists and is a socket.

`-t ex_file_descriptor`  
True if `ex_file_descriptor` is open and refers to a terminal.

`-u ex_file`  
True if `ex_file` exists and its setuid bit is set.

`-w ex_file`  
True if `ex_file` exists and is writable.

`-x ex_file`  
True if `ex_file` exists and is executable.

Binary operator file tests:

`ex_file_1 -ef ex_file_2`  
True if `ex_file_1` and `ex_file_2` refer to the same device and inode numbers ([hard link](https://en.wikipedia.org/wiki/Hard_link) test).

`ex_file_1 -nt ex_file_2`  
True if `ex_file_1` is newer (according to modification date) than `ex_file_2`, or if `ex_file_1` exists and `ex_file_2` does not.

`ex_file_1 -ot ex_file_2`  
True if `ex_file_1` is older than `ex_file_2` (according to modification date), or if `ex_file_2` exists and `ex_file_1` does not.

#### Shell Tests

The following tests are for use with the internal version of the `test` command:

`-o ex_opt_name`  
True if the shell option `ex_opt_name` is enabled.

`-R ex_var`  
True if the shell variable `ex_var` is set and is a name reference.

`-v ex_var`  
True if the shell variable `ex_var` is set (i.e., has been assigned a value).

#### String Tests

`ex_str_1 == ex_str_2`  
True if `ex_str_1` and `ex_str_2` are equal, e.g., `[[ 'a' == 'a' ]]`.

`ex_str_1 != ex_str_2`  
True if `ex_str_1` and `ex_str_2` are not equal.

`ex_str_1 < ex_str_2`  
True if `ex_str_1` sorts before `ex_str_2` lexicographically, e.g., `[[ 'a' < 'b' ]]`. This test is for use with the internal version of the `test` command.

`ex_str_1 > ex_str_2`  
True if `ex_str_1` sorts after `ex_str_2` lexicographically. This test is for use with the internal version of the `test` command.

`-n ex_str`  
True if the length of `ex_str` is non-zero.

`-z ex_str`  
True if the length of `ex_str` is zero, e.g., `[[ -z 'GNU/Linux' ]]`.

## Bash String Operators

A built-in resource-light way to manipulate the values of variables.

A special character that denotes an operator is inserted between a variable's name and the right curly brace (`}`). Any argument that the operator may need to work on is inserted to the operator's right.

###  Substitution Operators

Deal with variables' default values, undefined variables, variable existence, and variables' substrings.

#### :-

`${ex_var:-ex_word}`  
If `ex_var` exists and is not null, return its value. Otherwise, return `ex_word`.

Lets you _return_ a default value if a variable is undefined.

```console
$ greeting='Hi there!'
$ echo "${greeting:-How are you?}"
Hi there!

$ greeting=''
$ echo "${greeting:-How are you?}"
How are you?
```

#### :=

`${ex_var:=ex_word}`  
If `ex_var` exists and is not null, return its value. Otherwise, set it to `ex_word`, and then return its value. If you omit the colon, the assignment takes place only if `ex_var` does not exist (i.e., an existing variable with no set value is left unchanged).

Lets you _set_ a variable to a default value if it is undefined, _and_ return that value. Bash's [positional parameters](https://tldp.org/LDP/abs/html/othertypesv.html) cannot be set in this way.

```console
$ greeting='Hi there!'
$ echo "${greeting:=How are you?}"
Hi there!

$ greeting=''
$ echo "${greeting:=How are you?}"
How are you?

$ echo "$greeting"
How are you?
```

#### :?

`${ex_var:?ex_message}`  
If `ex_var` exists and is not null, return its value. Otherwise, print `ex_var:`, followed by `ex_message`, and abort the current command or script. 

If you omit `ex_message`, the default message `parameter null or not set` is displayed via both the standard error and the standard output (if the shell is not interactive, it exits). If the colon is omitted, the message will only appear if the variable does not exist.

Lets you catch errors that result from variables being undefined.

```console
$ greeting='Hi there!'
$ echo "${greeting:?Make sure you set the \'greeting\' variable.}"
Hi there!

$ greeting=''
$ echo "${greeting:?Make sure you set the \'greeting\' variable.}"
greeting: Make sure you set the 'greeting' variable.
```

#### :+

`${ex_var:+ex_word}`  
If `ex_var` exists and is not null, return `ex_word`. Otherwise, return null.

Lets you test for the existence of a variable.

```console
$ greeting='Hi there!'
$ echo "${greeting:+The 'greeting' variable exists.}"
The 'greeting' variable exists.

$ greeting=''
$ echo "${greeting:+The 'greeting' variable exists.}"

$
```

#### :

`${ex_var:ex_offset:ex_length}`  
Returns the substring of `ex_var` starting at `ex_offset` and up to `ex_length` characters (`ex_offset` and `ex_length` are considered arithmetic expressions). Bash strings are zero-based, so the first character in `ex_var` is at position `0`. If `ex_length` is omitted, the substring starts at `ex_offset` and continues to the end of `ex_var`.

If `ex_offset` is negative, counting begins from the end of `ex_var`, and a space must be placed between the `:` and `ex_offset`. If `ex_length` is negative, it is interpreted as an offset in characters from the end of `ex_var`, rather than a number of characters, and the expansion is the characters between `ex_offset` and `ex_length`.

If `ex_var` is `@` or an indexed array subscripted by `@` or `*`, then `ex_length` is the number of positional parameters you'd like returned, starting at `ex_offset`.

Lets you return parts of a string. It performs substring expansion.

```console
$ greeting='Hi there!'
$ echo "${greeting:0:2}"
Hi

$ echo "${greeting:3}"
there!

$ echo "${greeting: -6}"
there!

$ echo "${greeting:0:-7}"
Hi

$ cat example.bash
#!/usr/bin/env bash

echo "${@:1:2}"

$ example.bash one two three
one two

$ my_array=(one two three)
$ echo "${my_array[@]:1:2}"
two three
```

### Pattern Matching Operators

Used to match portions of a variable's string value against patterns. These patterns can contain [shell wildcard characters](https://tldp.org/LDP/GNU-Linux-Tools-Summary/html/x11655.htm).

Pattern matching operators do not change the contents of variables. They work on-the-fly and can be used with other commands (e.g., `echo`) to return your desired output.

#### &#35;

`${ex_var#ex_pattern}`  
If the pattern matches the beginning of the variable's value, delete the shortest part that matches `ex_pattern` and return the rest.

```console
$ my_file='/home/tux/Documents/test.txt'
$ echo "${my_file#/*/}"
tux/Documents/test.txt
```

#### &#35;&#35;

`${ex_var##ex_pattern}`  
If the pattern matches the beginning of the variable's value, delete the longest part that matches `ex_pattern` and return the rest.

```console
$ my_file='/home/tux/Documents/test.txt'
$ echo "${my_file##/*/}"
test.txt
```

#### %

`${ex_var%ex_pattern}`  
If the pattern matches the end of the variable's value, delete the shortest part that matches `ex_pattern` and return the rest.

```console
$ my_file='/home/tux/Documents/test.txt'
$ echo "${my_file%/*}"
/home/tux/Documents
```

#### %%

`${ex_var%%ex_pattern}`  
If the pattern matches the end of the variable's value, delete the longest part `ex_pattern` that matches and return the rest.

```console
$ my_file='/home/tux/Documents/test.txt'
$ echo "${my_file%%/*}"

$
```

#### / and //

`${ex_var/ex_pattern/ex_str}` and `${ex_var//ex_pattern/ex_str}`  
The longest match to `ex_pattern` in `ex_var` is replaced by `ex_str`. In the `${ex_var/ex_pattern/ex_str}` form, only the first match is replaced. In the `${ex_var//ex_pattern/ex_str}` form, all matches are replaced. If `ex_str` is null, the matches are deleted.

If `ex_pattern` begins with a `#`, it must match at the start of the variable. If `ex_pattern` begins with a `%`, it must match with the end of the variable.

```console
$ my_file='/home/tux/Documents/tux/test.txt'
$ echo "${my_file/tux/new_tux}"
/home/new_tux/Documents/tux/test.txt

$ echo "${my_file//tux/new_tux}"
/home/new_tux/Documents/new_tux/test.txt

$ echo "${my_file/tux/}"
/home//Documents/tux/test.txt

$ echo "${my_file//tux/}"
/home//Documents//test.txt

$ my_string='stuff like that is good stuff'
$ echo "${my_string/#stuff/junk}"
junk like that is good stuff

$ echo "${my_string/%stuff/junk}"
stuff like that is good junk

$ cat example.bash
#!/usr/bin/env bash

echo "${@/two/2}"

$ example.bash one two three
one 2 three
```

If `ex_var` is `@` or `*`, the operation is applied to each positional parameter in turn, and the expansion is the output.  If `ex_var` is an array variable subscripted with `@` or `*`, the operation is applied to each member of the array in turn, and the expansion is the output.

### Length Operator

`${#ex_var}`  
Returns the length of the value of the variable as a character string.

```console
$ greeting='Hi there!'
$ echo ${#greeting}
9
```

## File System Object Management

### Object Types

- `-` Regular file
- `b` Block device file
- `c` Character device file
- `d` Directory
- `l` Symbolic link
- `n` Network file
- `p` FIFO (First In, First Out, i.e., a [named pipe](https://en.wikipedia.org/wiki/Named_pipe))
- `s` Socket

### Permission Trios

<table>
  <caption>Permission Trios</caption>
  <tbody>
    <tr>
      <td>rwx</td>
      <td>rwx</td>
      <td>rwx</td>
    </tr>
    <tr>
      <td>u</td>
      <td>g</td>
      <td>o</td>
    </tr>
  </tbody>
</table>

### Permission Bit Values

The permission bit values can be:

- `r` Permission to read
    - Grants ability to view file contents.
    - For directories, grants ability to view directory contents. If `x` permission bit is also set, grants the ability to view object attributes with a long listing (`ls -l`).
- `w` Permission to write
    - Grants ability to change file contents.
    - For directories, if the `x` permission bit is also set, grants the ability to add/delete items from directories.
- `x` Permission to execute
    - Grants the ability to execute a file. If file is a script and `r` permission bit is also set, grants the ability to execute the script.
    - For directories, grants the ability to use `cd` to change to a directory and use the directory in a pathname.
- `s` Special Bit Permission, i.e., [set user ID (SUID) and set group ID (SGID)](https://en.wikipedia.org/wiki/Setuid)
    - `SUID` (`4XXX` `XXsXXXXXX`) allows users to run programs as the User Owner of the program. Usually, this is the root user.
    - For directories, `GUID` (`2XXX` `XXXXXsXXX`) automatically gives group ownership of all new files created in the directory to the Group Owner of the directory.

        For files, `GUID` allows users to run a program as the Group Owner of the file. Usually, this is used to access other files assigned to that group.

- `t` Sticky Bit Permission
    - The `Sticky Bit` (`1XXX` `XXXXXXXXt`) is used to keep non-User Owners from deleting files in a common directory. In a sticky bit directory, only the User Owner of an object, the User Owner of the sticky bit directory, or the root user can delete an object in the directory.
- `-` No permissions

After the permission trios,  a `.` or `+` pertain to [SELinux](https://en.wikipedia.org/wiki/Security-Enhanced_Linux) and [Access Control Lists (ACLs)](https://en.wikipedia.org/wiki/Access-control_list), respectively.

### umask

Default permissions for file system objects is set by the system's _umask_:

```console
$ umask
0022
$ umask -S
u=rwx,g=rx,o=rx
```

Default permissions for files and directories when no umask is set:

- `666` for files
- `777` for directories

Formula for determining bit permission values when setting a umask:

`Maximum default value - umask value = new bit permission value`

### Setting Different Permissions For Files and Directories In a Directory Tree

```console
sudo chmod -R ex_dir_perm ex_dir &&
    sudo find ex_dir -type f -exec chmod ex_file_perm '{}' \;
```

### User Ownership

Only the root user can change the User Owner of an object.

`sudo chown ex_new_user_owner ex_obj`

### Group Ownership

A non-root user can change the Group Owner of an object if the user is the User Owner of the object and a member of the group that they are changing group ownership of the object to.

`chown :ex_new_group_owner ex_obj`

Otherwise, you will need to be acting as the root user to change group ownership.

`sudo chown :ex_new_group_owner ex_obj`

### User and Group Ownership

`sudo chown ex_user_owner:ex_group_owner ex_obj`

### Temporarily Change Current User's Primary Group

`newgrp ex_group`

Useful when you want to create multiple new file system objects that need to have a specific Group Owner (that is distinct from your user's normal primary group).

When you are done with the `newgrp` command, enter `exit` to leave the new shell that was created when you issued the command and revert back to your user's normal primary group.

If the group in question has been assigned a password, you will need to enter that password to use the `newgrp` command, unless you have a password set for your user account and you are a member of the group that you are temporarily switching to.

## `find`

The `find` command syntax is:

`find ex_options ex_dir_tree_to_search... ex_search_expression`

Some of `find`'s options include:

`-P`  
Never follow symbolic links. This is the default behavior for `find`.

`-L`  
Follow symbolic links.

`-H`  
Do not follow symbolic links, except while processing command line arguments. The information about the link itself is used as a fallback if the object pointed to by the symbolic link cannot be examined.

You can provide `find` with a space-separated list of directory trees to search, as well.

By default, `find` is recursive, outputs hidden objects, and includes the `-print` action, which prints the name of each found object on the standard output, followed by a newline.

If `ex_dir_tree_to_search` is an absolute path name, `find` will return absolute path names. If `ex_dir_tree_to_search` is a relative path name, `find` will return relative path names.

### Expressions

The part of the command after the list of starting point(s) (i.e., `ex_dir_tree_to_search`) is the _expression_ (i.e., `ex_search_expression`). This expression is a query specification that describes how to match file system objects and what to do with the items that are matched.

A `find` expression is composed of:

- **Actions** Actions have side effects and return either `true` or `false`, usually based on whether or not they are successful.
- **Global Options** Global options affect the operation of actions and tests specified on any part of the command line. Global options always return `true`.
- **Operators** Operators join together the other items within the expression.
- **Positional Options** Positional options affect only actions and tests that follow them. Positional options always return `true`.
- **Tests** Tests return a `true` or `false` value, usually on the basis of some property of an object that is being considered.

There are many potential values for each of the items listed above, but the following are some of the most useful ones.

#### Actions

`-delete`  
Delete files. `true` if removal succeeded. If the removal failed, an error message is issued.

`-exec ex_command`  
Execute a command provided to the action as an argument. `true` if exit status `0` is returned. All following arguments to `find` are taken to be arguments to the command until a `\;` is encountered. The string `'{}'` is replaced by the current object name being processed.

`-ls`  
`true`. List current object in `ls -dils` format on the standard output.

`-ok ex_command`  
Like the `-exec` action, but prompts the user first. If the user agrees (`y`), the command is run. Otherwise (`n`), returns `false`. This is a good way to test the results before executing a potentially dangerous command.

`-print`  
`true`. Print the full object name on the standard output, followed by a newline. This is the default behavior of `find`.

`-print0`  
`true`. Print the full object name on the standard output, followed by a [null character](https://en.wikipedia.org/wiki/Null_character). This allows object names that contain newlines or other types of white space to be correctly interpreted by programs that process `find` output.

`-prune`  
`true`. If the object is a directory, do not descend into it.

If `-depth` is given, `-prune` has no effect.

`-delete` implies `-depth`, so `-delete` and `-prune` cannot be used together.

#### Global Options

`-help`  
Print a summary of the command line usage of `find`, and then exit.

`-maxdepth ex_depth`  
Descend at most `ex_depth` levels of directories below the command line arguments. `ex_depth` is a non-negative integer. `-maxdepth 0` means only apply the tests and actions to the starting point(s) themselves.

`-xdev`  
Do not descend directories on other file systems.

#### Operators

`!`  
The logical NOT (e.g., `! ex_expression`).

`-a`  
The logical AND (e.g., `ex_expression_1 -a ex_expression_2` ).

`-o`  
The logical OR (e.g., `ex_expression_1 -o ex_expression_2`).

It is best to enclose your `find` operator expressions in escaped parentheses (e.g., `\( ex_expression_1 -a ex_expression_2 \)`).

If you do not specify an operator, `-a` is assumed.

#### Positional Options

`-daystart`  
Measure times from the beginning of today, rather than from 24 hours ago (for `-amin`, `-atime`, `-cmin`, `-ctime`, `-mmin`, and `-mtime` tests).

`-regextype ex_reg_ex_type`  
Changes the regular expression syntax understood by the `-regex` and `-iregex` tests that occur later on the command line. `ex_reg_ex_type` can be `findutils-default`, `awk`, `egrep`, `ed`, `emacs`, `gnu-awk`, `grep`, `posix-awk`, `posix-basic`, `posix-egrep`, `posix-extended`, `posix-minimal-basic`, and `sed`.

`-warn`, `-nowarn`  
Turn warning messages on or off, respectively.

#### Tests

`-atime ex_time`  
Specify object access time. `ex_time` can be an exact number, `+ex_time` for greater than the number, or `-ex_time` for less than the number.

`-ctime ex_time`  
Specify object change time. `ex_time` can be an exact number, `+ex_time` for greater than the number, or `-ex_time` for less than the number.

`-group ex_group`  
Specify a group owner. `ex_group` can be either a group name or a group ID (GID).

`-iname ex_pattern`  
Specify a base case-insensitive object name pattern (i.e., the path with the leading directories removed).

`-inum ex_inode_number`  
Specify [inode](https://en.wikipedia.org/wiki/Inode) number.

`-iregex ex_reg_ex`  
Like `-regex`, but the match is case insensitive.

`-links ex_reference_count`  
Specify reference count (i.e., [hard link](https://en.wikipedia.org/wiki/Hard_link) count).

`-mtime ex_time`  
Specify object modification time. `ex_time` can be an exact number, `+ex_time` for greater than the number, or `-ex_time` for less than the number.

`-name ex_pattern`  
Specify a base object name pattern (i.e., the path with the leading directories removed).

`-path ex_pattern`  
Specify an entire object name pattern.

`ex_pattern` is interpreted by `find` as starting with the starting-point specified on the command line and is not converted to an absolute pathname (e.g., `cd '/' ; find 'tmp' -path '/tmp'` never finds anything).

`-perm ex_permissions_set`  
Specify permissions set. Matches the _exact_ [mode](https://en.wikipedia.org/wiki/Modes_(Unix)).

`-regex ex_reg_ex`  
Object path name matches regular expression.

`-size ex_size`  
Specify an object size (default 512 byte-blocks). `ex_size` can be the exact number, `+ex_size` for greater than the number, or `-ex_size` for less than the number. `ex_size` can be in bytes (`c`), kibibytes (`k`), mebibytes (`M`), and gibibytes (`G`).

`-type ex_type`  
Specify object type. Accepted types include block (`b`), character (`c`), directory (`d`), regular file (`f`), symbolic link (`l`), named pipe (`p`), and socket (`s`).

`-user ex_user`  
Specify a user owner. `ex_user` can be a user name or a user ID (UID).

### Examples

More than most commands, examples help illustrate how the `find` command works.

#### Find All Objects With the Object Name `foo.txt`

```console
$ find '/home/amnesia/' -name 'foo.txt'
/home/amnesia/foobar/foo.txt
/home/amnesia/Documents/foobar/foo.txt
/home/amnesia/foo.txt
```

#### Find All Objects With an Object Name That Starts With `foo`

Adding search patterns to your expression can make `find` more flexible.

```console
$ find '/home/amnesia/' -name 'foo*'
/home/amnesia/foo_4.txt
/home/amnesia/foo_3.txt
/home/amnesia/foobar
/home/amnesia/foobar/foo_2.txt
/home/amnesia/foobar/foo.txt
/home/amnesia/foo_1.txt
/home/amnesia/foo_2.txt
/home/amnesia/Documents/foobar
/home/amnesia/Documents/foobar/foo_2.txt
/home/amnesia/Documents/foobar/foo.txt
/home/amnesia/foo.txt
/home/amnesia/foo_5.txt
```

#### Find All Directories With a Name That Starts With `foo`

```console
$ find '/home/amnesia/' -type d -name 'foo*'
/home/amnesia/foobar
/home/amnesia/Documents/foobar
```

#### Find All Directories With a Name That Starts With Either `D` or `P`

```console
$ find '/home/amnesia/' -type d \( -name 'D*' -o -name 'P*' \)
/home/amnesia/Pictures
/home/amnesia/Documents
/home/amnesia/Public
/home/amnesia/Downloads
/home/amnesia/Desktop
```

#### Find All Objects Accessed Two Days Ago

```console
$ find '/home/amnesia/' -atime 2
/home/amnesia/.cache/event-sound-cache.tdb.7bf962cd8dcd427f95d5b537cddf5e72.x86_64-pc-linux-gnu
```

#### Find All Objects Modified More Than Two Days Ago

```console
$ find '/home/amnesia/' -mtime +2
/home/amnesia/.cache/flatpak
/home/amnesia/.cache/flatpak/system-cache
/home/amnesia/foo_5.txt
```

#### Find All Objects Changed Less Than Two Days Ago

```console
$ find '/home/amnesia/' -ctime -2
/home/amnesia/.cache/tracker/meta.db
/home/amnesia/.cache/tracker/meta.db-wal
/home/amnesia/.cache/tracker/locale-for-miner-apps.txt
/home/amnesia/.cache/gnome-software/shell-extensions
/home/amnesia/.cache/gnome-software/shell-extensions/gnome.json
```

#### Find All Objects Owned By `amnesia` User

```console
$ find '/home/amnesia/' -user 'amnesia' | head
/home/amnesia
/home/amnesia/.ssh
/home/amnesia/Videos
/home/amnesia/Pictures
/home/amnesia/Music
/home/amnesia/Documents
/home/amnesia/Public
/home/amnesia/Templates
/home/amnesia/Downloads
/home/amnesia/.ICEauthority
```

Above, the output of the `find` command is piped into the `head` command, so that we only see the first ten lines of command output.

#### Find All Objects Less Than One Gibibyte in Size

```console
$ find '/home/amnesia/' -size -1G
/home/amnesia/.local/share/tracker/data/.meta.isrunning
/home/amnesia/.local/share/gnome-settings-daemon/input-sources-converted
/home/amnesia/.local/share/gnome-shell/gnome-overrides-migrated
/home/amnesia/.gnupg/S.gpg-agent.ssh
/home/amnesia/.gnupg/S.gpg-agent.browser
/home/amnesia/.gnupg/S.gpg-agent.extra
/home/amnesia/.gnupg/S.gpg-agent
/home/amnesia/.gnome2_private/.placeholder
/home/amnesia/.gnome2/accels/.placeholder
```

#### Find All Files Named `foo.txt` and Exclude `bar` Directory

```console
$ find '/home/amnesia/' \
    -path '/home/amnesia/bar' -prune -o \
    -type f -name 'foo.txt' -print
/home/amnesia/Downloads/foo.txt
```

#### Find All Files Named `Foo.txt` or `foo.txt` and Remove Them

`find '/home/amnesia/' -type f -iname 'foo.txt' -exec rm -f '{}' \;`

`-exec` starts a new process for every file system object found by `find`. This can be inefficient, and you can address this by using the `xargs` command.

For example, you can redo the command above as such:

`find '/home/amnesia/' -print0 -type f -iname 'foo.txt' | xargs -0 -n 1 -r rm -f`

`xargs` (Execute as Arguments) takes the file system objects found by the `find` command and passes them to `rm` as arguments. `xargs` can have problems with object names that contain spaces or newlines, but you can get around this by telling `find` and `xargs` to use [null bytes](https://en.wikipedia.org/wiki/Null_character) instead of newlines. `-print0` tells `find` to use null bytes to separate its results, instead of newlines. `-0` helps `xargs` understand this kind of input.

`-n1` tells `xargs` to use, at max, one argument per command line, and `-r` (`--no-run-if-empty`) makes sure `xargs` only runs if it receives input from `find`.

## User/Group Management

### `sudo`

There are several commands that can be used to substitute (switch) user accounts on a GNU/Linux system. These commands can be used to switch to any user account, but they are often used to gain access to the root user.

The `sudo` (_substitute user do_) program allows permitted users to execute a command as the root user (or another user), as specified by the system's security policy.

Your user's Real User Identifier (RUID) is used to determine the login name with which to query the security policy. Since the system is keeping track of which actual user is employing the `sudo` command, a log can be maintained for auditing purposes.

- Depending on how it is configured, `sudo` can ask for either the root user's password or the password of the user using the `sudo` command.
- A correctly entered password is retained for 15 minutes (this time period is configurable). You can manually expire the cached password by running `sudo -k` (`-k` is short for `--reset-timestamp`).
- All commands executed via `sudo` are logged using the system's logging mechanism. Failures to use `sudo` will appear in the `/var/log/auth.log` file for Debian and in `/var/log/messages` and/or `/var/log/secure` file on Fedora. A typical entry will include:
    - Calling username
    - Terminal information
    - Working directory where command was executed
    - User account invoked
    - Command used, with arguments

    A `sudo` entry in a log looks something like `Jun 10 09:50:38 tails sudo: gnu : TTY=pts/2 ; PWD=/home/gnu ; USER=root ; COMMAND=/usr/bin/passwd`.

You can grant a user the ability to use the `sudo` command in at least three ways:

1. Create an entry for the user in `/etc/sudoers`.
2. Create a file in `/etc/sudoers.d/` with the user's username as the filename. Then, create an entry inside this file for the user. The permissions for this file should be `440`.
3. Create an entry for the `sudo` group in the `/etc/sudoers` file and add the user to this group.

Some GNU/Linux distributions extend `sudo` privileges to the first user account created during installation and completely disable log-ins to the root user account. This prevents naive users from working as the root user all of the time.

#### /etc/sudoers

The `/etc/sudoers` file controls the users that are allowed to use the `sudo` command and for what purposes.

Example Debian `/etc/sudoers` file:

<details>
    <summary>Show/Hide <code>/etc/sudoers</code> File</summary>

```cfg
#
# This file MUST be edited with the 'visudo' command as root.
#
# Please consider adding local content in /etc/sudoers.d/ instead of
# directly modifying this file.
#
# See the man page for details on how to write a sudoers file.
#
Defaults	env_reset
Defaults	mail_badpass
Defaults	secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Host alias specification

# User alias specification

# Cmnd alias specification

# User privilege specification
root	ALL=(ALL:ALL) ALL

# Allow members of group sudo to execute any command
%sudo	ALL=(ALL:ALL) ALL

# See sudoers(5) for more information on "#include" directives:

#includedir /etc/sudoers.d
```

</details>

The `%sudo	ALL=(ALL:ALL) ALL` line is what allows members of the `sudo` group to execute any command. The percentage sign (`%`) indicates that this a _group definition_, versus the `root	ALL=(ALL:ALL) ALL` line which is a _user definition_.

The `%sudo	ALL=(ALL:ALL) ALL` line can broken down like so:

`%sudo`  
The name of the group. GNU/Linux distributions like Fedora use a `%wheel` group, instead (e.g., `%wheel ALL=(ALL) ALL`).

`ALL=`  
The hosts on the network that the rule applies to (in this case, all hosts).

`(ALL:ALL)`  
Specifies which users and groups the members of this group can run commands as. Here, all users and all groups are covered.

`ALL`  
Specifies which commands can be run by members of this group. For this example, all commands are included here.

The `%sudo	ALL=(ALL:ALL) ALL` line's syntax can be generalized as follows:

`who where=(as user:as group) what`

Or:

`user host=(run as) command`

For example, here is a `/etc/sudoers` entry for the `gnu` user:

`gnu ALL=(ALL) ALL`

Wherever a login name is expected, you may specify a group too. Prefix the group's name with a percent sign to indicate that it is a group, as we saw for the `sudo` group above (e.g., `%operators ALL=/usr/local/bin/do-backup`).

By default, `sudo` runs all commands as root, but you can also specify another user. For example, with `gnu ALL=(mysql) /usr/bin/mysqladmin`, `gnu` can run the `mysqladmin` command as the `mysql` user. To do so, the `gnu` user must run a command like `sudo -u mysql mysqladmin flush-privileges`.

When you only specify a command (without parameters), arbitrary parameters are allowed:

`gnu ALL=/usr/bin/cancel`.

When no parameters should be allowed, the only parameter specified in the `sudoers` file must be an empty string `gnu ALL=/usr/bin/passwd ''`.

In this case, `gnu` would _only_ be allowed to change the password of the root user when using `passwd` with the `sudo` command.

Be careful about commands like `vi` that allow users to execute shell commands. These commands are also executed with root privileges. To prevent this, you could add a rule like `gnu ALL= NOEXEC: /usr/bin/vi`. This line stops `vi` from running child processes.

You can also specify multiple commands:

`gnu ALL=/usr/bin/cancel [a-z]+,/usr/bin/cupsenable [a-z]+`

The above command allows `sudo cancel`, as well as `sudo cupsenable`, both with (at least) one parameter.

When you specify a directory name (as an absolute pathname with a trailing slash), this stands for all executable files in that directory (but _not subdirectories_), e.g., `gnu ALL=/usr/bin/`.

A nice feature of `sudo` is the `-e` (`--edit`) or `sudoedit` command. These commands create a temporary copy of the file to be edited and then pass that copy to your default editor. Afterwards, it checks whether the copy has been changed, compared to the original, and if so, the copy replaces the original file.

This method has convenient advantages, like the fact that the editor only runs with the ordinary user's privileges so that they cannot spawn a root shell from the editor. Incidentally, the `/etc/sudoers` file is evaluated by `sudoedit` when it decides whether a user gets to use this feature or not.

`/etc/sudoers` supports many options that control the function of `sudo`. Run `man 8 sudo` and `man 5 sudoers` for more information.

##### `visudo`

Changes can be made to the `/etc/sudoers` file with your system's default editor by using the `visudo` command (`visudo` checks the `VISUAL` environment variable first, then the `EDITOR` environment variable, and finally the `editor` setting in `/etc/sudoers` before falling back to `vi`):

`# visudo`

`visudo` ensures that only one user is editing `/etc/sudoers` at a time and performs a syntax check immediately after the file is saved (and before putting your new configuration into effect). This is important because, in case of an error, you do not want to end up with a configuration file that will break `sudo`. A manual check of `/etc/sudoers` can be done at any time by running the `# visudo -c` (`-c` is short for `--check`) command.

##### sudoer Aliases

If you have several user accounts that you want to grant the same sudoer permissions, you can add them to a user alias.

`User_Alias ex_alias = ex_users`

`ex_users` is a comma-separated list of users. For example:

```cfg
# Installer user alias definition
User_Alias INSTALLERS = gnu,tux,monty

# INSTALLERS permissions definition
INSTALLERS ALL=/usr/bin/dnf
```

Above, the `User_Alias` keyword is used to create an `INSTALLERS` user alias in the `/etc/sudoers` file and with three users added to it. Then, the permissions associated with this new `INSTALLERS` alias were defined with the `INSTALLERS ALL=/usr/bin/dnf` line.

Aliases can be set for hosts (`Host_Alias`) and commands (`Cmnd_Alias`), as well.

Use `Host_Alias` to specify what systems the users can run the commands on. The following is a host alias for a network's file servers:

`Host_Alias FILESERVERS = fs_1,fs_2`

Use `Cmnd_Alias` to define an alias that contains the commands (using the full path) that you want the user you just defined to be able to run. Separate multiple commands with commas.

This is a command alias used to group storage-related commands:

`Cmnd_Alias KILLPROCS = /bin/kill,/usr/bin/killall`

Command aliases can be mixed with literal commands in the same rule:

`gnu ALL=PRINTING,/usr/bin/accept [a-z]+`

A leading `!` character can be used to exclude commands that would otherwise be allowed by a configuration:

`gnu ALL=/usr/bin/passwd [a-z]+,!/usr/bin/passwd root`

Different kinds of aliases can be combined:

`INSTALLERS FILESERVERS=(root) KILLPROCS`

##### Defaults

You can manually expire a cached password by running `sudo -k`. This behavior can be changed in `/etc/sudoers` by use of the `Defaults` keyword.

For example, the following line sets the timeout to `0` (i.e., password caching is disabled):

`Defaults timestamp_timeout=0`

#### Using `sudo`

To start a non-login, interactive shell for the root user, you can use `sudo`'s `-s` (`--shell`) option:

`sudo -s`

When changing to the root user with `sudo`, you are _not_ asked for the root user's password. You are asked for the _sudoer's_ password, i.e., your user account's password. This is advantageous, since it isolates permissions between multiple sudoers.

If you want a login shell for the root user, you can use `sudo`'s `-i` (`--login`) option:

`sudo -i`

If you provide `sudo`'s `-u` (`--user=`) option with a specific username as an argument, you can switch to that user's account, instead of the root user:

`sudo -i -u ex_login_name`

Often, `sudo` is used to run a command as a different user (usually the root user) via a non-login shell. This can be done by appending the command to the `sudo` command. If a specific user is not specified, the root user is assumed:

`sudo ex_command`

To run a command as a user other than the root user via a non-login shell, pass the username in question to `sudo`'s `-u` option:

`sudo -u ex_login_name ex_cmd`

The following example prints out a hello message to the standard output as the `gnu` user:

```console
sudo -u gnu echo Hello, world!
Hello, world!
```

## Package Management

### Debian

Debian binary packages are `.deb` packages whose names follow this format:

`ex_pkg_name_ex_ver_num-ex_release_num_ex_arch.deb`

Packages that contain only documentation or architecture-independent scripts use `all` for `ex_arch`, instead.

A Debian `.deb` binary package is an archive created using the `ar` command (part of the `binutils` package, `# apt install binutils`) and generally contains three components:

1. The `debian-binary` file contains the version number of the package format.
2. `control.tar.xz` contains Debian-specific scripts and control files.
3. `data.tar.xz` has the actual package files.

During installation, `control.tar.xz` is unpacked first. This makes it possible for a `preinst` script to be executed prior to unpacking the actual package files. After that, `data.tar.xz` is unpacked and the package is configured, if necessary, by executing the `postinst` script from `control.tar.xz`.

`ar` is used to create these packages, versus a command like `tar`, because `ar` archives only contain flat sets of files. This makes `ar` suitable for building sets of assembled object code in separate files, while `tar` is more suitable for packaging up a directory for distribution.

In addition to a `.deb` binary package, each Debian package has several source code files associated with it. The `.deb` binary package is created from the source code files of the package.

A Debian package's source code is contained in a `.orig.tar.gz` archive. All changes for each package version's source code are placed in a separate `.debian.tar.xz` archive. These `.debian.tar.xz` files include fixes and customizations to the software itself.

For each package version's source code, there is also a _Debian Source Control_  file (using the `.dsc` suffix) containing checksums for the original source code archive and the changes archive. The `.dsc` file is signed by the Debian maintainer in charge of the package.

To summarize, a given Debian package will have the additional source code files associated with it:

1. An original source code archive (`ex_program_version.number.orig.tar.gz`).
2. A Debian-specific changes archive (`ex_program_version.number-release_number.debian.tar.xz`).
3. A source control file (`ex_program_version.number-release_number.dsc`), which provides checksums for the above two archives and is signed by the Debian package's maintainer.

#### Low-Level

Low-level package management on Debian is handled by [Debian Package (dpkg)](https://en.wikipedia.org/wiki/Dpkg).

`/var/lib/dpkg/`  
dpkg database directory.

`/etc/dpkg/dpkg.cfg`  
dpkg configuration file.

Options for `dpkg` can be given on the command line or placed in `/etc/dpkg/dpkg.cfg`. For options placed in the `/etc/dpkg/dpkg.cfg` file, dashes (`-`) at the start of option names must be omitted.

[Debian package priorities](https://www.debian.org/doc/debian-policy/ch-archive#s-priorities), from highest to lowest:

`required`  
Packages that are necessary for proper operation of the system (usually because dpkg depends on them).

`important`  
Encompasses packages one would expect to be available on a GNU/Linux system.

`standard`  
Adds those packages that make sense for a networked system (but not an overly restrictive system) running in text mode. This priority describes what you get if you install Debian without selecting any additional packages.

`optional`  
This is the default priority for the majority of the archive. Applies to everything you might want to install if you have no particular requirements (e.g., the [X11 Graphical User Interface](https://en.wikipedia.org/wiki/X_Window_System), or GUI). Packages with a priority of `optional` may conflict with each other.

`extra`  
This priority is deprecated. Use the `optional` priority instead. This priority should be treated as equivalent to `optional`.

Packages may not depend on packages of lower priority. For this to hold in all cases, the priorities of some packages have been deliberately adjusted.

[Debian package dependencies](https://www.debian.org/doc/debian-policy/ch-relationships.html) include:

`Depends`  
This declares an absolute dependency. The named packages must be configured for the package in question to be configured. Specific versions of the packages may be called for.

`Recommends`  
This declares a strong, but not absolute, dependency (i.e., a non-absolute, but obvious dependency). Named packages should nearly always be installed along with this package, unless there are unusual circumstances.

`Suggests`  
This is used to declare that one package may be more useful with one or more other packages (i.e., the named packages are useful in connection with the package, but not required).

`Enhances`  
Like `Suggests`, but in reverse. It is used to declare that a package can enhance the functionality of another package.

`Pre-Depends`  
Like `Depends`, except the named packages must be completely installed before installation of the package in question can begin. For example, this type of dependency is used if the package's installation scripts absolutely require software from the other package(s).

`Breaks`  
When one binary package declares that it breaks another, dpkg will refuse to allow the package which declares `Breaks` to be unpacked unless the broken package is deconfigured first, and it will refuse to allow the broken package to be reconfigured.

In short, this package cannot be installed until the named package(s) is uninstalled.

`Conflicts`  
When one binary package declares a conflict with another using a `Conflicts` field, dpkg will refuse to allow them to be unpacked on the system at the same time. This is a stronger restriction than `Breaks`, which prevents the broken package from being configured while the breaking package is in the `Unpacked` state, but allows both packages to be unpacked at the same time.

In short, this package cannot be installed at the same time as the named package(s).

##### Commands

`dpkg -l`  
List all packages in the dpkg database. The `-l` option can be passed the name of a package to only see a listing for that package.

Shell search patterns are supported.

The first three columns of output show the _desired action_, _package status_, and _errors_.

Desired Action:

- `u` Unknown
- `i` Install
- `h` Hold
- `r` Remove
- `p` Purge

Package Status:

- `n` Not-installed
- `c` Config-files
- `H` Half-installed
- `U` Unpacked
- `F` Half-configured
- `W` Triggers-awaiting
- `t` Triggers-pending
- `i` Installed

Error Flags

- `<empty>` (none)
- `R` Reinst-required

Packages with version `<none>` are part of the distribution, but are either not installed on the current system (i.e., status `un`), or have been removed (i.e., status `pn`).

`dpkg --get-selections > ex_file.txt`  
List all package selections and save to a file.

`# dpkg --set-selections < ex_file.txt`  
Set package selections using a file read from the standard input. This can be used to install dpkg packages from a file.

`ex_file.txt` contains a list of package names, one package per line. You can make such a file with the `dpkg --get-selections > ex_file.txt` command.

`dpkg -L ex_pkg...`  
List the files in a dpkg package.

`dpkg -S ex_filename_string...`  
Search the dpkg database for a filename string.

This command's output gives you the file path for the file, as well as the package that it is associated with. It can be used to see which package (if any) is associated with a given file.

Shell search patterns are supported.

If you are looking for the package for a file that is not on your system, you can use the search form on <https://www.debian.org/distrib/packages#search_contents>. This allows you to search all Debian distributions and architectures, as well as to search for exact filename matches and filenames containing certain search terms.

`dpkg --info ex_pkg.deb`  
Display information about a `.deb` package, including its dependencies.

`dpkg -s ex_pkg...`  
Display information about a dpkg package, including its dependencies.

Aside from the package name, this command's output includes information about the package's _status_ and _priority_, and its approximate area of interest (i.e., `Section:`). The `Maintainer:` is the person that is in charge of the package on behalf of the Debian project.

`# dpkg -i ex_pkg.deb...`  
Install a `.deb` package. After running this command, run `# apt-get install -f` to resolve any broken dependencies (`-f` is short for `--fix-broken`).

A dpkg package can be reinstalled from a `.deb` package with the `# apt-get reinstall ex_pkg.deb` command.

`# dpkg --unpack ex_pkg.deb...`  
Unpack a `.deb` package.

`# dpkg --configure ex_pkg.deb...`  
Configure a `.deb` package.

`dpkg --force-help`  
Display `dpkg` forcing options.

`# dpkg -i --force-depends ex_pkg.deb...`  
Force install a `.deb` package with dependency errors. Turns all dependency problems into warnings.

This can be used when a package requires one or more other packages that either have not yet been installed, or that are not included in the same installation operation.

Sometimes, packages do not depend on a particular other package, but on a _virtual_ package that can be provided by any of several other packages. A list of virtual package names can be found at <https://www.debian.org/doc/packaging-manuals/virtual-package-names-list.yaml> (direct download link).

`# dpkg -i --force-overwrite ex_pkg.deb...`  
Force install a `.deb` package that tries to unpack a file that already exists on the system and belongs to a different package (i.e., overwrite a file from one package with another).

`# dpkg -i --force-conflicts ex_pkg.deb...`  
Force install a `.deb` package that conflicts with other packages.

`# dpkg -r ex_pkg...`  
Remove a dpkg package.

The package's configuration files (i.e., all the files listed in the `conffiles` file within `control.tar.gz`) are kept in order to facilitate a subsequent re-installation of the package.

`# dpkg -r --force-remove-reinstreq ex_pkg...`  
Force removal of a dpkg package that requires re-installation.

`# dpkg -r --force-remove-essential ex_pkg...`  
Force removal of a dpkg package that that is essential for other packages.

`# dpkg -P ex_pkg...`  
Remove a dpkg package, along with its configuration files

`# dpkg -i ex_pkg.deb...`  
Upgrade a dpkg package from a `.deb` package.

The older version of `ex_pkg` is uninstalled before configuration of the new version. If an error occurs during installation, the old version can often be restored.

If an earlier version of the package is installed and set to `hold` (e.g., using a tool like `aptitude`), newer versions of the package will be prevented from being installed.

`dpkg-source -x ex_pkg.dsc`  
Reconstruct a `.deb` package's source code. The package's source control file is provided to the `-x` option as an argument. The `.deb` package's original source archive (`ex_pkg.orig.tar.gz`) and Debian-specific changes archive (`ex_pkg.debian.tar.xz`) must be in the same directory as the source control file.

If a directory location is not provided to the command as a second argument (if provided, this directory must not yet exist, as `dpkg-source` will create it), the source package is extracted into a directory named `source-version` under the current working directory.

`dpkg-source` is also used when generating source archives and Debian change files during Debian package preparation. This command is installed as part of the `dpkg-dev` package, which may need to manually installed from Debian's repository on your system (`# apt install dpkg-dev`).

`# dpkg-reconfigure ex_pkg...`  
Reconfigure an already installed dpkg package.

`alien --to-deb ex_pkg...`  
Convert a non-`.deb` package to a `.deb` package. `--to-deb` is the default option and can be left out if that is the desired outcome.

`alien` does not deal with dependencies, so those must be manually addressed (i.e., run `# apt-get install -f`). To assemble and disassemble `.rpm` files, the `rpm` program must be installed (which is available as a package for Debian in its repository (`# apt install rpm`).

There is no guarantee that the resulting package created by this command will be viable. The farther down a package sits in the system, the smaller the probability that `alien` will create a working package for your GNU/Linux distribution.

`debsums ex_pkg...`  
Verify a dpkg package.

This command compares the SHA256 checksums of the individual files with the content of the corresponding file in `/var/lib/dpkg/info`. If an actual file's checksum does not match the set value, `FAILED` is displayed in place of `OK`.

#### High-Level

High-level package management on Debian is handled by the [Advanced Package Tool (APT)](https://en.wikipedia.org/wiki/APT_(software)).

`/var/lib/apt/`  
APT database directory.

`/etc/apt/apt.conf.d/`  
APT configuration directory.

`/etc/apt/sources.list`  
APT repository sources file.

`/etc/apt/sources.list.d/`  
APT repository sources directory.

APT's installation history can be viewed by running `less /var/log/apt/history.log`.

The APT infrastructure only trusts package sources for which a public GPG key has been placed in the `/etc/apt/trusted.gpg.d/` directory. The `apt-key` program is used to maintain the files in this directory. The current public keys for Debian package sources are contained in the `debian-archive-keyring` package and can be renewed by updating the files in `/etc/apt/trusted.gpg.d/` (Debian rotates the keys on a yearly basis).

##### Commands

`apt-cache policy`  
List the priorities of configured APT repositories. If provided a package as an argument, prints out detailed information about the priority selection of the provided package.

`apt list ex_pkg...`  
List an APT package. Shell search patterns are supported.

Contrast this to `dpkg -l` (`--list`) for `dpkg`.

`apt search ex_reg_ex`  
Search APT repositories for a given regular expression.

Contrast this to `dpkg -S ex_string...` (`--search`) for `dpkg`.

`apt show ex_pkg...`  
Display APT package information, including its dependencies, installation and download size, sources the package is available from, the description of the package's content, and more.

Contrast this to `dpkg --info ex_pkg.deb` and `dpkg -s ex_pkg...` (`--status`) for `dpkg`.

`apt depends ex_pkg...`  
Display all dependencies of an APT package.

This command shows all the dependencies of an APT package, as well as the names of packages fulfilling that dependency. A vertical bar in a line indicates that the dependency in that line, or the one in the following line, must be fulfilled.

Contrast this to `dpkg --info ex_pkg.deb` and `dpkg -s ex_pkg...` (`--status`) for `dpkg`.

`apt rdepends ex_pkg...`  
Display all packages depending on an APT package.

If a package occurs several times in the output, it is probably because the original package specified it several times, typically with version numbers.

`apt-cache stats`  
Display an overview of the contents of the package cache.

`# apt install ex_pkg...`, `# apt install ex_pkg.deb...`  
Install an APT or `.deb` package (e.g., `./ex_package.deb`).

This command will also install or upgrade all packages mentioned in `Depends:` dependencies, as well as any packages that these packages depend upon.

`apt install` can be used to simultaneously install and remove packages (e.g., `# apt install ex_pkg_to_remove- pkg_to_install`).

Contrast to `# dpkg -i ex_pkg.deb...` (`--install`) for `dpkg`.

`# apt-get install -f`  
Attempt to correct a system with broken dependencies in place.

`apt-mark showmanual > ex_file.txt`  
List manually installed APT packages and save to a file.

Contrast this to `dpkg --get-selections > ex_file.txt` for `dpkg`.

`# apt install "$(cat ex_file.txt)"`  
Install APT packages listed in a file.

Contrast this to `# dpkg --set-selections < ex_file.txt` for `dpkg`.

`# apt-get reinstall ex_pkg...`  
Reinstall an APT package.

`apt download ex_pkg`  
Download an APT package's `.deb` file to the current directory.

`apt-get source ex_pkg`  
Download an APT package's source code to the current directory.

This command also works if `ex_pkg` is one of several that have been created from a differently named source package.

Before running `apt-get source`, you may need to install the `dpkg-dev` package (`# apt install dpkg-dev`).

`# apt remove ex_pkg...`  
Remove an APT package.

Contrast to `# dpkg -r ex_pkg...` (`--remove`) for `dpkg`.

`# apt purge ex_pkg...`  
Remove an APT package and its configuration files.

Contrast to `# dpkg -P ex_pkg...` (`--purge`) for `dpkg`.

`# apt-get autoclean`  
Clears out the local repository of retrieved package files. Only removes package files that can no longer be downloaded, and are largely useless.

`# apt-get autoremove`  
Remove packages that were automatically installed to satisfy dependencies for other packages and are no longer needed.

`# apt update`  
Update APT repositories (i.e., download package information from all configured sources).

This command updates the local package availability database. It consults all package sources and integrates the results into a common package list.

`apt list --upgradeable`  
List APT packages with available upgrades.

`# apt upgrade`  
Conservatively upgrade APT packages. A single APT package can be upgraded by providing its name to this command as an argument.

This command installs the newest available versions of all packages installed on the system. It will not remove installed packages, nor install new packages. Packages that cannot be updated without such action (because dependencies have changed) remain at their present state.

Contrast to `# dpkg -i ex_pkg.deb` (`--install`) for `dpkg`.

`# apt full-upgrade`  
Liberally upgrade APT packages, with removals.

This command enables a conflict resolution scheme that tries to resolve changed dependencies by judiciously removing and installing packages. It prefers more important packages (according to their priority) over less important ones.

`apt-key list`  
List trusted keys with fingerprints. Can be used to obtain `key_id` values.

This command lists the keys associated with the GNU/Linux system's packaging system, APT, which are used to authenticate packages. It lists the key values in the `/etc/apt/trusted.gpg.d/` directory's files.

`# apt-key del ex_key_id`  
Remove a key.

### Fedora

Fedora binary packages are _RPM Package Manager (RPM)_ packages (`.rpm`) whose names follow this format:

`ex_pkg_name-ex_ver_num-ex_release_num.ex_dist.ex_arch.rpm`

The compatible distribution (`ex_dist`) component is optional.

Essentially, RPM packages are `cpio` archives with a prepended header. This fact can be used to extract individual files from an RPM package without having to install the package first. To do so, you can convert a RPM package into a `cpio` archive using the `rpm2cpio` program, and feed the archive into `cpio`.

#### Low-Level

Low-level package management on Fedora is handled by [RPM](https://en.wikipedia.org/wiki/RPM_Package_Manager), which uses the `rpm` command, followed by a basic mode. There are a number of global options, as well as supplementary, mode-specific options.

Since some modes and supplementary options are identical, the mode must be specified first. Global options include `-v` and `-vv`, which increases the verbosity of RPM's output.

`/var/lib/rpm/`  
RPM database directory.

`/usr/lib/rpm/`  
RPM configuration directory.

##### Commands

`rpm -q -a`  
List all packages in the RPM database. By default, this only lists the full package names.

`rpm -q ex_pkg_name...`  
List RPM database entry for a specific package. If provided an abbreviated internal package name, the full package name will be output, if it is installed.

`rpm -q -l -p ex_pkg.rpm`  
List the files in a `.rpm` package.

`rpm -q -l ex_pkg...`  
List the files in a RPM package.

The files listed are only those that appear in the RPM database, i.e., those that a package brought with it when it was installed. This does not include files that were generated during installation or during system operation.

The `-c` (`--configfiles`) and `-d` (`--docfiles`) options behave like `-l`, except that they can be used to view only configuration and documentation files, respectively.

`rpm -q -f ex_file...`  
Determine which RPM package a file belongs to.

This command lets you relate unknown files to a package.

`rpm -q -a | grep 'ex_pkg_name_string'  
Search the RPM database for a package name string.

`rpm -q -i -p ex_pkg.rpm`
Display information about a `.rpm` package. The output does not include dependency information. When used with `-q`, `-i` is short for `--info`.

`rpm -q -i ex_pkg...`  
Display information about a RPM package. The output does not include dependency information.

`rpm -q -R -p ex_pkg.rpm`  
List capabilities on which this `.rpm` package depends.

This command displays the _files_ and _capabilities_ (e.g., `web_browser`) that a package requires. Letting packages depend on capabilities, instead of specific dependencies, gives RPM flexibility.

`rpm -q -R ex_pkg...`  
List capabilities on which a RPM package depends.

`rpm -q --provides ex_pkg...`  
Display capabilities that a RPM package offers.

`rpm -q --whatprovides ex_capability`  
Display RPM package(s) providing a specific capability.

`rpm -q --whatrequires ex_capability`  
Display RPM packages that require a specific capability for proper functioning.

`rpm -q --changelog ex_pkg...`  
Show changelog for a RPM package.

`# rpm -i ex_pkg.rpm...`  
Install a `.rpm` package.

`rpm` automatically checks to see if dependent software is installed before installing a new package.

Shell search patterns are supported.

The `--test` and `-h` (`--hash`) options can be added to `# rpm -i ex_pkg.rpm...` to check for conflicts _before_ installation and to add a progress bar, respectively.

Instead of providing a `.rpm` package as an argument, an HTTP or FTP URL can be substituted in order to install package files from a remote server.

`# rpm -i --nodeps ex_pkg.rpm...`  
Do not do a dependency check before installing or upgrading a package (i.e., force install a `.rpm` package with dependency errors).

You can also use other options to influence the installation itself (rather than just the security checks), e.g., to specify a different installation directory for a program, so you can have two different versions of it installed at the same time.

`# rpm -i --replacefiles ex_pkg.rpm...`  
Install the package(s) even if it replaces files from other, already installed, packages (i.e., force install a `.rpm` package that will overwrite files).

`# rpm -i --force ex_pkg.rpm...`  
Same as using the `--replacepkgs`, `--replacefiles`, and `--oldpackage` options (i.e., force install a `.rpm` package that requires safety overrides, including overwriting files).

Conflicts arise if:

- An already-installed package is to be installed again.
- A package is to be installed, even though it is already installed in a different version (mode `-i`) or a more current version (mode `-U`).
- The installation would overwrite a file belonging to a different package.
- A package requires a different package that is not already installed or about to be installed.

`# rpm --reinstall -f ex_pkg.rpm...`  
Reinstall a RPM package from a `.rpm` package file. An alternative is `# rpm -U --replacepkgs ex_pkg.rpm...`, where `-U` is short for `--upgrade`.

`# rpm -e ex_pkg...`  
Remove a RPM package. The provided package name can be abbreviated, as long as it stays unique.

The `--test` and `--nodeps` options can be added to check for conflicts and disable dependency checks (i.e., the package will be removed even if other packages are depending on it), respectively. The `--allmatches` option can be added to remove all versions of a RPM package.

When removing a RPM package, all of the package's installed files will be removed, unless they are configuration files that you have changed. The changed files will not be removed, but renamed by appending the `.rpmsave` suffix (the RPM package determines which of its files will be considered configuration files).

`# rpm -U ex_pkg.rpm...`  
Upgrade a RPM package from a `.rpm` package.

This command removes any older versions of `ex_pkg.rpm` and installs the new one. If `ex_pkg.rpm` is not already installed, it will be installed.

`# rpm -F ex_pkg.rpm...`  
Freshen a RPM package from a `.rpm` package.

This command installs `ex_pkg.rpm` only if an earlier version is already installed (which is subsequently removed), i.e., it is like a stricter version of `# rpm -U ex_pkg.rpm...`.

`alien --to-rpm ex_pkg.deb...`  
Convert a `.deb` package to a `.rpm` package.

To assemble a `.deb` package, the `gcc`, `make`, `debhelper`, `dpkg-dev`, and `dpkg` packages must be installed (`# dnf install gcc make debhelper dpkg-dev dpkg`).

There is no guarantee that the resulting package created by this command will be viable. The farther down a package sits in the system, the smaller the probability that `alien` will create a working package for your GNU/Linux distribution.

`# rpm --rebuilddb`  
Rebuild the RPM Database.

`rpmkeys -K ex_pkg.rpm...`  
Verify a `.rpm` package.

This command compares an checksum of the package to the checksum contained in the `ex_pkg.rpm` itself. This guarantees the proper transmission of the package.

The signature within `ex_pkg.rpm`, which was created using the private PGP or GPG key of the package preparer, is checked using the package preparer's public key, which must be available on your GNU/Linux system. This guarantees that the correct package has arrived.

`rpm -V ex_pkg...`  
Verify a RPM package.

This command's output will contain all files for which at least one _required_ value from the database differs from the _actual_ value within the file system. A `.` signifies agreement, while a letter indicates a deviation.

The following checks are performed:

- Access mode and file type (`M`)
- User owner and group owner (`U`,`G`)
- For symbolic links, the path of the referenced file (`L`)
- For device files, major and minor device numbers (`D`)
- For plain files, the size (`S`)
- Modification time (`T`)
- Content (`5`), i.e., the digest (formerly MD5 sum) differs

Configuration files are unlikely to remain in their original state, so they are labeled with a `c`.

Add the `-a` (`--all`) and `-c` (`--configfiles`) options to verify all RPM packages or package(s) configuration files, respectively.

```console
$ rpm -V openssh
.......T c /etc/init.d/sshd
S.5....T c /etc/pam.d/sshd
S.5....T c /etc/ssh/ssh_config
SM5....T c /etc/ssh/sshd_config
.M...... /usr/bin/ssh
```

`rpm -q gpg-pubkey --queryformat '%{NAME}-%{VERSION}-%{RELEASE}\t%{SUMMARY}\n'`  
View trusted keys.

`rpm -q -i ex_gpg-pubkey...`  
Inspect a specific key (e.g., `rpm -q -i gpg-pubkey-3c3359c4-5c6ae44d`).

`# rpm -e ex_gpg-pubkey...`  
Remove a specific key (e.g., `# rpm -e gpg-pubkey-3c3359c4-5c6ae44d`).

#### High-Level

High-level package management on Fedora is handled by [Dandified YUM (dnf)](https://en.wikipedia.org/wiki/DNF_(software)).

`/var/lib/dnf/`  
DNF database directory.

`/etc/dnf/dnf.conf`  
DNF configuration file.

`/etc/dnf/dnf.conf` may contain software repository URLs and their names, the directory where downloaded packages will be saved, and where the DNF log file will be saved.

`/etc/yum.repos.d/`  
DNF repository sources directory.

This directory contains repository information for `dnf`. Each repository has its own file in the format of `ex_repo.repo`. Each file may contain multiple repositories, which are grouped according to function.

For example, the main distribution repository is in one file along with the repository for the source packages and a repository for the debugging symbols. Another file contains the repositories for the updates, source for the updates, and the debugging symbols for the updates.

A repository either has a `baseurl`, which is a link to the repository, or it has a `metalink`, which returns a list of mirrors for the repository.

##### Commands

`dnf repolist`  
List enabled DNF repositories.

This command can be used to find _repo IDs_. Only enabled repositories are searched with the `dnf` command.

`dnf repolist disabled`  
List disabled repositories.

`# dnf --enablerepo=ex_repo_id repolist`  
Enable a DNF repository. `ex_repo` will be enabled until a system reboot.

`--enablerepo` can accept a comma-separated list of repositories (e.g., `--enablerepo=ex_repo_id_1,ex_repo_id_2,ex_repo_id_3`).

`# dnf --disablerepo=ex_repo_id repolist`  
Disable a DNF repository. `ex_repo` will be disabled until a system reboot.

`--disablerepo` can accept a comma-separated list of repositories (e.g., `--disablerepo=ex_repo_id_1,ex_repo_id_2,ex_repo_id_3`).

`# dnf config-manager --set-enabled ex_repo_id`  
Permanently enable a DNF repository.

`--set-enabled` can accept a space-separated list of repositories (e.g., `--set-enabled ex_repo_id_1 ex_repo_id_2 ex_repo_id_3`).

`# dnf config-manager --set-disabled ex_repo_id`  
Permanently disable a DNF repository.

`--set-disabled` can accept a space-separated list of repositories (e.g., `--set-disabled ex_repo_id_1 ex_repo_id_2 ex_repo_id_3`).

`dnf list ex_pkg...`  
List DNF package in RPM database, in a repository, or both.

This command just gives a line for the package, including its full name. _Installed packages_ are installed on the local system. _Available packages_ can be fetched from repositories. The repository offering the package is displayed on the far right.

Shell search patterns are accepted.

Contrast to `rpm -q ex_pkg...` (`--query`) for `rpm`.

`dnf list installed`  
List locally installed DNF packages.

This command _only_ shows packages installed in the RPM database.

Contrast to `rpm -q -a` (`--query --all`) for `rpm`.

`dnf list updates`  
List installed DNF packages that have updates available.

This command is comparable to `dnf check-update`.

`dnf list extras`  
List locally installed DNF packages that are not available from any repository.

`dnf list available`  
List locally uninstalled (but available, i.e., from a repository) DNF packages.

`dnf list recent`  
List DNF packages that have recently arrived in the repository.

`dnf search ex_string`  
Search repositories for a given string. This command lists all packages whose name or description (i.e., its metadata) contains a given string.

Shell search patterns are supported.

Contrast this to `rpm -q -a | grep 'ex_string'` (`--query --all`) for `rpm`.

`dnf info ex_pkg...`  
Display DNF package information. This command displays a description and summary information about installed and available packages.

Contrast this to `rpm -q -i -p ex_pkg.rpm` (`--query --info --package`) and `rpm -q -i ex_pkg...` (`--query --info`) for `rpm`.

`dnf info installed`  
Display information for all installed DNF packages.

`dnf deplist ex_pkg...`  
Display a DNF package's dependencies. This command produces a list of all direct dependencies and what packages provide those dependencies for the given package(s).

Contrast this to `rpm -q -R -p ex_pkg.rpm` (`--query --requires --package`) and `rpm -q -R ex_pkg...` (`--query --requires`) for `rpm`.

`# dnf install ex_pkg...`, `# dnf install ex_pkg.rpm...`  
Install a DNF or `.rpm` package (e.g., `./ex_package.rpm`).

`dnf` checks whether the active repositories contain an appropriately-named package, resolves any dependencies the package may have, downloads the package and possibly other packages that it depends on, and installs all of them.

`dnf` accepts not just simple package names, but also package names with architecture specifications, version numbers, and release numbers. See `man 8 dnf` for the allowable formats.

Contrast this to `# rpm -i ex_pkg.rpm...` (`--install`) for `rpm`.

`# dnf reinstall ex_pkg...`  
Reinstall a DNF package.

Contrast to `# rpm --reinstall -f ex_pkg.rpm` (`--force`) for `rpm`.

`# dnf install --downloadonly ex_pkg...`  
Download a DNF package to the cache.

`dnf download ex_pkg...`  
Download a DNF package's `.rpm` file to the current directory.

`# dnf remove ex_pkg...`  
Remove a DNF package.

This command will also remove packages that `ex_pkg` depends on, as long as these are not required by another installed package.

Contrast to `# rpm -e ex_pkg...` (`--erase`) for `rpm`.

`# dnf autoremove`  
Remove all leaf packages from the system that were originally installed as dependencies of user-installed packages, but which are no longer required by any such package.

`# dnf clean all`  
Perform cleanup of temporary files for the currently enabled repositories.

`dnf check-update`  
Update DNF repositories (i.e., checks whether any updates at all are available for your system).

This command will also list the packages that have updates available, i.e., it is equivalent to `dnf list updates`.

`# dnf upgrade`  
Upgrade DNF packages. A single DNF package can be upgraded by providing its name to this command as an argument. `dnf` takes care that all dependencies are resolved.

The `--obsoletes` option ensures that `dnf` tries to handle the case where one package has been replaced by another of a different name. This makes full upgrades of the whole dsitribution easier to perform. This option is implied by default.

Contrast to `# rpm -U ex_pkg.rpm...` (`--upgrade`) for `rpm`.

`# dnf upgrade --refresh`  
Update DNF repositories and upgrade DNF packages.

`--refresh` sets repository metadata as expired before running the `upgrade` mode, which causes it to refresh the repository metadata before upgrading packages.

`# dnf history | head`  
List recent DNF transactions. Can be used to obtain transaction ID numbers.

`# dnf history undo last`  
Undo last DNF transaction (i.e., perform the opposite operation to all operations performed in the specified transaction).

`# dnf history rollback ex_transaction_id`  
Undo all transactions performed after the specified transaction. `ex_transaction_id` is the transaction ID number before the problematic transaction.

`ex_transaction_id`'s can be obtained from the `# dnf history` command.

`dnf grouplist`  
Display package groups.

_Package Groups_ are packages that, together, are useful for a certain task. A group is considered installed if all of its _mandatory_ packages are installed. Also, there are _default packages_ and _optional packages_.

`dnf groupinfo ex_pkg_group...`  
Show which packages a package group consists of.

`# dnf groupinstall ex_pkg_group...`  
Install the packages of a group.

The configuration option `group_package_types` determines which class package will be installed, i.e., it tells `dnf` which type of packages in groups will be installed when `groupinstall` is called. The default is: `default, mandatory`. See `man 5 dnf.conf` for more information.

`# dnf groupremove ex_pkg_group...`  
Remove the packages of a group.

This command does not take into account package classes (i.e., `group_package_types` is ignored). Keep in mind, packages can belong to more than one group at a time, so they may be missing from group `A` after having been removed along with group `B`.

## Secure Shell (SSH)

[Secure Shell (SSH)](https://en.wikipedia.org/wiki/Ssh_(Secure_Shell)) is a Transmission Control Protocol/Internet Protocol (TCP/IP)-based networking protocol. It provides data transmission in a public network using strong authentication and encryption. Its applications include interactive sessions, file transfer, and the secure forwarding of other protocols (i.e., [tunneling](https://en.wikipedia.org/wiki/Tunneling_protocol#SSH)).

_Encryption_ is important for preventing unauthorized people listening to network traffic from being able to read the content being transferred. _Authentication_ ensures that you, as the user, are communicating with the correct server and that the server lets you access the correct user account.

[OpenSSH](https://www.openssh.com/), which comes with most GNU/Linux distributions, is a freely available implementation of SSH (on Debian, you can install the OpenSSH server with the `# apt install openssh-server` command). This implementation contains SSH clients, as well as an SSH server (`sshd`).

Used properly, SSH can prevent the following attacks:

- Forged or adulterated DNS entries (_DNS spoofing_).
- When an attacker sends datagrams from one host, which pretend that they come from another (trusted) host (_IP spoofing_).
- When a host can pretend that datagrams come from another (trusted) host (_IP source routing_).
- _Sniffing_ of passwords and content transmitted in the clear on hosts along the transmission path.
- _Manipulation_ of transmitted data by hosts along the transmission path.
- _Attacks_ on the X11 server by means of sniffed authentication data and spoofed connections to the X11 server.

SSH offers a complete replacement for the insecure [Teletype Network (TELNET)](https://en.wikipedia.org/wiki/Telnet), [Remote Login (RLOGIN)](https://en.wikipedia.org/wiki/Berkeley_r-commands#rlogin), and [Remote Shell (RSH)](https://en.wikipedia.org/wiki/Berkeley_r-commands#rsh) protocols. In addition, it enables users to copy files from or to remote hosts, and is a secure replacement for [Remote Copy (RCP)](https://en.wikipedia.org/wiki/Berkeley_r-commands#rcp) and the many applications of [File Transfer Protocol (FTP)](https://en.wikipedia.org/wiki/File_Transfer_Protocol).

There are two versions of the SSH protocol, 1 and 2. Most servers can accept connections using both versions. Avoid using version 1, which exhibits [various security vulnerabilities](https://www.kb.cert.org/vuls/id/684820).

### OpenSSH

A _Certificate Authority (CA)_ issues public key certificates. A public key certificate is a digital message signed with the private key that provides a cryptographic binding between the public key and the organization that owns the private key.

A certificate contains the following information:

- The name of the organization
- The public key of the organization
- The expiration date of the certificate
- The certificate's serial number
- The name of the CA that signed the certificate
- A digital signature from the CA

There are two types of CAs:

1. Internal CA ([OpenSSL](https://www.openssl.org/), the library that OpenSSH relies on, is used to create a CA on your system)
2. External CA

OpenSSH provides the following encryption-enabled components:

- `sshd` Allows remote access to the shell prompt.
- `ssh` The `ssh` client used to connect to `sshd` on another system.
- `scp` Used to securely copy files between systems.
- `sftp` Used to securely FTP files between systems.
- `slogin` Used to access the shell prompt remotely (`slogin` is just a symbolic link to `ssh`).

### System-Specific SSH Configuration

The SSH configuration directory is located at `/etc/ssh/`. Its permissions should be set to `755`.

`/etc/ssh/` contains several important files.

`/etc/ssh/sshd_config`  
The main configuration file for the `sshd` daemon. Its options include:

- `AllowUsers`

    Restricts logins to the SSH server to only the users listed. Specify a list of users separated by spaces.

- `DenyUsers`

    Prevents the users listed from logging in through the SSH server. Specify a list of users separated by spaces.

- `HostKey`

    Specifies which private host key file should be used by SSH.

- `ListenAddress`

    If the host where `sshd` is running has multiple IP addresses assigned, you can restrict `sshd` to only listening on specific addresses using this parameter. The syntax is `ex_listen_address ex_ip_address:ex_port`.

- `PermitRootLogin`

    Specifies whether you can authenticate through the SSH server as the root user.

- `Port`

    Specifies the port on which `sshd` will listen for SSH requests.

- `Protocol`

    Specifies which version of SSH to use. Specify one of the following (`1`, `2`, `2,1`).

`/etc/ssh/ssh_config`  
The main configuration file for the `ssh` client. Its options include:

- `Port`

    Specifies the port number to connect to on the SSH server system to initiate an SSH request.

- `Protocol`

    Specifies which version of SSH to use. Specify one of the following (`1`, `2`, `2,1`).

- `StrictHostKeyChecking`

    If set to a value of `yes`, the client can only establish connections to SSH servers whose public key has already been added to either the `/etc/ssh/ssh_known_hosts` or `${HOME}/.ssh/known_hosts` file.

- `User`

    Specifies the user to log in to the SSH server as.

- The precedence for SSH client configuration settings is as follows:
    1. Any command-line options included with the `ssh` command at the shell prompt.
    2. Settings in the `${HOME}/.ssh/config` file.
    3. Settings in the `/etc/ssh/ssh_config` file.

`/etc/ssh/ssh_known_hosts`  
This file is used to check the public key of a remote server you are attempting to connect to via SSH (i.e., it is a list of remote SSH server public keys).

`/etc/ssh/ssh_host_ex_key`  
The private part of the host's key structure, where `ex_key` represents a specific cryptographic method that clients can use to check for the server's authenticity (e.g., `rsa`, `ecdsa`).

`/etc/ssh/ssh_host_ex_key.pub`  
The public part of the host's key structure, where `ex_key` represents a specific cryptographic method that clients can use to check for the server's authenticity (e.g., `rsa`, `ecdsa`). Its permissions should be set to `644`.

### User-Specific SSH Configuration

The user-specific SSH configuration directory is `${HOME}/.ssh/`. It contains several important files.

`${HOME}/.ssh/config`  
The user configuration file for the `ssh` client.

`${HOME}/.ssh/authorized_keys`  
Used to store the public keys for remote clients attempting to connect via SSH to a SSH server (i.e., this file is a list of remote SSH client public keys). These keys are matched with the keys presented by an `ssh` or `scp` client upon login request.

`${HOME}/.ssh/known_hosts`  
Used to check the public key of a remote server you are attempting to connect to via SSH (i.e., this file is a list of remote SSH server public keys).

`${HOME}/.ssh/ex_key`  
The private part of the host's key structure, where `ex_key` represents a specific cryptographic method that clients can use to check for the server's authenticity (e.g., `rsa`, `ecdsa`).

`${HOME}/.ssh/ex_key.pub`  
The public part of the host's key structure, where `ex_key` represents a specific cryptographic method that clients can use to check for the server's authenticity (e.g., `rsa`, `ecdsa`). Its permissions should be set to `644`.

### Logging Into a Host

To log into a remote host using SSH, invoke the `ssh` command:

`ssh ex_host`

`ex_host` can be either a hostname or an IP address.

`ssh` assumes that your username on the remote host is the same as the local username. If this is not the case, specify the remote user name:

`ssh ex_username@ex_host`

If `/etc/nologin` is present, no user can log in to the system except for the root user. Non-root users will see the contents of this file (or `/etc/nologin.txt`).

### The SSH Connection

The following steps take place to establish a SSH connection:

1. Client and server send each other information about their host keys and supported cryptographic schemes.

    The client negotiates a shared secret with the server, which then serves as the symmetric key to encrypt the connection. Simultaneously, the client checks the server's authenticity and breaks the connection if there is a discrepancy.

    The details of this process are outlined in [RFC4253](https://tools.ietf.org/html/rfc4253).

2. The server checks the client's authenticity using one of several methods. The password is already sent over the encrypted connection and, unlike other protocols like FTP or TELNET, cannot be sniffed by people who listen in.

The first step is important. The following example shows what happens if you contact the remote host for the _first time_:

```console
$ ssh io.example.com
The authenticity of host 'io.example.com (192.168.33.2)' can't be established.
RSA key fingerprint is 81:24:bf:3b:29:b8:f9:f3:46:57:18:1b:e8:40:5a:09.
Are you sure you want to continue connecting (yes/no)?
```

The host `io.example.com` is still unknown here and `ssh` asks you to verify its host key. If you skip this verification step, you lose the guarantee that no one is listening in to your connection.

After having established a connection using `ssh`, you can use the remote host as if you were in front of it. You can close the connection using the `exit` command or **Ctrl+d**.

Unless you specify otherwise, during interactive `ssh` sessions, the tilde (`~`) is considered a special _escape character_ if it occurs immediately after a newline character. This lets you control `ssh` during an ongoing session.

In particular, the `~.` sequence will close the connection, which may be useful if a program has become stuck at the _remote end_. You can do other interesting things with escape characters. Run `man 1 ssh` for more information.

`ssh` does not restrict you to interactive sessions, but lets you execute single commands on a remote host:

```console
$ ssh io.example.com hostname
io.example.com
```

Keep in mind, the shell on your computer will try to process the command line in order to replace shell wildcard patterns _before_ it is transmitted to the remote host. Use backslashes or quotes to disable filename expansion if you are in doubt.

### Verifying Remote Host Authenticity

Here, there is a danger that someone will intercept your connection request and pretend that they are a remote host that you are trying to connect to, like `io.example.com`. They can surreptitiously establish their own connection to `io.example.com` and pass everything along that you send to them.

Conversely, they can forward `io.example.com`'s answers back to you. You will not see a difference, and the attacker can read everything that you transmit, i.e., a [person-in-the-middle (PITM)](https://en.wikipedia.org/wiki/Man-in-the-middle_attack) attack.

To check for this type of attack, you need to contact the remote system's administrator (via an [out-of-band](https://en.wikipedia.org/wiki/Out-of-band) connection method, e.g., a telephone call) and ask them to read their public key's _fingerprint_. This can be displayed using `ssh-keygen -l -f ex_ssh_pub_key_file` and must be identical to the `key fingerprint` from the SSH login dialogue.

### SSH Key Pairs

The SSH key pairs of a host can be found in the `ssh_host_ex_key` and `ssh_host_ex_key.pub` files within the `/etc/ssh/` and `${HOME}/.ssh/` directories. `ex_key` represents a specific cryptographic method that clients can use to check the server's authenticity.

Possible values of `ex_key` include:

`dsa`  
The [DSA](https://en.wikipedia.org/wiki/Digital_Signature_Algorithm) algorithm. This only allows `1024`-bit keys and should be avoided because it is susceptible to weaknesses in random number generation.

`ecdsa`  
The DSA algorithm [based on elliptic curves](https://en.wikipedia.org/wiki/Elliptic_Curve_Digital_Signature_Algorithm). This lets you pick between `256`, `384`, and `521` bits. Elliptic curves do not need as many bits, so the lower numbers are not problematic.

`ed25519`  
A fast and secure method invented by [Daniel J. Bernstein](https://en.wikipedia.org/wiki/Daniel_J._Bernstein). Within the SSH context, this is [still fairly new](https://en.wikipedia.org/wiki/EdDSA#Ed25519).

`rsa`  
The RSA algorithm. This is secure, as long as you use keys that are longer than `2048` bits (`4096` bits or higher is preferable).

An SSH _key pair_ is a set of two matching keys, one private and one public. The public key may be told to everyone as long as the private key stays confidential. Whatever is encrypted using the public key can _only_ be decrypted using the private key from the same pair, and vice versa (i.e., asymmetric cryptography).

### Verifying Remote Server Authenticity

When connecting to a remote server via SSH and you have ensured that the remote host's public key is authentic, `ssh` will store the remote server's public key in the `${HOME}/.ssh/known_hosts` file once you have agreed to accept the remote server's fingerprint. The `${HOME}/.ssh/known_hosts` file is used as a base for comparison during future connection requests.

If you ever see a `WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!` message when trying to establish a `ssh` connection, you may be the subject of a PITM attack. The public key that the server presents does not match the one stored for the server in the `${HOME}/.ssh/known_hosts` file. You should contact the remote host's administrator to find out what is going on (i.e., perhaps the host key needed to be changed for valid reasons).

How `ssh` behaves in regard to evaluating remote hosts' keys can be configured in the `${HOME}/.ssh/config` (or the `/etc/ssh/ssh_config` file for system-wide changes).

`StrictHostKeyChecking ask`  
The default setting.

`StrictHostKeyChecking no`  
Always accept everything.

`StrictHostKeyChecking yes`  
Never accept anything new.

When `StrictHostKeyChecking yes` is set, you can only establish connections to hosts that are already in your `known_hosts` file. All others will be refused.

### Verifying Remote Client Authenticity

Normally, the SSH server will authenticate you as a user by means of a password that is assigned to your account on the server (usually in `/etc/passwd` or `/etc/shadow`). Since the password is queried only after the encrypted connection has already been established, this is, in principle, safe from unwanted listeners.

However, your password itself is stored on the server. Even though it is encrypted, the password file could be compromised. It would be better if nothing secret about you would be stored on the remote server at all.

You can achieve this by using _public-key client authentication_ instead of the password-based client authentication. In short, you create a key pair for your SSH client consisting of a public and private key, and deposit the public key on the SSH server.

The _public key_ does not need to be specially protected. You will need to keep the _private key_, which should never leave your computer (you can also put your private key on a USB stick or specialized hardware token).

#### Public Key Client Authentication

If you configure the SSH server to use public key authentication, the SSH client tells the SSH server which public key should be used for authentication when the SSH session is initially established. Then, the SSH server checks to see if it has that client’s public key (i.e., is the public key present in the `authorized_keys` file for the associated user). If it does, it generates a random number and encrypts it with that public key.

It then sends the encrypted number to the client, which decrypts it using the private key associated with the public key. The client then calculates a checksum of the number it received from the server.

It sends the checksum to the SSH server, which then calculates its own checksum of the number it originally sent. If the two checksums match, then the user is automatically logged in.

All of the above takes place across the encrypted connection and is secure from unwanted listeners that want to manipulate your data.

To use public-key client authentication, you first need to generate a key pair. This is done using the `ssh-keygen` command:

`ssh-keygen -t rsa -b 4096`

The `-t ex_key_type` option specifies the type of key to create. Accepted values are:

- `dsa`
- `ecdsa`
- `ed25519`
- `rsa`

The `-b ex_bits` option specifies the number of bits in the key to create.

First, the command asks where you want the key pair to be stored. The default location (i.e., `${HOME}/.ssh/`) is reasonable. Next, `ssh-keygen` asks for a _passphrase_. This is used to encrypt the private key in order to prevent someone who happens to find your private key from impersonating you to a SSH server.

You must use keys without a passphrase for non-interactive SSH connections, e.g., for shell scripts and `cron` jobs. In this case, you just press **Enter** when you are asked for the passphrase.

It is possible to connect a public key on the server with a particular command. Then, client connections using this public key will not launch a shell session. Instead, the command in question will be directly started. This can significantly mitigate the security risk with unencrypted private keys for the use of scripts.

The result of `ssh-keygen` are the two files `id_ex_key` and `id_ex_key.pub` in the `${HOME}/.ssh/` directory (the exact key file names will depend on the type of key that you create). The former contains the private key and the latter the public key.

When you first generate a key pair, `ssh-keygen` also shows you the fingerprint of the public key and a _randomart image_. The latter is a graphical representation of the public key, i.e., a kind of graphical fingerprint. Theoretically, this should enable you to tell at a glance whether a public key has changed or not.

The next step is to deposit the public key (i.e., the content of the `id_ex_key.pub` file) in the `${HOME}/.ssh/authorized_keys` file for your user account on the _remote_ host. This is most easily done using the `ssh-copy-id` command:

`ssh-copy-id ex_username@ex_remote_host`

If you set the `PasswordAuthentication` entry in the `/etc/ssh/sshd_config` file on the remote server to `no` and `PubkeyAuthentication` to `yes`, then users can only authenticate via the public key method. Essentially, this is a good idea since attackers enjoy running automatic programs that try obvious passwords on SSH servers.

#### ssh-agent

If you are using a passphrase for your private SSH key, public-key authentication is not more convenient than password authentication, but more secure. If you want to log into the same host as the same user several times in a row, constantly re-entering your private SSH key's passphrase can be a nuisance. The `ssh-agent` was developed to help with this.

The `ssh-agent` program remembers your private SSH key's passphrase and passes it to SSH client programs, as needed. For example, the program is started using `ssh-agent bash`. This opens a new `bash` shell in which you must add the private SSH key passphrase using `ssh-add`:

```console
$ ssh-add
Enter passphrase for /home/amnesia/.ssh/id_rsa: foobar
Identity added: /home/amnesia/.ssh/id_rsa (/home/amnesia/.ssh/id_rsa)
```

Every instance of `ssh`, `scp`, or `sftp` started from the new shell gets the passphrase from the SSH agent. The agent forgets the passphrase once you leave the shell using `exit` or instruct it to using `ssh-add -D` to forget all stored identities (with Debian, the login shell/GUI may be started with the `ssh-agent` active right away, so you can `ssh-add` your passphrase at the very beginning of your session).

`ssh-agent` increases convenience to the detriment of security. If you leave your computer unattended (or if you lose your suspended laptop), an unauthorized person might be able to use SSH programs without being asked for a passphrase. The same applies to programs that get access to your session.

### Secure Copy (scp)

Using `scp`, you can copy files between two hosts via an SSH connection. `scp`'s syntax is based on the `cp` command. Files and directories can be specified with either absolute or relative paths, and `ex_host` can be either a hostname or an IP address.

The following command copies remote files to a local directory:

`scp ex_host:ex_source_file... ex_destination_directory`

This command copies local files to a remote host:

`scp ex_source_file... ex_host:ex_destination_directory`

Directories can be recursively copied via `scp`'s `-r` option. This example recursively downloads a remote directory to a local directory:

`scp -r ex_host:ex_source_directory ex_destination_directory`

It is possible to copy files between two remote hosts, as well. This example copies files from one remote host to another remote host:

```console
scp \
    ex_username@ex_host_1:ex_file... \
    ex_username@ex_host_2:ex_destination_directory
```

### Secure File Transfer Protocol (SFTP)

The `sftp` command is loosely inspired by common FTP clients, but needs an SSH connection. Otherwise, it has nothing to do with FTP. In particular, you cannot use it to communicate with an FTP server.

`sftp ex_username@ex_host`

`ex_host` can be either a hostname or an IP address.

After having established a connection using a command like `sftp ex_username@ex_host`, you can use commands like `get` or `put` to transfer files between your local host and the remote host, inspect the contents of a directory on the remote host using `ls`, and change into different directories on the remote server by means of `cd`.

At the beginning of a session, you will be placed in your home directory on the remote computer. To end a session, enter `quit`.

### SSH Port Forwarding (Tunneling)

SSH can be used to encrypt clear-text traffic by tunneling it through a SSH connection. When client software for the tunneled protocol (e.g., an e-mail client using POP3) establishes a connection with the local SSH client, the traffic is encrypted using SSH and tunneled through to the SSH server.

On the SSH server's end, the traffic is decrypted and forwarded to the appropriate target service (e.g., the POP3 daemon). This is desirable because the information is encrypted before being transmitted, even though the original protocol (POP3) does not support encryption.

To utilize SSH port forwarding, ensure that `AllowTcpForwarding yes` is set in the `/etc/ssh/sshd_config` file.

To create a local SSH tunnel from a local high port to the port that you want to tunnel, you can use the following command (you may need to enter the remote user's password when prompted):

```console
ssh -f -N -L \
    ex_local_port_number:ex_host:ex_port_number_to_tunnel ex_username@ex_host
```

The `-f` option tells `ssh` to go into the background just before command execution and the `-N` option prevents the execution of remote commands. Together, these options are useful for SSH port forwarding.

The `-L` option specifies three things:

1. The local port to be used for the client end of the tunnel (e.g., `59823`). If you want to use a port number less than `1024`, you will need to switch to the root user, as these are _privileged ports_.
2. The hostname or IP address of the remote server running the service that we want to tunnel (e.g., POP3).
3. The port number on the remote server that will be used for the server end of the tunnel (i.e., the port number to tunnel, like port `110` for POP3).

The final argument in the above command, `ex_username@ex_host`, is the remote SSH user and host.

Next, configure the local client program to receive communications via the port that you configured for the client end of the SSH tunnel (i.e., port `59823`). You can test the tunnel that you created using the `netcat` command from the client end of the tunnel. The syntax is `netcat localhost ex_client_tunnel_port`.

For example:

`netcat localhost 59823`

Afterwards, you should see a connection established with the remote system where the tunneled service daemon is running.

SSH can forward and tunnel nearly every other TCP-based protocol by using the `-R` and `-L` options. The following command tunnels connections to the local TCP port `10110` first via an SSH connection to the computer `io.example.com`. From there, it continues (unencrypted) to the TCP port `110` (POP3) on the `mail.example.com` host:

`ssh -L 10110:mail.example.com:110 amnesia@io.example.com`

There are benefits to this approach:

- Imagine that your firewall blocks POP3, but allows SSH traffic. By means of the port redirection, you can enter the internal network via SSH and then connect from the `io.example.com` host to the mail server on the internal network. In your mail program, you need to specify `localhost` and the local TCP port `10110` as the POP3 server.
    - Theoretically, You could forward the local TCP port `110`, but you need to be root to do it.
- The name of the forwarding destination host (i.e., `mail.example.com`) is resolved from the perspective of the SSH server (`io.example.com`). This means that a redirection of the form `ssh -L 10110:localhost:110 amnesia@io.example.com` connects you to port `110` on `io.example.com`, rather than your own computer.
- A port forwarding like `-L 10110:mail.example.com:110` opens port `10110` on all IP addresses on your computer. This opens the redirection, in principle, to all other hosts that can reach this port over the network.

    To prevent this, you can use the fact that `ssh` allows you to specify a local address for the redirected port. With `-L localhost:10110:mail.example.com:110`, the redirection only applies to the local interface.

If you invoke `ssh` as shown in the `ssh -L 10110:mail.example.com:110 amnesia@io.example.com` command, you get an interactive session on top of the port forwarding. If you do not need this (e.g., because the forwarding takes place within a `cron` job), you can specify the previously mentioned `-N` option, which restricts `ssh` to do the port forwarding and not establish an interactive session.

Another technique for automatically forwarding services uses a `ssh` invocation like:

```console
$ ssh -f -L 10110:mail.example.com:110 amnesia sleep 10
$ getmail_fetch -p10110 localhost amnesiamail Mail123 Maildir/
```

The `-f` option causes the `ssh` process to immediately go to the background before the `sleep 10` command is executed. This means that a command that you immediately execute after the `ssh` command (here, `getmail_fetch`, which retrieves email via POP3) has `10` seconds to establish a connection to the local port `10110`. The `ssh` process exists either after `10` seconds or else when the last connection via the local port `10110` is torn down, whichever occurs later.

Port forwarding also works the other way around. For example, `ssh -R 10631:localhost:631 amnesia@io.example.com` opens the TCP port `10631` _on the SSH server_, and connections that programs there make with that port are redirected across the SSH connection to your local host.

Your local host then takes care of redirecting the decrypted data to the destination, here port `631` on your local host. This type of port forwarding is considerably less important than the forwarding using `-L`.

Usually, the `-R` port forwarding binds the remote port to the `localhost` interface on the SSH server. In principle, you can pick another interface, but whether that works or not depends on the configuration of the SSH server.

You can add port forwarding after the fact using the `~C` key combination, which gives you a command line:

```console
$ ~C
ssh> -L 10025:localhost:25
Forwarding port
```

On the command line, you can add `-L` and `-R` options, as if you had typed them directly on the `ssh` command line. Using `-KR`, followed by the port number, you can also cancel a `-R` port forwarding.

With the `~#` command, you can check the currently active connections:

```console
$ ~#
The following connections are open:
  #2 client-session (t4 r0 i0/0 o0/0 fd 6/7 cfd -1)
  #3 direct-tcpip: listening port 10025 for localhost port 25,
    connect from 127.0.0.1 port 57250
    (t4 r1 i0/0 o0/0 fd 9/9 cfd -1)
```

X server traffic can also be tunneled to remote X clients using an SSH connection (i.e., you can execute graphical programs on a remote host, where graphics output and keyboard/mouse input take place on your local computer).

To configure a remote X client _without_ encryption, you can use the following procedure:

1. On the remote X client, enter `xhost +ex_x_server_hostname`. This tells the client to accept connections from the X server.
2. On the X server, enter `export DISPLAY=ex_x_client_hostname:0.0`. This tells the X server to display its output on the remote X client.
3. From the X client, use the `ssh` client to access the shell prompt on the X server and then run the graphical application that you want displayed on the X client.

To use SSH to tunnel the X server traffic between the X server and the X client:

- Use the `-X` option with the `ssh` client program (e.g., `ssh -X ex_username@ex_host`). When logging in using `-X`, the `DISPLAY` variable is set up to point to a proxy X server provided by `sshd`. This directs X clients started on the remote host to this server.

    Everything a remote X client sends to the proxy X server is sent to the (real) X server on the SSH client. All the X11 traffic is encrypted so that eavesdroppers cannot listen in (i.e., it is tunneled).

- Set the `X11Forwarding` option to `yes` in the `/etc/ssh/sshd_config` file on the X server system.

You can also globally enable X11 forwarding in order to avoid having to type the `-X` option. To do so, add `ForwardX11 yes` to your `${HOME}/.ssh/config` SSH client configuration file (or the `/etc/ssh/ssh_config` SSH client configuration file for a system-wide default).

X11 forwarding is preferable to the standard X packet redirection (using `DISPLAY`) not only because of its increased security, but because it is more convenient. You pay for this with some extra effort for encryption (which, on modern hardware, is negligible).

X11 forwarding is not without security risks. Users that can circumvent file access rights on the remote host (e.g., because they are root) may access your local X11 display. For this reason, you should probably avoid globally enabling X11 forwarding. The same risk exists with conventional X11 redirection using `DISPLAY`.

More information on `ssh` configuration can be found by running `man 5 sshd_config`.

### SSH Commands

`ssh-keygen -t ex_key_type -b ex_bits`  
Generate authentication keys for `ssh`.

The `-t` option specifies the type of key and the `-b` option specifies the number of bits in the key to create (e.g., `ssh-keygen -t rsa -b 4096`).

`ssh-keygen -l -f ex_public_key`  
Show fingerprint of specified public key file.

The `-l` option tells `ssh-keygen` to display a public key file fingerprint and the `-f` option is used to specify the filename of the public key file.

Add the `-v` option to display a visual ASCII art representation of the key along with the fingerprint (e.g., `ssh-keygen -l -f "${HOME}/.ssh/id_rsa.pub" -v`).

`ssh-copy-id ex_host`  
Use locally available keys to authorize logins on a remote machine, i.e., upload your user's public keys to a remote server (this command assumes that you already have access to `ex_host`). `ex_host` can be either a hostname or an IP address.

`ssh-copy-id` assembles a list of one or more fingerprints and tries to log in with each key to see if any of them are already installed. Then, it assembles a list of those that failed to log in and, using `ssh`, enables logins with those keys on the remote server. By default, it adds the keys by appending them to the remote user's `${HOME}/.ssh/authorized_keys` file.

The alternative `ssh-copy-id ex_username@ex_host` form can be used to specify a specific user.

The `-i ex_identity_file` option can be used to tell `ssh-copy-id` to only use the key(s) contained in `ex_identity_file`.

`ssh ex_destination`  
Connect and log into `ex_destination`, which may be specified as `ex_host`, `ex_username@ex_host`, or a URI of the form `ssh://[user@]hostname[:port]`, where `[user@]` and `[:port]` are optional.

Add the `-p ex_port` option to `ssh ex_destination` to specify the port to connect to on the remote host (`ssh -p ex_port ex_destination`).

Add the `-i ex_identity_file` option to `ssh ex_destination` to select the file from which the identity (i.e., private key) for public key authentication is read (`ssh -i ex_identity_file ex_destination`).

Append `ex_command` to `ssh ex_destination` to specify a command to execute on the remote host, instead of being provided with a login shell (`ssh ex_destination ex_command`).

To log out of a SSH session, use the `exit` command or **Ctrl+d**.

`ssh-agent "${SHELL}"`  
Invoke the ssh-agent as a wrapper to your shell. This command puts you in a shell that is spawned by `ssh-agent`.

The `ssh-agent` resides in memory and places a security wrapper around your shell session, answering any SSH-related security requests for you. After entering a private SSH key's passphrase once, `ssh-agent` will remember it and automatically apply it for you, when necessary.

`ssh-add`  
Add your user's private SSH key to `ssh-agent`. When prompted, enter the key file’s passphrase. Once done, `ssh-agent` stores the passphrase in memory. Then, it listens for SSH requests and automatically provides the key passphrase for you, when requested.

This command needs to be run after `ssh-agent "${SHELL}"`. It tries to add the standard key _identity_ to the key manager.

A key with a different location can be added by entering `ssh-add ex_key_path`.

`scp ex_source... ex_target`  
Copy objects between network hosts.

`ex_source` and `ex_target` can be specified as a local path, a remote host with optional path in the form `[user@]host:[path]` (e.g., `scp ex_file... ex_username@ex_host:ex_directory` or `scp ex_username@ex_host:ex_file... ex_directory`), or a URI in the form `scp://[user@]host[:port][/path]`.

Add the `-r` option to copy directories recursively.

Add the `-P ex_port` option to specify the port to connect to on the remote host.

Add the `-i ex_identity_file` option to select the file from which the identity (i.e., private key) for public key authentication is read.

Make sure that all used options come _before_ the hosts in question in your `scp` commands.

### Documentation

For more information on SSH, run the following commands:

- `man 8 sshd`
- `man 1 ssh`
- `man 5 ssh_config`
- `man 1 ssh-keygen`
- `man 1 ssh-add`
- `man 1 ssh-agent`
- `man 1 scp`
- `man 1 sftp`

## Documentation

- [Ansible Documentation](https://docs.ansible.com/ansible/latest/index.html)
- [ArchWiki](https://wiki.archlinux.org/)
- [Boot Loader Specification](https://systemd.io/BOOT_LOADER_SPECIFICATION/)
- [Buildah](https://buildah.io/)
- [Certbot Documentation](https://eff-certbot.readthedocs.io/en/stable/)
- [Cockpit Documentation](https://cockpit-project.org/documentation.html)
- [Content-Security-Policy Header Reference](https://content-security-policy.com/)
- [coreboot Documentation](https://doc.coreboot.org/)
- [Cron Expression Generator and Describer](https://www.freeformatter.com/cron-expression-generator-quartz.html)
- [curl - The Art Of Scripting HTTP Requests Using Curl](https://curl.se/docs/httpscripting.html)
- [Debian Administrator's Handbook](https://www.debian.org/doc/manuals/debian-handbook/)
- [Debian Bug Tracking System](https://www.debian.org/Bugs/)
- [Debian Documentation](https://www.debian.org/doc/)
- [Debian GitLab Instance](https://salsa.debian.org/)
- [Debian Mailing Lists](https://www.debian.org/MailingLists/)
- [Debian Releases](https://www.debian.org/releases/)
- [Debian Teams](https://wiki.debian.org/Teams)
- [Debian Users' Manuals](https://www.debian.org/doc/user-manuals)
- [DebianOn](https://wiki.debian.org/InstallingDebianOn)
- [DebianStable Wiki](https://wiki.debian.org/DebianStable)
- [Decimal/Binary Converter](https://www.exploringbinary.com/binary-converter/)
- [DevDocs](https://devdocs.io/)
- [dnf Documentation](https://dnf.readthedocs.io/en/latest/)
- [Docker Docs](https://docs.docker.com/)
- [Email Self-Defense](https://emailselfdefense.fsf.org/en/)
- [Fedora Documentation](https://docs.fedoraproject.org/en-US/docs/)
- [Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html)
- [Firefox Source Tree Documentation](https://firefox-source-docs.mozilla.org/index.html)
- [firewalld](https://firewalld.org/documentation/)
- [Flatpak Documentation](https://docs.flatpak.org/en/latest/)
- [Free Software Directory](https://directory.fsf.org/wiki/Main_Page)
- [Get Tor Bridges](https://bridges.torproject.org/)
- [GitHub Documentation](https://docs.github.com/en)
- [GitLab Documentation](https://docs.gitlab.com/)
- [GNOME Apps](https://wiki.gnome.org/Apps)
- [GNOME Development](https://developer.gnome.org/)
- [GNOME GitLab Instance](https://gitlab.gnome.org/explore/groups)
- [GNOME Help](https://help.gnome.org/)
- [GNOME Wiki](https://wiki.gnome.org/)
- [GNU GRUB Manual](https://www.gnu.org/software/grub/manual/grub/grub.html)
- [GNU Software](https://www.gnu.org/software/)
- [How To Use AppArmor](https://wiki.debian.org/AppArmor/HowToUse)
- [Internet Relay Chat Help](https://www.irchelp.org/)
- [JSON](https://www.json.org/json-en.html)
- [Kubernetes Documentation](https://kubernetes.io/docs/home/)
- [LibreOffice](https://documentation.libreoffice.org/en/english-documentation/)
- [libvirt](https://libvirt.org/docs.html)
- [Linux Containers](https://linuxcontainers.org/)
- [Linux Documentation Project](https://tldp.org/)
- [Linux Kernel Documentation](https://docs.kernel.org/)
- [Linux Kernel Wikis](https://www.wiki.kernel.org/)
- [Linux man Pages Online](https://man7.org/linux/man-pages/)
- [Linux Professional Institute – Learning](https://learning.lpi.org/en/)
- [Liquid Reference](https://shopify.dev/api/liquid)
- [MDN Web Docs](https://developer.mozilla.org)
- [Nextcloud Documentation](https://docs.nextcloud.com/)
- [nftables Wiki](https://wiki.nftables.org/wiki-nftables/index.php/Main_Page)
- [NGINX Wiki](https://www.nginx.com/resources/wiki/)
- [nmcli](https://developer.gnome.org/NetworkManager/stable/nmcli.html)
- [OSDev Wiki](https://wiki.osdev.org/Main_Page)
- [Packages Search](https://pkgs.org/)
- [Pandoc](https://pandoc.org/MANUAL.html)
- [Podman](https://docs.podman.io/en/latest/)
- [Red Hat Developer Learn](https://developers.redhat.com/learn)
- [reStructuredText](https://docutils.sourceforge.io/rst.html)
- [SELinux Notebook](https://github.com/SELinuxProject/selinux-notebook/blob/main/src/toc.md)
- [Shellcheck Error Guide](https://gist.github.com/nicerobot/53cee11ee0abbdc997661e65b348f375)
- [Sphinx Documentation](https://www.sphinx-doc.org/en/master/)
- [Syslinux](https://wiki.syslinux.org/wiki/index.php?title=The_Syslinux_Project)
- [systemd](https://systemd.io/)
- [Tails Documentation](https://tails.boum.org/doc/index.en.html)
- [timeanddate](https://www.timeanddate.com/)
- [Tor Browser User Manual](https://tb-manual.torproject.org/)
- [UEFI Specifications](https://uefi.org/specifications)
- [Vim Documentation](https://www.vim.org/docs.php)
- [Vim Help Files](https://vimhelp.org/)
- [WebAssembly](https://webassembly.org/)
- [YAML Specification](https://yaml.org/spec/1.2.2/)