# References

A collection of reference materials.

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="data_refs/">Data References</a></td>
      <td>A collection of data reference materials.</td>
    </tr>
    <tr>
      <td><a href="git_ref/">Git Reference</a></td>
      <td>A collection of Git reference materials.</td>
    </tr>
    <tr>
      <td><a href="gnu_linux_ref/">GNU/Linux Reference</a></td>
      <td>A collection of GNU/Linux reference materials.</td>
    </tr>
    <tr>
      <td><a href="kb_shortcut_ref/">Keyboard Shortcut Reference</a></td>
      <td>A collection of keyboard shortcut reference materials.</td>
    </tr>
    <tr>
      <td><a href="py_refs">Python References</a></td>
      <td>A collection of Python reference materials.</td>
    </tr>
    <tr>
      <td><a href="regex_ref/">Regular Expression Reference</a></td>
      <td>A collection of regular expression reference materials.</td>
    </tr>
    <tr>
      <td><a href="web_dev_refs/">Web Development References</a></td>
      <td>A collection of Web development reference materials.</td>
    </tr>
  </tbody>
</table>

Non-code content is licensed under a [CC BY-NC-ND 4.0](http://creativecommons.org/licenses/by-nc-nd/4.0/) license.

## Project Avatar

`logo.png` is [library-pictogram.svg](https://openclipart.org/detail/173650/library-pictogram-by-libberry-173650) by [libberry](https://openclipart.org/artist/libberry) and is in the [public domain](https://openclipart.org/faq).